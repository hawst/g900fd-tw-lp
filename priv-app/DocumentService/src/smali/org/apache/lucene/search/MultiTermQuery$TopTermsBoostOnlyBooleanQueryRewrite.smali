.class public final Lorg/apache/lucene/search/MultiTermQuery$TopTermsBoostOnlyBooleanQueryRewrite;
.super Lorg/apache/lucene/search/TopTermsRewrite;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TopTermsBoostOnlyBooleanQueryRewrite"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TopTermsRewrite",
        "<",
        "Lorg/apache/lucene/search/BooleanQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopTermsRewrite;-><init>(I)V

    .line 195
    return-void
.end method


# virtual methods
.method protected addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;F)V
    .locals 2
    .param p1, "topLevel"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "boost"    # F

    .prologue
    .line 209
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v1, p2}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 210
    .local v0, "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0, p3}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 211
    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 212
    return-void
.end method

.method protected bridge synthetic addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;F)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/Query;
    .param p2, "x1"    # Lorg/apache/lucene/index/Term;
    .param p3, "x2"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    check-cast p1, Lorg/apache/lucene/search/BooleanQuery;

    .end local p1    # "x0":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/search/MultiTermQuery$TopTermsBoostOnlyBooleanQueryRewrite;->addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;F)V

    return-void
.end method

.method protected getMaxSize()I
    .locals 1

    .prologue
    .line 199
    invoke-static {}, Lorg/apache/lucene/search/BooleanQuery;->getMaxClauseCount()I

    move-result v0

    return v0
.end method

.method protected getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiTermQuery$TopTermsBoostOnlyBooleanQueryRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method
