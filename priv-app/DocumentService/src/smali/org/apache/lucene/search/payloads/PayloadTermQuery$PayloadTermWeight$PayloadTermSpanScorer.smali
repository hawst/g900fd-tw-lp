.class public Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;
.super Lorg/apache/lucene/search/spans/SpanScorer;
.source "PayloadTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PayloadTermSpanScorer"
.end annotation


# instance fields
.field protected payload:[B

.field protected payloadScore:F

.field protected payloadsSeen:I

.field protected positions:Lorg/apache/lucene/index/TermPositions;

.field final synthetic this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;Lorg/apache/lucene/search/spans/TermSpans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V
    .locals 1
    .param p2, "spans"    # Lorg/apache/lucene/search/spans/TermSpans;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p5, "norms"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    .line 102
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/search/spans/SpanScorer;-><init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V

    .line 95
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:[B

    .line 103
    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/TermSpans;->getPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->positions:Lorg/apache/lucene/index/TermPositions;

    .line 104
    return-void
.end method


# virtual methods
.method protected explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-super {p0, p1}, Lorg/apache/lucene/search/spans/SpanScorer;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 184
    .local v0, "nonPayloadExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v1}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 186
    .local v1, "payloadBoost":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getPayloadScore()F

    move-result v2

    .line 187
    .local v2, "payloadScore":F
    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 190
    const-string/jumbo v4, "scorePayload(...)"

    invoke-virtual {v1, v4}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 192
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 193
    .local v3, "result":Lorg/apache/lucene/search/ComplexExplanation;
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v4, v4, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z
    invoke-static {v4}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$000(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 194
    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 195
    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 196
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    mul-float/2addr v4, v2

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 197
    const-string/jumbo v4, "btq, product of:"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 203
    :goto_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_1
    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 205
    return-object v3

    .line 199
    :cond_0
    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 200
    invoke-virtual {v3, v2}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 201
    const-string/jumbo v4, "btq(includeSpanScore=false), result of:"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_1
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method protected getPayloadScore()F
    .locals 5

    .prologue
    .line 175
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v2, v2, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$300(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/search/payloads/PayloadFunction;->docScore(ILjava/lang/String;IF)F

    move-result v0

    return v0
.end method

.method protected getSpanScore()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->score()F

    move-result v0

    return v0
.end method

.method protected processPayload(Lorg/apache/lucene/search/Similarity;)V
    .locals 15
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->isPayloadAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->positions:Lorg/apache/lucene/index/TermPositions;

    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:[B

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/index/TermPositions;->getPayload([BI)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:[B

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    iget-object v8, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v9, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$100(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v11

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v12

    iget v13, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    iget v14, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$200(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payload:[B

    const/4 v6, 0x0

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->getPayloadLength()I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/Similarity;->scorePayload(ILjava/lang/String;II[BII)F

    move-result v7

    move-object v0, v8

    move v1, v9

    move-object v2, v10

    move v3, v11

    move v4, v12

    move v5, v13

    move v6, v14

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/payloads/PayloadFunction;->currentScore(ILjava/lang/String;IIIFF)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    .line 135
    iget v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    .line 140
    :cond_0
    return-void
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->this$1:Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;

    iget-object v0, v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z
    invoke-static {v0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$000(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getSpanScore()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getPayloadScore()F

    move-result v1

    mul-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getPayloadScore()F

    move-result v0

    goto :goto_0
.end method

.method protected setFreqCurrentDoc()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 108
    iget-boolean v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    if-nez v3, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v2

    .line 111
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    .line 112
    iput v5, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    .line 113
    iput v5, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadScore:F

    .line 114
    iput v2, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->payloadsSeen:I

    .line 115
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    .line 116
    .local v1, "similarity1":Lorg/apache/lucene/search/Similarity;
    :goto_1
    iget-boolean v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->doc:I

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 117
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v4

    sub-int v0, v3, v4

    .line 119
    .local v0, "matchLength":I
    iget v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/Similarity;->sloppyFreq(I)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    .line 120
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->processPayload(Lorg/apache/lucene/search/Similarity;)V

    .line 122
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    goto :goto_1

    .line 125
    .end local v0    # "matchLength":I
    :cond_2
    iget-boolean v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->more:Z

    if-nez v3, :cond_3

    iget v3, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->freq:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method
