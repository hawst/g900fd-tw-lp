.class Lorg/apache/lucene/search/ScoringRewrite$3;
.super Ljava/lang/Object;
.source "ScoringRewrite.java"

# interfaces
.implements Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/ScoringRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/ScoringRewrite;

.field final synthetic val$query:Lorg/apache/lucene/search/MultiTermQuery;

.field final synthetic val$result:Lorg/apache/lucene/search/Query;

.field final synthetic val$size:[I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/ScoringRewrite;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/MultiTermQuery;[I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lorg/apache/lucene/search/ScoringRewrite$3;, "Lorg/apache/lucene/search/ScoringRewrite.3;"
    iput-object p1, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->this$0:Lorg/apache/lucene/search/ScoringRewrite;

    iput-object p2, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->val$result:Lorg/apache/lucene/search/Query;

    iput-object p3, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->val$query:Lorg/apache/lucene/search/MultiTermQuery;

    iput-object p4, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->val$size:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public collect(Lorg/apache/lucene/index/Term;F)Z
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "boost"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lorg/apache/lucene/search/ScoringRewrite$3;, "Lorg/apache/lucene/search/ScoringRewrite.3;"
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->this$0:Lorg/apache/lucene/search/ScoringRewrite;

    iget-object v1, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->val$result:Lorg/apache/lucene/search/Query;

    iget-object v2, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->val$query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v2

    mul-float/2addr v2, p2

    invoke-virtual {v0, v1, p1, v2}, Lorg/apache/lucene/search/ScoringRewrite;->addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;F)V

    .line 96
    iget-object v0, p0, Lorg/apache/lucene/search/ScoringRewrite$3;->val$size:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 97
    const/4 v0, 0x1

    return v0
.end method
