.class public Lorg/apache/lucene/search/FilterManager;
.super Ljava/lang/Object;
.source "FilterManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FilterManager$FilterCleaner;,
        Lorg/apache/lucene/search/FilterManager$FilterItem;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field protected static final DEFAULT_CACHE_CLEAN_SIZE:I = 0x64

.field protected static final DEFAULT_CACHE_SLEEP_TIME:J = 0x927c0L

.field protected static manager:Lorg/apache/lucene/search/FilterManager;


# instance fields
.field protected cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FilterManager$FilterItem;",
            ">;"
        }
    .end annotation
.end field

.field protected cacheCleanSize:I

.field protected cleanSleepTime:J

.field protected filterCleaner:Lorg/apache/lucene/search/FilterManager$FilterCleaner;


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    .line 75
    const/16 v1, 0x64

    iput v1, p0, Lorg/apache/lucene/search/FilterManager;->cacheCleanSize:I

    .line 76
    const-wide/32 v2, 0x927c0

    iput-wide v2, p0, Lorg/apache/lucene/search/FilterManager;->cleanSleepTime:J

    .line 78
    new-instance v1, Lorg/apache/lucene/search/FilterManager$FilterCleaner;

    invoke-direct {v1, p0}, Lorg/apache/lucene/search/FilterManager$FilterCleaner;-><init>(Lorg/apache/lucene/search/FilterManager;)V

    iput-object v1, p0, Lorg/apache/lucene/search/FilterManager;->filterCleaner:Lorg/apache/lucene/search/FilterManager$FilterCleaner;

    .line 79
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lorg/apache/lucene/search/FilterManager;->filterCleaner:Lorg/apache/lucene/search/FilterManager$FilterCleaner;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 81
    .local v0, "fcThread":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 82
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 83
    return-void
.end method

.method public static declared-synchronized getInstance()Lorg/apache/lucene/search/FilterManager;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lorg/apache/lucene/search/FilterManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/apache/lucene/search/FilterManager;->manager:Lorg/apache/lucene/search/FilterManager;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lorg/apache/lucene/search/FilterManager;

    invoke-direct {v0}, Lorg/apache/lucene/search/FilterManager;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FilterManager;->manager:Lorg/apache/lucene/search/FilterManager;

    .line 67
    :cond_0
    sget-object v0, Lorg/apache/lucene/search/FilterManager;->manager:Lorg/apache/lucene/search/FilterManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getFilter(Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Filter;
    .locals 8
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 110
    iget-object v4, p0, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    monitor-enter v4

    .line 111
    const/4 v2, 0x0

    .line 112
    .local v2, "fi":Lorg/apache/lucene/search/FilterManager$FilterItem;
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lorg/apache/lucene/search/FilterManager$FilterItem;

    move-object v2, v0

    .line 113
    if-eqz v2, :cond_0

    .line 114
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    iput-wide v6, v2, Lorg/apache/lucene/search/FilterManager$FilterItem;->timestamp:J

    .line 115
    iget-object p1, v2, Lorg/apache/lucene/search/FilterManager$FilterItem;->filter:Lorg/apache/lucene/search/Filter;

    .end local p1    # "filter":Lorg/apache/lucene/search/Filter;
    monitor-exit v4

    .line 118
    :goto_0
    return-object p1

    .line 117
    .restart local p1    # "filter":Lorg/apache/lucene/search/Filter;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lorg/apache/lucene/search/FilterManager$FilterItem;

    invoke-direct {v6, p0, p1}, Lorg/apache/lucene/search/FilterManager$FilterItem;-><init>(Lorg/apache/lucene/search/FilterManager;Lorg/apache/lucene/search/Filter;)V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    monitor-exit v4

    goto :goto_0

    .line 119
    .end local p1    # "filter":Lorg/apache/lucene/search/Filter;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public setCacheSize(I)V
    .locals 0
    .param p1, "cacheCleanSize"    # I

    .prologue
    .line 90
    iput p1, p0, Lorg/apache/lucene/search/FilterManager;->cacheCleanSize:I

    .line 91
    return-void
.end method

.method public setCleanThreadSleepTime(J)V
    .locals 1
    .param p1, "cleanSleepTime"    # J

    .prologue
    .line 98
    iput-wide p1, p0, Lorg/apache/lucene/search/FilterManager;->cleanSleepTime:J

    .line 99
    return-void
.end method
