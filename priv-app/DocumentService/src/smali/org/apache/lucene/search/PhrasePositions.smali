.class final Lorg/apache/lucene/search/PhrasePositions;
.super Ljava/lang/Object;
.source "PhrasePositions.java"


# instance fields
.field count:I

.field doc:I

.field next:Lorg/apache/lucene/search/PhrasePositions;

.field offset:I

.field final ord:I

.field position:I

.field rptGroup:I

.field rptInd:I

.field final terms:[Lorg/apache/lucene/index/Term;

.field tp:Lorg/apache/lucene/index/TermPositions;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/TermPositions;II[Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/TermPositions;
    .param p2, "o"    # I
    .param p3, "ord"    # I
    .param p4, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    .line 40
    iput p2, p0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    .line 41
    iput p3, p0, Lorg/apache/lucene/search/PhrasePositions;->ord:I

    .line 42
    iput-object p4, p0, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    .line 43
    return-void
.end method


# virtual methods
.method final firstPosition()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhrasePositions;->nextPosition()Z

    .line 71
    return-void
.end method

.method final next()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    iget-object v1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->close()V

    .line 48
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 53
    :goto_0
    return v0

    .line 51
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 52
    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 53
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final nextPosition()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    if-lez v0, :cond_0

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 82
    const/4 v0, 0x1

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 57
    iget-object v1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1, p1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->close()V

    .line 59
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 64
    :goto_0
    return v0

    .line 62
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/PhrasePositions;->tp:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 63
    iput v0, p0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 64
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "d:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " o:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " p:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " c:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "s":Ljava/lang/String;
    iget v1, p0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    if-ltz v1, :cond_0

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " rpt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",i"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    :cond_0
    return-object v0
.end method
