.class public final Lorg/apache/lucene/search/SearcherLifetimeManager$PruneByAge;
.super Ljava/lang/Object;
.source "SearcherLifetimeManager.java"

# interfaces
.implements Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/SearcherLifetimeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PruneByAge"
.end annotation


# instance fields
.field private final maxAgeSec:D


# direct methods
.method public constructor <init>(D)V
    .locals 3
    .param p1, "maxAgeSec"    # D

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "maxAgeSec must be > 0 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$PruneByAge;->maxAgeSec:D

    .line 239
    return-void
.end method


# virtual methods
.method public doPrune(DLorg/apache/lucene/search/IndexSearcher;)Z
    .locals 3
    .param p1, "ageSec"    # D
    .param p3, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 242
    iget-wide v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager$PruneByAge;->maxAgeSec:D

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
