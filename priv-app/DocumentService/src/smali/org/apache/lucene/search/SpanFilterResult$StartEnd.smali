.class public Lorg/apache/lucene/search/SpanFilterResult$StartEnd;
.super Ljava/lang/Object;
.source "SpanFilterResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/SpanFilterResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StartEnd"
.end annotation


# instance fields
.field private end:I

.field private start:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput p1, p0, Lorg/apache/lucene/search/SpanFilterResult$StartEnd;->start:I

    .line 93
    iput p2, p0, Lorg/apache/lucene/search/SpanFilterResult$StartEnd;->end:I

    .line 94
    return-void
.end method


# virtual methods
.method public getEnd()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/search/SpanFilterResult$StartEnd;->end:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lorg/apache/lucene/search/SpanFilterResult$StartEnd;->start:I

    return v0
.end method
