.class Lorg/apache/lucene/search/DisjunctionMaxScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "DisjunctionMaxScorer.java"


# instance fields
.field private doc:I

.field private numScorers:I

.field private scoreMax:F

.field private scoreSum:F

.field private final subScorers:[Lorg/apache/lucene/search/Scorer;

.field private final tieBreakerMultiplier:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Weight;FLorg/apache/lucene/search/Similarity;[Lorg/apache/lucene/search/Scorer;I)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "tieBreakerMultiplier"    # F
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "subScorers"    # [Lorg/apache/lucene/search/Scorer;
    .param p5, "numScorers"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p3, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    .line 60
    iput p2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->tieBreakerMultiplier:F

    .line 64
    iput-object p4, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    .line 65
    iput p5, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    .line 67
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapify()V

    .line 68
    return-void
.end method

.method private heapAdjust(I)V
    .locals 10
    .param p1, "root"    # I

    .prologue
    .line 143
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v8, v9, p1

    .line 144
    .local v8, "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 145
    .local v0, "doc":I
    move v1, p1

    .line 146
    .local v1, "i":I
    :goto_0
    iget v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    shr-int/lit8 v9, v9, 0x1

    add-int/lit8 v9, v9, -0x1

    if-gt v1, v9, :cond_3

    .line 147
    shl-int/lit8 v9, v1, 0x1

    add-int/lit8 v2, v9, 0x1

    .line 148
    .local v2, "lchild":I
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v4, v9, v2

    .line 149
    .local v4, "lscorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    .line 150
    .local v3, "ldoc":I
    const v6, 0x7fffffff

    .local v6, "rdoc":I
    shl-int/lit8 v9, v1, 0x1

    add-int/lit8 v5, v9, 0x2

    .line 151
    .local v5, "rchild":I
    const/4 v7, 0x0

    .line 152
    .local v7, "rscorer":Lorg/apache/lucene/search/Scorer;
    iget v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-ge v5, v9, :cond_0

    .line 153
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v7, v9, v5

    .line 154
    invoke-virtual {v7}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v6

    .line 156
    :cond_0
    if-ge v3, v0, :cond_2

    .line 157
    if-ge v6, v3, :cond_1

    .line 158
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v7, v9, v1

    .line 159
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v5

    .line 160
    move v1, v5

    goto :goto_0

    .line 162
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v4, v9, v1

    .line 163
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v2

    .line 164
    move v1, v2

    goto :goto_0

    .line 166
    :cond_2
    if-ge v6, v0, :cond_3

    .line 167
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v7, v9, v1

    .line 168
    iget-object v9, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v8, v9, v5

    .line 169
    move v1, v5

    goto :goto_0

    .line 174
    .end local v2    # "lchild":I
    .end local v3    # "ldoc":I
    .end local v4    # "lscorer":Lorg/apache/lucene/search/Scorer;
    .end local v5    # "rchild":I
    .end local v6    # "rdoc":I
    .end local v7    # "rscorer":Lorg/apache/lucene/search/Scorer;
    :cond_3
    return-void
.end method

.method private heapRemoveRoot()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 178
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 179
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aput-object v4, v0, v3

    .line 180
    iput v3, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    .line 187
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    aput-object v1, v0, v3

    .line 183
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    add-int/lit8 v1, v1, -0x1

    aput-object v4, v0, v1

    .line 184
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    .line 185
    invoke-direct {p0, v3}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapAdjust(I)V

    goto :goto_0
.end method

.method private heapify()V
    .locals 2

    .prologue
    .line 134
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    shr-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 135
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapAdjust(I)V

    .line 134
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 137
    :cond_0
    return-void
.end method

.method private scoreAll(III)V
    .locals 2
    .param p1, "root"    # I
    .param p2, "size"    # I
    .param p3, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    if-ge p1, p2, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-ne v1, p3, :cond_0

    .line 108
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 109
    .local v0, "sub":F
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    add-float/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    .line 110
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    .line 111
    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1, p2, p3}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(III)V

    .line 112
    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {p0, v1, p2, p3}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(III)V

    .line 114
    .end local v0    # "sub":F
    :cond_0
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v2, 0x0

    .line 118
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-nez v1, :cond_0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    .line 129
    :goto_0
    return v0

    .line 119
    :cond_0
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    if-ge v1, p1, :cond_2

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 121
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapAdjust(I)V

    goto :goto_1

    .line 123
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapRemoveRoot()V

    .line 124
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-nez v1, :cond_0

    .line 125
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    goto :goto_0

    .line 129
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/4 v3, 0x0

    .line 72
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-nez v1, :cond_0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    .line 84
    :goto_0
    return v0

    .line 73
    :cond_0
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    if-ne v1, v2, :cond_2

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 75
    invoke-direct {p0, v3}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapAdjust(I)V

    goto :goto_1

    .line 77
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->heapRemoveRoot()V

    .line 78
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    if-nez v1, :cond_0

    .line 79
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    goto :goto_0

    .line 84
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 97
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 98
    .local v0, "doc":I
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->subScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    iput v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    .line 99
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->numScorers:I

    .line 100
    .local v1, "size":I
    const/4 v2, 0x1

    invoke-direct {p0, v2, v1, v0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(III)V

    .line 101
    const/4 v2, 0x2

    invoke-direct {p0, v2, v1, v0}, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreAll(III)V

    .line 102
    iget v2, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    iget v3, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreSum:F

    iget v4, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->scoreMax:F

    sub-float/2addr v3, v4

    iget v4, p0, Lorg/apache/lucene/search/DisjunctionMaxScorer;->tieBreakerMultiplier:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    return v2
.end method
