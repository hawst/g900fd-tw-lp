.class public Lorg/apache/lucene/search/SpanQueryFilter;
.super Lorg/apache/lucene/search/SpanFilter;
.source "SpanQueryFilter.java"


# instance fields
.field protected query:Lorg/apache/lucene/search/spans/SpanQuery;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/search/SpanFilter;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/search/SpanFilter;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 52
    return-void
.end method


# virtual methods
.method public bitSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v6

    invoke-direct {v0, v6}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 64
    .local v0, "bits":Lorg/apache/lucene/util/FixedBitSet;
    iget-object v6, p0, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v4

    .line 65
    .local v4, "spans":Lorg/apache/lucene/search/spans/Spans;
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0x14

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 66
    .local v5, "tmp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;>;"
    const/4 v1, -0x1

    .line 67
    .local v1, "currentDoc":I
    const/4 v2, 0x0

    .line 68
    .local v2, "currentInfo":Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;
    :goto_0
    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 70
    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v3

    .line 71
    .local v3, "doc":I
    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 72
    if-eq v1, v3, :cond_0

    .line 74
    new-instance v2, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;

    .end local v2    # "currentInfo":Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;
    invoke-direct {v2, v3}, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;-><init>(I)V

    .line 75
    .restart local v2    # "currentInfo":Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    move v1, v3

    .line 78
    :cond_0
    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v6

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;->addPosition(II)V

    goto :goto_0

    .line 80
    .end local v3    # "doc":I
    :cond_1
    new-instance v6, Lorg/apache/lucene/search/SpanFilterResult;

    invoke-direct {v6, v0, v5}, Lorg/apache/lucene/search/SpanFilterResult;-><init>(Lorg/apache/lucene/search/DocIdSet;Ljava/util/List;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 95
    instance-of v0, p1, Lorg/apache/lucene/search/SpanQueryFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    check-cast p1, Lorg/apache/lucene/search/SpanQueryFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SpanQueryFilter;->bitSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;

    move-result-object v0

    .line 57
    .local v0, "result":Lorg/apache/lucene/search/SpanFilterResult;
    invoke-virtual {v0}, Lorg/apache/lucene/search/SpanFilterResult;->getDocIdSet()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    return-object v1
.end method

.method public getQuery()Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    const v1, -0x6dc09b47

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SpanQueryFilter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/SpanQueryFilter;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
