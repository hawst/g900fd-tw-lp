.class final Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;
.super Ljava/lang/Object;
.source "ConstantScoreAutoRewrite.java"

# interfaces
.implements Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CutOffTermCollector"
.end annotation


# instance fields
.field final docCountCutoff:I

.field docVisitCount:I

.field hasCutOff:Z

.field final pendingTerms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field

.field final reader:Lorg/apache/lucene/index/IndexReader;

.field final termCountLimit:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/IndexReader;II)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docCountCutoff"    # I
    .param p3, "termCountLimit"    # I

    .prologue
    const/4 v0, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    .line 131
    iput-boolean v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->hasCutOff:Z

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Ljava/util/ArrayList;

    .line 111
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 112
    iput p2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docCountCutoff:I

    .line 113
    iput p3, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termCountLimit:I

    .line 114
    return-void
.end method


# virtual methods
.method public collect(Lorg/apache/lucene/index/Term;F)Z
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "boost"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    .line 123
    iget-object v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->termCountLimit:I

    if-ge v1, v2, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docVisitCount:I

    iget v2, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->docCountCutoff:I

    if-lt v1, v2, :cond_1

    .line 124
    :cond_0
    iput-boolean v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->hasCutOff:Z

    .line 125
    const/4 v0, 0x0

    .line 127
    :cond_1
    return v0
.end method
