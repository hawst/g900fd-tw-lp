.class public Lorg/apache/lucene/search/TopDocs;
.super Ljava/lang/Object;
.source "TopDocs.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TopDocs$MergeSortQueue;,
        Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;,
        Lorg/apache/lucene/search/TopDocs$ShardRef;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private maxScore:F

.field public scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

.field public totalHits:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/search/TopDocs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopDocs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I[Lorg/apache/lucene/search/ScoreDoc;)V
    .locals 1
    .param p1, "totalHits"    # I
    .param p2, "scoreDocs"    # [Lorg/apache/lucene/search/ScoreDoc;

    .prologue
    .line 53
    const/high16 v0, 0x7fc00000    # NaNf

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    .line 54
    return-void
.end method

.method public constructor <init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V
    .locals 0
    .param p1, "totalHits"    # I
    .param p2, "scoreDocs"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p3, "maxScore"    # F

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    .line 58
    iput-object p2, p0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 59
    iput p3, p0, Lorg/apache/lucene/search/TopDocs;->maxScore:F

    .line 60
    return-void
.end method

.method public static merge(Lorg/apache/lucene/search/Sort;I[Lorg/apache/lucene/search/TopDocs;)Lorg/apache/lucene/search/TopDocs;
    .locals 13
    .param p0, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p1, "topN"    # I
    .param p2, "shardHits"    # [Lorg/apache/lucene/search/TopDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    if-nez p0, :cond_1

    .line 210
    new-instance v5, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;

    invoke-direct {v5, p2}, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;-><init>([Lorg/apache/lucene/search/TopDocs;)V

    .line 215
    .local v5, "queue":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/search/TopDocs$ShardRef;>;"
    :goto_0
    const/4 v9, 0x0

    .line 216
    .local v9, "totalHitCount":I
    const/4 v0, 0x0

    .line 217
    .local v0, "availHitCount":I
    const/4 v4, 0x1

    .line 218
    .local v4, "maxScore":F
    const/4 v8, 0x0

    .local v8, "shardIDX":I
    :goto_1
    array-length v10, p2

    if-ge v8, v10, :cond_2

    .line 219
    aget-object v7, p2, v8

    .line 222
    .local v7, "shard":Lorg/apache/lucene/search/TopDocs;
    iget v10, v7, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    add-int/2addr v9, v10

    .line 223
    iget-object v10, v7, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    if-eqz v10, :cond_0

    iget-object v10, v7, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v10, v10

    if-lez v10, :cond_0

    .line 224
    iget-object v10, v7, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v10, v10

    add-int/2addr v0, v10

    .line 225
    new-instance v10, Lorg/apache/lucene/search/TopDocs$ShardRef;

    invoke-direct {v10, v8}, Lorg/apache/lucene/search/TopDocs$ShardRef;-><init>(I)V

    invoke-virtual {v5, v10}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    invoke-virtual {v7}, Lorg/apache/lucene/search/TopDocs;->getMaxScore()F

    move-result v10

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 218
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 212
    .end local v0    # "availHitCount":I
    .end local v4    # "maxScore":F
    .end local v5    # "queue":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/search/TopDocs$ShardRef;>;"
    .end local v7    # "shard":Lorg/apache/lucene/search/TopDocs;
    .end local v8    # "shardIDX":I
    .end local v9    # "totalHitCount":I
    :cond_1
    new-instance v5, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;

    invoke-direct {v5, p0, p2}, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;-><init>(Lorg/apache/lucene/search/Sort;[Lorg/apache/lucene/search/TopDocs;)V

    .restart local v5    # "queue":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/search/TopDocs$ShardRef;>;"
    goto :goto_0

    .line 231
    .restart local v0    # "availHitCount":I
    .restart local v4    # "maxScore":F
    .restart local v8    # "shardIDX":I
    .restart local v9    # "totalHitCount":I
    :cond_2
    if-nez v0, :cond_3

    .line 232
    const/high16 v4, 0x7fc00000    # NaNf

    .line 235
    :cond_3
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v10

    new-array v3, v10, [Lorg/apache/lucene/search/ScoreDoc;

    .line 237
    .local v3, "hits":[Lorg/apache/lucene/search/ScoreDoc;
    const/4 v2, 0x0

    .line 238
    .local v2, "hitUpto":I
    :cond_4
    :goto_2
    array-length v10, v3

    if-ge v2, v10, :cond_6

    .line 239
    sget-boolean v10, Lorg/apache/lucene/search/TopDocs;->$assertionsDisabled:Z

    if-nez v10, :cond_5

    invoke-virtual {v5}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v10

    if-gtz v10, :cond_5

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 240
    :cond_5
    invoke-virtual {v5}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/TopDocs$ShardRef;

    .line 241
    .local v6, "ref":Lorg/apache/lucene/search/TopDocs$ShardRef;
    iget v10, v6, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v10, p2, v10

    iget-object v10, v10, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    iget v11, v6, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    add-int/lit8 v12, v11, 0x1

    iput v12, v6, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v1, v10, v11

    .line 242
    .local v1, "hit":Lorg/apache/lucene/search/ScoreDoc;
    iget v10, v6, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iput v10, v1, Lorg/apache/lucene/search/ScoreDoc;->shardIndex:I

    .line 243
    aput-object v1, v3, v2

    .line 248
    add-int/lit8 v2, v2, 0x1

    .line 250
    iget v10, v6, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v11, v6, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v11, p2, v11

    iget-object v11, v11, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v11, v11

    if-ge v10, v11, :cond_4

    .line 252
    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 256
    .end local v1    # "hit":Lorg/apache/lucene/search/ScoreDoc;
    .end local v6    # "ref":Lorg/apache/lucene/search/TopDocs$ShardRef;
    :cond_6
    if-nez p0, :cond_7

    .line 257
    new-instance v10, Lorg/apache/lucene/search/TopDocs;

    invoke-direct {v10, v9, v3, v4}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    .line 259
    :goto_3
    return-object v10

    :cond_7
    new-instance v10, Lorg/apache/lucene/search/TopFieldDocs;

    invoke-virtual {p0}, Lorg/apache/lucene/search/Sort;->getSort()[Lorg/apache/lucene/search/SortField;

    move-result-object v11

    invoke-direct {v10, v9, v3, v11, v4}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    goto :goto_3
.end method


# virtual methods
.method public getMaxScore()F
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lorg/apache/lucene/search/TopDocs;->maxScore:F

    return v0
.end method

.method public setMaxScore(F)V
    .locals 0
    .param p1, "maxScore"    # F

    .prologue
    .line 48
    iput p1, p0, Lorg/apache/lucene/search/TopDocs;->maxScore:F

    .line 49
    return-void
.end method
