.class Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;
.super Lorg/apache/lucene/search/Weight;
.source "MatchAllDocsQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MatchAllDocsQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MatchAllDocsWeight"
.end annotation


# instance fields
.field private queryNorm:F

.field private queryWeight:F

.field private similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 1
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;

    .prologue
    .line 87
    iput-object p1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 88
    invoke-virtual {p2}, Lorg/apache/lucene/search/Searcher;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 89
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I

    .prologue
    .line 127
    new-instance v0, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->getValue()F

    move-result v2

    const-string/jumbo v3, "MatchAllDocsQuery, product of:"

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 129
    .local v0, "queryExpl":Lorg/apache/lucene/search/Explanation;
    iget-object v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 130
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    iget-object v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v2

    const-string/jumbo v3, "boost"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 132
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    iget v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryNorm:F

    const-string/jumbo v3, "queryNorm"

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 134
    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "queryNorm"    # F

    .prologue
    .line 114
    iput p1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryNorm:F

    .line 115
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    .line 116
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    iget-object v3, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    # getter for: Lorg/apache/lucene/search/MatchAllDocsQuery;->normsField:Ljava/lang/String;
    invoke-static {v2}, Lorg/apache/lucene/search/MatchAllDocsQuery;->access$000(Lorg/apache/lucene/search/MatchAllDocsQuery;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    # getter for: Lorg/apache/lucene/search/MatchAllDocsQuery;->normsField:Ljava/lang/String;
    invoke-static {v2}, Lorg/apache/lucene/search/MatchAllDocsQuery;->access$000(Lorg/apache/lucene/search/MatchAllDocsQuery;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v5

    :goto_0
    move-object v2, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;-><init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;[B)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    .line 109
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "weight("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
