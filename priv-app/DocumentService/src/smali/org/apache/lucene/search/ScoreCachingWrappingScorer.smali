.class public Lorg/apache/lucene/search/ScoreCachingWrappingScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ScoreCachingWrappingScorer.java"


# instance fields
.field private curDoc:I

.field private curScore:F

.field private final scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 41
    invoke-virtual {p1}, Lorg/apache/lucene/search/Scorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iget-object v1, p1, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curDoc:I

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 43
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/Similarity;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 58
    .local v0, "doc":I
    iget v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curDoc:I

    if-eq v0, v1, :cond_0

    .line 59
    iget-object v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curScore:F

    .line 60
    iput v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curDoc:I

    .line 63
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->curScore:F

    return v1
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 1
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    .line 79
    return-void
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 1
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v0

    return v0
.end method
