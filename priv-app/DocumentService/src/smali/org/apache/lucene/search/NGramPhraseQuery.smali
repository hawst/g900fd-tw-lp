.class public Lorg/apache/lucene/search/NGramPhraseQuery;
.super Lorg/apache/lucene/search/PhraseQuery;
.source "NGramPhraseQuery.java"


# instance fields
.field private final n:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/PhraseQuery;-><init>()V

    .line 42
    iput p1, p0, Lorg/apache/lucene/search/NGramPhraseQuery;->n:I

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 82
    instance-of v2, p1, Lorg/apache/lucene/search/NGramPhraseQuery;

    if-nez v2, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 84
    check-cast v0, Lorg/apache/lucene/search/NGramPhraseQuery;

    .line 85
    .local v0, "other":Lorg/apache/lucene/search/NGramPhraseQuery;
    iget v2, p0, Lorg/apache/lucene/search/NGramPhraseQuery;->n:I

    iget v3, v0, Lorg/apache/lucene/search/NGramPhraseQuery;->n:I

    if-ne v2, v3, :cond_0

    .line 86
    invoke-super {p0, v0}, Lorg/apache/lucene/search/PhraseQuery;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getSlop()I

    move-result v1

    xor-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getTerms()[Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getPositions()[I

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/NGramPhraseQuery;->n:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getSlop()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-super {p0, p1}, Lorg/apache/lucene/search/PhraseQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 76
    :cond_0
    :goto_0
    return-object v2

    .line 50
    :cond_1
    iget v7, p0, Lorg/apache/lucene/search/NGramPhraseQuery;->n:I

    const/4 v8, 0x2

    if-lt v7, v8, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getTerms()[Lorg/apache/lucene/index/Term;

    move-result-object v7

    array-length v7, v7

    const/4 v8, 0x3

    if-ge v7, v8, :cond_3

    .line 52
    :cond_2
    invoke-super {p0, p1}, Lorg/apache/lucene/search/PhraseQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_0

    .line 56
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getPositions()[I

    move-result-object v4

    .line 57
    .local v4, "positions":[I
    invoke-virtual {p0}, Lorg/apache/lucene/search/NGramPhraseQuery;->getTerms()[Lorg/apache/lucene/index/Term;

    move-result-object v6

    .line 58
    .local v6, "terms":[Lorg/apache/lucene/index/Term;
    const/4 v7, 0x0

    aget v5, v4, v7

    .line 59
    .local v5, "prevPosition":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    array-length v7, v4

    if-ge v0, v7, :cond_5

    .line 60
    aget v3, v4, v0

    .line 61
    .local v3, "pos":I
    add-int/lit8 v7, v5, 0x1

    if-eq v7, v3, :cond_4

    invoke-super {p0, p1}, Lorg/apache/lucene/search/PhraseQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_0

    .line 62
    :cond_4
    move v5, v3

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 66
    .end local v3    # "pos":I
    :cond_5
    new-instance v2, Lorg/apache/lucene/search/PhraseQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/PhraseQuery;-><init>()V

    .line 67
    .local v2, "optimized":Lorg/apache/lucene/search/PhraseQuery;
    const/4 v3, 0x0

    .line 68
    .restart local v3    # "pos":I
    array-length v7, v6

    add-int/lit8 v1, v7, -0x1

    .line 69
    .local v1, "lastPos":I
    const/4 v0, 0x0

    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_0

    .line 70
    iget v7, p0, Lorg/apache/lucene/search/NGramPhraseQuery;->n:I

    rem-int v7, v3, v7

    if-eqz v7, :cond_6

    if-lt v3, v1, :cond_7

    .line 71
    :cond_6
    aget-object v7, v6, v0

    aget v8, v4, v0

    invoke-virtual {v2, v7, v8}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;I)V

    .line 73
    :cond_7
    add-int/lit8 v3, v3, 0x1

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
