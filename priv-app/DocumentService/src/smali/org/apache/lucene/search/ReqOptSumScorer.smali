.class Lorg/apache/lucene/search/ReqOptSumScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ReqOptSumScorer.java"


# instance fields
.field private optScorer:Lorg/apache/lucene/search/Scorer;

.field private reqScorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "reqScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "optScorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 41
    iget-object v0, p1, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    .line 44
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 69
    .local v0, "curDoc":I
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 70
    .local v2, "reqScore":F
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v3, :cond_1

    .line 80
    .end local v2    # "reqScore":F
    :cond_0
    :goto_0
    return v2

    .line 74
    .restart local v2    # "reqScore":F
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    .line 75
    .local v1, "optScorerDoc":I
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    const v3, 0x7fffffff

    if-ne v1, v3, :cond_2

    .line 76
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    goto :goto_0

    .line 80
    :cond_2
    if-ne v1, v0, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/search/ReqOptSumScorer;->optScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v3

    add-float/2addr v2, v3

    goto :goto_0
.end method
