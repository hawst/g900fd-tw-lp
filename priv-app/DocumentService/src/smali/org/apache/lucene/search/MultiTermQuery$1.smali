.class Lorg/apache/lucene/search/MultiTermQuery$1;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    return-void
.end method


# virtual methods
.method protected readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;

    .prologue
    .line 93
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    new-instance v1, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;

    invoke-direct {v1, p2}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Filter;)V

    .line 94
    .local v0, "result":Lorg/apache/lucene/search/Query;
    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 95
    return-object v0
.end method
