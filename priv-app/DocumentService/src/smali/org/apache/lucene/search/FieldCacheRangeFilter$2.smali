.class Lorg/apache/lucene/search/FieldCacheRangeFilter$2;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newByteRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;Ljava/lang/Byte;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/Byte;
    .param p4, "x3"    # Ljava/lang/Byte;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    .line 152
    .local v6, "i":B
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->includeLower:Z

    if-nez v0, :cond_0

    const/16 v0, 0x7f

    if-ne v6, v0, :cond_0

    .line 153
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 171
    .end local v6    # "i":B
    :goto_0
    return-object v0

    .line 154
    .restart local v6    # "i":B
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v6    # "i":B
    :goto_1
    int-to-byte v4, v6

    .line 158
    .local v4, "inclusiveLowerPoint":B
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 159
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    .line 160
    .restart local v6    # "i":B
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->includeUpper:Z

    if-nez v0, :cond_3

    const/16 v0, -0x80

    if-ne v6, v0, :cond_3

    .line 161
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 154
    .end local v4    # "inclusiveLowerPoint":B
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 156
    .end local v6    # "i":B
    :cond_2
    const/16 v4, -0x80

    .restart local v4    # "inclusiveLowerPoint":B
    goto :goto_2

    .line 162
    .restart local v6    # "i":B
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v6    # "i":B
    :goto_3
    int-to-byte v5, v6

    .line 167
    .local v5, "inclusiveUpperPoint":B
    :goto_4
    if-le v4, v5, :cond_6

    .line 168
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 162
    .end local v5    # "inclusiveUpperPoint":B
    .restart local v6    # "i":B
    :cond_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .line 164
    .end local v6    # "i":B
    :cond_5
    const/16 v5, 0x7f

    .restart local v5    # "inclusiveUpperPoint":B
    goto :goto_4

    .line 170
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-interface {v1, p1, v2, v0}, Lorg/apache/lucene/search/FieldCache;->getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;)[B

    move-result-object v3

    .line 171
    .local v3, "values":[B
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$2;Lorg/apache/lucene/index/IndexReader;[BBB)V

    goto :goto_0
.end method
