.class Lorg/apache/lucene/search/function/OrdFieldSource$1;
.super Lorg/apache/lucene/search/function/DocValues;
.source "OrdFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/function/OrdFieldSource;->getValues(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/function/OrdFieldSource;

.field final synthetic val$arr:[I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/function/OrdFieldSource;[I)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lorg/apache/lucene/search/function/OrdFieldSource$1;->this$0:Lorg/apache/lucene/search/function/OrdFieldSource;

    iput-object p2, p0, Lorg/apache/lucene/search/function/OrdFieldSource$1;->val$arr:[I

    invoke-direct {p0}, Lorg/apache/lucene/search/function/DocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/function/OrdFieldSource$1;->val$arr:[I

    aget v0, v0, p1

    int-to-float v0, v0

    return v0
.end method

.method getInnerArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/search/function/OrdFieldSource$1;->val$arr:[I

    return-object v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/search/function/OrdFieldSource$1;->val$arr:[I

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/search/function/OrdFieldSource$1;->this$0:Lorg/apache/lucene/search/function/OrdFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/search/function/OrdFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/OrdFieldSource$1;->intVal(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
