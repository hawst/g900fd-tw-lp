.class final Lorg/apache/lucene/search/BooleanScorer$BucketTable;
.super Ljava/lang/Object;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "BucketTable"
.end annotation


# static fields
.field public static final MASK:I = 0x7ff

.field public static final SIZE:I = 0x800


# instance fields
.field final buckets:[Lorg/apache/lucene/search/BooleanScorer$Bucket;

.field first:Lorg/apache/lucene/search/BooleanScorer$Bucket;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x800

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    new-array v1, v3, [Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v1, p0, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->buckets:[Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 155
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 160
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 161
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->buckets:[Lorg/apache/lucene/search/BooleanScorer$Bucket;

    new-instance v2, Lorg/apache/lucene/search/BooleanScorer$Bucket;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanScorer$Bucket;-><init>()V

    aput-object v2, v1, v0

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_0
    return-void
.end method


# virtual methods
.method public newCollector(I)Lorg/apache/lucene/search/Collector;
    .locals 1
    .param p1, "mask"    # I

    .prologue
    .line 166
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;-><init>(ILorg/apache/lucene/search/BooleanScorer$BucketTable;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 169
    const/16 v0, 0x800

    return v0
.end method
