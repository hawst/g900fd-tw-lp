.class public Lorg/apache/lucene/search/spans/SpanNotQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanNotQuery.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private exclude:Lorg/apache/lucene/search/spans/SpanQuery;

.field private include:Lorg/apache/lucene/search/spans/SpanQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 2
    .param p1, "include"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "exclude"    # Lorg/apache/lucene/search/spans/SpanQuery;

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 41
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Clauses must have same field."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/spans/SpanNotQuery;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/SpanNotQuery;

    .prologue
    .line 31
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/spans/SpanNotQuery;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/SpanNotQuery;

    .prologue
    .line 31
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lorg/apache/lucene/search/spans/SpanNotQuery;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/spans/SpanNotQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 72
    .local v0, "spanNotQuery":Lorg/apache/lucene/search/spans/SpanNotQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanNotQuery;->setBoost(F)V

    .line 73
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 193
    if-ne p0, p1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanNotQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 196
    check-cast v0, Lorg/apache/lucene/search/spans/SpanNotQuery;

    .line 197
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanNotQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    return-void
.end method

.method public getExclude()Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInclude()Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/spans/SpanNotQuery$1;-><init>(Lorg/apache/lucene/search/spans/SpanNotQuery;Lorg/apache/lucene/index/IndexReader;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 204
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    .line 205
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0x1

    ushr-int/lit8 v2, v0, 0x1f

    or-int v0, v1, v2

    .line 206
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 207
    shl-int/lit8 v1, v0, 0x1

    ushr-int/lit8 v2, v0, 0x1f

    or-int v0, v1, v2

    .line 208
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    .line 209
    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 172
    .local v0, "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 173
    .local v2, "rewrittenInclude":Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    if-eq v2, v3, :cond_0

    .line 174
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    check-cast v0, Lorg/apache/lucene/search/spans/SpanNotQuery;

    .line 175
    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    iput-object v2, v0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 177
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 178
    .local v1, "rewrittenExclude":Lorg/apache/lucene/search/spans/SpanQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    if-eq v1, v3, :cond_2

    .line 179
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    check-cast v0, Lorg/apache/lucene/search/spans/SpanNotQuery;

    .line 180
    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    :cond_1
    iput-object v1, v0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 183
    :cond_2
    if-eqz v0, :cond_3

    .line 186
    .end local v0    # "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    :goto_0
    return-object v0

    .restart local v0    # "clone":Lorg/apache/lucene/search/spans/SpanNotQuery;
    :cond_3
    move-object v0, p0

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "spanNot("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
