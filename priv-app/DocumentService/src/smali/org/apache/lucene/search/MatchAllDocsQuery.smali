.class public Lorg/apache/lucene/search/MatchAllDocsQuery;
.super Lorg/apache/lucene/search/Query;
.source "MatchAllDocsQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;,
        Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;
    }
.end annotation


# instance fields
.field private final normsField:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "normsField"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery;->normsField:Ljava/lang/String;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/MatchAllDocsQuery;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/MatchAllDocsQuery;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery;->normsField:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;

    .prologue
    .line 140
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllDocsWeight;-><init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 157
    instance-of v2, p1, Lorg/apache/lucene/search/MatchAllDocsQuery;

    if-nez v2, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 159
    check-cast v0, Lorg/apache/lucene/search/MatchAllDocsQuery;

    .line 160
    .local v0, "other":Lorg/apache/lucene/search/MatchAllDocsQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const v1, 0x1aa71190

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "*:*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
