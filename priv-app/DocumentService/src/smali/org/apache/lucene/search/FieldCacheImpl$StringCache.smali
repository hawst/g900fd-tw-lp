.class final Lorg/apache/lucene/search/FieldCacheImpl$StringCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "StringCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 816
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 817
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 10
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "entryKey"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 822
    iget-object v9, p2, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    invoke-static {v9}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 823
    .local v0, "field":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v9

    new-array v1, v9, [Ljava/lang/String;

    .line 824
    .local v1, "retArray":[Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v6

    .line 825
    .local v6, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v9, Lorg/apache/lucene/index/Term;

    invoke-direct {v9, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v9}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v7

    .line 826
    .local v7, "termEnum":Lorg/apache/lucene/index/TermEnum;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v5

    .line 827
    .local v5, "termCountHardLimit":I
    const/4 v3, 0x0

    .local v3, "termCount":I
    move v4, v3

    .line 830
    .end local v3    # "termCount":I
    .local v4, "termCount":I
    :goto_0
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "termCount":I
    .restart local v3    # "termCount":I
    if-ne v4, v5, :cond_1

    .line 846
    :cond_0
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 847
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 849
    return-object v1

    .line 837
    :cond_1
    :try_start_0
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v2

    .line 838
    .local v2, "term":Lorg/apache/lucene/index/Term;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v9

    if-ne v9, v0, :cond_0

    .line 839
    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v8

    .line 840
    .local v8, "termval":Ljava/lang/String;
    invoke-interface {v6, v7}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V

    .line 841
    :goto_1
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 842
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v9

    aput-object v8, v1, v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 846
    .end local v2    # "term":Lorg/apache/lucene/index/Term;
    .end local v8    # "termval":Ljava/lang/String;
    :catchall_0
    move-exception v9

    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 847
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 846
    throw v9

    .line 844
    .restart local v2    # "term":Lorg/apache/lucene/index/Term;
    .restart local v8    # "termval":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    if-eqz v9, :cond_0

    move v4, v3

    .end local v3    # "termCount":I
    .restart local v4    # "termCount":I
    goto :goto_0
.end method
