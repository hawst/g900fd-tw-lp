.class public Lorg/apache/lucene/search/CachingWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "CachingWrapperFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;,
        Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;
    }
.end annotation


# instance fields
.field protected final cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache",
            "<",
            "Lorg/apache/lucene/search/DocIdSet;",
            ">;"
        }
    .end annotation
.end field

.field filter:Lorg/apache/lucene/search/Filter;

.field hitCount:I

.field missCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Filter;)V
    .locals 1
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 138
    sget-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->IGNORE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/CachingWrapperFilter;-><init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V

    .line 139
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V
    .locals 1
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p2, "deletesMode"    # Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    .prologue
    .line 149
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 150
    iput-object p1, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    .line 151
    new-instance v0, Lorg/apache/lucene/search/CachingWrapperFilter$1;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/search/CachingWrapperFilter$1;-><init>(Lorg/apache/lucene/search/CachingWrapperFilter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;

    .line 162
    return-void
.end method


# virtual methods
.method protected docIdSetToCache(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 3
    .param p1, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    if-nez p1, :cond_1

    .line 173
    sget-object p1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 186
    .end local p1    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_0
    :goto_0
    return-object p1

    .line 174
    .restart local p1    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSet;->isCacheable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 181
    .local v1, "it":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v1, :cond_2

    .line 182
    sget-object p1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 184
    :cond_2
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 185
    .local v0, "bits":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->or(Lorg/apache/lucene/search/DocIdSetIterator;)V

    move-object p1, v0

    .line 186
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 225
    instance-of v0, p1, Lorg/apache/lucene/search/CachingWrapperFilter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 226
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    check-cast p1, Lorg/apache/lucene/search/CachingWrapperFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v0

    .line 198
    .local v0, "coreKey":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getDeletesCacheKey()Ljava/lang/Object;

    move-result-object v1

    .line 200
    .local v1, "delCoreKey":Ljava/lang/Object;
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;

    invoke-virtual {v4, p1, v0, v1}, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->get(Lorg/apache/lucene/index/IndexReader;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/DocIdSet;

    .line 201
    .local v2, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-eqz v2, :cond_1

    .line 202
    iget v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->hitCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->hitCount:I

    move-object v3, v2

    .line 215
    .end local v2    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    .local v3, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :goto_1
    return-object v3

    .end local v1    # "delCoreKey":Ljava/lang/Object;
    .end local v3    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_0
    move-object v1, v0

    .line 198
    goto :goto_0

    .line 206
    .restart local v1    # "delCoreKey":Ljava/lang/Object;
    .restart local v2    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    iget v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->missCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->missCount:I

    .line 209
    iget-object v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v4

    invoke-virtual {p0, v4, p1}, Lorg/apache/lucene/search/CachingWrapperFilter;->docIdSetToCache(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v2

    .line 211
    if-eqz v2, :cond_2

    .line 212
    iget-object v4, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;

    invoke-virtual {v4, v0, v1, v2}, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->put(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_2
    move-object v3, v2

    .line 215
    .end local v2    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    .restart local v3    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x1117bf25

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CachingWrapperFilter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
