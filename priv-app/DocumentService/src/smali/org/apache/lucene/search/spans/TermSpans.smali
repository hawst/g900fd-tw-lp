.class public Lorg/apache/lucene/search/spans/TermSpans;
.super Lorg/apache/lucene/search/spans/Spans;
.source "TermSpans.java"


# instance fields
.field protected count:I

.field protected doc:I

.field protected freq:I

.field protected position:I

.field protected positions:Lorg/apache/lucene/index/TermPositions;

.field protected term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermPositions;Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "positions"    # Lorg/apache/lucene/index/TermPositions;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/search/spans/TermSpans;->term:Lorg/apache/lucene/index/Term;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 44
    return-void
.end method


# virtual methods
.method public doc()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->getPayloadLength()I

    move-result v1

    new-array v0, v1, [B

    .line 98
    .local v0, "bytes":[B
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lorg/apache/lucene/index/TermPositions;->getPayload([BI)[B

    move-result-object v0

    .line 99
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 48
    iget v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    iget v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->freq:I

    if-ne v1, v2, :cond_1

    .line 49
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 59
    :goto_0
    return v0

    .line 53
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 54
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->freq:I

    .line 55
    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 57
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    .line 58
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 59
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1, p1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 76
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    .line 70
    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->freq:I

    .line 71
    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->positions:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    .line 74
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->count:I

    .line 76
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "spans("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/TermSpans;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    const-string/jumbo v0, "END"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->doc:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lorg/apache/lucene/search/spans/TermSpans;->position:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
