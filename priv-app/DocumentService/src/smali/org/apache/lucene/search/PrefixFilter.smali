.class public Lorg/apache/lucene/search/PrefixFilter;
.super Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.source "PrefixFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter",
        "<",
        "Lorg/apache/lucene/search/PrefixQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "prefix"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 29
    new-instance v0, Lorg/apache/lucene/search/PrefixQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/PrefixQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    .line 30
    return-void
.end method


# virtual methods
.method public getPrefix()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/apache/lucene/search/PrefixFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/PrefixQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/PrefixQuery;->getPrefix()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "PrefixFilter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {p0}, Lorg/apache/lucene/search/PrefixFilter;->getPrefix()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
