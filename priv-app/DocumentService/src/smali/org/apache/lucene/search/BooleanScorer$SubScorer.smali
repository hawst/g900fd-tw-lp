.class final Lorg/apache/lucene/search/BooleanScorer$SubScorer;
.super Ljava/lang/Object;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SubScorer"
.end annotation


# instance fields
.field public collector:Lorg/apache/lucene/search/Collector;

.field public next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

.field public prohibited:Z

.field public scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;ZZLorg/apache/lucene/search/Collector;Lorg/apache/lucene/search/BooleanScorer$SubScorer;)V
    .locals 2
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "required"    # Z
    .param p3, "prohibited"    # Z
    .param p4, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p5, "next"    # Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    if-eqz p2, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "this scorer cannot handle required=true"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 189
    iput-boolean p3, p0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->prohibited:Z

    .line 190
    iput-object p4, p0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->collector:Lorg/apache/lucene/search/Collector;

    .line 191
    iput-object p5, p0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .line 192
    return-void
.end method
