.class public Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;,
        Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/MultiTermQuery;",
        ">",
        "Lorg/apache/lucene/search/spans/SpanQuery;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SCORING_SPAN_QUERY_REWRITE:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;


# instance fields
.field private getFieldMethod:Ljava/lang/reflect/Method;

.field private getTermMethod:Ljava/lang/reflect/Method;

.field protected final query:Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TQ;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->$assertionsDisabled:Z

    .line 176
    new-instance v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->SCORING_SPAN_QUERY_REWRITE:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/MultiTermQuery;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    .local p1, "query":Lorg/apache/lucene/search/MultiTermQuery;, "TQ;"
    const/4 v5, 0x0

    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 49
    iput-object v5, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getFieldMethod:Ljava/lang/reflect/Method;

    iput-object v5, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getTermMethod:Ljava/lang/reflect/Method;

    .line 66
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    .line 68
    invoke-virtual {p1}, Lorg/apache/lucene/search/MultiTermQuery;->getRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    move-result-object v3

    .line 69
    .local v3, "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    instance-of v5, v3, Lorg/apache/lucene/search/TopTermsRewrite;

    if-eqz v5, :cond_0

    .line 70
    check-cast v3, Lorg/apache/lucene/search/TopTermsRewrite;

    .end local v3    # "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    invoke-virtual {v3}, Lorg/apache/lucene/search/TopTermsRewrite;->getSize()I

    move-result v4

    .line 71
    .local v4, "pqsize":I
    new-instance v5, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;

    invoke-direct {v5, v4}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;-><init>(I)V

    invoke-virtual {p0, v5}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->setRewriteMethod(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;)V

    .line 79
    .end local v4    # "pqsize":I
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string/jumbo v6, "getField"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getFieldMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_1
    return-void

    .line 73
    .restart local v3    # "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :cond_0
    sget-object v5, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->SCORING_SPAN_QUERY_REWRITE:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->setRewriteMethod(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;)V

    goto :goto_0

    .line 80
    .end local v3    # "method":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e1":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string/jumbo v6, "getTerm"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getTermMethod:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 83
    :catch_1
    move-exception v1

    .line 85
    .local v1, "e2":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string/jumbo v6, "getPrefix"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getTermMethod:Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 86
    :catch_2
    move-exception v2

    .line 87
    .local v2, "e3":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "SpanMultiTermQueryWrapper can only wrap MultiTermQueries that can return a field name using getField() or getTerm()"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    const/4 v1, 0x0

    .line 156
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 160
    :cond_0
    :goto_0
    return v1

    .line 157
    :cond_1
    if-eqz p1, :cond_0

    .line 158
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 159
    check-cast v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;

    .line 160
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    iget-object v2, v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getField()Ljava/lang/String;
    .locals 4

    .prologue
    .line 120
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getFieldMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getFieldMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 124
    :goto_0
    return-object v1

    .line 123
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getTermMethod:Ljava/lang/reflect/Method;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Cannot invoke getField() or getTerm() on wrapped query."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 124
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->getTermMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0
.end method

.method public final getRewriteMethod()Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;
    .locals 3

    .prologue
    .line 98
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/MultiTermQuery;->getRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    move-result-object v0

    .line 99
    .local v0, "m":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    instance-of v1, v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    if-nez v1, :cond_0

    .line 100
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v2, "You can only use SpanMultiTermQueryWrapper with a suitable SpanRewriteMethod."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :cond_0
    check-cast v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    .end local v0    # "m":Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Query should have been rewritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 150
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/MultiTermQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 143
    .local v0, "q":Lorg/apache/lucene/search/Query;
    instance-of v1, v0, Lorg/apache/lucene/search/spans/SpanQuery;

    if-nez v1, :cond_0

    .line 144
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v2, "You can only use SpanMultiTermQueryWrapper with a suitable SpanRewriteMethod."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :cond_0
    return-object v0
.end method

.method public final setRewriteMethod(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;)V
    .locals 1
    .param p1, "rewriteMethod"    # Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;

    .prologue
    .line 109
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 110
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 133
    .local p0, "this":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;, "Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper<TQ;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "SpanMultiTermQueryWrapper("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/MultiTermQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
