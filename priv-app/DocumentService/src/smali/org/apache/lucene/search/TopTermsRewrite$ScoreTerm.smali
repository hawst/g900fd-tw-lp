.class Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
.super Ljava/lang/Object;
.source "TopTermsRewrite.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopTermsRewrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScoreTerm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;",
        ">;"
    }
.end annotation


# instance fields
.field public boost:F

.field public term:Lorg/apache/lucene/index/Term;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/TopTermsRewrite$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/TopTermsRewrite$1;

    .prologue
    .line 100
    invoke-direct {p0}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 100
    check-cast p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->compareTo(Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    iget v1, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    iget-object v1, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    iget v1, p1, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    goto :goto_0
.end method
