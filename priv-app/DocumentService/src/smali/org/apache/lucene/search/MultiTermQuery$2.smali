.class Lorg/apache/lucene/search/MultiTermQuery$2;
.super Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;-><init>()V

    return-void
.end method


# virtual methods
.method protected readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 250
    sget-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method public setDocCountPercent(D)V
    .locals 2
    .param p1, "percent"    # D

    .prologue
    .line 245
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Please create a private instance"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setTermCountCutoff(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 240
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Please create a private instance"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
