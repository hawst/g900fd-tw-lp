.class public final Lorg/apache/lucene/search/FieldComparator$FloatComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FloatComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:F

.field private currentReaderValues:[F

.field private final parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

.field private final values:[F


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Float;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Float;

    .prologue
    .line 399
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 400
    new-array v0, p1, [F

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->values:[F

    .line 401
    check-cast p3, Lorg/apache/lucene/search/FieldCache$FloatParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

    .line 402
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 3
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 408
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->values:[F

    aget v0, v2, p1

    .line 409
    .local v0, "v1":F
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->values:[F

    aget v1, v2, p2

    .line 410
    .local v1, "v2":F
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    .line 411
    const/4 v2, 0x1

    .line 415
    :goto_0
    return v2

    .line 412
    :cond_0
    cmpg-float v2, v0, v1

    if-gez v2, :cond_1

    .line 413
    const/4 v2, -0x1

    goto :goto_0

    .line 415
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 423
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->currentReaderValues:[F

    aget v0, v1, p1

    .line 426
    .local v0, "v2":F
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 427
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 429
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->bottom:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_1

    .line 430
    const/4 v1, 0x1

    .line 434
    :goto_0
    return v1

    .line 431
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->bottom:F

    cmpg-float v1, v1, v0

    if-gez v1, :cond_2

    .line 432
    const/4 v1, -0x1

    goto :goto_0

    .line 434
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 440
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->currentReaderValues:[F

    aget v0, v1, p2

    .line 443
    .local v0, "v2":F
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 444
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 446
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->values:[F

    aput v0, v1, p1

    .line 447
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 459
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->values:[F

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->bottom:F

    .line 460
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 453
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->parser:Lorg/apache/lucene/search/FieldCache$FloatParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldCache;->getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->currentReaderValues:[F

    .line 454
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 455
    return-void

    .line 453
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Float;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 464
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->values:[F

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 392
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$FloatComparator;->value(I)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
