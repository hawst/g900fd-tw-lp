.class public Lorg/apache/lucene/search/ParallelMultiSearcher;
.super Lorg/apache/lucene/search/MultiSearcher;
.source "ParallelMultiSearcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;,
        Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/ExecutorService;

.field private final searchables:[Lorg/apache/lucene/search/Searchable;

.field private final starts:[I


# direct methods
.method public varargs constructor <init>(Ljava/util/concurrent/ExecutorService;[Lorg/apache/lucene/search/Searchable;)V
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/ExecutorService;
    .param p2, "searchables"    # [Lorg/apache/lucene/search/Searchable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p2}, Lorg/apache/lucene/search/MultiSearcher;-><init>([Lorg/apache/lucene/search/Searchable;)V

    .line 65
    iput-object p2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    .line 66
    invoke-virtual {p0}, Lorg/apache/lucene/search/ParallelMultiSearcher;->getStarts()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->starts:[I

    .line 67
    iput-object p1, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    .line 68
    return-void
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/search/Searchable;)V
    .locals 2
    .param p1, "searchables"    # [Lorg/apache/lucene/search/Searchable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lorg/apache/lucene/util/NamedThreadFactory;

    const-class v1, Lorg/apache/lucene/search/ParallelMultiSearcher;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/search/ParallelMultiSearcher;-><init>(Ljava/util/concurrent/ExecutorService;[Lorg/apache/lucene/search/Searchable;)V

    .line 58
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 202
    invoke-super {p0}, Lorg/apache/lucene/search/MultiSearcher;->close()V

    .line 203
    return-void
.end method

.method createDocFrequencyMap(Ljava/util/Set;)Ljava/util/HashMap;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v11

    new-array v11, v11, [Lorg/apache/lucene/index/Term;

    invoke-interface {p1, v11}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/index/Term;

    .line 208
    .local v1, "allTermsArray":[Lorg/apache/lucene/index/Term;
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v11

    new-array v0, v11, [I

    .line 209
    .local v0, "aggregatedDocFreqs":[I
    new-instance v9, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;

    iget-object v11, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v9, v11}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 210
    .local v9, "runner":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<[I>;"
    iget-object v2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    .local v2, "arr$":[Lorg/apache/lucene/search/Searchable;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v10, v2, v7

    .line 211
    .local v10, "searchable":Lorg/apache/lucene/search/Searchable;
    new-instance v11, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;

    invoke-direct {v11, v10, v1}, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;-><init>(Lorg/apache/lucene/search/Searchable;[Lorg/apache/lucene/index/Term;)V

    invoke-virtual {v9, v11}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 210
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 214
    .end local v10    # "searchable":Lorg/apache/lucene/search/Searchable;
    :cond_0
    array-length v4, v0

    .line 215
    .local v4, "docFreqLen":I
    invoke-virtual {v9}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    .line 216
    .local v5, "docFreqs":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v4, :cond_1

    .line 217
    aget v11, v0, v6

    aget v12, v5, v6

    add-int/2addr v11, v12

    aput v11, v0, v6

    .line 216
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 221
    .end local v5    # "docFreqs":[I
    .end local v6    # "i":I
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 222
    .local v3, "dfMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    array-length v11, v1

    if-ge v6, v11, :cond_3

    .line 223
    aget-object v11, v1, v6

    aget v12, v0, v6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v3, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 225
    :cond_3
    return-object v3
.end method

.method bridge synthetic createDocFrequencyMap(Ljava/util/Set;)Ljava/util/Map;
    .locals 1
    .param p1, "x0"    # Ljava/util/Set;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/ParallelMultiSearcher;->createDocFrequencyMap(Ljava/util/Set;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v4, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;

    iget-object v6, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v4, v6}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 76
    .local v4, "runner":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v6, v6

    if-ge v1, v6, :cond_0

    .line 77
    iget-object v6, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v5, v6, v1

    .line 78
    .local v5, "searchable":Lorg/apache/lucene/search/Searchable;
    new-instance v6, Lorg/apache/lucene/search/ParallelMultiSearcher$1;

    invoke-direct {v6, p0, v5, p1}, Lorg/apache/lucene/search/ParallelMultiSearcher$1;-><init>(Lorg/apache/lucene/search/ParallelMultiSearcher;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/index/Term;)V

    invoke-virtual {v4, v6}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    .end local v5    # "searchable":Lorg/apache/lucene/search/Searchable;
    :cond_0
    const/4 v0, 0x0

    .line 85
    .local v0, "docFreq":I
    invoke-virtual {v4}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 86
    .local v3, "num":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/2addr v0, v6

    goto :goto_1

    .line 88
    .end local v3    # "num":Ljava/lang/Integer;
    :cond_1
    return v0
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 16
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "nDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    new-instance v7, Lorg/apache/lucene/search/HitQueue;

    const/4 v1, 0x0

    move/from16 v0, p3

    invoke-direct {v7, v0, v1}, Lorg/apache/lucene/search/HitQueue;-><init>(IZ)V

    .line 99
    .local v7, "hq":Lorg/apache/lucene/search/HitQueue;
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 100
    .local v2, "lock":Ljava/util/concurrent/locks/Lock;
    new-instance v12, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v12, v1}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 102
    .local v12, "runner":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<Lorg/apache/lucene/search/TopDocs;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v1, v1

    if-ge v8, v1, :cond_0

    .line 103
    new-instance v1, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v3, v3, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->starts:[I

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v1 .. v9}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/HitQueue;I[I)V

    invoke-virtual {v12, v1}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 102
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 107
    :cond_0
    const/4 v15, 0x0

    .line 108
    .local v15, "totalHits":I
    const/high16 v11, -0x800000    # Float.NEGATIVE_INFINITY

    .line 109
    .local v11, "maxScore":F
    invoke-virtual {v12}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/search/TopDocs;

    .line 110
    .local v14, "topDocs":Lorg/apache/lucene/search/TopDocs;
    iget v1, v14, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    add-int/2addr v15, v1

    .line 111
    invoke-virtual {v14}, Lorg/apache/lucene/search/TopDocs;->getMaxScore()F

    move-result v1

    invoke-static {v11, v1}, Ljava/lang/Math;->max(FF)F

    move-result v11

    goto :goto_1

    .line 114
    .end local v14    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_1
    invoke-virtual {v7}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v1

    new-array v13, v1, [Lorg/apache/lucene/search/ScoreDoc;

    .line 115
    .local v13, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual {v7}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v1

    add-int/lit8 v8, v1, -0x1

    :goto_2
    if-ltz v8, :cond_2

    .line 116
    invoke-virtual {v7}, Lorg/apache/lucene/search/HitQueue;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v1, v13, v8

    .line 115
    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 118
    :cond_2
    new-instance v1, Lorg/apache/lucene/search/TopDocs;

    invoke-direct {v1, v15, v13, v11}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    return-object v1
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 17
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "nDocs"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    if-nez p4, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 130
    :cond_0
    new-instance v7, Lorg/apache/lucene/search/FieldDocSortedHitQueue;

    move/from16 v0, p3

    invoke-direct {v7, v0}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;-><init>(I)V

    .line 131
    .local v7, "hq":Lorg/apache/lucene/search/FieldDocSortedHitQueue;
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 132
    .local v2, "lock":Ljava/util/concurrent/locks/Lock;
    new-instance v13, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v13, v1}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 133
    .local v13, "runner":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<Lorg/apache/lucene/search/TopFieldDocs;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v1, v1

    if-ge v9, v1, :cond_1

    .line 134
    new-instance v1, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v3, v3, v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/search/ParallelMultiSearcher;->starts:[I

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v10}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/FieldDocSortedHitQueue;Lorg/apache/lucene/search/Sort;I[I)V

    invoke-virtual {v13, v1}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 133
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 137
    :cond_1
    const/16 v16, 0x0

    .line 138
    .local v16, "totalHits":I
    const/high16 v12, -0x800000    # Float.NEGATIVE_INFINITY

    .line 139
    .local v12, "maxScore":F
    invoke-virtual {v13}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/search/TopFieldDocs;

    .line 140
    .local v15, "topFieldDocs":Lorg/apache/lucene/search/TopFieldDocs;
    iget v1, v15, Lorg/apache/lucene/search/TopFieldDocs;->totalHits:I

    add-int v16, v16, v1

    .line 141
    invoke-virtual {v15}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v1

    invoke-static {v12, v1}, Ljava/lang/Math;->max(FF)F

    move-result v12

    goto :goto_1

    .line 143
    .end local v15    # "topFieldDocs":Lorg/apache/lucene/search/TopFieldDocs;
    :cond_2
    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->size()I

    move-result v1

    new-array v14, v1, [Lorg/apache/lucene/search/ScoreDoc;

    .line 144
    .local v14, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->size()I

    move-result v1

    add-int/lit8 v9, v1, -0x1

    :goto_2
    if-ltz v9, :cond_3

    .line 145
    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v1, v14, v9

    .line 144
    add-int/lit8 v9, v9, -0x1

    goto :goto_2

    .line 147
    :cond_3
    new-instance v1, Lorg/apache/lucene/search/TopFieldDocs;

    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->getFields()[Lorg/apache/lucene/search/SortField;

    move-result-object v3

    move/from16 v0, v16

    invoke-direct {v1, v0, v14, v3, v12}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    return-object v1
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 4
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 171
    iget-object v3, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->starts:[I

    aget v2, v3, v1

    .line 173
    .local v2, "start":I
    new-instance v0, Lorg/apache/lucene/search/ParallelMultiSearcher$2;

    invoke-direct {v0, p0, p3, v2}, Lorg/apache/lucene/search/ParallelMultiSearcher$2;-><init>(Lorg/apache/lucene/search/ParallelMultiSearcher;Lorg/apache/lucene/search/Collector;I)V

    .line 195
    .local v0, "hc":Lorg/apache/lucene/search/Collector;
    iget-object v3, p0, Lorg/apache/lucene/search/ParallelMultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v3, v3, v1

    invoke-interface {v3, p1, p2, v0}, Lorg/apache/lucene/search/Searchable;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    .end local v0    # "hc":Lorg/apache/lucene/search/Collector;
    .end local v2    # "start":I
    :cond_0
    return-void
.end method
