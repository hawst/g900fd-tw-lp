.class public final Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StringComparatorLocale"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:Ljava/lang/String;

.field final collator:Ljava/text/Collator;

.field private currentReaderValues:[Ljava/lang/String;

.field private final field:Ljava/lang/String;

.field private final values:[Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 766
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 767
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->values:[Ljava/lang/String;

    .line 768
    iput-object p2, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->field:Ljava/lang/String;

    .line 769
    invoke-static {p3}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->collator:Ljava/text/Collator;

    .line 770
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 3
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 774
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->values:[Ljava/lang/String;

    aget-object v0, v2, p1

    .line 775
    .local v0, "val1":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->values:[Ljava/lang/String;

    aget-object v1, v2, p2

    .line 776
    .local v1, "val2":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 777
    if-nez v1, :cond_0

    .line 778
    const/4 v2, 0x0

    .line 784
    :goto_0
    return v2

    .line 780
    :cond_0
    const/4 v2, -0x1

    goto :goto_0

    .line 781
    :cond_1
    if-nez v1, :cond_2

    .line 782
    const/4 v2, 0x1

    goto :goto_0

    .line 784
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->collator:Ljava/text/Collator;

    invoke-virtual {v2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 789
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->currentReaderValues:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 790
    .local v0, "val2":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->bottom:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 791
    if-nez v0, :cond_0

    .line 792
    const/4 v1, 0x0

    .line 798
    :goto_0
    return v1

    .line 794
    :cond_0
    const/4 v1, -0x1

    goto :goto_0

    .line 795
    :cond_1
    if-nez v0, :cond_2

    .line 796
    const/4 v1, 0x1

    goto :goto_0

    .line 798
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->collator:Ljava/text/Collator;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->bottom:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 758
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->compareValues(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compareValues(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "val1"    # Ljava/lang/String;
    .param p2, "val2"    # Ljava/lang/String;

    .prologue
    .line 823
    if-nez p1, :cond_1

    .line 824
    if-nez p2, :cond_0

    .line 825
    const/4 v0, 0x0

    .line 831
    :goto_0
    return v0

    .line 827
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 828
    :cond_1
    if-nez p2, :cond_2

    .line 829
    const/4 v0, 0x1

    goto :goto_0

    .line 831
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->collator:Ljava/text/Collator;

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 803
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->values:[Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->currentReaderValues:[Ljava/lang/String;

    aget-object v1, v1, p2

    aput-object v1, v0, p1

    .line 804
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 813
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->values:[Ljava/lang/String;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->bottom:Ljava/lang/String;

    .line 814
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 808
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->field:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/apache/lucene/search/FieldCache;->getStrings(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->currentReaderValues:[Ljava/lang/String;

    .line 809
    return-void
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 758
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->value(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Ljava/lang/String;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 818
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;->values:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
