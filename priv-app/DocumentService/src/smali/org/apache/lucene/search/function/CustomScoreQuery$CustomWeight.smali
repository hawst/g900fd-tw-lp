.class Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;
.super Lorg/apache/lucene/search/Weight;
.source "CustomScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/function/CustomScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomWeight"
.end annotation


# instance fields
.field qStrict:Z

.field similarity:Lorg/apache/lucene/search/Similarity;

.field subQueryWeight:Lorg/apache/lucene/search/Weight;

.field final synthetic this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

.field valSrcWeights:[Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/function/CustomScoreQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 3
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    iput-object p1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 191
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 192
    # getter for: Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;
    invoke-static {p1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->access$000(Lorg/apache/lucene/search/function/CustomScoreQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    .line 193
    # getter for: Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;
    invoke-static {p1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->access$100(Lorg/apache/lucene/search/function/CustomScoreQuery;)[Lorg/apache/lucene/search/function/ValueSourceQuery;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [Lorg/apache/lucene/search/Weight;

    iput-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    .line 194
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    # getter for: Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;
    invoke-static {p1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->access$100(Lorg/apache/lucene/search/function/CustomScoreQuery;)[Lorg/apache/lucene/search/function/ValueSourceQuery;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 195
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    # getter for: Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;
    invoke-static {p1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->access$100(Lorg/apache/lucene/search/function/CustomScoreQuery;)[Lorg/apache/lucene/search/function/ValueSourceQuery;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/function/ValueSourceQuery;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v2

    aput-object v2, v1, v0

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_0
    # getter for: Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z
    invoke-static {p1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->access$200(Lorg/apache/lucene/search/function/CustomScoreQuery;)Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->qStrict:Z

    .line 198
    return-void
.end method

.method private doExplain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-object v6, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v6, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    .line 267
    .local v4, "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v6

    if-nez v6, :cond_0

    .line 281
    .end local v4    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v4

    .line 271
    .restart local v4    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v6, v6

    new-array v5, v6, [Lorg/apache/lucene/search/Explanation;

    .line 272
    .local v5, "valSrcExpls":[Lorg/apache/lucene/search/Explanation;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v6, v6

    if-ge v1, v6, :cond_1

    .line 273
    iget-object v6, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v6, v6, v1

    invoke-virtual {v6, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v6

    aput-object v6, v5, v1

    .line 272
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 275
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getCustomScoreProvider(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/CustomScoreProvider;

    move-result-object v6

    invoke-virtual {v6, p2, v4, v5}, Lorg/apache/lucene/search/function/CustomScoreProvider;->customExplain(ILorg/apache/lucene/search/Explanation;[Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 276
    .local v0, "customExp":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->getValue()F

    move-result v6

    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v7

    mul-float v3, v6, v7

    .line 277
    .local v3, "sc":F
    new-instance v2, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-virtual {v8}, Lorg/apache/lucene/search/function/CustomScoreQuery;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", product of:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v6, v3, v7}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 279
    .local v2, "res":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 280
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->getValue()F

    move-result v7

    const-string/jumbo v8, "queryBoost"

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v6}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v4, v2

    .line 281
    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->doExplain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 262
    .local v0, "explain":Lorg/apache/lucene/search/Explanation;
    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/lucene/search/Explanation;

    .end local v0    # "explain":Lorg/apache/lucene/search/Explanation;
    const/4 v1, 0x0

    const-string/jumbo v2, "no matching docs"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v0

    return v0
.end method

.method public normalize(F)V
    .locals 3
    .param p1, "norm"    # F

    .prologue
    .line 230
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v1

    mul-float/2addr p1, v1

    .line 231
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    .line 232
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 233
    iget-boolean v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->qStrict:Z

    if-eqz v1, :cond_0

    .line 234
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v1, v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    .line 232
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    goto :goto_1

    .line 239
    :cond_1
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 248
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 249
    .local v5, "subQueryScorer":Lorg/apache/lucene/search/Scorer;
    if-nez v5, :cond_0

    .line 256
    :goto_0
    return-object v7

    .line 252
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v0, v0

    new-array v6, v0, [Lorg/apache/lucene/search/Scorer;

    .line 253
    .local v6, "valSrcScorers":[Lorg/apache/lucene/search/Scorer;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    array-length v0, v6

    if-ge v8, v0, :cond_1

    .line 254
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v0, v0, v8

    invoke-virtual {v0, p1, v2, p3}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    aput-object v0, v6, v8

    .line 253
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 256
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;-><init>(Lorg/apache/lucene/search/function/CustomScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;Lorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/function/CustomScoreQuery$1;)V

    move-object v7, v0

    goto :goto_0
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    return v0
.end method

.method public sumOfSquaredWeights()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    move-result v1

    .line 216
    .local v1, "sum":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 217
    iget-boolean v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->qStrict:Z

    if-eqz v2, :cond_0

    .line 218
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    .line 216
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    move-result v2

    add-float/2addr v1, v2

    goto :goto_1

    .line 223
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v3

    mul-float/2addr v2, v3

    mul-float/2addr v1, v2

    .line 224
    return v1
.end method
