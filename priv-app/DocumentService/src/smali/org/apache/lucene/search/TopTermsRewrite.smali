.class public abstract Lorg/apache/lucene/search/TopTermsRewrite;
.super Lorg/apache/lucene/search/TermCollectingRewrite;
.source "TopTermsRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/Query;",
        ">",
        "Lorg/apache/lucene/search/TermCollectingRewrite",
        "<TQ;>;"
    }
.end annotation


# instance fields
.field private final size:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 42
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite;-><init>()V

    .line 43
    iput p1, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    .line 44
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-ne p0, p1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 94
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 95
    check-cast v0, Lorg/apache/lucene/search/TopTermsRewrite;

    .line 96
    .local v0, "other":Lorg/apache/lucene/search/TopTermsRewrite;
    iget v3, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    iget v4, v0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method protected abstract getMaxSize()I
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 48
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 87
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    iget v0, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "Lorg/apache/lucene/search/MultiTermQuery;",
            ")TQ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite;, "Lorg/apache/lucene/search/TopTermsRewrite<TQ;>;"
    iget v5, p0, Lorg/apache/lucene/search/TopTermsRewrite;->size:I

    invoke-virtual {p0}, Lorg/apache/lucene/search/TopTermsRewrite;->getMaxSize()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 57
    .local v1, "maxSize":I
    new-instance v4, Ljava/util/PriorityQueue;

    invoke-direct {v4}, Ljava/util/PriorityQueue;-><init>()V

    .line 58
    .local v4, "stQueue":Ljava/util/PriorityQueue;, "Ljava/util/PriorityQueue<Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;>;"
    new-instance v5, Lorg/apache/lucene/search/TopTermsRewrite$1;

    invoke-direct {v5, p0, v4, v1}, Lorg/apache/lucene/search/TopTermsRewrite$1;-><init>(Lorg/apache/lucene/search/TopTermsRewrite;Ljava/util/PriorityQueue;I)V

    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/lucene/search/TopTermsRewrite;->collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V

    .line 76
    invoke-virtual {p0}, Lorg/apache/lucene/search/TopTermsRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 77
    .local v2, "q":Lorg/apache/lucene/search/Query;, "TQ;"
    invoke-virtual {v4}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    .line 78
    .local v3, "st":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
    iget-object v5, v3, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v6

    iget v7, v3, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    mul-float/2addr v6, v7

    invoke-virtual {p0, v2, v5, v6}, Lorg/apache/lucene/search/TopTermsRewrite;->addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;F)V

    goto :goto_0

    .line 80
    .end local v3    # "st":Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;
    :cond_0
    invoke-virtual {v4}, Ljava/util/PriorityQueue;->size()I

    move-result v5

    invoke-virtual {p2, v5}, Lorg/apache/lucene/search/MultiTermQuery;->incTotalNumberOfTerms(I)V

    .line 82
    return-object v2
.end method
