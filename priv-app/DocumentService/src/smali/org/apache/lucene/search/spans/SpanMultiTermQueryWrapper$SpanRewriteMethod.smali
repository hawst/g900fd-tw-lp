.class public abstract Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SpanRewriteMethod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "x0"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "x1"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    return-object v0
.end method

.method public abstract rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/spans/SpanQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
