.class Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$3;

.field final synthetic val$inclusiveLowerPoint:S

.field final synthetic val$inclusiveUpperPoint:S

.field final synthetic val$values:[S


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$3;Lorg/apache/lucene/index/IndexReader;[SSS)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 223
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$3;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$values:[S

    iput-short p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$inclusiveLowerPoint:S

    iput-short p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$inclusiveUpperPoint:S

    invoke-direct {p0, p2}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-void
.end method


# virtual methods
.method protected matchDoc(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$values:[S

    aget-short v0, v0, p1

    iget-short v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$inclusiveLowerPoint:S

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$values:[S

    aget-short v0, v0, p1

    iget-short v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;->val$inclusiveUpperPoint:S

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
