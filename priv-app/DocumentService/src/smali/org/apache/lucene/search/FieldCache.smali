.class public interface abstract Lorg/apache/lucene/search/FieldCache;
.super Ljava/lang/Object;
.source "FieldCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldCache$CacheEntry;,
        Lorg/apache/lucene/search/FieldCache$DoubleParser;,
        Lorg/apache/lucene/search/FieldCache$LongParser;,
        Lorg/apache/lucene/search/FieldCache$FloatParser;,
        Lorg/apache/lucene/search/FieldCache$IntParser;,
        Lorg/apache/lucene/search/FieldCache$ShortParser;,
        Lorg/apache/lucene/search/FieldCache$ByteParser;,
        Lorg/apache/lucene/search/FieldCache$Parser;,
        Lorg/apache/lucene/search/FieldCache$StringIndex;,
        Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    }
.end annotation


# static fields
.field public static final DEFAULT:Lorg/apache/lucene/search/FieldCache;

.field public static final DEFAULT_BYTE_PARSER:Lorg/apache/lucene/search/FieldCache$ByteParser;

.field public static final DEFAULT_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

.field public static final DEFAULT_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

.field public static final DEFAULT_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

.field public static final DEFAULT_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

.field public static final DEFAULT_SHORT_PARSER:Lorg/apache/lucene/search/FieldCache$ShortParser;

.field public static final NUMERIC_UTILS_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

.field public static final NUMERIC_UTILS_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

.field public static final NUMERIC_UTILS_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

.field public static final NUMERIC_UTILS_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

.field public static final STRING_INDEX:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCacheImpl;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    .line 151
    new-instance v0, Lorg/apache/lucene/search/FieldCache$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_BYTE_PARSER:Lorg/apache/lucene/search/FieldCache$ByteParser;

    .line 165
    new-instance v0, Lorg/apache/lucene/search/FieldCache$2;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_SHORT_PARSER:Lorg/apache/lucene/search/FieldCache$ShortParser;

    .line 179
    new-instance v0, Lorg/apache/lucene/search/FieldCache$3;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$3;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    .line 193
    new-instance v0, Lorg/apache/lucene/search/FieldCache$4;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$4;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

    .line 207
    new-instance v0, Lorg/apache/lucene/search/FieldCache$5;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$5;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 221
    new-instance v0, Lorg/apache/lucene/search/FieldCache$6;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$6;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .line 238
    new-instance v0, Lorg/apache/lucene/search/FieldCache$7;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$7;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    .line 258
    new-instance v0, Lorg/apache/lucene/search/FieldCache$8;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$8;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

    .line 278
    new-instance v0, Lorg/apache/lucene/search/FieldCache$9;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$9;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 298
    new-instance v0, Lorg/apache/lucene/search/FieldCache$10;

    invoke-direct {v0}, Lorg/apache/lucene/search/FieldCache$10;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    return-void
.end method


# virtual methods
.method public abstract getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;
.end method

.method public abstract getDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;)[D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)[D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;)[F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getInfoStream()Ljava/io/PrintStream;
.end method

.method public abstract getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getStrings(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract purge(Lorg/apache/lucene/index/IndexReader;)V
.end method

.method public abstract purgeAllCaches()V
.end method

.method public abstract setInfoStream(Ljava/io/PrintStream;)V
.end method
