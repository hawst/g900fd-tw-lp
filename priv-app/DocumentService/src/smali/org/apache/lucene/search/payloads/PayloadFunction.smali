.class public abstract Lorg/apache/lucene/search/payloads/PayloadFunction;
.super Ljava/lang/Object;
.source "PayloadFunction.java"

# interfaces
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method


# virtual methods
.method public abstract currentScore(ILjava/lang/String;IIIFF)F
.end method

.method public abstract docScore(ILjava/lang/String;IF)F
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public explain(IIF)Lorg/apache/lucene/search/Explanation;
    .locals 2
    .param p1, "docId"    # I
    .param p2, "numPayloadsSeen"    # I
    .param p3, "payloadScore"    # F

    .prologue
    .line 60
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 61
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    const-string/jumbo v1, "Unimpl Payload Function Explain"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 62
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 63
    return-object v0
.end method

.method public abstract hashCode()I
.end method
