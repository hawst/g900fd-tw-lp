.class final Lorg/apache/lucene/search/FieldCacheImpl$LongCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LongCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 675
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 676
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 19
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "entry"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 681
    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    .line 682
    .local v7, "field":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v10, v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    check-cast v10, Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 683
    .local v10, "parser":Lorg/apache/lucene/search/FieldCache$LongParser;
    if-nez v10, :cond_1

    .line 685
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    sget-object v18, Lorg/apache/lucene/search/FieldCache;->DEFAULT_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, p3

    invoke-virtual {v15, v0, v7, v1, v2}, Lorg/apache/lucene/search/FieldCacheImpl;->getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)[J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 727
    :cond_0
    :goto_0
    return-object v11

    .line 686
    :catch_0
    move-exception v9

    .line 687
    .local v9, "ne":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    sget-object v18, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_LONG_PARSER:Lorg/apache/lucene/search/FieldCache$LongParser;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, p3

    invoke-virtual {v15, v0, v7, v1, v2}, Lorg/apache/lucene/search/FieldCacheImpl;->getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)[J

    move-result-object v11

    goto :goto_0

    .line 690
    .end local v9    # "ne":Ljava/lang/NumberFormatException;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v8

    .line 691
    .local v8, "maxDoc":I
    const/4 v11, 0x0

    .line 692
    .local v11, "retArray":[J
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v13

    .line 693
    .local v13, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v15, Lorg/apache/lucene/index/Term;

    invoke-direct {v15, v7}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v14

    .line 694
    .local v14, "termEnum":Lorg/apache/lucene/index/TermEnum;
    const/4 v5, 0x0

    .line 697
    .local v5, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_1
    :try_start_1
    invoke-virtual {v14}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v12

    .line 698
    .local v12, "term":Lorg/apache/lucene/index/Term;
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;
    :try_end_1
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v15

    if-eq v15, v7, :cond_4

    .line 718
    :cond_2
    :goto_2
    invoke-interface {v13}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 719
    .end local v12    # "term":Lorg/apache/lucene/index/Term;
    :goto_3
    invoke-virtual {v14}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 721
    if-eqz p3, :cond_3

    .line 722
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0, v7, v5}, Lorg/apache/lucene/search/FieldCacheImpl;->setDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V

    .line 724
    :cond_3
    if-nez v11, :cond_0

    .line 725
    new-array v11, v8, [J

    goto :goto_0

    .line 699
    .restart local v12    # "term":Lorg/apache/lucene/index/Term;
    :cond_4
    :try_start_2
    invoke-virtual {v12}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v15}, Lorg/apache/lucene/search/FieldCache$LongParser;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 700
    .local v16, "termval":J
    if-nez v11, :cond_5

    .line 701
    new-array v11, v8, [J

    .line 703
    :cond_5
    invoke-interface {v13, v14}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V
    :try_end_2
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v6, v5

    .line 704
    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .local v6, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_4
    :try_start_3
    invoke-interface {v13}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 705
    invoke-interface {v13}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v4

    .line 706
    .local v4, "docID":I
    aput-wide v16, v11, v4

    .line 707
    if-eqz p3, :cond_9

    .line 708
    if-nez v6, :cond_8

    .line 710
    new-instance v5, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v5, v8}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V
    :try_end_3
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 712
    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_5
    :try_start_4
    invoke-virtual {v5, v4}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V
    :try_end_4
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_6
    move-object v6, v5

    .line 714
    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_4

    .line 715
    .end local v4    # "docID":I
    :cond_6
    :try_start_5
    invoke-virtual {v14}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_5
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v15

    if-nez v15, :cond_7

    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_2

    .line 718
    .end local v12    # "term":Lorg/apache/lucene/index/Term;
    .end local v16    # "termval":J
    :catchall_0
    move-exception v15

    :goto_7
    invoke-interface {v13}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 719
    invoke-virtual {v14}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 718
    throw v15

    .line 716
    :catch_1
    move-exception v15

    .line 718
    :goto_8
    invoke-interface {v13}, Lorg/apache/lucene/index/TermDocs;->close()V

    goto :goto_3

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v12    # "term":Lorg/apache/lucene/index/Term;
    .restart local v16    # "termval":J
    :catchall_1
    move-exception v15

    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_7

    .line 716
    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :catch_2
    move-exception v15

    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_8

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_7
    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_1

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docID":I
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_8
    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_5

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_9
    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_6
.end method
