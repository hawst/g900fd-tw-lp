.class Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;
.super Lorg/apache/lucene/search/Weight;
.source "PhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/PhraseQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhraseWeight"
.end annotation


# instance fields
.field private idf:F

.field private idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

.field private queryNorm:F

.field private queryWeight:F

.field private final similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/PhraseQuery;

.field private value:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/PhraseQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 2
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iput-object p1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 203
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/PhraseQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 205
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/search/Similarity;->idfExplain(Ljava/util/Collection;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    .line 206
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->getIdf()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idf:F

    .line 207
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 24
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    new-instance v17, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct/range {v17 .. v17}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 270
    .local v17, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "weight("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " in "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, "), product of:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 272
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    .local v6, "docFreqs":Ljava/lang/StringBuilder;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    .local v14, "query":Ljava/lang/StringBuilder;
    const/16 v21, 0x22

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->explain()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v11, v0, :cond_1

    .line 277
    if-eqz v11, :cond_0

    .line 278
    const-string/jumbo v21, " "

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/Term;

    .line 283
    .local v19, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 285
    .end local v19    # "term":Lorg/apache/lucene/index/Term;
    :cond_1
    const/16 v21, 0x22

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 287
    new-instance v12, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idf:F

    move/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "idf("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v23, v0

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lorg/apache/lucene/search/PhraseQuery;->access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v12, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 291
    .local v12, "idfExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v15, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v15}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 292
    .local v15, "queryExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "queryWeight("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, "), product of:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 294
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v21

    const-string/jumbo v22, "boost"

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v4, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 295
    .local v4, "boostExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v21

    const/high16 v22, 0x3f800000    # 1.0f

    cmpl-float v21, v21, v22

    if-eqz v21, :cond_2

    .line 296
    invoke-virtual {v15, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 297
    :cond_2
    invoke-virtual {v15, v12}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 299
    new-instance v16, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryNorm:F

    move/from16 v21, v0

    const-string/jumbo v22, "queryNorm"

    move-object/from16 v0, v16

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 300
    .local v16, "queryNormExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 302
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v21

    invoke-virtual {v12}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v22

    mul-float v21, v21, v22

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v22

    mul-float v21, v21, v22

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 306
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 309
    new-instance v7, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v7}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 310
    .local v7, "fieldExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "fieldWeight("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v22, v0

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/search/PhraseQuery;->access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ":"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " in "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, "), product of:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 313
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v18

    .line 314
    .local v18, "scorer":Lorg/apache/lucene/search/Scorer;
    if-nez v18, :cond_3

    .line 315
    new-instance v17, Lorg/apache/lucene/search/Explanation;

    .end local v17    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    const/16 v21, 0x0

    const-string/jumbo v22, "no matching docs"

    move-object/from16 v0, v17

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 349
    :goto_1
    return-object v17

    .line 317
    .restart local v17    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_3
    new-instance v20, Lorg/apache/lucene/search/Explanation;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 318
    .local v20, "tfExplanation":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v5

    .line 320
    .local v5, "d":I
    move/from16 v0, p2

    if-ne v5, v0, :cond_4

    .line 321
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/Scorer;->freq()F

    move-result v13

    .line 326
    .local v13, "phraseFreq":F
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 327
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "tf(phraseFreq="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 329
    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 330
    invoke-virtual {v7, v12}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 332
    new-instance v9, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v9}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 333
    .local v9, "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/search/PhraseQuery;->access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v10

    .line 334
    .local v10, "fieldNorms":[B
    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v21, v0

    aget-byte v22, v10, p2

    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v8

    .line 336
    .local v8, "fieldNorm":F
    :goto_3
    invoke-virtual {v9, v8}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 337
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "fieldNorm(field="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    move-object/from16 v22, v0

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/search/PhraseQuery;->access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ", doc="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 338
    invoke-virtual {v7, v9}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 340
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v21

    invoke-virtual {v12}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v22

    mul-float v21, v21, v22

    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v22

    mul-float v21, v21, v22

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 344
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 347
    invoke-virtual {v15}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v21

    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v22

    mul-float v21, v21, v22

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 348
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    goto/16 :goto_1

    .line 323
    .end local v8    # "fieldNorm":F
    .end local v9    # "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    .end local v10    # "fieldNorms":[B
    .end local v13    # "phraseFreq":F
    :cond_4
    const/4 v13, 0x0

    .restart local v13    # "phraseFreq":F
    goto/16 :goto_2

    .line 334
    .restart local v9    # "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    .restart local v10    # "fieldNorms":[B
    :cond_5
    const/high16 v8, 0x3f800000    # 1.0f

    goto :goto_3
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->value:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "queryNorm"    # F

    .prologue
    .line 226
    iput p1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryNorm:F

    .line 227
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryWeight:F

    mul-float/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryWeight:F

    .line 228
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idf:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->value:F

    .line 229
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 11
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 233
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    move-object v8, v1

    .line 259
    :cond_0
    :goto_0
    return-object v8

    .line 236
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .line 237
    .local v2, "postingsFreqs":[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 238
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/Term;

    .line 239
    .local v9, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p1, v9}, Lorg/apache/lucene/index/IndexReader;->termPositions(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermPositions;

    move-result-object v7

    .line 240
    .local v7, "p":Lorg/apache/lucene/index/TermPositions;
    if-nez v7, :cond_2

    move-object v8, v1

    .line 241
    goto :goto_0

    .line 242
    :cond_2
    new-instance v3, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    invoke-virtual {p1, v9}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v4

    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$100(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v5, 0x1

    new-array v5, v5, [Lorg/apache/lucene/index/Term;

    const/4 v10, 0x0

    aput-object v9, v5, v10

    invoke-direct {v3, v7, v4, v0, v5}, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;-><init>(Lorg/apache/lucene/index/TermPositions;II[Lorg/apache/lucene/index/Term;)V

    aput-object v3, v2, v6

    .line 237
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 246
    .end local v7    # "p":Lorg/apache/lucene/index/TermPositions;
    .end local v9    # "t":Lorg/apache/lucene/index/Term;
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$200(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v0

    if-nez v0, :cond_4

    .line 247
    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;)V

    .line 250
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v0}, Lorg/apache/lucene/search/PhraseQuery;->access$200(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v0

    if-nez v0, :cond_5

    .line 251
    new-instance v8, Lorg/apache/lucene/search/ExactPhraseScorer;

    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static {v3}, Lorg/apache/lucene/search/PhraseQuery;->access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v8, p0, v2, v0, v3}, Lorg/apache/lucene/search/ExactPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;[B)V

    .line 253
    .local v8, "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    iget-boolean v0, v8, Lorg/apache/lucene/search/ExactPhraseScorer;->noDocs:Z

    if-eqz v0, :cond_0

    move-object v8, v1

    .line 254
    goto :goto_0

    .line 259
    .end local v8    # "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    :cond_5
    new-instance v0, Lorg/apache/lucene/search/SloppyPhraseScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->slop:I
    invoke-static {v1}, Lorg/apache/lucene/search/PhraseQuery;->access$200(Lorg/apache/lucene/search/PhraseQuery;)I

    move-result v4

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    # getter for: Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/search/PhraseQuery;->access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;I[B)V

    move-object v8, v0

    goto/16 :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 2

    .prologue
    .line 220
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->idf:F

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryWeight:F

    .line 221
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "weight("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;->this$0:Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
