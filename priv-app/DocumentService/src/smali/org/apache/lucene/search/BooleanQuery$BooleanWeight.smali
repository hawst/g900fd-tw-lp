.class public Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;
.super Lorg/apache/lucene/search/Weight;
.source "BooleanQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "BooleanWeight"
.end annotation


# instance fields
.field private final disableCoord:Z

.field protected maxCoord:I

.field protected similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanQuery;

.field protected weights:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Weight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/Searcher;Z)V
    .locals 4
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .param p3, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 171
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/BooleanQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 172
    iput-boolean p3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    .line 173
    new-instance v2, Ljava/util/ArrayList;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    .line 174
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 175
    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 176
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v3

    invoke-virtual {v3, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    .line 174
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    :cond_1
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 20
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/BooleanQuery;->getMinimumNumberShouldMatch()I

    move-result v9

    .line 218
    .local v9, "minShouldMatch":I
    new-instance v14, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v14}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 219
    .local v14, "sumExpl":Lorg/apache/lucene/search/ComplexExplanation;
    const-string/jumbo v17, "sum of:"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 220
    const/4 v5, 0x0

    .line 221
    .local v5, "coord":I
    const/4 v13, 0x0

    .line 222
    .local v13, "sum":F
    const/4 v8, 0x0

    .line 223
    .local v8, "fail":Z
    const/4 v12, 0x0

    .line 224
    .local v12, "shouldMatchCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    move-object/from16 v17, v0

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 225
    .local v4, "cIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/BooleanClause;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "wIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/Weight;>;"
    :cond_0
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 226
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/search/Weight;

    .line 227
    .local v15, "w":Lorg/apache/lucene/search/Weight;
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/BooleanClause;

    .line 228
    .local v3, "c":Lorg/apache/lucene/search/BooleanClause;
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v15, v0, v1, v2}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v17

    if-nez v17, :cond_1

    .line 229
    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 230
    const/4 v8, 0x1

    .line 231
    new-instance v10, Lorg/apache/lucene/search/Explanation;

    const/16 v17, 0x0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "no match on required clause ("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v10, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 232
    .local v10, "r":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v14, v10}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    goto :goto_0

    .line 236
    .end local v10    # "r":Lorg/apache/lucene/search/Explanation;
    :cond_1
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v15, v0, v1}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v7

    .line 237
    .local v7, "e":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 238
    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v17

    if-nez v17, :cond_2

    .line 239
    invoke-virtual {v14, v7}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 240
    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v17

    add-float v13, v13, v17

    .line 241
    add-int/lit8 v5, v5, 0x1

    .line 249
    :goto_1
    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v17

    sget-object v18, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_0

    .line 250
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 243
    :cond_2
    new-instance v10, Lorg/apache/lucene/search/Explanation;

    const/16 v17, 0x0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "match on prohibited clause ("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v10, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 245
    .restart local v10    # "r":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v10, v7}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 246
    invoke-virtual {v14, v10}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 247
    const/4 v8, 0x1

    goto :goto_1

    .line 251
    .end local v10    # "r":Lorg/apache/lucene/search/Explanation;
    :cond_3
    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 252
    new-instance v10, Lorg/apache/lucene/search/Explanation;

    const/16 v17, 0x0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "no match on required clause ("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/search/Query;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v10, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 253
    .restart local v10    # "r":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v10, v7}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 254
    invoke-virtual {v14, v10}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 255
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 258
    .end local v3    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v7    # "e":Lorg/apache/lucene/search/Explanation;
    .end local v10    # "r":Lorg/apache/lucene/search/Explanation;
    .end local v15    # "w":Lorg/apache/lucene/search/Weight;
    :cond_4
    if-eqz v8, :cond_6

    .line 259
    sget-object v17, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 260
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 261
    const-string/jumbo v17, "Failure to meet condition(s) of required/prohibited clause(s)"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 285
    .end local v14    # "sumExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_5
    :goto_2
    return-object v14

    .line 264
    .restart local v14    # "sumExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_6
    if-ge v12, v9, :cond_7

    .line 265
    sget-object v17, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 266
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 267
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Failure to match minimum number of optional clauses: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    goto :goto_2

    .line 272
    :cond_7
    if-lez v5, :cond_8

    sget-object v17, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_3
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 273
    invoke-virtual {v14, v13}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 275
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    move/from16 v17, v0

    if-eqz v17, :cond_9

    const/high16 v6, 0x3f800000    # 1.0f

    .line 276
    .local v6, "coordFactor":F
    :goto_4
    const/high16 v17, 0x3f800000    # 1.0f

    cmpl-float v17, v6, v17

    if-eqz v17, :cond_5

    .line 279
    new-instance v11, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-virtual {v14}, Lorg/apache/lucene/search/ComplexExplanation;->isMatch()Z

    move-result v17

    mul-float v18, v13, v6

    const-string/jumbo v19, "product of:"

    move/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v11, v0, v1, v2}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 282
    .local v11, "result":Lorg/apache/lucene/search/ComplexExplanation;
    invoke-virtual {v11, v14}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 283
    new-instance v17, Lorg/apache/lucene/search/Explanation;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "coord("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v14, v11

    .line 285
    goto/16 :goto_2

    .line 272
    .end local v6    # "coordFactor":F
    .end local v11    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_8
    sget-object v17, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_3

    .line 275
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, Lorg/apache/lucene/search/Similarity;->coord(II)F

    move-result v6

    goto :goto_4
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v0

    return v0
.end method

.method public normalize(F)V
    .locals 3
    .param p1, "norm"    # F

    .prologue
    .line 206
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v2

    mul-float/2addr p1, v2

    .line 207
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Weight;

    .line 209
    .local v1, "w":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    goto :goto_0

    .line 211
    .end local v1    # "w":Lorg/apache/lucene/search/Weight;
    :cond_0
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 23
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 293
    .local v14, "required":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v8, "prohibited":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v7, "optional":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {v2}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 296
    .local v19, "cIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/BooleanClause;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/search/Weight;

    .line 297
    .local v22, "w":Lorg/apache/lucene/search/Weight;
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/search/BooleanClause;

    .line 298
    .local v18, "c":Lorg/apache/lucene/search/BooleanClause;
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v21

    .line 299
    .local v21, "subScorer":Lorg/apache/lucene/search/Scorer;
    if-nez v21, :cond_1

    .line 300
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 301
    const/4 v2, 0x0

    .line 328
    .end local v18    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v21    # "subScorer":Lorg/apache/lucene/search/Scorer;
    .end local v22    # "w":Lorg/apache/lucene/search/Weight;
    :goto_1
    return-object v2

    .line 303
    .restart local v18    # "c":Lorg/apache/lucene/search/BooleanClause;
    .restart local v21    # "subScorer":Lorg/apache/lucene/search/Scorer;
    .restart local v22    # "w":Lorg/apache/lucene/search/Weight;
    :cond_1
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 304
    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 305
    :cond_2
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 306
    move-object/from16 v0, v21

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 308
    :cond_3
    move-object/from16 v0, v21

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 313
    .end local v18    # "c":Lorg/apache/lucene/search/BooleanClause;
    .end local v21    # "subScorer":Lorg/apache/lucene/search/Scorer;
    .end local v22    # "w":Lorg/apache/lucene/search/Weight;
    :cond_4
    if-nez p2, :cond_5

    if-eqz p3, :cond_5

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_5

    .line 314
    new-instance v2, Lorg/apache/lucene/search/BooleanScorer;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v6, v3, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/search/BooleanScorer;-><init>(Lorg/apache/lucene/search/Weight;ZLorg/apache/lucene/search/Similarity;ILjava/util/List;Ljava/util/List;I)V

    goto :goto_1

    .line 317
    :cond_5
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_6

    .line 319
    const/4 v2, 0x0

    goto :goto_1

    .line 320
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v3, v3, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    if-ge v2, v3, :cond_7

    .line 324
    const/4 v2, 0x0

    goto :goto_1

    .line 328
    :cond_7
    new-instance v9, Lorg/apache/lucene/search/BooleanScorer2;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->disableCoord:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    iget v13, v2, Lorg/apache/lucene/search/BooleanQuery;->minNrShouldMatch:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->maxCoord:I

    move/from16 v17, v0

    move-object/from16 v10, p0

    move-object v15, v8

    move-object/from16 v16, v7

    invoke-direct/range {v9 .. v17}, Lorg/apache/lucene/search/BooleanScorer2;-><init>(Lorg/apache/lucene/search/Weight;ZLorg/apache/lucene/search/Similarity;ILjava/util/List;Ljava/util/List;Ljava/util/List;I)V

    move-object v2, v9

    goto :goto_1
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 3

    .prologue
    .line 333
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {v2}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 334
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isRequired()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 335
    const/4 v2, 0x0

    .line 340
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    const/4 v2, 0x0

    .line 190
    .local v2, "sum":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 192
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/Weight;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    move-result v1

    .line 193
    .local v1, "s":F
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    # getter for: Lorg/apache/lucene/search/BooleanQuery;->clauses:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/apache/lucene/search/BooleanQuery;->access$100(Lorg/apache/lucene/search/BooleanQuery;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/BooleanClause;

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v3

    if-nez v3, :cond_0

    .line 195
    add-float/2addr v2, v1

    .line 190
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "s":F
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;->this$0:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getBoost()F

    move-result v4

    mul-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 200
    return v2
.end method
