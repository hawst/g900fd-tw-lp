.class Lorg/apache/lucene/search/FieldCache$10;
.super Ljava/lang/Object;
.source "FieldCache.java"

# interfaces
.implements Lorg/apache/lucene/search/FieldCache$DoubleParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parseDouble(Ljava/lang/String;)D
    .locals 4
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 300
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v0, v1, -0x20

    .line 301
    .local v0, "shift":I
    if-lez v0, :cond_0

    const/16 v1, 0x3f

    if-gt v0, v1, :cond_0

    .line 302
    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException;

    invoke-direct {v1}, Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException;-><init>()V

    throw v1

    .line 303
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/util/NumericUtils;->prefixCodedToLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/apache/lucene/util/NumericUtils;->sortableLongToDouble(J)D

    move-result-wide v2

    return-wide v2
.end method

.method protected readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_DOUBLE_PARSER:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".NUMERIC_UTILS_DOUBLE_PARSER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
