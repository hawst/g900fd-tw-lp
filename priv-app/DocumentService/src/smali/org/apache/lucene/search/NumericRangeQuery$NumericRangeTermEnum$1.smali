.class Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;
.super Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
.source "NumericRangeQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;-><init>(Lorg/apache/lucene/search/NumericRangeQuery;Lorg/apache/lucene/index/IndexReader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;

.field final synthetic val$this$0:Lorg/apache/lucene/search/NumericRangeQuery;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;Lorg/apache/lucene/search/NumericRangeQuery;)V
    .locals 0

    .prologue
    .line 454
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;, "Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum.1;"
    iput-object p1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;->this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;

    iput-object p2, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;->val$this$0:Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-direct {p0}, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public final addRange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "minPrefixCoded"    # Ljava/lang/String;
    .param p2, "maxPrefixCoded"    # Ljava/lang/String;

    .prologue
    .line 455
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;, "Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum.1;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;->this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;

    # getter for: Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;
    invoke-static {v0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->access$000(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 456
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;->this$1:Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;

    # getter for: Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;
    invoke-static {v0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->access$000(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 457
    return-void
.end method
