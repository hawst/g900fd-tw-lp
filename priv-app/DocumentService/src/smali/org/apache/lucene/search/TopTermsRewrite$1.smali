.class Lorg/apache/lucene/search/TopTermsRewrite$1;
.super Ljava/lang/Object;
.source "TopTermsRewrite.java"

# interfaces
.implements Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/TopTermsRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

.field final synthetic this$0:Lorg/apache/lucene/search/TopTermsRewrite;

.field final synthetic val$maxSize:I

.field final synthetic val$stQueue:Ljava/util/PriorityQueue;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/TopTermsRewrite;Ljava/util/PriorityQueue;I)V
    .locals 2

    .prologue
    .line 73
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite$1;, "Lorg/apache/lucene/search/TopTermsRewrite.1;"
    iput-object p1, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->this$0:Lorg/apache/lucene/search/TopTermsRewrite;

    iput-object p2, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$stQueue:Ljava/util/PriorityQueue;

    iput p3, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$maxSize:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;-><init>(Lorg/apache/lucene/search/TopTermsRewrite$1;)V

    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    return-void
.end method


# virtual methods
.method public collect(Lorg/apache/lucene/index/Term;F)Z
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "boost"    # F

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/TopTermsRewrite$1;, "Lorg/apache/lucene/search/TopTermsRewrite.1;"
    const/4 v2, 0x1

    .line 61
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$maxSize:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iget v0, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    .line 69
    :goto_0
    return v2

    .line 64
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iput-object p1, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->term:Lorg/apache/lucene/index/Term;

    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    iput p2, v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;->boost:F

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$stQueue:Ljava/util/PriorityQueue;

    iget-object v1, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$maxSize:I

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->val$stQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    :goto_1
    iput-object v0, p0, Lorg/apache/lucene/search/TopTermsRewrite$1;->st:Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/TopTermsRewrite$ScoreTerm;-><init>(Lorg/apache/lucene/search/TopTermsRewrite$1;)V

    goto :goto_1
.end method
