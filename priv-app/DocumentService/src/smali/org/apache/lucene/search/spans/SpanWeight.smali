.class public Lorg/apache/lucene/search/spans/SpanWeight;
.super Lorg/apache/lucene/search/Weight;
.source "SpanWeight.java"


# instance fields
.field protected idf:F

.field private idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

.field protected query:Lorg/apache/lucene/search/spans/SpanQuery;

.field protected queryNorm:F

.field protected queryWeight:F

.field protected similarity:Lorg/apache/lucene/search/Similarity;

.field protected terms:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field

.field protected value:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 45
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/spans/SpanQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->terms:Ljava/util/Set;

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->terms:Ljava/util/Set;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    .line 51
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->terms:Ljava/util/Set;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/search/Similarity;->idfExplain(Ljava/util/Collection;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    .line 52
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->getIdf()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idf:F

    .line 53
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 15
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v10, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v10}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 89
    .local v10, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "weight("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "), product of:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v12}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "field":Ljava/lang/String;
    new-instance v7, Lorg/apache/lucene/search/Explanation;

    iget v12, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idf:F

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "idf("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    invoke-virtual {v14}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->explain()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v12, v13}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 96
    .local v7, "idfExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v8, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v8}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 97
    .local v8, "queryExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "queryWeight("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "), product of:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 99
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v12

    const-string/jumbo v13, "boost"

    invoke-direct {v1, v12, v13}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 100
    .local v1, "boostExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v12

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-eqz v12, :cond_0

    .line 101
    invoke-virtual {v8, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 102
    :cond_0
    invoke-virtual {v8, v7}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 104
    new-instance v9, Lorg/apache/lucene/search/Explanation;

    iget v12, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryNorm:F

    const-string/jumbo v13, "queryNorm"

    invoke-direct {v9, v12, v13}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 105
    .local v9, "queryNormExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v8, v9}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 107
    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v12

    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v13

    mul-float/2addr v12, v13

    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v13

    mul-float/2addr v12, v13

    invoke-virtual {v8, v12}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 111
    invoke-virtual {v10, v8}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 114
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 115
    .local v3, "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "fieldWeight("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ":"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v13, v2}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "), product of:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 118
    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v12, v13}, Lorg/apache/lucene/search/spans/SpanWeight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/search/spans/SpanScorer;

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/spans/SpanScorer;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v11

    .line 119
    .local v11, "tfExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v3, v11}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 120
    invoke-virtual {v3, v7}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 122
    new-instance v5, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v5}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 123
    .local v5, "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v6

    .line 124
    .local v6, "fieldNorms":[B
    if-eqz v6, :cond_1

    iget-object v12, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    aget-byte v13, v6, p2

    invoke-virtual {v12, v13}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v4

    .line 126
    .local v4, "fieldNorm":F
    :goto_0
    invoke-virtual {v5, v4}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 127
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "fieldNorm(field="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ", doc="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v3, v5}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 130
    invoke-virtual {v11}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v12

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v3, v12}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 131
    invoke-virtual {v11}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v12

    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v13

    mul-float/2addr v12, v13

    invoke-virtual {v5}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v13

    mul-float/2addr v12, v13

    invoke-virtual {v3, v12}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 135
    invoke-virtual {v10, v3}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 136
    invoke-virtual {v3}, Lorg/apache/lucene/search/ComplexExplanation;->getMatch()Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v12}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 139
    invoke-virtual {v8}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v12

    invoke-virtual {v3}, Lorg/apache/lucene/search/ComplexExplanation;->getValue()F

    move-result v13

    mul-float/2addr v12, v13

    invoke-virtual {v10, v12}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 141
    invoke-virtual {v8}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v12

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-nez v12, :cond_2

    .line 144
    .end local v3    # "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :goto_1
    return-object v3

    .line 124
    .end local v4    # "fieldNorm":F
    .restart local v3    # "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_1
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_0

    .restart local v4    # "fieldNorm":F
    :cond_2
    move-object v3, v10

    .line 144
    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->value:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "queryNorm"    # F

    .prologue
    .line 69
    iput p1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryNorm:F

    .line 70
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryWeight:F

    mul-float/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryWeight:F

    .line 71
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idf:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->value:F

    .line 72
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/spans/SpanScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v0, v1, p0, v2, v3}, Lorg/apache/lucene/search/spans/SpanScorer;-><init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V

    goto :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->idf:F

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryWeight:F

    .line 64
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method
