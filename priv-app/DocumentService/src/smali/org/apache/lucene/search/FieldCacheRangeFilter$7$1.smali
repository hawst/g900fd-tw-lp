.class Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$7;

.field final synthetic val$inclusiveLowerPoint:D

.field final synthetic val$inclusiveUpperPoint:D

.field final synthetic val$values:[D


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$7;Lorg/apache/lucene/index/IndexReader;[DDD)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 431
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$7;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$values:[D

    iput-wide p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$inclusiveLowerPoint:D

    iput-wide p6, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$inclusiveUpperPoint:D

    invoke-direct {p0, p2}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-void
.end method


# virtual methods
.method protected matchDoc(I)Z
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 432
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$values:[D

    aget-wide v0, v0, p1

    iget-wide v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$inclusiveLowerPoint:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$values:[D

    aget-wide v0, v0, p1

    iget-wide v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;->val$inclusiveUpperPoint:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
