.class public abstract Lorg/apache/lucene/search/function/DocValues;
.super Ljava/lang/Object;
.source "DocValues.java"


# instance fields
.field private avgVal:F

.field private computed:Z

.field private maxVal:F

.field private minVal:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput v0, p0, Lorg/apache/lucene/search/function/DocValues;->minVal:F

    .line 115
    iput v0, p0, Lorg/apache/lucene/search/function/DocValues;->maxVal:F

    .line 116
    iput v0, p0, Lorg/apache/lucene/search/function/DocValues;->avgVal:F

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/function/DocValues;->computed:Z

    return-void
.end method

.method private compute()V
    .locals 5

    .prologue
    .line 120
    iget-boolean v4, p0, Lorg/apache/lucene/search/function/DocValues;->computed:Z

    if-eqz v4, :cond_0

    .line 140
    :goto_0
    return-void

    .line 123
    :cond_0
    const/4 v2, 0x0

    .line 124
    .local v2, "sum":F
    const/4 v1, 0x0

    .line 128
    .local v1, "n":I
    :goto_1
    :try_start_0
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 132
    .local v3, "val":F
    add-float/2addr v2, v3

    .line 133
    iget v4, p0, Lorg/apache/lucene/search/function/DocValues;->minVal:F

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v3

    :goto_2
    iput v4, p0, Lorg/apache/lucene/search/function/DocValues;->minVal:F

    .line 134
    iget v4, p0, Lorg/apache/lucene/search/function/DocValues;->maxVal:F

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_2

    .end local v3    # "val":F
    :goto_3
    iput v3, p0, Lorg/apache/lucene/search/function/DocValues;->maxVal:F

    .line 135
    add-int/lit8 v1, v1, 0x1

    .line 136
    goto :goto_1

    .line 129
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    if-nez v1, :cond_3

    const/high16 v4, 0x7fc00000    # NaNf

    :goto_4
    iput v4, p0, Lorg/apache/lucene/search/function/DocValues;->avgVal:F

    .line 139
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/lucene/search/function/DocValues;->computed:Z

    goto :goto_0

    .line 133
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v3    # "val":F
    :cond_1
    iget v4, p0, Lorg/apache/lucene/search/function/DocValues;->minVal:F

    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto :goto_2

    .line 134
    :cond_2
    iget v4, p0, Lorg/apache/lucene/search/function/DocValues;->maxVal:F

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    goto :goto_3

    .line 138
    .end local v3    # "val":F
    .restart local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_3
    int-to-float v4, v1

    div-float v4, v2, v4

    goto :goto_4
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v0

    float-to-double v0, v0

    return-wide v0
.end method

.method public explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 94
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v1

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/DocValues;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v0
.end method

.method public abstract floatVal(I)F
.end method

.method public getAverageValue()F
    .locals 1

    .prologue
    .line 183
    invoke-direct {p0}, Lorg/apache/lucene/search/function/DocValues;->compute()V

    .line 184
    iget v0, p0, Lorg/apache/lucene/search/function/DocValues;->avgVal:F

    return v0
.end method

.method getInnerArray()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "this optional method is for test purposes only"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMaxValue()F
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lorg/apache/lucene/search/function/DocValues;->compute()V

    .line 169
    iget v0, p0, Lorg/apache/lucene/search/function/DocValues;->maxVal:F

    return v0
.end method

.method public getMinValue()F
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Lorg/apache/lucene/search/function/DocValues;->compute()V

    .line 154
    iget v0, p0, Lorg/apache/lucene/search/function/DocValues;->minVal:F

    return v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v0

    float-to-long v0, v0

    return-wide v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract toString(I)Ljava/lang/String;
.end method
