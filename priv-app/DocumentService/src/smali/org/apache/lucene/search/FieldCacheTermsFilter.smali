.class public Lorg/apache/lucene/search/FieldCacheTermsFilter;
.super Lorg/apache/lucene/search/Filter;
.source "FieldCacheTermsFilter.java"


# instance fields
.field private field:Ljava/lang/String;

.field private terms:[Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "terms"    # [Ljava/lang/String;

    .prologue
    .line 100
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 101
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->field:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Ljava/lang/String;

    .line 103
    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p0}, Lorg/apache/lucene/search/FieldCacheTermsFilter;->getFieldCache()Lorg/apache/lucene/search/FieldCache;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->field:Ljava/lang/String;

    invoke-interface {v4, p1, v5}, Lorg/apache/lucene/search/FieldCache;->getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;

    move-result-object v1

    .line 112
    .local v1, "fcsi":Lorg/apache/lucene/search/FieldCache$StringIndex;
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    iget-object v4, v1, Lorg/apache/lucene/search/FieldCache$StringIndex;->lookup:[Ljava/lang/String;

    array-length v4, v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 113
    .local v0, "bits":Lorg/apache/lucene/util/FixedBitSet;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 114
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCacheTermsFilter;->terms:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Lorg/apache/lucene/search/FieldCache$StringIndex;->binarySearchLookup(Ljava/lang/String;)I

    move-result v3

    .line 115
    .local v3, "termNumber":I
    if-lez v3, :cond_0

    .line 116
    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 113
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    .end local v3    # "termNumber":I
    :cond_1
    new-instance v4, Lorg/apache/lucene/search/FieldCacheTermsFilter$1;

    invoke-direct {v4, p0, p1, v0, v1}, Lorg/apache/lucene/search/FieldCacheTermsFilter$1;-><init>(Lorg/apache/lucene/search/FieldCacheTermsFilter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/FixedBitSet;Lorg/apache/lucene/search/FieldCache$StringIndex;)V

    return-object v4
.end method

.method public getFieldCache()Lorg/apache/lucene/search/FieldCache;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    return-object v0
.end method
