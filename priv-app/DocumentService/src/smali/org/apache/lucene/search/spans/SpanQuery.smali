.class public abstract Lorg/apache/lucene/search/spans/SpanQuery;
.super Lorg/apache/lucene/search/Query;
.source "SpanQuery.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lorg/apache/lucene/search/spans/SpanWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/spans/SpanWeight;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public abstract getField()Ljava/lang/String;
.end method

.method public abstract getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
