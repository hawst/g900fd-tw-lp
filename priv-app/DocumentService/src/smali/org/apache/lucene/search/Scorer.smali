.class public abstract Lorg/apache/lucene/search/Scorer;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "Scorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/Scorer$1;,
        Lorg/apache/lucene/search/Scorer$ScorerVisitor;
    }
.end annotation


# instance fields
.field private final similarity:Lorg/apache/lucene/search/Similarity;

.field protected final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/Similarity;)V
    .locals 1
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 61
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V
    .locals 0
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 71
    iput-object p1, p0, Lorg/apache/lucene/search/Scorer;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 72
    iput-object p2, p0, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    .line 73
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/search/Weight;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 52
    return-void
.end method


# virtual methods
.method public freq()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not implement freq()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/Similarity;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/search/Scorer;->similarity:Lorg/apache/lucene/search/Similarity;

    return-object v0
.end method

.method public abstract score()F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 89
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 90
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 116
    move v0, p3

    .line 117
    .local v0, "doc":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 118
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    goto :goto_0

    .line 121
    :cond_0
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public visitScorers(Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Scorer$ScorerVisitor",
            "<",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "visitor":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;>;"
    const/4 v0, 0x0

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    .line 187
    return-void
.end method

.method protected visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/lucene/search/Query;
    .param p2, "relationship"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/BooleanClause$Occur;",
            "Lorg/apache/lucene/search/Scorer$ScorerVisitor",
            "<",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p3, "visitor":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    if-nez v1, :cond_0

    .line 204
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 206
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Weight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 207
    .local v0, "q":Lorg/apache/lucene/search/Query;
    sget-object v1, Lorg/apache/lucene/search/Scorer$1;->$SwitchMap$org$apache$lucene$search$BooleanClause$Occur:[I

    invoke-virtual {p2}, Lorg/apache/lucene/search/BooleanClause$Occur;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 218
    :goto_0
    return-void

    .line 209
    :pswitch_0
    invoke-virtual {p3, p1, v0, p0}, Lorg/apache/lucene/search/Scorer$ScorerVisitor;->visitRequired(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0

    .line 212
    :pswitch_1
    invoke-virtual {p3, p1, v0, p0}, Lorg/apache/lucene/search/Scorer$ScorerVisitor;->visitProhibited(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0

    .line 215
    :pswitch_2
    invoke-virtual {p3, p1, v0, p0}, Lorg/apache/lucene/search/Scorer$ScorerVisitor;->visitOptional(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
