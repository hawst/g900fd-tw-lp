.class public abstract Lorg/apache/lucene/search/Query;
.super Ljava/lang/Object;
.source "Query.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# instance fields
.field private boost:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/search/Query;->boost:F

    return-void
.end method

.method public static varargs mergeBooleanQueries([Lorg/apache/lucene/search/BooleanQuery;)Lorg/apache/lucene/search/Query;
    .locals 11
    .param p0, "queries"    # [Lorg/apache/lucene/search/BooleanQuery;

    .prologue
    const/4 v5, 0x0

    .line 180
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 181
    .local v0, "allClauses":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/search/BooleanClause;>;"
    move-object v1, p0

    .local v1, "arr$":[Lorg/apache/lucene/search/BooleanQuery;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v6    # "i$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v2, v1, v7

    .line 182
    .local v2, "booleanQuery":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v2}, Lorg/apache/lucene/search/BooleanQuery;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .end local v7    # "i$":I
    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/BooleanClause;

    .line 183
    .local v3, "clause":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 181
    .end local v3    # "clause":Lorg/apache/lucene/search/BooleanClause;
    :cond_0
    add-int/lit8 v6, v7, 0x1

    .local v6, "i$":I
    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_0

    .line 187
    .end local v2    # "booleanQuery":Lorg/apache/lucene/search/BooleanQuery;
    :cond_1
    array-length v10, p0

    if-nez v10, :cond_2

    .line 189
    .local v5, "coordDisabled":Z
    :goto_2
    new-instance v9, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v9, v5}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 190
    .local v9, "result":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .end local v7    # "i$":I
    .local v6, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/BooleanClause;

    .line 191
    .local v4, "clause2":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v9, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/BooleanClause;)V

    goto :goto_3

    .line 187
    .end local v4    # "clause2":Lorg/apache/lucene/search/BooleanClause;
    .end local v5    # "coordDisabled":Z
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v9    # "result":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v7    # "i$":I
    :cond_2
    aget-object v10, p0, v5

    invoke-virtual {v10}, Lorg/apache/lucene/search/BooleanQuery;->isCoordDisabled()Z

    move-result v5

    goto :goto_2

    .line 193
    .end local v7    # "i$":I
    .restart local v5    # "coordDisabled":Z
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v9    # "result":Lorg/apache/lucene/search/BooleanQuery;
    :cond_3
    return-object v9
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 212
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Clone not supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public combine([Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;
    .locals 12
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;

    .prologue
    const/4 v9, 0x1

    .line 128
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 129
    .local v8, "uniques":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/search/Query;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v10, p1

    if-ge v2, v10, :cond_4

    .line 130
    aget-object v5, p1, v2

    .line 131
    .local v5, "query":Lorg/apache/lucene/search/Query;
    const/4 v1, 0x0

    .line 133
    .local v1, "clauses":[Lorg/apache/lucene/search/BooleanClause;
    instance-of v7, v5, Lorg/apache/lucene/search/BooleanQuery;

    .line 134
    .local v7, "splittable":Z
    if-eqz v7, :cond_1

    move-object v0, v5

    .line 135
    check-cast v0, Lorg/apache/lucene/search/BooleanQuery;

    .line 136
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->isCoordDisabled()Z

    move-result v7

    .line 137
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v1

    .line 138
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-eqz v7, :cond_1

    array-length v10, v1

    if-ge v4, v10, :cond_1

    .line 139
    aget-object v10, v1, v4

    invoke-virtual {v10}, Lorg/apache/lucene/search/BooleanClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v10

    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v10, v11, :cond_0

    move v7, v9

    .line 138
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 139
    :cond_0
    const/4 v7, 0x0

    goto :goto_2

    .line 142
    .end local v0    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v4    # "j":I
    :cond_1
    if-eqz v7, :cond_2

    .line 143
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_3
    array-length v10, v1

    if-ge v4, v10, :cond_3

    .line 144
    aget-object v10, v1, v4

    invoke-virtual {v10}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 143
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 147
    .end local v4    # "j":I
    :cond_2
    invoke-virtual {v8, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 151
    .end local v1    # "clauses":[Lorg/apache/lucene/search/BooleanClause;
    .end local v5    # "query":Lorg/apache/lucene/search/Query;
    .end local v7    # "splittable":Z
    :cond_4
    invoke-virtual {v8}, Ljava/util/HashSet;->size()I

    move-result v10

    if-ne v10, v9, :cond_5

    .line 152
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/Query;

    .line 157
    :goto_4
    return-object v9

    .line 154
    :cond_5
    new-instance v6, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v6, v9}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 155
    .local v6, "result":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/Query;

    .line 156
    .restart local v5    # "query":Lorg/apache/lucene/search/Query;
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v6, v5, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_5

    .end local v5    # "query":Lorg/apache/lucene/search/Query;
    :cond_6
    move-object v9, v6

    .line 157
    goto :goto_4
.end method

.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 3
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Query "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not implement createWeight"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 228
    if-ne p0, p1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v1

    .line 230
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 231
    goto :goto_0

    .line 232
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 233
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 234
    check-cast v0, Lorg/apache/lucene/search/Query;

    .line 235
    .local v0, "other":Lorg/apache/lucene/search/Query;
    iget v3, p0, Lorg/apache/lucene/search/Query;->boost:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lorg/apache/lucene/search/Query;->boost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 236
    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getBoost()F
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/lucene/search/Query;->boost:F

    return v0
.end method

.method public getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p1}, Lorg/apache/lucene/search/Searcher;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 220
    const/16 v0, 0x1f

    .line 221
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 222
    .local v1, "result":I
    iget v2, p0, Lorg/apache/lucene/search/Query;->boost:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 223
    return v1
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    return-object p0
.end method

.method public setBoost(F)V
    .locals 0
    .param p1, "b"    # F

    .prologue
    .line 56
    iput p1, p0, Lorg/apache/lucene/search/Query;->boost:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract toString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public final weight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 103
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    return-object v0
.end method
