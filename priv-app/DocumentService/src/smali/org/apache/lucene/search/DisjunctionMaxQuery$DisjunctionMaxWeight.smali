.class public Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;
.super Lorg/apache/lucene/search/Weight;
.source "DisjunctionMaxQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/DisjunctionMaxQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DisjunctionMaxWeight"
.end annotation


# instance fields
.field protected similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

.field protected weights:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Weight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/DisjunctionMaxQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 4
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iput-object p1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    .line 105
    invoke-virtual {p2}, Lorg/apache/lucene/search/Searcher;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 106
    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$000(Lorg/apache/lucene/search/DisjunctionMaxQuery;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    .line 107
    .local v0, "disjunctQuery":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    .end local v0    # "disjunctQuery":Lorg/apache/lucene/search/Query;
    :cond_0
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->disjuncts:Ljava/util/ArrayList;
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$000(Lorg/apache/lucene/search/DisjunctionMaxQuery;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/Weight;

    invoke-virtual {v6, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v3

    .line 176
    :goto_0
    return-object v3

    .line 163
    :cond_0
    new-instance v3, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 164
    .local v3, "result":Lorg/apache/lucene/search/ComplexExplanation;
    const/4 v2, 0x0

    .local v2, "max":F
    const/4 v4, 0x0

    .line 165
    .local v4, "sum":F
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$100(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_2

    const-string/jumbo v6, "max of:"

    :goto_1
    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 166
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/Weight;

    .line 167
    .local v5, "wt":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 168
    .local v0, "e":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 169
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 170
    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 171
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    add-float/2addr v4, v6

    .line 172
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_2

    .line 165
    .end local v0    # "e":Lorg/apache/lucene/search/Explanation;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "wt":Lorg/apache/lucene/search/Weight;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "max plus "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v7}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$100(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " times others of:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 175
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    sub-float v6, v4, v2

    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v7}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$100(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v7

    mul-float/2addr v6, v7

    add-float/2addr v6, v2

    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    goto :goto_0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v0

    return v0
.end method

.method public normalize(F)V
    .locals 3
    .param p1, "norm"    # F

    .prologue
    .line 136
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v2

    mul-float/2addr p1, v2

    .line 137
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Weight;

    .line 138
    .local v1, "wt":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    goto :goto_0

    .line 140
    .end local v1    # "wt":Lorg/apache/lucene/search/Weight;
    :cond_0
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 10
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v4, v1, [Lorg/apache/lucene/search/Scorer;

    .line 147
    .local v4, "scorers":[Lorg/apache/lucene/search/Scorer;
    const/4 v5, 0x0

    .line 148
    .local v5, "idx":I
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/Weight;

    .line 149
    .local v9, "w":Lorg/apache/lucene/search/Weight;
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v9, p1, v1, v2}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v8

    .line 150
    .local v8, "subScorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 151
    add-int/lit8 v7, v5, 0x1

    .end local v5    # "idx":I
    .local v7, "idx":I
    aput-object v8, v4, v5

    move v5, v7

    .end local v7    # "idx":I
    .restart local v5    # "idx":I
    goto :goto_0

    .line 154
    .end local v8    # "subScorer":Lorg/apache/lucene/search/Scorer;
    .end local v9    # "w":Lorg/apache/lucene/search/Weight;
    :cond_1
    if-nez v5, :cond_2

    const/4 v0, 0x0

    .line 156
    :goto_1
    return-object v0

    .line 155
    :cond_2
    new-instance v0, Lorg/apache/lucene/search/DisjunctionMaxScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$100(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/DisjunctionMaxScorer;-><init>(Lorg/apache/lucene/search/Weight;FLorg/apache/lucene/search/Similarity;[Lorg/apache/lucene/search/Scorer;I)V

    .line 156
    .local v0, "result":Lorg/apache/lucene/search/DisjunctionMaxScorer;
    goto :goto_1
.end method

.method public sumOfSquaredWeights()F
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v3, 0x0

    .local v3, "max":F
    const/4 v5, 0x0

    .line 123
    .local v5, "sum":F
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->weights:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Weight;

    .line 124
    .local v1, "currentWeight":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    move-result v4

    .line 125
    .local v4, "sub":F
    add-float/2addr v5, v4

    .line 126
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 128
    goto :goto_0

    .line 129
    .end local v1    # "currentWeight":Lorg/apache/lucene/search/Weight;
    .end local v4    # "sub":F
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    invoke-virtual {v6}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->getBoost()F

    move-result v0

    .line 130
    .local v0, "boost":F
    sub-float v6, v5, v3

    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v7}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$100(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v7

    mul-float/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/search/DisjunctionMaxQuery$DisjunctionMaxWeight;->this$0:Lorg/apache/lucene/search/DisjunctionMaxQuery;

    # getter for: Lorg/apache/lucene/search/DisjunctionMaxQuery;->tieBreakerMultiplier:F
    invoke-static {v7}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->access$100(Lorg/apache/lucene/search/DisjunctionMaxQuery;)F

    move-result v7

    mul-float/2addr v6, v7

    add-float/2addr v6, v3

    mul-float/2addr v6, v0

    mul-float/2addr v6, v0

    return v6
.end method
