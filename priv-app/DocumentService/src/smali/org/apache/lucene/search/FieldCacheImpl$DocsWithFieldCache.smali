.class final Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DocsWithFieldCache"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 539
    const-class v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 541
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 542
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "entryKey"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 547
    move-object v0, p2

    .line 548
    .local v0, "entry":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    iget-object v1, v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    .line 549
    .local v1, "field":Ljava/lang/String;
    const/4 v3, 0x0

    .line 550
    .local v3, "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v6

    .line 551
    .local v6, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v8, Lorg/apache/lucene/index/Term;

    invoke-direct {v8, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v8}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v7

    .local v7, "termEnum":Lorg/apache/lucene/index/TermEnum;
    move-object v4, v3

    .line 554
    .end local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .local v4, "res":Lorg/apache/lucene/util/FixedBitSet;
    :goto_0
    :try_start_0
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v5

    .line 555
    .local v5, "term":Lorg/apache/lucene/index/Term;
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v8

    if-eq v8, v1, :cond_2

    move-object v3, v4

    .line 564
    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :cond_0
    :goto_1
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 565
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 567
    if-nez v3, :cond_4

    .line 568
    new-instance v3, Lorg/apache/lucene/util/Bits$MatchNoBits;

    .end local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v8

    invoke-direct {v3, v8}, Lorg/apache/lucene/util/Bits$MatchNoBits;-><init>(I)V

    .line 576
    :cond_1
    :goto_2
    return-object v3

    .line 556
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :cond_2
    if-nez v4, :cond_6

    .line 557
    :try_start_1
    new-instance v3, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v8

    invoke-direct {v3, v8}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 558
    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :goto_3
    :try_start_2
    invoke-interface {v6, v7}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V

    .line 559
    :goto_4
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 560
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v8

    invoke-virtual {v3, v8}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 564
    :catchall_0
    move-exception v8

    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    :goto_5
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 565
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 564
    throw v8

    .line 562
    .restart local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_3
    :try_start_3
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v8

    if-eqz v8, :cond_0

    move-object v4, v3

    .end local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_0

    .line 569
    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :cond_4
    invoke-virtual {v3}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v2

    .line 570
    .local v2, "numSet":I
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v8

    if-lt v2, v8, :cond_1

    .line 573
    sget-boolean v8, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;->$assertionsDisabled:Z

    if-nez v8, :cond_5

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v8

    if-eq v2, v8, :cond_5

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 574
    :cond_5
    new-instance v3, Lorg/apache/lucene/util/Bits$MatchAllBits;

    .end local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v8

    invoke-direct {v3, v8}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    goto :goto_2

    .line 564
    .end local v2    # "numSet":I
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_5

    .end local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_6
    move-object v3, v4

    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_3

    .end local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :cond_7
    move-object v3, v4

    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "res":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_1
.end method
