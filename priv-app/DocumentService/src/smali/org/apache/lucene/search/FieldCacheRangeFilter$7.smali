.class Lorg/apache/lucene/search/FieldCacheRangeFilter$7;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newDoubleRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Ljava/lang/Double;Ljava/lang/Double;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;Ljava/lang/Double;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/Double;
    .param p4, "x3"    # Ljava/lang/Double;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 402
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 406
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 407
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    .line 408
    .local v8, "f":D
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeUpper:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmpl-double v0, v8, v0

    if-lez v0, :cond_0

    invoke-static {v8, v9}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 429
    .end local v8    # "f":D
    :goto_0
    return-object v0

    .line 410
    .restart local v8    # "f":D
    :cond_0
    invoke-static {v8, v9}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v10

    .line 411
    .local v10, "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v10    # "i":J
    :goto_1
    invoke-static {v10, v11}, Lorg/apache/lucene/util/NumericUtils;->sortableLongToDouble(J)D

    move-result-wide v4

    .line 415
    .end local v8    # "f":D
    .local v4, "inclusiveLowerPoint":D
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 416
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    .line 417
    .restart local v8    # "f":D
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeUpper:Z

    if-nez v0, :cond_3

    const-wide/16 v0, 0x0

    cmpg-double v0, v8, v0

    if-gez v0, :cond_3

    invoke-static {v8, v9}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 418
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 411
    .end local v4    # "inclusiveLowerPoint":D
    .restart local v10    # "i":J
    :cond_1
    const-wide/16 v0, 0x1

    add-long/2addr v10, v0

    goto :goto_1

    .line 413
    .end local v8    # "f":D
    .end local v10    # "i":J
    :cond_2
    const-wide/high16 v4, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .restart local v4    # "inclusiveLowerPoint":D
    goto :goto_2

    .line 419
    .restart local v8    # "f":D
    :cond_3
    invoke-static {v8, v9}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v10

    .line 420
    .restart local v10    # "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v10    # "i":J
    :goto_3
    invoke-static {v10, v11}, Lorg/apache/lucene/util/NumericUtils;->sortableLongToDouble(J)D

    move-result-wide v6

    .line 425
    .end local v8    # "f":D
    .local v6, "inclusiveUpperPoint":D
    :goto_4
    cmpl-double v0, v4, v6

    if-lez v0, :cond_6

    .line 426
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 420
    .end local v6    # "inclusiveUpperPoint":D
    .restart local v8    # "f":D
    .restart local v10    # "i":J
    :cond_4
    const-wide/16 v0, 0x1

    sub-long/2addr v10, v0

    goto :goto_3

    .line 422
    .end local v8    # "f":D
    .end local v10    # "i":J
    :cond_5
    const-wide/high16 v6, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .restart local v6    # "inclusiveUpperPoint":D
    goto :goto_4

    .line 428
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$DoubleParser;

    invoke-interface {v1, p1, v2, v0}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;)[D

    move-result-object v3

    .line 429
    .local v3, "values":[D
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter$7$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$7;Lorg/apache/lucene/index/IndexReader;[DDD)V

    goto :goto_0
.end method
