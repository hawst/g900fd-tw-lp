.class Lorg/apache/lucene/search/IndexSearcher$1;
.super Ljava/lang/Object;
.source "IndexSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/IndexSearcher;->docFreq(Lorg/apache/lucene/index/Term;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/IndexSearcher;

.field final synthetic val$searchable:Lorg/apache/lucene/search/IndexSearcher;

.field final synthetic val$term:Lorg/apache/lucene/index/Term;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/index/Term;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher$1;->this$0:Lorg/apache/lucene/search/IndexSearcher;

    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher$1;->val$searchable:Lorg/apache/lucene/search/IndexSearcher;

    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher$1;->val$term:Lorg/apache/lucene/index/Term;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Integer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$1;->val$searchable:Lorg/apache/lucene/search/IndexSearcher;

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher$1;->val$term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/IndexSearcher;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 242
    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher$1;->call()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
