.class public abstract Lorg/apache/lucene/search/FilteredTermEnum;
.super Lorg/apache/lucene/index/TermEnum;
.source "FilteredTermEnum.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected actualEnum:Lorg/apache/lucene/index/TermEnum;

.field protected currentTerm:Lorg/apache/lucene/index/Term;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/search/FilteredTermEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FilteredTermEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/TermEnum;-><init>()V

    .line 30
    iput-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 33
    iput-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    .line 35
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 101
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 102
    iput-object v1, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    .line 103
    return-void
.end method

.method public abstract difference()F
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 67
    :goto_0
    return v0

    .line 66
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/search/FilteredTermEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->docFreq()I

    move-result v0

    goto :goto_0
.end method

.method protected abstract endEnum()Z
.end method

.method public next()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    if-nez v2, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    .line 74
    :cond_1
    iput-object v3, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 75
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_3

    .line 76
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredTermEnum;->endEnum()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 79
    .local v0, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FilteredTermEnum;->termCompare(Lorg/apache/lucene/index/Term;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    iput-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 81
    const/4 v1, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "term":Lorg/apache/lucene/index/Term;
    :cond_3
    iput-object v3, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    goto :goto_0
.end method

.method protected setEnum(Lorg/apache/lucene/index/TermEnum;)V
    .locals 2
    .param p1, "actualEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    .line 53
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 54
    .local v0, "term":Lorg/apache/lucene/index/Term;
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FilteredTermEnum;->termCompare(Lorg/apache/lucene/index/Term;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    iput-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredTermEnum;->next()Z

    goto :goto_0
.end method

.method public term()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method protected abstract termCompare(Lorg/apache/lucene/index/Term;)Z
.end method
