.class public Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;
.super Lorg/apache/lucene/search/Weight;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ConstantWeight"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final innerWeight:Lorg/apache/lucene/search/Weight;

.field private queryNorm:F

.field private queryWeight:F

.field private final similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/ConstantScoreQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 1
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 104
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/ConstantScoreQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 105
    iget-object v0, p1, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    .line 106
    return-void

    .line 105
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 160
    invoke-virtual {p0, p1, v1, v3}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    .line 161
    .local v0, "cs":Lorg/apache/lucene/search/Scorer;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v4

    if-ne v4, p2, :cond_0

    .line 163
    .local v1, "exists":Z
    :goto_0
    new-instance v2, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 164
    .local v2, "result":Lorg/apache/lucene/search/ComplexExplanation;
    if-eqz v1, :cond_1

    .line 165
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/ConstantScoreQuery;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", product of:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 166
    iget v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 167
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 168
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/ConstantScoreQuery;->getBoost()F

    move-result v4

    const-string/jumbo v5, "boost"

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 169
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    iget v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryNorm:F

    const-string/jumbo v5, "queryNorm"

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 175
    :goto_1
    return-object v2

    .end local v1    # "exists":Z
    .end local v2    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_0
    move v1, v3

    .line 161
    goto :goto_0

    .line 171
    .restart local v1    # "exists":Z
    .restart local v2    # "result":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/ConstantScoreQuery;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " doesn\'t match id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 172
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 173
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "norm"    # F

    .prologue
    .line 128
    iput p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryNorm:F

    .line 129
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    .line 132
    :cond_0
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 137
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    if-eqz v3, :cond_3

    .line 138
    sget-boolean v3, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 139
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 140
    .local v0, "dis":Lorg/apache/lucene/search/DocIdSet;
    if-nez v0, :cond_2

    .line 150
    .end local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    :goto_0
    return-object v2

    .line 142
    .restart local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 148
    .end local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    .local v1, "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_1
    if-eqz v1, :cond_1

    .line 150
    new-instance v2, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    invoke-direct {v2, v3, v4, v1, p0}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Weight;)V

    goto :goto_0

    .line 144
    .end local v1    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    :cond_3
    sget-boolean v3, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-nez v3, :cond_5

    :cond_4
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 145
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v3, p1, p2, p3}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .restart local v1    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    goto :goto_1
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->scoresDocsOutOfOrder()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->innerWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    .line 122
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/ConstantScoreQuery;->getBoost()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    .line 123
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method
