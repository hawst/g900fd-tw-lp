.class Lorg/apache/lucene/search/FilteredQuery$2;
.super Lorg/apache/lucene/search/Scorer;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FilteredQuery;->getFilteredScorer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private filterDoc:I

.field private scorerDoc:I

.field final synthetic val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

.field final synthetic val$scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "x0"    # Lorg/apache/lucene/search/Similarity;
    .param p2, "x1"    # Lorg/apache/lucene/search/Weight;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 210
    iput-object p3, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    iput-object p4, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 151
    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    return-void
.end method

.method private advanceToNextCommonDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    if-ge v0, v1, :cond_0

    .line 181
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    goto :goto_0

    .line 182
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    if-ne v0, v1, :cond_1

    .line 183
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    return v0

    .line 185
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    iget v1, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    if-le p1, v0, :cond_0

    .line 199
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    .line 201
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$2;->advanceToNextCommonDoc()I

    move-result v0

    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->scorerDoc:I

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->filterDoc:I

    .line 193
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredQuery$2;->advanceToNextCommonDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    return v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 3
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .line 157
    .local v0, "filterDoc":I
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    .line 160
    .local v1, "scorerDoc":I
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 162
    :goto_0
    if-ne v1, v0, :cond_1

    .line 164
    const v2, 0x7fffffff

    if-ne v1, v2, :cond_0

    .line 176
    return-void

    .line 167
    :cond_0
    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 168
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .line 169
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    goto :goto_0

    .line 170
    :cond_1
    if-le v1, v0, :cond_2

    .line 171
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$filterIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    goto :goto_0

    .line 173
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$2;->val$scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    goto :goto_0
.end method
