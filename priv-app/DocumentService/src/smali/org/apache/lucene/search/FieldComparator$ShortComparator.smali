.class public final Lorg/apache/lucene/search/FieldComparator$ShortComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShortComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Short;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:S

.field private currentReaderValues:[S

.field private final parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

.field private final values:[S


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Short;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Short;

    .prologue
    .line 705
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 706
    new-array v0, p1, [S

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->values:[S

    .line 707
    check-cast p3, Lorg/apache/lucene/search/FieldCache$ShortParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    .line 708
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 2
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 712
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->values:[S

    aget-short v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->values:[S

    aget-short v1, v1, p2

    sub-int/2addr v0, v1

    return v0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 717
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->currentReaderValues:[S

    aget-short v0, v1, p1

    .line 720
    .local v0, "v2":S
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 721
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v0

    .line 723
    :cond_0
    iget-short v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->bottom:S

    sub-int/2addr v1, v0

    return v1
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 728
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->currentReaderValues:[S

    aget-short v0, v1, p2

    .line 731
    .local v0, "v2":S
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 732
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v0

    .line 734
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->values:[S

    aput-short v0, v1, p1

    .line 735
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 747
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->values:[S

    aget-short v0, v0, p1

    iput-short v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->bottom:S

    .line 748
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 741
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldCache;->getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)[S

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->currentReaderValues:[S

    .line 742
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 743
    return-void

    .line 741
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 698
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->value(I)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Ljava/lang/Short;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 752
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ShortComparator;->values:[S

    aget-short v0, v0, p1

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method
