.class Lorg/apache/lucene/search/spans/SpanNotQuery$1;
.super Lorg/apache/lucene/search/spans/Spans;
.source "SpanNotQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/spans/SpanNotQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private excludeSpans:Lorg/apache/lucene/search/spans/Spans;

.field private includeSpans:Lorg/apache/lucene/search/spans/Spans;

.field private moreExclude:Z

.field private moreInclude:Z

.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

.field final synthetic val$reader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/spans/SpanNotQuery;Lorg/apache/lucene/index/IndexReader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

    iput-object p2, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->val$reader:Lorg/apache/lucene/index/IndexReader;

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

    # getter for: Lorg/apache/lucene/search/spans/SpanNotQuery;->include:Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->access$000(Lorg/apache/lucene/search/spans/SpanNotQuery;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->val$reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

    # getter for: Lorg/apache/lucene/search/spans/SpanNotQuery;->exclude:Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/SpanNotQuery;->access$100(Lorg/apache/lucene/search/spans/SpanNotQuery;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->val$reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    .line 83
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    return-void
.end method


# virtual methods
.method public doc()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 151
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_0
    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    .line 90
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_3

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    .line 97
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    goto :goto_1

    .line 101
    :cond_2
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    if-gt v0, v1, :cond_4

    .line 108
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    return v0

    .line 106
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    .line 116
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreInclude:Z

    if-nez v0, :cond_1

    .line 117
    const/4 v0, 0x0

    .line 134
    :goto_0
    return v0

    .line 119
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    .line 125
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 126
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    goto :goto_1

    .line 129
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->moreExclude:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->excludeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    if-gt v0, v1, :cond_5

    .line 132
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->next()Z

    move-result v0

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->includeSpans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "spans("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNotQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanNotQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanNotQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
