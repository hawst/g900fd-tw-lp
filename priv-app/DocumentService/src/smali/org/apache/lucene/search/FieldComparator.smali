.class public abstract Lorg/apache/lucene/search/FieldComparator;
.super Ljava/lang/Object;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldComparator$StringValComparator;,
        Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;,
        Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;,
        Lorg/apache/lucene/search/FieldComparator$ShortComparator;,
        Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;,
        Lorg/apache/lucene/search/FieldComparator$LongComparator;,
        Lorg/apache/lucene/search/FieldComparator$IntComparator;,
        Lorg/apache/lucene/search/FieldComparator$FloatComparator;,
        Lorg/apache/lucene/search/FieldComparator$DoubleComparator;,
        Lorg/apache/lucene/search/FieldComparator$DocComparator;,
        Lorg/apache/lucene/search/FieldComparator$ByteComparator;,
        Lorg/apache/lucene/search/FieldComparator$NumericComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 989
    return-void
.end method

.method protected static final binarySearch([Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "a"    # [Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1067
    const/4 v0, 0x0

    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, p1, v0, v1}, Lorg/apache/lucene/search/FieldComparator;->binarySearch([Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method protected static final binarySearch([Ljava/lang/String;Ljava/lang/String;II)I
    .locals 4
    .param p0, "a"    # [Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "low"    # I
    .param p3, "high"    # I

    .prologue
    .line 1072
    :goto_0
    if-gt p2, p3, :cond_2

    .line 1073
    add-int v3, p2, p3

    ushr-int/lit8 v1, v3, 0x1

    .line 1074
    .local v1, "mid":I
    aget-object v2, p0, v1

    .line 1076
    .local v2, "midVal":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1077
    invoke-virtual {v2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 1082
    .local v0, "cmp":I
    :goto_1
    if-gez v0, :cond_1

    .line 1083
    add-int/lit8 p2, v1, 0x1

    goto :goto_0

    .line 1079
    .end local v0    # "cmp":I
    :cond_0
    const/4 v0, -0x1

    .restart local v0    # "cmp":I
    goto :goto_1

    .line 1084
    :cond_1
    if-lez v0, :cond_3

    .line 1085
    add-int/lit8 p3, v1, -0x1

    goto :goto_0

    .line 1089
    .end local v0    # "cmp":I
    .end local v1    # "mid":I
    .end local v2    # "midVal":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, p2, 0x1

    neg-int v1, v3

    :cond_3
    return v1
.end method


# virtual methods
.method public abstract compare(II)I
.end method

.method public abstract compareBottom(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<TT;>;"
    .local p1, "first":Ljava/lang/Object;, "TT;"
    .local p2, "second":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_1

    .line 174
    if-nez p2, :cond_0

    .line 175
    const/4 v0, 0x0

    .line 182
    .end local p1    # "first":Ljava/lang/Object;, "TT;"
    :goto_0
    return v0

    .line 177
    .restart local p1    # "first":Ljava/lang/Object;, "TT;"
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 179
    :cond_1
    if-nez p2, :cond_2

    .line 180
    const/4 v0, 0x1

    goto :goto_0

    .line 182
    :cond_2
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "first":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public abstract copy(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setBottom(I)V
.end method

.method public abstract setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 156
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<TT;>;"
    return-void
.end method

.method public abstract value(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method
