.class public Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "MultiTermQueryWrapperFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/MultiTermQuery;",
        ">",
        "Lorg/apache/lucene/search/Filter;"
    }
.end annotation


# instance fields
.field protected final query:Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TQ;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/MultiTermQuery;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    .local p1, "query":Lorg/apache/lucene/search/MultiTermQuery;, "TQ;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    .line 51
    return-void
.end method


# virtual methods
.method public clearTotalNumberOfTerms()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 99
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->clearTotalNumberOfTerms()V

    .line 100
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    const/4 v0, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 67
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 63
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_0

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast p1, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 10
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v9, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v9, p1}, Lorg/apache/lucene/search/MultiTermQuery;->getEnum(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/FilteredTermEnum;

    move-result-object v3

    .line 111
    .local v3, "enumerator":Lorg/apache/lucene/index/TermEnum;
    :try_start_0
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v9

    if-nez v9, :cond_0

    .line 112
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :goto_0
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 143
    return-object v0

    .line 114
    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v9

    invoke-direct {v0, v9}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 115
    .local v0, "bitSet":Lorg/apache/lucene/util/FixedBitSet;
    const/16 v9, 0x20

    new-array v2, v9, [I

    .line 116
    .local v2, "docs":[I
    const/16 v9, 0x20

    new-array v4, v9, [I

    .line 117
    .local v4, "freqs":[I
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 119
    .local v8, "termDocs":Lorg/apache/lucene/index/TermDocs;
    const/4 v7, 0x0

    .line 121
    .local v7, "termCount":I
    :cond_1
    :try_start_2
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v6

    .line 122
    .local v6, "term":Lorg/apache/lucene/index/Term;
    if-nez v6, :cond_2

    .line 138
    :goto_1
    iget-object v9, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v9, v7}, Lorg/apache/lucene/search/MultiTermQuery;->incTotalNumberOfTerms(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 141
    :try_start_3
    invoke-interface {v8}, Lorg/apache/lucene/index/TermDocs;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 145
    .end local v0    # "bitSet":Lorg/apache/lucene/util/FixedBitSet;
    .end local v2    # "docs":[I
    .end local v4    # "freqs":[I
    .end local v6    # "term":Lorg/apache/lucene/index/Term;
    .end local v7    # "termCount":I
    .end local v8    # "termDocs":Lorg/apache/lucene/index/TermDocs;
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermEnum;->close()V

    throw v9

    .line 124
    .restart local v0    # "bitSet":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v2    # "docs":[I
    .restart local v4    # "freqs":[I
    .restart local v6    # "term":Lorg/apache/lucene/index/Term;
    .restart local v7    # "termCount":I
    .restart local v8    # "termDocs":Lorg/apache/lucene/index/TermDocs;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    .line 125
    :try_start_4
    invoke-interface {v8, v6}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 127
    :cond_3
    invoke-interface {v8, v2, v4}, Lorg/apache/lucene/index/TermDocs;->read([I[I)I

    move-result v1

    .line 128
    .local v1, "count":I
    if-eqz v1, :cond_4

    .line 129
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v1, :cond_3

    .line 130
    aget v9, v2, v5

    invoke-virtual {v0, v9}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 129
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 136
    .end local v5    # "i":I
    :cond_4
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v9

    if-nez v9, :cond_1

    goto :goto_1

    .line 141
    .end local v1    # "count":I
    .end local v6    # "term":Lorg/apache/lucene/index/Term;
    :catchall_1
    move-exception v9

    :try_start_5
    invoke-interface {v8}, Lorg/apache/lucene/index/TermDocs;->close()V

    throw v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public getTotalNumberOfTerms()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->getTotalNumberOfTerms()I

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    .local p0, "this":Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;, "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter<TQ;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
