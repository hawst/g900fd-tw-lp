.class final Lorg/apache/lucene/search/TermScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "TermScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final SCORE_CACHE_SIZE:I = 0x20


# instance fields
.field private doc:I

.field private final docs:[I

.field private freq:I

.field private final freqs:[I

.field private final norms:[B

.field private pointer:I

.field private pointerMax:I

.field private final scoreCache:[F

.field private final termDocs:Lorg/apache/lucene/index/TermDocs;

.field private weightValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/search/TermScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TermScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/TermDocs;Lorg/apache/lucene/search/Similarity;[B)V
    .locals 5
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "td"    # Lorg/apache/lucene/index/TermDocs;
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "norms"    # [B

    .prologue
    const/16 v4, 0x20

    .line 55
    invoke-direct {p0, p3, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 30
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    .line 33
    new-array v1, v4, [I

    iput-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    .line 34
    new-array v1, v4, [I

    iput-object v1, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    .line 39
    new-array v1, v4, [F

    iput-object v1, p0, Lorg/apache/lucene/search/TermScorer;->scoreCache:[F

    .line 57
    iput-object p2, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    .line 58
    iput-object p4, p0, Lorg/apache/lucene/search/TermScorer;->norms:[B

    .line 59
    invoke-virtual {p1}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->weightValue:F

    .line 61
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 62
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->scoreCache:[F

    invoke-virtual {p0}, Lorg/apache/lucene/search/TermScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Similarity;->tf(I)F

    move-result v2

    iget v3, p0, Lorg/apache/lucene/search/TermScorer;->weightValue:F

    mul-float/2addr v2, v3

    aput v2, v1, v0

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 4
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    :goto_0
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    if-ge v1, v2, :cond_1

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v1, v1, v2

    if-lt v1, p1, :cond_0

    .line 150
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    .line 165
    :goto_1
    return v1

    .line 148
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    goto :goto_0

    .line 156
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1, p1}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v0

    .line 157
    .local v0, "result":Z
    if-eqz v0, :cond_2

    .line 158
    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    .line 159
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    .line 160
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    iget-object v3, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    aput v3, v1, v2

    .line 161
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    iget-object v3, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermDocs;->freq()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    aput v3, v1, v2

    .line 165
    :goto_2
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    goto :goto_1

    .line 163
    :cond_2
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    goto :goto_2
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    return v0
.end method

.method public freq()F
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    int-to-float v0, v0

    return v0
.end method

.method public nextDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    .line 111
    iget v0, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    if-lt v0, v1, :cond_0

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget-object v2, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/index/TermDocs;->read([I[I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    .line 113
    iget v0, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    if-eqz v0, :cond_1

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    .line 120
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    .line 122
    iget v0, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    :goto_0
    return v0

    .line 116
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 117
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 4

    .prologue
    .line 127
    sget-boolean v1, Lorg/apache/lucene/search/TermScorer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 128
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    const/16 v2, 0x20

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->scoreCache:[F

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    aget v0, v1, v2

    .line 133
    .local v0, "raw":F
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->norms:[B

    if-nez v1, :cond_2

    .end local v0    # "raw":F
    :goto_1
    return v0

    .line 128
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->tf(I)F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->weightValue:F

    mul-float v0, v1, v2

    goto :goto_0

    .line 133
    .restart local v0    # "raw":F
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/TermScorer;->norms:[B

    iget v3, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    aget-byte v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_1
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "c"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    const v0, 0x7fffffff

    invoke-virtual {p0}, Lorg/apache/lucene/search/TermScorer;->nextDoc()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/search/TermScorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    .line 68
    return-void
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 4
    .param p1, "c"    # Lorg/apache/lucene/search/Collector;
    .param p2, "end"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 74
    :goto_0
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    if-ge v1, p2, :cond_2

    .line 75
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 77
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    if-lt v1, v2, :cond_0

    .line 78
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    iget-object v2, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget-object v3, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/index/TermDocs;->read([I[I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    .line 79
    iget v1, p0, Lorg/apache/lucene/search/TermScorer;->pointerMax:I

    if-eqz v1, :cond_1

    .line 80
    iput v0, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    .line 87
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->docs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    .line 88
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->freqs:[I

    iget v2, p0, Lorg/apache/lucene/search/TermScorer;->pointer:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->freq:I

    goto :goto_0

    .line 82
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 83
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/search/TermScorer;->doc:I

    .line 90
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "scorer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/TermScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
