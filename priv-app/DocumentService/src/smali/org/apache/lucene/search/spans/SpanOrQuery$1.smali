.class Lorg/apache/lucene/search/spans/SpanOrQuery$1;
.super Lorg/apache/lucene/search/spans/Spans;
.source "SpanOrQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/spans/SpanOrQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

.field final synthetic val$reader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    iput-object p2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$reader:Lorg/apache/lucene/index/IndexReader;

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    return-void
.end method

.method private initSpanQueue(I)Z
    .locals 6
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 173
    new-instance v2, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    # getter for: Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/lucene/search/spans/SpanOrQuery;->access$000(Lorg/apache/lucene/search/spans/SpanOrQuery;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;-><init>(Lorg/apache/lucene/search/spans/SpanOrQuery;I)V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    .line 174
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    # getter for: Lorg/apache/lucene/search/spans/SpanOrQuery;->clauses:Ljava/util/List;
    invoke-static {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery;->access$000(Lorg/apache/lucene/search/spans/SpanOrQuery;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 175
    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 176
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->val$reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    .line 177
    .local v1, "spans":Lorg/apache/lucene/search/spans/Spans;
    if-ne p1, v5, :cond_1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    if-eq p1, v5, :cond_0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 182
    .end local v1    # "spans":Lorg/apache/lucene/search/spans/Spans;
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private top()Lorg/apache/lucene/search/spans/Spans;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method


# virtual methods
.method public doc()I
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    const/4 v0, 0x0

    .line 238
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    .line 239
    .local v1, "theTop":Lorg/apache/lucene/search/spans/Spans;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 242
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_0
    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 2

    .prologue
    .line 247
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    .line 248
    .local v0, "top":Lorg/apache/lucene/search/spans/Spans;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 187
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    if-nez v2, :cond_1

    .line 188
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->initSpanQueue(I)Z

    move-result v0

    .line 201
    :cond_0
    :goto_0
    return v0

    .line 191
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 192
    goto :goto_0

    .line 195
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 196
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0

    .line 200
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->pop()Ljava/lang/Object;

    .line 201
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    if-nez v1, :cond_0

    .line 209
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->initSpanQueue(I)Z

    move-result v1

    .line 225
    :goto_0
    return v1

    .line 212
    :cond_0
    const/4 v0, 0x0

    .line 213
    .local v0, "skipCalled":Z
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ge v1, p1, :cond_2

    .line 214
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->updateTop()Ljava/lang/Object;

    .line 219
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 217
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->pop()Ljava/lang/Object;

    goto :goto_2

    .line 222
    :cond_2
    if-eqz v0, :cond_4

    .line 223
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 225
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->next()Z

    move-result v1

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->top()Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "spans("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->this$0:Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    if-nez v0, :cond_0

    const-string/jumbo v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->queue:Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanOrQuery$SpanQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->doc()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->start()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanOrQuery$1;->end()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "END"

    goto :goto_0
.end method
