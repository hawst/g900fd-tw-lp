.class Lorg/apache/lucene/search/FieldCacheRangeFilter$3;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newShortRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Ljava/lang/Short;Ljava/lang/Short;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Short;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Short;Ljava/lang/Short;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/Short;
    .param p4, "x3"    # Ljava/lang/Short;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 201
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v6

    .line 202
    .local v6, "i":S
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeLower:Z

    if-nez v0, :cond_0

    const/16 v0, 0x7fff

    if-ne v6, v0, :cond_0

    .line 203
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 221
    .end local v6    # "i":S
    :goto_0
    return-object v0

    .line 204
    .restart local v6    # "i":S
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v6    # "i":S
    :goto_1
    int-to-short v4, v6

    .line 208
    .local v4, "inclusiveLowerPoint":S
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 209
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v6

    .line 210
    .restart local v6    # "i":S
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeUpper:Z

    if-nez v0, :cond_3

    const/16 v0, -0x8000

    if-ne v6, v0, :cond_3

    .line 211
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 204
    .end local v4    # "inclusiveLowerPoint":S
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 206
    .end local v6    # "i":S
    :cond_2
    const/16 v4, -0x8000

    .restart local v4    # "inclusiveLowerPoint":S
    goto :goto_2

    .line 212
    .restart local v6    # "i":S
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v6    # "i":S
    :goto_3
    int-to-short v5, v6

    .line 217
    .local v5, "inclusiveUpperPoint":S
    :goto_4
    if-le v4, v5, :cond_6

    .line 218
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 212
    .end local v5    # "inclusiveUpperPoint":S
    .restart local v6    # "i":S
    :cond_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .line 214
    .end local v6    # "i":S
    :cond_5
    const/16 v5, 0x7fff

    .restart local v5    # "inclusiveUpperPoint":S
    goto :goto_4

    .line 220
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$ShortParser;

    invoke-interface {v1, p1, v2, v0}, Lorg/apache/lucene/search/FieldCache;->getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;)[S

    move-result-object v3

    .line 221
    .local v3, "values":[S
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter$3$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$3;Lorg/apache/lucene/index/IndexReader;[SSS)V

    goto :goto_0
.end method
