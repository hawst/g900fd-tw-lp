.class Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;
.super Lorg/apache/lucene/search/Collector;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

.field final synthetic val$collector:Lorg/apache/lucene/search/Collector;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;Lorg/apache/lucene/search/Collector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iput-object p2, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    return v0
.end method

.method public collect(I)V
    .locals 1
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 221
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 226
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 5
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->val$collector:Lorg/apache/lucene/search/Collector;

    new-instance v1, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v2, v2, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;->this$1:Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;

    iget-object v4, v4, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {v1, v2, v3, p1, v4}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Weight;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 216
    return-void
.end method
