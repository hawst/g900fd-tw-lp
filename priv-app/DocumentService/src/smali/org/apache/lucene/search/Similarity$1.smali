.class Lorg/apache/lucene/search/Similarity$1;
.super Lorg/apache/lucene/search/Explanation$IDFExplanation;
.source "Similarity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/Similarity;->idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;I)Lorg/apache/lucene/search/Explanation$IDFExplanation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/Similarity;

.field final synthetic val$df:I

.field final synthetic val$idf:F

.field final synthetic val$max:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Similarity;IIF)V
    .locals 0

    .prologue
    .line 812
    iput-object p1, p0, Lorg/apache/lucene/search/Similarity$1;->this$0:Lorg/apache/lucene/search/Similarity;

    iput p2, p0, Lorg/apache/lucene/search/Similarity$1;->val$df:I

    iput p3, p0, Lorg/apache/lucene/search/Similarity$1;->val$max:I

    iput p4, p0, Lorg/apache/lucene/search/Similarity$1;->val$idf:F

    invoke-direct {p0}, Lorg/apache/lucene/search/Explanation$IDFExplanation;-><init>()V

    return-void
.end method


# virtual methods
.method public explain()Ljava/lang/String;
    .locals 2

    .prologue
    .line 808
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "idf(docFreq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/search/Similarity$1;->val$df:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", maxDocs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/search/Similarity$1;->val$max:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIdf()F
    .locals 1

    .prologue
    .line 813
    iget v0, p0, Lorg/apache/lucene/search/Similarity$1;->val$idf:F

    return v0
.end method
