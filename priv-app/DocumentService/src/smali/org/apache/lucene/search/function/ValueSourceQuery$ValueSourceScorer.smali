.class Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ValueSourceQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/function/ValueSourceQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValueSourceScorer"
.end annotation


# instance fields
.field private doc:I

.field private final qWeight:F

.field private final termDocs:Lorg/apache/lucene/index/TermDocs;

.field final synthetic this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

.field private final vals:Lorg/apache/lucene/search/function/DocValues;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/search/function/ValueSourceQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;)V
    .locals 1
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p3, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "w"    # Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    iput-object p1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    .line 135
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 131
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->doc:I

    .line 136
    invoke-virtual {p4}, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->getValue()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->qWeight:F

    .line 138
    iget-object v0, p1, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    invoke-virtual {v0, p3}, Lorg/apache/lucene/search/function/ValueSource;->getValues(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->vals:Lorg/apache/lucene/search/function/DocValues;

    .line 139
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    .line 140
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/function/ValueSourceQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;Lorg/apache/lucene/search/function/ValueSourceQuery$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/function/ValueSourceQuery;
    .param p2, "x1"    # Lorg/apache/lucene/search/Similarity;
    .param p3, "x2"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "x3"    # Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;
    .param p5, "x4"    # Lorg/apache/lucene/search/function/ValueSourceQuery$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;-><init>(Lorg/apache/lucene/search/function/ValueSourceQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;)V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->doc:I

    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->doc:I

    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->qWeight:F

    iget-object v1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->vals:Lorg/apache/lucene/search/function/DocValues;

    iget-object v2, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v2}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method
