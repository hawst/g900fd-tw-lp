.class Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;
.super Lorg/apache/lucene/search/TopScoreDocCollector;
.source "TopScoreDocCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopScoreDocCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OutOfOrderTopScoreDocCollector"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    const-class v0, Lorg/apache/lucene/search/TopScoreDocCollector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(I)V
    .locals 1
    .param p1, "numHits"    # I

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/TopScoreDocCollector;-><init>(ILorg/apache/lucene/search/TopScoreDocCollector$1;)V

    .line 136
    return-void
.end method

.method synthetic constructor <init>(ILorg/apache/lucene/search/TopScoreDocCollector$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lorg/apache/lucene/search/TopScoreDocCollector$1;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;-><init>(I)V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 2
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 143
    .local v0, "score":F
    sget-boolean v1, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 145
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->totalHits:I

    .line 146
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->docBase:I

    add-int/2addr p1, v1

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    if-gt p1, v1, :cond_1

    .line 155
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iput p1, v1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 156
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iput v0, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 157
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    iput-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$OutOfOrderTopScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    goto :goto_0
.end method
