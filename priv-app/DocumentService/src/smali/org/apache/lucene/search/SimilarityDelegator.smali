.class public Lorg/apache/lucene/search/SimilarityDelegator;
.super Lorg/apache/lucene/search/Similarity;
.source "SimilarityDelegator.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private delegee:Lorg/apache/lucene/search/Similarity;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Similarity;)V
    .locals 0
    .param p1, "delegee"    # Lorg/apache/lucene/search/Similarity;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/search/Similarity;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    .line 38
    return-void
.end method


# virtual methods
.method public computeNorm(Ljava/lang/String;Lorg/apache/lucene/index/FieldInvertState;)F
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Similarity;->computeNorm(Ljava/lang/String;Lorg/apache/lucene/index/FieldInvertState;)F

    move-result v0

    return v0
.end method

.method public coord(II)F
    .locals 1
    .param p1, "overlap"    # I
    .param p2, "maxOverlap"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Similarity;->coord(II)F

    move-result v0

    return v0
.end method

.method public idf(II)F
    .locals 1
    .param p1, "docFreq"    # I
    .param p2, "numDocs"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Similarity;->idf(II)F

    move-result v0

    return v0
.end method

.method public queryNorm(F)F
    .locals 1
    .param p1, "sumOfSquaredWeights"    # F

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Similarity;->queryNorm(F)F

    move-result v0

    return v0
.end method

.method public scorePayload(ILjava/lang/String;II[BII)F
    .locals 8
    .param p1, "docId"    # I
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "payload"    # [B
    .param p6, "offset"    # I
    .param p7, "length"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/Similarity;->scorePayload(ILjava/lang/String;II[BII)F

    move-result v0

    return v0
.end method

.method public sloppyFreq(I)F
    .locals 1
    .param p1, "distance"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Similarity;->sloppyFreq(I)F

    move-result v0

    return v0
.end method

.method public tf(F)F
    .locals 1
    .param p1, "freq"    # F

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/search/SimilarityDelegator;->delegee:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v0

    return v0
.end method
