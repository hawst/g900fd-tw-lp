.class final Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;
.super Ljava/lang/Object;
.source "ParallelMultiSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ParallelMultiSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DocumentFrequencyCallable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<[I>;"
    }
.end annotation


# instance fields
.field private final searchable:Lorg/apache/lucene/search/Searchable;

.field private final terms:[Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Searchable;[Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "searchable"    # Lorg/apache/lucene/search/Searchable;
    .param p2, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    iput-object p1, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;->searchable:Lorg/apache/lucene/search/Searchable;

    .line 238
    iput-object p2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;->terms:[Lorg/apache/lucene/index/Term;

    .line 239
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p0}, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;->call()[I

    move-result-object v0

    return-object v0
.end method

.method public call()[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;->searchable:Lorg/apache/lucene/search/Searchable;

    iget-object v1, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$DocumentFrequencyCallable;->terms:[Lorg/apache/lucene/index/Term;

    invoke-interface {v0, v1}, Lorg/apache/lucene/search/Searchable;->docFreqs([Lorg/apache/lucene/index/Term;)[I

    move-result-object v0

    return-object v0
.end method
