.class public Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;
.super Ljava/lang/Object;
.source "SpanFilterResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/SpanFilterResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PositionInfo"
.end annotation


# instance fields
.field private doc:I

.field private positions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/SpanFilterResult$StartEnd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;->doc:I

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;->positions:Ljava/util/List;

    .line 65
    return-void
.end method


# virtual methods
.method public addPosition(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;->positions:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/search/SpanFilterResult$StartEnd;

    invoke-direct {v1, p1, p2}, Lorg/apache/lucene/search/SpanFilterResult$StartEnd;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public getDoc()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;->doc:I

    return v0
.end method

.method public getPositions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/SpanFilterResult$StartEnd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;->positions:Ljava/util/List;

    return-object v0
.end method
