.class final Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OutOfOrderMultiComparatorScoringMaxScoreCollector"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 620
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 621
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 625
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 626
    .local v2, "score":F
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->maxScore:F

    cmpl-float v4, v2, v4

    if-lez v4, :cond_0

    .line 627
    iput v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->maxScore:F

    .line 629
    :cond_0
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->totalHits:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->totalHits:I

    .line 630
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_7

    .line 632
    const/4 v1, 0x0

    .line 633
    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->reverseMul:[I

    aget v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v5, v5, v1

    invoke-virtual {v5, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v5

    mul-int v0, v4, v5

    .line 634
    .local v0, "c":I
    if-gez v0, :cond_2

    .line 674
    .end local v0    # "c":I
    :cond_1
    :goto_1
    return-void

    .line 637
    .restart local v0    # "c":I
    :cond_2
    if-lez v0, :cond_4

    .line 651
    :cond_3
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 652
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 651
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 640
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_5

    .line 642
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->docBase:I

    add-int/2addr v4, p1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v4, v5, :cond_3

    goto :goto_1

    .line 632
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 655
    :cond_6
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->updateBottom(IF)V

    .line 657
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 658
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 657
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 662
    .end local v0    # "c":I
    .end local v1    # "i":I
    :cond_7
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->totalHits:I

    add-int/lit8 v3, v4, -0x1

    .line 664
    .local v3, "slot":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_8

    .line 665
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 664
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 667
    :cond_8
    invoke-virtual {p0, v3, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->add(IIF)V

    .line 668
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_1

    .line 669
    const/4 v1, 0x0

    :goto_5
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 670
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 669
    add-int/lit8 v1, v1, 0x1

    goto :goto_5
.end method
