.class Lorg/apache/lucene/search/FieldCacheRangeFilter$4;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newIntRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/Integer;
    .param p4, "x3"    # Ljava/lang/Integer;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 252
    .local v6, "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->includeLower:Z

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    if-ne v6, v0, :cond_0

    .line 253
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 271
    .end local v6    # "i":I
    :goto_0
    return-object v0

    .line 254
    .restart local v6    # "i":I
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->includeLower:Z

    if-eqz v0, :cond_1

    move v4, v6

    .line 258
    .end local v6    # "i":I
    .local v4, "inclusiveLowerPoint":I
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 259
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 260
    .restart local v6    # "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->includeUpper:Z

    if-nez v0, :cond_3

    const/high16 v0, -0x80000000

    if-ne v6, v0, :cond_3

    .line 261
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 254
    .end local v4    # "inclusiveLowerPoint":I
    :cond_1
    add-int/lit8 v4, v6, 0x1

    goto :goto_1

    .line 256
    .end local v6    # "i":I
    :cond_2
    const/high16 v4, -0x80000000

    .restart local v4    # "inclusiveLowerPoint":I
    goto :goto_1

    .line 262
    .restart local v6    # "i":I
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->includeUpper:Z

    if-eqz v0, :cond_4

    move v5, v6

    .line 267
    .end local v6    # "i":I
    .local v5, "inclusiveUpperPoint":I
    :goto_2
    if-le v4, v5, :cond_6

    .line 268
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 262
    .end local v5    # "inclusiveUpperPoint":I
    .restart local v6    # "i":I
    :cond_4
    add-int/lit8 v5, v6, -0x1

    goto :goto_2

    .line 264
    .end local v6    # "i":I
    :cond_5
    const v5, 0x7fffffff

    .restart local v5    # "inclusiveUpperPoint":I
    goto :goto_2

    .line 270
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$IntParser;

    invoke-interface {v1, p1, v2, v0}, Lorg/apache/lucene/search/FieldCache;->getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;)[I

    move-result-object v3

    .line 271
    .local v3, "values":[I
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$4$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter$4$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$4;Lorg/apache/lucene/index/IndexReader;[III)V

    goto :goto_0
.end method
