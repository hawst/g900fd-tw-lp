.class Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
.super Lorg/apache/lucene/search/TermCollectingRewrite;
.source "ConstantScoreAutoRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TermCollectingRewrite",
        "<",
        "Lorg/apache/lucene/search/BooleanQuery;",
        ">;"
    }
.end annotation


# static fields
.field public static DEFAULT_DOC_COUNT_PERCENT:D

.field public static DEFAULT_TERM_COUNT_CUTOFF:I


# instance fields
.field private docCountPercent:D

.field private termCountCutoff:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const/16 v0, 0x15e

    sput v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_TERM_COUNT_CUTOFF:I

    .line 35
    const-wide v0, 0x3fb999999999999aL    # 0.1

    sput-wide v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_DOC_COUNT_PERCENT:D

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite;-><init>()V

    .line 37
    sget v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_TERM_COUNT_CUTOFF:I

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    .line 38
    sget-wide v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->DEFAULT_DOC_COUNT_PERCENT:D

    iput-wide v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    .line 109
    return-void
.end method


# virtual methods
.method protected addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;F)V
    .locals 2
    .param p1, "topLevel"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "boost"    # F

    .prologue
    .line 73
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v0, p2}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 74
    return-void
.end method

.method protected bridge synthetic addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;F)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/Query;
    .param p2, "x1"    # Lorg/apache/lucene/index/Term;
    .param p3, "x2"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lorg/apache/lucene/search/BooleanQuery;

    .end local p1    # "x0":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;F)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    if-ne p0, p1, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v1

    .line 148
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 149
    goto :goto_0

    .line 150
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 151
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 153
    check-cast v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;

    .line 154
    .local v0, "other":Lorg/apache/lucene/search/ConstantScoreAutoRewrite;
    iget v3, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    iget v4, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 155
    goto :goto_0

    .line 158
    :cond_4
    iget-wide v4, v0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 159
    goto :goto_0
.end method

.method public getDocCountPercent()D
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    return-wide v0
.end method

.method public getTermCountCutoff()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    return v0
.end method

.method protected getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 140
    const/16 v0, 0x4ff

    .line 141
    .local v0, "prime":I
    iget v1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    mul-int/lit16 v1, v1, 0x4ff

    int-to-long v2, v1

    iget-wide v4, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-int v1, v2

    return v1
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-wide v8, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v7

    int-to-double v10, v7

    mul-double/2addr v8, v10

    double-to-int v2, v8

    .line 84
    .local v2, "docCountCutoff":I
    invoke-static {}, Lorg/apache/lucene/search/BooleanQuery;->getMaxClauseCount()I

    move-result v7

    iget v8, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 86
    .local v6, "termCountLimit":I
    new-instance v1, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;

    invoke-direct {v1, p1, v2, v6}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;-><init>(Lorg/apache/lucene/index/IndexReader;II)V

    .line 87
    .local v1, "col":Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;
    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V

    .line 89
    iget-boolean v7, v1, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->hasCutOff:Z

    if-eqz v7, :cond_0

    .line 90
    sget-object v7, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v7, p1, p2}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 105
    :goto_0
    return-object v4

    .line 93
    :cond_0
    iget-object v7, v1, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 94
    invoke-virtual {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v4

    .line 104
    .local v4, "result":Lorg/apache/lucene/search/Query;
    :goto_1
    iget-object v7, v1, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {p2, v7}, Lorg/apache/lucene/search/MultiTermQuery;->incTotalNumberOfTerms(I)V

    goto :goto_0

    .line 96
    .end local v4    # "result":Lorg/apache/lucene/search/Query;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    .line 97
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    iget-object v7, v1, Lorg/apache/lucene/search/ConstantScoreAutoRewrite$CutOffTermCollector;->pendingTerms:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    .line 98
    .local v5, "term":Lorg/apache/lucene/index/Term;
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v5, v7}, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->addClause(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/index/Term;F)V

    goto :goto_2

    .line 101
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_2
    new-instance v4, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {v4, v0}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 102
    .restart local v4    # "result":Lorg/apache/lucene/search/Query;
    invoke-virtual {p2}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v7

    invoke-virtual {v4, v7}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    goto :goto_1
.end method

.method public setDocCountPercent(D)V
    .locals 1
    .param p1, "percent"    # D

    .prologue
    .line 58
    iput-wide p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->docCountPercent:D

    .line 59
    return-void
.end method

.method public setTermCountCutoff(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 44
    iput p1, p0, Lorg/apache/lucene/search/ConstantScoreAutoRewrite;->termCountCutoff:I

    .line 45
    return-void
.end method
