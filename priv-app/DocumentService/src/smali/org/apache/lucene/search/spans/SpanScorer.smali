.class public Lorg/apache/lucene/search/spans/SpanScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "SpanScorer.java"


# instance fields
.field protected doc:I

.field protected freq:F

.field protected more:Z

.field protected norms:[B

.field protected spans:Lorg/apache/lucene/search/spans/Spans;

.field protected value:F


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V
    .locals 1
    .param p1, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "norms"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p3, p2}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 44
    iput-object p4, p0, Lorg/apache/lucene/search/spans/SpanScorer;->norms:[B

    .line 45
    invoke-virtual {p2}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->value:F

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 64
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    if-nez v1, :cond_0

    .line 65
    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 73
    :goto_0
    return v0

    .line 67
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    if-ge v1, p1, :cond_1

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 70
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->setFreqCurrentDoc()Z

    move-result v1

    if-nez v1, :cond_2

    .line 71
    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 73
    :cond_2
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    return v0
.end method

.method protected explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 109
    .local v2, "tfExplanation":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/spans/SpanScorer;->advance(I)I

    move-result v0

    .line 111
    .local v0, "expDoc":I
    if-ne v0, p1, :cond_0

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    .line 112
    .local v1, "phraseFreq":F
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 113
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "tf(phraseFreq="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 115
    return-object v2

    .line 111
    .end local v1    # "phraseFreq":F
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public freq()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->setFreqCurrentDoc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 59
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    return v0
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->value:F

    mul-float v0, v1, v2

    .line 96
    .local v0, "raw":F
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->norms:[B

    if-nez v1, :cond_0

    .end local v0    # "raw":F
    :goto_0
    return v0

    .restart local v0    # "raw":F
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->norms:[B

    iget v3, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    aget-byte v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method protected setFreqCurrentDoc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    if-nez v1, :cond_0

    .line 78
    const/4 v1, 0x0

    .line 87
    :goto_0
    return v1

    .line 80
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    .line 81
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    .line 83
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    sub-int v0, v1, v2

    .line 84
    .local v0, "matchLength":I
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Similarity;->sloppyFreq(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->freq:F

    .line 85
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    .line 86
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->more:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 87
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
