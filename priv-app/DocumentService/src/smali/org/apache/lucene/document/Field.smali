.class public final Lorg/apache/lucene/document/Field;
.super Lorg/apache/lucene/document/AbstractField;
.source "Field.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/apache/lucene/document/Fieldable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/document/Field$1;,
        Lorg/apache/lucene/document/Field$TermVector;,
        Lorg/apache/lucene/document/Field$Index;,
        Lorg/apache/lucene/document/Field$Store;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/Reader;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 439
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 440
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .param p3, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 453
    invoke-direct {p0}, Lorg/apache/lucene/document/AbstractField;-><init>()V

    .line 454
    if-nez p1, :cond_0

    .line 455
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_0
    if-nez p2, :cond_1

    .line 457
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "reader cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 459
    :cond_1
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 460
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 462
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->isStored:Z

    .line 464
    iput-boolean v2, p0, Lorg/apache/lucene/document/Field;->isIndexed:Z

    .line 465
    iput-boolean v2, p0, Lorg/apache/lucene/document/Field;->isTokenized:Z

    .line 467
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    .line 469
    invoke-virtual {p0, p3}, Lorg/apache/lucene/document/Field;->setStoreTermVector(Lorg/apache/lucene/document/Field$TermVector;)V

    .line 470
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "index"    # Lorg/apache/lucene/document/Field$Index;

    .prologue
    .line 352
    sget-object v5, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 353
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "index"    # Lorg/apache/lucene/document/Field$Index;
    .param p5, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;

    .prologue
    .line 373
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;ZLjava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 374
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 484
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 485
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 499
    invoke-direct {p0}, Lorg/apache/lucene/document/AbstractField;-><init>()V

    .line 500
    if-nez p1, :cond_0

    .line 501
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :cond_0
    if-nez p2, :cond_1

    .line 503
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "tokenStream cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 505
    :cond_1
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 506
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 507
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    .line 509
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->isStored:Z

    .line 511
    iput-boolean v2, p0, Lorg/apache/lucene/document/Field;->isIndexed:Z

    .line 512
    iput-boolean v2, p0, Lorg/apache/lucene/document/Field;->isTokenized:Z

    .line 514
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    .line 516
    invoke-virtual {p0, p3}, Lorg/apache/lucene/document/Field;->setStoreTermVector(Lorg/apache/lucene/document/Field$TermVector;)V

    .line 517
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "internName"    # Z
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p5, "index"    # Lorg/apache/lucene/document/Field$Index;
    .param p6, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;

    .prologue
    .line 394
    invoke-direct {p0}, Lorg/apache/lucene/document/AbstractField;-><init>()V

    .line 395
    if-nez p1, :cond_0

    .line 396
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_0
    if-nez p3, :cond_1

    .line 398
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_1
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    if-ne p5, v0, :cond_2

    sget-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    if-ne p4, v0, :cond_2

    .line 400
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "it doesn\'t make sense to have a field that is neither indexed nor stored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_2
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    if-ne p5, v0, :cond_3

    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    if-eq p6, v0, :cond_3

    .line 403
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot store term vector information for a field that is not indexed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :cond_3
    if-eqz p2, :cond_4

    .line 407
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 409
    :cond_4
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 411
    iput-object p3, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 413
    invoke-virtual {p4}, Lorg/apache/lucene/document/Field$Store;->isStored()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->isStored:Z

    .line 415
    invoke-virtual {p5}, Lorg/apache/lucene/document/Field$Index;->isIndexed()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->isIndexed:Z

    .line 416
    invoke-virtual {p5}, Lorg/apache/lucene/document/Field$Index;->isAnalyzed()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->isTokenized:Z

    .line 417
    invoke-virtual {p5}, Lorg/apache/lucene/document/Field$Index;->omitNorms()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->omitNorms:Z

    .line 418
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    if-ne p5, v0, :cond_5

    .line 420
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 423
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    .line 425
    invoke-virtual {p0, p6}, Lorg/apache/lucene/document/Field;->setStoreTermVector(Lorg/apache/lucene/document/Field$TermVector;)V

    .line 426
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 545
    const/4 v0, 0x0

    array-length v1, p2

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BII)V

    .line 546
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BII)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 576
    invoke-direct {p0}, Lorg/apache/lucene/document/AbstractField;-><init>()V

    .line 578
    if-nez p1, :cond_0

    .line 579
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 580
    :cond_0
    if-nez p2, :cond_1

    .line 581
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_1
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 584
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 586
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->isStored:Z

    .line 587
    iput-boolean v2, p0, Lorg/apache/lucene/document/Field;->isIndexed:Z

    .line 588
    iput-boolean v2, p0, Lorg/apache/lucene/document/Field;->isTokenized:Z

    .line 589
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 590
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->omitNorms:Z

    .line 592
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    .line 593
    iput p4, p0, Lorg/apache/lucene/document/Field;->binaryLength:I

    .line 594
    iput p3, p0, Lorg/apache/lucene/document/Field;->binaryOffset:I

    .line 596
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/document/Field;->setStoreTermVector(Lorg/apache/lucene/document/Field$TermVector;)V

    .line 597
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BIILorg/apache/lucene/document/Field$Store;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "store"    # Lorg/apache/lucene/document/Field$Store;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 561
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BII)V

    .line 563
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    if-ne p5, v0, :cond_0

    .line 564
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "binary values can\'t be unstored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BLorg/apache/lucene/document/Field$Store;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 531
    const/4 v0, 0x0

    array-length v1, p2

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BII)V

    .line 533
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    if-ne p3, v0, :cond_0

    .line 534
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "binary values can\'t be unstored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536
    :cond_0
    return-void
.end method


# virtual methods
.method public readerValue()Ljava/io/Reader;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/io/Reader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    check-cast v0, Ljava/io/Reader;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTokenStream(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    const/4 v0, 0x1

    .line 334
    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->isIndexed:Z

    .line 335
    iput-boolean v0, p0, Lorg/apache/lucene/document/Field;->isTokenized:Z

    .line 336
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    .line 337
    return-void
.end method

.method public setValue(Ljava/io/Reader;)V
    .locals 2
    .param p1, "value"    # Ljava/io/Reader;

    .prologue
    .line 302
    iget-boolean v0, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    if-eqz v0, :cond_0

    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot set a Reader value on a binary field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/document/Field;->isStored:Z

    if-eqz v0, :cond_1

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot set a Reader value on a stored field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 309
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 294
    iget-boolean v0, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    if-eqz v0, :cond_0

    .line 295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot set a String value on a binary field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 298
    return-void
.end method

.method public setValue([B)V
    .locals 2
    .param p1, "value"    # [B

    .prologue
    .line 313
    iget-boolean v0, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    if-nez v0, :cond_0

    .line 314
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot set a byte[] value on a non-binary field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 317
    array-length v0, p1

    iput v0, p0, Lorg/apache/lucene/document/Field;->binaryLength:I

    .line 318
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/document/Field;->binaryOffset:I

    .line 319
    return-void
.end method

.method public setValue([BII)V
    .locals 2
    .param p1, "value"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 323
    iget-boolean v0, p0, Lorg/apache/lucene/document/Field;->isBinary:Z

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot set a byte[] value on a non-binary field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 327
    iput p3, p0, Lorg/apache/lucene/document/Field;->binaryLength:I

    .line 328
    iput p2, p0, Lorg/apache/lucene/document/Field;->binaryOffset:I

    .line 329
    return-void
.end method

.method public stringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tokenStreamValue()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    return-object v0
.end method
