.class public interface abstract Lorg/apache/lucene/document/Fieldable;
.super Ljava/lang/Object;
.source "Fieldable.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract getBinaryLength()I
.end method

.method public abstract getBinaryOffset()I
.end method

.method public abstract getBinaryValue()[B
.end method

.method public abstract getBinaryValue([B)[B
.end method

.method public abstract getBoost()F
.end method

.method public abstract getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;
.end method

.method public abstract getOmitNorms()Z
.end method

.method public abstract isBinary()Z
.end method

.method public abstract isIndexed()Z
.end method

.method public abstract isLazy()Z
.end method

.method public abstract isStoreOffsetWithTermVector()Z
.end method

.method public abstract isStorePositionWithTermVector()Z
.end method

.method public abstract isStored()Z
.end method

.method public abstract isTermVectorStored()Z
.end method

.method public abstract isTokenized()Z
.end method

.method public abstract name()Ljava/lang/String;
.end method

.method public abstract readerValue()Ljava/io/Reader;
.end method

.method public abstract setBoost(F)V
.end method

.method public abstract setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V
.end method

.method public abstract setOmitNorms(Z)V
.end method

.method public abstract stringValue()Ljava/lang/String;
.end method

.method public abstract tokenStreamValue()Lorg/apache/lucene/analysis/TokenStream;
.end method
