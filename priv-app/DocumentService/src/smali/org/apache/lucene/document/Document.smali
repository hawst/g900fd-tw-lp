.class public final Lorg/apache/lucene/document/Document;
.super Ljava/lang/Object;
.source "Document.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final NO_BYTES:[[B

.field private static final NO_FIELDABLES:[Lorg/apache/lucene/document/Fieldable;

.field private static final NO_FIELDS:[Lorg/apache/lucene/document/Field;

.field private static final NO_STRINGS:[Ljava/lang/String;


# instance fields
.field private boost:F

.field fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/document/Fieldable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 184
    new-array v0, v1, [Lorg/apache/lucene/document/Field;

    sput-object v0, Lorg/apache/lucene/document/Document;->NO_FIELDS:[Lorg/apache/lucene/document/Field;

    .line 215
    new-array v0, v1, [Lorg/apache/lucene/document/Fieldable;

    sput-object v0, Lorg/apache/lucene/document/Document;->NO_FIELDABLES:[Lorg/apache/lucene/document/Fieldable;

    .line 240
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lorg/apache/lucene/document/Document;->NO_STRINGS:[Ljava/lang/String;

    .line 264
    new-array v0, v1, [[B

    sput-object v0, Lorg/apache/lucene/document/Document;->NO_BYTES:[[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/Document;->boost:F

    .line 44
    return-void
.end method


# virtual methods
.method public final add(Lorg/apache/lucene/document/Fieldable;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public final get(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 167
    iget-object v2, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 168
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isBinary()Z

    move-result v2

    if-nez v2, :cond_0

    .line 169
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->stringValue()Ljava/lang/String;

    move-result-object v2

    .line 171
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getBinaryValue(Ljava/lang/String;)[B
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 298
    iget-object v2, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 299
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isBinary()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 300
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->getBinaryValue()[B

    move-result-object v2

    .line 302
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getBinaryValues(Ljava/lang/String;)[[B
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 276
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<[B>;"
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 278
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isBinary()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 279
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->getBinaryValue()[B

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 282
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 283
    sget-object v3, Lorg/apache/lucene/document/Document;->NO_BYTES:[[B

    .line 285
    :goto_1
    return-object v3

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [[B

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[B

    goto :goto_1
.end method

.method public getBoost()F
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/lucene/document/Document;->boost:F

    return v0
.end method

.method public final getField(Ljava/lang/String;)Lorg/apache/lucene/document/Field;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lorg/apache/lucene/document/Document;->getFieldable(Ljava/lang/String;)Lorg/apache/lucene/document/Fieldable;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Field;

    return-object v0
.end method

.method public getFieldable(Ljava/lang/String;)Lorg/apache/lucene/document/Fieldable;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 152
    iget-object v2, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 153
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFieldables(Ljava/lang/String;)[Lorg/apache/lucene/document/Fieldable;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 226
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 228
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 233
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 234
    sget-object v3, Lorg/apache/lucene/document/Document;->NO_FIELDABLES:[Lorg/apache/lucene/document/Fieldable;

    .line 236
    :goto_1
    return-object v3

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/document/Fieldable;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/document/Fieldable;

    goto :goto_1
.end method

.method public final getFields()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/document/Fieldable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    return-object v0
.end method

.method public final getFields(Ljava/lang/String;)[Lorg/apache/lucene/document/Field;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 201
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Field;>;"
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 203
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 204
    check-cast v0, Lorg/apache/lucene/document/Field;

    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 208
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 209
    sget-object v3, Lorg/apache/lucene/document/Document;->NO_FIELDS:[Lorg/apache/lucene/document/Field;

    .line 211
    :goto_1
    return-object v3

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/document/Field;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/document/Field;

    goto :goto_1
.end method

.method public final getValues(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 252
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 253
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 254
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isBinary()Z

    move-result v3

    if-nez v3, :cond_0

    .line 255
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->stringValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 259
    sget-object v3, Lorg/apache/lucene/document/Document;->NO_STRINGS:[Ljava/lang/String;

    .line 261
    :goto_1
    return-object v3

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    goto :goto_1
.end method

.method public final removeField(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v2, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 104
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/document/Fieldable;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 106
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 111
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    return-void
.end method

.method public final removeFields(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v2, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 124
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/document/Fieldable;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 126
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 130
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    return-void
.end method

.method public setBoost(F)V
    .locals 0
    .param p1, "boost"    # F

    .prologue
    .line 59
    iput p1, p0, Lorg/apache/lucene/document/Document;->boost:F

    .line 60
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "Document<"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 311
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/document/Fieldable;

    .line 312
    .local v1, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    iget-object v3, p0, Lorg/apache/lucene/document/Document;->fields:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_0

    .line 314
    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 316
    .end local v1    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    const-string/jumbo v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
