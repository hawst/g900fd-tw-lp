.class public abstract Lorg/apache/lucene/document/AbstractField;
.super Ljava/lang/Object;
.source "AbstractField.java"

# interfaces
.implements Lorg/apache/lucene/document/Fieldable;


# instance fields
.field protected binaryLength:I

.field protected binaryOffset:I

.field protected boost:F

.field protected fieldsData:Ljava/lang/Object;

.field protected indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field protected isBinary:Z

.field protected isIndexed:Z

.field protected isStored:Z

.field protected isTokenized:Z

.field protected lazy:Z

.field protected name:Ljava/lang/String;

.field protected omitNorms:Z

.field protected storeOffsetWithTermVector:Z

.field protected storePositionWithTermVector:Z

.field protected storeTermVector:Z

.field protected tokenStream:Lorg/apache/lucene/analysis/TokenStream;


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string/jumbo v0, "body"

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->name:Ljava/lang/String;

    .line 34
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storeTermVector:Z

    .line 35
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storeOffsetWithTermVector:Z

    .line 36
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storePositionWithTermVector:Z

    .line 37
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->omitNorms:Z

    .line 38
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isStored:Z

    .line 39
    iput-boolean v2, p0, Lorg/apache/lucene/document/AbstractField;->isIndexed:Z

    .line 40
    iput-boolean v2, p0, Lorg/apache/lucene/document/AbstractField;->isTokenized:Z

    .line 41
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    .line 42
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->lazy:Z

    .line 43
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/AbstractField;->boost:F

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    .line 55
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p3, "index"    # Lorg/apache/lucene/document/Field$Index;
    .param p4, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string/jumbo v0, "body"

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->name:Ljava/lang/String;

    .line 34
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storeTermVector:Z

    .line 35
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storeOffsetWithTermVector:Z

    .line 36
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storePositionWithTermVector:Z

    .line 37
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->omitNorms:Z

    .line 38
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isStored:Z

    .line 39
    iput-boolean v2, p0, Lorg/apache/lucene/document/AbstractField;->isIndexed:Z

    .line 40
    iput-boolean v2, p0, Lorg/apache/lucene/document/AbstractField;->isTokenized:Z

    .line 41
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    .line 42
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->lazy:Z

    .line 43
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/AbstractField;->boost:F

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    .line 58
    if-nez p1, :cond_0

    .line 59
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->name:Ljava/lang/String;

    .line 62
    invoke-virtual {p2}, Lorg/apache/lucene/document/Field$Store;->isStored()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isStored:Z

    .line 63
    invoke-virtual {p3}, Lorg/apache/lucene/document/Field$Index;->isIndexed()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isIndexed:Z

    .line 64
    invoke-virtual {p3}, Lorg/apache/lucene/document/Field$Index;->isAnalyzed()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isTokenized:Z

    .line 65
    invoke-virtual {p3}, Lorg/apache/lucene/document/Field$Index;->omitNorms()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->omitNorms:Z

    .line 67
    iput-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    .line 69
    invoke-virtual {p0, p4}, Lorg/apache/lucene/document/AbstractField;->setStoreTermVector(Lorg/apache/lucene/document/Field$TermVector;)V

    .line 70
    return-void
.end method


# virtual methods
.method public getBinaryLength()I
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    if-eqz v0, :cond_0

    .line 194
    iget v0, p0, Lorg/apache/lucene/document/AbstractField;->binaryLength:I

    .line 198
    :goto_0
    return v0

    .line 195
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    array-length v0, v0

    goto :goto_0

    .line 198
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBinaryOffset()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/lucene/document/AbstractField;->binaryOffset:I

    return v0
.end method

.method public getBinaryValue()[B
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/document/AbstractField;->getBinaryValue([B)[B

    move-result-object v0

    return-object v0
.end method

.method public getBinaryValue([B)[B
    .locals 1
    .param p1, "result"    # [B

    .prologue
    .line 181
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    .line 184
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBoost()F
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/lucene/document/AbstractField;->boost:F

    return v0
.end method

.method public getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    return-object v0
.end method

.method public getOmitNorms()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->omitNorms:Z

    return v0
.end method

.method public getOmitTermFreqAndPositions()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isBinary()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    return v0
.end method

.method public final isIndexed()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isIndexed:Z

    return v0
.end method

.method public isLazy()Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->lazy:Z

    return v0
.end method

.method public isStoreOffsetWithTermVector()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->storeOffsetWithTermVector:Z

    return v0
.end method

.method public isStorePositionWithTermVector()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->storePositionWithTermVector:Z

    return v0
.end method

.method public final isStored()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isStored:Z

    return v0
.end method

.method public final isTermVectorStored()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->storeTermVector:Z

    return v0
.end method

.method public final isTokenized()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->isTokenized:Z

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/document/AbstractField;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setBoost(F)V
    .locals 0
    .param p1, "boost"    # F

    .prologue
    .line 95
    iput p1, p0, Lorg/apache/lucene/document/AbstractField;->boost:F

    .line 96
    return-void
.end method

.method public setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 0
    .param p1, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    .line 248
    iput-object p1, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    return-void
.end method

.method public setOmitNorms(Z)V
    .locals 0
    .param p1, "omitNorms"    # Z

    .prologue
    .line 225
    iput-boolean p1, p0, Lorg/apache/lucene/document/AbstractField;->omitNorms:Z

    return-void
.end method

.method public setOmitTermFreqAndPositions(Z)V
    .locals 1
    .param p1, "omitTermFreqAndPositions"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 230
    if-eqz p1, :cond_0

    .line 231
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 235
    :goto_0
    return-void

    .line 233
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    goto :goto_0
.end method

.method protected setStoreTermVector(Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 1
    .param p1, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;

    .prologue
    .line 119
    invoke-virtual {p1}, Lorg/apache/lucene/document/Field$TermVector;->isStored()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->storeTermVector:Z

    .line 120
    invoke-virtual {p1}, Lorg/apache/lucene/document/Field$TermVector;->withPositions()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->storePositionWithTermVector:Z

    .line 121
    invoke-virtual {p1}, Lorg/apache/lucene/document/Field$TermVector;->withOffsets()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/AbstractField;->storeOffsetWithTermVector:Z

    .line 122
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    .local v0, "result":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isStored:Z

    if-eqz v1, :cond_0

    .line 259
    const-string/jumbo v1, "stored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isIndexed:Z

    if-eqz v1, :cond_2

    .line 262
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 263
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_1
    const-string/jumbo v1, "indexed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isTokenized:Z

    if-eqz v1, :cond_4

    .line 267
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 268
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_3
    const-string/jumbo v1, "tokenized"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_4
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storeTermVector:Z

    if-eqz v1, :cond_6

    .line 272
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 273
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :cond_5
    const-string/jumbo v1, "termVector"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_6
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storeOffsetWithTermVector:Z

    if-eqz v1, :cond_8

    .line 277
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 278
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_7
    const-string/jumbo v1, "termVectorOffsets"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_8
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->storePositionWithTermVector:Z

    if-eqz v1, :cond_a

    .line 282
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 283
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_9
    const-string/jumbo v1, "termVectorPosition"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_a
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->isBinary:Z

    if-eqz v1, :cond_c

    .line 287
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_b

    .line 288
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    :cond_b
    const-string/jumbo v1, "binary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_c
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->omitNorms:Z

    if-eqz v1, :cond_d

    .line 292
    const-string/jumbo v1, ",omitNorms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_d
    iget-object v1, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_e

    .line 295
    const-string/jumbo v1, ",indexOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    iget-object v1, p0, Lorg/apache/lucene/document/AbstractField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 298
    :cond_e
    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->lazy:Z

    if-eqz v1, :cond_f

    .line 299
    const-string/jumbo v1, ",lazy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    :cond_f
    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 302
    iget-object v1, p0, Lorg/apache/lucene/document/AbstractField;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 305
    iget-object v1, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    if-eqz v1, :cond_10

    iget-boolean v1, p0, Lorg/apache/lucene/document/AbstractField;->lazy:Z

    if-nez v1, :cond_10

    .line 306
    iget-object v1, p0, Lorg/apache/lucene/document/AbstractField;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 309
    :cond_10
    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 310
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
