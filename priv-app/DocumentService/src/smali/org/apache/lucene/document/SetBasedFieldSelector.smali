.class public Lorg/apache/lucene/document/SetBasedFieldSelector;
.super Ljava/lang/Object;
.source "SetBasedFieldSelector.java"

# interfaces
.implements Lorg/apache/lucene/document/FieldSelector;


# instance fields
.field private fieldsToLoad:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lazyFieldsToLoad:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "lazyFieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/document/SetBasedFieldSelector;->fieldsToLoad:Ljava/util/Set;

    .line 37
    iput-object p2, p0, Lorg/apache/lucene/document/SetBasedFieldSelector;->lazyFieldsToLoad:Ljava/util/Set;

    .line 38
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)Lorg/apache/lucene/document/FieldSelectorResult;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 49
    sget-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->NO_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 50
    .local v0, "result":Lorg/apache/lucene/document/FieldSelectorResult;
    iget-object v1, p0, Lorg/apache/lucene/document/SetBasedFieldSelector;->fieldsToLoad:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 51
    sget-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 53
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/document/SetBasedFieldSelector;->lazyFieldsToLoad:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 54
    sget-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->LAZY_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 56
    :cond_1
    return-object v0
.end method
