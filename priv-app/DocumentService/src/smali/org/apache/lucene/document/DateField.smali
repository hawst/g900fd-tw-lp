.class public Lorg/apache/lucene/document/DateField;
.super Ljava/lang/Object;
.source "DateField.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static DATE_LEN:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    const-wide v0, 0x1cae8c13e000L

    const/16 v2, 0x24

    invoke-static {v0, v1, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static MAX_DATE_STRING()Ljava/lang/String;
    .locals 5

    .prologue
    .line 74
    sget v3, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    new-array v0, v3, [C

    .line 75
    .local v0, "buffer":[C
    const/16 v3, 0x23

    const/16 v4, 0x24

    invoke-static {v3, v4}, Ljava/lang/Character;->forDigit(II)C

    move-result v1

    .line 76
    .local v1, "c":C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget v3, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    if-ge v2, v3, :cond_0

    .line 77
    aput-char v1, v0, v2

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 78
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method public static MIN_DATE_STRING()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lorg/apache/lucene/document/DateField;->timeToString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static dateToString(Ljava/util/Date;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/lucene/document/DateField;->timeToString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static stringToDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Ljava/util/Date;

    invoke-static {p0}, Lorg/apache/lucene/document/DateField;->stringToTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public static stringToTime(Ljava/lang/String;)J
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 117
    const/16 v0, 0x24

    invoke-static {p0, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static timeToString(J)Ljava/lang/String;
    .locals 6
    .param p0, "time"    # J

    .prologue
    const/4 v4, 0x0

    .line 95
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-gez v2, :cond_0

    .line 96
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "time \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' is too early, must be >= 0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_0
    const/16 v2, 0x24

    invoke-static {p0, p1, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    sget v3, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    if-le v2, v3, :cond_1

    .line 101
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "time \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' is too late, length of string "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "representation must be <= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 105
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    sget v3, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    if-ge v2, v3, :cond_3

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 107
    .local v1, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    sget v3, Lorg/apache/lucene/document/DateField;->DATE_LEN:I

    if-ge v2, v3, :cond_2

    .line 108
    invoke-virtual {v1, v4, v4}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 109
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    return-object v0
.end method
