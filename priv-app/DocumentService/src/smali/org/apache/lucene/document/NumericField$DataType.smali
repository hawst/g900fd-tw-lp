.class public final enum Lorg/apache/lucene/document/NumericField$DataType;
.super Ljava/lang/Enum;
.source "NumericField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/NumericField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/NumericField$DataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/document/NumericField$DataType;

.field public static final enum DOUBLE:Lorg/apache/lucene/document/NumericField$DataType;

.field public static final enum FLOAT:Lorg/apache/lucene/document/NumericField$DataType;

.field public static final enum INT:Lorg/apache/lucene/document/NumericField$DataType;

.field public static final enum LONG:Lorg/apache/lucene/document/NumericField$DataType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 138
    new-instance v0, Lorg/apache/lucene/document/NumericField$DataType;

    const-string/jumbo v1, "INT"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/NumericField$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/NumericField$DataType;->INT:Lorg/apache/lucene/document/NumericField$DataType;

    new-instance v0, Lorg/apache/lucene/document/NumericField$DataType;

    const-string/jumbo v1, "LONG"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/document/NumericField$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/NumericField$DataType;->LONG:Lorg/apache/lucene/document/NumericField$DataType;

    new-instance v0, Lorg/apache/lucene/document/NumericField$DataType;

    const-string/jumbo v1, "FLOAT"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/document/NumericField$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/NumericField$DataType;->FLOAT:Lorg/apache/lucene/document/NumericField$DataType;

    new-instance v0, Lorg/apache/lucene/document/NumericField$DataType;

    const-string/jumbo v1, "DOUBLE"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/document/NumericField$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/NumericField$DataType;->DOUBLE:Lorg/apache/lucene/document/NumericField$DataType;

    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->INT:Lorg/apache/lucene/document/NumericField$DataType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->LONG:Lorg/apache/lucene/document/NumericField$DataType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->FLOAT:Lorg/apache/lucene/document/NumericField$DataType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->DOUBLE:Lorg/apache/lucene/document/NumericField$DataType;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/document/NumericField$DataType;->$VALUES:[Lorg/apache/lucene/document/NumericField$DataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/NumericField$DataType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 138
    const-class v0, Lorg/apache/lucene/document/NumericField$DataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/NumericField$DataType;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/document/NumericField$DataType;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lorg/apache/lucene/document/NumericField$DataType;->$VALUES:[Lorg/apache/lucene/document/NumericField$DataType;

    invoke-virtual {v0}, [Lorg/apache/lucene/document/NumericField$DataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/document/NumericField$DataType;

    return-object v0
.end method
