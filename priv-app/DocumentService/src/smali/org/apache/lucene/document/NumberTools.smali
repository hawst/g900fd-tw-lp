.class public Lorg/apache/lucene/document/NumberTools;
.super Ljava/lang/Object;
.source "NumberTools.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final MAX_STRING_VALUE:Ljava/lang/String; = "01y2p0ij32e8e7"

.field public static final MIN_STRING_VALUE:Ljava/lang/String; = "-0000000000000"

.field private static final NEGATIVE_PREFIX:C = '-'

.field private static final POSITIVE_PREFIX:C = '0'

.field private static final RADIX:I = 0x24

.field public static final STR_SIZE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-string/jumbo v0, "-0000000000000"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lorg/apache/lucene/document/NumberTools;->STR_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static longToString(J)Ljava/lang/String;
    .locals 10
    .param p0, "l"    # J

    .prologue
    const/16 v8, 0x30

    .line 80
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v4, p0, v4

    if-nez v4, :cond_0

    .line 82
    const-string/jumbo v4, "-0000000000000"

    .line 101
    :goto_0
    return-object v4

    .line 85
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    sget v4, Lorg/apache/lucene/document/NumberTools;->STR_SIZE:I

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 87
    .local v0, "buf":Ljava/lang/StringBuilder;
    const-wide/16 v4, 0x0

    cmp-long v4, p0, v4

    if-gez v4, :cond_1

    .line 88
    const/16 v4, 0x2d

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    const-wide v4, 0x7fffffffffffffffL

    add-long/2addr v4, p0

    const-wide/16 v6, 0x1

    add-long p0, v4, v6

    .line 93
    :goto_1
    const/16 v4, 0x24

    invoke-static {p0, p1, v4}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "num":Ljava/lang/String;
    sget v4, Lorg/apache/lucene/document/NumberTools;->STR_SIZE:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    sub-int v2, v4, v5

    .local v2, "padLen":I
    move v3, v2

    .line 96
    .end local v2    # "padLen":I
    .local v3, "padLen":I
    :goto_2
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "padLen":I
    .restart local v2    # "padLen":I
    if-lez v3, :cond_2

    .line 97
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v3, v2

    .end local v2    # "padLen":I
    .restart local v3    # "padLen":I
    goto :goto_2

    .line 91
    .end local v1    # "num":Ljava/lang/String;
    .end local v3    # "padLen":I
    :cond_1
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 99
    .restart local v1    # "num":Ljava/lang/String;
    .restart local v2    # "padLen":I
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static stringToLong(Ljava/lang/String;)J
    .locals 8
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 115
    if-nez p0, :cond_0

    .line 116
    new-instance v3, Ljava/lang/NullPointerException;

    const-string/jumbo v4, "string cannot be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 118
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    sget v4, Lorg/apache/lucene/document/NumberTools;->STR_SIZE:I

    if-eq v3, v4, :cond_1

    .line 119
    new-instance v3, Ljava/lang/NumberFormatException;

    const-string/jumbo v4, "string is the wrong size"

    invoke-direct {v3, v4}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 122
    :cond_1
    const-string/jumbo v3, "-0000000000000"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 123
    const-wide/high16 v0, -0x8000000000000000L

    .line 138
    :cond_2
    :goto_0
    return-wide v0

    .line 126
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 127
    .local v2, "prefix":C
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x24

    invoke-static {v3, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 129
    .local v0, "l":J
    const/16 v3, 0x30

    if-eq v2, v3, :cond_2

    .line 131
    const/16 v3, 0x2d

    if-ne v2, v3, :cond_4

    .line 132
    const-wide v4, 0x7fffffffffffffffL

    sub-long v4, v0, v4

    const-wide/16 v6, 0x1

    sub-long v0, v4, v6

    goto :goto_0

    .line 134
    :cond_4
    new-instance v3, Ljava/lang/NumberFormatException;

    const-string/jumbo v4, "string does not begin with the correct prefix"

    invoke-direct {v3, v4}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
