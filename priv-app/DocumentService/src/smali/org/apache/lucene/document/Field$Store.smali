.class public abstract enum Lorg/apache/lucene/document/Field$Store;
.super Ljava/lang/Enum;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "Store"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/Field$Store;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/document/Field$Store;

.field public static final enum NO:Lorg/apache/lucene/document/Field$Store;

.field public static final enum YES:Lorg/apache/lucene/document/Field$Store;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lorg/apache/lucene/document/Field$Store$1;

    const-string/jumbo v1, "YES"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/Field$Store$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    .line 53
    new-instance v0, Lorg/apache/lucene/document/Field$Store$2;

    const-string/jumbo v1, "NO"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/document/Field$Store$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/document/Field$Store;

    sget-object v1, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/document/Field$Store;->$VALUES:[Lorg/apache/lucene/document/Field$Store;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lorg/apache/lucene/document/Field$1;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/Field$Store;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/Field$Store;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/document/Field$Store;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Field$Store;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/document/Field$Store;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->$VALUES:[Lorg/apache/lucene/document/Field$Store;

    invoke-virtual {v0}, [Lorg/apache/lucene/document/Field$Store;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/document/Field$Store;

    return-object v0
.end method


# virtual methods
.method public abstract isStored()Z
.end method
