.class public final enum Lorg/apache/lucene/document/FieldSelectorResult;
.super Ljava/lang/Enum;
.source "FieldSelectorResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/FieldSelectorResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum LATENT:Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum LAZY_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum LOAD_AND_BREAK:Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum NO_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum SIZE:Lorg/apache/lucene/document/FieldSelectorResult;

.field public static final enum SIZE_AND_BREAK:Lorg/apache/lucene/document/FieldSelectorResult;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "LOAD"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 40
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "LAZY_LOAD"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->LAZY_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 48
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "NO_LOAD"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->NO_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 57
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "LOAD_AND_BREAK"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD_AND_BREAK:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 63
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "SIZE"

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->SIZE:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 66
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "SIZE_AND_BREAK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->SIZE_AND_BREAK:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 75
    new-instance v0, Lorg/apache/lucene/document/FieldSelectorResult;

    const-string/jumbo v1, "LATENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/FieldSelectorResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->LATENT:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 23
    const/4 v0, 0x7

    new-array v0, v0, [Lorg/apache/lucene/document/FieldSelectorResult;

    sget-object v1, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/document/FieldSelectorResult;->LAZY_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/document/FieldSelectorResult;->NO_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD_AND_BREAK:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/document/FieldSelectorResult;->SIZE:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/document/FieldSelectorResult;->SIZE_AND_BREAK:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/lucene/document/FieldSelectorResult;->LATENT:Lorg/apache/lucene/document/FieldSelectorResult;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->$VALUES:[Lorg/apache/lucene/document/FieldSelectorResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/FieldSelectorResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lorg/apache/lucene/document/FieldSelectorResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/FieldSelectorResult;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/document/FieldSelectorResult;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->$VALUES:[Lorg/apache/lucene/document/FieldSelectorResult;

    invoke-virtual {v0}, [Lorg/apache/lucene/document/FieldSelectorResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/document/FieldSelectorResult;

    return-object v0
.end method
