.class public Lorg/apache/lucene/document/MapFieldSelector;
.super Ljava/lang/Object;
.source "MapFieldSelector.java"

# interfaces
.implements Lorg/apache/lucene/document/FieldSelector;


# instance fields
.field fieldSelections:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/document/FieldSelectorResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x5

    div-int/lit8 v3, v3, 0x3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/document/MapFieldSelector;->fieldSelections:Ljava/util/Map;

    .line 45
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    .local v0, "field":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/document/MapFieldSelector;->fieldSelections:Ljava/util/Map;

    sget-object v3, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 47
    .end local v0    # "field":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/document/FieldSelectorResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "fieldSelections":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/document/FieldSelectorResult;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/document/MapFieldSelector;->fieldSelections:Ljava/util/Map;

    .line 38
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1, "fields"    # [Ljava/lang/String;

    .prologue
    .line 53
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/document/MapFieldSelector;-><init>(Ljava/util/List;)V

    .line 54
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)Lorg/apache/lucene/document/FieldSelectorResult;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v1, p0, Lorg/apache/lucene/document/MapFieldSelector;->fieldSelections:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/FieldSelectorResult;

    .line 64
    .local v0, "selection":Lorg/apache/lucene/document/FieldSelectorResult;
    if-eqz v0, :cond_0

    .end local v0    # "selection":Lorg/apache/lucene/document/FieldSelectorResult;
    :goto_0
    return-object v0

    .restart local v0    # "selection":Lorg/apache/lucene/document/FieldSelectorResult;
    :cond_0
    sget-object v0, Lorg/apache/lucene/document/FieldSelectorResult;->NO_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    goto :goto_0
.end method
