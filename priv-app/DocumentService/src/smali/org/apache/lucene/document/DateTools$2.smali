.class Lorg/apache/lucene/document/DateTools$2;
.super Ljava/lang/ThreadLocal;
.source "DateTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/DateTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ThreadLocal",
        "<[",
        "Ljava/text/SimpleDateFormat;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/ThreadLocal;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic initialValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/document/DateTools$2;->initialValue()[Ljava/text/SimpleDateFormat;

    move-result-object v0

    return-object v0
.end method

.method protected initialValue()[Ljava/text/SimpleDateFormat;
    .locals 7

    .prologue
    .line 68
    sget-object v5, Lorg/apache/lucene/document/DateTools$Resolution;->MILLISECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    iget v5, v5, Lorg/apache/lucene/document/DateTools$Resolution;->formatLen:I

    add-int/lit8 v5, v5, 0x1

    new-array v0, v5, [Ljava/text/SimpleDateFormat;

    .line 69
    .local v0, "arr":[Ljava/text/SimpleDateFormat;
    invoke-static {}, Lorg/apache/lucene/document/DateTools$Resolution;->values()[Lorg/apache/lucene/document/DateTools$Resolution;

    move-result-object v1

    .local v1, "arr$":[Lorg/apache/lucene/document/DateTools$Resolution;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    .line 70
    .local v4, "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    iget v6, v4, Lorg/apache/lucene/document/DateTools$Resolution;->formatLen:I

    iget-object v5, v4, Lorg/apache/lucene/document/DateTools$Resolution;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v5}, Ljava/text/SimpleDateFormat;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/text/SimpleDateFormat;

    aput-object v5, v0, v6

    .line 69
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 72
    .end local v4    # "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_0
    return-object v0
.end method
