.class public Lorg/apache/lucene/document/CompressionTools;
.super Ljava/lang/Object;
.source "CompressionTools.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compress([B)[B
    .locals 3
    .param p0, "value"    # [B

    .prologue
    .line 75
    const/4 v0, 0x0

    array-length v1, p0

    const/16 v2, 0x9

    invoke-static {p0, v0, v1, v2}, Lorg/apache/lucene/document/CompressionTools;->compress([BIII)[B

    move-result-object v0

    return-object v0
.end method

.method public static compress([BII)[B
    .locals 1
    .param p0, "value"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 70
    const/16 v0, 0x9

    invoke-static {p0, p1, p2, v0}, Lorg/apache/lucene/document/CompressionTools;->compress([BIII)[B

    move-result-object v0

    return-object v0
.end method

.method public static compress([BIII)[B
    .locals 5
    .param p0, "value"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "compressionLevel"    # I

    .prologue
    .line 46
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, p2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 48
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/util/zip/Deflater;

    invoke-direct {v2}, Ljava/util/zip/Deflater;-><init>()V

    .line 51
    .local v2, "compressor":Ljava/util/zip/Deflater;
    :try_start_0
    invoke-virtual {v2, p3}, Ljava/util/zip/Deflater;->setLevel(I)V

    .line 52
    invoke-virtual {v2, p0, p1, p2}, Ljava/util/zip/Deflater;->setInput([BII)V

    .line 53
    invoke-virtual {v2}, Ljava/util/zip/Deflater;->finish()V

    .line 56
    const/16 v4, 0x400

    new-array v1, v4, [B

    .line 57
    .local v1, "buf":[B
    :goto_0
    invoke-virtual {v2}, Ljava/util/zip/Deflater;->finished()Z

    move-result v4

    if-nez v4, :cond_0

    .line 58
    invoke-virtual {v2, v1}, Ljava/util/zip/Deflater;->deflate([B)I

    move-result v3

    .line 59
    .local v3, "count":I
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 62
    .end local v1    # "buf":[B
    .end local v3    # "count":I
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->end()V

    throw v4

    .restart local v1    # "buf":[B
    :cond_0
    invoke-virtual {v2}, Ljava/util/zip/Deflater;->end()V

    .line 65
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method public static compressString(Ljava/lang/String;)[B
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 80
    const/16 v0, 0x9

    invoke-static {p0, v0}, Lorg/apache/lucene/document/CompressionTools;->compressString(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static compressString(Ljava/lang/String;I)[B
    .locals 4
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "compressionLevel"    # I

    .prologue
    const/4 v3, 0x0

    .line 87
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    .line 88
    .local v0, "result":Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p0, v3, v1, v0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/String;IILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V

    .line 89
    iget-object v1, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    iget v2, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    invoke-static {v1, v3, v2, p1}, Lorg/apache/lucene/document/CompressionTools;->compress([BIII)[B

    move-result-object v1

    return-object v1
.end method

.method public static decompress([B)[B
    .locals 5
    .param p0, "value"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    array-length v4, p0

    invoke-direct {v0, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 98
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/util/zip/Inflater;

    invoke-direct {v3}, Ljava/util/zip/Inflater;-><init>()V

    .line 101
    .local v3, "decompressor":Ljava/util/zip/Inflater;
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/util/zip/Inflater;->setInput([B)V

    .line 104
    const/16 v4, 0x400

    new-array v1, v4, [B

    .line 105
    .local v1, "buf":[B
    :goto_0
    invoke-virtual {v3}, Ljava/util/zip/Inflater;->finished()Z

    move-result v4

    if-nez v4, :cond_0

    .line 106
    invoke-virtual {v3, v1}, Ljava/util/zip/Inflater;->inflate([B)I

    move-result v2

    .line 107
    .local v2, "count":I
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 110
    .end local v1    # "buf":[B
    .end local v2    # "count":I
    :catchall_0
    move-exception v4

    invoke-virtual {v3}, Ljava/util/zip/Inflater;->end()V

    throw v4

    .restart local v1    # "buf":[B
    :cond_0
    invoke-virtual {v3}, Ljava/util/zip/Inflater;->end()V

    .line 113
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method public static decompressString([B)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 119
    new-instance v1, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-direct {v1}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;-><init>()V

    .line 120
    .local v1, "result":Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;
    invoke-static {p0}, Lorg/apache/lucene/document/CompressionTools;->decompress([B)[B

    move-result-object v0

    .line 121
    .local v0, "bytes":[B
    array-length v2, v0

    invoke-static {v0, v5, v2, v1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    .line 122
    new-instance v2, Ljava/lang/String;

    iget-object v3, v1, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget v4, v1, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    invoke-direct {v2, v3, v5, v4}, Ljava/lang/String;-><init>([CII)V

    return-object v2
.end method
