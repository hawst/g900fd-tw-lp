.class public final Lorg/apache/lucene/document/NumericField;
.super Lorg/apache/lucene/document/AbstractField;
.source "NumericField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/document/NumericField$1;,
        Lorg/apache/lucene/document/NumericField$DataType;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private transient numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

.field private final precisionStep:I

.field private type:Lorg/apache/lucene/document/NumericField$DataType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    const-class v0, Lorg/apache/lucene/document/NumericField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/document/NumericField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 153
    const/4 v0, 0x4

    sget-object v1, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;Z)V

    .line 154
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "precisionStep"    # I

    .prologue
    .line 180
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;Z)V

    .line 181
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "precisionStep"    # I
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "index"    # Z

    .prologue
    .line 195
    if-eqz p4, :cond_0

    sget-object v0, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    :goto_0
    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-direct {p0, p1, p3, v0, v1}, Lorg/apache/lucene/document/AbstractField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 196
    const/4 v0, 0x1

    if-ge p2, v0, :cond_1

    .line 197
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "precisionStep must be >=1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_0
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    goto :goto_0

    .line 198
    :cond_1
    iput p2, p0, Lorg/apache/lucene/document/NumericField;->precisionStep:I

    .line 199
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/document/NumericField;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 200
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Z)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p3, "index"    # Z

    .prologue
    .line 167
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Store;Z)V

    .line 168
    return-void
.end method


# virtual methods
.method public getBinaryValue([B)[B
    .locals 1
    .param p1, "result"    # [B

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDataType()Lorg/apache/lucene/document/NumericField$DataType;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    return-object v0
.end method

.method public getNumericValue()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    return-object v0
.end method

.method public getPrecisionStep()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lorg/apache/lucene/document/NumericField;->precisionStep:I

    return v0
.end method

.method public readerValue()Ljava/io/Reader;
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    return-object v0
.end method

.method public setDoubleValue(D)Lorg/apache/lucene/document/NumericField;
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 300
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/NumericTokenStream;->setDoubleValue(D)Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 301
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    .line 302
    sget-object v0, Lorg/apache/lucene/document/NumericField$DataType;->DOUBLE:Lorg/apache/lucene/document/NumericField$DataType;

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    .line 303
    return-object p0
.end method

.method public setFloatValue(F)Lorg/apache/lucene/document/NumericField;
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 313
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/NumericTokenStream;->setFloatValue(F)Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 314
    :cond_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    .line 315
    sget-object v0, Lorg/apache/lucene/document/NumericField$DataType;->FLOAT:Lorg/apache/lucene/document/NumericField$DataType;

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    .line 316
    return-object p0
.end method

.method public setIntValue(I)Lorg/apache/lucene/document/NumericField;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 287
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/NumericTokenStream;->setIntValue(I)Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 288
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    .line 289
    sget-object v0, Lorg/apache/lucene/document/NumericField$DataType;->INT:Lorg/apache/lucene/document/NumericField$DataType;

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    .line 290
    return-object p0
.end method

.method public setLongValue(J)Lorg/apache/lucene/document/NumericField;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/NumericTokenStream;->setLongValue(J)Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 275
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    .line 276
    sget-object v0, Lorg/apache/lucene/document/NumericField$DataType;->LONG:Lorg/apache/lucene/document/NumericField$DataType;

    iput-object v0, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    .line 277
    return-object p0
.end method

.method public stringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public tokenStreamValue()Lorg/apache/lucene/analysis/TokenStream;
    .locals 4

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/apache/lucene/document/NumericField;->isIndexed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 205
    const/4 v1, 0x0

    .line 228
    :goto_0
    return-object v1

    .line 206
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    if-nez v1, :cond_2

    .line 209
    new-instance v1, Lorg/apache/lucene/analysis/NumericTokenStream;

    iget v2, p0, Lorg/apache/lucene/document/NumericField;->precisionStep:I

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/NumericTokenStream;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 211
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 212
    sget-boolean v1, Lorg/apache/lucene/document/NumericField;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 213
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/document/NumericField;->fieldsData:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    .line 214
    .local v0, "val":Ljava/lang/Number;
    sget-object v1, Lorg/apache/lucene/document/NumericField$1;->$SwitchMap$org$apache$lucene$document$NumericField$DataType:[I

    iget-object v2, p0, Lorg/apache/lucene/document/NumericField;->type:Lorg/apache/lucene/document/NumericField$DataType;

    invoke-virtual {v2}, Lorg/apache/lucene/document/NumericField$DataType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 224
    sget-boolean v1, Lorg/apache/lucene/document/NumericField;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    const-string/jumbo v2, "Should never get here"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 216
    :pswitch_0
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/NumericTokenStream;->setIntValue(I)Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 228
    .end local v0    # "val":Ljava/lang/Number;
    :cond_2
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_0

    .line 218
    .restart local v0    # "val":Ljava/lang/Number;
    :pswitch_1
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/NumericTokenStream;->setLongValue(J)Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_1

    .line 220
    :pswitch_2
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/NumericTokenStream;->setFloatValue(F)Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_1

    .line 222
    :pswitch_3
    iget-object v1, p0, Lorg/apache/lucene/document/NumericField;->numericTS:Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/NumericTokenStream;->setDoubleValue(D)Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_1

    .line 214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
