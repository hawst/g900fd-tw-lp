.class public interface abstract Lorg/apache/lucene/queryParser/CharStream;
.super Ljava/lang/Object;
.source "CharStream.java"


# virtual methods
.method public abstract BeginToken()C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract Done()V
.end method

.method public abstract GetImage()Ljava/lang/String;
.end method

.method public abstract GetSuffix(I)[C
.end method

.method public abstract backup(I)V
.end method

.method public abstract getBeginColumn()I
.end method

.method public abstract getBeginLine()I
.end method

.method public abstract getColumn()I
.end method

.method public abstract getEndColumn()I
.end method

.method public abstract getEndLine()I
.end method

.method public abstract getLine()I
.end method

.method public abstract readChar()C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
