.class public Lorg/apache/lucene/queryParser/MultiFieldQueryParser;
.super Lorg/apache/lucene/queryParser/QueryParser;
.source "MultiFieldQueryParser.java"


# instance fields
.field protected boosts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected fields:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 96
    iput-object p2, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    .line 97
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Ljava/util/Map;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "[",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p4, "boosts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;-><init>(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 71
    iput-object p4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    .line 72
    return-void
.end method

.method private applySlop(Lorg/apache/lucene/search/Query;I)V
    .locals 1
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .param p2, "slop"    # I

    .prologue
    .line 128
    instance-of v0, p1, Lorg/apache/lucene/search/PhraseQuery;

    if-eqz v0, :cond_1

    .line 129
    check-cast p1, Lorg/apache/lucene/search/PhraseQuery;

    .end local p1    # "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    .restart local p1    # "q":Lorg/apache/lucene/search/Query;
    :cond_1
    instance-of v0, p1, Lorg/apache/lucene/search/MultiPhraseQuery;

    if-eqz v0, :cond_0

    .line 131
    check-cast p1, Lorg/apache/lucene/search/MultiPhraseQuery;

    .end local p1    # "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    goto :goto_0
.end method

.method public static parse(Lorg/apache/lucene/util/Version;Ljava/lang/String;[Ljava/lang/String;[Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "flags"    # [Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p4, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 286
    array-length v4, p2

    array-length v5, p3

    if-eq v4, v5, :cond_0

    .line 287
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "fields.length != flags.length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 288
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 289
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_3

    .line 290
    new-instance v3, Lorg/apache/lucene/queryParser/QueryParser;

    aget-object v4, p2, v1

    invoke-direct {v3, p0, v4, p4}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 291
    .local v3, "qp":Lorg/apache/lucene/queryParser/QueryParser;
    invoke-virtual {v3, p1}, Lorg/apache/lucene/queryParser/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 292
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_2

    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_1

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_2

    .line 294
    :cond_1
    aget-object v4, p3, v1

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 289
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    .end local v2    # "q":Lorg/apache/lucene/search/Query;
    .end local v3    # "qp":Lorg/apache/lucene/queryParser/QueryParser;
    :cond_3
    return-object v0
.end method

.method public static parse(Lorg/apache/lucene/util/Version;[Ljava/lang/String;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "queries"    # [Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 238
    array-length v4, p1

    array-length v5, p2

    if-eq v4, v5, :cond_0

    .line 239
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "queries.length != fields.length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 240
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 241
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_3

    .line 243
    new-instance v3, Lorg/apache/lucene/queryParser/QueryParser;

    aget-object v4, p2, v1

    invoke-direct {v3, p0, v4, p3}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 244
    .local v3, "qp":Lorg/apache/lucene/queryParser/QueryParser;
    aget-object v4, p1, v1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 245
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_2

    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_1

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_2

    .line 247
    :cond_1
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 241
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 250
    .end local v2    # "q":Lorg/apache/lucene/search/Query;
    .end local v3    # "qp":Lorg/apache/lucene/queryParser/QueryParser;
    :cond_3
    return-object v0
.end method

.method public static parse(Lorg/apache/lucene/util/Version;[Ljava/lang/String;[Ljava/lang/String;[Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "queries"    # [Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "flags"    # [Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p4, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 335
    array-length v4, p1

    array-length v5, p2

    if-ne v4, v5, :cond_0

    array-length v4, p1

    array-length v5, p3

    if-eq v4, v5, :cond_1

    .line 336
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "queries, fields, and flags array have have different length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 337
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 338
    .local v0, "bQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_4

    .line 340
    new-instance v3, Lorg/apache/lucene/queryParser/QueryParser;

    aget-object v4, p2, v1

    invoke-direct {v3, p0, v4, p4}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 341
    .local v3, "qp":Lorg/apache/lucene/queryParser/QueryParser;
    aget-object v4, p1, v1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 342
    .local v2, "q":Lorg/apache/lucene/search/Query;
    if-eqz v2, :cond_3

    instance-of v4, v2, Lorg/apache/lucene/search/BooleanQuery;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_3

    .line 344
    :cond_2
    aget-object v4, p3, v1

    invoke-virtual {v0, v2, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 338
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 347
    .end local v2    # "q":Lorg/apache/lucene/search/Query;
    .end local v3    # "qp":Lorg/apache/lucene/queryParser/QueryParser;
    :cond_4
    return-object v0
.end method


# virtual methods
.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "slop"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 101
    if-nez p1, :cond_4

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 104
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-super {p0, v4, p2, v6}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 105
    .local v3, "q":Lorg/apache/lucene/search/Query;
    if-eqz v3, :cond_1

    .line 107
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    if-eqz v4, :cond_0

    .line 109
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    iget-object v5, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 110
    .local v0, "boost":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 114
    .end local v0    # "boost":Ljava/lang/Float;
    :cond_0
    invoke-direct {p0, v3, p3}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->applySlop(Lorg/apache/lucene/search/Query;I)V

    .line 115
    new-instance v4, Lorg/apache/lucene/search/BooleanClause;

    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v4, v3, v5}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 118
    .end local v3    # "q":Lorg/apache/lucene/search/Query;
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 119
    const/4 v3, 0x0

    .line 124
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :goto_1
    return-object v3

    .line 120
    .restart local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v2    # "i":I
    :cond_3
    invoke-virtual {p0, v1, v6}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    goto :goto_1

    .line 122
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :cond_4
    invoke-super {p0, p1, p2, v6}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 123
    .restart local v3    # "q":Lorg/apache/lucene/search/Query;
    invoke-direct {p0, v3, p3}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->applySlop(Lorg/apache/lucene/search/Query;I)V

    goto :goto_1
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "quoted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 138
    if-nez p1, :cond_4

    .line 139
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 140
    .local v1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 141
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-super {p0, v4, p2, p3}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 142
    .local v3, "q":Lorg/apache/lucene/search/Query;
    if-eqz v3, :cond_1

    .line 144
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    if-eqz v4, :cond_0

    .line 146
    iget-object v4, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->boosts:Ljava/util/Map;

    iget-object v5, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 147
    .local v0, "boost":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 151
    .end local v0    # "boost":Ljava/lang/Float;
    :cond_0
    new-instance v4, Lorg/apache/lucene/search/BooleanClause;

    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v4, v3, v5}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 154
    .end local v3    # "q":Lorg/apache/lucene/search/Query;
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 155
    const/4 v3, 0x0

    .line 159
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :goto_1
    return-object v3

    .line 156
    .restart local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .restart local v2    # "i":I
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p0, v1, v4}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    goto :goto_1

    .line 158
    .end local v1    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v2    # "i":I
    :cond_4
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 159
    .restart local v3    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_1
.end method

.method protected getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 166
    if-nez p1, :cond_1

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 169
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2, p3}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 174
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/queryParser/QueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method

.method protected getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 180
    if-nez p1, :cond_1

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 183
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 186
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 188
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    :cond_1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryParser/QueryParser;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method

.method protected getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "inclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 207
    if-nez p1, :cond_1

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 210
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2, p3, p4}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 213
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 215
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queryParser/QueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method

.method protected getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 193
    if-nez p1, :cond_1

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 196
    new-instance v2, Lorg/apache/lucene/search/BooleanClause;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->fields:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, p2}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/queryParser/MultiFieldQueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 201
    .end local v0    # "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    .end local v1    # "i":I
    :goto_1
    return-object v2

    :cond_1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/queryParser/QueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_1
.end method
