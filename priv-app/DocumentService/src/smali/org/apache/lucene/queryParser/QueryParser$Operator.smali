.class public final enum Lorg/apache/lucene/queryParser/QueryParser$Operator;
.super Ljava/lang/Enum;
.source "QueryParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queryParser/QueryParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Operator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/queryParser/QueryParser$Operator;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/queryParser/QueryParser$Operator;

.field public static final enum AND:Lorg/apache/lucene/queryParser/QueryParser$Operator;

.field public static final enum OR:Lorg/apache/lucene/queryParser/QueryParser$Operator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    new-instance v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;

    const-string/jumbo v1, "OR"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryParser/QueryParser$Operator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;->OR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    new-instance v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;

    const-string/jumbo v1, "AND"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/queryParser/QueryParser$Operator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;->AND:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/queryParser/QueryParser$Operator;

    sget-object v1, Lorg/apache/lucene/queryParser/QueryParser$Operator;->OR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/queryParser/QueryParser$Operator;->AND:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;->$VALUES:[Lorg/apache/lucene/queryParser/QueryParser$Operator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/queryParser/QueryParser$Operator;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 175
    const-class v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/queryParser/QueryParser$Operator;
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;->$VALUES:[Lorg/apache/lucene/queryParser/QueryParser$Operator;

    invoke-virtual {v0}, [Lorg/apache/lucene/queryParser/QueryParser$Operator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/queryParser/QueryParser$Operator;

    return-object v0
.end method
