.class public Lorg/apache/lucene/queryParser/QueryParserTokenManager;
.super Ljava/lang/Object;
.source "QueryParserTokenManager.java"

# interfaces
.implements Lorg/apache/lucene/queryParser/QueryParserConstants;


# static fields
.field static final jjbitVec0:[J

.field static final jjbitVec1:[J

.field static final jjbitVec3:[J

.field static final jjbitVec4:[J

.field public static final jjnewLexState:[I

.field static final jjnextStates:[I

.field public static final jjstrLiteralImages:[Ljava/lang/String;

.field static final jjtoSkip:[J

.field static final jjtoToken:[J

.field public static final lexStateNames:[Ljava/lang/String;


# instance fields
.field protected curChar:C

.field curLexState:I

.field public debugStream:Ljava/io/PrintStream;

.field defaultLexState:I

.field protected input_stream:Lorg/apache/lucene/queryParser/CharStream;

.field jjmatchedKind:I

.field jjmatchedPos:I

.field jjnewStateCnt:I

.field jjround:I

.field private final jjrounds:[I

.field private final jjstateSet:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 97
    new-array v0, v4, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec0:[J

    .line 100
    new-array v0, v4, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec1:[J

    .line 103
    new-array v0, v4, [J

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec3:[J

    .line 106
    new-array v0, v4, [J

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec4:[J

    .line 1005
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnextStates:[I

    .line 1047
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v5

    aput-object v3, v0, v6

    aput-object v3, v0, v7

    const/4 v1, 0x3

    aput-object v3, v0, v1

    aput-object v3, v0, v4

    const/4 v1, 0x5

    aput-object v3, v0, v1

    const/4 v1, 0x6

    aput-object v3, v0, v1

    const/4 v1, 0x7

    aput-object v3, v0, v1

    const/16 v1, 0x8

    aput-object v3, v0, v1

    const/16 v1, 0x9

    aput-object v3, v0, v1

    const/16 v1, 0xa

    aput-object v3, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    aput-object v3, v0, v1

    const/16 v1, 0x13

    aput-object v3, v0, v1

    const/16 v1, 0x14

    aput-object v3, v0, v1

    const/16 v1, 0x15

    aput-object v3, v0, v1

    const/16 v1, 0x16

    aput-object v3, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "TO"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    aput-object v3, v0, v1

    const/16 v1, 0x1d

    aput-object v3, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "TO"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "}"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    aput-object v3, v0, v1

    const/16 v1, 0x21

    aput-object v3, v0, v1

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    .line 1053
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "Boost"

    aput-object v1, v0, v5

    const-string/jumbo v1, "RangeEx"

    aput-object v1, v0, v6

    const-string/jumbo v1, "RangeIn"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string/jumbo v2, "DEFAULT"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->lexStateNames:[Ljava/lang/String;

    .line 1061
    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewLexState:[I

    .line 1065
    new-array v0, v6, [J

    const-wide v2, 0x3ffffff01L

    aput-wide v2, v0, v5

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjtoToken:[J

    .line 1068
    new-array v0, v6, [J

    const-wide/16 v2, 0x80

    aput-wide v2, v0, v5

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjtoSkip:[J

    return-void

    .line 97
    :array_0
    .array-data 8
        0x1
        0x0
        0x0
        0x0
    .end array-data

    .line 100
    :array_1
    .array-data 8
        -0x2
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 103
    :array_2
    .array-data 8
        0x0
        0x0
        -0x1
        -0x1
    .end array-data

    .line 106
    :array_3
    .array-data 8
        -0x1000000000002L
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 1005
    :array_4
    .array-data 4
        0xf
        0x10
        0x12
        0x1d
        0x20
        0x17
        0x21
        0x1e
        0x14
        0x15
        0x20
        0x17
        0x21
        0x1f
        0x22
        0x1b
        0x2
        0x4
        0x5
        0x0
        0x1
    .end array-data

    .line 1061
    :array_5
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x0
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x2
        0x1
        0x3
        -0x1
        0x3
        -0x1
        -0x1
        -0x1
        0x3
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/queryParser/CharStream;)V
    .locals 2
    .param p1, "stream"    # Lorg/apache/lucene/queryParser/CharStream;

    .prologue
    const/4 v1, 0x3

    .line 1076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->debugStream:Ljava/io/PrintStream;

    .line 1072
    const/16 v0, 0x24

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjrounds:[I

    .line 1073
    const/16 v0, 0x48

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    .line 1142
    iput v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    .line 1143
    iput v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->defaultLexState:I

    .line 1077
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    .line 1078
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/queryParser/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryParser/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 1082
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;-><init>(Lorg/apache/lucene/queryParser/CharStream;)V

    .line 1083
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->SwitchTo(I)V

    .line 1084
    return-void
.end method

.method private ReInitRounds()V
    .locals 4

    .prologue
    .line 1097
    const v2, -0x7fffffff

    iput v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    .line 1098
    const/16 v0, 0x24

    .local v0, "i":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-lez v1, :cond_0

    .line 1099
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjrounds:[I

    const/high16 v3, -0x80000000

    aput v3, v2, v0

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 1100
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_0
    return-void
.end method

.method private jjAddStates(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1244
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    iget v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    sget-object v3, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnextStates:[I

    aget v3, v3, p1

    aput v3, v1, v2

    .line 1245
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 1246
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private static final jjCanMove_0(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const/4 v0, 0x0

    .line 1011
    packed-switch p0, :pswitch_data_0

    .line 1016
    :cond_0
    :goto_0
    return v0

    .line 1014
    :pswitch_0
    sget-object v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec0:[J

    aget-wide v2, v1, p2

    and-long/2addr v2, p5

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1011
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_1(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1021
    packed-switch p0, :pswitch_data_0

    .line 1026
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec1:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return v0

    .line 1024
    :pswitch_0
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1028
    goto :goto_0

    .line 1021
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_2(IIIJJ)Z
    .locals 7
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1033
    sparse-switch p0, :sswitch_data_0

    .line 1040
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec4:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1042
    :cond_0
    :goto_0
    return v0

    .line 1036
    :sswitch_0
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1038
    :sswitch_1
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjbitVec1:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1042
    goto :goto_0

    .line 1033
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x30 -> :sswitch_1
    .end sparse-switch
.end method

.method private jjCheckNAdd(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 1235
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjrounds:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    if-eq v0, v1, :cond_0

    .line 1237
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    aput p1, v0, v1

    .line 1238
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjrounds:[I

    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    aput v1, v0, p1

    .line 1240
    :cond_0
    return-void
.end method

.method private jjCheckNAddStates(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1256
    :goto_0
    sget-object v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnextStates:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 1257
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 1258
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private jjCheckNAddTwoStates(II)V
    .locals 0
    .param p1, "state1"    # I
    .param p2, "state2"    # I

    .prologue
    .line 1249
    invoke-direct {p0, p1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 1250
    invoke-direct {p0, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 1251
    return-void
.end method

.method private jjMoveNfa_0(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 733
    const/4 v14, 0x0

    .line 734
    .local v14, "startsAt":I
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    .line 735
    const/4 v4, 0x1

    .line 736
    .local v4, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 737
    const v7, 0x7fffffff

    .line 740
    .local v7, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 741
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInitRounds()V

    .line 742
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    .line 744
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shl-long v8, v16, v15

    .line 747
    .local v8, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    packed-switch v15, :pswitch_data_0

    .line 769
    :cond_2
    :goto_1
    if-ne v4, v14, :cond_1

    .line 797
    .end local v8    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v7, v15, :cond_3

    .line 799
    move-object/from16 v0, p0

    iput v7, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 800
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 801
    const v7, 0x7fffffff

    .line 803
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 804
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x3

    if-ne v4, v14, :cond_a

    .line 807
    :goto_3
    return p2

    .line 750
    .restart local v8    # "l":J
    :pswitch_0
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 752
    const/16 v15, 0x19

    if-le v7, v15, :cond_4

    .line 753
    const/16 v7, 0x19

    .line 754
    :cond_4
    const/16 v15, 0x13

    const/16 v16, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_1

    .line 757
    :pswitch_1
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 758
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 761
    :pswitch_2
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v8

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 763
    const/16 v15, 0x19

    if-le v7, v15, :cond_5

    .line 764
    const/16 v7, 0x19

    .line 765
    :cond_5
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 771
    .end local v8    # "l":J
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 773
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 776
    .restart local v8    # "l":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 780
    if-ne v4, v14, :cond_7

    goto/16 :goto_2

    .line 784
    .end local v8    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 785
    .local v3, "hiByte":I
    shr-int/lit8 v5, v3, 0x6

    .line 786
    .local v5, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v10, v16, v15

    .line 787
    .local v10, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v6, v15, 0x6

    .line 788
    .local v6, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 791
    .local v12, "l2":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v4, v4, -0x1

    aget v15, v15, v4

    .line 795
    if-ne v4, v14, :cond_9

    goto/16 :goto_2

    .line 806
    .end local v3    # "hiByte":I
    .end local v5    # "i1":I
    .end local v6    # "i2":I
    .end local v10    # "l1":J
    .end local v12    # "l2":J
    :cond_a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 807
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 747
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private jjMoveNfa_1(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 591
    const/4 v14, 0x0

    .line 592
    .local v14, "startsAt":I
    const/4 v15, 0x7

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    .line 593
    const/4 v10, 0x1

    .line 594
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 595
    const v11, 0x7fffffff

    .line 598
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 599
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInitRounds()V

    .line 600
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 602
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 605
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 647
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 714
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 716
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 717
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 718
    const v11, 0x7fffffff

    .line 720
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 721
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x7

    if-ne v10, v14, :cond_12

    .line 724
    :goto_3
    return p2

    .line 608
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_5

    .line 610
    const/16 v15, 0x21

    if-le v11, v15, :cond_4

    .line 611
    const/16 v11, 0x21

    .line 612
    :cond_4
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 614
    :cond_5
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_6

    .line 616
    const/4 v15, 0x7

    if-le v11, v15, :cond_2

    .line 617
    const/4 v11, 0x7

    goto :goto_1

    .line 619
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 620
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 623
    :pswitch_2
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 624
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 627
    :pswitch_3
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 628
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 631
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 632
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 635
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x20

    if-le v11, v15, :cond_2

    .line 636
    const/16 v11, 0x20

    goto/16 :goto_1

    .line 639
    :pswitch_6
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 641
    const/16 v15, 0x21

    if-le v11, v15, :cond_7

    .line 642
    const/16 v11, 0x21

    .line 643
    :cond_7
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 649
    .end local v12    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    .line 651
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 654
    .restart local v12    # "l":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 673
    :cond_a
    :goto_4
    :pswitch_7
    if-ne v10, v14, :cond_9

    goto/16 :goto_2

    .line 658
    :pswitch_8
    const-wide v16, -0x2000000000000001L    # -2.681561585988519E154

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_a

    .line 660
    const/16 v15, 0x21

    if-le v11, v15, :cond_b

    .line 661
    const/16 v11, 0x21

    .line 662
    :cond_b
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 665
    :pswitch_9
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 668
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 669
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto :goto_4

    .line 677
    .end local v12    # "l":J
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 678
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 679
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 680
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 681
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 684
    .local v8, "l2":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 712
    :cond_e
    :goto_5
    if-ne v10, v14, :cond_d

    goto/16 :goto_2

    .line 687
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 689
    const/4 v15, 0x7

    if-le v11, v15, :cond_f

    .line 690
    const/4 v11, 0x7

    .line 692
    :cond_f
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 694
    const/16 v15, 0x21

    if-le v11, v15, :cond_10

    .line 695
    const/16 v11, 0x21

    .line 696
    :cond_10
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 700
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 701
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 704
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 706
    const/16 v15, 0x21

    if-le v11, v15, :cond_11

    .line 707
    const/16 v11, 0x21

    .line 708
    :cond_11
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 723
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_12
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 724
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 605
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 654
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 684
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveNfa_2(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 869
    const/4 v14, 0x0

    .line 870
    .local v14, "startsAt":I
    const/4 v15, 0x7

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    .line 871
    const/4 v10, 0x1

    .line 872
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 873
    const v11, 0x7fffffff

    .line 876
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 877
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInitRounds()V

    .line 878
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 880
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 883
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 925
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 992
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 994
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 995
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 996
    const v11, 0x7fffffff

    .line 998
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 999
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x7

    if-ne v10, v14, :cond_12

    .line 1002
    :goto_3
    return p2

    .line 886
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_5

    .line 888
    const/16 v15, 0x1d

    if-le v11, v15, :cond_4

    .line 889
    const/16 v11, 0x1d

    .line 890
    :cond_4
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    .line 892
    :cond_5
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_6

    .line 894
    const/4 v15, 0x7

    if-le v11, v15, :cond_2

    .line 895
    const/4 v11, 0x7

    goto :goto_1

    .line 897
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 898
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 901
    :pswitch_2
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 902
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 905
    :pswitch_3
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 906
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 909
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 910
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 913
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x1c

    if-le v11, v15, :cond_2

    .line 914
    const/16 v11, 0x1c

    goto/16 :goto_1

    .line 917
    :pswitch_6
    const-wide v16, -0x100000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 919
    const/16 v15, 0x1d

    if-le v11, v15, :cond_7

    .line 920
    const/16 v11, 0x1d

    .line 921
    :cond_7
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 927
    .end local v12    # "l":J
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    .line 929
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 932
    .restart local v12    # "l":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 951
    :cond_a
    :goto_4
    :pswitch_7
    if-ne v10, v14, :cond_9

    goto/16 :goto_2

    .line 936
    :pswitch_8
    const-wide/32 v16, -0x20000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_a

    .line 938
    const/16 v15, 0x1d

    if-le v11, v15, :cond_b

    .line 939
    const/16 v11, 0x1d

    .line 940
    :cond_b
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 943
    :pswitch_9
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 946
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 947
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto :goto_4

    .line 955
    .end local v12    # "l":J
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 956
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 957
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 958
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 959
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 962
    .local v8, "l2":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 990
    :cond_e
    :goto_5
    if-ne v10, v14, :cond_d

    goto/16 :goto_2

    .line 965
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 967
    const/4 v15, 0x7

    if-le v11, v15, :cond_f

    .line 968
    const/4 v11, 0x7

    .line 970
    :cond_f
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 972
    const/16 v15, 0x1d

    if-le v11, v15, :cond_10

    .line 973
    const/16 v11, 0x1d

    .line 974
    :cond_10
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 978
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 979
    const/16 v15, 0x10

    const/16 v16, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 982
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 984
    const/16 v15, 0x1d

    if-le v11, v15, :cond_11

    .line 985
    const/16 v11, 0x1d

    .line 986
    :cond_11
    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_5

    .line 1001
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_12
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1002
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 883
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 932
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 962
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveNfa_3(II)I
    .locals 20
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 111
    const/4 v14, 0x0

    .line 112
    .local v14, "startsAt":I
    const/16 v15, 0x24

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    .line 113
    const/4 v10, 0x1

    .line 114
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 115
    const v11, 0x7fffffff

    .line 118
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 119
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInitRounds()V

    .line 120
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_14

    .line 122
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shl-long v12, v16, v15

    .line 125
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 257
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 519
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 521
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 522
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 523
    const v11, 0x7fffffff

    .line 525
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 526
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x24

    if-ne v10, v14, :cond_37

    .line 529
    :goto_3
    return p2

    .line 129
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v16, -0x400030700002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 131
    const/16 v15, 0x16

    if-le v11, v15, :cond_4

    .line 132
    const/16 v11, 0x16

    .line 133
    :cond_4
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 136
    :pswitch_2
    const-wide v16, -0x4002b0700002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_9

    .line 138
    const/16 v15, 0x16

    if-le v11, v15, :cond_5

    .line 139
    const/16 v11, 0x16

    .line 140
    :cond_5
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 154
    :cond_6
    :goto_4
    const-wide v16, 0x7bffd0f8ffffd9ffL    # 1.937873929427167E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_c

    .line 156
    const/16 v15, 0x13

    if-le v11, v15, :cond_7

    .line 157
    const/16 v11, 0x13

    .line 158
    :cond_7
    const/4 v15, 0x3

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    .line 165
    :cond_8
    :goto_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 166
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4

    aput v17, v15, v16

    goto/16 :goto_1

    .line 142
    :cond_9
    const-wide v16, 0x100002600L    # 2.122000597E-314

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_a

    .line 144
    const/4 v15, 0x7

    if-le v11, v15, :cond_6

    .line 145
    const/4 v11, 0x7

    goto :goto_4

    .line 147
    :cond_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 148
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 149
    :cond_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 151
    const/16 v15, 0xa

    if-le v11, v15, :cond_6

    .line 152
    const/16 v11, 0xa

    goto :goto_4

    .line 160
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 162
    const/16 v15, 0x15

    if-le v11, v15, :cond_8

    .line 163
    const/16 v11, 0x15

    goto :goto_5

    .line 169
    :pswitch_3
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x8

    if-le v11, v15, :cond_2

    .line 170
    const/16 v11, 0x8

    goto/16 :goto_1

    .line 173
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 174
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4

    aput v17, v15, v16

    goto/16 :goto_1

    .line 177
    :pswitch_5
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0xa

    if-le v11, v15, :cond_2

    .line 178
    const/16 v11, 0xa

    goto/16 :goto_1

    .line 181
    :pswitch_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 182
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 185
    :pswitch_7
    const-wide v16, -0x400000001L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 186
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 189
    :pswitch_8
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 192
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x12

    if-le v11, v15, :cond_2

    .line 193
    const/16 v11, 0x12

    goto/16 :goto_1

    .line 196
    :pswitch_a
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 198
    const/16 v15, 0x14

    if-le v11, v15, :cond_d

    .line 199
    const/16 v11, 0x14

    .line 200
    :cond_d
    const/16 v15, 0x8

    const/16 v16, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 203
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 204
    const/16 v15, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 207
    :pswitch_c
    const-wide/high16 v16, 0x3ff000000000000L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 209
    const/16 v15, 0x14

    if-le v11, v15, :cond_e

    .line 210
    const/16 v11, 0x14

    .line 211
    :cond_e
    const/16 v15, 0x16

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 214
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x15

    if-le v11, v15, :cond_2

    .line 215
    const/16 v11, 0x15

    goto/16 :goto_1

    .line 218
    :pswitch_e
    const-wide v16, -0x4002b0700002601L

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 220
    const/16 v15, 0x16

    if-le v11, v15, :cond_f

    .line 221
    const/16 v11, 0x16

    .line 222
    :cond_f
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 225
    :pswitch_f
    const/16 v15, 0x16

    if-le v11, v15, :cond_10

    .line 226
    const/16 v11, 0x16

    .line 227
    :cond_10
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 230
    :pswitch_10
    const-wide v16, 0x7bffd0f8ffffd9ffL    # 1.937873929427167E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 232
    const/16 v15, 0x13

    if-le v11, v15, :cond_11

    .line 233
    const/16 v11, 0x13

    .line 234
    :cond_11
    const/4 v15, 0x3

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 237
    :pswitch_11
    const-wide v16, 0x7bfff8f8ffffd9ffL    # 1.9473908376414249E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 239
    const/16 v15, 0x13

    if-le v11, v15, :cond_12

    .line 240
    const/16 v11, 0x13

    .line 241
    :cond_12
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 244
    :pswitch_12
    const/16 v15, 0x13

    if-le v11, v15, :cond_13

    .line 245
    const/16 v11, 0x13

    .line 246
    :cond_13
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 249
    :pswitch_13
    const-wide v16, 0x7bfff8f8ffffd9ffL    # 1.9473908376414249E289

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_2

    .line 250
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 253
    :pswitch_14
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 259
    .end local v12    # "l":J
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_2a

    .line 261
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v12, v16, v15

    .line 264
    .restart local v12    # "l":J
    :cond_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 427
    :cond_16
    :goto_6
    :pswitch_15
    if-ne v10, v14, :cond_15

    goto/16 :goto_2

    .line 267
    :pswitch_16
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_18

    .line 269
    const/16 v15, 0x16

    if-le v11, v15, :cond_17

    .line 270
    const/16 v11, 0x16

    .line 271
    :cond_17
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_6

    .line 273
    :cond_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 274
    const/16 v15, 0x1b

    const/16 v16, 0x1b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_6

    .line 277
    :pswitch_17
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1d

    .line 279
    const/16 v15, 0x13

    if-le v11, v15, :cond_19

    .line 280
    const/16 v11, 0x13

    .line 281
    :cond_19
    const/4 v15, 0x3

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    .line 291
    :cond_1a
    :goto_7
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_1c

    .line 293
    const/16 v15, 0x16

    if-le v11, v15, :cond_1b

    .line 294
    const/16 v11, 0x16

    .line 295
    :cond_1b
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 297
    :cond_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_20

    .line 298
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_6

    .line 283
    :cond_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_1e

    .line 284
    const/16 v15, 0xd

    const/16 v16, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 285
    :cond_1e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7e

    move/from16 v0, v16

    if-ne v15, v0, :cond_1a

    .line 287
    const/16 v15, 0x14

    if-le v11, v15, :cond_1f

    .line 288
    const/16 v11, 0x14

    .line 289
    :cond_1f
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x14

    aput v17, v15, v16

    goto :goto_7

    .line 299
    :cond_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 300
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_6

    .line 301
    :cond_21
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 302
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_6

    .line 303
    :cond_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 304
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2

    aput v17, v15, v16

    goto/16 :goto_6

    .line 307
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x44

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    const/16 v15, 0x8

    if-le v11, v15, :cond_16

    .line 308
    const/16 v11, 0x8

    goto/16 :goto_6

    .line 311
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 312
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1

    aput v17, v15, v16

    goto/16 :goto_6

    .line 315
    :pswitch_1a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x41

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 316
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2

    aput v17, v15, v16

    goto/16 :goto_6

    .line 319
    :pswitch_1b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x52

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    const/16 v15, 0x9

    if-le v11, v15, :cond_16

    .line 320
    const/16 v11, 0x9

    goto/16 :goto_6

    .line 323
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 324
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_6

    .line 327
    :pswitch_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    const/16 v15, 0x9

    if-le v11, v15, :cond_16

    .line 328
    const/16 v11, 0x9

    goto/16 :goto_6

    .line 331
    :pswitch_1e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 332
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_6

    .line 335
    :pswitch_1f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x54

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    const/16 v15, 0xa

    if-le v11, v15, :cond_16

    .line 336
    const/16 v11, 0xa

    goto/16 :goto_6

    .line 339
    :pswitch_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4f

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 340
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xa

    aput v17, v15, v16

    goto/16 :goto_6

    .line 343
    :pswitch_21
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x4e

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 344
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_6

    .line 347
    :pswitch_22
    const-wide/32 v16, -0x10000001

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_16

    .line 348
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 351
    :pswitch_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 352
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_6

    .line 355
    :pswitch_24
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 358
    :pswitch_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x7e

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 360
    const/16 v15, 0x14

    if-le v11, v15, :cond_23

    .line 361
    const/16 v11, 0x14

    .line 362
    :cond_23
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x14

    aput v17, v15, v16

    goto/16 :goto_6

    .line 365
    :pswitch_26
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_16

    .line 367
    const/16 v15, 0x16

    if-le v11, v15, :cond_24

    .line 368
    const/16 v11, 0x16

    .line 369
    :cond_24
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 372
    :pswitch_27
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_16

    .line 374
    const/16 v15, 0x16

    if-le v11, v15, :cond_25

    .line 375
    const/16 v11, 0x16

    .line 376
    :cond_25
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 379
    :pswitch_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 380
    const/16 v15, 0x1b

    const/16 v16, 0x1b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 383
    :pswitch_29
    const/16 v15, 0x16

    if-le v11, v15, :cond_26

    .line 384
    const/16 v11, 0x16

    .line 385
    :cond_26
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 388
    :pswitch_2a
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_16

    .line 390
    const/16 v15, 0x13

    if-le v11, v15, :cond_27

    .line 391
    const/16 v11, 0x13

    .line 392
    :cond_27
    const/4 v15, 0x3

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 395
    :pswitch_2b
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_16

    .line 397
    const/16 v15, 0x13

    if-le v11, v15, :cond_28

    .line 398
    const/16 v11, 0x13

    .line 399
    :cond_28
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 402
    :pswitch_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 403
    const/16 v15, 0x1f

    const/16 v16, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 406
    :pswitch_2d
    const/16 v15, 0x13

    if-le v11, v15, :cond_29

    .line 407
    const/16 v11, 0x13

    .line 408
    :cond_29
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 411
    :pswitch_2e
    const-wide v16, -0x6800000078000001L    # -4.383617718201673E-193

    and-long v16, v16, v12

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_16

    .line 412
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 415
    :pswitch_2f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 416
    const/16 v15, 0x22

    const/16 v16, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_6

    .line 419
    :pswitch_30
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 422
    :pswitch_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 423
    const/16 v15, 0xd

    const/16 v16, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_6

    .line 431
    .end local v12    # "l":J
    :cond_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    shr-int/lit8 v3, v15, 0x8

    .line 432
    .local v3, "hiByte":I
    shr-int/lit8 v4, v3, 0x6

    .line 433
    .local v4, "i1":I
    const-wide/16 v16, 0x1

    and-int/lit8 v15, v3, 0x3f

    shl-long v6, v16, v15

    .line 434
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v5, v15, 0x6

    .line 435
    .local v5, "i2":I
    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    and-int/lit8 v15, v15, 0x3f

    shl-long v8, v16, v15

    .line 438
    .local v8, "l2":J
    :cond_2b
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 517
    :cond_2c
    :goto_8
    if-ne v10, v14, :cond_2b

    goto/16 :goto_2

    .line 442
    :sswitch_0
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 444
    const/16 v15, 0x16

    if-le v11, v15, :cond_2d

    .line 445
    const/16 v11, 0x16

    .line 446
    :cond_2d
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_8

    .line 449
    :sswitch_1
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2e

    .line 451
    const/4 v15, 0x7

    if-le v11, v15, :cond_2e

    .line 452
    const/4 v11, 0x7

    .line 454
    :cond_2e
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_30

    .line 456
    const/16 v15, 0x16

    if-le v11, v15, :cond_2f

    .line 457
    const/16 v11, 0x16

    .line 458
    :cond_2f
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 460
    :cond_30
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 462
    const/16 v15, 0x13

    if-le v11, v15, :cond_31

    .line 463
    const/16 v11, 0x13

    .line 464
    :cond_31
    const/4 v15, 0x3

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_8

    .line 469
    :sswitch_2
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 470
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_8

    .line 473
    :sswitch_3
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 475
    const/16 v15, 0x16

    if-le v11, v15, :cond_32

    .line 476
    const/16 v11, 0x16

    .line 477
    :cond_32
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_8

    .line 480
    :sswitch_4
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 482
    const/16 v15, 0x16

    if-le v11, v15, :cond_33

    .line 483
    const/16 v11, 0x16

    .line 484
    :cond_33
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 487
    :sswitch_5
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 489
    const/16 v15, 0x13

    if-le v11, v15, :cond_34

    .line 490
    const/16 v11, 0x13

    .line 491
    :cond_34
    const/4 v15, 0x3

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_8

    .line 494
    :sswitch_6
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 496
    const/16 v15, 0x13

    if-le v11, v15, :cond_35

    .line 497
    const/16 v11, 0x13

    .line 498
    :cond_35
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 501
    :sswitch_7
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 503
    const/16 v15, 0x13

    if-le v11, v15, :cond_36

    .line 504
    const/16 v11, 0x13

    .line 505
    :cond_36
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_8

    .line 508
    :sswitch_8
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_2(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 509
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_8

    .line 512
    :sswitch_9
    invoke-static/range {v3 .. v9}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_2c

    .line 513
    const/16 v15, 0xa

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_8

    .line 528
    .end local v3    # "hiByte":I
    .end local v4    # "i1":I
    .end local v5    # "i2":I
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    :cond_37
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v15}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 529
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_1
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 264
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_15
        :pswitch_15
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_15
        :pswitch_15
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_15
        :pswitch_25
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_16
    .end packed-switch

    .line 438
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xf -> :sswitch_2
        0x11 -> :sswitch_2
        0x18 -> :sswitch_3
        0x19 -> :sswitch_0
        0x1b -> :sswitch_4
        0x1c -> :sswitch_5
        0x1d -> :sswitch_6
        0x1f -> :sswitch_7
        0x20 -> :sswitch_8
        0x22 -> :sswitch_9
        0x24 -> :sswitch_0
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_0()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 729
    invoke-direct {p0, v0, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    return v0
.end method

.method private jjMoveStringLiteralDfa0_1()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 553
    iget-char v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 560
    invoke-direct {p0, v1, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    :goto_0
    return v0

    .line 556
    :sswitch_0
    const-wide/32 v0, 0x40000000

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 558
    :sswitch_1
    const/16 v0, 0x1f

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 553
    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_2()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 831
    iget-char v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 838
    invoke-direct {p0, v1, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    :goto_0
    return v0

    .line 834
    :sswitch_0
    const-wide/32 v0, 0x4000000

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 836
    :sswitch_1
    const/16 v0, 0x1b

    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 831
    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_3()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    iget-char v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 86
    invoke-direct {p0, v2, v2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_3(II)I

    move-result v0

    :goto_0
    return v0

    .line 68
    :sswitch_0
    const/16 v0, 0xd

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 70
    :sswitch_1
    const/16 v0, 0xe

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 72
    :sswitch_2
    const/16 v0, 0x10

    const/16 v1, 0x24

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStartNfaWithStates_3(III)I

    move-result v0

    goto :goto_0

    .line 74
    :sswitch_3
    const/16 v0, 0xb

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 76
    :sswitch_4
    const/16 v0, 0xc

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 78
    :sswitch_5
    const/16 v0, 0xf

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 80
    :sswitch_6
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 82
    :sswitch_7
    const/16 v0, 0x11

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 84
    :sswitch_8
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 65
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2a -> :sswitch_2
        0x2b -> :sswitch_3
        0x2d -> :sswitch_4
        0x3a -> :sswitch_5
        0x5b -> :sswitch_6
        0x5e -> :sswitch_7
        0x7b -> :sswitch_8
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_1(J)I
    .locals 7
    .param p1, "active0"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 565
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    iget-char v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 579
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 566
    :catch_0
    move-exception v0

    .line 567
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 573
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/32 v2, 0x40000000

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 574
    const/16 v2, 0x1e

    const/4 v3, 0x6

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 570
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa1_2(J)I
    .locals 7
    .param p1, "active0"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 843
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 848
    iget-char v2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 857
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 844
    :catch_0
    move-exception v0

    .line 845
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 851
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/32 v2, 0x4000000

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 852
    const/16 v2, 0x1a

    const/4 v3, 0x6

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 848
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method private jjStartNfaWithStates_1(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 583
    iput p2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 584
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 585
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v1}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v1

    :goto_0
    return v1

    .line 586
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_2(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 861
    iput p2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 862
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 863
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v1}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 865
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_2(II)I

    move-result v1

    :goto_0
    return v1

    .line 864
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_3(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 91
    iput p2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 92
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 93
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v1}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_3(II)I

    move-result v1

    :goto_0
    return v1

    .line 94
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private final jjStartNfa_1(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 549
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_2(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 827
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_3(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjStopStringLiteralDfa_3(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveNfa_3(II)I

    move-result v0

    return v0
.end method

.method private jjStopAtPos(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "kind"    # I

    .prologue
    .line 59
    iput p2, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 60
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 61
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final jjStopStringLiteralDfa_1(IJ)I
    .locals 6
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v0, -0x1

    .line 534
    packed-switch p1, :pswitch_data_0

    .line 544
    :cond_0
    :goto_0
    return v0

    .line 537
    :pswitch_0
    const-wide/32 v2, 0x40000000

    and-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 539
    const/16 v0, 0x21

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 540
    const/4 v0, 0x6

    goto :goto_0

    .line 534
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_2(IJ)I
    .locals 6
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v0, -0x1

    .line 812
    packed-switch p1, :pswitch_data_0

    .line 822
    :cond_0
    :goto_0
    return v0

    .line 815
    :pswitch_0
    const-wide/32 v2, 0x4000000

    and-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 817
    const/16 v0, 0x1d

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 818
    const/4 v0, 0x6

    goto :goto_0

    .line 812
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_3(IJ)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 47
    .line 50
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public ReInit(Lorg/apache/lucene/queryParser/CharStream;)V
    .locals 1
    .param p1, "stream"    # Lorg/apache/lucene/queryParser/CharStream;

    .prologue
    .line 1089
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewStateCnt:I

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 1090
    iget v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->defaultLexState:I

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    .line 1091
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    .line 1092
    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInitRounds()V

    .line 1093
    return-void
.end method

.method public ReInit(Lorg/apache/lucene/queryParser/CharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/queryParser/CharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 1105
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInit(Lorg/apache/lucene/queryParser/CharStream;)V

    .line 1106
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->SwitchTo(I)V

    .line 1107
    return-void
.end method

.method public SwitchTo(I)V
    .locals 3
    .param p1, "lexState"    # I

    .prologue
    .line 1112
    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 1113
    :cond_0
    new-instance v0, Lorg/apache/lucene/queryParser/TokenMgrError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error: Ignoring invalid lexical state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". State unchanged."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/queryParser/TokenMgrError;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1115
    :cond_1
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    .line 1116
    return-void
.end method

.method public getNextToken()Lorg/apache/lucene/queryParser/Token;
    .locals 18

    .prologue
    .line 1153
    const/4 v10, 0x0

    .line 1160
    .local v10, "curPos":I
    :cond_0
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->BeginToken()C

    move-result v2

    move-object/from16 v0, p0

    iput-char v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1169
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    packed-switch v2, :pswitch_data_0

    .line 1192
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    const v4, 0x7fffffff

    if-eq v2, v4, :cond_4

    .line 1194
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v10, :cond_1

    .line 1195
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    sub-int v4, v10, v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryParser/CharStream;->backup(I)V

    .line 1196
    :cond_1
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjtoToken:[J

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    shr-int/lit8 v4, v4, 0x6

    aget-wide v8, v2, v4

    const-wide/16 v16, 0x1

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    and-int/lit8 v2, v2, 0x3f

    shl-long v16, v16, v2

    and-long v8, v8, v16

    const-wide/16 v16, 0x0

    cmp-long v2, v8, v16

    if-eqz v2, :cond_3

    .line 1198
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v13

    .line 1199
    .local v13, "matchedToken":Lorg/apache/lucene/queryParser/Token;
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 1200
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    :cond_2
    move-object v14, v13

    .line 1201
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryParser/Token;
    .local v14, "matchedToken":Lorg/apache/lucene/queryParser/Token;
    :goto_2
    return-object v14

    .line 1162
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryParser/Token;
    :catch_0
    move-exception v11

    .line 1164
    .local v11, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 1165
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjFillToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v13

    .restart local v13    # "matchedToken":Lorg/apache/lucene/queryParser/Token;
    move-object v14, v13

    .line 1166
    .end local v13    # "matchedToken":Lorg/apache/lucene/queryParser/Token;
    .restart local v14    # "matchedToken":Lorg/apache/lucene/queryParser/Token;
    goto :goto_2

    .line 1172
    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "matchedToken":Lorg/apache/lucene/queryParser/Token;
    :pswitch_0
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 1173
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 1174
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveStringLiteralDfa0_0()I

    move-result v10

    .line 1175
    goto :goto_1

    .line 1177
    :pswitch_1
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 1178
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 1179
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveStringLiteralDfa0_1()I

    move-result v10

    .line 1180
    goto/16 :goto_1

    .line 1182
    :pswitch_2
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 1183
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 1184
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveStringLiteralDfa0_2()I

    move-result v10

    .line 1185
    goto/16 :goto_1

    .line 1187
    :pswitch_3
    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    .line 1188
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedPos:I

    .line 1189
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjMoveStringLiteralDfa0_3()I

    move-result v10

    goto/16 :goto_1

    .line 1205
    :cond_3
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 1206
    sget-object v2, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjnewLexState:[I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    aget v2, v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    goto/16 :goto_0

    .line 1210
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->getEndLine()I

    move-result v5

    .line 1211
    .local v5, "error_line":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->getEndColumn()I

    move-result v6

    .line 1212
    .local v6, "error_column":I
    const/4 v7, 0x0

    .line 1213
    .local v7, "error_after":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1214
    .local v3, "EOFSeen":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->readChar()C

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryParser/CharStream;->backup(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1225
    :goto_3
    if-nez v3, :cond_5

    .line 1226
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lorg/apache/lucene/queryParser/CharStream;->backup(I)V

    .line 1227
    const/4 v2, 0x1

    if-gt v10, v2, :cond_9

    const-string/jumbo v7, ""

    .line 1229
    :cond_5
    :goto_4
    new-instance v2, Lorg/apache/lucene/queryParser/TokenMgrError;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curLexState:I

    move-object/from16 v0, p0

    iget-char v8, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/queryParser/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v2

    .line 1215
    :catch_1
    move-exception v12

    .line 1216
    .local v12, "e1":Ljava/io/IOException;
    const/4 v3, 0x1

    .line 1217
    const/4 v2, 0x1

    if-gt v10, v2, :cond_7

    const-string/jumbo v7, ""

    .line 1218
    :goto_5
    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v4, 0xa

    if-eq v2, v4, :cond_6

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->curChar:C

    const/16 v4, 0xd

    if-ne v2, v4, :cond_8

    .line 1219
    :cond_6
    add-int/lit8 v5, v5, 0x1

    .line 1220
    const/4 v6, 0x0

    goto :goto_3

    .line 1217
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    .line 1223
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1227
    .end local v12    # "e1":Ljava/io/IOException;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v2}, Lorg/apache/lucene/queryParser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 1169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected jjFillToken()Lorg/apache/lucene/queryParser/Token;
    .locals 9

    .prologue
    .line 1126
    sget-object v7, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    iget v8, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    aget-object v5, v7, v8

    .line 1127
    .local v5, "im":Ljava/lang/String;
    if-nez v5, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryParser/CharStream;->GetImage()Ljava/lang/String;

    move-result-object v2

    .line 1128
    .local v2, "curTokenImage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryParser/CharStream;->getBeginLine()I

    move-result v1

    .line 1129
    .local v1, "beginLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryParser/CharStream;->getBeginColumn()I

    move-result v0

    .line 1130
    .local v0, "beginColumn":I
    iget-object v7, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryParser/CharStream;->getEndLine()I

    move-result v4

    .line 1131
    .local v4, "endLine":I
    iget-object v7, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->input_stream:Lorg/apache/lucene/queryParser/CharStream;

    invoke-interface {v7}, Lorg/apache/lucene/queryParser/CharStream;->getEndColumn()I

    move-result v3

    .line 1132
    .local v3, "endColumn":I
    iget v7, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->jjmatchedKind:I

    invoke-static {v7, v2}, Lorg/apache/lucene/queryParser/Token;->newToken(ILjava/lang/String;)Lorg/apache/lucene/queryParser/Token;

    move-result-object v6

    .line 1134
    .local v6, "t":Lorg/apache/lucene/queryParser/Token;
    iput v1, v6, Lorg/apache/lucene/queryParser/Token;->beginLine:I

    .line 1135
    iput v4, v6, Lorg/apache/lucene/queryParser/Token;->endLine:I

    .line 1136
    iput v0, v6, Lorg/apache/lucene/queryParser/Token;->beginColumn:I

    .line 1137
    iput v3, v6, Lorg/apache/lucene/queryParser/Token;->endColumn:I

    .line 1139
    return-object v6

    .end local v0    # "beginColumn":I
    .end local v1    # "beginLine":I
    .end local v2    # "curTokenImage":Ljava/lang/String;
    .end local v3    # "endColumn":I
    .end local v4    # "endLine":I
    .end local v6    # "t":Lorg/apache/lucene/queryParser/Token;
    :cond_0
    move-object v2, v5

    .line 1127
    goto :goto_0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "ds"    # Ljava/io/PrintStream;

    .prologue
    .line 44
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->debugStream:Ljava/io/PrintStream;

    return-void
.end method
