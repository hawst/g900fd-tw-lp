.class public interface abstract Lorg/apache/lucene/queryParser/QueryParserConstants;
.super Ljava/lang/Object;
.source "QueryParserConstants.java"


# static fields
.field public static final AND:I = 0x8

.field public static final Boost:I = 0x0

.field public static final CARAT:I = 0x11

.field public static final COLON:I = 0xf

.field public static final DEFAULT:I = 0x3

.field public static final EOF:I = 0x0

.field public static final FUZZY_SLOP:I = 0x14

.field public static final LPAREN:I = 0xd

.field public static final MINUS:I = 0xc

.field public static final NOT:I = 0xa

.field public static final NUMBER:I = 0x19

.field public static final OR:I = 0x9

.field public static final PLUS:I = 0xb

.field public static final PREFIXTERM:I = 0x15

.field public static final QUOTED:I = 0x12

.field public static final RANGEEX_END:I = 0x1f

.field public static final RANGEEX_GOOP:I = 0x21

.field public static final RANGEEX_QUOTED:I = 0x20

.field public static final RANGEEX_START:I = 0x18

.field public static final RANGEEX_TO:I = 0x1e

.field public static final RANGEIN_END:I = 0x1b

.field public static final RANGEIN_GOOP:I = 0x1d

.field public static final RANGEIN_QUOTED:I = 0x1c

.field public static final RANGEIN_START:I = 0x17

.field public static final RANGEIN_TO:I = 0x1a

.field public static final RPAREN:I = 0xe

.field public static final RangeEx:I = 0x1

.field public static final RangeIn:I = 0x2

.field public static final STAR:I = 0x10

.field public static final TERM:I = 0x13

.field public static final WILDTERM:I = 0x16

.field public static final _ESCAPED_CHAR:I = 0x2

.field public static final _NUM_CHAR:I = 0x1

.field public static final _QUOTED_CHAR:I = 0x6

.field public static final _TERM_CHAR:I = 0x4

.field public static final _TERM_START_CHAR:I = 0x3

.field public static final _WHITESPACE:I = 0x5

.field public static final tokenImage:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 88
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "<EOF>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "<_NUM_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "<_ESCAPED_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "<_TERM_START_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "<_TERM_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "<_WHITESPACE>"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "<_QUOTED_CHAR>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "<token of kind 7>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "<AND>"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "<OR>"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "<NOT>"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "\"+\""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "\"-\""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "\"(\""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "\")\""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "\":\""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "\"*\""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "\"^\""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "<QUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "<TERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "<FUZZY_SLOP>"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "<PREFIXTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "<WILDTERM>"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "\"[\""

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "\"{\""

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "<NUMBER>"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "\"TO\""

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "\"]\""

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "<RANGEIN_QUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "<RANGEIN_GOOP>"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "\"TO\""

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "\"}\""

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "<RANGEEX_QUOTED>"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "<RANGEEX_GOOP>"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParserConstants;->tokenImage:[Ljava/lang/String;

    return-void
.end method
