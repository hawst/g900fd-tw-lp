.class public Lorg/apache/lucene/queryParser/QueryParser;
.super Ljava/lang/Object;
.source "QueryParser.java"

# interfaces
.implements Lorg/apache/lucene/queryParser/QueryParserConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queryParser/QueryParser$1;,
        Lorg/apache/lucene/queryParser/QueryParser$JJCalls;,
        Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;,
        Lorg/apache/lucene/queryParser/QueryParser$Operator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AND_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

.field private static final CONJ_AND:I = 0x1

.field private static final CONJ_NONE:I = 0x0

.field private static final CONJ_OR:I = 0x2

.field private static final MOD_NONE:I = 0x0

.field private static final MOD_NOT:I = 0xa

.field private static final MOD_REQ:I = 0xb

.field public static final OR_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

.field private static final getFieldQueryMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/queryParser/QueryParser;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final getFieldQueryWithQuotedMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/queryParser/QueryParser;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static jj_la1_0:[I

.field private static jj_la1_1:[I


# instance fields
.field allowLeadingWildcard:Z

.field analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private autoGeneratePhraseQueries:Z

.field dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

.field enablePositionIncrements:Z

.field field:Ljava/lang/String;

.field fieldToDateResolution:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/document/DateTools$Resolution;",
            ">;"
        }
    .end annotation
.end field

.field fuzzyMinSim:F

.field fuzzyPrefixLength:I

.field private final hasNewAPI:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

.field private jj_endpos:I

.field private jj_expentries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private jj_expentry:[I

.field private jj_gc:I

.field private jj_gen:I

.field private jj_kind:I

.field private jj_la:I

.field private final jj_la1:[I

.field private jj_lastpos:Lorg/apache/lucene/queryParser/Token;

.field private jj_lasttokens:[I

.field private final jj_ls:Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;

.field public jj_nt:Lorg/apache/lucene/queryParser/Token;

.field private jj_ntk:I

.field private jj_rescan:Z

.field private jj_scanpos:Lorg/apache/lucene/queryParser/Token;

.field locale:Ljava/util/Locale;

.field lowercaseExpandedTerms:Z

.field multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field private operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

.field phraseSlop:I

.field rangeCollator:Ljava/text/Collator;

.field public token:Lorg/apache/lucene/queryParser/Token;

.field public token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    const-class v0, Lorg/apache/lucene/queryParser/QueryParser;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/queryParser/QueryParser;->$assertionsDisabled:Z

    .line 128
    sget-object v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;->AND:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser;->AND_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    .line 130
    sget-object v0, Lorg/apache/lucene/queryParser/QueryParser$Operator;->OR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser;->OR_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    .line 158
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/queryParser/QueryParser;

    const-string/jumbo v4, "getFieldQuery"

    new-array v5, v7, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    aput-object v6, v5, v2

    const-class v6, Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQueryMethod:Lorg/apache/lucene/util/VirtualMethod;

    .line 162
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/queryParser/QueryParser;

    const-string/jumbo v4, "getFieldQuery"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    aput-object v6, v5, v2

    const-class v2, Ljava/lang/String;

    aput-object v2, v5, v1

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v1, v5, v7

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQueryWithQuotedMethod:Lorg/apache/lucene/util/VirtualMethod;

    .line 1650
    invoke-static {}, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1_init_0()V

    .line 1651
    invoke-static {}, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1_init_1()V

    .line 1652
    return-void

    :cond_0
    move v0, v2

    .line 115
    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/queryParser/CharStream;)V
    .locals 9
    .param p1, "stream"    # Lorg/apache/lucene/queryParser/CharStream;

    .prologue
    const/16 v8, 0x17

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    sget-object v1, Lorg/apache/lucene/queryParser/QueryParser;->OR_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    .line 135
    iput-boolean v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    .line 136
    sget-object v1, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 137
    iput-boolean v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->allowLeadingWildcard:Z

    .line 138
    iput-boolean v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    .line 142
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    .line 143
    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyMinSim:F

    .line 144
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyPrefixLength:I

    .line 145
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->locale:Ljava/util/Locale;

    .line 148
    iput-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 150
    iput-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    .line 154
    iput-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->rangeCollator:Ljava/text/Collator;

    .line 166
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v4, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQueryWithQuotedMethod:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v5, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQueryMethod:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v1, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v1

    if-ltz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->hasNewAPI:Z

    .line 1646
    new-array v1, v8, [I

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    .line 1659
    new-array v1, v2, [Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    .line 1660
    iput-boolean v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_rescan:Z

    .line 1661
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gc:I

    .line 1728
    new-instance v1, Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;

    invoke-direct {v1, v6}, Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryParser/QueryParser$1;)V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ls:Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;

    .line 1777
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    .line 1779
    iput v7, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_kind:I

    .line 1780
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lasttokens:[I

    .line 1665
    new-instance v1, Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-direct {v1, p1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;-><init>(Lorg/apache/lucene/queryParser/CharStream;)V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    .line 1666
    new-instance v1, Lorg/apache/lucene/queryParser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1667
    iput v7, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1668
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    .line 1669
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v8, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    aput v7, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    :cond_0
    move v1, v3

    .line 166
    goto :goto_0

    .line 1670
    .restart local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1671
    :cond_2
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/queryParser/QueryParserTokenManager;)V
    .locals 9
    .param p1, "tm"    # Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    .prologue
    const/16 v8, 0x17

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    sget-object v1, Lorg/apache/lucene/queryParser/QueryParser;->OR_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    .line 135
    iput-boolean v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    .line 136
    sget-object v1, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 137
    iput-boolean v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->allowLeadingWildcard:Z

    .line 138
    iput-boolean v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    .line 142
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    .line 143
    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyMinSim:F

    .line 144
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyPrefixLength:I

    .line 145
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->locale:Ljava/util/Locale;

    .line 148
    iput-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 150
    iput-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    .line 154
    iput-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->rangeCollator:Ljava/text/Collator;

    .line 166
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v4, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQueryWithQuotedMethod:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v5, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQueryMethod:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v1, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v1

    if-ltz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->hasNewAPI:Z

    .line 1646
    new-array v1, v8, [I

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    .line 1659
    new-array v1, v2, [Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    .line 1660
    iput-boolean v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_rescan:Z

    .line 1661
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gc:I

    .line 1728
    new-instance v1, Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;

    invoke-direct {v1, v6}, Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;-><init>(Lorg/apache/lucene/queryParser/QueryParser$1;)V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ls:Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;

    .line 1777
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    .line 1779
    iput v7, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_kind:I

    .line 1780
    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lasttokens:[I

    .line 1685
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    .line 1686
    new-instance v1, Lorg/apache/lucene/queryParser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1687
    iput v7, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1688
    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    .line 1689
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v8, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    aput v7, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    :cond_0
    move v1, v3

    .line 166
    goto :goto_0

    .line 1690
    .restart local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1691
    :cond_2
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 5
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 183
    new-instance v0, Lorg/apache/lucene/queryParser/FastCharStream;

    new-instance v1, Ljava/io/StringReader;

    const-string/jumbo v2, ""

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/queryParser/CharStream;)V

    .line 184
    iput-object p3, p0, Lorg/apache/lucene/queryParser/QueryParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 185
    iput-object p2, p0, Lorg/apache/lucene/queryParser/QueryParser;->field:Ljava/lang/String;

    .line 186
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_29:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iput-boolean v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    .line 191
    :goto_0
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    invoke-virtual {p0, v3}, Lorg/apache/lucene/queryParser/QueryParser;->setAutoGeneratePhraseQueries(Z)V

    .line 196
    :goto_1
    return-void

    .line 189
    :cond_0
    iput-boolean v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    goto :goto_0

    .line 194
    :cond_1
    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->setAutoGeneratePhraseQueries(Z)V

    goto :goto_1
.end method

.method private discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1062
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    new-array v7, v8, [C

    .line 1067
    .local v7, "output":[C
    const/4 v5, 0x0

    .line 1071
    .local v5, "length":I
    const/4 v4, 0x0

    .line 1075
    .local v4, "lastCharWasEscapeChar":Z
    const/4 v1, 0x0

    .line 1078
    .local v1, "codePointMultiplier":I
    const/4 v0, 0x0

    .line 1080
    .local v0, "codePoint":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v3, v8, :cond_5

    .line 1081
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1082
    .local v2, "curChar":C
    if-lez v1, :cond_1

    .line 1083
    invoke-static {v2}, Lorg/apache/lucene/queryParser/QueryParser;->hexToInt(C)I

    move-result v8

    mul-int/2addr v8, v1

    add-int/2addr v0, v8

    .line 1084
    ushr-int/lit8 v1, v1, 0x4

    .line 1085
    if-nez v1, :cond_0

    .line 1086
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "length":I
    .local v6, "length":I
    int-to-char v8, v0

    aput-char v8, v7, v5

    .line 1087
    const/4 v0, 0x0

    move v5, v6

    .line 1080
    .end local v6    # "length":I
    .restart local v5    # "length":I
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1089
    :cond_1
    if-eqz v4, :cond_3

    .line 1090
    const/16 v8, 0x75

    if-ne v2, v8, :cond_2

    .line 1092
    const/16 v1, 0x1000

    .line 1098
    :goto_2
    const/4 v4, 0x0

    goto :goto_1

    .line 1095
    :cond_2
    aput-char v2, v7, v5

    .line 1096
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1100
    :cond_3
    const/16 v8, 0x5c

    if-ne v2, v8, :cond_4

    .line 1101
    const/4 v4, 0x1

    goto :goto_1

    .line 1103
    :cond_4
    aput-char v2, v7, v5

    .line 1104
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1109
    .end local v2    # "curChar":C
    :cond_5
    if-lez v1, :cond_6

    .line 1110
    new-instance v8, Lorg/apache/lucene/queryParser/ParseException;

    const-string/jumbo v9, "Truncated unicode escape sequence."

    invoke-direct {v8, v9}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1113
    :cond_6
    if-eqz v4, :cond_7

    .line 1114
    new-instance v8, Lorg/apache/lucene/queryParser/ParseException;

    const-string/jumbo v9, "Term can not end with escape character."

    invoke-direct {v8, v9}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1117
    :cond_7
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v8, v7, v9, v5}, Ljava/lang/String;-><init>([CII)V

    return-object v8
.end method

.method public static escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x5c

    .line 1138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1139
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1140
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1142
    .local v0, "c":C
    if-eq v0, v4, :cond_0

    const/16 v3, 0x2b

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2d

    if-eq v0, v3, :cond_0

    const/16 v3, 0x21

    if-eq v0, v3, :cond_0

    const/16 v3, 0x28

    if-eq v0, v3, :cond_0

    const/16 v3, 0x29

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x5e

    if-eq v0, v3, :cond_0

    const/16 v3, 0x5b

    if-eq v0, v3, :cond_0

    const/16 v3, 0x5d

    if-eq v0, v3, :cond_0

    const/16 v3, 0x22

    if-eq v0, v3, :cond_0

    const/16 v3, 0x7b

    if-eq v0, v3, :cond_0

    const/16 v3, 0x7d

    if-eq v0, v3, :cond_0

    const/16 v3, 0x7e

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x7c

    if-eq v0, v3, :cond_0

    const/16 v3, 0x26

    if-ne v0, v3, :cond_1

    .line 1145
    :cond_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1147
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1149
    .end local v0    # "c":C
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static final hexToInt(C)I
    .locals 3
    .param p0, "c"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1122
    const/16 v0, 0x30

    if-gt v0, p0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 1123
    add-int/lit8 v0, p0, -0x30

    .line 1127
    :goto_0
    return v0

    .line 1124
    :cond_0
    const/16 v0, 0x61

    if-gt v0, p0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    .line 1125
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 1126
    :cond_1
    const/16 v0, 0x41

    if-gt v0, p0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    .line 1127
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 1129
    :cond_2
    new-instance v0, Lorg/apache/lucene/queryParser/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "None-hex character in unicode escape sequence: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private jj_2_1(I)Z
    .locals 4
    .param p1, "xla"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1608
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la:I

    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryParser/Token;

    .line 1609
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1611
    :goto_0
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_save(II)V

    .line 1610
    return v1

    :cond_0
    move v1, v2

    .line 1609
    goto :goto_0

    .line 1610
    :catch_0
    move-exception v0

    .local v0, "ls":Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;
    goto :goto_0

    .line 1611
    .end local v0    # "ls":Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_save(II)V

    throw v1
.end method

.method private jj_3R_2()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1615
    const/16 v1, 0x13

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1617
    :cond_0
    :goto_0
    return v0

    .line 1616
    :cond_1
    const/16 v1, 0xf

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1617
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3R_3()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1631
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1633
    :cond_0
    :goto_0
    return v0

    .line 1632
    :cond_1
    const/16 v1, 0xf

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_scan_token(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1633
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jj_3_1()Z
    .locals 2

    .prologue
    .line 1622
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    .line 1623
    .local v0, "xsp":Lorg/apache/lucene/queryParser/Token;
    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_3R_2()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1624
    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    .line 1625
    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_3R_3()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1627
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private jj_add_error_token(II)V
    .locals 6
    .param p1, "kind"    # I
    .param p2, "pos"    # I

    .prologue
    .line 1784
    const/16 v3, 0x64

    if-lt p2, v3, :cond_1

    .line 1806
    :cond_0
    :goto_0
    return-void

    .line 1785
    :cond_1
    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    add-int/lit8 v3, v3, 0x1

    if-ne p2, v3, :cond_2

    .line 1786
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lasttokens:[I

    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    aput p1, v3, v4

    goto :goto_0

    .line 1787
    :cond_2
    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    if-eqz v3, :cond_0

    .line 1788
    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    .line 1789
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    if-ge v0, v3, :cond_3

    .line 1790
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lasttokens:[I

    aget v4, v4, v0

    aput v4, v3, v0

    .line 1789
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1792
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1793
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    move-object v2, v3

    check-cast v2, [I

    .line 1794
    .local v2, "oldentry":[I
    array-length v3, v2

    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    array-length v4, v4

    if-ne v3, v4, :cond_4

    .line 1795
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 1796
    aget v3, v2, v0

    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    aget v4, v4, v0

    if-ne v3, v4, :cond_4

    .line 1795
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1800
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1804
    .end local v2    # "oldentry":[I
    :cond_6
    if-eqz p2, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lasttokens:[I

    iput p2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    add-int/lit8 v4, p2, -0x1

    aput p1, v3, v4

    goto :goto_0
.end method

.method private jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;
    .locals 5
    .param p1, "kind"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1705
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .local v2, "oldToken":Lorg/apache/lucene/queryParser/Token;
    iget-object v3, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v3, v3, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1707
    :goto_0
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1708
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget v3, v3, Lorg/apache/lucene/queryParser/Token;->kind:I

    if-ne v3, p1, :cond_4

    .line 1709
    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    .line 1710
    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gc:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gc:I

    const/16 v4, 0x64

    if-le v3, v4, :cond_3

    .line 1711
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gc:I

    .line 1712
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 1713
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    aget-object v0, v3, v1

    .line 1714
    .local v0, "c":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :goto_2
    if-eqz v0, :cond_2

    .line 1715
    iget v3, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->gen:I

    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    if-ge v3, v4, :cond_0

    const/4 v3, 0x0

    iput-object v3, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryParser/Token;

    .line 1716
    :cond_0
    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    goto :goto_2

    .line 1706
    .end local v0    # "c":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-virtual {v4}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v4

    iput-object v4, v3, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    goto :goto_0

    .line 1712
    .restart local v0    # "c":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    .restart local v1    # "i":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1720
    .end local v0    # "c":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    return-object v3

    .line 1722
    :cond_4
    iput-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1723
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_kind:I

    .line 1724
    invoke-virtual {p0}, Lorg/apache/lucene/queryParser/QueryParser;->generateParseException()Lorg/apache/lucene/queryParser/ParseException;

    move-result-object v3

    throw v3
.end method

.method private static jj_la1_init_0()V
    .locals 1

    .prologue
    .line 1654
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1_0:[I

    .line 1655
    return-void

    .line 1654
    :array_0
    .array-data 4
        0x300
        0x300
        0x1c00
        0x1c00
        0x3ed3f00
        0x90000
        0x20000
        0x3ed2000
        0x2690000
        0x100000
        0x100000
        0x20000
        0x30000000
        0x4000000
        0x30000000
        0x20000
        0x0
        0x40000000    # 2.0f
        0x0
        0x20000
        0x100000
        0x20000
        0x3ed0000
    .end array-data
.end method

.method private static jj_la1_init_1()V
    .locals 1

    .prologue
    .line 1657
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1_1:[I

    .line 1658
    return-void

    .line 1657
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3
        0x0
        0x3
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private jj_ntk()I
    .locals 2

    .prologue
    .line 1771
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_nt:Lorg/apache/lucene/queryParser/Token;

    if-nez v0, :cond_0

    .line 1772
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iget v0, v1, Lorg/apache/lucene/queryParser/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1774
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_nt:Lorg/apache/lucene/queryParser/Token;

    iget v0, v0, Lorg/apache/lucene/queryParser/Token;->kind:I

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_0
.end method

.method private jj_rescan_token()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1854
    iput-boolean v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_rescan:Z

    .line 1855
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_2

    .line 1857
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    aget-object v1, v2, v0

    .line 1859
    .local v1, "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :cond_0
    iget v2, v1, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    if-le v2, v3, :cond_1

    .line 1860
    iget v2, v1, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->arg:I

    iput v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la:I

    iget-object v2, v1, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryParser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryParser/Token;

    .line 1861
    packed-switch v0, :pswitch_data_0

    .line 1865
    :cond_1
    :goto_1
    iget-object v1, v1, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    .line 1866
    if-nez v1, :cond_0

    .line 1855
    .end local v1    # "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1862
    .restart local v1    # "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_3_1()Z
    :try_end_0
    .catch Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1867
    .end local v1    # "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :catch_0
    move-exception v2

    goto :goto_2

    .line 1869
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_rescan:Z

    .line 1870
    return-void

    .line 1861
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private jj_save(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "xla"    # I

    .prologue
    .line 1873
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    aget-object v0, v2, p1

    .line 1874
    .local v0, "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :goto_0
    iget v2, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->gen:I

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    if-le v2, v3, :cond_0

    .line 1875
    iget-object v2, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    if-nez v2, :cond_1

    new-instance v1, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;-><init>()V

    iput-object v1, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    .end local v0    # "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    .local v1, "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    move-object v0, v1

    .line 1878
    .end local v1    # "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    .restart local v0    # "p":Lorg/apache/lucene/queryParser/QueryParser$JJCalls;
    :cond_0
    iget v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    add-int/2addr v2, p2

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la:I

    sub-int/2addr v2, v3

    iput v2, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->gen:I

    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iput-object v2, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->first:Lorg/apache/lucene/queryParser/Token;

    iput p2, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->arg:I

    .line 1879
    return-void

    .line 1876
    :cond_1
    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;->next:Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    goto :goto_0
.end method

.method private jj_scan_token(I)Z
    .locals 4
    .param p1, "kind"    # I

    .prologue
    .line 1730
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryParser/Token;

    if-ne v2, v3, :cond_1

    .line 1731
    iget v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la:I

    .line 1732
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    if-nez v2, :cond_0

    .line 1733
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v3

    iput-object v3, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iput-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryParser/Token;

    .line 1740
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_rescan:Z

    if-eqz v2, :cond_3

    .line 1741
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1742
    .local v1, "tok":Lorg/apache/lucene/queryParser/Token;
    :goto_1
    if-eqz v1, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    if-eq v1, v2, :cond_2

    add-int/lit8 v0, v0, 0x1

    iget-object v1, v1, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    goto :goto_1

    .line 1735
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryParser/Token;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryParser/Token;

    goto :goto_0

    .line 1738
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget-object v2, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    goto :goto_0

    .line 1743
    .restart local v0    # "i":I
    .restart local v1    # "tok":Lorg/apache/lucene/queryParser/Token;
    :cond_2
    if-eqz v1, :cond_3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_add_error_token(II)V

    .line 1745
    .end local v0    # "i":I
    .end local v1    # "tok":Lorg/apache/lucene/queryParser/Token;
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget v2, v2, Lorg/apache/lucene/queryParser/Token;->kind:I

    if-eq v2, p1, :cond_4

    const/4 v2, 0x1

    .line 1747
    :goto_2
    return v2

    .line 1746
    :cond_4
    iget v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la:I

    if-nez v2, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_scanpos:Lorg/apache/lucene/queryParser/Token;

    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_lastpos:Lorg/apache/lucene/queryParser/Token;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ls:Lorg/apache/lucene/queryParser/QueryParser$LookaheadSuccess;

    throw v2

    .line 1747
    :cond_5
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1158
    array-length v2, p0

    if-nez v2, :cond_0

    .line 1159
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v3, "Usage: java org.apache.lucene.queryParser.QueryParser <input>"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1160
    invoke-static {v5}, Ljava/lang/System;->exit(I)V

    .line 1162
    :cond_0
    new-instance v1, Lorg/apache/lucene/queryParser/QueryParser;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    const-string/jumbo v3, "field"

    new-instance v4, Lorg/apache/lucene/analysis/SimpleAnalyzer;

    invoke-direct {v4}, Lorg/apache/lucene/analysis/SimpleAnalyzer;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 1164
    .local v1, "qp":Lorg/apache/lucene/queryParser/QueryParser;
    aget-object v2, p0, v5

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queryParser/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 1165
    .local v0, "q":Lorg/apache/lucene/search/Query;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v3, "field"

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1166
    return-void
.end method


# virtual methods
.method public final Clause(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xf

    const/4 v7, -0x1

    .line 1287
    const/4 v2, 0x0

    .local v2, "fieldToken":Lorg/apache/lucene/queryParser/Token;
    const/4 v0, 0x0

    .line 1288
    .local v0, "boost":Lorg/apache/lucene/queryParser/Token;
    const/4 v4, 0x2

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_1(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1289
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v4, v7, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v4

    :goto_0
    packed-switch v4, :pswitch_data_0

    .line 1301
    :pswitch_0
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v5, 0x5

    iget v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v6, v4, v5

    .line 1302
    invoke-direct {p0, v7}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1303
    new-instance v4, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct {v4}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v4

    .line 1289
    :cond_0
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 1291
    :pswitch_1
    const/16 v4, 0x13

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v2

    .line 1292
    invoke-direct {p0, v5}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1293
    iget-object v4, v2, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1308
    :cond_1
    :goto_1
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v4, v7, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v4

    :goto_2
    packed-switch v4, :pswitch_data_1

    .line 1334
    :pswitch_2
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v5, 0x7

    iget v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v6, v4, v5

    .line 1335
    invoke-direct {p0, v7}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1336
    new-instance v4, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct {v4}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v4

    .line 1296
    :pswitch_3
    const/16 v4, 0x10

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1297
    invoke-direct {p0, v5}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1298
    const-string/jumbo p1, "*"

    .line 1299
    goto :goto_1

    .line 1308
    :cond_2
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_2

    .line 1317
    :pswitch_4
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParser;->Term(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 1338
    .local v3, "q":Lorg/apache/lucene/search/Query;
    :goto_3
    if-eqz v0, :cond_3

    .line 1339
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1341
    .local v1, "f":F
    :try_start_0
    iget-object v4, v0, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1342
    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/Query;->setBoost(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1345
    .end local v1    # "f":F
    :cond_3
    :goto_4
    return-object v3

    .line 1320
    .end local v3    # "q":Lorg/apache/lucene/search/Query;
    :pswitch_5
    const/16 v4, 0xd

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1321
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParser;->Query(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 1322
    .restart local v3    # "q":Lorg/apache/lucene/search/Query;
    const/16 v4, 0xe

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1323
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v4, v7, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v4

    :goto_5
    packed-switch v4, :pswitch_data_2

    .line 1329
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v5, 0x6

    iget v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v6, v4, v5

    goto :goto_3

    .line 1323
    :cond_4
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_5

    .line 1325
    :pswitch_6
    const/16 v4, 0x11

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1326
    const/16 v4, 0x19

    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v0

    .line 1327
    goto :goto_3

    .line 1343
    .restart local v1    # "f":F
    :catch_0
    move-exception v4

    goto :goto_4

    .line 1289
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1308
    :pswitch_data_1
    .packed-switch 0xd
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 1323
    :pswitch_data_2
    .packed-switch 0x11
        :pswitch_6
    .end packed-switch
.end method

.method public final Conjunction()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 1171
    const/4 v0, 0x0

    .line 1172
    .local v0, "ret":I
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1191
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v2, 0x1

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 1194
    :goto_1
    return v0

    .line 1172
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 1175
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v1

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 1185
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 1186
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1187
    new-instance v1, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v1

    .line 1175
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_2

    .line 1177
    :pswitch_1
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1178
    const/4 v0, 0x1

    .line 1179
    goto :goto_1

    .line 1181
    :pswitch_2
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1182
    const/4 v0, 0x2

    .line 1183
    goto :goto_1

    .line 1172
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1175
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final Modifiers()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 1199
    const/4 v0, 0x0

    .line 1200
    .local v0, "ret":I
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v1

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1224
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v2, 0x3

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 1227
    :goto_1
    return v0

    .line 1200
    :cond_0
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_0

    .line 1204
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v1

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 1218
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v2, 0x2

    iget v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v3, v1, v2

    .line 1219
    invoke-direct {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1220
    new-instance v1, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v1

    .line 1204
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_2

    .line 1206
    :pswitch_1
    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1207
    const/16 v0, 0xb

    .line 1208
    goto :goto_1

    .line 1210
    :pswitch_2
    const/16 v1, 0xc

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1211
    const/16 v0, 0xa

    .line 1212
    goto :goto_1

    .line 1214
    :pswitch_3
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1215
    const/16 v0, 0xa

    .line 1216
    goto :goto_1

    .line 1200
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1204
    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final Query(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1242
    .local v0, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v2, 0x0

    .line 1244
    .local v2, "firstQuery":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/queryParser/QueryParser;->Modifiers()I

    move-result v3

    .line 1245
    .local v3, "mods":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParser;->Clause(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 1246
    .local v4, "q":Lorg/apache/lucene/search/Query;
    const/4 v5, 0x0

    invoke-virtual {p0, v0, v5, v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;->addClause(Ljava/util/List;IILorg/apache/lucene/search/Query;)V

    .line 1247
    if-nez v3, :cond_0

    .line 1248
    move-object v2, v4

    .line 1251
    :cond_0
    :goto_0
    iget v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v5

    :goto_1
    packed-switch v5, :pswitch_data_0

    .line 1269
    :pswitch_0
    iget-object v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    const/4 v6, 0x4

    iget v7, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    aput v7, v5, v6

    .line 1277
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    if-eqz v2, :cond_2

    .line 1280
    .end local v2    # "firstQuery":Lorg/apache/lucene/search/Query;
    :goto_2
    return-object v2

    .line 1251
    .restart local v2    # "firstQuery":Lorg/apache/lucene/search/Query;
    :cond_1
    iget v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    goto :goto_1

    .line 1272
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/lucene/queryParser/QueryParser;->Conjunction()I

    move-result v1

    .line 1273
    .local v1, "conj":I
    invoke-virtual {p0}, Lorg/apache/lucene/queryParser/QueryParser;->Modifiers()I

    move-result v3

    .line 1274
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParser;->Clause(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 1275
    invoke-virtual {p0, v0, v1, v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;->addClause(Ljava/util/List;IILorg/apache/lucene/search/Query;)V

    goto :goto_0

    .line 1280
    .end local v1    # "conj":I
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryParser/QueryParser;->getBooleanQuery(Ljava/util/List;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    goto :goto_2

    .line 1251
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public ReInit(Lorg/apache/lucene/queryParser/CharStream;)V
    .locals 3
    .param p1, "stream"    # Lorg/apache/lucene/queryParser/CharStream;

    .prologue
    const/4 v2, -0x1

    .line 1675
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->ReInit(Lorg/apache/lucene/queryParser/CharStream;)V

    .line 1676
    new-instance v1, Lorg/apache/lucene/queryParser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1677
    iput v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1678
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    .line 1679
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1680
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1681
    :cond_1
    return-void
.end method

.method public ReInit(Lorg/apache/lucene/queryParser/QueryParserTokenManager;)V
    .locals 3
    .param p1, "tm"    # Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    .prologue
    const/4 v2, -0x1

    .line 1695
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    .line 1696
    new-instance v1, Lorg/apache/lucene/queryParser/Token;

    invoke-direct {v1}, Lorg/apache/lucene/queryParser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1697
    iput v2, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1698
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    .line 1699
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1700
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_2_rtns:[Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    new-instance v2, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;

    invoke-direct {v2}, Lorg/apache/lucene/queryParser/QueryParser$JJCalls;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1701
    :cond_1
    return-void
.end method

.method public final Term(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 23
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1350
    const/4 v5, 0x0

    .local v5, "boost":Lorg/apache/lucene/queryParser/Token;
    const/4 v10, 0x0

    .line 1351
    .local v10, "fuzzySlop":Lorg/apache/lucene/queryParser/Token;
    const/4 v13, 0x0

    .line 1352
    .local v13, "prefix":Z
    const/16 v19, 0x0

    .line 1353
    .local v19, "wildcard":Z
    const/4 v9, 0x0

    .line 1355
    .local v9, "fuzzy":Z
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_0

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_0
    packed-switch v20, :pswitch_data_0

    .line 1583
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x16

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1584
    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1585
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v20

    .line 1355
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_0

    .line 1361
    :pswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_1
    packed-switch v20, :pswitch_data_1

    .line 1381
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1382
    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1383
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v20

    .line 1361
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_1

    .line 1363
    :pswitch_3
    const/16 v20, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v17

    .line 1385
    .local v17, "term":Lorg/apache/lucene/queryParser/Token;
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_3
    packed-switch v20, :pswitch_data_2

    .line 1391
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x9

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1394
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_5
    packed-switch v20, :pswitch_data_3

    .line 1409
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0xb

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1412
    :goto_6
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1413
    .local v18, "termImage":Ljava/lang/String;
    if-eqz v19, :cond_6

    .line 1414
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryParser/QueryParser;->getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .line 1587
    .end local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    .end local v18    # "termImage":Ljava/lang/String;
    .local v14, "q":Lorg/apache/lucene/search/Query;
    :goto_7
    if-eqz v5, :cond_2

    .line 1588
    const/high16 v7, 0x3f800000    # 1.0f

    .line 1590
    .local v7, "f":F
    :try_start_0
    iget-object v0, v5, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 1599
    :goto_8
    if-eqz v14, :cond_2

    .line 1600
    invoke-virtual {v14, v7}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 1603
    .end local v7    # "f":F
    :cond_2
    return-object v14

    .line 1366
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    :pswitch_4
    const/16 v20, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v17

    .line 1367
    .restart local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    const/16 v19, 0x1

    .line 1368
    goto/16 :goto_2

    .line 1370
    .end local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    :pswitch_5
    const/16 v20, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v17

    .line 1371
    .restart local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    const/4 v13, 0x1

    .line 1372
    goto/16 :goto_2

    .line 1374
    .end local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    :pswitch_6
    const/16 v20, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v17

    .line 1375
    .restart local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    const/16 v19, 0x1

    .line 1376
    goto/16 :goto_2

    .line 1378
    .end local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    :pswitch_7
    const/16 v20, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v17

    .line 1379
    .restart local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    goto/16 :goto_2

    .line 1385
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto/16 :goto_3

    .line 1387
    :pswitch_8
    const/16 v20, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v10

    .line 1388
    const/4 v9, 0x1

    .line 1389
    goto/16 :goto_4

    .line 1394
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto/16 :goto_5

    .line 1396
    :pswitch_9
    const/16 v20, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1397
    const/16 v20, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v5

    .line 1398
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_9
    packed-switch v20, :pswitch_data_4

    .line 1404
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    goto/16 :goto_6

    .line 1398
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_9

    .line 1400
    :pswitch_a
    const/16 v20, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v10

    .line 1401
    const/4 v9, 0x1

    .line 1402
    goto/16 :goto_6

    .line 1415
    .restart local v18    # "termImage":Ljava/lang/String;
    :cond_6
    if-eqz v13, :cond_7

    .line 1416
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryParser/QueryParser;->getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 1419
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    :cond_7
    if-eqz v9, :cond_a

    .line 1420
    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyMinSim:F

    .line 1422
    .local v8, "fms":F
    :try_start_1
    iget-object v0, v10, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v8

    .line 1424
    :goto_a
    const/16 v20, 0x0

    cmpg-float v20, v8, v20

    if-ltz v20, :cond_8

    const/high16 v20, 0x3f800000    # 1.0f

    cmpl-float v20, v8, v20

    if-lez v20, :cond_9

    .line 1425
    :cond_8
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    const-string/jumbo v21, "Minimum similarity for a FuzzyQuery has to be between 0.0f and 1.0f !"

    invoke-direct/range {v20 .. v21}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 1427
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2, v8}, Lorg/apache/lucene/queryParser/QueryParser;->getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .line 1428
    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 1429
    .end local v8    # "fms":F
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->hasNewAPI:Z

    move/from16 v20, v0

    if-eqz v20, :cond_b

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .line 1431
    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    :goto_b
    goto/16 :goto_7

    .line 1429
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v14

    goto :goto_b

    .line 1433
    .end local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    .end local v18    # "termImage":Ljava/lang/String;
    :pswitch_b
    const/16 v20, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1434
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_c
    packed-switch v20, :pswitch_data_5

    .line 1442
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0xc

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1443
    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1444
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v20

    .line 1434
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_c

    .line 1436
    :pswitch_c
    const/16 v20, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v11

    .line 1446
    .local v11, "goop1":Lorg/apache/lucene/queryParser/Token;
    :goto_d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_e
    packed-switch v20, :pswitch_data_6

    .line 1451
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0xd

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1454
    :goto_f
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_10
    packed-switch v20, :pswitch_data_7

    .line 1462
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0xe

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1463
    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1464
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v20

    .line 1439
    .end local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    :pswitch_d
    const/16 v20, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v11

    .line 1440
    .restart local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    goto :goto_d

    .line 1446
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_e

    .line 1448
    :pswitch_e
    const/16 v20, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    goto :goto_f

    .line 1454
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_10

    .line 1456
    :pswitch_f
    const/16 v20, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v12

    .line 1466
    .local v12, "goop2":Lorg/apache/lucene/queryParser/Token;
    :goto_11
    const/16 v20, 0x1b

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1467
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_12
    packed-switch v20, :pswitch_data_8

    .line 1473
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0xf

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1477
    :goto_13
    const/16 v16, 0x0

    .line 1478
    .local v16, "startOpen":Z
    const/4 v6, 0x0

    .line 1479
    .local v6, "endOpen":Z
    iget v0, v11, Lorg/apache/lucene/queryParser/Token;->kind:I

    move/from16 v20, v0

    const/16 v21, 0x1c

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_12

    .line 1480
    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    .line 1484
    :cond_f
    :goto_14
    iget v0, v12, Lorg/apache/lucene/queryParser/Token;->kind:I

    move/from16 v20, v0

    const/16 v21, 0x1c

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_13

    .line 1485
    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    .line 1489
    :cond_10
    :goto_15
    if-eqz v16, :cond_14

    const/16 v20, 0x0

    move-object/from16 v21, v20

    :goto_16
    if-eqz v6, :cond_15

    const/16 v20, 0x0

    :goto_17
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v20

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .line 1491
    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 1459
    .end local v6    # "endOpen":Z
    .end local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    .end local v16    # "startOpen":Z
    :pswitch_10
    const/16 v20, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v12

    .line 1460
    .restart local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    goto/16 :goto_11

    .line 1467
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto/16 :goto_12

    .line 1469
    :pswitch_11
    const/16 v20, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1470
    const/16 v20, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v5

    .line 1471
    goto/16 :goto_13

    .line 1481
    .restart local v6    # "endOpen":Z
    .restart local v16    # "startOpen":Z
    :cond_12
    const-string/jumbo v20, "*"

    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 1482
    const/16 v16, 0x1

    goto :goto_14

    .line 1486
    :cond_13
    const-string/jumbo v20, "*"

    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 1487
    const/4 v6, 0x1

    goto :goto_15

    .line 1489
    :cond_14
    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v21, v20

    goto :goto_16

    :cond_15
    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    goto :goto_17

    .line 1493
    .end local v6    # "endOpen":Z
    .end local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    .end local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    .end local v16    # "startOpen":Z
    :pswitch_12
    const/16 v20, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1494
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_16

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_18
    packed-switch v20, :pswitch_data_9

    .line 1502
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x10

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1503
    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1504
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v20

    .line 1494
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_18

    .line 1496
    :pswitch_13
    const/16 v20, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v11

    .line 1506
    .restart local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    :goto_19
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_1a
    packed-switch v20, :pswitch_data_a

    .line 1511
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x11

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1514
    :goto_1b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_18

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_1c
    packed-switch v20, :pswitch_data_b

    .line 1522
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x12

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1523
    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1524
    new-instance v20, Lorg/apache/lucene/queryParser/ParseException;

    invoke-direct/range {v20 .. v20}, Lorg/apache/lucene/queryParser/ParseException;-><init>()V

    throw v20

    .line 1499
    .end local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    :pswitch_14
    const/16 v20, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v11

    .line 1500
    .restart local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    goto :goto_19

    .line 1506
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_1a

    .line 1508
    :pswitch_15
    const/16 v20, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    goto :goto_1b

    .line 1514
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_1c

    .line 1516
    :pswitch_16
    const/16 v20, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v12

    .line 1526
    .restart local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    :goto_1d
    const/16 v20, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1527
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1b

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_1e
    packed-switch v20, :pswitch_data_c

    .line 1533
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x13

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1537
    :goto_1f
    const/16 v16, 0x0

    .line 1538
    .restart local v16    # "startOpen":Z
    const/4 v6, 0x0

    .line 1539
    .restart local v6    # "endOpen":Z
    iget v0, v11, Lorg/apache/lucene/queryParser/Token;->kind:I

    move/from16 v20, v0

    const/16 v21, 0x20

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1c

    .line 1540
    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    .line 1544
    :cond_19
    :goto_20
    iget v0, v12, Lorg/apache/lucene/queryParser/Token;->kind:I

    move/from16 v20, v0

    const/16 v21, 0x20

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 1545
    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    .line 1550
    :cond_1a
    :goto_21
    if-eqz v16, :cond_1e

    const/16 v20, 0x0

    move-object/from16 v21, v20

    :goto_22
    if-eqz v6, :cond_1f

    const/16 v20, 0x0

    :goto_23
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    move-object/from16 v3, v20

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/queryParser/QueryParser;->getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .line 1552
    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 1519
    .end local v6    # "endOpen":Z
    .end local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    .end local v16    # "startOpen":Z
    :pswitch_17
    const/16 v20, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v12

    .line 1520
    .restart local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    goto/16 :goto_1d

    .line 1527
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto/16 :goto_1e

    .line 1529
    :pswitch_18
    const/16 v20, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1530
    const/16 v20, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v5

    .line 1531
    goto/16 :goto_1f

    .line 1541
    .restart local v6    # "endOpen":Z
    .restart local v16    # "startOpen":Z
    :cond_1c
    const-string/jumbo v20, "*"

    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_19

    .line 1542
    const/16 v16, 0x1

    goto :goto_20

    .line 1546
    :cond_1d
    const-string/jumbo v20, "*"

    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1a

    .line 1547
    const/4 v6, 0x1

    goto :goto_21

    .line 1550
    :cond_1e
    iget-object v0, v11, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v21, v20

    goto :goto_22

    :cond_1f
    iget-object v0, v12, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    goto :goto_23

    .line 1554
    .end local v6    # "endOpen":Z
    .end local v11    # "goop1":Lorg/apache/lucene/queryParser/Token;
    .end local v12    # "goop2":Lorg/apache/lucene/queryParser/Token;
    .end local v16    # "startOpen":Z
    :pswitch_19
    const/16 v20, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v17

    .line 1555
    .restart local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_21

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_24
    packed-switch v20, :pswitch_data_d

    .line 1560
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x14

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1563
    :goto_25
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_22

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk()I

    move-result v20

    :goto_26
    packed-switch v20, :pswitch_data_e

    .line 1569
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    move-object/from16 v20, v0

    const/16 v21, 0x15

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    move/from16 v22, v0

    aput v22, v20, v21

    .line 1572
    :goto_27
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    .line 1574
    .local v15, "s":I
    if-eqz v10, :cond_20

    .line 1576
    :try_start_2
    iget-object v0, v10, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Float;->intValue()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v15

    .line 1580
    :cond_20
    :goto_28
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->image:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->discardEscapeChar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v15}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;

    move-result-object v14

    .line 1581
    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    goto/16 :goto_7

    .line 1555
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    .end local v15    # "s":I
    :cond_21
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto/16 :goto_24

    .line 1557
    :pswitch_1a
    const/16 v20, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v10

    .line 1558
    goto :goto_25

    .line 1563
    :cond_22
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    move/from16 v20, v0

    goto :goto_26

    .line 1565
    :pswitch_1b
    const/16 v20, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1566
    const/16 v20, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    move-result-object v5

    .line 1567
    goto :goto_27

    .line 1592
    .end local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    .restart local v7    # "f":F
    .restart local v14    # "q":Lorg/apache/lucene/search/Query;
    :catch_0
    move-exception v20

    goto/16 :goto_8

    .line 1578
    .end local v7    # "f":F
    .end local v14    # "q":Lorg/apache/lucene/search/Query;
    .restart local v15    # "s":I
    .restart local v17    # "term":Lorg/apache/lucene/queryParser/Token;
    :catch_1
    move-exception v20

    goto :goto_28

    .line 1423
    .end local v15    # "s":I
    .restart local v8    # "fms":F
    .restart local v18    # "termImage":Ljava/lang/String;
    :catch_2
    move-exception v20

    goto/16 :goto_a

    .line 1355
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_19
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_b
        :pswitch_12
        :pswitch_1
    .end packed-switch

    .line 1361
    :pswitch_data_1
    .packed-switch 0x10
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_2
        :pswitch_7
    .end packed-switch

    .line 1385
    :pswitch_data_2
    .packed-switch 0x14
        :pswitch_8
    .end packed-switch

    .line 1394
    :pswitch_data_3
    .packed-switch 0x11
        :pswitch_9
    .end packed-switch

    .line 1398
    :pswitch_data_4
    .packed-switch 0x14
        :pswitch_a
    .end packed-switch

    .line 1434
    :pswitch_data_5
    .packed-switch 0x1c
        :pswitch_d
        :pswitch_c
    .end packed-switch

    .line 1446
    :pswitch_data_6
    .packed-switch 0x1a
        :pswitch_e
    .end packed-switch

    .line 1454
    :pswitch_data_7
    .packed-switch 0x1c
        :pswitch_10
        :pswitch_f
    .end packed-switch

    .line 1467
    :pswitch_data_8
    .packed-switch 0x11
        :pswitch_11
    .end packed-switch

    .line 1494
    :pswitch_data_9
    .packed-switch 0x20
        :pswitch_14
        :pswitch_13
    .end packed-switch

    .line 1506
    :pswitch_data_a
    .packed-switch 0x1e
        :pswitch_15
    .end packed-switch

    .line 1514
    :pswitch_data_b
    .packed-switch 0x20
        :pswitch_17
        :pswitch_16
    .end packed-switch

    .line 1527
    :pswitch_data_c
    .packed-switch 0x11
        :pswitch_18
    .end packed-switch

    .line 1555
    :pswitch_data_d
    .packed-switch 0x14
        :pswitch_1a
    .end packed-switch

    .line 1563
    :pswitch_data_e
    .packed-switch 0x11
        :pswitch_1b
    .end packed-switch
.end method

.method public final TopLevelQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1234
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParser;->Query(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 1235
    .local v0, "q":Lorg/apache/lucene/search/Query;
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->jj_consume_token(I)Lorg/apache/lucene/queryParser/Token;

    .line 1236
    return-object v0
.end method

.method protected addClause(Ljava/util/List;IILorg/apache/lucene/search/Query;)V
    .locals 8
    .param p2, "conj"    # I
    .param p3, "mods"    # I
    .param p4, "q"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;II",
            "Lorg/apache/lucene/search/Query;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/16 v7, 0xa

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 511
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    if-ne p2, v3, :cond_0

    .line 512
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 513
    .local v0, "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v4

    if-nez v4, :cond_0

    .line 514
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/BooleanClause;->setOccur(Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 517
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    sget-object v5, Lorg/apache/lucene/queryParser/QueryParser;->AND_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    if-ne v4, v5, :cond_1

    if-ne p2, v6, :cond_1

    .line 522
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 523
    .restart local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v4

    if-nez v4, :cond_1

    .line 524
    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/BooleanClause;->setOccur(Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 529
    .end local v0    # "c":Lorg/apache/lucene/search/BooleanClause;
    :cond_1
    if-nez p4, :cond_2

    .line 554
    :goto_0
    return-void

    .line 532
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    sget-object v5, Lorg/apache/lucene/queryParser/QueryParser;->OR_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    if-ne v4, v5, :cond_6

    .line 535
    if-ne p3, v7, :cond_5

    move v1, v3

    .line 536
    .local v1, "prohibited":Z
    :goto_1
    const/16 v4, 0xb

    if-ne p3, v4, :cond_3

    move v2, v3

    .line 537
    .local v2, "required":Z
    :cond_3
    if-ne p2, v3, :cond_4

    if-nez v1, :cond_4

    .line 538
    const/4 v2, 0x1

    .line 546
    :cond_4
    :goto_2
    if-eqz v2, :cond_9

    if-nez v1, :cond_9

    .line 547
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, p4, v3}, Lorg/apache/lucene/queryParser/QueryParser;->newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v1    # "prohibited":Z
    .end local v2    # "required":Z
    :cond_5
    move v1, v2

    .line 535
    goto :goto_1

    .line 543
    :cond_6
    if-ne p3, v7, :cond_8

    move v1, v3

    .line 544
    .restart local v1    # "prohibited":Z
    :goto_3
    if-nez v1, :cond_7

    if-eq p2, v6, :cond_7

    move v2, v3

    .restart local v2    # "required":Z
    :cond_7
    goto :goto_2

    .end local v1    # "prohibited":Z
    .end local v2    # "required":Z
    :cond_8
    move v1, v2

    .line 543
    goto :goto_3

    .line 548
    .restart local v1    # "prohibited":Z
    .restart local v2    # "required":Z
    :cond_9
    if-nez v2, :cond_a

    if-nez v1, :cond_a

    .line 549
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, p4, v3}, Lorg/apache/lucene/queryParser/QueryParser;->newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 550
    :cond_a
    if-nez v2, :cond_b

    if-eqz v1, :cond_b

    .line 551
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p0, p4, v3}, Lorg/apache/lucene/queryParser/QueryParser;->newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 553
    :cond_b
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Clause cannot be both required and prohibited"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public final disable_tracing()V
    .locals 0

    .prologue
    .line 1851
    return-void
.end method

.method public final enable_tracing()V
    .locals 0

    .prologue
    .line 1847
    return-void
.end method

.method public generateParseException()Lorg/apache/lucene/queryParser/ParseException;
    .locals 9

    .prologue
    const/16 v8, 0x22

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1810
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1811
    new-array v3, v8, [Z

    .line 1812
    .local v3, "la1tokens":[Z
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_kind:I

    if-ltz v4, :cond_0

    .line 1813
    iget v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_kind:I

    aput-boolean v6, v3, v4

    .line 1814
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_kind:I

    .line 1816
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0x17

    if-ge v1, v4, :cond_4

    .line 1817
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1:[I

    aget v4, v4, v1

    iget v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    if-ne v4, v5, :cond_3

    .line 1818
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/16 v4, 0x20

    if-ge v2, v4, :cond_3

    .line 1819
    sget-object v4, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1_0:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_1

    .line 1820
    aput-boolean v6, v3, v2

    .line 1822
    :cond_1
    sget-object v4, Lorg/apache/lucene/queryParser/QueryParser;->jj_la1_1:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    .line 1823
    add-int/lit8 v4, v2, 0x20

    aput-boolean v6, v3, v4

    .line 1818
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1816
    .end local v2    # "j":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1828
    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v8, :cond_6

    .line 1829
    aget-boolean v4, v3, v1

    if-eqz v4, :cond_5

    .line 1830
    new-array v4, v6, [I

    iput-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    .line 1831
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    aput v1, v4, v7

    .line 1832
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    iget-object v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentry:[I

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1828
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1835
    :cond_6
    iput v7, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_endpos:I

    .line 1836
    invoke-direct {p0}, Lorg/apache/lucene/queryParser/QueryParser;->jj_rescan_token()V

    .line 1837
    invoke-direct {p0, v7, v7}, Lorg/apache/lucene/queryParser/QueryParser;->jj_add_error_token(II)V

    .line 1838
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v0, v4, [[I

    .line 1839
    .local v0, "exptokseq":[[I
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 1840
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_expentries:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    aput-object v4, v0, v1

    .line 1839
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1842
    :cond_7
    new-instance v4, Lorg/apache/lucene/queryParser/ParseException;

    iget-object v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    sget-object v6, Lorg/apache/lucene/queryParser/QueryParser;->tokenImage:[Ljava/lang/String;

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Lorg/apache/lucene/queryParser/Token;[[I[Ljava/lang/String;)V

    return-object v4
.end method

.method public getAllowLeadingWildcard()Z
    .locals 1

    .prologue
    .line 329
    iget-boolean v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->allowLeadingWildcard:Z

    return v0
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public final getAutoGeneratePhraseQueries()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->autoGeneratePhraseQueries:Z

    return v0
.end method

.method protected getBooleanQuery(Ljava/util/List;)Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 933
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/queryParser/QueryParser;->getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected getBooleanQuery(Ljava/util/List;Z)Lorg/apache/lucene/search/Query;
    .locals 4
    .param p2, "disableCoord"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/BooleanClause;",
            ">;Z)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 953
    .local p1, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/BooleanClause;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 954
    const/4 v2, 0x0

    .line 960
    :cond_0
    return-object v2

    .line 956
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queryParser/QueryParser;->newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v2

    .line 957
    .local v2, "query":Lorg/apache/lucene/search/BooleanQuery;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 958
    .local v0, "clause":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/BooleanClause;)V

    goto :goto_0
.end method

.method public getDateResolution(Ljava/lang/String;)Lorg/apache/lucene/document/DateTools$Resolution;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 464
    if-nez p1, :cond_0

    .line 465
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Field cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 468
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    if-nez v1, :cond_2

    .line 470
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 479
    :cond_1
    :goto_0
    return-object v0

    .line 473
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/DateTools$Resolution;

    .line 474
    .local v0, "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    if-nez v0, :cond_1

    .line 476
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    goto :goto_0
.end method

.method public getDefaultOperator()Lorg/apache/lucene/queryParser/QueryParser$Operator;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    return-object v0
.end method

.method public getEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    return v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->field:Ljava/lang/String;

    return-object v0
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 562
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "slop"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 752
    iget-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->hasNewAPI:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 754
    .local v0, "query":Lorg/apache/lucene/search/Query;
    :goto_0
    instance-of v1, v0, Lorg/apache/lucene/search/PhraseQuery;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 755
    check-cast v1, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 757
    :cond_0
    instance-of v1, v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 758
    check-cast v1, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    .line 761
    :cond_1
    return-object v0

    .line 752
    .end local v0    # "query":Lorg/apache/lucene/search/Query;
    :cond_2
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queryParser/QueryParser;->getFieldQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method protected getFieldQuery(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 26
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "queryText"    # Ljava/lang/String;
    .param p3, "quoted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 574
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v24, v0

    new-instance v25, Ljava/io/StringReader;

    move-object/from16 v0, v25

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/Analyzer;->reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v20

    .line 575
    .local v20, "source":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/analysis/TokenStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 579
    :goto_0
    new-instance v3, Lorg/apache/lucene/analysis/CachingTokenFilter;

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 580
    .local v3, "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    const/16 v23, 0x0

    .line 581
    .local v23, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    const/4 v13, 0x0

    .line 582
    .local v13, "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    const/4 v11, 0x0

    .line 584
    .local v11, "numTokens":I
    const/16 v21, 0x0

    .line 586
    .local v21, "success":Z
    :try_start_1
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 587
    const/16 v21, 0x1

    .line 591
    :goto_1
    if-eqz v21, :cond_1

    .line 592
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->hasAttribute(Ljava/lang/Class;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 593
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v23

    .end local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    check-cast v23, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 595
    .restart local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :cond_0
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->hasAttribute(Ljava/lang/Class;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 596
    const-class v24, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v13

    .end local v13    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    check-cast v13, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 600
    .restart local v13    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    :cond_1
    const/4 v15, 0x0

    .line 601
    .local v15, "positionCount":I
    const/16 v19, 0x0

    .line 603
    .local v19, "severalTokensAtSamePosition":Z
    const/4 v6, 0x0

    .line 604
    .local v6, "hasMoreTokens":Z
    if-eqz v23, :cond_4

    .line 606
    :try_start_2
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v6

    .line 607
    :goto_2
    if-eqz v6, :cond_4

    .line 608
    add-int/lit8 v11, v11, 0x1

    .line 609
    if-eqz v13, :cond_2

    invoke-interface {v13}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v16

    .line 610
    .local v16, "positionIncrement":I
    :goto_3
    if-eqz v16, :cond_3

    .line 611
    add-int v15, v15, v16

    .line 615
    :goto_4
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v6

    .line 616
    goto :goto_2

    .line 576
    .end local v3    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .end local v6    # "hasMoreTokens":Z
    .end local v11    # "numTokens":I
    .end local v13    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v15    # "positionCount":I
    .end local v16    # "positionIncrement":I
    .end local v19    # "severalTokensAtSamePosition":Z
    .end local v20    # "source":Lorg/apache/lucene/analysis/TokenStream;
    .end local v21    # "success":Z
    .end local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :catch_0
    move-exception v5

    .line 577
    .local v5, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v24, v0

    new-instance v25, Ljava/io/StringReader;

    move-object/from16 v0, v25

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v20

    .restart local v20    # "source":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0

    .line 609
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "buffer":Lorg/apache/lucene/analysis/CachingTokenFilter;
    .restart local v6    # "hasMoreTokens":Z
    .restart local v11    # "numTokens":I
    .restart local v13    # "posIncrAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v15    # "positionCount":I
    .restart local v19    # "severalTokensAtSamePosition":Z
    .restart local v21    # "success":Z
    .restart local v23    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :cond_2
    const/16 v16, 0x1

    goto :goto_3

    .line 613
    .restart local v16    # "positionIncrement":I
    :cond_3
    const/16 v19, 0x1

    goto :goto_4

    .line 617
    .end local v16    # "positionIncrement":I
    :catch_1
    move-exception v24

    .line 623
    :cond_4
    :try_start_3
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->reset()V

    .line 626
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/analysis/TokenStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    .line 632
    :goto_5
    if-nez v11, :cond_6

    .line 633
    const/16 v17, 0x0

    .line 736
    :cond_5
    :goto_6
    return-object v17

    .line 634
    :cond_6
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v11, v0, :cond_8

    .line 635
    const/16 v22, 0x0

    .line 637
    .local v22, "term":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v7

    .line 638
    .local v7, "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryParser/QueryParser;->$assertionsDisabled:Z

    if-nez v24, :cond_7

    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v7, v0, :cond_7

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 640
    .end local v7    # "hasNext":Z
    :catch_2
    move-exception v24

    .line 643
    :goto_7
    new-instance v24, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v17

    goto :goto_6

    .line 639
    .restart local v7    # "hasNext":Z
    :cond_7
    :try_start_5
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v22

    goto :goto_7

    .line 645
    .end local v7    # "hasNext":Z
    .end local v22    # "term":Ljava/lang/String;
    :cond_8
    if-nez v19, :cond_9

    if-nez p3, :cond_16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->autoGeneratePhraseQueries:Z

    move/from16 v24, v0

    if-nez v24, :cond_16

    .line 646
    :cond_9
    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v15, v0, :cond_a

    if-nez p3, :cond_f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->autoGeneratePhraseQueries:Z

    move/from16 v24, v0

    if-nez v24, :cond_f

    .line 648
    :cond_a
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v15, v0, :cond_b

    const/16 v24, 0x1

    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v18

    .line 650
    .local v18, "q":Lorg/apache/lucene/search/BooleanQuery;
    const/16 v24, 0x1

    move/from16 v0, v24

    if-le v15, v0, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    move-object/from16 v24, v0

    sget-object v25, Lorg/apache/lucene/queryParser/QueryParser;->AND_OPERATOR:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_c

    sget-object v12, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 653
    .local v12, "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    :goto_9
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_a
    if-ge v8, v11, :cond_e

    .line 654
    const/16 v22, 0x0

    .line 656
    .restart local v22    # "term":Ljava/lang/String;
    :try_start_6
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v7

    .line 657
    .restart local v7    # "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryParser/QueryParser;->$assertionsDisabled:Z

    if-nez v24, :cond_d

    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v7, v0, :cond_d

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 659
    .end local v7    # "hasNext":Z
    :catch_3
    move-exception v24

    .line 663
    :goto_b
    new-instance v24, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queryParser/QueryParser;->newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    .line 665
    .local v4, "currentQuery":Lorg/apache/lucene/search/Query;
    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v12}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 653
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 648
    .end local v4    # "currentQuery":Lorg/apache/lucene/search/Query;
    .end local v8    # "i":I
    .end local v12    # "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    .end local v18    # "q":Lorg/apache/lucene/search/BooleanQuery;
    .end local v22    # "term":Ljava/lang/String;
    :cond_b
    const/16 v24, 0x0

    goto :goto_8

    .line 650
    .restart local v18    # "q":Lorg/apache/lucene/search/BooleanQuery;
    :cond_c
    sget-object v12, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_9

    .line 658
    .restart local v7    # "hasNext":Z
    .restart local v8    # "i":I
    .restart local v12    # "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    .restart local v22    # "term":Ljava/lang/String;
    :cond_d
    :try_start_7
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    move-result-object v22

    goto :goto_b

    .end local v7    # "hasNext":Z
    .end local v22    # "term":Ljava/lang/String;
    :cond_e
    move-object/from16 v17, v18

    .line 667
    goto/16 :goto_6

    .line 671
    .end local v8    # "i":I
    .end local v12    # "occur":Lorg/apache/lucene/search/BooleanClause$Occur;
    .end local v18    # "q":Lorg/apache/lucene/search/BooleanQuery;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->newMultiPhraseQuery()Lorg/apache/lucene/search/MultiPhraseQuery;

    move-result-object v9

    .line 672
    .local v9, "mpq":Lorg/apache/lucene/search/MultiPhraseQuery;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->setSlop(I)V

    .line 673
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 674
    .local v10, "multiTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    const/4 v14, -0x1

    .line 675
    .local v14, "position":I
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_c
    if-ge v8, v11, :cond_14

    .line 676
    const/16 v22, 0x0

    .line 677
    .restart local v22    # "term":Ljava/lang/String;
    const/16 v16, 0x1

    .line 679
    .restart local v16    # "positionIncrement":I
    :try_start_8
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v7

    .line 680
    .restart local v7    # "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryParser/QueryParser;->$assertionsDisabled:Z

    if-nez v24, :cond_12

    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v7, v0, :cond_12

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 685
    .end local v7    # "hasNext":Z
    :catch_4
    move-exception v24

    .line 689
    :cond_10
    :goto_d
    if-lez v16, :cond_11

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v24

    if-lez v24, :cond_11

    .line 690
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    move/from16 v24, v0

    if-eqz v24, :cond_13

    .line 691
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    invoke-virtual {v9, v0, v14}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    .line 695
    :goto_e
    invoke-interface {v10}, Ljava/util/List;->clear()V

    .line 697
    :cond_11
    add-int v14, v14, v16

    .line 698
    new-instance v24, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 675
    add-int/lit8 v8, v8, 0x1

    goto :goto_c

    .line 681
    .restart local v7    # "hasNext":Z
    :cond_12
    :try_start_9
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 682
    if-eqz v13, :cond_10

    .line 683
    invoke-interface {v13}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    move-result v16

    goto :goto_d

    .line 693
    .end local v7    # "hasNext":Z
    :cond_13
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    goto :goto_e

    .line 700
    .end local v16    # "positionIncrement":I
    .end local v22    # "term":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    move/from16 v24, v0

    if-eqz v24, :cond_15

    .line 701
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    invoke-virtual {v9, v0, v14}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    :goto_f
    move-object/from16 v17, v9

    .line 705
    goto/16 :goto_6

    .line 703
    :cond_15
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    goto :goto_f

    .line 709
    .end local v8    # "i":I
    .end local v9    # "mpq":Lorg/apache/lucene/search/MultiPhraseQuery;
    .end local v10    # "multiTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v14    # "position":I
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/queryParser/QueryParser;->newPhraseQuery()Lorg/apache/lucene/search/PhraseQuery;

    move-result-object v17

    .line 710
    .local v17, "pq":Lorg/apache/lucene/search/PhraseQuery;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    move/from16 v24, v0

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/PhraseQuery;->setSlop(I)V

    .line 711
    const/4 v14, -0x1

    .line 714
    .restart local v14    # "position":I
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_10
    if-ge v8, v11, :cond_5

    .line 715
    const/16 v22, 0x0

    .line 716
    .restart local v22    # "term":Ljava/lang/String;
    const/16 v16, 0x1

    .line 719
    .restart local v16    # "positionIncrement":I
    :try_start_a
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CachingTokenFilter;->incrementToken()Z

    move-result v7

    .line 720
    .restart local v7    # "hasNext":Z
    sget-boolean v24, Lorg/apache/lucene/queryParser/QueryParser;->$assertionsDisabled:Z

    if-nez v24, :cond_18

    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v7, v0, :cond_18

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 725
    .end local v7    # "hasNext":Z
    :catch_5
    move-exception v24

    .line 729
    :cond_17
    :goto_11
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    move/from16 v24, v0

    if-eqz v24, :cond_19

    .line 730
    add-int v14, v14, v16

    .line 731
    new-instance v24, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v14}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;I)V

    .line 714
    :goto_12
    add-int/lit8 v8, v8, 0x1

    goto :goto_10

    .line 721
    .restart local v7    # "hasNext":Z
    :cond_18
    :try_start_b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    .line 722
    if-eqz v13, :cond_17

    .line 723
    invoke-interface {v13}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    move-result v16

    goto :goto_11

    .line 733
    .end local v7    # "hasNext":Z
    :cond_19
    new-instance v24, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;)V

    goto :goto_12

    .line 588
    .end local v6    # "hasMoreTokens":Z
    .end local v8    # "i":I
    .end local v14    # "position":I
    .end local v15    # "positionCount":I
    .end local v16    # "positionIncrement":I
    .end local v17    # "pq":Lorg/apache/lucene/search/PhraseQuery;
    .end local v19    # "severalTokensAtSamePosition":Z
    .end local v22    # "term":Ljava/lang/String;
    :catch_6
    move-exception v24

    goto/16 :goto_1

    .line 628
    .restart local v6    # "hasMoreTokens":Z
    .restart local v15    # "positionCount":I
    .restart local v19    # "severalTokensAtSamePosition":Z
    :catch_7
    move-exception v24

    goto/16 :goto_5
.end method

.method public getFuzzyMinSim()F
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyMinSim:F

    return v0
.end method

.method public getFuzzyPrefixLength()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyPrefixLength:I

    return v0
.end method

.method protected getFuzzyQuery(Ljava/lang/String;Ljava/lang/String;F)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1045
    iget-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_0

    .line 1046
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    .line 1048
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    .local v0, "t":Lorg/apache/lucene/index/Term;
    iget v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyPrefixLength:I

    invoke-virtual {p0, v0, p3, v1}, Lorg/apache/lucene/queryParser/QueryParser;->newFuzzyQuery(Lorg/apache/lucene/index/Term;FI)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getLowercaseExpandedTerms()Z
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    return v0
.end method

.method public getMultiTermRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method public final getNextToken()Lorg/apache/lucene/queryParser/Token;
    .locals 2

    .prologue
    .line 1753
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v0, v0, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1755
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_ntk:I

    .line 1756
    iget v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->jj_gen:I

    .line 1757
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    return-object v0

    .line 1754
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    iput-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    goto :goto_0
.end method

.method public getPhraseSlop()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    return v0
.end method

.method protected getPrefixQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 1023
    iget-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->allowLeadingWildcard:Z

    if-nez v1, :cond_0

    const-string/jumbo v1, "*"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1024
    new-instance v1, Lorg/apache/lucene/queryParser/ParseException;

    const-string/jumbo v2, "\'*\' not allowed as first character in PrefixQuery"

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1025
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_1

    .line 1026
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    .line 1028
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    .local v0, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryParser/QueryParser;->newPrefixQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method public getRangeCollator()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->rangeCollator:Ljava/text/Collator;

    return-object v0
.end method

.method protected getRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "inclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 773
    iget-boolean v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    if-eqz v6, :cond_0

    .line 774
    if-nez p2, :cond_2

    move-object p2, v5

    .line 775
    :goto_0
    if-nez p3, :cond_3

    move-object p3, v5

    .line 777
    :cond_0
    :goto_1
    const/4 v5, 0x3

    iget-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->locale:Ljava/util/Locale;

    invoke-static {v5, v6}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v3

    .line 778
    .local v3, "df":Ljava/text/DateFormat;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/text/DateFormat;->setLenient(Z)V

    .line 779
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queryParser/QueryParser;->getDateResolution(Ljava/lang/String;)Lorg/apache/lucene/document/DateTools$Resolution;

    move-result-object v4

    .line 781
    .local v4, "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :try_start_0
    invoke-virtual {v3, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 782
    .local v1, "d1":Ljava/util/Date;
    if-nez v4, :cond_4

    .line 786
    invoke-static {v1}, Lorg/apache/lucene/document/DateField;->dateToString(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p2

    .line 793
    .end local v1    # "d1":Ljava/util/Date;
    :goto_2
    :try_start_1
    invoke-virtual {v3, p3}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 794
    .local v2, "d2":Ljava/util/Date;
    if-eqz p4, :cond_1

    .line 798
    iget-object v5, p0, Lorg/apache/lucene/queryParser/QueryParser;->locale:Ljava/util/Locale;

    invoke-static {v5}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 799
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 800
    const/16 v5, 0xb

    const/16 v6, 0x17

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 801
    const/16 v5, 0xc

    const/16 v6, 0x3b

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 802
    const/16 v5, 0xd

    const/16 v6, 0x3b

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 803
    const/16 v5, 0xe

    const/16 v6, 0x3e7

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 804
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 806
    .end local v0    # "cal":Ljava/util/Calendar;
    :cond_1
    if-nez v4, :cond_5

    .line 810
    invoke-static {v2}, Lorg/apache/lucene/document/DateField;->dateToString(Ljava/util/Date;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object p3

    .line 816
    .end local v2    # "d2":Ljava/util/Date;
    :goto_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queryParser/QueryParser;->newRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;

    move-result-object v5

    return-object v5

    .line 774
    .end local v3    # "df":Ljava/text/DateFormat;
    .end local v4    # "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 775
    :cond_3
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 788
    .restart local v1    # "d1":Ljava/util/Date;
    .restart local v3    # "df":Ljava/text/DateFormat;
    .restart local v4    # "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    :cond_4
    :try_start_2
    invoke-static {v1, v4}, Lorg/apache/lucene/document/DateTools;->dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object p2

    goto :goto_2

    .line 812
    .end local v1    # "d1":Ljava/util/Date;
    .restart local v2    # "d2":Ljava/util/Date;
    :cond_5
    :try_start_3
    invoke-static {v2, v4}, Lorg/apache/lucene/document/DateTools;->dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object p3

    goto :goto_3

    .line 814
    .end local v2    # "d2":Ljava/util/Date;
    :catch_0
    move-exception v5

    goto :goto_3

    .line 790
    :catch_1
    move-exception v5

    goto :goto_2
.end method

.method public final getToken(I)Lorg/apache/lucene/queryParser/Token;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1762
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->token:Lorg/apache/lucene/queryParser/Token;

    .line 1763
    .local v1, "t":Lorg/apache/lucene/queryParser/Token;
    const/4 v0, 0x0

    .local v0, "i":I
    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryParser/Token;
    .local v2, "t":Lorg/apache/lucene/queryParser/Token;
    :goto_0
    if-ge v0, p1, :cond_1

    .line 1764
    iget-object v3, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    if-eqz v3, :cond_0

    iget-object v1, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    .line 1763
    .end local v2    # "t":Lorg/apache/lucene/queryParser/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryParser/Token;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move-object v2, v1

    .end local v1    # "t":Lorg/apache/lucene/queryParser/Token;
    .restart local v2    # "t":Lorg/apache/lucene/queryParser/Token;
    goto :goto_0

    .line 1765
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/queryParser/QueryParser;->token_source:Lorg/apache/lucene/queryParser/QueryParserTokenManager;

    invoke-virtual {v3}, Lorg/apache/lucene/queryParser/QueryParserTokenManager;->getNextToken()Lorg/apache/lucene/queryParser/Token;

    move-result-object v1

    iput-object v1, v2, Lorg/apache/lucene/queryParser/Token;->next:Lorg/apache/lucene/queryParser/Token;

    .end local v2    # "t":Lorg/apache/lucene/queryParser/Token;
    .restart local v1    # "t":Lorg/apache/lucene/queryParser/Token;
    goto :goto_1

    .line 1767
    .end local v1    # "t":Lorg/apache/lucene/queryParser/Token;
    .restart local v2    # "t":Lorg/apache/lucene/queryParser/Token;
    :cond_1
    return-object v2
.end method

.method protected getWildcardQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "termStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 986
    const-string/jumbo v1, "*"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 987
    const-string/jumbo v1, "*"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/queryParser/QueryParser;->newMatchAllDocsQuery()Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 995
    :goto_0
    return-object v1

    .line 989
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->allowLeadingWildcard:Z

    if-nez v1, :cond_2

    const-string/jumbo v1, "*"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "?"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 990
    :cond_1
    new-instance v1, Lorg/apache/lucene/queryParser/ParseException;

    const-string/jumbo v2, "\'*\' or \'?\' not allowed as first character in WildcardQuery"

    invoke-direct {v1, v2}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 991
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    if-eqz v1, :cond_3

    .line 992
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    .line 994
    :cond_3
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    .local v0, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/queryParser/QueryParser;->newWildcardQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    goto :goto_0
.end method

.method protected newBooleanClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)Lorg/apache/lucene/search/BooleanClause;
    .locals 1
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 835
    new-instance v0, Lorg/apache/lucene/search/BooleanClause;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/search/BooleanClause;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    return-object v0
.end method

.method protected newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;
    .locals 1
    .param p1, "disableCoord"    # Z

    .prologue
    .line 825
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    return-object v0
.end method

.method protected newFuzzyQuery(Lorg/apache/lucene/index/Term;FI)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F
    .param p3, "prefixLength"    # I

    .prologue
    .line 883
    new-instance v0, Lorg/apache/lucene/search/FuzzyQuery;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FI)V

    return-object v0
.end method

.method protected newMatchAllDocsQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 905
    new-instance v0, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    return-object v0
.end method

.method protected newMultiPhraseQuery()Lorg/apache/lucene/search/MultiPhraseQuery;
    .locals 1

    .prologue
    .line 860
    new-instance v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    return-object v0
.end method

.method protected newPhraseQuery()Lorg/apache/lucene/search/PhraseQuery;
    .locals 1

    .prologue
    .line 852
    new-instance v0, Lorg/apache/lucene/search/PhraseQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/PhraseQuery;-><init>()V

    return-object v0
.end method

.method protected newPrefixQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "prefix"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 869
    new-instance v0, Lorg/apache/lucene/search/PrefixQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/PrefixQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 870
    .local v0, "query":Lorg/apache/lucene/search/PrefixQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/PrefixQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 871
    return-object v0
.end method

.method protected newRangeQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/lucene/search/Query;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "part1"    # Ljava/lang/String;
    .param p3, "part2"    # Ljava/lang/String;
    .param p4, "inclusive"    # Z

    .prologue
    .line 895
    new-instance v0, Lorg/apache/lucene/search/TermRangeQuery;

    iget-object v6, p0, Lorg/apache/lucene/queryParser/QueryParser;->rangeCollator:Ljava/text/Collator;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V

    .line 896
    .local v0, "query":Lorg/apache/lucene/search/TermRangeQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/TermRangeQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 897
    return-object v0
.end method

.method protected newTermQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 844
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method protected newWildcardQuery(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 914
    new-instance v0, Lorg/apache/lucene/search/WildcardQuery;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 915
    .local v0, "query":Lorg/apache/lucene/search/WildcardQuery;
    iget-object v1, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/WildcardQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 916
    return-object v0
.end method

.method public parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 203
    new-instance v4, Lorg/apache/lucene/queryParser/FastCharStream;

    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lorg/apache/lucene/queryParser/FastCharStream;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->ReInit(Lorg/apache/lucene/queryParser/CharStream;)V

    .line 206
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/queryParser/QueryParser;->field:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->TopLevelQuery(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 207
    .local v1, "res":Lorg/apache/lucene/search/Query;
    if-eqz v1, :cond_0

    .end local v1    # "res":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v1

    .restart local v1    # "res":Lorg/apache/lucene/search/Query;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lorg/apache/lucene/queryParser/QueryParser;->newBooleanQuery(Z)Lorg/apache/lucene/search/BooleanQuery;
    :try_end_0
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/lucene/queryParser/TokenMgrError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/lucene/search/BooleanQuery$TooManyClauses; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    goto :goto_0

    .line 209
    .end local v1    # "res":Lorg/apache/lucene/search/Query;
    :catch_0
    move-exception v3

    .line 211
    .local v3, "tme":Lorg/apache/lucene/queryParser/ParseException;
    new-instance v0, Lorg/apache/lucene/queryParser/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Cannot parse \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\': "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/lucene/queryParser/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    .line 212
    .local v0, "e":Lorg/apache/lucene/queryParser/ParseException;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryParser/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 213
    throw v0

    .line 215
    .end local v0    # "e":Lorg/apache/lucene/queryParser/ParseException;
    .end local v3    # "tme":Lorg/apache/lucene/queryParser/ParseException;
    :catch_1
    move-exception v3

    .line 216
    .local v3, "tme":Lorg/apache/lucene/queryParser/TokenMgrError;
    new-instance v0, Lorg/apache/lucene/queryParser/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Cannot parse \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\': "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/lucene/queryParser/TokenMgrError;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    .line 217
    .restart local v0    # "e":Lorg/apache/lucene/queryParser/ParseException;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/queryParser/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 218
    throw v0

    .line 220
    .end local v0    # "e":Lorg/apache/lucene/queryParser/ParseException;
    .end local v3    # "tme":Lorg/apache/lucene/queryParser/TokenMgrError;
    :catch_2
    move-exception v2

    .line 221
    .local v2, "tmc":Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    new-instance v0, Lorg/apache/lucene/queryParser/ParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Cannot parse \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\': too many boolean clauses"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/lucene/queryParser/ParseException;-><init>(Ljava/lang/String;)V

    .line 222
    .restart local v0    # "e":Lorg/apache/lucene/queryParser/ParseException;
    invoke-virtual {v0, v2}, Lorg/apache/lucene/queryParser/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 223
    throw v0
.end method

.method public setAllowLeadingWildcard(Z)V
    .locals 0
    .param p1, "allowLeadingWildcard"    # Z

    .prologue
    .line 322
    iput-boolean p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->allowLeadingWildcard:Z

    .line 323
    return-void
.end method

.method public final setAutoGeneratePhraseQueries(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 258
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->hasNewAPI:Z

    if-nez v0, :cond_0

    .line 259
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "You must implement the new API: getFieldQuery(String,String,boolean) to use setAutoGeneratePhraseQueries(false)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_0
    iput-boolean p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->autoGeneratePhraseQueries:Z

    .line 262
    return-void
.end method

.method public setDateResolution(Ljava/lang/String;Lorg/apache/lucene/document/DateTools$Resolution;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "dateResolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 445
    if-nez p1, :cond_0

    .line 446
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Field cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 451
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    .line 454
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queryParser/QueryParser;->fieldToDateResolution:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    return-void
.end method

.method public setDateResolution(Lorg/apache/lucene/document/DateTools$Resolution;)V
    .locals 0
    .param p1, "dateResolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 435
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->dateResolution:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 436
    return-void
.end method

.method public setDefaultOperator(Lorg/apache/lucene/queryParser/QueryParser$Operator;)V
    .locals 0
    .param p1, "op"    # Lorg/apache/lucene/queryParser/QueryParser$Operator;

    .prologue
    .line 362
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->operator:Lorg/apache/lucene/queryParser/QueryParser$Operator;

    .line 363
    return-void
.end method

.method public setEnablePositionIncrements(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 343
    iput-boolean p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->enablePositionIncrements:Z

    .line 344
    return-void
.end method

.method public setFuzzyMinSim(F)V
    .locals 0
    .param p1, "fuzzyMinSim"    # F

    .prologue
    .line 276
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyMinSim:F

    .line 277
    return-void
.end method

.method public setFuzzyPrefixLength(I)V
    .locals 0
    .param p1, "fuzzyPrefixLength"    # I

    .prologue
    .line 292
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->fuzzyPrefixLength:I

    .line 293
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 417
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->locale:Ljava/util/Locale;

    .line 418
    return-void
.end method

.method public setLowercaseExpandedTerms(Z)V
    .locals 0
    .param p1, "lowercaseExpandedTerms"    # Z

    .prologue
    .line 380
    iput-boolean p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->lowercaseExpandedTerms:Z

    .line 381
    return-void
.end method

.method public setMultiTermRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V
    .locals 0
    .param p1, "method"    # Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .prologue
    .line 402
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->multiTermRewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 403
    return-void
.end method

.method public setPhraseSlop(I)V
    .locals 0
    .param p1, "phraseSlop"    # I

    .prologue
    .line 300
    iput p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->phraseSlop:I

    .line 301
    return-void
.end method

.method public setRangeCollator(Ljava/text/Collator;)V
    .locals 0
    .param p1, "rc"    # Ljava/text/Collator;

    .prologue
    .line 495
    iput-object p1, p0, Lorg/apache/lucene/queryParser/QueryParser;->rangeCollator:Ljava/text/Collator;

    .line 496
    return-void
.end method
