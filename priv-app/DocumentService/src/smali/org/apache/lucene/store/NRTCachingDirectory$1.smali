.class Lorg/apache/lucene/store/NRTCachingDirectory$1;
.super Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.source "NRTCachingDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/NRTCachingDirectory;->getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/NRTCachingDirectory;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/NRTCachingDirectory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    iput-object p1, p0, Lorg/apache/lucene/store/NRTCachingDirectory$1;->this$0:Lorg/apache/lucene/store/NRTCachingDirectory;

    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;-><init>()V

    return-void
.end method


# virtual methods
.method protected doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 3
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory$1;->this$0:Lorg/apache/lucene/store/NRTCachingDirectory;

    # getter for: Lorg/apache/lucene/store/NRTCachingDirectory;->merges:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lorg/apache/lucene/store/NRTCachingDirectory;->access$000(Lorg/apache/lucene/store/NRTCachingDirectory;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    invoke-super {p0, p1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory$1;->this$0:Lorg/apache/lucene/store/NRTCachingDirectory;

    # getter for: Lorg/apache/lucene/store/NRTCachingDirectory;->merges:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lorg/apache/lucene/store/NRTCachingDirectory;->access$000(Lorg/apache/lucene/store/NRTCachingDirectory;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    return-void

    .line 305
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory$1;->this$0:Lorg/apache/lucene/store/NRTCachingDirectory;

    # getter for: Lorg/apache/lucene/store/NRTCachingDirectory;->merges:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lorg/apache/lucene/store/NRTCachingDirectory;->access$000(Lorg/apache/lucene/store/NRTCachingDirectory;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method
