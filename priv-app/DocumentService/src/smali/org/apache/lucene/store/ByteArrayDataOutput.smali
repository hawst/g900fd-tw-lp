.class public Lorg/apache/lucene/store/ByteArrayDataOutput;
.super Lorg/apache/lucene/store/DataOutput;
.source "ByteArrayDataOutput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bytes:[B

.field private limit:I

.field private pos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/ByteArrayDataOutput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 43
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([B)V

    .line 44
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 35
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([B)V

    .line 36
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 39
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([BII)V

    .line 40
    return-void
.end method


# virtual methods
.method public getPosition()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    return v0
.end method

.method public reset([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 47
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([BII)V

    .line 48
    return-void
.end method

.method public reset([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 51
    iput-object p1, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->bytes:[B

    .line 52
    iput p2, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    .line 53
    add-int v0, p2, p3

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->limit:I

    .line 54
    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B

    .prologue
    .line 62
    sget-boolean v0, Lorg/apache/lucene/store/ByteArrayDataOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->limit:I

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    aput-byte p1, v0, v1

    .line 64
    return-void
.end method

.method public writeBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 68
    sget-boolean v0, Lorg/apache/lucene/store/ByteArrayDataOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    add-int/2addr v0, p3

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->limit:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataOutput;->pos:I

    .line 71
    return-void
.end method
