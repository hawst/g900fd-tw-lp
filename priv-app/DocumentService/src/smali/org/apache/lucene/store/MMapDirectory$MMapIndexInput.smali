.class final Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
.super Lorg/apache/lucene/store/IndexInput;
.source "MMapDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/MMapDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MMapIndexInput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffers:[Ljava/nio/ByteBuffer;

.field private final chunkSize:J

.field private final chunkSizeMask:J

.field private final chunkSizePower:I

.field private final clones:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;",
            ">;"
        }
    .end annotation
.end field

.field private curBuf:Ljava/nio/ByteBuffer;

.field private curBufIndex:I

.field private isClone:Z

.field private final length:J

.field final synthetic this$0:Lorg/apache/lucene/store/MMapDirectory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 230
    const-class v0, Lorg/apache/lucene/store/MMapDirectory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/MMapDirectory;Ljava/lang/String;Ljava/io/RandomAccessFile;I)V
    .locals 14
    .param p2, "resourceDescription"    # Ljava/lang/String;
    .param p3, "raf"    # Ljava/io/RandomAccessFile;
    .param p4, "chunkSizePower"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    iput-object p1, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->this$0:Lorg/apache/lucene/store/MMapDirectory;

    .line 245
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 241
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->isClone:Z

    .line 242
    new-instance v3, Lorg/apache/lucene/util/MapBackedSet;

    new-instance v6, Ljava/util/WeakHashMap;

    invoke-direct {v6}, Ljava/util/WeakHashMap;-><init>()V

    invoke-direct {v3, v6}, Lorg/apache/lucene/util/MapBackedSet;-><init>(Ljava/util/Map;)V

    iput-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    .line 246
    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->length:J

    .line 247
    move/from16 v0, p4

    iput v0, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSizePower:I

    .line 248
    const-wide/16 v6, 0x1

    shl-long v6, v6, p4

    iput-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSize:J

    .line 249
    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSize:J

    const-wide/16 v12, 0x1

    sub-long/2addr v6, v12

    iput-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSizeMask:J

    .line 251
    if-ltz p4, :cond_0

    const/16 v3, 0x1e

    move/from16 v0, p4

    if-le v0, v3, :cond_1

    .line 252
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Invalid chunkSizePower used for ByteBuffer size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 254
    :cond_1
    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->length:J

    ushr-long v6, v6, p4

    const-wide/32 v12, 0x7fffffff

    cmp-long v3, v6, v12

    if-ltz v3, :cond_2

    .line 255
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "RandomAccessFile too big for chunk size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 258
    :cond_2
    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->length:J

    ushr-long v6, v6, p4

    long-to-int v3, v6

    add-int/lit8 v10, v3, 0x1

    .line 262
    .local v10, "nrBuffers":I
    new-array v3, v10, [Ljava/nio/ByteBuffer;

    iput-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 264
    const-wide/16 v4, 0x0

    .line 265
    .local v4, "bufferStart":J
    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 266
    .local v2, "rafc":Ljava/nio/channels/FileChannel;
    const/4 v8, 0x0

    .local v8, "bufNr":I
    :goto_0
    if-ge v8, v10, :cond_4

    .line 267
    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->length:J

    iget-wide v12, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSize:J

    add-long/2addr v12, v4

    cmp-long v3, v6, v12

    if-lez v3, :cond_3

    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSize:J

    :goto_1
    long-to-int v9, v6

    .line 271
    .local v9, "bufSize":I
    iget-object v11, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    int-to-long v6, v9

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v3

    aput-object v3, v11, v8

    .line 272
    int-to-long v6, v9

    add-long/2addr v4, v6

    .line 266
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 267
    .end local v9    # "bufSize":I
    :cond_3
    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->length:J

    sub-long/2addr v6, v4

    goto :goto_1

    .line 274
    :cond_4
    const-wide/16 v6, 0x0

    invoke-virtual {p0, v6, v7}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->seek(J)V

    .line 275
    return-void
.end method

.method private unsetBuffers()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 420
    iput-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 421
    iput-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    .line 422
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    .line 423
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 394
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    if-nez v3, :cond_0

    .line 395
    new-instance v3, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "MMapIndexInput already closed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 397
    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    .line 398
    .local v1, "clone":Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    const/4 v3, 0x1

    iput-boolean v3, v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->isClone:Z

    .line 400
    sget-boolean v3, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    iget-object v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    if-eq v3, v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 401
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    new-array v3, v3, [Ljava/nio/ByteBuffer;

    iput-object v3, v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 402
    const/4 v0, 0x0

    .local v0, "bufNr":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 403
    iget-object v3, v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v4

    aput-object v4, v3, v0

    .line 402
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 406
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    iget-object v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    monitor-enter v4

    .line 413
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 414
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416
    return-object v1

    .line 407
    :catch_0
    move-exception v2

    .line 408
    .local v2, "ioe":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Should never happen: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 414
    .end local v2    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    :try_start_0
    iget-boolean v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->isClone:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v3, :cond_1

    .line 444
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->unsetBuffers()V

    .line 446
    return-void

    .line 431
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 432
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    .line 433
    .local v1, "clone":Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    sget-boolean v3, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-boolean v3, v1, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->isClone:Z

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 437
    .end local v1    # "clone":Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 444
    :catchall_1
    move-exception v3

    invoke-direct {p0}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->unsetBuffers()V

    throw v3

    .line 434
    .restart local v1    # "clone":Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_4
    invoke-direct {v1}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->unsetBuffers()V

    goto :goto_0

    .line 436
    .end local v1    # "clone":Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clones:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 437
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 439
    const/4 v3, 0x0

    :try_start_5
    iput-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    .line 440
    const/4 v0, 0x0

    .local v0, "bufNr":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 441
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->this$0:Lorg/apache/lucene/store/MMapDirectory;

    iget-object v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/MMapDirectory;->cleanMapping(Ljava/nio/ByteBuffer;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 440
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 457
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilePointer()J
    .locals 6

    .prologue
    .line 356
    :try_start_0
    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    int-to-long v2, v1

    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSizePower:I

    shl-long/2addr v2, v1

    iget-object v1, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    return-wide v2

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "npe":Ljava/lang/NullPointerException;
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "MMapIndexInput already closed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 451
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 389
    iget-wide v0, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->length:J

    return-wide v0
.end method

.method public readByte()B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 290
    :goto_0
    return v2

    .line 281
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    :cond_0
    iget v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    .line 284
    iget v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 285
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "read past EOF: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 287
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    iget v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    aget-object v2, v2, v3

    iput-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    .line 288
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 289
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    goto :goto_0

    .line 291
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 292
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MMapIndexInput already closed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readBytes([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, p2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 318
    :goto_0
    return-void

    .line 300
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Ljava/nio/BufferUnderflowException;
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 302
    .local v0, "curAvail":I
    :goto_1
    if-le p3, v0, :cond_1

    .line 303
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, p2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 304
    sub-int/2addr p3, v0

    .line 305
    add-int/2addr p2, v0

    .line 306
    iget v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    .line 307
    iget v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    iget-object v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v4, v4

    if-lt v3, v4, :cond_0

    .line 308
    new-instance v3, Ljava/io/EOFException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "read past EOF: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 310
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    iget v4, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    aget-object v3, v3, v4

    iput-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    .line 311
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 312
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    goto :goto_1

    .line 314
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, p2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 315
    .end local v0    # "curAvail":I
    .end local v1    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v2

    .line 316
    .local v2, "npe":Ljava/lang/NullPointerException;
    new-instance v3, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "MMapIndexInput already closed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public readInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 336
    :goto_0
    return v2

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    goto :goto_0

    .line 337
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 338
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MMapIndexInput already closed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readLong()J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getLong()J
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v2

    .line 347
    :goto_0
    return-wide v2

    .line 346
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    goto :goto_0

    .line 348
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 349
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MMapIndexInput already closed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readShort()S
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getShort()S
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 325
    :goto_0
    return v2

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readShort()S

    move-result v2

    goto :goto_0

    .line 326
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 327
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MMapIndexInput already closed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public seek(J)V
    .locals 11
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    .line 365
    iget v5, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSizePower:I

    shr-long v6, p1, v5

    long-to-int v2, v6

    .line 367
    .local v2, "bi":I
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    aget-object v1, v5, v2

    .line 368
    .local v1, "b":Ljava/nio/ByteBuffer;
    iget-wide v6, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->chunkSizeMask:J

    and-long/2addr v6, p1

    long-to-int v5, v6

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 370
    iput v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBufIndex:I

    .line 371
    iput-object v1, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->curBuf:Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 385
    return-void

    .line 372
    .end local v1    # "b":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v0

    .line 373
    .local v0, "aioobe":Ljava/lang/ArrayIndexOutOfBoundsException;
    cmp-long v5, p1, v8

    if-gez v5, :cond_0

    .line 374
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Seeking to negative position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 376
    :cond_0
    new-instance v5, Ljava/io/EOFException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "seek past EOF: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 377
    .end local v0    # "aioobe":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v3

    .line 378
    .local v3, "iae":Ljava/lang/IllegalArgumentException;
    cmp-long v5, p1, v8

    if-gez v5, :cond_1

    .line 379
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Seeking to negative position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 381
    :cond_1
    new-instance v5, Ljava/io/EOFException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "seek past EOF: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 382
    .end local v3    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v4

    .line 383
    .local v4, "npe":Ljava/lang/NullPointerException;
    new-instance v5, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "MMapIndexInput already closed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v5
.end method
