.class Lorg/apache/lucene/store/transform/LRUChunkCache$1;
.super Ljava/util/LinkedHashMap;
.source "LRUChunkCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/transform/LRUChunkCache;-><init>(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/ref/SoftReference",
        "<[B>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/transform/LRUChunkCache;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/transform/LRUChunkCache;IFZ)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # F
    .param p4, "x2"    # Z

    .prologue
    .line 41
    iput-object p1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache$1;->this$0:Lorg/apache/lucene/store/transform/LRUChunkCache;

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/ref/SoftReference",
            "<[B>;>;)Z"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/ref/SoftReference<[B>;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/LRUChunkCache$1;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache$1;->this$0:Lorg/apache/lucene/store/transform/LRUChunkCache;

    # getter for: Lorg/apache/lucene/store/transform/LRUChunkCache;->cacheSize:I
    invoke-static {v1}, Lorg/apache/lucene/store/transform/LRUChunkCache;->access$000(Lorg/apache/lucene/store/transform/LRUChunkCache;)I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
