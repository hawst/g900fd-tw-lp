.class public Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;
.super Ljava/lang/Object;
.source "InflateDataTransformer.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;


# instance fields
.field private inflateCount:I

.field private inflater:Ljava/util/zip/Inflater;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;-><init>(I)V

    .line 41
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1
    .param p1, "inflateCount"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflateCount:I

    .line 36
    new-instance v0, Ljava/util/zip/Inflater;

    invoke-direct {v0}, Ljava/util/zip/Inflater;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflater:Ljava/util/zip/Inflater;

    .line 37
    return-void
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 0

    .prologue
    .line 70
    return-object p0
.end method

.method public setConfig([B)V
    .locals 1
    .param p1, "pData"    # [B

    .prologue
    .line 74
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput v0, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflateCount:I

    .line 75
    return-void
.end method

.method public declared-synchronized transform([BII[BI)I
    .locals 7
    .param p1, "inBytes"    # [B
    .param p2, "inOffset"    # I
    .param p3, "inLength"    # I
    .param p4, "outBytes"    # [B
    .param p5, "maxOutLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflateCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v5, :cond_0

    if-ne p3, p5, :cond_2

    .line 49
    :cond_0
    const/4 v3, -0x1

    .line 65
    :cond_1
    monitor-exit p0

    return v3

    .line 51
    :cond_2
    move-object v0, p1

    .line 52
    .local v0, "buffer":[B
    move v4, p2

    .line 53
    .local v4, "offset":I
    move v3, p3

    .line 54
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_1
    iget v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflateCount:I

    if-ge v2, v5, :cond_1

    .line 55
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflater:Ljava/util/zip/Inflater;

    invoke-virtual {v5}, Ljava/util/zip/Inflater;->reset()V

    .line 56
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflater:Ljava/util/zip/Inflater;

    invoke-virtual {v5, v0, v4, v3}, Ljava/util/zip/Inflater;->setInput([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    :try_start_2
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;->inflater:Ljava/util/zip/Inflater;

    const/4 v6, 0x0

    invoke-virtual {v5, p4, v6, p5}, Ljava/util/zip/Inflater;->inflate([BII)I
    :try_end_2
    .catch Ljava/util/zip/DataFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 62
    move-object v0, p4

    .line 63
    const/4 v4, 0x0

    .line 54
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    .line 60
    .local v1, "ex":Ljava/util/zip/DataFormatException;
    :try_start_3
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 48
    .end local v0    # "buffer":[B
    .end local v1    # "ex":Ljava/util/zip/DataFormatException;
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "offset":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method
