.class public Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
.super Ljava/lang/Object;
.source "DataDecryptor.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;


# instance fields
.field private algorithm:Ljava/lang/String;

.field private cipher:Ljavax/crypto/Cipher;

.field private deepCopy:Z

.field private iv:[B

.field private keyLength:I

.field private password:Ljava/lang/String;

.field private salt:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;[BZ)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "salt"    # [B
    .param p3, "deepCopy"    # Z

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->password:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->salt:[B

    .line 51
    iput-boolean p3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->deepCopy:Z

    .line 52
    return-void
.end method

.method private initCipher()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    .line 56
    const-string/jumbo v4, "PBKDF2WithHmacSHA1"

    invoke-static {v4}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v0

    .line 57
    .local v0, "factory":Ljavax/crypto/SecretKeyFactory;
    new-instance v2, Ljavax/crypto/spec/PBEKeySpec;

    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->password:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->salt:[B

    const/16 v6, 0x400

    iget v7, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->keyLength:I

    invoke-direct {v2, v4, v5, v6, v7}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 58
    .local v2, "spec":Ljava/security/spec/KeySpec;
    invoke-virtual {v0, v2}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 59
    .local v3, "tmp":Ljavax/crypto/SecretKey;
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v3}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->algorithm:Ljava/lang/String;

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-direct {v1, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 60
    .local v1, "secret":Ljavax/crypto/SecretKey;
    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->algorithm:Ljava/lang/String;

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->cipher:Ljavax/crypto/Cipher;

    .line 61
    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    array-length v4, v4

    if-lez v4, :cond_0

    .line 62
    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->cipher:Ljavax/crypto/Cipher;

    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    invoke-direct {v5, v6}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v4, v8, v1, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 67
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->cipher:Ljavax/crypto/Cipher;

    invoke-virtual {v4, v8, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    goto :goto_0
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 5

    .prologue
    .line 85
    iget-boolean v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->deepCopy:Z

    if-eqz v2, :cond_2

    .line 86
    new-instance v0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;

    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->password:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->salt:[B

    iget-boolean v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->deepCopy:Z

    invoke-direct {v0, v2, v3, v4}, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;-><init>(Ljava/lang/String;[BZ)V

    .line 87
    .local v0, "dec":Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->algorithm:Ljava/lang/String;

    iput-object v2, v0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->algorithm:Ljava/lang/String;

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    iput-object v2, v0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    .line 91
    :cond_0
    iget v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->keyLength:I

    iput v2, v0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->keyLength:I

    .line 93
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->algorithm:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 94
    invoke-direct {v0}, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->initCipher()V

    .line 103
    .end local v0    # "dec":Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
    :goto_0
    return-object v0

    .line 96
    .restart local v0    # "dec":Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Cipher algorithm not specified"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :catch_0
    move-exception v1

    .line 99
    .local v1, "ex":Ljava/security/GeneralSecurityException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .end local v0    # "dec":Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
    .end local v1    # "ex":Ljava/security/GeneralSecurityException;
    :cond_2
    move-object v0, p0

    .line 103
    goto :goto_0
.end method

.method public declared-synchronized setConfig([B)V
    .locals 5
    .param p1, "pData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    new-instance v1, Lorg/apache/lucene/store/transform/ByteIndexInput;

    invoke-direct {v1, p1}, Lorg/apache/lucene/store/transform/ByteIndexInput;-><init>([B)V

    .line 71
    .local v1, "input":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->algorithm:Ljava/lang/String;

    .line 72
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->keyLength:I

    .line 73
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 74
    .local v2, "ivlen":I
    new-array v3, v2, [B

    iput-object v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    .line 75
    iget-object v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->iv:[B

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 76
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->initCipher()V
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    monitor-exit p0

    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "ex":Ljava/security/GeneralSecurityException;
    :try_start_2
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 70
    .end local v0    # "ex":Ljava/security/GeneralSecurityException;
    .end local v1    # "input":Lorg/apache/lucene/store/IndexInput;
    .end local v2    # "ivlen":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized transform([BII[BI)I
    .locals 3
    .param p1, "inBytes"    # [B
    .param p2, "inOffset"    # I
    .param p3, "inLength"    # I
    .param p4, "outBytes"    # [B
    .param p5, "maxOutLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;->cipher:Ljavax/crypto/Cipher;

    invoke-virtual {v2, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->doFinal([BII[B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 110
    .local v0, "cnt":I
    monitor-exit p0

    return v0

    .line 112
    .end local v0    # "cnt":I
    :catch_0
    move-exception v1

    .line 113
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
