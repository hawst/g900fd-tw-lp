.class public Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;
.super Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
.source "SequentialTransformedIndexOutput.java"


# instance fields
.field private buffer:[B

.field private bufferOffset:I

.field private bufferPosition:J

.field private length:J

.field private maxBufferOffset:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexOutput;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/TransformedDirectory;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .param p3, "pCunkSize"    # I
    .param p4, "pTransformer"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p5, "compressedDir"    # Lorg/apache/lucene/store/transform/TransformedDirectory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p4, p5}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/TransformedDirectory;)V

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    .line 71
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    .line 72
    new-array v0, p3, [B

    iput-object v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    .line 75
    const-wide/16 v0, -0x1

    invoke-virtual {p2, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->writeConfig()V

    .line 78
    return-void
.end method

.method private declared-synchronized flushBuffer()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->maxBufferOffset:I

    iget v1, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    if-le v0, v1, :cond_0

    .line 121
    iget v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->maxBufferOffset:I

    iput v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    .line 123
    :cond_0
    iget v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    if-lez v0, :cond_1

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    iget-wide v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    iget v1, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    invoke-virtual {p0, v0, v2, v3, v1}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->writeChunk([BJI)V

    .line 125
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    iget v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    iput v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->maxBufferOffset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_1
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->flush()V

    .line 135
    iget-object v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    .line 136
    .local v0, "pos":J
    iget-object v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 138
    invoke-super {p0}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-void

    .line 134
    .end local v0    # "pos":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->flushBuffer()V

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 145
    return-void
.end method

.method public declared-synchronized getFilePointer()J
    .locals 4

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    iget v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v2, v2

    add-long/2addr v0, v2

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public length()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->length:J

    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 192
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->length:J

    .line 194
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v0

    goto :goto_0
.end method

.method public seek(J)V
    .locals 5
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_3

    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    iget v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    iget v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->maxBufferOffset:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_3

    .line 165
    :cond_0
    iget v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    iget v1, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->maxBufferOffset:I

    if-le v0, v1, :cond_1

    .line 166
    iget v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    iput v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->maxBufferOffset:I

    .line 168
    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    .line 185
    :cond_2
    :goto_0
    return-void

    .line 172
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_2

    .line 173
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->flushBuffer()V

    .line 175
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->length:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_4

    .line 176
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Warning seek beyond eof "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 179
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->length:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 180
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->length:J

    .line 182
    :cond_5
    iput-wide p1, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferPosition:J

    goto :goto_0
.end method

.method public sync()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->flush()V

    .line 200
    return-void
.end method

.method protected updateFileLength(J)V
    .locals 5
    .param p1, "pLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 205
    iget-object v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 206
    return-void
.end method

.method public declared-synchronized writeByte(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    iget-object v1, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 83
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->flushBuffer()V

    .line 85
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    aput-byte p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized writeBytes([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v3, v3

    iget v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    sub-int/2addr v3, v4

    if-ge p3, v3, :cond_1

    .line 92
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    invoke-static {p1, p2, v3, v4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    iget v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :cond_0
    monitor-exit p0

    return-void

    .line 96
    :cond_1
    move v1, p3

    .line 97
    .local v1, "toWrite":I
    move v2, p2

    .line 98
    .local v2, "woffset":I
    :cond_2
    if-lez v1, :cond_0

    .line 99
    move v0, v1

    .line 100
    .local v0, "maxWrite":I
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v3, v3

    iget v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    sub-int/2addr v3, v4

    if-le v0, v3, :cond_3

    .line 101
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v3, v3

    iget v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    sub-int v0, v3, v4

    .line 103
    :cond_3
    if-gez v0, :cond_4

    .line 104
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Invalid flush"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    .end local v0    # "maxWrite":I
    .end local v1    # "toWrite":I
    .end local v2    # "woffset":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 106
    .restart local v0    # "maxWrite":I
    .restart local v1    # "toWrite":I
    .restart local v2    # "woffset":I
    :cond_4
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    invoke-static {p1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 107
    add-int/2addr v2, v0

    .line 108
    sub-int/2addr v1, v0

    .line 109
    iget v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    add-int/2addr v3, v0

    iput v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    .line 110
    iget v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    iget-object v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v4, v4

    if-ne v3, v4, :cond_5

    .line 111
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->flushBuffer()V

    .line 113
    :cond_5
    iget v3, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    iget-object v4, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v4, v4

    if-le v3, v4, :cond_2

    .line 114
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Incorrect offset "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->bufferOffset:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;->buffer:[B

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
