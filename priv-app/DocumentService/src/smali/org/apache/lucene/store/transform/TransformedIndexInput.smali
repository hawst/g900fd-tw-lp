.class public Lorg/apache/lucene/store/transform/TransformedIndexInput;
.super Lorg/apache/lucene/store/IndexInput;
.source "TransformedIndexInput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final READ_BUFFER_LOCK:Ljava/lang/Object;

.field private buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

.field private bufferInflatedPos:J

.field private bufferOffset:I

.field private bufferPos:J

.field private bufsize:I

.field private cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

.field private chunkPos:I

.field private chunkPositions:[J

.field private crc:Ljava/util/zip/CRC32;

.field private endOfFilePosition:J

.field private firstOverwrittenPos:[I

.field private inflatedLengths:[I

.field private inflatedPositions:[J

.field private inflater:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

.field private input:Lorg/apache/lucene/store/IndexInput;

.field private length:J

.field private maxChunkSize:I

.field private maxInflatedLength:I

.field private maxReadSize:I

.field private memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

.field private name:Ljava/lang/String;

.field private orderedChunks:Z

.field private overwrittenChunks:[I

.field private readBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Lorg/apache/lucene/store/transform/DecompressionChunkCache;Lorg/apache/lucene/store/transform/SharedBufferCache;)V
    .locals 10
    .param p1, "pName"    # Ljava/lang/String;
    .param p2, "openInput"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "inflater"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;
    .param p4, "cache"    # Lorg/apache/lucene/store/transform/DecompressionChunkCache;
    .param p5, "memCache"    # Lorg/apache/lucene/store/transform/SharedBufferCache;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/16 v4, 0x2000

    const/4 v6, 0x0

    .line 115
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexInput;-><init>()V

    .line 113
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->READ_BUFFER_LOCK:Ljava/lang/Object;

    .line 116
    iput-object p2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    .line 117
    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->crc:Ljava/util/zip/CRC32;

    .line 118
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 119
    iput-wide v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 120
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    .line 121
    iput-object p1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->name:Ljava/lang/String;

    .line 122
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 123
    iput-object p4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    .line 124
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferInflatedPos:J

    .line 125
    iput-object p3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflater:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    .line 126
    invoke-virtual {p5, v4}, Lorg/apache/lucene/store/transform/SharedBufferCache;->newBuffer(I)Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    .line 127
    new-array v2, v4, [B

    iput-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    .line 128
    iput-object p5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    .line 130
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x10

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 131
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    .line 132
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 133
    .local v1, "configLen":I
    new-array v0, v1, [B

    .line 134
    .local v0, "config":[B
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2, v0, v6, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 135
    invoke-interface {p3, v0}, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;->setConfig([B)V

    .line 137
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readChunkDirectory()V

    .line 138
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    array-length v2, v2

    if-lez v2, :cond_0

    .line 139
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v4, v3, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 141
    :cond_0
    iput-wide v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 142
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    .line 143
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 144
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 149
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buildOverwritten()V

    .line 150
    return-void

    .line 146
    .end local v0    # "config":[B
    .end local v1    # "configLen":I
    :cond_1
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "Invalid chunked file"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method static synthetic access$000(Lorg/apache/lucene/store/transform/TransformedIndexInput;)[J
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/store/transform/TransformedIndexInput;

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    return-object v0
.end method

.method private buildOverwritten()V
    .locals 18

    .prologue
    .line 658
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v13, v13

    new-array v12, v13, [I

    .line 659
    .local v12, "tov":[I
    const/4 v9, 0x0

    .line 660
    .local v9, "pos":I
    const-wide/16 v10, 0x0

    .line 661
    .local v10, "maxPos":J
    const-wide/16 v6, 0x0

    .line 662
    .local v6, "cpos":J
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v13, v13

    if-ge v5, v13, :cond_2

    .line 663
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v14, v13, v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v13, v13, v5

    int-to-long v0, v13

    move-wide/from16 v16, v0

    add-long v6, v14, v16

    .line 664
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v14, v13, v5

    cmp-long v13, v14, v10

    if-gez v13, :cond_0

    .line 665
    aput v5, v12, v9

    .line 666
    add-int/lit8 v9, v9, 0x1

    .line 668
    :cond_0
    cmp-long v13, v6, v10

    if-lez v13, :cond_1

    .line 669
    move-wide v10, v6

    .line 662
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 672
    :cond_2
    new-array v13, v9, [I

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    .line 673
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    const/4 v15, 0x0

    invoke-static {v12, v13, v14, v15, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 674
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    array-length v13, v13

    if-lez v13, :cond_5

    .line 675
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v13, v13

    new-array v13, v13, [I

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->firstOverwrittenPos:[I

    .line 676
    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v13, v13

    if-ge v5, v13, :cond_5

    .line 677
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->firstOverwrittenPos:[I

    const/4 v14, -0x1

    aput v14, v13, v5

    .line 678
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    if-ge v8, v9, :cond_4

    .line 679
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    aget v14, v14, v8

    aget-wide v2, v13, v14

    .line 680
    .local v2, "bPos":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    aget v14, v14, v8

    aget v4, v13, v14

    .line 681
    .local v4, "bSize":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v14, v13, v5

    cmp-long v13, v2, v14

    if-ltz v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v14, v13, v5

    int-to-long v0, v4

    move-wide/from16 v16, v0

    add-long v16, v16, v2

    cmp-long v13, v14, v16

    if-gez v13, :cond_3

    .line 682
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->firstOverwrittenPos:[I

    aput v8, v13, v5

    .line 678
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 676
    .end local v2    # "bPos":J
    .end local v4    # "bSize":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 688
    .end local v8    # "j":I
    :cond_5
    return-void
.end method

.method private checkOverwriten(J)V
    .locals 31
    .param p1, "currentPos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v24, v0

    if-eqz v24, :cond_3

    .line 160
    const-wide/16 v10, 0x0

    .line 161
    .local v10, "inflatedPos":J
    const-wide/16 v6, 0x0

    .line 162
    .local v6, "chunkPosition":J
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 163
    .local v15, "obufsize":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_3

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->overwrittenChunks:[I

    move-object/from16 v24, v0

    aget v8, v24, v9

    .line 165
    .local v8, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v24, v0

    aget-wide v24, v24, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    move-object/from16 v26, v0

    aget v26, v26, v8

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v24, v24, v26

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    move-wide/from16 v26, v0

    cmp-long v24, v24, v26

    if-ltz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v24, v0

    aget-wide v24, v24, v8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    add-long v26, v26, v28

    cmp-long v24, v24, v26

    if-gez v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    move-object/from16 v24, v0

    aget-wide v24, v24, v8

    cmp-long v24, p1, v24

    if-gez v24, :cond_0

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v24, v0

    aget-wide v24, v24, v8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    move-wide/from16 v26, v0

    sub-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    .line 167
    .local v23, "tbufsize":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-gt v0, v1, :cond_0

    .line 169
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v24, v0

    aget-wide v10, v24, v8

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    move-object/from16 v24, v0

    aget-wide v6, v24, v8

    .line 176
    .end local v23    # "tbufsize":I
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v0, v15, :cond_1

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v19, v0

    .line 180
    .local v19, "original":Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    move-object/from16 v24, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lorg/apache/lucene/store/transform/SharedBufferCache;->newBuffer(I)Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    .line 182
    .local v12, "lpos":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    move-wide/from16 v16, v0

    .line 183
    .local v16, "obufferPos":J
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v21, v0

    .line 184
    .local v21, "overOffset":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    move/from16 v20, v0

    .line 185
    .local v20, "overChunkPos":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    move/from16 v18, v0

    .line 188
    .local v18, "origBuffOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 189
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 190
    move-object/from16 v0, p0

    iput v8, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    .line 191
    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 192
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readDecompressImp(Z)V

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12, v13}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 196
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v24, v0

    add-int v24, v24, v21

    move/from16 v0, v24

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 198
    .local v14, "nbufsize":I
    new-array v0, v14, [B

    move-object/from16 v22, v0

    .line 199
    .local v22, "result":[B
    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move-object/from16 v2, v22

    move/from16 v3, v26

    invoke-static {v0, v1, v2, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    if-gez v21, :cond_2

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    move-object/from16 v24, v0

    move/from16 v0, v21

    neg-int v0, v0

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v27, v0

    add-int v27, v27, v21

    move-object/from16 v0, v24

    move/from16 v1, v25

    move-object/from16 v2, v22

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 205
    :goto_1
    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    .line 208
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 209
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 210
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/transform/SharedBufferCache;->release(Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;)V

    .line 163
    .end local v12    # "lpos":J
    .end local v14    # "nbufsize":I
    .end local v16    # "obufferPos":J
    .end local v18    # "origBuffOffset":I
    .end local v19    # "original":Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    .end local v20    # "overChunkPos":I
    .end local v21    # "overOffset":I
    .end local v22    # "result":[B
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 203
    .restart local v12    # "lpos":J
    .restart local v14    # "nbufsize":I
    .restart local v16    # "obufferPos":J
    .restart local v18    # "origBuffOffset":I
    .restart local v19    # "original":Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    .restart local v20    # "overChunkPos":I
    .restart local v21    # "overOffset":I
    .restart local v22    # "result":[B
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move/from16 v26, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move-object/from16 v2, v22

    move/from16 v3, v21

    move/from16 v4, v26

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 217
    .end local v6    # "chunkPosition":J
    .end local v8    # "i":I
    .end local v9    # "j":I
    .end local v10    # "inflatedPos":J
    .end local v12    # "lpos":J
    .end local v14    # "nbufsize":I
    .end local v15    # "obufsize":I
    .end local v16    # "obufferPos":J
    .end local v18    # "origBuffOffset":I
    .end local v19    # "original":Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    .end local v20    # "overChunkPos":I
    .end local v21    # "overOffset":I
    .end local v22    # "result":[B
    :cond_3
    return-void
.end method

.method private detectOrder()V
    .locals 6

    .prologue
    .line 691
    const-wide/16 v0, 0x0

    .line 692
    .local v0, "cpos":J
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->orderedChunks:Z

    .line 693
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 694
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v4, v3, v2

    cmp-long v3, v4, v0

    if-eqz v3, :cond_0

    .line 695
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->orderedChunks:Z

    .line 693
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 699
    :cond_1
    return-void
.end method

.method private findFirstChunk(J)I
    .locals 9
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 593
    const/4 v0, 0x0

    .line 594
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v1, v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_1

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxInflatedLength:I

    if-lez v1, :cond_1

    .line 595
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v1, v1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-gtz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v2, v1, v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v1, v1, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-gtz v1, :cond_6

    .line 596
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 599
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    iget v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxInflatedLength:I

    int-to-long v2, v2

    sub-long v2, p1, v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 600
    if-gez v0, :cond_2

    .line 601
    const/4 v0, 0x0

    .line 603
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-gtz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v2, v1, v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v1, v1, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-gtz v1, :cond_4

    .line 604
    :cond_3
    const/4 v0, 0x0

    .line 606
    :cond_4
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v1, v1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-gtz v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v2, v1, v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v1, v1, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-gtz v1, :cond_6

    .line 607
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 611
    :cond_6
    sget-boolean v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->$assertionsDisabled:Z

    if-nez v1, :cond_7

    if-gez v0, :cond_7

    new-instance v1, Ljava/lang/AssertionError;

    const-string/jumbo v2, "Invalid chunk offset table"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 612
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    array-length v1, v1

    if-ne v0, v1, :cond_9

    iget-wide v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    cmp-long v1, p1, v2

    if-ltz v1, :cond_9

    .line 613
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .line 619
    .end local v0    # "i":I
    :cond_8
    return v0

    .line 616
    .restart local v0    # "i":I
    :cond_9
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    array-length v1, v1

    if-lt v0, v1, :cond_8

    .line 617
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Incorrect chunk directory. Seek pos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " last chunkPos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v3, v4

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    iget-object v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget v3, v3, v6

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private readChunkDirectory()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    .line 224
    iget-wide v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    cmp-long v7, v8, v12

    if-gez v7, :cond_0

    .line 227
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->scanPositions()V

    .line 268
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->detectOrder()V

    .line 269
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->sortChunks()V

    .line 271
    :goto_1
    return-void

    .line 231
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x8

    sub-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 232
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    iput-wide v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    .line 233
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    iget-wide v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 234
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readDecompressImp(Z)V

    .line 235
    new-instance v2, Lorg/apache/lucene/store/transform/ByteIndexInput;

    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v7, v7, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    invoke-direct {v2, v7}, Lorg/apache/lucene/store/transform/ByteIndexInput;-><init>([B)V

    .line 238
    .local v2, "in":Lorg/apache/lucene/store/IndexInput;
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    const/4 v8, 0x0

    iput-object v8, v7, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    .line 239
    const/16 v7, 0x200

    new-array v7, v7, [B

    iput-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    .line 240
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 241
    .local v0, "entries":I
    new-array v7, v0, [J

    iput-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    .line 242
    new-array v7, v0, [J

    iput-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    .line 243
    new-array v7, v0, [I

    iput-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    .line 244
    const-wide/16 v4, 0x0

    .line 245
    .local v4, "lastFilePos":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v0, :cond_5

    .line 246
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v8

    aput-wide v8, v7, v1

    .line 247
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v8

    aput-wide v8, v7, v1

    .line 248
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    aput v3, v7, v1

    .line 249
    .local v3, "infLen":I
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v8, v7, v1

    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v7, v7, v1

    int-to-long v10, v7

    add-long/2addr v8, v10

    iget-wide v10, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    cmp-long v7, v8, v10

    if-gtz v7, :cond_1

    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v8, v7, v1

    cmp-long v7, v8, v12

    if-ltz v7, :cond_1

    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v7, v7, v1

    if-gez v7, :cond_2

    .line 251
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->scanPositions()V

    goto/16 :goto_1

    .line 254
    :cond_2
    iget v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    if-le v3, v7, :cond_3

    .line 255
    iput v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    .line 257
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v8, v7, v1

    sub-long/2addr v8, v4

    long-to-int v6, v8

    .line 258
    .local v6, "readLen":I
    iget v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxReadSize:I

    if-ge v7, v6, :cond_4

    .line 259
    iput v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxReadSize:I

    .line 261
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v4, v7, v1

    .line 245
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 264
    .end local v3    # "infLen":I
    .end local v6    # "readLen":I
    :cond_5
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    new-array v8, v8, [B

    iput-object v8, v7, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    .line 265
    iget v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxReadSize:I

    add-int/lit8 v7, v7, 0x4

    new-array v7, v7, [B

    iput-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    .line 266
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto/16 :goto_0
.end method

.method private readDecompress()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 428
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Over EOF"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "  input="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "  max="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 430
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readDecompressImp(Z)V

    .line 431
    return-void
.end method

.method private declared-synchronized readDecompressImp(Z)V
    .locals 22
    .param p1, "hasDeflatedPosition"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    int-to-long v6, v4

    add-long/2addr v2, v6

    move-object/from16 v0, p0

    iput-wide v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 435
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    cmp-long v2, v2, v6

    if-ltz v2, :cond_0

    .line 436
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Beyond eof read "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ">="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 440
    :cond_0
    if-eqz p1, :cond_5

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->orderedChunks:Z

    if-nez v2, :cond_5

    .line 441
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->seekToChunk()I

    move-result v18

    .line 445
    .local v18, "locBufferOffset":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v14

    .line 446
    .local v14, "currentPos":J
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 447
    .local v10, "cachepos":J
    const/4 v8, 0x0

    .line 448
    .local v8, "cacheData":[B
    if-eqz p1, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    if-eqz v2, :cond_1

    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    invoke-interface {v2, v10, v11}, Lorg/apache/lucene/store/transform/DecompressionChunkCache;->lock(J)V

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    invoke-interface {v2, v10, v11}, Lorg/apache/lucene/store/transform/DecompressionChunkCache;->getChunk(J)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 453
    :cond_1
    if-eqz v8, :cond_9

    .line 454
    :try_start_2
    array-length v2, v8

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 455
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget v2, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    .line 456
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget v3, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    .line 457
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/store/transform/SharedBufferCache;->newBuffer(I)Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    .line 461
    :cond_2
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    invoke-static {v8, v2, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 462
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_8

    .line 463
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    add-int/lit8 v4, v4, 0x1

    aget-wide v6, v3, v4

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 525
    :cond_3
    :goto_2
    if-eqz p1, :cond_4

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    if-eqz v2, :cond_4

    .line 526
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    invoke-interface {v2, v10, v11}, Lorg/apache/lucene/store/transform/DecompressionChunkCache;->unlock(J)V

    .line 529
    :cond_4
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 530
    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferInflatedPos:J

    .line 531
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 532
    monitor-exit p0

    return-void

    .line 443
    .end local v8    # "cacheData":[B
    .end local v10    # "cachepos":J
    .end local v14    # "currentPos":J
    .end local v18    # "locBufferOffset":I
    :cond_5
    const/16 v18, 0x0

    .restart local v18    # "locBufferOffset":I
    goto/16 :goto_0

    .line 458
    .restart local v8    # "cacheData":[B
    .restart local v10    # "cachepos":J
    .restart local v14    # "currentPos":J
    :cond_6
    :try_start_4
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v3, v3

    if-le v2, v3, :cond_2

    .line 459
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    new-array v3, v3, [B

    iput-object v3, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 525
    :catchall_1
    move-exception v2

    if-eqz p1, :cond_7

    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    if-eqz v3, :cond_7

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    invoke-interface {v3, v10, v11}, Lorg/apache/lucene/store/transform/DecompressionChunkCache;->unlock(J)V

    :cond_7
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 465
    :cond_8
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_2

    .line 468
    :cond_9
    if-eqz p1, :cond_a

    .line 469
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v16

    .line 470
    .local v16, "inflatedPos":J
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v2, v2, v16

    if-eqz v2, :cond_a

    .line 471
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Invalid compression chunk location "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "!="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 474
    .end local v16    # "inflatedPos":J
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v12

    .line 475
    .local v12, "chunkCRC":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v5

    .line 476
    .local v5, "compressed":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 478
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget v2, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_b

    .line 479
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget v3, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    .line 480
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/store/transform/SharedBufferCache;->newBuffer(I)Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    .line 482
    :cond_b
    if-nez p1, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v3, v3

    if-le v2, v3, :cond_c

    .line 483
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    new-array v3, v3, [B

    iput-object v3, v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    .line 487
    :cond_c
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferInflatedPos:J

    cmp-long v2, v2, v14

    if-nez v2, :cond_d

    .line 488
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    int-to-long v0, v5

    move-wide/from16 v20, v0

    add-long v6, v6, v20

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto/16 :goto_2

    .line 490
    :cond_d
    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferInflatedPos:J

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->READ_BUFFER_LOCK:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 494
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    array-length v2, v2

    if-le v5, v2, :cond_e

    .line 495
    new-array v2, v5, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    .line 497
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 498
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflater:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v6, v6, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    invoke-interface/range {v2 .. v7}, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;->transform([BII[BI)I

    move-result v9

    .line 500
    .local v9, "lcnt":I
    if-gez v9, :cond_f

    .line 501
    move v9, v5

    .line 502
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v4, v4, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 504
    :cond_f
    monitor-exit v19
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 505
    :try_start_8
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    if-eq v9, v2, :cond_10

    .line 506
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Incorrect buffer size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "!="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 504
    .end local v9    # "lcnt":I
    :catchall_2
    move-exception v2

    :try_start_9
    monitor-exit v19
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v2

    .line 509
    .restart local v9    # "lcnt":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->crc:Ljava/util/zip/CRC32;

    if-eqz v2, :cond_11

    .line 510
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->crc:Ljava/util/zip/CRC32;

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->reset()V

    .line 511
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->crc:Ljava/util/zip/CRC32;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    invoke-virtual {v2, v3, v4, v6}, Ljava/util/zip/CRC32;->update([BII)V

    .line 512
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->crc:Ljava/util/zip/CRC32;

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    cmp-long v2, v2, v12

    if-eqz v2, :cond_11

    .line 513
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "CRC mismatch"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 516
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->orderedChunks:Z

    if-nez v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->firstOverwrittenPos:[I

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->firstOverwrittenPos:[I

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    aget v2, v2, v3

    if-ltz v2, :cond_12

    .line 517
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->checkOverwriten(J)V

    .line 519
    :cond_12
    if-eqz p1, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    if-eqz v2, :cond_3

    .line 520
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->cache:Lorg/apache/lucene/store/transform/DecompressionChunkCache;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    invoke-interface {v2, v10, v11, v3, v4}, Lorg/apache/lucene/store/transform/DecompressionChunkCache;->putChunk(J[BI)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_2
.end method

.method private scanPositions()V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v10

    .line 321
    .local v10, "fileLen":J
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 322
    .local v7, "chunks":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 323
    .local v13, "inflated":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v17, "sizes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    .line 325
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v18

    cmp-long v18, v18, v10

    if-gez v18, :cond_2

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    .line 327
    .local v4, "chunkPos":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v14

    .line 328
    .local v14, "inflatedPos":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v8

    .line 329
    .local v8, "crc":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v6

    .line 330
    .local v6, "chunkSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v16

    .line 331
    .local v16, "inflatedSize":I
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v18

    if-le v0, v1, :cond_0

    .line 335
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    .line 337
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxReadSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    move/from16 v1, v16

    if-ge v0, v1, :cond_1

    .line 338
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxReadSize:I

    .line 340
    :cond_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    move-wide/from16 v18, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v20

    int-to-long v0, v6

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto/16 :goto_0

    .line 343
    .end local v4    # "chunkPos":J
    .end local v6    # "chunkSize":I
    .end local v8    # "crc":J
    .end local v14    # "inflatedPos":J
    .end local v16    # "inflatedSize":I
    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    .line 344
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [J

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    .line 345
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [J

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    .line 346
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v12, v0, :cond_3

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    aput v18, v19, v12

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    move-object/from16 v19, v0

    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    aput-wide v20, v19, v12

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    move-object/from16 v19, v0

    invoke-interface {v7, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    aput-wide v20, v19, v12

    .line 346
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 351
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxChunkSize:I

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    .line 352
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxReadSize:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x4

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readBuffer:[B

    .line 353
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->detectOrder()V

    .line 354
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->sortChunks()V

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    .line 356
    return-void
.end method

.method private seekToChunk()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 376
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    aget-wide v4, v3, v4

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 415
    :cond_0
    :goto_0
    return v2

    .line 380
    :cond_1
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v4, v4

    if-ge v3, v4, :cond_3

    .line 381
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    add-int/lit8 v4, v4, 0x1

    aget-wide v4, v3, v4

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 388
    iget-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->findFirstChunk(J)I

    move-result v0

    .line 390
    .local v0, "fchunk":I
    move v1, v0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v3, v3

    if-ge v1, v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v4, v3, v1

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_5

    .line 391
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v4, v3, v1

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    .line 392
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v6, v3, v1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    .line 394
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v4, v4, v1

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 397
    :cond_2
    iput v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    goto :goto_0

    .line 386
    .end local v0    # "fchunk":I
    .end local v1    # "i":I
    :cond_3
    new-instance v3, Ljava/io/EOFException;

    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    throw v3

    .line 390
    .restart local v0    # "fchunk":I
    .restart local v1    # "i":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 406
    :cond_5
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Warning chunk "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "  at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " not cought by overwriten. Using fallback"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 407
    move v1, v0

    :goto_2
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 408
    iget-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v6, v3, v1

    cmp-long v3, v4, v6

    if-ltz v3, :cond_6

    iget-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v6, v3, v1

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget v3, v3, v1

    int-to-long v8, v3

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_6

    .line 409
    iget-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v6, v3, v1

    sub-long/2addr v4, v6

    long-to-int v2, v4

    .line 410
    .local v2, "newOffset":I
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v4, v3, v1

    iput-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 411
    iput v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    .line 412
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v6, v3, v1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 413
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v4, v4, v1

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto/16 :goto_0

    .line 407
    .end local v2    # "newOffset":I
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 423
    :cond_7
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Chunk not found for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private sortChunks()V
    .locals 8

    .prologue
    .line 277
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    new-array v4, v5, [Ljava/lang/Integer;

    .line 278
    .local v4, "sortOrder":[Ljava/lang/Integer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 279
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 278
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :cond_0
    new-instance v5, Lorg/apache/lucene/store/transform/TransformedIndexInput$1;

    invoke-direct {v5, p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput$1;-><init>(Lorg/apache/lucene/store/transform/TransformedIndexInput;)V

    invoke-static {v4, v5}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 295
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    new-array v3, v5, [J

    .line 296
    .local v3, "newInflatedPositions":[J
    const/4 v0, 0x0

    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 297
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-wide v6, v5, v6

    aput-wide v6, v3, v0

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 299
    :cond_1
    iput-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    .line 300
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    new-array v1, v5, [J

    .line 301
    .local v1, "newChunkPositions":[J
    const/4 v0, 0x0

    :goto_2
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    if-ge v0, v5, :cond_2

    .line 302
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-wide v6, v5, v6

    aput-wide v6, v1, v0

    .line 301
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 304
    :cond_2
    iput-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    .line 305
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    new-array v2, v5, [I

    .line 306
    .local v2, "newInflatedLengths":[I
    const/4 v0, 0x0

    :goto_3
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    array-length v5, v5

    if-ge v0, v5, :cond_4

    .line 307
    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget v5, v5, v6

    aput v5, v2, v0

    .line 308
    aget v5, v2, v0

    iget v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxInflatedLength:I

    if-le v5, v6, :cond_3

    .line 309
    aget v5, v2, v0

    iput v5, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->maxInflatedLength:I

    .line 306
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 312
    :cond_4
    iput-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedLengths:[I

    .line 313
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 570
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;

    .line 571
    .local v0, "clone":Lorg/apache/lucene/store/transform/TransformedIndexInput;
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/IndexInput;

    iput-object v1, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    .line 573
    iget-object v1, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget v2, v1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    .line 575
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflater:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    iput-object v1, v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflater:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    .line 576
    return-object v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 581
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 582
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/transform/SharedBufferCache;->release(Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;)V

    .line 583
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    .line 584
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 588
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    iget v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 651
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    return-wide v0
.end method

.method public readByte()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 536
    iget v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    if-lt v0, v1, :cond_0

    .line 537
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readDecompress()V

    .line 539
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v0, v0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 8
    .param p1, "b"    # [B
    .param p2, "boffset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 544
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    sub-int/2addr v3, v4

    if-ge p3, v3, :cond_1

    .line 545
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    invoke-static {v3, v4, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 546
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 566
    :cond_0
    return-void

    .line 549
    :cond_1
    move v0, p3

    .line 550
    .local v0, "llen":I
    move v1, p2

    .line 551
    .local v1, "loffset":I
    :cond_2
    :goto_0
    if-lez v0, :cond_0

    .line 552
    move v2, v0

    .line 553
    .local v2, "toCopy":I
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    sub-int/2addr v3, v4

    if-le v2, v3, :cond_3

    .line 554
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    sub-int v2, v3, v4

    .line 556
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->buffer:Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    invoke-static {v3, v4, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 558
    add-int/2addr v1, v2

    .line 559
    sub-int/2addr v0, v2

    .line 560
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    add-int/2addr v3, v2

    iput v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 562
    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    iget v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    if-lt v3, v4, :cond_2

    if-lez v0, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->endOfFilePosition:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 563
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readDecompress()V

    goto :goto_0
.end method

.method public seek(J)V
    .locals 11
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 626
    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v1, p1, v6

    if-ltz v1, :cond_1

    .line 627
    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    sub-long v2, p1, v6

    .line 628
    .local v2, "ioffset":J
    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    int-to-long v6, v1

    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    .line 629
    long-to-int v1, v2

    iput v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 647
    .end local v2    # "ioffset":J
    :cond_0
    return-void

    .line 633
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->findFirstChunk(J)I

    move-result v0

    .line 634
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J

    aget-wide v4, v1, v0

    .line 635
    .local v4, "newBufferPos":J
    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    if-nez v1, :cond_3

    .line 636
    :cond_2
    iput-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    .line 637
    iput v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPos:I

    .line 638
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    .line 639
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->chunkPositions:[J

    aget-wide v6, v6, v0

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 640
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->readDecompress()V

    .line 642
    :cond_3
    iget-wide v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferPos:J

    sub-long v6, p1, v6

    long-to-int v1, v6

    iput v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    .line 643
    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    iget v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    if-le v1, v6, :cond_4

    .line 644
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v6, "Incorrect chunk directory"

    invoke-direct {v1, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 646
    :cond_4
    sget-boolean v1, Lorg/apache/lucene/store/transform/TransformedIndexInput;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    if-ltz v1, :cond_5

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    iget v6, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufsize:I

    if-ge v1, v6, :cond_5

    iget v1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->bufferOffset:I

    int-to-long v6, v1

    iget-wide v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput;->length:J

    cmp-long v1, v6, v8

    if-ltz v1, :cond_0

    :cond_5
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method
