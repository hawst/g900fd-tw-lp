.class public Lorg/apache/lucene/store/transform/CompressedIndexDirectory;
.super Lorg/apache/lucene/store/transform/TransformedDirectory;
.source "CompressedIndexDirectory.java"


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "pDirectory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {p1}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/transform/CompressedIndexDirectory;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 2
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;

    invoke-direct {v0}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>()V

    new-instance v1, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;

    invoke-direct {v1}, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;IZ)V
    .locals 6
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "chunkSize"    # I
    .param p3, "directCompression"    # Z

    .prologue
    .line 37
    new-instance v3, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;

    invoke-direct {v3}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>()V

    new-instance v4, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;

    invoke-direct {v4}, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V

    .line 38
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;IZ)V
    .locals 7
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "tempDir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "chunkSize"    # I
    .param p4, "directStore"    # Z

    .prologue
    .line 33
    new-instance v4, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;

    invoke-direct {v4}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>()V

    new-instance v5, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;

    invoke-direct {v5}, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V

    .line 34
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Z)V
    .locals 2
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "directCompression"    # Z

    .prologue
    .line 41
    new-instance v0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;

    invoke-direct {v0}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>()V

    new-instance v1, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;

    invoke-direct {v1}, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;-><init>()V

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V

    .line 42
    return-void
.end method
