.class public Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
.super Ljava/lang/Object;
.source "SharedBufferCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/transform/SharedBufferCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SharedBuffer"
.end annotation


# instance fields
.field data:[B

.field volatile refCount:I


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    .line 16
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    .line 17
    return-void
.end method

.method synthetic constructor <init>(ILorg/apache/lucene/store/transform/SharedBufferCache$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lorg/apache/lucene/store/transform/SharedBufferCache$1;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;-><init>(I)V

    return-void
.end method


# virtual methods
.method public toString(I)Ljava/lang/String;
    .locals 4
    .param p1, "bufSize"    # I

    .prologue
    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .local v1, "result":Ljava/lang/StringBuilder;
    const-string/jumbo v2, "refc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 23
    if-eqz v0, :cond_0

    .line 24
    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    aget-byte v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_1
    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
