.class public Lorg/apache/lucene/store/transform/TransformedDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "TransformedDirectory.java"


# static fields
.field private static memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;


# instance fields
.field private cacheSize:I

.field private final chunkSize:I

.field private final directStore:Z

.field private final nested:Lorg/apache/lucene/store/Directory;

.field private final openOutputs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;",
            ">;"
        }
    .end annotation
.end field

.field private readTransformer:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

.field private seqNum:I

.field private storeTransformer:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

.field private final tempDirectory:Lorg/apache/lucene/store/Directory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lorg/apache/lucene/store/transform/SharedBufferCache;

    invoke-direct {v0}, Lorg/apache/lucene/store/transform/SharedBufferCache;-><init>()V

    sput-object v0, Lorg/apache/lucene/store/transform/TransformedDirectory;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V
    .locals 7
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "chunkSize"    # I
    .param p3, "storeTransformer"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p4, "readTransformer"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;
    .param p5, "directStore"    # Z

    .prologue
    .line 141
    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V

    .line 142
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V
    .locals 1
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "tempDir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "chunkSize"    # I
    .param p4, "storeTransformer"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p5, "readTransformer"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;
    .param p6, "directStore"    # Z

    .prologue
    .line 153
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 106
    const/16 v0, 0x64

    iput v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->cacheSize:I

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->seqNum:I

    .line 155
    iput-object p1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    .line 156
    iput p3, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->chunkSize:I

    .line 157
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    .line 158
    iput-object p2, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->tempDirectory:Lorg/apache/lucene/store/Directory;

    .line 159
    iput-boolean p6, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->directStore:Z

    .line 160
    iput-object p4, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->storeTransformer:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    .line 161
    iput-object p5, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->readTransformer:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    .line 162
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;)V
    .locals 1
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "storeTransformer"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p3, "readTransformer"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V

    .line 118
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V
    .locals 6
    .param p1, "nested"    # Lorg/apache/lucene/store/Directory;
    .param p2, "storeTransformer"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p3, "readTransformer"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;
    .param p4, "directStore"    # Z

    .prologue
    .line 128
    const/high16 v2, 0x20000

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Z)V

    .line 129
    return-void
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->clearLock(Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    monitor-enter v4

    .line 231
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 232
    .local v0, "copy":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;

    .line 233
    .local v2, "out":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    invoke-virtual {v2}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->close()V

    goto :goto_0

    .line 234
    .end local v0    # "copy":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "out":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "copy":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;>;"
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3}, Lorg/apache/lucene/store/Directory;->close()V

    .line 236
    return-void
.end method

.method public createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    .line 208
    .local v2, "out":Lorg/apache/lucene/store/IndexOutput;
    iget-boolean v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->directStore:Z

    if-eqz v1, :cond_0

    .line 209
    new-instance v0, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;

    iget v3, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->chunkSize:I

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->storeTransformer:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/SequentialTransformedIndexOutput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexOutput;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/TransformedDirectory;)V

    .line 213
    .local v0, "output":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    monitor-enter v3

    .line 214
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    return-object v0

    .line 211
    .end local v0    # "output":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    :cond_0
    new-instance v0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;

    iget-object v5, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->tempDirectory:Lorg/apache/lucene/store/Directory;

    iget v8, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->chunkSize:I

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->storeTransformer:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    move-object v3, v0

    move-object v4, p0

    move-object v6, v2

    move-object v7, p1

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/store/transform/TransformedIndexOutput;-><init>(Lorg/apache/lucene/store/transform/TransformedDirectory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IndexOutput;Ljava/lang/String;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;)V

    .restart local v0    # "output":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    goto :goto_0

    .line 215
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    const/16 v5, 0x8

    invoke-virtual {v4, p1, v5}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 192
    .local v1, "input":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    .line 193
    .local v2, "length":J
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 196
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 197
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/transform/TransformedDirectory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 198
    .local v0, "in":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v2

    .line 199
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 201
    .end local v0    # "in":Lorg/apache/lucene/store/IndexInput;
    :cond_0
    return-wide v2
.end method

.method public fileModified(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileModified(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCachedPageCount()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->cacheSize:I

    return v0
.end method

.method public getLockFactory()Lorg/apache/lucene/store/LockFactory;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v0

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 255
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    return-object v0
.end method

.method declared-synchronized nextSequence()I
    .locals 1

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->seqNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->seqNum:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v4, 0x0

    .line 222
    .local v4, "cache":Lorg/apache/lucene/store/transform/DecompressionChunkCache;
    iget v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->cacheSize:I

    if-lez v0, :cond_0

    .line 223
    new-instance v4, Lorg/apache/lucene/store/transform/LRUChunkCache;

    .end local v4    # "cache":Lorg/apache/lucene/store/transform/DecompressionChunkCache;
    iget v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->cacheSize:I

    invoke-direct {v4, v0}, Lorg/apache/lucene/store/transform/LRUChunkCache;-><init>(I)V

    .line 225
    .restart local v4    # "cache":Lorg/apache/lucene/store/transform/DecompressionChunkCache;
    :cond_0
    new-instance v0, Lorg/apache/lucene/store/transform/TransformedIndexInput;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->readTransformer:Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    sget-object v5, Lorg/apache/lucene/store/transform/TransformedDirectory;->memCache:Lorg/apache/lucene/store/transform/SharedBufferCache;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/TransformedIndexInput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Lorg/apache/lucene/store/transform/DecompressionChunkCache;Lorg/apache/lucene/store/transform/SharedBufferCache;)V

    return-object v0
.end method

.method release(Ljava/lang/String;)V
    .locals 2
    .param p1, "pName"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    monitor-enter v1

    .line 292
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    monitor-exit v1

    .line 294
    return-void

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setCachePageCount(I)V
    .locals 0
    .param p1, "pCached"    # I

    .prologue
    .line 301
    iput p1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->cacheSize:I

    .line 302
    return-void
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 261
    return-void
.end method

.method public sync(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    monitor-enter v2

    .line 270
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->openOutputs:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;

    .line 271
    .local v0, "output":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->sync()V

    .line 275
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->sync(Ljava/lang/String;)V

    .line 276
    return-void

    .line 271
    .end local v0    # "output":Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public touchFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedDirectory;->nested:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->touchFile(Ljava/lang/String;)V

    .line 182
    return-void
.end method
