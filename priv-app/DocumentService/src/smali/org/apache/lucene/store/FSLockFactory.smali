.class public abstract Lorg/apache/lucene/store/FSLockFactory;
.super Lorg/apache/lucene/store/LockFactory;
.source "FSLockFactory.java"


# instance fields
.field protected lockDir:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/lucene/store/LockFactory;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/FSLockFactory;->lockDir:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public getLockDir()Ljava/io/File;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/store/FSLockFactory;->lockDir:Ljava/io/File;

    return-object v0
.end method

.method protected final setLockDir(Ljava/io/File;)V
    .locals 2
    .param p1, "lockDir"    # Ljava/io/File;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/store/FSLockFactory;->lockDir:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "You can set the lock directory for this factory only once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/store/FSLockFactory;->lockDir:Ljava/io/File;

    .line 44
    return-void
.end method
