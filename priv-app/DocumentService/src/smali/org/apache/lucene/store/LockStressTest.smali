.class public Lorg/apache/lucene/store/LockStressTest;
.super Ljava/lang/Object;
.source "LockStressTest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 15
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v14, 0x1

    .line 36
    array-length v12, p0

    const/4 v13, 0x6

    if-eq v12, v13, :cond_0

    .line 37
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "\nUsage: java org.apache.lucene.store.LockStressTest myID verifierHostOrIP verifierPort lockFactoryClassName lockDirName sleepTime\n\n  myID = int from 0 .. 255 (should be unique for test process)\n  verifierHostOrIP = host name or IP address where LockVerifyServer is running\n  verifierPort = port that LockVerifyServer is listening on\n  lockFactoryClassName = primary LockFactory class that we will use\n  lockDirName = path to the lock directory (only set for Simple/NativeFSLockFactory\n  sleepTimeMS = milliseconds to pause betweeen each lock obtain/release\n\nYou should run multiple instances of this process, each with its own\nunique ID, and each pointing to the same lock directory, to verify\nthat locking is working correctly.\n\nMake sure you are first running LockVerifyServer.\n\n"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 52
    invoke-static {v14}, Ljava/lang/System;->exit(I)V

    .line 55
    :cond_0
    const/4 v12, 0x0

    aget-object v12, p0, v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 57
    .local v6, "myID":I
    if-ltz v6, :cond_1

    const/16 v12, 0xff

    if-le v6, v12, :cond_2

    .line 58
    :cond_1
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "myID must be a unique int 0..255"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 59
    invoke-static {v14}, Ljava/lang/System;->exit(I)V

    .line 62
    :cond_2
    aget-object v9, p0, v14

    .line 63
    .local v9, "verifierHost":Ljava/lang/String;
    const/4 v12, 0x2

    aget-object v12, p0, v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 64
    .local v10, "verifierPort":I
    const/4 v12, 0x3

    aget-object v5, p0, v12

    .line 65
    .local v5, "lockFactoryClassName":Ljava/lang/String;
    const/4 v12, 0x4

    aget-object v3, p0, v12

    .line 66
    .local v3, "lockDirName":Ljava/lang/String;
    const/4 v12, 0x5

    aget-object v12, p0, v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 70
    .local v8, "sleepTimeMS":I
    :try_start_0
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v12

    const-class v13, Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v12, v13}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/store/LockFactory;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 81
    .local v4, "lockFactory":Lorg/apache/lucene/store/LockFactory;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .local v2, "lockDir":Ljava/io/File;
    instance-of v12, v4, Lorg/apache/lucene/store/FSLockFactory;

    if-eqz v12, :cond_3

    move-object v12, v4

    .line 84
    check-cast v12, Lorg/apache/lucene/store/FSLockFactory;

    invoke-virtual {v12, v2}, Lorg/apache/lucene/store/FSLockFactory;->setLockDir(Ljava/io/File;)V

    .line 87
    :cond_3
    const-string/jumbo v12, "test"

    invoke-virtual {v4, v12}, Lorg/apache/lucene/store/LockFactory;->setLockPrefix(Ljava/lang/String;)V

    .line 89
    new-instance v11, Lorg/apache/lucene/store/VerifyingLockFactory;

    int-to-byte v12, v6

    invoke-direct {v11, v12, v4, v9, v10}, Lorg/apache/lucene/store/VerifyingLockFactory;-><init>(BLorg/apache/lucene/store/LockFactory;Ljava/lang/String;I)V

    .line 91
    .local v11, "verifyLF":Lorg/apache/lucene/store/LockFactory;
    const-string/jumbo v12, "test.lock"

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/LockFactory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v1

    .line 95
    .local v1, "l":Lorg/apache/lucene/store/Lock;
    :goto_0
    const/4 v7, 0x0

    .line 98
    .local v7, "obtained":Z
    const-wide/16 v12, 0xa

    :try_start_1
    invoke-virtual {v1, v12, v13}, Lorg/apache/lucene/store/Lock;->obtain(J)Z
    :try_end_1
    .catch Lorg/apache/lucene/store/LockObtainFailedException; {:try_start_1 .. :try_end_1} :catch_4

    move-result v7

    .line 103
    :goto_1
    if-eqz v7, :cond_4

    .line 104
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "l"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v1}, Lorg/apache/lucene/store/Lock;->release()V

    .line 107
    :cond_4
    int-to-long v12, v8

    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 71
    .end local v1    # "l":Lorg/apache/lucene/store/Lock;
    .end local v2    # "lockDir":Ljava/io/File;
    .end local v4    # "lockFactory":Lorg/apache/lucene/store/LockFactory;
    .end local v7    # "obtained":Z
    .end local v11    # "verifyLF":Lorg/apache/lucene/store/LockFactory;
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "IllegalAccessException when instantiating LockClass "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 73
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/InstantiationException;
    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "InstantiationException when instantiating LockClass "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 75
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "unable to cast LockClass "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " instance to a LockFactory"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 77
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_3
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "unable to find LockClass "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 99
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v1    # "l":Lorg/apache/lucene/store/Lock;
    .restart local v2    # "lockDir":Ljava/io/File;
    .restart local v4    # "lockFactory":Lorg/apache/lucene/store/LockFactory;
    .restart local v7    # "obtained":Z
    .restart local v11    # "verifyLF":Lorg/apache/lucene/store/LockFactory;
    :catch_4
    move-exception v0

    .line 100
    .local v0, "e":Lorg/apache/lucene/store/LockObtainFailedException;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "x"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
