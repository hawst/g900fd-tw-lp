.class public abstract Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
.super Lorg/apache/lucene/store/IndexOutput;
.source "AbstractTransformedIndexOutput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;
    }
.end annotation


# instance fields
.field private chunkDirectory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private compressedDir:Lorg/apache/lucene/store/transform/TransformedDirectory;

.field private crc:Ljava/util/zip/CRC32;

.field private deflatedBuffer:[B

.field private deflater:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

.field protected name:Ljava/lang/String;

.field protected output:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/TransformedDirectory;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .param p3, "deflater"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p4, "compressedDir"    # Lorg/apache/lucene/store/transform/TransformedDirectory;

    .prologue
    .line 116
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 117
    iput-object p1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->name:Ljava/lang/String;

    .line 118
    iput-object p2, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    .line 119
    iput-object p3, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflater:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    .line 120
    iput-object p4, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->compressedDir:Lorg/apache/lucene/store/transform/TransformedDirectory;

    .line 121
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflatedBuffer:[B

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    .line 123
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->crc:Ljava/util/zip/CRC32;

    .line 125
    return-void
.end method

.method private addDirectoryEntry(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 2
    .param p1, "pInflatedPos"    # Ljava/lang/Long;
    .param p2, "pDeflatedPos"    # Ljava/lang/Long;
    .param p3, "pLength"    # Ljava/lang/Integer;

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;

    invoke-direct {v1, p1, p2, p3}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    return-void
.end method

.method private declared-synchronized writeChunkImp([BI)V
    .locals 7
    .param p1, "pData"    # [B
    .param p2, "pSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflatedBuffer:[B

    array-length v0, v0

    array-length v1, p1

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    .line 155
    array-length v0, p1

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflatedBuffer:[B

    .line 157
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflater:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    const/4 v2, 0x0

    iget-object v4, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflatedBuffer:[B

    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflatedBuffer:[B

    array-length v5, v1

    move-object v1, p1

    move v3, p2

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;->transform([BII[BI)I

    move-result v6

    .line 160
    .local v6, "count":I
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->crc:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->reset()V

    .line 161
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->crc:Ljava/util/zip/CRC32;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Ljava/util/zip/CRC32;->update([BII)V

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->crc:Ljava/util/zip/CRC32;

    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 165
    if-gez v6, :cond_1

    .line 167
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 170
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 172
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :goto_0
    monitor-exit p0

    return-void

    .line 175
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 181
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflatedBuffer:[B

    invoke-virtual {v0, v1, v6}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 154
    .end local v6    # "count":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->flush()V

    .line 194
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->length()J

    move-result-wide v4

    .line 196
    .local v4, "directoryPos":J
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 197
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lorg/apache/lucene/store/transform/StreamIndexOutput;

    invoke-direct {v1, v0}, Lorg/apache/lucene/store/transform/StreamIndexOutput;-><init>(Ljava/io/OutputStream;)V

    .line 200
    .local v1, "boutput":Lorg/apache/lucene/store/IndexOutput;
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v1, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 201
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_0

    .line 202
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;

    invoke-virtual {v8}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->getInflatedPos()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 203
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;

    invoke-virtual {v8}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->getDeflatedPos()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 204
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->chunkDirectory:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;

    invoke-virtual {v8}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->getLength()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v1, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 201
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 207
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 208
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 211
    .local v2, "dir":[B
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    .line 214
    .local v6, "entryPos":J
    array-length v8, v2

    invoke-direct {p0, v2, v8}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->writeChunkImp([BI)V

    .line 217
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 218
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 219
    invoke-virtual {p0, v4, v5}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->updateFileLength(J)V

    .line 220
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 221
    iget-object v8, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->compressedDir:Lorg/apache/lucene/store/transform/TransformedDirectory;

    iget-object v9, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/transform/TransformedDirectory;->release(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    monitor-exit p0

    return-void

    .line 193
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "boutput":Lorg/apache/lucene/store/IndexOutput;
    .end local v2    # "dir":[B
    .end local v3    # "i":I
    .end local v4    # "directoryPos":J
    .end local v6    # "entryPos":J
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8
.end method

.method public abstract sync()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract updateFileLength(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected declared-synchronized writeChunk([BJI)V
    .locals 4
    .param p1, "pData"    # [B
    .param p2, "pPosition"    # J
    .param p4, "pSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->addDirectoryEntry(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 148
    invoke-direct {p0, p1, p4}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->writeChunkImp([BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    monitor-exit p0

    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected writeConfig()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->deflater:Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;->getConfig()[B

    move-result-object v0

    .line 129
    .local v0, "config":[B
    if-eqz v0, :cond_0

    .line 130
    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    array-length v2, v0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_0
.end method
