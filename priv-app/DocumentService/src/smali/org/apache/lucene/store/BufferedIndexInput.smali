.class public abstract Lorg/apache/lucene/store/BufferedIndexInput;
.super Lorg/apache/lucene/store/IndexInput;
.source "BufferedIndexInput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BUFFER_SIZE:I = 0x400


# instance fields
.field protected buffer:[B

.field private bufferLength:I

.field private bufferPosition:I

.field private bufferSize:I

.field private bufferStart:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/apache/lucene/store/BufferedIndexInput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/BufferedIndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 47
    const-string/jumbo v0, "anonymous BuffereIndexInput"

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "bufferSize"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 58
    const-string/jumbo v0, "anonymous BuffereIndexInput"

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "resourceDesc"    # Ljava/lang/String;

    .prologue
    .line 51
    const/16 v0, 0x400

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 29
    const/16 v0, 0x400

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 34
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 35
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 64
    invoke-direct {p0, p2}, Lorg/apache/lucene/store/BufferedIndexInput;->checkBufferSize(I)V

    .line 65
    iput p2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    .line 66
    return-void
.end method

.method private checkBufferSize(I)V
    .locals 3
    .param p1, "bufferSize"    # I

    .prologue
    .line 105
    if-gtz p1, :cond_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bufferSize must be greater than 0 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    return-void
.end method

.method private refill()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 258
    iget-wide v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v8, v3

    add-long v4, v6, v8

    .line 259
    .local v4, "start":J
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    int-to-long v6, v3

    add-long v0, v4, v6

    .line 260
    .local v0, "end":J
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->length()J

    move-result-wide v6

    cmp-long v3, v0, v6

    if-lez v3, :cond_0

    .line 261
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->length()J

    move-result-wide v0

    .line 262
    :cond_0
    sub-long v6, v0, v4

    long-to-int v2, v6

    .line 263
    .local v2, "newLength":I
    if-gtz v2, :cond_1

    .line 264
    new-instance v3, Ljava/io/EOFException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "read past EOF: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 266
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-nez v3, :cond_2

    .line 267
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    new-array v3, v3, [B

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/BufferedIndexInput;->newBuffer([B)V

    .line 268
    iget-wide v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    invoke-virtual {p0, v6, v7}, Lorg/apache/lucene/store/BufferedIndexInput;->seekInternal(J)V

    .line 270
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    invoke-virtual {p0, v3, v10, v2}, Lorg/apache/lucene/store/BufferedIndexInput;->readInternal([BII)V

    .line 271
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 272
    iput-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 273
    iput v10, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 274
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 308
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/BufferedIndexInput;

    .line 310
    .local v0, "clone":Lorg/apache/lucene/store/BufferedIndexInput;
    const/4 v1, 0x0

    iput-object v1, v0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    .line 311
    iput v2, v0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 312
    iput v2, v0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 313
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->getFilePointer()J

    move-result-wide v2

    iput-wide v2, v0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 315
    return-object v0
.end method

.method public copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 4
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 341
    sget-boolean v0, Lorg/apache/lucene/store/BufferedIndexInput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    cmp-long v0, p2, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "numBytes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 343
    :cond_0
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_2

    .line 344
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    if-ne v0, v1, :cond_1

    .line 345
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->refill()V

    .line 347
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/BufferedIndexInput;->flushBuffer(Lorg/apache/lucene/store/IndexOutput;J)I

    move-result v0

    int-to-long v0, v0

    sub-long/2addr p2, v0

    goto :goto_0

    .line 349
    :cond_2
    return-void
.end method

.method protected final flushBuffer(Lorg/apache/lucene/store/IndexOutput;J)I
    .locals 4
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int v0, v1, v2

    .line 329
    .local v0, "toCopy":I
    int-to-long v2, v0

    cmp-long v1, v2, p2

    if-lez v1, :cond_0

    .line 330
    long-to-int v0, p2

    .line 332
    :cond_0
    if-lez v0, :cond_1

    .line 333
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-virtual {p1, v1, v2, v0}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 334
    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 336
    :cond_1
    return v0
.end method

.method public final getBufferSize()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    return v0
.end method

.method public final getFilePointer()J
    .locals 4

    .prologue
    .line 286
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method protected newBuffer([B)V
    .locals 0
    .param p1, "newBuffer"    # [B

    .prologue
    .line 96
    iput-object p1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    .line 97
    return-void
.end method

.method public final readByte()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    if-lt v0, v1, :cond_0

    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->refill()V

    .line 41
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public final readBytes([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/store/BufferedIndexInput;->readBytes([BIIZ)V

    .line 112
    return-void
.end method

.method public final readBytes([BIIZ)V
    .locals 9
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "useBuffer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 117
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-gt p3, v3, :cond_1

    .line 119
    if-lez p3, :cond_0

    .line 120
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-static {v3, v4, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    :cond_0
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 162
    :goto_0
    return-void

    .line 124
    :cond_1
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int v2, v3, v4

    .line 125
    .local v2, "available":I
    if-lez v2, :cond_2

    .line 126
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-static {v3, v4, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    add-int/2addr p2, v2

    .line 128
    sub-int/2addr p3, v2

    .line 129
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/2addr v3, v2

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 132
    :cond_2
    if-eqz p4, :cond_4

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    if-ge p3, v3, :cond_4

    .line 136
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->refill()V

    .line 137
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    if-ge v3, p3, :cond_3

    .line 139
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    invoke-static {v3, v8, p1, p2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    new-instance v3, Ljava/io/EOFException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "read past EOF: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 142
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    invoke-static {v3, v8, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    iput p3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    goto :goto_0

    .line 153
    :cond_4
    iget-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    int-to-long v6, p3

    add-long v0, v4, v6

    .line 154
    .local v0, "after":J
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->length()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-lez v3, :cond_5

    .line 155
    new-instance v3, Ljava/io/EOFException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "read past EOF: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 156
    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/BufferedIndexInput;->readInternal([BII)V

    .line 157
    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 158
    iput v8, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 159
    iput v8, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    goto/16 :goto_0
.end method

.method public final readInt()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    const/4 v0, 0x4

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 176
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 179
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    goto :goto_0
.end method

.method protected abstract readInternal([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final readLong()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    const/16 v2, 0x8

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-gt v2, v3, :cond_0

    .line 186
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int v0, v2, v3

    .line 188
    .local v0, "i1":I
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int v1, v2, v3

    .line 190
    .local v1, "i2":I
    int-to-long v2, v0

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    int-to-long v4, v1

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    .line 192
    .end local v0    # "i1":I
    .end local v1    # "i2":I
    :goto_0
    return-wide v2

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    goto :goto_0
.end method

.method public final readShort()S
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    const/4 v0, 0x2

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    .line 169
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readShort()S

    move-result v0

    goto :goto_0
.end method

.method public final readVInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v2, 0x5

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-gt v2, v3, :cond_2

    .line 199
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 200
    .local v0, "b":B
    and-int/lit8 v1, v0, 0x7f

    .line 201
    .local v1, "i":I
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_1

    .line 217
    .end local v0    # "b":B
    .end local v1    # "i":I
    :cond_0
    :goto_0
    return v1

    .line 202
    .restart local v0    # "b":B
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 203
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x7

    or-int/2addr v1, v2

    .line 204
    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_0

    .line 205
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 206
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v1, v2

    .line 207
    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 209
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x15

    or-int/2addr v1, v2

    .line 210
    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_0

    .line 211
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 213
    and-int/lit8 v2, v0, 0xf

    shl-int/lit8 v2, v2, 0x1c

    or-int/2addr v1, v2

    .line 214
    and-int/lit16 v2, v0, 0xf0

    if-eqz v2, :cond_0

    .line 215
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "Invalid vInt detected (too many bits)"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 217
    .end local v0    # "b":B
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    goto :goto_0
.end method

.method public final readVLong()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x7f

    .line 223
    const/16 v1, 0x9

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v4, v5

    if-gt v1, v4, :cond_2

    .line 224
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 225
    .local v0, "b":B
    int-to-long v4, v0

    and-long v2, v4, v6

    .line 226
    .local v2, "i":J
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_1

    .line 253
    .end local v0    # "b":B
    .end local v2    # "i":J
    :cond_0
    :goto_0
    return-wide v2

    .line 227
    .restart local v0    # "b":B
    .restart local v2    # "i":J
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 228
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/4 v1, 0x7

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 229
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 231
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0xe

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 232
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 234
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x15

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 235
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 237
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x1c

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 238
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 240
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x23

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 241
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 243
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x2a

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 244
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 246
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x31

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 247
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 249
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x38

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 250
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 251
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v4, "Invalid vLong detected (negative values disallowed)"

    invoke-direct {v1, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 253
    .end local v0    # "b":B
    .end local v2    # "i":J
    :cond_2
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    goto/16 :goto_0
.end method

.method public final seek(J)V
    .locals 5
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 290
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 291
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 298
    :goto_0
    return-void

    .line 293
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 294
    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 295
    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 296
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/store/BufferedIndexInput;->seekInternal(J)V

    goto :goto_0
.end method

.method protected abstract seekInternal(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final setBufferSize(I)V
    .locals 8
    .param p1, "newSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 70
    sget-boolean v4, Lorg/apache/lucene/store/BufferedIndexInput;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-eqz v4, :cond_1

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    iget-object v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    array-length v5, v5

    if-eq v4, v5, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "buffer="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " bufferSize="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " buffer.length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-eqz v6, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    array-length v3, v3

    :cond_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 71
    :cond_1
    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    if-eq p1, v4, :cond_2

    .line 72
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/BufferedIndexInput;->checkBufferSize(I)V

    .line 73
    iput p1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    .line 74
    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-eqz v4, :cond_2

    .line 78
    new-array v1, p1, [B

    .line 79
    .local v1, "newBuffer":[B
    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int v0, v4, v5

    .line 81
    .local v0, "leftInBuffer":I
    if-le v0, p1, :cond_3

    .line 82
    move v2, p1

    .line 85
    .local v2, "numToCopy":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-static {v4, v5, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    iget-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 87
    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 88
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 89
    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/BufferedIndexInput;->newBuffer([B)V

    .line 92
    .end local v0    # "leftInBuffer":I
    .end local v1    # "newBuffer":[B
    .end local v2    # "numToCopy":I
    :cond_2
    return-void

    .line 84
    .restart local v0    # "leftInBuffer":I
    .restart local v1    # "newBuffer":[B
    :cond_3
    move v2, v0

    .restart local v2    # "numToCopy":I
    goto :goto_0
.end method
