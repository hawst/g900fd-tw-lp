.class public abstract Lorg/apache/lucene/store/BufferedIndexOutput;
.super Lorg/apache/lucene/store/IndexOutput;
.source "BufferedIndexOutput.java"


# static fields
.field static final BUFFER_SIZE:I = 0x4000


# instance fields
.field private final buffer:[B

.field private bufferPosition:I

.field private bufferStart:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 26
    const/16 v0, 0x4000

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    .line 27
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    return-void
.end method

.method private flushBuffer([BI)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BII)V

    .line 100
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 114
    return-void
.end method

.method public flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BI)V

    .line 89
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 91
    return-void
.end method

.method protected abstract flushBuffer([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 122
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public abstract length()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public seek(J)V
    .locals 1
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 131
    iput-wide p1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    .line 132
    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    const/16 v1, 0x4000

    if-lt v0, v1, :cond_0

    .line 36
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 37
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    aput-byte p1, v0, v1

    .line 38
    return-void
.end method

.method public writeBytes([BII)V
    .locals 8
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    rsub-int v0, v3, 0x4000

    .line 49
    .local v0, "bytesLeft":I
    if-lt v0, p3, :cond_1

    .line 51
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    invoke-static {p1, p2, v3, v4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 54
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    rsub-int v3, v3, 0x4000

    if-nez v3, :cond_0

    .line 55
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    const/16 v3, 0x4000

    if-le p3, v3, :cond_3

    .line 60
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    if-lez v3, :cond_2

    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 63
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BII)V

    .line 64
    iget-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    goto :goto_0

    .line 67
    :cond_3
    const/4 v2, 0x0

    .line 69
    .local v2, "pos":I
    :cond_4
    :goto_1
    if-ge v2, p3, :cond_0

    .line 70
    sub-int v3, p3, v2

    if-ge v3, v0, :cond_5

    sub-int v1, p3, v2

    .line 71
    .local v1, "pieceLength":I
    :goto_2
    add-int v3, v2, p2

    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    invoke-static {p1, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    add-int/2addr v2, v1

    .line 73
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 75
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    rsub-int v0, v3, 0x4000

    .line 76
    if-nez v0, :cond_4

    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 78
    const/16 v0, 0x4000

    goto :goto_1

    .end local v1    # "pieceLength":I
    :cond_5
    move v1, v0

    .line 70
    goto :goto_2
.end method
