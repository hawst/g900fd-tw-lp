.class public Lorg/apache/lucene/store/MMapDirectory;
.super Lorg/apache/lucene/store/FSDirectory;
.source "MMapDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_BUFF:I

.field public static final UNMAP_SUPPORTED:Z


# instance fields
.field private chunkSizePower:I

.field private useUnmapHack:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 85
    const-class v3, Lorg/apache/lucene/store/MMapDirectory;

    invoke-virtual {v3}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    sput-boolean v2, Lorg/apache/lucene/store/MMapDirectory;->$assertionsDisabled:Z

    .line 87
    sget-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v2, :cond_1

    const/high16 v2, 0x40000000    # 2.0f

    :goto_0
    sput v2, Lorg/apache/lucene/store/MMapDirectory;->DEFAULT_MAX_BUFF:I

    .line 119
    :try_start_0
    const-string/jumbo v2, "sun.misc.Cleaner"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 120
    const-string/jumbo v2, "java.nio.DirectByteBuffer"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string/jumbo v3, "cleaner"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    const/4 v1, 0x1

    .line 126
    .local v1, "v":Z
    :goto_1
    sput-boolean v1, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    .line 127
    return-void

    .line 87
    .end local v1    # "v":Z
    :cond_1
    const/high16 v2, 0x10000000

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .restart local v1    # "v":Z
    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 86
    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    iput-boolean v0, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    .line 109
    sget v0, Lorg/apache/lucene/store/MMapDirectory;->DEFAULT_MAX_BUFF:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/MMapDirectory;->setMaxChunkSize(I)V

    .line 110
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 86
    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    iput-boolean v0, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    .line 99
    sget v0, Lorg/apache/lucene/store/MMapDirectory;->DEFAULT_MAX_BUFF:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/MMapDirectory;->setMaxChunkSize(I)V

    .line 100
    return-void
.end method


# virtual methods
.method final cleanMapping(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-boolean v2, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    if-eqz v2, :cond_0

    .line 163
    :try_start_0
    new-instance v2, Lorg/apache/lucene/store/MMapDirectory$1;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/store/MMapDirectory$1;-><init>(Lorg/apache/lucene/store/MMapDirectory;Ljava/nio/ByteBuffer;)V

    invoke-static {v2}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/security/PrivilegedActionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/security/PrivilegedActionException;
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "unable to unmap the mapped buffer"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 178
    .local v1, "ioe":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 179
    throw v1
.end method

.method public final getMaxChunkSize()I
    .locals 2

    .prologue
    .line 211
    const/4 v0, 0x1

    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    shl-int/2addr v0, v1

    return v0
.end method

.method public getUseUnmap()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    return v0
.end method

.method public openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0}, Lorg/apache/lucene/store/MMapDirectory;->ensureOpen()V

    .line 218
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/lucene/store/MMapDirectory;->getDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 219
    .local v0, "f":Ljava/io/File;
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string/jumbo v2, "r"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 221
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MMapIndexInput(path=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    invoke-direct {v2, p0, v3, v1, v4}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;-><init>(Lorg/apache/lucene/store/MMapDirectory;Ljava/lang/String;Ljava/io/RandomAccessFile;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 221
    return-object v2

    .line 223
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    throw v2
.end method

.method public final setMaxChunkSize(I)V
    .locals 2
    .param p1, "maxChunkSize"    # I

    .prologue
    .line 198
    if-gtz p1, :cond_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Maximum chunk size for mmap must be >0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1f

    iput v0, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    .line 202
    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    if-ltz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204
    :cond_2
    return-void
.end method

.method public setUseUnmap(Z)V
    .locals 2
    .param p1, "useUnmapHack"    # Z

    .prologue
    .line 142
    if-eqz p1, :cond_0

    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Unmap hack not supported on this platform!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_0
    iput-boolean p1, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    .line 145
    return-void
.end method
