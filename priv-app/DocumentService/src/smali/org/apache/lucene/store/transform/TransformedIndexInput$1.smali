.class Lorg/apache/lucene/store/transform/TransformedIndexInput$1;
.super Ljava/lang/Object;
.source "TransformedIndexInput.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/transform/TransformedIndexInput;->sortChunks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/transform/TransformedIndexInput;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/transform/TransformedIndexInput;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput$1;->this$0:Lorg/apache/lucene/store/transform/TransformedIndexInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Integer;Ljava/lang/Integer;)I
    .locals 8
    .param p1, "o1"    # Ljava/lang/Integer;
    .param p2, "o2"    # Ljava/lang/Integer;

    .prologue
    const-wide/16 v6, 0x0

    .line 284
    iget-object v2, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput$1;->this$0:Lorg/apache/lucene/store/transform/TransformedIndexInput;

    # getter for: Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J
    invoke-static {v2}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->access$000(Lorg/apache/lucene/store/transform/TransformedIndexInput;)[J

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-wide v2, v2, v3

    iget-object v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexInput$1;->this$0:Lorg/apache/lucene/store/transform/TransformedIndexInput;

    # getter for: Lorg/apache/lucene/store/transform/TransformedIndexInput;->inflatedPositions:[J
    invoke-static {v4}, Lorg/apache/lucene/store/transform/TransformedIndexInput;->access$000(Lorg/apache/lucene/store/transform/TransformedIndexInput;)[J

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aget-wide v4, v4, v5

    sub-long v0, v2, v4

    .line 285
    .local v0, "result":J
    cmp-long v2, v0, v6

    if-lez v2, :cond_0

    .line 286
    const/4 v2, 0x1

    .line 291
    :goto_0
    return v2

    .line 287
    :cond_0
    cmp-long v2, v0, v6

    if-gez v2, :cond_1

    .line 288
    const/4 v2, -0x1

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 281
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/store/transform/TransformedIndexInput$1;->compare(Ljava/lang/Integer;Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
