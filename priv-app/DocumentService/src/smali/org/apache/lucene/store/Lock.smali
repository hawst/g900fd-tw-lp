.class public abstract Lorg/apache/lucene/store/Lock;
.super Ljava/lang/Object;
.source "Lock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/Lock$With;
    }
.end annotation


# static fields
.field public static final LOCK_OBTAIN_WAIT_FOREVER:J = -0x1L

.field public static LOCK_POLL_INTERVAL:J


# instance fields
.field protected failureReason:Ljava/lang/Throwable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lorg/apache/lucene/store/Lock;->LOCK_POLL_INTERVAL:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    return-void
.end method


# virtual methods
.method public abstract isLocked()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract obtain()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public obtain(J)Z
    .locals 13
    .param p1, "lockWaitTimeout"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v10, 0x0

    iput-object v10, p0, Lorg/apache/lucene/store/Lock;->failureReason:Ljava/lang/Throwable;

    .line 72
    invoke-virtual {p0}, Lorg/apache/lucene/store/Lock;->obtain()Z

    move-result v2

    .line 73
    .local v2, "locked":Z
    const-wide/16 v10, 0x0

    cmp-long v10, p1, v10

    if-gez v10, :cond_0

    const-wide/16 v10, -0x1

    cmp-long v10, p1, v10

    if-eqz v10, :cond_0

    .line 74
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "lockWaitTimeout should be LOCK_OBTAIN_WAIT_FOREVER or a non-negative number (got "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 76
    :cond_0
    sget-wide v10, Lorg/apache/lucene/store/Lock;->LOCK_POLL_INTERVAL:J

    div-long v4, p1, v10

    .line 77
    .local v4, "maxSleepCount":J
    const-wide/16 v6, 0x0

    .local v6, "sleepCount":J
    move-wide v8, v6

    .line 78
    .end local v6    # "sleepCount":J
    .local v8, "sleepCount":J
    :goto_0
    if-nez v2, :cond_5

    .line 79
    const-wide/16 v10, -0x1

    cmp-long v10, p1, v10

    if-eqz v10, :cond_3

    const-wide/16 v10, 0x1

    add-long v6, v8, v10

    .end local v8    # "sleepCount":J
    .restart local v6    # "sleepCount":J
    cmp-long v10, v8, v4

    if-ltz v10, :cond_4

    .line 80
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Lock obtain timed out: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 81
    .local v3, "reason":Ljava/lang/String;
    iget-object v10, p0, Lorg/apache/lucene/store/Lock;->failureReason:Ljava/lang/Throwable;

    if-eqz v10, :cond_1

    .line 82
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ": "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/store/Lock;->failureReason:Ljava/lang/Throwable;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    :cond_1
    new-instance v0, Lorg/apache/lucene/store/LockObtainFailedException;

    invoke-direct {v0, v3}, Lorg/apache/lucene/store/LockObtainFailedException;-><init>(Ljava/lang/String;)V

    .line 85
    .local v0, "e":Lorg/apache/lucene/store/LockObtainFailedException;
    iget-object v10, p0, Lorg/apache/lucene/store/Lock;->failureReason:Ljava/lang/Throwable;

    if-eqz v10, :cond_2

    .line 86
    iget-object v10, p0, Lorg/apache/lucene/store/Lock;->failureReason:Ljava/lang/Throwable;

    invoke-virtual {v0, v10}, Lorg/apache/lucene/store/LockObtainFailedException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 88
    :cond_2
    throw v0

    .end local v0    # "e":Lorg/apache/lucene/store/LockObtainFailedException;
    .end local v3    # "reason":Ljava/lang/String;
    .end local v6    # "sleepCount":J
    .restart local v8    # "sleepCount":J
    :cond_3
    move-wide v6, v8

    .line 91
    .end local v8    # "sleepCount":J
    .restart local v6    # "sleepCount":J
    :cond_4
    :try_start_0
    sget-wide v10, Lorg/apache/lucene/store/Lock;->LOCK_POLL_INTERVAL:J

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/store/Lock;->obtain()Z

    move-result v2

    move-wide v8, v6

    .end local v6    # "sleepCount":J
    .restart local v8    # "sleepCount":J
    goto :goto_0

    .line 92
    .end local v8    # "sleepCount":J
    .restart local v6    # "sleepCount":J
    :catch_0
    move-exception v1

    .line 93
    .local v1, "ie":Ljava/lang/InterruptedException;
    new-instance v10, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v10, v1}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v10

    .line 97
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    .end local v6    # "sleepCount":J
    .restart local v8    # "sleepCount":J
    :cond_5
    return v2
.end method

.method public abstract release()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
