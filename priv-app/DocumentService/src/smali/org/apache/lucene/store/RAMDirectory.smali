.class public Lorg/apache/lucene/store/RAMDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "RAMDirectory.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field protected final fileMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/RAMFile;",
            ">;"
        }
    .end annotation
.end field

.field protected final sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 54
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    .line 64
    :try_start_0
    new-instance v0, Lorg/apache/lucene/store/SingleInstanceLockFactory;

    invoke-direct {v0}, Lorg/apache/lucene/store/SingleInstanceLockFactory;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/RAMDirectory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_0
    return-void

    .line 65
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/RAMDirectory;-><init>(Lorg/apache/lucene/store/Directory;Z)V

    .line 98
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/store/Directory;Z)V
    .locals 6
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "closeDir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMDirectory;-><init>()V

    .line 103
    invoke-static {}, Lorg/apache/lucene/index/IndexFileNameFilter;->getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;

    move-result-object v2

    .line 104
    .local v2, "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    invoke-virtual {p1}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 105
    .local v1, "file":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v1}, Lorg/apache/lucene/index/IndexFileNameFilter;->accept(Ljava/io/File;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 106
    invoke-virtual {p1, p0, v1, v1}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 109
    .end local v1    # "file":Ljava/lang/String;
    :cond_1
    if-eqz p2, :cond_2

    .line 110
    invoke-virtual {p1}, Lorg/apache/lucene/store/Directory;->close()V

    .line 112
    :cond_2
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/RAMDirectory;->isOpen:Z

    .line 246
    iget-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 247
    return-void
.end method

.method public createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 212
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->newRAMFile()Lorg/apache/lucene/store/RAMFile;

    move-result-object v1

    .line 213
    .local v1, "file":Lorg/apache/lucene/store/RAMFile;
    iget-object v2, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 214
    .local v0, "existing":Lorg/apache/lucene/store/RAMFile;
    if-eqz v0, :cond_0

    .line 215
    iget-object v2, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    iget-wide v4, v0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J

    neg-long v4, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 216
    const/4 v2, 0x0

    iput-object v2, v0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    .line 218
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    new-instance v2, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v2, v1}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    return-object v2
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 199
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 200
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-eqz v0, :cond_0

    .line 201
    const/4 v1, 0x0

    iput-object v1, v0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    .line 202
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    iget-wide v2, v0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J

    neg-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 206
    return-void

    .line 204
    :cond_0
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final fileLength(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 177
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 178
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-nez v0, :cond_0

    .line 179
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMFile;->getLength()J

    move-result-wide v2

    return-wide v2
.end method

.method public final fileModified(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 138
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 139
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-nez v0, :cond_0

    .line 140
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMFile;->getLastModified()J

    move-result-wide v2

    return-wide v2
.end method

.method public final listAll()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 119
    iget-object v4, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 120
    .local v0, "fileNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 121
    .local v3, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .local v2, "name":Ljava/lang/String;
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    .end local v2    # "name":Ljava/lang/String;
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    return-object v4
.end method

.method protected newRAMFile()Lorg/apache/lucene/store/RAMFile;
    .locals 1

    .prologue
    .line 228
    new-instance v0, Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v0, p0}, Lorg/apache/lucene/store/RAMFile;-><init>(Lorg/apache/lucene/store/RAMDirectory;)V

    return-object v0
.end method

.method public openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 234
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 235
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 236
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-nez v0, :cond_0

    .line 237
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 239
    :cond_0
    new-instance v1, Lorg/apache/lucene/store/RAMInputStream;

    invoke-direct {v1, p1, v0}, Lorg/apache/lucene/store/RAMInputStream;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/RAMFile;)V

    return-object v1
.end method

.method public final sizeInBytes()J
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 190
    iget-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public touchFile(Ljava/lang/String;)V
    .locals 9
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 153
    iget-object v6, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 154
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-nez v0, :cond_0

    .line 155
    new-instance v6, Ljava/io/FileNotFoundException;

    invoke-direct {v6, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 158
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 161
    .local v2, "ts1":J
    :cond_1
    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    :try_start_0
    invoke-static {v6, v7, v8}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 166
    .local v4, "ts2":J
    cmp-long v6, v2, v4

    if-eqz v6, :cond_1

    .line 168
    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/RAMFile;->setLastModified(J)V

    .line 169
    return-void

    .line 162
    .end local v4    # "ts2":J
    :catch_0
    move-exception v1

    .line 163
    .local v1, "ie":Ljava/lang/InterruptedException;
    new-instance v6, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v6, v1}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v6
.end method
