.class public Lorg/apache/lucene/store/transform/LRUChunkCache;
.super Ljava/lang/Object;
.source "LRUChunkCache.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/DecompressionChunkCache;


# instance fields
.field private cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/ref/SoftReference",
            "<[B>;>;"
        }
    .end annotation
.end field

.field private cacheSize:I

.field private hit:J

.field private final locks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private miss:J


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "pCacheSize"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cacheSize:I

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    .line 41
    new-instance v0, Lorg/apache/lucene/store/transform/LRUChunkCache$1;

    iget v1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cacheSize:I

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/lucene/store/transform/LRUChunkCache$1;-><init>(Lorg/apache/lucene/store/transform/LRUChunkCache;IFZ)V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/store/transform/LRUChunkCache;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/store/transform/LRUChunkCache;

    .prologue
    .line 30
    iget v0, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cacheSize:I

    return v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 98
    return-void
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 104
    return-void
.end method

.method public declared-synchronized getChunk(J)[B
    .locals 7
    .param p1, "pos"    # J

    .prologue
    const-wide/16 v4, 0x1

    .line 52
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    .line 53
    .local v1, "w":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<[B>;"
    if-eqz v1, :cond_1

    .line 54
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 55
    .local v0, "result":[B
    if-eqz v0, :cond_0

    .line 56
    iget-wide v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->hit:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->hit:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    .end local v0    # "result":[B
    :goto_0
    monitor-exit p0

    return-object v0

    .line 58
    .restart local v0    # "result":[B
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-wide v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->miss:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->miss:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 52
    .end local v0    # "result":[B
    .end local v1    # "w":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<[B>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 63
    .restart local v1    # "w":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<[B>;"
    :cond_1
    :try_start_2
    iget-wide v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->miss:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->miss:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 64
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lock(J)V
    .locals 7
    .param p1, "pos"    # J

    .prologue
    .line 107
    const/4 v1, 0x0

    .line 108
    .local v1, "lock":Ljava/lang/Object;
    iget-object v3, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    monitor-enter v3

    .line 109
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 110
    if-nez v1, :cond_0

    .line 111
    new-instance v1, Ljava/lang/Object;

    .end local v1    # "lock":Ljava/lang/Object;
    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 112
    .restart local v1    # "lock":Ljava/lang/Object;
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    monitor-exit v3

    .line 123
    :goto_0
    return-void

    .line 115
    :cond_0
    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 121
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 122
    :try_start_3
    monitor-exit v3

    goto :goto_0

    .end local v1    # "lock":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 118
    .restart local v1    # "lock":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 119
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_4
    const-class v2, Lorg/apache/lucene/store/transform/LRUChunkCache;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 121
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v2

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized putChunk(J[BI)V
    .locals 5
    .param p1, "pos"    # J
    .param p3, "data"    # [B
    .param p4, "pSize"    # I

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cacheSize:I

    if-le v2, v3, :cond_0

    .line 71
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 73
    :cond_0
    new-array v0, p4, [B

    .line 74
    .local v0, "copy":[B
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p3, v2, v0, v3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    .end local v0    # "copy":[B
    :goto_0
    monitor-exit p0

    return-void

    .line 78
    :catch_0
    move-exception v1

    .line 79
    .local v1, "ex":Ljava/lang/OutOfMemoryError;
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 69
    .end local v1    # "ex":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LRUCache pages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->cache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " locks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unlock(J)V
    .locals 5
    .param p1, "pos"    # J

    .prologue
    .line 85
    iget-object v2, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    monitor-enter v2

    .line 86
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    .local v0, "lock":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 88
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 89
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 90
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/store/transform/LRUChunkCache;->locks:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 94
    return-void

    .line 90
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 93
    .end local v0    # "lock":Ljava/lang/Object;
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method
