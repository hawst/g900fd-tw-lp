.class public Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;
.super Ljava/lang/Object;
.source "DataEncryptor.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;


# instance fields
.field private algorithm:Ljava/lang/String;

.field private cipher:Ljavax/crypto/Cipher;

.field private deepCopy:Z

.field private iv:[B

.field private keyLength:I

.field private password:Ljava/lang/String;

.field private salt:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[BIZ)V
    .locals 0
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "salt"    # [B
    .param p4, "keyLength"    # I
    .param p5, "deepCopy"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->algorithm:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->password:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->salt:[B

    .line 53
    iput p4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->keyLength:I

    .line 54
    iput-boolean p5, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->deepCopy:Z

    .line 55
    invoke-direct {p0}, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->initCipher()V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BZ)V
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "salt"    # [B
    .param p3, "deepCopy"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 59
    const-string/jumbo v1, "AES/CBC/PKCS5Padding"

    const/16 v4, 0x80

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;-><init>(Ljava/lang/String;Ljava/lang/String;[BIZ)V

    .line 60
    return-void
.end method

.method private initCipher()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 63
    const-string/jumbo v6, "PBKDF2WithHmacSHA1"

    invoke-static {v6}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v0

    .line 64
    .local v0, "factory":Ljavax/crypto/SecretKeyFactory;
    new-instance v4, Ljavax/crypto/spec/PBEKeySpec;

    iget-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->password:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->salt:[B

    const/16 v8, 0x400

    iget v9, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->keyLength:I

    invoke-direct {v4, v6, v7, v8, v9}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 65
    .local v4, "spec":Ljava/security/spec/KeySpec;
    invoke-virtual {v0, v4}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v5

    .line 66
    .local v5, "tmp":Ljavax/crypto/SecretKey;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v5}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->algorithm:Ljava/lang/String;

    const-string/jumbo v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v10

    invoke-direct {v3, v6, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 68
    .local v3, "secret":Ljavax/crypto/SecretKey;
    iget-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->algorithm:Ljava/lang/String;

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->cipher:Ljavax/crypto/Cipher;

    .line 69
    iget-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->cipher:Ljavax/crypto/Cipher;

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 70
    iget-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->cipher:Ljavax/crypto/Cipher;

    invoke-virtual {v6}, Ljavax/crypto/Cipher;->getParameters()Ljava/security/AlgorithmParameters;

    move-result-object v1

    .line 71
    .local v1, "params":Ljava/security/AlgorithmParameters;
    const/4 v2, 0x0

    .line 72
    .local v2, "pspec":Ljavax/crypto/spec/IvParameterSpec;
    if-eqz v1, :cond_0

    .line 73
    const-class v6, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v1, v6}, Ljava/security/AlgorithmParameters;->getParameterSpec(Ljava/lang/Class;)Ljava/security/spec/AlgorithmParameterSpec;

    move-result-object v2

    .end local v2    # "pspec":Ljavax/crypto/spec/IvParameterSpec;
    check-cast v2, Ljavax/crypto/spec/IvParameterSpec;

    .line 75
    .restart local v2    # "pspec":Ljavax/crypto/spec/IvParameterSpec;
    :cond_0
    if-eqz v2, :cond_1

    .line 76
    invoke-virtual {v2}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->iv:[B

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_1
    new-array v6, v10, [B

    iput-object v6, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->iv:[B

    goto :goto_0
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 7

    .prologue
    .line 83
    iget-boolean v0, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->deepCopy:Z

    if-eqz v0, :cond_0

    .line 85
    :try_start_0
    new-instance v0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->algorithm:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->password:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->salt:[B

    iget v4, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->keyLength:I

    iget-boolean v5, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->deepCopy:Z

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;-><init>(Ljava/lang/String;Ljava/lang/String;[BIZ)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-object v0

    .line 86
    :catch_0
    move-exception v6

    .line 87
    .local v6, "ex":Ljava/security/GeneralSecurityException;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .end local v6    # "ex":Ljava/security/GeneralSecurityException;
    :cond_0
    move-object v0, p0

    .line 90
    goto :goto_0
.end method

.method public getConfig()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 107
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lorg/apache/lucene/store/transform/StreamIndexOutput;

    invoke-direct {v1, v0}, Lorg/apache/lucene/store/transform/StreamIndexOutput;-><init>(Ljava/io/OutputStream;)V

    .line 108
    .local v1, "out":Lorg/apache/lucene/store/IndexOutput;
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->algorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 109
    iget v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->keyLength:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 110
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->iv:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 111
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->iv:[B

    iget-object v3, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->iv:[B

    array-length v3, v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 112
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 113
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method public declared-synchronized transform([BII[BI)I
    .locals 3
    .param p1, "inBytes"    # [B
    .param p2, "inOffset"    # I
    .param p3, "inLength"    # I
    .param p4, "outBytes"    # [B
    .param p5, "maxOutLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;->cipher:Ljavax/crypto/Cipher;

    invoke-virtual {v2, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->doFinal([BII[B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 97
    .local v0, "cnt":I
    monitor-exit p0

    return v0

    .line 99
    .end local v0    # "cnt":I
    :catch_0
    move-exception v1

    .line 100
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
