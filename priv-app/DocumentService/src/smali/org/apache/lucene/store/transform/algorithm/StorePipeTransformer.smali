.class public Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;
.super Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;
.source "StorePipeTransformer.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;)V
    .locals 0
    .param p1, "first"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .param p2, "second"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;-><init>(Lorg/apache/lucene/store/transform/algorithm/DataTransformer;Lorg/apache/lucene/store/transform/algorithm/DataTransformer;)V

    .line 33
    return-void
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 3

    .prologue
    .line 36
    new-instance v2, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;

    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;->first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    invoke-interface {v0}, Lorg/apache/lucene/store/transform/algorithm/DataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;->second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/DataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    invoke-direct {v2, v0, v1}, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;-><init>(Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;)V

    return-object v2
.end method

.method public getConfig()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 41
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Lorg/apache/lucene/store/transform/StreamIndexOutput;

    invoke-direct {v3, v0}, Lorg/apache/lucene/store/transform/StreamIndexOutput;-><init>(Ljava/io/OutputStream;)V

    .line 42
    .local v3, "out":Lorg/apache/lucene/store/IndexOutput;
    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;->first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    check-cast v4, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    invoke-interface {v4}, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;->getConfig()[B

    move-result-object v1

    .line 43
    .local v1, "configFirst":[B
    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;->second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    check-cast v4, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;

    invoke-interface {v4}, Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;->getConfig()[B

    move-result-object v2

    .line 44
    .local v2, "configSecond":[B
    array-length v4, v1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 45
    array-length v4, v1

    invoke-virtual {v3, v1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 46
    array-length v4, v2

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 47
    array-length v4, v2

    invoke-virtual {v3, v2, v4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 48
    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 49
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method
