.class public abstract Lorg/apache/lucene/store/DataOutput;
.super Ljava/lang/Object;
.source "DataOutput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static COPY_BUFFER_SIZE:I


# instance fields
.field private copyBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/store/DataOutput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/DataOutput;->$assertionsDisabled:Z

    .line 117
    const/16 v0, 0x4000

    sput v0, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copyBytes(Lorg/apache/lucene/store/DataInput;J)V
    .locals 10
    .param p1, "input"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 122
    sget-boolean v3, Lorg/apache/lucene/store/DataOutput;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    cmp-long v3, p2, v8

    if-gez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "numBytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 123
    :cond_0
    move-wide v0, p2

    .line 124
    .local v0, "left":J
    iget-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    if-nez v3, :cond_1

    .line 125
    sget v3, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    .line 126
    :cond_1
    :goto_0
    cmp-long v3, v0, v8

    if-lez v3, :cond_3

    .line 128
    sget v3, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    int-to-long v4, v3

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 129
    sget v2, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    .line 132
    .local v2, "toCopy":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    invoke-virtual {p1, v3, v6, v2}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 133
    iget-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    invoke-virtual {p0, v3, v6, v2}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 134
    int-to-long v4, v2

    sub-long/2addr v0, v4

    .line 135
    goto :goto_0

    .line 131
    .end local v2    # "toCopy":I
    :cond_2
    long-to-int v2, v0

    .restart local v2    # "toCopy":I
    goto :goto_1

    .line 136
    .end local v2    # "toCopy":I
    :cond_3
    return-void
.end method

.method public abstract writeByte(B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public writeBytes([BI)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 44
    return-void
.end method

.method public abstract writeBytes([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public writeChars(Ljava/lang/String;II)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 149
    add-int v1, p2, p3

    .line 150
    .local v1, "end":I
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_4

    .line 151
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 152
    .local v0, "code":I
    const/4 v3, 0x1

    if-lt v0, v3, :cond_0

    const/16 v3, 0x7f

    if-gt v0, v3, :cond_0

    .line 153
    int-to-byte v3, v0

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 150
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 154
    :cond_0
    const/16 v3, 0x80

    if-lt v0, v3, :cond_1

    const/16 v3, 0x7ff

    if-le v0, v3, :cond_2

    :cond_1
    if-nez v0, :cond_3

    .line 155
    :cond_2
    shr-int/lit8 v3, v0, 0x6

    or-int/lit16 v3, v3, 0xc0

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 156
    and-int/lit8 v3, v0, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_1

    .line 158
    :cond_3
    ushr-int/lit8 v3, v0, 0xc

    or-int/lit16 v3, v3, 0xe0

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 159
    shr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 160
    and-int/lit8 v3, v0, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_1

    .line 163
    .end local v0    # "code":I
    :cond_4
    return-void
.end method

.method public writeChars([CII)V
    .locals 4
    .param p1, "s"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 175
    add-int v1, p2, p3

    .line 176
    .local v1, "end":I
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_4

    .line 177
    aget-char v0, p1, v2

    .line 178
    .local v0, "code":I
    const/4 v3, 0x1

    if-lt v0, v3, :cond_0

    const/16 v3, 0x7f

    if-gt v0, v3, :cond_0

    .line 179
    int-to-byte v3, v0

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 176
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    :cond_0
    const/16 v3, 0x80

    if-lt v0, v3, :cond_1

    const/16 v3, 0x7ff

    if-le v0, v3, :cond_2

    :cond_1
    if-nez v0, :cond_3

    .line 181
    :cond_2
    shr-int/lit8 v3, v0, 0x6

    or-int/lit16 v3, v3, 0xc0

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 182
    and-int/lit8 v3, v0, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_1

    .line 184
    :cond_3
    ushr-int/lit8 v3, v0, 0xc

    or-int/lit16 v3, v3, 0xe0

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 185
    shr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 186
    and-int/lit8 v3, v0, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_1

    .line 189
    .end local v0    # "code":I
    :cond_4
    return-void
.end method

.method public writeInt(I)V
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    shr-int/lit8 v0, p1, 0x18

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 59
    shr-int/lit8 v0, p1, 0x10

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 60
    shr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 61
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 62
    return-void
.end method

.method public writeLong(J)V
    .locals 3
    .param p1, "i"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const/16 v0, 0x20

    shr-long v0, p1, v0

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 90
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 91
    return-void
.end method

.method public writeShort(S)V
    .locals 1
    .param p1, "i"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    shr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 69
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 70
    return-void
.end method

.method public writeString(Ljava/lang/String;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 111
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 112
    .local v0, "utf8Result":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v3, v1, v0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V

    .line 113
    iget v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 114
    iget-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p0, v1, v3, v2}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 115
    return-void
.end method

.method public writeStringStringMap(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 193
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 201
    :cond_0
    return-void

    .line 195
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 196
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 197
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    .line 198
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final writeVInt(I)V
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-eqz v0, :cond_0

    .line 79
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 80
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0

    .line 82
    :cond_0
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 83
    return-void
.end method

.method public final writeVLong(J)V
    .locals 7
    .param p1, "i"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 99
    sget-boolean v0, Lorg/apache/lucene/store/DataOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    cmp-long v0, p1, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 100
    :cond_0
    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 101
    const-wide/16 v0, 0x7f

    and-long/2addr v0, p1

    const-wide/16 v2, 0x80

    or-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 102
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0

    .line 104
    :cond_1
    long-to-int v0, p1

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 105
    return-void
.end method
