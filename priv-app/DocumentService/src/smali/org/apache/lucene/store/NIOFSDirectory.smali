.class public Lorg/apache/lucene/store/NIOFSDirectory;
.super Lorg/apache/lucene/store/FSDirectory;
.source "NIOFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 0
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 64
    return-void
.end method


# virtual methods
.method public openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->ensureOpen()V

    .line 79
    new-instance v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->getDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->getReadChunkSize()I

    move-result v2

    invoke-direct {v0, v1, p2, v2}, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;-><init>(Ljava/io/File;II)V

    return-object v0
.end method
