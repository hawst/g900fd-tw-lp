.class public Lorg/apache/lucene/store/VerifyingLockFactory;
.super Lorg/apache/lucene/store/LockFactory;
.source "VerifyingLockFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;
    }
.end annotation


# instance fields
.field host:Ljava/lang/String;

.field id:B

.field lf:Lorg/apache/lucene/store/LockFactory;

.field port:I


# direct methods
.method public constructor <init>(BLorg/apache/lucene/store/LockFactory;Ljava/lang/String;I)V
    .locals 0
    .param p1, "id"    # B
    .param p2, "lf"    # Lorg/apache/lucene/store/LockFactory;
    .param p3, "host"    # Ljava/lang/String;
    .param p4, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/store/LockFactory;-><init>()V

    .line 109
    iput-byte p1, p0, Lorg/apache/lucene/store/VerifyingLockFactory;->id:B

    .line 110
    iput-object p2, p0, Lorg/apache/lucene/store/VerifyingLockFactory;->lf:Lorg/apache/lucene/store/LockFactory;

    .line 111
    iput-object p3, p0, Lorg/apache/lucene/store/VerifyingLockFactory;->host:Ljava/lang/String;

    .line 112
    iput p4, p0, Lorg/apache/lucene/store/VerifyingLockFactory;->port:I

    .line 113
    return-void
.end method


# virtual methods
.method public declared-synchronized clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "lockName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/VerifyingLockFactory;->lf:Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/LockFactory;->clearLock(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 2
    .param p1, "lockName"    # Ljava/lang/String;

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;

    iget-object v1, p0, Lorg/apache/lucene/store/VerifyingLockFactory;->lf:Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/LockFactory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;-><init>(Lorg/apache/lucene/store/VerifyingLockFactory;Lorg/apache/lucene/store/Lock;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
