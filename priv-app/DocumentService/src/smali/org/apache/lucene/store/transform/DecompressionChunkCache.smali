.class public interface abstract Lorg/apache/lucene/store/transform/DecompressionChunkCache;
.super Ljava/lang/Object;
.source "DecompressionChunkCache.java"


# virtual methods
.method public abstract clear()V
.end method

.method public abstract getChunk(J)[B
.end method

.method public abstract lock(J)V
.end method

.method public abstract putChunk(J[BI)V
.end method

.method public abstract unlock(J)V
.end method
