.class public Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;
.super Lorg/apache/lucene/store/BufferedIndexOutput;
.source "FSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/FSDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "FSIndexOutput"
.end annotation


# instance fields
.field private final file:Ljava/io/RandomAccessFile;

.field private volatile isOpen:Z

.field private final name:Ljava/lang/String;

.field private final parent:Lorg/apache/lucene/store/FSDirectory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/FSDirectory;Ljava/lang/String;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/lucene/store/FSDirectory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;-><init>()V

    .line 439
    iput-object p1, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->parent:Lorg/apache/lucene/store/FSDirectory;

    .line 440
    iput-object p2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->name:Ljava/lang/String;

    .line 441
    new-instance v0, Ljava/io/RandomAccessFile;

    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v1, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string/jumbo v2, "rw"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    .line 442
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->isOpen:Z

    .line 443
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;

    .prologue
    .line 432
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->name:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 453
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->parent:Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v1, p0}, Lorg/apache/lucene/store/FSDirectory;->onIndexOutputClosed(Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;)V

    .line 455
    iget-boolean v1, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->isOpen:Z

    if-eqz v1, :cond_0

    .line 456
    const/4 v0, 0x0

    .line 458
    .local v0, "success":Z
    :try_start_0
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    const/4 v0, 0x1

    .line 461
    iput-boolean v2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->isOpen:Z

    .line 462
    if-nez v0, :cond_2

    .line 464
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 473
    .end local v0    # "success":Z
    :cond_0
    :goto_0
    return-void

    .line 461
    .restart local v0    # "success":Z
    :catchall_0
    move-exception v1

    iput-boolean v2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->isOpen:Z

    .line 462
    if-nez v0, :cond_1

    .line 464
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 461
    :goto_1
    throw v1

    .line 469
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_0

    .line 465
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public flushBuffer([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 448
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 449
    return-void
.end method

.method public length()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public seek(J)V
    .locals 1
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 478
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/store/BufferedIndexOutput;->seek(J)V

    .line 479
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 480
    return-void
.end method

.method public setLength(J)V
    .locals 1
    .param p1, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 489
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 490
    return-void
.end method
