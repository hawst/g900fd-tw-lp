.class public abstract Lorg/apache/lucene/store/IndexInput;
.super Lorg/apache/lucene/store/DataInput;
.source "IndexInput.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final resourceDescription:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/IndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 61
    const-string/jumbo v0, "anonymous IndexInput"

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "resourceDescription"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "resourceDescription must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/store/IndexInput;->resourceDescription:Ljava/lang/String;

    .line 72
    return-void
.end method


# virtual methods
.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 8
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 103
    sget-boolean v2, Lorg/apache/lucene/store/IndexInput;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    cmp-long v2, p2, v6

    if-gez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "numBytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 105
    :cond_0
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 107
    .local v0, "copyBuf":[B
    :goto_0
    cmp-long v2, p2, v6

    if-lez v2, :cond_2

    .line 108
    array-length v2, v0

    int-to-long v2, v2

    cmp-long v2, p2, v2

    if-lez v2, :cond_1

    array-length v2, v0

    int-to-long v2, v2

    :goto_1
    long-to-int v1, v2

    .line 109
    .local v1, "toCopy":I
    invoke-virtual {p0, v0, v4, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 110
    invoke-virtual {p1, v0, v4, v1}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 111
    int-to-long v2, v1

    sub-long/2addr p2, v2

    .line 112
    goto :goto_0

    .end local v1    # "toCopy":I
    :cond_1
    move-wide v2, p2

    .line 108
    goto :goto_1

    .line 113
    :cond_2
    return-void
.end method

.method public abstract getFilePointer()J
.end method

.method public abstract length()J
.end method

.method public abstract seek(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public skipChars(I)V
    .locals 4
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 42
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 43
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v0

    .line 44
    .local v0, "b":B
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_0

    .line 42
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :cond_0
    and-int/lit16 v2, v0, 0xe0

    const/16 v3, 0xe0

    if-eq v2, v3, :cond_1

    .line 47
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    goto :goto_1

    .line 50
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    .line 51
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    goto :goto_1

    .line 54
    .end local v0    # "b":B
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/store/IndexInput;->resourceDescription:Ljava/lang/String;

    return-object v0
.end method
