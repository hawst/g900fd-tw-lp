.class public Lorg/apache/lucene/store/transform/TransformedIndexOutput;
.super Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
.source "TransformedIndexOutput.java"


# instance fields
.field private chunkSize:I

.field private closeLength:J

.field private tempDirectory:Lorg/apache/lucene/store/Directory;

.field private tempOut:Lorg/apache/lucene/store/IndexOutput;

.field private tmpName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/transform/TransformedDirectory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IndexOutput;Ljava/lang/String;ILorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;)V
    .locals 2
    .param p1, "pCompressed"    # Lorg/apache/lucene/store/transform/TransformedDirectory;
    .param p2, "pTempDir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "pOut"    # Lorg/apache/lucene/store/IndexOutput;
    .param p4, "pName"    # Ljava/lang/String;
    .param p5, "pChunkSize"    # I
    .param p6, "pTransformer"    # Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p4, p3, p6, p1}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/TransformedDirectory;)V

    .line 52
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->closeLength:J

    .line 68
    iput-object p2, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempDirectory:Lorg/apache/lucene/store/Directory;

    .line 69
    iput p5, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->chunkSize:I

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".plain."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/store/transform/TransformedDirectory;->nextSequence()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tmpName:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tmpName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    .line 72
    return-void
.end method


# virtual methods
.method public close()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->chunkSize:I

    new-array v0, v7, [B

    .line 96
    .local v0, "buffer":[B
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 99
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tmpName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 100
    .local v1, "in":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->closeLength:J

    .line 102
    .local v4, "len":J
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    const-wide/16 v8, -0x1

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 105
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->writeConfig()V

    .line 108
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_1

    .line 109
    array-length v7, v0

    int-to-long v8, v7

    cmp-long v7, v4, v8

    if-lez v7, :cond_0

    .line 110
    array-length v6, v0

    .line 117
    .local v6, "toRead":I
    :goto_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    .line 119
    .local v2, "bufferPos":J
    const/4 v7, 0x0

    invoke-virtual {v1, v0, v7, v6}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 121
    invoke-virtual {p0, v0, v2, v3, v6}, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->writeChunk([BJI)V

    .line 123
    int-to-long v8, v6

    sub-long/2addr v4, v8

    .line 124
    goto :goto_0

    .line 112
    .end local v2    # "bufferPos":J
    .end local v6    # "toRead":I
    :cond_0
    long-to-int v6, v4

    .restart local v6    # "toRead":I
    goto :goto_1

    .line 128
    .end local v6    # "toRead":I
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 129
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tmpName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 130
    iget-object v7, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v8, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tmpName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 132
    :cond_2
    invoke-super {p0}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;->close()V

    .line 134
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 87
    return-void
.end method

.method public getFilePointer()J
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public length()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->closeLength:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 149
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->closeLength:J

    .line 151
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method public seek(J)V
    .locals 1
    .param p1, "l"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 144
    return-void
.end method

.method public sync()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->flush()V

    .line 156
    return-void
.end method

.method protected updateFileLength(J)V
    .locals 5
    .param p1, "pLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 161
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 162
    return-void
.end method

.method public writeByte(B)V
    .locals 1
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 77
    return-void
.end method

.method public writeBytes([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/store/transform/TransformedIndexOutput;->tempOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 82
    return-void
.end method
