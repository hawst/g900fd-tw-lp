.class public Lorg/apache/lucene/store/SimpleFSDirectory;
.super Lorg/apache/lucene/store/FSDirectory;
.source "SimpleFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 0
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 42
    return-void
.end method


# virtual methods
.method public openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory;->ensureOpen()V

    .line 57
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/SimpleFSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 58
    .local v0, "path":Ljava/io/File;
    new-instance v1, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SimpleFSIndexInput(path=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory;->getReadChunkSize()I

    move-result v3

    invoke-direct {v1, v2, v0, p2, v3}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;II)V

    return-object v1
.end method
