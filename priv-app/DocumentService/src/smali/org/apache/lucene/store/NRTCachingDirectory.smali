.class public Lorg/apache/lucene/store/NRTCachingDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "NRTCachingDirectory.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final VERBOSE:Z


# instance fields
.field private final cache:Lorg/apache/lucene/store/RAMDirectory;

.field private final delegate:Lorg/apache/lucene/store/Directory;

.field private final maxCachedBytes:J

.field private final maxMergeSizeBytes:J

.field private final merges:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Thread;",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field private final uncacheLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lorg/apache/lucene/store/NRTCachingDirectory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/NRTCachingDirectory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;DD)V
    .locals 4
    .param p1, "delegate"    # Lorg/apache/lucene/store/Directory;
    .param p2, "maxMergeSizeMB"    # D
    .param p4, "maxCachedMB"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 98
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 84
    new-instance v0, Lorg/apache/lucene/store/RAMDirectory;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMDirectory;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    .line 295
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->merges:Ljava/util/concurrent/ConcurrentHashMap;

    .line 319
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->uncacheLock:Ljava/lang/Object;

    .line 99
    iput-object p1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    .line 100
    mul-double v0, p2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxMergeSizeBytes:J

    .line 101
    mul-double v0, p4, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxCachedBytes:J

    .line 102
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/store/NRTCachingDirectory;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/store/NRTCachingDirectory;

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->merges:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private unCache(Ljava/lang/String;)V
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    iget-object v3, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->uncacheLock:Ljava/lang/Object;

    monitor-enter v3

    .line 328
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 330
    monitor-exit v3

    .line 351
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 333
    new-instance v2, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "cannot uncache file=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\": it was separately also created in the delegate directory"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 350
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 335
    :cond_1
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 336
    .local v1, "out":Lorg/apache/lucene/store/IndexOutput;
    const/4 v0, 0x0

    .line 338
    .local v0, "in":Lorg/apache/lucene/store/IndexInput;
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/store/RAMDirectory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 339
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 341
    const/4 v2, 0x2

    :try_start_3
    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v4, 0x0

    aput-object v0, v2, v4

    const/4 v4, 0x1

    aput-object v1, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 345
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 348
    :try_start_4
    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/store/RAMDirectory;->deleteFile(Ljava/lang/String;)V

    .line 349
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 350
    :try_start_5
    monitor-exit v3

    goto :goto_0

    .line 341
    :catchall_1
    move-exception v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 349
    :catchall_2
    move-exception v2

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->clearLock(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iget-object v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v4}, Lorg/apache/lucene/store/RAMDirectory;->listAll()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 289
    .local v1, "fileName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lorg/apache/lucene/store/NRTCachingDirectory;->unCache(Ljava/lang/String;)V

    .line 288
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 291
    .end local v1    # "fileName":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v4}, Lorg/apache/lucene/store/RAMDirectory;->close()V

    .line 292
    iget-object v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4}, Lorg/apache/lucene/store/Directory;->close()V

    .line 293
    return-void
.end method

.method public createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/NRTCachingDirectory;->doCacheWrite(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 233
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    .line 240
    :goto_1
    return-object v0

    .line 236
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 240
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    goto :goto_1

    .line 237
    :catch_0
    move-exception v0

    goto :goto_2

    .line 230
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized deleteFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    sget-boolean v0, Lorg/apache/lucene/store/NRTCachingDirectory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 200
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :goto_0
    monitor-exit p0

    return-void

    .line 202
    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected doCacheWrite(Ljava/lang/String;)Z
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 314
    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->merges:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 316
    .local v0, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    const-string/jumbo v1, "segments.gen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    iget-wide v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    iget-wide v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxMergeSizeBytes:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes()J

    move-result-wide v2

    iget-wide v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxCachedBytes:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized fileLength(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileLength(Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 211
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized fileModified(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileModified(Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 177
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileModified(Ljava/lang/String;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLockFactory()Lorg/apache/lucene/store/LockFactory;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v0

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;
    .locals 1

    .prologue
    .line 298
    new-instance v0, Lorg/apache/lucene/store/NRTCachingDirectory$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/store/NRTCachingDirectory$1;-><init>(Lorg/apache/lucene/store/NRTCachingDirectory;)V

    return-object v0
.end method

.method public declared-synchronized listAll()[Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 137
    .local v3, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v6, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v6}, Lorg/apache/lucene/store/RAMDirectory;->listAll()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 138
    .local v2, "f":Ljava/lang/String;
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 145
    .end local v2    # "f":Ljava/lang/String;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v6}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 149
    .restart local v2    # "f":Ljava/lang/String;
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 151
    .end local v2    # "f":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 154
    .local v1, "ex":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :try_start_2
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 155
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "ex":Lorg/apache/lucene/store/NoSuchDirectoryException;
    .end local v3    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 158
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v3    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    :try_start_3
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v6
.end method

.method public listCachedFiles()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMDirectory;->listAll()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 266
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/RAMDirectory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 275
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "lf"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 112
    return-void
.end method

.method public sizeInBytes()J
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "fileNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .local v0, "fileName":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/apache/lucene/store/NRTCachingDirectory;->unCache(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 253
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "NRTCachingDirectory("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; maxCacheMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxCachedBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " maxMergeSizeMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxMergeSizeBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized touchFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->touchFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :goto_0
    monitor-exit p0

    return-void

    .line 189
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->touchFile(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
