.class public Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;
.super Ljava/io/RandomAccessFile;
.source "SimpleFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Descriptor"
.end annotation


# instance fields
.field protected volatile isOpen:Z

.field final length:J

.field position:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->isOpen:Z

    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->length:J

    .line 74
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-boolean v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->isOpen:Z

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->isOpen:Z

    .line 80
    invoke-super {p0}, Ljava/io/RandomAccessFile;->close()V

    .line 82
    :cond_0
    return-void
.end method
