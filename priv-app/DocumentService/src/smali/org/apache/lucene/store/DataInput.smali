.class public abstract Lorg/apache/lucene/store/DataInput;
.super Ljava/lang/Object;
.source "DataInput.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private preUTF8Strings:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readModifiedUTF8String()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 191
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v1

    .line 192
    .local v1, "length":I
    new-array v0, v1, [C

    .line 193
    .local v0, "chars":[C
    invoke-virtual {p0, v0, v3, v1}, Lorg/apache/lucene/store/DataInput;->readChars([CII)V

    .line 194
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([CII)V

    return-object v2
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 237
    const/4 v1, 0x0

    .line 239
    .local v1, "clone":Lorg/apache/lucene/store/DataInput;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/store/DataInput;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :goto_0
    return-object v1

    .line 240
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public abstract readByte()B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract readBytes([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public readBytes([BIIZ)V
    .locals 0
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "useBuffer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 73
    return-void
.end method

.method public readChars([CII)V
    .locals 5
    .param p1, "buffer"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 210
    add-int v1, p2, p3

    .line 211
    .local v1, "end":I
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 212
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 213
    .local v0, "b":B
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_0

    .line 214
    and-int/lit8 v3, v0, 0x7f

    int-to-char v3, v3

    aput-char v3, p1, v2

    .line 211
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 215
    :cond_0
    and-int/lit16 v3, v0, 0xe0

    const/16 v4, 0xe0

    if-eq v3, v4, :cond_1

    .line 216
    and-int/lit8 v3, v0, 0x1f

    shl-int/lit8 v3, v3, 0x6

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    and-int/lit8 v4, v4, 0x3f

    or-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, p1, v2

    goto :goto_1

    .line 219
    :cond_1
    and-int/lit8 v3, v0, 0xf

    shl-int/lit8 v3, v3, 0xc

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    and-int/lit8 v4, v4, 0x3f

    shl-int/lit8 v4, v4, 0x6

    or-int/2addr v3, v4

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    and-int/lit8 v4, v4, 0x3f

    or-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, p1, v2

    goto :goto_1

    .line 224
    .end local v0    # "b":B
    :cond_2
    return-void
.end method

.method public readInt()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public readLong()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public readShort()S
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public readString()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 182
    iget-boolean v2, p0, Lorg/apache/lucene/store/DataInput;->preUTF8Strings:Z

    if-eqz v2, :cond_0

    .line 183
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;->readModifiedUTF8String()Ljava/lang/String;

    move-result-object v2

    .line 187
    :goto_0
    return-object v2

    .line 184
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v1

    .line 185
    .local v1, "length":I
    new-array v0, v1, [B

    .line 186
    .local v0, "bytes":[B
    invoke-virtual {p0, v0, v4, v1}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 187
    new-instance v2, Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-direct {v2, v0, v4, v1, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    goto :goto_0
.end method

.method public readStringStringMap()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 247
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v0

    .line 248
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v4

    .line 251
    .local v4, "val":Ljava/lang/String;
    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 254
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "val":Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public readVInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 108
    .local v0, "b":B
    and-int/lit8 v1, v0, 0x7f

    .line 109
    .local v1, "i":I
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_0

    move v2, v1

    .line 122
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    return v2

    .line 110
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 111
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0x7

    or-int/2addr v1, v3

    .line 112
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 113
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 114
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0xe

    or-int/2addr v1, v3

    .line 115
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_2

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 116
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 117
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0x15

    or-int/2addr v1, v3

    .line 118
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_3

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 119
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 121
    and-int/lit8 v3, v0, 0xf

    shl-int/lit8 v3, v3, 0x1c

    or-int/2addr v1, v3

    .line 122
    and-int/lit16 v3, v0, 0xf0

    if-nez v3, :cond_4

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 123
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_4
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Invalid vInt detected (too many bits)"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public readVLong()J
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x7f

    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 149
    .local v0, "b":B
    int-to-long v6, v0

    and-long v2, v6, v8

    .line 150
    .local v2, "i":J
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_0

    move-wide v4, v2

    .line 174
    .end local v2    # "i":J
    .local v4, "i":J
    :goto_0
    return-wide v4

    .line 151
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 152
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/4 v1, 0x7

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 153
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_1

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 154
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 155
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0xe

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 156
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_2

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 157
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 158
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x15

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 159
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_3

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 160
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 161
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x1c

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 162
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_4

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 163
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 164
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x23

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 165
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_5

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 166
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 167
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x2a

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 168
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_6

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 169
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 170
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x31

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 171
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_7

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 172
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_7
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 173
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x38

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 174
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_8

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 175
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_8
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v6, "Invalid vLong detected (negative values disallowed)"

    invoke-direct {v1, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setModifiedUTF8StringsMode()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/DataInput;->preUTF8Strings:Z

    .line 40
    return-void
.end method
