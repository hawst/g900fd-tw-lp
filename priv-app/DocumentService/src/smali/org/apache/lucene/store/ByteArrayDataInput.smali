.class public final Lorg/apache/lucene/store/ByteArrayDataInput;
.super Lorg/apache/lucene/store/DataInput;
.source "ByteArrayDataInput.java"


# instance fields
.field private bytes:[B

.field private limit:I

.field private pos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 43
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([B)V

    .line 44
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 35
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([B)V

    .line 36
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 39
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 40
    return-void
.end method


# virtual methods
.method public eof()Z
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->limit:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    return v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 152
    return-void
.end method

.method public readInt()I
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public readLong()J
    .locals 8

    .prologue
    .line 81
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int v0, v2, v3

    .line 83
    .local v0, "i1":I
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int v1, v2, v3

    .line 85
    .local v1, "i2":I
    int-to-long v2, v0

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    int-to-long v4, v1

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    return-wide v2
.end method

.method public readShort()S
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public readVInt()I
    .locals 6

    .prologue
    .line 90
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v3, v4

    .line 91
    .local v0, "b":B
    and-int/lit8 v1, v0, 0x7f

    .line 92
    .local v1, "i":I
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_0

    move v2, v1

    .line 105
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    return v2

    .line 93
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v3, v4

    .line 94
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0x7

    or-int/2addr v1, v3

    .line 95
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 96
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v3, v4

    .line 97
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0xe

    or-int/2addr v1, v3

    .line 98
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_2

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 99
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v3, v4

    .line 100
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0x15

    or-int/2addr v1, v3

    .line 101
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_3

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 102
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v3, v4

    .line 104
    and-int/lit8 v3, v0, 0xf

    shl-int/lit8 v3, v3, 0x1c

    or-int/2addr v1, v3

    .line 105
    and-int/lit16 v3, v0, 0xf0

    if-nez v3, :cond_4

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 106
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_4
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Invalid vInt detected (too many bits)"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public readVLong()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x7f

    .line 111
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 112
    .local v0, "b":B
    int-to-long v6, v0

    and-long v2, v6, v8

    .line 113
    .local v2, "i":J
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_0

    move-wide v4, v2

    .line 137
    .end local v2    # "i":J
    .local v4, "i":J
    :goto_0
    return-wide v4

    .line 114
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 115
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/4 v1, 0x7

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 116
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_1

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 117
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 118
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0xe

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 119
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_2

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 120
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 121
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x15

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 122
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_3

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 123
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 124
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x1c

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 125
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_4

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 126
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 127
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x23

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 128
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_5

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto :goto_0

    .line 129
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 130
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x2a

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 131
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_6

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto/16 :goto_0

    .line 132
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 133
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x31

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 134
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_7

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto/16 :goto_0

    .line 135
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v6

    .line 136
    int-to-long v6, v0

    and-long/2addr v6, v8

    const/16 v1, 0x38

    shl-long/2addr v6, v1

    or-long/2addr v2, v6

    .line 137
    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_8

    move-wide v4, v2

    .end local v2    # "i":J
    .restart local v4    # "i":J
    goto/16 :goto_0

    .line 138
    .end local v4    # "i":J
    .restart local v2    # "i":J
    :cond_8
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "Invalid vLong detected (negative values disallowed)"

    invoke-direct {v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public reset([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 47
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 48
    return-void
.end method

.method public reset([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    .line 56
    iput p2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 57
    add-int v0, p2, p3

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->limit:I

    .line 58
    return-void
.end method

.method public skipBytes(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 65
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 66
    return-void
.end method
