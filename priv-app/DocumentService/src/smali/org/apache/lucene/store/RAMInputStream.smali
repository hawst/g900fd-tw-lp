.class public Lorg/apache/lucene/store/RAMInputStream;
.super Lorg/apache/lucene/store/IndexInput;
.source "RAMInputStream.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BUFFER_SIZE:I = 0x400


# instance fields
.field private bufferLength:I

.field private bufferPosition:I

.field private bufferStart:J

.field private currentBuffer:[B

.field private currentBufferIndex:I

.field private file:Lorg/apache/lucene/store/RAMFile;

.field private length:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/store/RAMInputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/RAMInputStream;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/RAMFile;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "f"    # Lorg/apache/lucene/store/RAMFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "RAMInputStream(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 47
    iput-object p2, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    .line 48
    iget-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v0, v0, Lorg/apache/lucene/store/RAMFile;->length:J

    iput-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    .line 49
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 50
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "RAMInputStream too large length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/RAMFile;)V
    .locals 1
    .param p1, "f"    # Lorg/apache/lucene/store/RAMFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 42
    const-string/jumbo v0, "anonymous"

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/store/RAMInputStream;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/RAMFile;)V

    .line 43
    return-void
.end method

.method private final switchCurrentBuffer(Z)V
    .locals 10
    .param p1, "enforceEOF"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x400

    const/16 v2, 0x400

    .line 96
    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    int-to-long v4, v3

    mul-long/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    .line 97
    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    iget-object v4, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v4}, Lorg/apache/lucene/store/RAMFile;->numBuffers()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 99
    if-eqz p1, :cond_0

    .line 100
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "read past EOF: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 103
    :cond_0
    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 104
    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget v4, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    .line 108
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 109
    iget-wide v4, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    iget-wide v6, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    sub-long v0, v4, v6

    .line 110
    .local v0, "buflen":J
    cmp-long v3, v0, v8

    if-lez v3, :cond_2

    :goto_1
    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    goto :goto_0

    :cond_2
    long-to-int v2, v0

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 8
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 116
    sget-boolean v4, Lorg/apache/lucene/store/RAMInputStream;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    cmp-long v4, p2, v6

    if-gez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "numBytes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 118
    :cond_0
    move-wide v2, p2

    .line 119
    .local v2, "left":J
    :goto_0
    cmp-long v4, v2, v6

    if-lez v4, :cond_3

    .line 120
    iget v4, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    iget v5, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    if-ne v4, v5, :cond_1

    .line 121
    iget v4, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 122
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 125
    :cond_1
    iget v4, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    iget v5, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    sub-int v0, v4, v5

    .line 126
    .local v0, "bytesInBuffer":I
    int-to-long v4, v0

    cmp-long v4, v4, v2

    if-gez v4, :cond_2

    int-to-long v4, v0

    :goto_1
    long-to-int v1, v4

    .line 127
    .local v1, "toCopy":I
    iget-object v4, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    iget v5, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    invoke-virtual {p1, v4, v5, v1}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 128
    iget v4, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    add-int/2addr v4, v1

    iput v4, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 129
    int-to-long v4, v1

    sub-long/2addr v2, v4

    .line 130
    goto :goto_0

    .end local v1    # "toCopy":I
    :cond_2
    move-wide v4, v2

    .line 126
    goto :goto_1

    .line 132
    .end local v0    # "bytesInBuffer":I
    :cond_3
    sget-boolean v4, Lorg/apache/lucene/store/RAMInputStream;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    cmp-long v4, v2, v6

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Insufficient bytes to copy: numBytes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " copied="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, p2, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 133
    :cond_4
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 137
    iget v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    return-wide v0
.end method

.method public readByte()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    if-lt v0, v1, :cond_0

    .line 72
    iget v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 73
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 75
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    iget v1, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    :goto_0
    if-lez p3, :cond_2

    .line 81
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    if-lt v2, v3, :cond_0

    .line 82
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 83
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 86
    :cond_0
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    sub-int v1, v2, v3

    .line 87
    .local v1, "remainInBuffer":I
    if-ge p3, v1, :cond_1

    move v0, p3

    .line 88
    .local v0, "bytesToCopy":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    add-int/2addr p2, v0

    .line 90
    sub-int/2addr p3, v0

    .line 91
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    goto :goto_0

    .end local v0    # "bytesToCopy":I
    :cond_1
    move v0, v1

    .line 87
    goto :goto_1

    .line 93
    .end local v1    # "remainInBuffer":I
    :cond_2
    return-void
.end method

.method public seek(J)V
    .locals 5
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x400

    .line 142
    iget-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 143
    :cond_0
    div-long v0, p1, v2

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 146
    :cond_1
    rem-long v0, p1, v2

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 147
    return-void
.end method
