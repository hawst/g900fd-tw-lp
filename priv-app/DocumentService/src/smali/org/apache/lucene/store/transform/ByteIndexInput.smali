.class public Lorg/apache/lucene/store/transform/ByteIndexInput;
.super Lorg/apache/lucene/store/BufferedIndexInput;
.source "ByteIndexInput.java"


# instance fields
.field private data:[B

.field private pos:I


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->data:[B

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->pos:I

    .line 37
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    return-void
.end method

.method public length()J
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->data:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected readInternal([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->data:[B

    iget v1, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->pos:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 43
    iget v0, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->pos:I

    .line 44
    return-void
.end method

.method protected seekInternal(J)V
    .locals 1
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    long-to-int v0, p1

    iput v0, p0, Lorg/apache/lucene/store/transform/ByteIndexInput;->pos:I

    .line 49
    return-void
.end method
