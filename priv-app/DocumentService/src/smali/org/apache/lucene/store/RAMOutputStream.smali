.class public Lorg/apache/lucene/store/RAMOutputStream;
.super Lorg/apache/lucene/store/IndexOutput;
.source "RAMOutputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BUFFER_SIZE:I = 0x400


# instance fields
.field private bufferLength:I

.field private bufferPosition:I

.field private bufferStart:J

.field private currentBuffer:[B

.field private currentBufferIndex:I

.field private file:Lorg/apache/lucene/store/RAMFile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/RAMOutputStream;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMFile;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/RAMFile;)V
    .locals 1
    .param p1, "f"    # Lorg/apache/lucene/store/RAMFile;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    .line 51
    return-void
.end method

.method private setFileLength()V
    .locals 6

    .prologue
    .line 142
    iget-wide v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    iget v4, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 143
    .local v0, "pointer":J
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v2, v2, Lorg/apache/lucene/store/RAMFile;->length:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 144
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/RAMFile;->setLength(J)V

    .line 146
    :cond_0
    return-void
.end method

.method private final switchCurrentBuffer()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    iget-object v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMFile;->numBuffers()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/RAMFile;->addBuffer(I)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    .line 136
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 137
    const-wide/16 v0, 0x400

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    .line 139
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMOutputStream;->flush()V

    .line 83
    return-void
.end method

.method public copyBytes(Lorg/apache/lucene/store/DataInput;J)V
    .locals 6
    .param p1, "input"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 166
    sget-boolean v1, Lorg/apache/lucene/store/RAMOutputStream;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    cmp-long v1, p2, v4

    if-gez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "numBytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 168
    :cond_0
    :goto_0
    cmp-long v1, p2, v4

    if-lez v1, :cond_3

    .line 169
    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    if-ne v1, v2, :cond_1

    .line 170
    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 171
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 174
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    sub-int v0, v1, v2

    .line 175
    .local v0, "toCopy":I
    int-to-long v2, v0

    cmp-long v1, p2, v2

    if-gez v1, :cond_2

    .line 176
    long-to-int v0, p2

    .line 178
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v0, v3}, Lorg/apache/lucene/store/DataInput;->readBytes([BIIZ)V

    .line 179
    int-to-long v2, v0

    sub-long/2addr p2, v2

    .line 180
    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    goto :goto_0

    .line 183
    .end local v0    # "toCopy":I
    :cond_3
    return-void
.end method

.method public flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/RAMFile;->setLastModified(J)V

    .line 151
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->setFileLength()V

    .line 152
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 156
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v0, v0, Lorg/apache/lucene/store/RAMFile;->length:J

    return-wide v0
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 74
    iput v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 75
    iput-wide v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    .line 76
    iput v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/RAMFile;->setLength(J)V

    .line 78
    return-void
.end method

.method public seek(J)V
    .locals 7
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x400

    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->setFileLength()V

    .line 90
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 91
    :cond_0
    div-long v0, p1, v4

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 92
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 95
    :cond_1
    rem-long v0, p1, v4

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 96
    return-void
.end method

.method public sizeInBytes()J
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMFile;->numBuffers()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x400

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    if-ne v0, v1, :cond_0

    .line 106
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 107
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 109
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    aput-byte p1, v0, v1

    .line 110
    return-void
.end method

.method public writeBytes([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    sget-boolean v2, Lorg/apache/lucene/store/RAMOutputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 115
    :cond_0
    :goto_0
    if-lez p3, :cond_3

    .line 116
    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    iget v3, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    if-ne v2, v3, :cond_1

    .line 117
    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 118
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 121
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    sub-int v1, v2, v3

    .line 122
    .local v1, "remainInBuffer":I
    if-ge p3, v1, :cond_2

    move v0, p3

    .line 123
    .local v0, "bytesToCopy":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    iget v3, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    invoke-static {p1, p2, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 124
    add-int/2addr p2, v0

    .line 125
    sub-int/2addr p3, v0

    .line 126
    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    goto :goto_0

    .end local v0    # "bytesToCopy":I
    :cond_2
    move v0, v1

    .line 122
    goto :goto_1

    .line 128
    .end local v1    # "remainInBuffer":I
    :cond_3
    return-void
.end method

.method public writeTo(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 12
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMOutputStream;->flush()V

    .line 56
    iget-object v5, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v2, v5, Lorg/apache/lucene/store/RAMFile;->length:J

    .line 57
    .local v2, "end":J
    const-wide/16 v8, 0x0

    .line 58
    .local v8, "pos":J
    const/4 v0, 0x0

    .local v0, "buffer":I
    move v1, v0

    .line 59
    .end local v0    # "buffer":I
    .local v1, "buffer":I
    :goto_0
    cmp-long v5, v8, v2

    if-gez v5, :cond_1

    .line 60
    const/16 v4, 0x400

    .line 61
    .local v4, "length":I
    int-to-long v10, v4

    add-long v6, v8, v10

    .line 62
    .local v6, "nextPos":J
    cmp-long v5, v6, v2

    if-lez v5, :cond_0

    .line 63
    sub-long v10, v2, v8

    long-to-int v4, v10

    .line 65
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "buffer":I
    .restart local v0    # "buffer":I
    invoke-virtual {v5, v1}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v5

    invoke-virtual {p1, v5, v4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 66
    move-wide v8, v6

    move v1, v0

    .line 67
    .end local v0    # "buffer":I
    .restart local v1    # "buffer":I
    goto :goto_0

    .line 68
    .end local v4    # "length":I
    .end local v6    # "nextPos":J
    :cond_1
    return-void
.end method
