.class Lorg/apache/lucene/store/NoLock;
.super Lorg/apache/lucene/store/Lock;
.source "NoLockFactory.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/store/Lock;-><init>()V

    return-void
.end method


# virtual methods
.method public isLocked()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public obtain()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "NoLock"

    return-object v0
.end method
