.class public abstract Lorg/apache/lucene/store/Directory;
.super Ljava/lang/Object;
.source "Directory.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected volatile isOpen:Z

.field protected lockFactory:Lorg/apache/lucene/store/LockFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/Directory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/Directory;->isOpen:Z

    return-void
.end method

.method public static copy(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;Z)V
    .locals 6
    .param p0, "src"    # Lorg/apache/lucene/store/Directory;
    .param p1, "dest"    # Lorg/apache/lucene/store/Directory;
    .param p2, "closeDirSrc"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 274
    invoke-static {}, Lorg/apache/lucene/index/IndexFileNameFilter;->getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;

    move-result-object v2

    .line 275
    .local v2, "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 276
    .local v1, "file":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v1}, Lorg/apache/lucene/index/IndexFileNameFilter;->accept(Ljava/io/File;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 277
    invoke-virtual {p0, p1, v1, v1}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 280
    .end local v1    # "file":Ljava/lang/String;
    :cond_1
    if-eqz p2, :cond_2

    .line 281
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 283
    :cond_2
    return-void
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/LockFactory;->clearLock(Ljava/lang/String;)V

    .line 164
    :cond_0
    return-void
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "to"    # Lorg/apache/lucene/store/Directory;
    .param p2, "src"    # Ljava/lang/String;
    .param p3, "dest"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 230
    const/4 v2, 0x0

    .line 231
    .local v2, "os":Lorg/apache/lucene/store/IndexOutput;
    const/4 v1, 0x0

    .line 232
    .local v1, "is":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 234
    .local v3, "priorException":Ljava/io/IOException;
    :try_start_0
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    .line 235
    invoke-virtual {p0, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 236
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    new-array v4, v8, [Ljava/io/Closeable;

    aput-object v2, v4, v6

    aput-object v1, v4, v7

    invoke-static {v3, v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 242
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "ioe":Ljava/io/IOException;
    move-object v3, v0

    .line 240
    new-array v4, v8, [Ljava/io/Closeable;

    aput-object v2, v4, v6

    aput-object v1, v4, v7

    invoke-static {v3, v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    new-array v5, v8, [Ljava/io/Closeable;

    aput-object v2, v5, v6

    aput-object v1, v5, v7

    invoke-static {v3, v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    throw v4
.end method

.method public abstract createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract deleteFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 289
    iget-boolean v0, p0, Lorg/apache/lucene/store/Directory;->isOpen:Z

    if-nez v0, :cond_0

    .line 290
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v1, "this Directory is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    return-void
.end method

.method public abstract fileExists(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract fileLength(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract fileModified(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public getLockFactory()Lorg/apache/lucene/store/LockFactory;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract listAll()[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/LockFactory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    return-object v0
.end method

.method public abstract openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    sget-boolean v0, Lorg/apache/lucene/store/Directory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 181
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    .line 182
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->getLockID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/LockFactory;->setLockPrefix(Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public sync(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 115
    return-void
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 130
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/Directory;->sync(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " lockFactory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract touchFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
