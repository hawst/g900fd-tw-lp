.class public Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
.super Lorg/apache/lucene/store/BufferedIndexInput;
.source "SimpleFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/SimpleFSDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "SimpleFSIndexInput"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;
    }
.end annotation


# instance fields
.field protected final chunkSize:I

.field protected final file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

.field isClone:Z


# direct methods
.method public constructor <init>(Ljava/io/File;II)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .param p2, "bufferSize"    # I
    .param p3, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 93
    const-string/jumbo v0, "anonymous SimpleFSIndexInput"

    invoke-direct {p0, v0, p1, p2, p3}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;II)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;II)V
    .locals 2
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/io/File;
    .param p3, "bufferSize"    # I
    .param p4, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p3}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 98
    new-instance v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    const-string/jumbo v1, "r"

    invoke-direct {v0, p2, v1}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    .line 99
    iput p4, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I

    .line 100
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 164
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;

    .line 165
    .local v0, "clone":Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->isClone:Z

    .line 166
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-boolean v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->isClone:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    invoke-virtual {v0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->close()V

    .line 151
    :cond_0
    return-void
.end method

.method public copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 2
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->flushBuffer(Lorg/apache/lucene/store/IndexOutput;J)I

    move-result v0

    int-to-long v0, v0

    sub-long/2addr p2, v0

    .line 180
    invoke-virtual {p1, p0, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 181
    return-void
.end method

.method isFDValid()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    invoke-virtual {v0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->valid()Z

    move-result v0

    return v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    iget-wide v0, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->length:J

    return-wide v0
.end method

.method protected readInternal([BII)V
    .locals 18
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    monitor-enter v12

    .line 107
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->getFilePointer()J

    move-result-wide v8

    .line 108
    .local v8, "position":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    iget-wide v14, v11, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->position:J

    cmp-long v11, v8, v14

    if-eqz v11, :cond_0

    .line 109
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    invoke-virtual {v11, v8, v9}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->seek(J)V

    .line 110
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    iput-wide v8, v11, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->position:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    :cond_0
    const/4 v10, 0x0

    .line 117
    .local v10, "total":I
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I

    add-int/2addr v11, v10

    move/from16 v0, p3

    if-le v11, v0, :cond_2

    .line 118
    sub-int v7, p3, v10

    .line 123
    .local v7, "readLength":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    add-int v13, p2, v10

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v13, v7}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->read([BII)I

    move-result v3

    .line 124
    .local v3, "i":I
    const/4 v11, -0x1

    if-ne v3, v11, :cond_3

    .line 125
    new-instance v11, Ljava/io/EOFException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "read past EOF: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    .end local v3    # "i":I
    .end local v7    # "readLength":I
    :catch_0
    move-exception v2

    .line 133
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    new-instance v6, Ljava/lang/OutOfMemoryError;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "OutOfMemoryError likely caused by the Sun VM Bug described in https://issues.apache.org/jira/browse/LUCENE-1566; try calling FSDirectory.setReadChunkSize with a value smaller than the current chunk size ("

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v13, ")"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    .line 137
    .local v6, "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v6, v2}, Ljava/lang/OutOfMemoryError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 138
    throw v6

    .line 144
    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    .end local v6    # "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    .end local v8    # "position":J
    .end local v10    # "total":I
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v11

    .line 121
    .restart local v8    # "position":J
    .restart local v10    # "total":I
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I

    .restart local v7    # "readLength":I
    goto :goto_0

    .line 127
    .restart local v3    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    iget-wide v14, v11, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->position:J

    int-to-long v0, v3

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v11, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->position:J
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 128
    add-int/2addr v10, v3

    .line 129
    move/from16 v0, p3

    if-lt v10, v0, :cond_1

    .line 144
    :try_start_4
    monitor-exit v12

    .line 145
    return-void

    .line 139
    .end local v3    # "i":I
    .end local v7    # "readLength":I
    :catch_1
    move-exception v4

    .line 140
    .local v4, "ioe":Ljava/io/IOException;
    new-instance v5, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v13, ": "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 141
    .local v5, "newIOE":Ljava/io/IOException;
    invoke-virtual {v5, v4}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 142
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected seekInternal(J)V
    .locals 0
    .param p1, "position"    # J

    .prologue
    .line 155
    return-void
.end method
