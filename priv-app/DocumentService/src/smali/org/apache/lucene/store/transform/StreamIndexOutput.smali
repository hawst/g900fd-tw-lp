.class public Lorg/apache/lucene/store/transform/StreamIndexOutput;
.super Lorg/apache/lucene/store/BufferedIndexOutput;
.source "StreamIndexOutput.java"


# instance fields
.field private length:J

.field private stream:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/OutputStream;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->stream:Ljava/io/OutputStream;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->length:J

    .line 35
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->close()V

    .line 53
    iget-object v0, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->stream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 55
    return-void
.end method

.method protected flushBuffer([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->stream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 42
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->length:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->length:J

    .line 43
    return-void
.end method

.method public length()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-wide v0, p0, Lorg/apache/lucene/store/transform/StreamIndexOutput;->length:J

    return-wide v0
.end method
