.class Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;
.super Ljava/lang/Object;
.source "AbstractTransformedIndexOutput.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DirectoryEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private deflatedPos:Ljava/lang/Long;

.field private inflatedPos:Ljava/lang/Long;

.field private length:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "inflatedPos"    # Ljava/lang/Long;
    .param p2, "deflatedPos"    # Ljava/lang/Long;
    .param p3, "length"    # Ljava/lang/Integer;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->inflatedPos:Ljava/lang/Long;

    .line 57
    iput-object p2, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->deflatedPos:Ljava/lang/Long;

    .line 58
    iput-object p3, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->length:Ljava/lang/Integer;

    .line 59
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->compareTo(Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;)I
    .locals 4
    .param p1, "t"    # Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->inflatedPos:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->inflatedPos:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 82
    const/4 v0, -0x1

    .line 86
    :goto_0
    return v0

    .line 83
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->inflatedPos:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->inflatedPos:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 84
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeflatedPos()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->deflatedPos:Ljava/lang/Long;

    return-object v0
.end method

.method public getInflatedPos()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->inflatedPos:Ljava/lang/Long;

    return-object v0
.end method

.method public getLength()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/store/transform/AbstractTransformedIndexOutput$DirectoryEntry;->length:Ljava/lang/Integer;

    return-object v0
.end method
