.class public Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;
.super Ljava/lang/Object;
.source "DeflateDataTransformer.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;


# instance fields
.field private deflateCount:I

.field private deflateMethod:I

.field private deflater:Ljava/util/zip/Deflater;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>(II)V

    .line 43
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "deflateMethod"    # I
    .param p2, "deflateCount"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p2, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateCount:I

    .line 37
    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0, p1}, Ljava/util/zip/Deflater;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflater:Ljava/util/zip/Deflater;

    .line 38
    iput p1, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateMethod:I

    .line 39
    return-void
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 3

    .prologue
    .line 70
    new-instance v0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;

    iget v1, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateMethod:I

    iget v2, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateCount:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>(II)V

    return-object v0
.end method

.method public getConfig()[B
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateCount:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-object v0
.end method

.method public transform([BII[BI)I
    .locals 7
    .param p1, "inBytes"    # [B
    .param p2, "inOffset"    # I
    .param p3, "inLength"    # I
    .param p4, "outBytes"    # [B
    .param p5, "maxOutLength"    # I

    .prologue
    const/4 v4, -0x1

    .line 49
    iget v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateCount:I

    if-gtz v5, :cond_1

    move v2, v4

    .line 66
    :cond_0
    :goto_0
    return v2

    .line 52
    :cond_1
    move-object v0, p1

    .line 53
    .local v0, "buffer":[B
    move v3, p2

    .line 54
    .local v3, "offset":I
    move v2, p3

    .line 55
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflateCount:I

    if-ge v1, v5, :cond_2

    .line 56
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflater:Ljava/util/zip/Deflater;

    invoke-virtual {v5}, Ljava/util/zip/Deflater;->reset()V

    .line 57
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflater:Ljava/util/zip/Deflater;

    invoke-virtual {v5, v0, v3, v2}, Ljava/util/zip/Deflater;->setInput([BII)V

    .line 58
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflater:Ljava/util/zip/Deflater;

    invoke-virtual {v5}, Ljava/util/zip/Deflater;->finish()V

    .line 59
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;->deflater:Ljava/util/zip/Deflater;

    const/4 v6, 0x0

    invoke-virtual {v5, p4, v6, p5}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v2

    .line 60
    move-object v0, p4

    .line 61
    const/4 v3, 0x0

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 63
    :cond_2
    if-lt v2, p3, :cond_0

    move v2, v4

    .line 64
    goto :goto_0
.end method
