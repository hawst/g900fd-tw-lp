.class public abstract Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;
.super Ljava/lang/Object;
.source "AbstractPipedTransformer.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/DataTransformer;


# instance fields
.field private data:[B

.field protected first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

.field protected second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/store/transform/algorithm/DataTransformer;Lorg/apache/lucene/store/transform/algorithm/DataTransformer;)V
    .locals 1
    .param p1, "first"    # Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .param p2, "second"    # Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    .line 34
    iput-object p2, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    .line 35
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    .line 36
    return-void
.end method


# virtual methods
.method public transform([BII[BI)I
    .locals 8
    .param p1, "inBytes"    # [B
    .param p2, "inOffset"    # I
    .param p3, "inLength"    # I
    .param p4, "outBytes"    # [B
    .param p5, "maxOutLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 39
    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    array-length v0, v0

    if-ge v0, p5, :cond_0

    .line 40
    new-array v0, p5, [B

    iput-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    .line 42
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    iget-object v4, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/store/transform/algorithm/DataTransformer;->transform([BII[BI)I

    move-result v3

    .line 43
    .local v3, "cnt":I
    if-gez v3, :cond_1

    .line 44
    move v3, p3

    .line 45
    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    invoke-static {p1, p2, v0, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    move v2, v7

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/store/transform/algorithm/DataTransformer;->transform([BII[BI)I

    move-result v6

    .line 48
    .local v6, "cnt2":I
    if-ltz v3, :cond_2

    if-gez v6, :cond_2

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;->data:[B

    invoke-static {v0, v7, p4, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    .end local v3    # "cnt":I
    :goto_0
    return v3

    .restart local v3    # "cnt":I
    :cond_2
    move v3, v6

    goto :goto_0
.end method
