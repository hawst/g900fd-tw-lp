.class public Lorg/apache/lucene/store/SingleInstanceLockFactory;
.super Lorg/apache/lucene/store/LockFactory;
.source "SingleInstanceLockFactory.java"


# instance fields
.field private locks:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/store/LockFactory;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/SingleInstanceLockFactory;->locks:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 2
    .param p1, "lockName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v1, p0, Lorg/apache/lucene/store/SingleInstanceLockFactory;->locks:Ljava/util/HashSet;

    monitor-enter v1

    .line 49
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/SingleInstanceLockFactory;->locks:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lorg/apache/lucene/store/SingleInstanceLockFactory;->locks:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    monitor-exit v1

    .line 53
    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 2
    .param p1, "lockName"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Lorg/apache/lucene/store/SingleInstanceLock;

    iget-object v1, p0, Lorg/apache/lucene/store/SingleInstanceLockFactory;->locks:Ljava/util/HashSet;

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/store/SingleInstanceLock;-><init>(Ljava/util/HashSet;Ljava/lang/String;)V

    return-object v0
.end method
