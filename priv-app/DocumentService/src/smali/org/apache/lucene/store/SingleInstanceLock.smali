.class Lorg/apache/lucene/store/SingleInstanceLock;
.super Lorg/apache/lucene/store/Lock;
.source "SingleInstanceLockFactory.java"


# instance fields
.field lockName:Ljava/lang/String;

.field private locks:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/HashSet;Ljava/lang/String;)V
    .locals 0
    .param p2, "lockName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "locks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/apache/lucene/store/Lock;-><init>()V

    .line 62
    iput-object p1, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    .line 63
    iput-object p2, p0, Lorg/apache/lucene/store/SingleInstanceLock;->lockName:Ljava/lang/String;

    .line 64
    return-void
.end method


# virtual methods
.method public isLocked()Z
    .locals 3

    .prologue
    .line 82
    iget-object v1, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    iget-object v2, p0, Lorg/apache/lucene/store/SingleInstanceLock;->lockName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public obtain()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v1, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    monitor-enter v1

    .line 69
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    iget-object v2, p0, Lorg/apache/lucene/store/SingleInstanceLock;->lockName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 75
    iget-object v1, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    monitor-enter v1

    .line 76
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/SingleInstanceLock;->locks:Ljava/util/HashSet;

    iget-object v2, p0, Lorg/apache/lucene/store/SingleInstanceLock;->lockName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 77
    monitor-exit v1

    .line 78
    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/SingleInstanceLock;->lockName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
