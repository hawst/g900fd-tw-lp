.class public Lorg/apache/lucene/store/RAMFile;
.super Ljava/lang/Object;
.source "RAMFile.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field protected buffers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field directory:Lorg/apache/lucene/store/RAMDirectory;

.field private lastModified:J

.field length:J

.field protected sizeInBytes:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/RAMFile;->buffers:Ljava/util/ArrayList;

    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/store/RAMFile;->lastModified:J

    .line 37
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/RAMDirectory;)V
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/RAMDirectory;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/RAMFile;->buffers:Ljava/util/ArrayList;

    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/store/RAMFile;->lastModified:J

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    .line 41
    return-void
.end method


# virtual methods
.method protected final addBuffer(I)[B
    .locals 6
    .param p1, "size"    # I

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/RAMFile;->newBuffer(I)[B

    move-result-object v0

    .line 63
    .local v0, "buffer":[B
    monitor-enter p0

    .line 64
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/RAMFile;->buffers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    iget-wide v2, p0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J

    int-to-long v4, p1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J

    .line 66
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    iget-object v1, v1, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 71
    :cond_0
    return-object v0

    .line 66
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected final declared-synchronized getBuffer(I)[B
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMFile;->buffers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLastModified()J
    .locals 2

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMFile;->lastModified:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLength()J
    .locals 2

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMFile;->length:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSizeInBytes()J
    .locals 2

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected newBuffer(I)[B
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 89
    new-array v0, p1, [B

    return-object v0
.end method

.method protected final declared-synchronized numBuffers()I
    .locals 1

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMFile;->buffers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setLastModified(J)V
    .locals 1
    .param p1, "lastModified"    # J

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/lucene/store/RAMFile;->lastModified:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setLength(J)V
    .locals 1
    .param p1, "length"    # J

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/lucene/store/RAMFile;->length:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    monitor-exit p0

    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
