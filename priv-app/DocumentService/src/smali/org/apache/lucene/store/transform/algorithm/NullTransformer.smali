.class public Lorg/apache/lucene/store/transform/algorithm/NullTransformer;
.super Ljava/lang/Object;
.source "NullTransformer.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;
.implements Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 0

    .prologue
    .line 20
    return-object p0
.end method

.method public getConfig()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public setConfig([B)V
    .locals 0
    .param p1, "pData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    return-void
.end method

.method public transform([BII[BI)I
    .locals 1
    .param p1, "inBytes"    # [B
    .param p2, "inOffset"    # I
    .param p3, "inLength"    # I
    .param p4, "outBytes"    # [B
    .param p5, "maxOutLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    const/4 v0, -0x1

    return v0
.end method
