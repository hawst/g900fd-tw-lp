.class public Lorg/apache/lucene/store/FileSwitchDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "FileSwitchDirectory.java"


# instance fields
.field private doClose:Z

.field private final primaryDir:Lorg/apache/lucene/store/Directory;

.field private final primaryExtensions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final secondaryDir:Lorg/apache/lucene/store/Directory;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;Z)V
    .locals 1
    .param p2, "primaryDir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "secondaryDir"    # Lorg/apache/lucene/store/Directory;
    .param p4, "doClose"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/store/Directory;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "primaryExtensions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryExtensions:Ljava/util/Set;

    .line 50
    iput-object p2, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    .line 51
    iput-object p3, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    .line 52
    iput-boolean p4, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->doClose:Z

    .line 53
    invoke-virtual {p2}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    .line 54
    return-void
.end method

.method private getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-static {p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "ext":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryExtensions:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    .line 131
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    goto :goto_0
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 119
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 120
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 121
    const-string/jumbo v1, ""

    .line 123
    :goto_0
    return-object v1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-boolean v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->doClose:Z

    if-eqz v0, :cond_0

    .line 70
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->doClose:Z

    .line 76
    :cond_0
    return-void

    .line 72
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Directory;->close()V

    throw v0
.end method

.method public createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    return-object v0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public fileModified(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileModified(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPrimaryDir()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getSecondaryDir()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 85
    .local v4, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 87
    .local v2, "exc":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :try_start_0
    iget-object v7, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v7}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v3, v0, v5

    .line 88
    .local v3, "f":Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 90
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "f":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    move-object v2, v1

    .line 94
    .end local v1    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :cond_0
    :try_start_1
    iget-object v7, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v7}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_1
    if-ge v5, v6, :cond_2

    aget-object v3, v0, v5

    .line 95
    .restart local v3    # "f":Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_1 .. :try_end_1} :catch_1

    .line 94
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 97
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "f":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :catch_1
    move-exception v1

    .line 100
    .restart local v1    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    if-eqz v2, :cond_1

    .line 101
    throw v2

    .line 105
    :cond_1
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 106
    throw v1

    .line 111
    .end local v1    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 112
    throw v2

    .line 114
    :cond_3
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    return-object v7
.end method

.method public openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public sync(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 171
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/FileSwitchDirectory;->sync(Ljava/util/Collection;)V

    .line 172
    return-void
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v2, "primaryNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v3, "secondaryNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 180
    .local v1, "name":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryExtensions:Ljava/util/Set;

    invoke-static {v1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 181
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 185
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 186
    iget-object v4, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 187
    return-void
.end method

.method public touchFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->touchFile(Ljava/lang/String;)V

    .line 151
    return-void
.end method
