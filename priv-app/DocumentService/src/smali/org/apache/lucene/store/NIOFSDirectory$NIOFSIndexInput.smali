.class public Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;
.super Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
.source "NIOFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/NIOFSDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "NIOFSIndexInput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private byteBuf:Ljava/nio/ByteBuffer;

.field final channel:Ljava/nio/channels/FileChannel;

.field private otherBuffer:[B

.field private otherByteBuf:Ljava/nio/ByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lorg/apache/lucene/store/NIOFSDirectory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/File;II)V
    .locals 2
    .param p1, "path"    # Ljava/io/File;
    .param p2, "bufferSize"    # I
    .param p3, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "NIOFSIndexInput(path=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2, p3}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;II)V

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    invoke-virtual {v0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->channel:Ljava/nio/channels/FileChannel;

    .line 94
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-boolean v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->isClone:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    iget-boolean v0, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->isOpen:Z

    if-eqz v0, :cond_0

    .line 107
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    invoke-virtual {v0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->close()V

    .line 112
    :cond_0
    return-void

    .line 109
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->file:Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;

    invoke-virtual {v1}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput$Descriptor;->close()V

    throw v0
.end method

.method protected newBuffer([B)V
    .locals 1
    .param p1, "newBuffer"    # [B

    .prologue
    .line 98
    invoke-super {p0, p1}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->newBuffer([B)V

    .line 99
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    .line 100
    return-void
.end method

.method protected readInternal([BII)V
    .locals 16
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->buffer:[B

    move-object/from16 v0, p1

    if-ne v0, v13, :cond_1

    if-nez p2, :cond_1

    .line 122
    sget-boolean v13, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->$assertionsDisabled:Z

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    if-nez v13, :cond_0

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 123
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v13}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 124
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 125
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    .line 145
    .local v2, "bb":Ljava/nio/ByteBuffer;
    :goto_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v12

    .line 146
    .local v12, "readOffset":I
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v13

    sub-int v9, v13, v12

    .line 147
    .local v9, "readLength":I
    sget-boolean v13, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->$assertionsDisabled:Z

    if-nez v13, :cond_4

    move/from16 v0, p3

    if-eq v9, v0, :cond_4

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 127
    .end local v2    # "bb":Ljava/nio/ByteBuffer;
    .end local v9    # "readLength":I
    .end local v12    # "readOffset":I
    :cond_1
    if-nez p2, :cond_3

    .line 128
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->otherBuffer:[B

    move-object/from16 v0, p1

    if-eq v13, v0, :cond_2

    .line 133
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->otherBuffer:[B

    .line 134
    invoke-static/range {p1 .. p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->otherByteBuf:Ljava/nio/ByteBuffer;

    .line 137
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->otherByteBuf:Ljava/nio/ByteBuffer;

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->otherByteBuf:Ljava/nio/ByteBuffer;

    .restart local v2    # "bb":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 136
    .end local v2    # "bb":Ljava/nio/ByteBuffer;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->otherByteBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v13}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1

    .line 141
    :cond_3
    invoke-static/range {p1 .. p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    .restart local v2    # "bb":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 149
    .restart local v9    # "readLength":I
    .restart local v12    # "readOffset":I
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->getFilePointer()J

    move-result-wide v10

    .line 152
    .local v10, "pos":J
    :goto_2
    if-lez v9, :cond_7

    .line 154
    :try_start_0
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->chunkSize:I

    if-le v9, v13, :cond_5

    .line 157
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->chunkSize:I

    add-int v6, v12, v13

    .line 161
    .local v6, "limit":I
    :goto_3
    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 162
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v13, v2, v10, v11}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;J)I

    move-result v4

    .line 163
    .local v4, "i":I
    const/4 v13, -0x1

    if-ne v4, v13, :cond_6

    .line 164
    new-instance v13, Ljava/io/EOFException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "read past EOF: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 170
    .end local v4    # "i":I
    .end local v6    # "limit":I
    :catch_0
    move-exception v3

    .line 173
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    new-instance v8, Ljava/lang/OutOfMemoryError;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "OutOfMemoryError likely caused by the Sun VM Bug described in https://issues.apache.org/jira/browse/LUCENE-1566; try calling FSDirectory.setReadChunkSize with a value smaller than the current chunk size ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->chunkSize:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    .line 177
    .local v8, "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8, v3}, Ljava/lang/OutOfMemoryError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 178
    throw v8

    .line 159
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    .end local v8    # "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    :cond_5
    add-int v6, v12, v9

    .restart local v6    # "limit":I
    goto :goto_3

    .line 166
    .restart local v4    # "i":I
    :cond_6
    int-to-long v14, v4

    add-long/2addr v10, v14

    .line 167
    add-int/2addr v12, v4

    .line 168
    sub-int/2addr v9, v4

    .line 169
    goto :goto_2

    .line 179
    .end local v4    # "i":I
    .end local v6    # "limit":I
    :catch_1
    move-exception v5

    .line 180
    .local v5, "ioe":Ljava/io/IOException;
    new-instance v7, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 181
    .local v7, "newIOE":Ljava/io/IOException;
    invoke-virtual {v7, v5}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 182
    throw v7

    .line 184
    .end local v5    # "ioe":Ljava/io/IOException;
    .end local v7    # "newIOE":Ljava/io/IOException;
    :cond_7
    return-void
.end method
