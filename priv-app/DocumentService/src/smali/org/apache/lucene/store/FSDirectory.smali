.class public abstract Lorg/apache/lucene/store/FSDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "FSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;
    }
.end annotation


# static fields
.field public static final DEFAULT_READ_CHUNK_SIZE:I


# instance fields
.field private chunkSize:I

.field protected final directory:Ljava/io/File;

.field protected final staleFiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    sput v0, Lorg/apache/lucene/store/FSDirectory;->DEFAULT_READ_CHUNK_SIZE:I

    return-void

    :cond_0
    const/high16 v0, 0x6400000

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 3
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 124
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    .line 125
    sget v0, Lorg/apache/lucene/store/FSDirectory;->DEFAULT_READ_CHUNK_SIZE:I

    iput v0, p0, Lorg/apache/lucene/store/FSDirectory;->chunkSize:I

    .line 140
    if-nez p2, :cond_0

    .line 141
    new-instance p2, Lorg/apache/lucene/store/NativeFSLockFactory;

    .end local p2    # "lockFactory":Lorg/apache/lucene/store/LockFactory;
    invoke-direct {p2}, Lorg/apache/lucene/store/NativeFSLockFactory;-><init>()V

    .line 143
    .restart local p2    # "lockFactory":Lorg/apache/lucene/store/LockFactory;
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/store/FSDirectory;->getCanonicalPath(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    new-instance v0, Lorg/apache/lucene/store/NoSuchDirectoryException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "file \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' exists but is not a directory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/NoSuchDirectoryException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/lucene/store/FSDirectory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 149
    return-void
.end method

.method public static fileModified(Ljava/io/File;Ljava/lang/String;)J
    .locals 4
    .param p0, "directory"    # Ljava/io/File;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 262
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 263
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    return-wide v2
.end method

.method private static getCanonicalPath(Ljava/io/File;)Ljava/io/File;
    .locals 2
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static listAll(Ljava/io/File;)[Ljava/lang/String;
    .locals 4
    .param p0, "dir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    new-instance v1, Lorg/apache/lucene/store/NoSuchDirectoryException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "directory \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' does not exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/NoSuchDirectoryException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 219
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    new-instance v1, Lorg/apache/lucene/store/NoSuchDirectoryException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "file \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' exists but is not a directory"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/NoSuchDirectoryException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 223
    :cond_1
    new-instance v1, Lorg/apache/lucene/store/FSDirectory$1;

    invoke-direct {v1}, Lorg/apache/lucene/store/FSDirectory$1;-><init>()V

    invoke-virtual {p0, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "result":[Ljava/lang/String;
    if-nez v0, :cond_2

    .line 230
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "directory \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' exists and is a directory, but cannot be listed: list() returned null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 232
    :cond_2
    return-object v0
.end method

.method public static open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;
    .locals 1
    .param p0, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v0

    return-object v0
.end method

.method public static open(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)Lorg/apache/lucene/store/FSDirectory;
    .locals 1
    .param p0, "path"    # Ljava/io/File;
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->WINDOWS:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/apache/lucene/util/Constants;->SUN_OS:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/apache/lucene/util/Constants;->LINUX:Z

    if-eqz v0, :cond_1

    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    if-eqz v0, :cond_1

    .line 180
    new-instance v0, Lorg/apache/lucene/store/MMapDirectory;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/MMapDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 184
    :goto_0
    return-object v0

    .line 181
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->WINDOWS:Z

    if-eqz v0, :cond_2

    .line 182
    new-instance v0, Lorg/apache/lucene/store/SimpleFSDirectory;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/SimpleFSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    goto :goto_0

    .line 184
    :cond_2
    new-instance v0, Lorg/apache/lucene/store/NIOFSDirectory;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/NIOFSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 369
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory;->isOpen:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    monitor-exit p0

    return-void

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 305
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/FSDirectory;->ensureCanWrite(Ljava/lang/String;)V

    .line 306
    new-instance v0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;-><init>(Lorg/apache/lucene/store/FSDirectory;Ljava/lang/String;)V

    return-object v0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 294
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 295
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 296
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 297
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 298
    return-void
.end method

.method protected ensureCanWrite(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot create directory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 314
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 315
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 316
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot overwrite: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 317
    :cond_1
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 247
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 248
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 249
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 281
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 282
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 283
    .local v2, "len":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 284
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 286
    :cond_0
    return-wide v2
.end method

.method public fileModified(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 256
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 257
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    return-wide v2
.end method

.method protected fsync(Ljava/lang/String;)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 494
    new-instance v3, Ljava/io/File;

    iget-object v8, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v3, v8, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 495
    .local v3, "fullFile":Ljava/io/File;
    const/4 v7, 0x0

    .line 496
    .local v7, "success":Z
    const/4 v6, 0x0

    .line 497
    .local v6, "retryCount":I
    const/4 v0, 0x0

    .line 498
    .local v0, "exc":Ljava/io/IOException;
    :goto_0
    if-nez v7, :cond_3

    const/4 v8, 0x5

    if-ge v6, v8, :cond_3

    .line 499
    add-int/lit8 v6, v6, 0x1

    .line 500
    const/4 v1, 0x0

    .line 503
    .local v1, "file":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v8, "rw"

    invoke-direct {v2, v3, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .local v2, "file":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 505
    const/4 v7, 0x1

    .line 507
    if-eqz v2, :cond_0

    .line 508
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    move-object v1, v2

    .line 519
    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_0

    .line 507
    :catchall_0
    move-exception v8

    :goto_1
    if-eqz v1, :cond_1

    .line 508
    :try_start_3
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 507
    :cond_1
    throw v8
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 510
    :catch_0
    move-exception v5

    .line 511
    .local v5, "ioe":Ljava/io/IOException;
    :goto_2
    if-nez v0, :cond_2

    .line 512
    move-object v0, v5

    .line 515
    :cond_2
    const-wide/16 v8, 0x5

    :try_start_4
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 516
    :catch_1
    move-exception v4

    .line 517
    .local v4, "ie":Ljava/lang/InterruptedException;
    new-instance v8, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v8, v4}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v8

    .line 521
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .end local v4    # "ie":Ljava/lang/InterruptedException;
    .end local v5    # "ioe":Ljava/io/IOException;
    :cond_3
    if-nez v7, :cond_4

    .line 523
    throw v0

    .line 524
    :cond_4
    return-void

    .line 510
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catch_2
    move-exception v5

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 507
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method

.method public getDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 380
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 381
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 375
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->getDirectory()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 7

    .prologue
    .line 350
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 353
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 358
    .local v3, "dirName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 359
    .local v2, "digest":I
    const/4 v1, 0x0

    .local v1, "charIDX":I
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 360
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 361
    .local v0, "ch":C
    mul-int/lit8 v5, v2, 0x1f

    add-int v2, v5, v0

    .line 359
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 354
    .end local v0    # "ch":C
    .end local v1    # "charIDX":I
    .end local v2    # "digest":I
    .end local v3    # "dirName":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 355
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 363
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v1    # "charIDX":I
    .restart local v2    # "digest":I
    .restart local v3    # "dirName":Ljava/lang/String;
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "lucene-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public final getReadChunkSize()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lorg/apache/lucene/store/FSDirectory;->chunkSize:I

    return v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 241
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/lucene/store/FSDirectory;->listAll(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onIndexOutputClosed(Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;)V
    .locals 2
    .param p1, "io"    # Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;

    .prologue
    .line 320
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    # getter for: Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->name:Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->access$000(Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 321
    return-void
.end method

.method public openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 344
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 345
    const/16 v0, 0x400

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/store/FSDirectory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 5
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 190
    invoke-super {p0, p1}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 194
    instance-of v2, p1, Lorg/apache/lucene/store/FSLockFactory;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 195
    check-cast v1, Lorg/apache/lucene/store/FSLockFactory;

    .line 196
    .local v1, "lf":Lorg/apache/lucene/store/FSLockFactory;
    invoke-virtual {v1}, Lorg/apache/lucene/store/FSLockFactory;->getLockDir()Ljava/io/File;

    move-result-object v0

    .line 198
    .local v0, "dir":Ljava/io/File;
    if-nez v0, :cond_1

    .line 199
    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/FSLockFactory;->setLockDir(Ljava/io/File;)V

    .line 200
    invoke-virtual {v1, v4}, Lorg/apache/lucene/store/FSLockFactory;->setLockPrefix(Ljava/lang/String;)V

    .line 206
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "lf":Lorg/apache/lucene/store/FSLockFactory;
    :cond_0
    :goto_0
    return-void

    .line 201
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v1    # "lf":Lorg/apache/lucene/store/FSLockFactory;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    invoke-virtual {v1, v4}, Lorg/apache/lucene/store/FSLockFactory;->setLockPrefix(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final setReadChunkSize(I)V
    .locals 2
    .param p1, "chunkSize"    # I

    .prologue
    .line 414
    if-gtz p1, :cond_0

    .line 415
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "chunkSize must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 417
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-nez v0, :cond_1

    .line 418
    iput p1, p0, Lorg/apache/lucene/store/FSDirectory;->chunkSize:I

    .line 420
    :cond_1
    return-void
.end method

.method public sync(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/FSDirectory;->sync(Ljava/util/Collection;)V

    .line 327
    return-void
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 331
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 332
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 333
    .local v2, "toSync":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 335
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 336
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/FSDirectory;->fsync(Ljava/lang/String;)V

    goto :goto_0

    .line 338
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 339
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " lockFactory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public touchFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 272
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 273
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 274
    .local v0, "file":Ljava/io/File;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 275
    return-void
.end method
