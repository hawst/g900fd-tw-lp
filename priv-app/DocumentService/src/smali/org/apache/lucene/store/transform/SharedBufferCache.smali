.class public Lorg/apache/lucene/store/transform/SharedBufferCache;
.super Ljava/lang/Object;
.source "SharedBufferCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/transform/SharedBufferCache$1;,
        Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    }
.end annotation


# instance fields
.field private buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/16 v0, 0xa

    new-array v0, v0, [Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    iput-object v0, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    .line 36
    return-void
.end method


# virtual methods
.method declared-synchronized newBuffer(I)Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    .locals 5
    .param p1, "size"    # I

    .prologue
    .line 39
    monitor-enter p0

    const/4 v1, 0x0

    .line 40
    .local v1, "maxSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 41
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v3, v3

    if-le v3, v1, :cond_0

    .line 42
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v1, v3

    .line 44
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v3, v3

    if-lt v3, p1, :cond_1

    .line 45
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v2, v3, v0

    .line 46
    .local v2, "result":Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    iget-object v3, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    const/4 v4, 0x0

    aput-object v4, v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    .end local v2    # "result":Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;
    :goto_1
    monitor-exit p0

    return-object v2

    .line 40
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_2
    :try_start_1
    new-instance v2, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;-><init>(ILorg/apache/lucene/store/transform/SharedBufferCache$1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 39
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method declared-synchronized release(Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;)V
    .locals 5
    .param p1, "buffer"    # Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget v4, p1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    .line 55
    iget v4, p1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    if-nez v4, :cond_0

    .line 56
    const/4 v4, 0x1

    iput v4, p1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->refCount:I

    .line 57
    const/4 v1, 0x0

    .line 58
    .local v1, "minPos":I
    const v2, 0x7fffffff

    .line 59
    .local v2, "minSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    array-length v4, v4

    if-ge v0, v4, :cond_3

    .line 60
    iget-object v4, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v4, v4, v0

    if-nez v4, :cond_1

    .line 61
    iget-object v4, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aput-object p1, v4, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v0    # "i":I
    .end local v1    # "minPos":I
    .end local v2    # "minSize":I
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 64
    .restart local v0    # "i":I
    .restart local v1    # "minPos":I
    .restart local v2    # "minSize":I
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aget-object v4, v4, v0

    iget-object v4, v4, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v3, v4

    .line 65
    .local v3, "size":I
    if-le v2, v3, :cond_2

    .line 66
    move v2, v3

    .line 67
    move v1, v0

    .line 59
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    .end local v3    # "size":I
    :cond_3
    iget-object v4, p1, Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;->data:[B

    array-length v4, v4

    if-le v4, v2, :cond_0

    .line 73
    iget-object v4, p0, Lorg/apache/lucene/store/transform/SharedBufferCache;->buffers:[Lorg/apache/lucene/store/transform/SharedBufferCache$SharedBuffer;

    aput-object p1, v4, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 54
    .end local v0    # "i":I
    .end local v1    # "minPos":I
    .end local v2    # "minSize":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
