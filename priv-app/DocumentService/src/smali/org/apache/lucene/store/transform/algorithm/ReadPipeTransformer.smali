.class public Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;
.super Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;
.source "ReadPipeTransformer.java"

# interfaces
.implements Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;)V
    .locals 0
    .param p1, "first"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;
    .param p2, "second"    # Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/transform/algorithm/AbstractPipedTransformer;-><init>(Lorg/apache/lucene/store/transform/algorithm/DataTransformer;Lorg/apache/lucene/store/transform/algorithm/DataTransformer;)V

    .line 32
    return-void
.end method


# virtual methods
.method public copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;
    .locals 3

    .prologue
    .line 35
    new-instance v2, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;

    iget-object v0, p0, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;->first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    invoke-interface {v0}, Lorg/apache/lucene/store/transform/algorithm/DataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    iget-object v1, p0, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;->second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    invoke-interface {v1}, Lorg/apache/lucene/store/transform/algorithm/DataTransformer;->copy()Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    invoke-direct {v2, v0, v1}, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;-><init>(Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;)V

    return-object v2
.end method

.method public setConfig([B)V
    .locals 6
    .param p1, "pData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 39
    new-instance v2, Lorg/apache/lucene/store/transform/ByteIndexInput;

    invoke-direct {v2, p1}, Lorg/apache/lucene/store/transform/ByteIndexInput;-><init>([B)V

    .line 41
    .local v2, "input":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 42
    .local v4, "secondLen":I
    new-array v3, v4, [B

    .line 43
    .local v3, "secondConfig":[B
    invoke-virtual {v2, v3, v5, v4}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 45
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 46
    .local v1, "firstLen":I
    new-array v0, v1, [B

    .line 47
    .local v0, "firstConfig":[B
    invoke-virtual {v2, v0, v5, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 48
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 49
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;->first:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    check-cast v5, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    invoke-interface {v5, v0}, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;->setConfig([B)V

    .line 50
    iget-object v5, p0, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;->second:Lorg/apache/lucene/store/transform/algorithm/DataTransformer;

    check-cast v5, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;

    invoke-interface {v5, v3}, Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;->setConfig([B)V

    .line 53
    return-void
.end method
