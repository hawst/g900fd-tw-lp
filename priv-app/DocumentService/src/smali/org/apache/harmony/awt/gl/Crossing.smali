.class public Lorg/apache/harmony/awt/gl/Crossing;
.super Ljava/lang/Object;
.source "Crossing.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;,
        Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;
    }
.end annotation


# static fields
.field public static final CROSSING:I = 0xff

.field static final DELTA:D = 1.0E-5

.field static final ROOT_DELTA:D = 1.0E-10

.field static final UNKNOWN:I = 0xfe


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static crossBound([DIDD)I
    .locals 10
    .param p0, "bound"    # [D
    .param p1, "bc"    # I
    .param p2, "py1"    # D
    .param p4, "py2"    # D

    .prologue
    .line 561
    if-nez p1, :cond_0

    .line 562
    const/4 v5, 0x0

    .line 597
    :goto_0
    return v5

    .line 566
    :cond_0
    const/4 v4, 0x0

    .line 567
    .local v4, "up":I
    const/4 v0, 0x0

    .line 568
    .local v0, "down":I
    const/4 v1, 0x2

    .local v1, "i":I
    :goto_1
    if-lt v1, p1, :cond_1

    .line 581
    if-nez v0, :cond_4

    .line 582
    const/4 v5, 0x0

    goto :goto_0

    .line 569
    :cond_1
    aget-wide v6, p0, v1

    cmpg-double v5, v6, p2

    if-gez v5, :cond_2

    .line 570
    add-int/lit8 v4, v4, 0x1

    .line 568
    :goto_2
    add-int/lit8 v1, v1, 0x4

    goto :goto_1

    .line 573
    :cond_2
    aget-wide v6, p0, v1

    cmpl-double v5, v6, p4

    if-lez v5, :cond_3

    .line 574
    add-int/lit8 v0, v0, 0x1

    .line 575
    goto :goto_2

    .line 577
    :cond_3
    const/16 v5, 0xff

    goto :goto_0

    .line 585
    :cond_4
    if-eqz v4, :cond_5

    .line 587
    invoke-static {p0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->sortBound([DI)V

    .line 588
    const/4 v5, 0x2

    aget-wide v6, p0, v5

    cmpl-double v5, v6, p4

    if-lez v5, :cond_6

    const/4 v2, 0x1

    .line 589
    .local v2, "sign":Z
    :goto_3
    const/4 v1, 0x6

    :goto_4
    if-lt v1, p1, :cond_7

    .line 597
    .end local v2    # "sign":Z
    :cond_5
    const/16 v5, 0xfe

    goto :goto_0

    .line 588
    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 590
    .restart local v2    # "sign":Z
    :cond_7
    aget-wide v6, p0, v1

    cmpl-double v5, v6, p4

    if-lez v5, :cond_8

    const/4 v3, 0x1

    .line 591
    .local v3, "sign2":Z
    :goto_5
    if-eq v2, v3, :cond_9

    add-int/lit8 v5, v1, 0x1

    aget-wide v6, p0, v5

    add-int/lit8 v5, v1, -0x3

    aget-wide v8, p0, v5

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_9

    .line 592
    const/16 v5, 0xff

    goto :goto_0

    .line 590
    .end local v3    # "sign2":Z
    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    .line 594
    .restart local v3    # "sign2":Z
    :cond_9
    move v2, v3

    .line 589
    add-int/lit8 v1, v1, 0x4

    goto :goto_4
.end method

.method public static crossCubic(DDDDDDDDDD)I
    .locals 22
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "cx1"    # D
    .param p6, "cy1"    # D
    .param p8, "cx2"    # D
    .param p10, "cy2"    # D
    .param p12, "x2"    # D
    .param p14, "y2"    # D
    .param p16, "x"    # D
    .param p18, "y"    # D

    .prologue
    .line 437
    cmpg-double v2, p16, p0

    if-gez v2, :cond_0

    cmpg-double v2, p16, p4

    if-gez v2, :cond_0

    cmpg-double v2, p16, p8

    if-gez v2, :cond_0

    cmpg-double v2, p16, p12

    if-ltz v2, :cond_3

    .line 438
    :cond_0
    cmpl-double v2, p16, p0

    if-lez v2, :cond_1

    cmpl-double v2, p16, p4

    if-lez v2, :cond_1

    cmpl-double v2, p16, p8

    if-lez v2, :cond_1

    cmpl-double v2, p16, p12

    if-gtz v2, :cond_3

    .line 439
    :cond_1
    cmpl-double v2, p18, p2

    if-lez v2, :cond_2

    cmpl-double v2, p18, p6

    if-lez v2, :cond_2

    cmpl-double v2, p18, p10

    if-lez v2, :cond_2

    cmpl-double v2, p18, p14

    if-gtz v2, :cond_3

    .line 440
    :cond_2
    cmpl-double v2, p0, p4

    if-nez v2, :cond_4

    cmpl-double v2, p4, p8

    if-nez v2, :cond_4

    cmpl-double v2, p8, p12

    if-nez v2, :cond_4

    .line 442
    :cond_3
    const/4 v2, 0x0

    .line 459
    :goto_0
    return v2

    .line 446
    :cond_4
    cmpg-double v2, p18, p2

    if-gez v2, :cond_8

    cmpg-double v2, p18, p6

    if-gez v2, :cond_8

    cmpg-double v2, p18, p10

    if-gez v2, :cond_8

    cmpg-double v2, p18, p14

    if-gez v2, :cond_8

    cmpl-double v2, p16, p0

    if-eqz v2, :cond_8

    cmpl-double v2, p16, p12

    if-eqz v2, :cond_8

    .line 447
    cmpg-double v2, p0, p12

    if-gez v2, :cond_6

    .line 448
    cmpg-double v2, p0, p16

    if-gez v2, :cond_5

    cmpg-double v2, p16, p12

    if-gez v2, :cond_5

    const/4 v2, 0x1

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    .line 450
    :cond_6
    cmpg-double v2, p12, p16

    if-gez v2, :cond_7

    cmpg-double v2, p16, p0

    if-gez v2, :cond_7

    const/4 v2, -0x1

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_0

    .line 454
    :cond_8
    new-instance v3, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;

    move-wide/from16 v4, p0

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    move-wide/from16 v12, p8

    move-wide/from16 v14, p10

    move-wide/from16 v16, p12

    move-wide/from16 v18, p14

    invoke-direct/range {v3 .. v19}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;-><init>(DDDDDDDD)V

    .line 455
    .local v3, "c":Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;
    sub-double v20, p16, p0

    .line 456
    .local v20, "px":D
    sub-double v6, p18, p2

    .line 457
    .local v6, "py":D
    const/4 v2, 0x3

    new-array v4, v2, [D

    .line 458
    .local v4, "res":[D
    move-wide/from16 v0, v20

    invoke-virtual {v3, v4, v0, v1}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->solvePoint([DD)I

    move-result v5

    .local v5, "rc":I
    move-wide v8, v6

    .line 459
    invoke-virtual/range {v3 .. v9}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cross([DIDD)I

    move-result v2

    goto :goto_0
.end method

.method public static crossLine(DDDDDD)I
    .locals 4
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "x"    # D
    .param p10, "y"    # D

    .prologue
    .line 367
    cmpg-double v0, p8, p0

    if-gez v0, :cond_0

    cmpg-double v0, p8, p4

    if-ltz v0, :cond_3

    .line 368
    :cond_0
    cmpl-double v0, p8, p0

    if-lez v0, :cond_1

    cmpl-double v0, p8, p4

    if-gtz v0, :cond_3

    .line 369
    :cond_1
    cmpl-double v0, p10, p2

    if-lez v0, :cond_2

    cmpl-double v0, p10, p6

    if-gtz v0, :cond_3

    .line 370
    :cond_2
    cmpl-double v0, p0, p4

    if-nez v0, :cond_4

    .line 372
    :cond_3
    const/4 v0, 0x0

    .line 396
    :goto_0
    return v0

    .line 376
    :cond_4
    cmpg-double v0, p10, p2

    if-gez v0, :cond_5

    cmpg-double v0, p10, p6

    if-ltz v0, :cond_6

    .line 379
    :cond_5
    sub-double v0, p6, p2

    sub-double v2, p8, p0

    mul-double/2addr v0, v2

    sub-double v2, p4, p0

    div-double/2addr v0, v2

    sub-double v2, p10, p2

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    .line 381
    const/4 v0, 0x0

    goto :goto_0

    .line 386
    :cond_6
    cmpl-double v0, p8, p0

    if-nez v0, :cond_8

    .line 387
    cmpg-double v0, p0, p4

    if-gez v0, :cond_7

    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    const/4 v0, -0x1

    goto :goto_0

    .line 391
    :cond_8
    cmpl-double v0, p8, p4

    if-nez v0, :cond_a

    .line 392
    cmpg-double v0, p0, p4

    if-gez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_0

    .line 396
    :cond_a
    cmpg-double v0, p0, p4

    if-gez v0, :cond_b

    const/4 v0, 0x1

    goto :goto_0

    :cond_b
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static crossPath(Lorg/icepdf/index/java/awt/geom/PathIterator;DD)I
    .locals 41
    .param p0, "p"    # Lorg/icepdf/index/java/awt/geom/PathIterator;
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 466
    const/16 v39, 0x0

    .line 468
    .local v39, "cross":I
    const-wide/16 v4, 0x0

    .local v4, "cy":D
    move-wide v2, v4

    .local v2, "cx":D
    move-wide v8, v4

    .local v8, "my":D
    move-wide v6, v4

    .line 469
    .local v6, "mx":D
    const/4 v10, 0x6

    new-array v0, v10, [D

    move-object/from16 v38, v0

    .line 471
    .local v38, "coords":[D
    :goto_0
    invoke-interface/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/PathIterator;->isDone()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 504
    :goto_1
    cmpl-double v10, v4, v8

    if-eqz v10, :cond_0

    move-wide/from16 v10, p1

    move-wide/from16 v12, p3

    .line 505
    invoke-static/range {v2 .. v13}, Lorg/apache/harmony/awt/gl/Crossing;->crossLine(DDDDDD)I

    move-result v10

    add-int v39, v39, v10

    .line 507
    :cond_0
    return v39

    .line 472
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, Lorg/icepdf/index/java/awt/geom/PathIterator;->currentSegment([D)I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 497
    :cond_2
    :goto_2
    cmpl-double v10, p1, v2

    if-nez v10, :cond_6

    cmpl-double v10, p3, v4

    if-nez v10, :cond_6

    .line 498
    const/16 v39, 0x0

    .line 499
    move-wide v4, v8

    .line 500
    goto :goto_1

    .line 474
    :pswitch_0
    cmpl-double v10, v2, v6

    if-nez v10, :cond_3

    cmpl-double v10, v4, v8

    if-eqz v10, :cond_4

    :cond_3
    move-wide/from16 v10, p1

    move-wide/from16 v12, p3

    .line 475
    invoke-static/range {v2 .. v13}, Lorg/apache/harmony/awt/gl/Crossing;->crossLine(DDDDDD)I

    move-result v10

    add-int v39, v39, v10

    .line 477
    :cond_4
    const/4 v10, 0x0

    aget-wide v2, v38, v10

    move-wide v6, v2

    .line 478
    const/4 v10, 0x1

    aget-wide v4, v38, v10

    move-wide v8, v4

    .line 479
    goto :goto_2

    .line 481
    :pswitch_1
    const/4 v10, 0x0

    aget-wide v14, v38, v10

    .end local v2    # "cx":D
    .local v14, "cx":D
    const/4 v10, 0x1

    aget-wide v16, v38, v10

    .end local v4    # "cy":D
    .local v16, "cy":D
    move-wide v10, v2

    move-wide v12, v4

    move-wide/from16 v18, p1

    move-wide/from16 v20, p3

    invoke-static/range {v10 .. v21}, Lorg/apache/harmony/awt/gl/Crossing;->crossLine(DDDDDD)I

    move-result v10

    add-int v39, v39, v10

    move-wide/from16 v4, v16

    .end local v16    # "cy":D
    .restart local v4    # "cy":D
    move-wide v2, v14

    .line 482
    .end local v14    # "cx":D
    .restart local v2    # "cx":D
    goto :goto_2

    .line 484
    :pswitch_2
    const/4 v10, 0x0

    aget-wide v22, v38, v10

    const/4 v10, 0x1

    aget-wide v24, v38, v10

    const/4 v10, 0x2

    aget-wide v14, v38, v10

    .end local v2    # "cx":D
    .restart local v14    # "cx":D
    const/4 v10, 0x3

    aget-wide v16, v38, v10

    .end local v4    # "cy":D
    .restart local v16    # "cy":D
    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    move-wide/from16 v26, v14

    move-wide/from16 v28, v16

    move-wide/from16 v30, p1

    move-wide/from16 v32, p3

    invoke-static/range {v18 .. v33}, Lorg/apache/harmony/awt/gl/Crossing;->crossQuad(DDDDDDDD)I

    move-result v10

    add-int v39, v39, v10

    move-wide/from16 v4, v16

    .end local v16    # "cy":D
    .restart local v4    # "cy":D
    move-wide v2, v14

    .line 485
    .end local v14    # "cx":D
    .restart local v2    # "cx":D
    goto :goto_2

    .line 487
    :pswitch_3
    const/4 v10, 0x0

    aget-wide v22, v38, v10

    const/4 v10, 0x1

    aget-wide v24, v38, v10

    const/4 v10, 0x2

    aget-wide v26, v38, v10

    const/4 v10, 0x3

    aget-wide v28, v38, v10

    const/4 v10, 0x4

    aget-wide v14, v38, v10

    .end local v2    # "cx":D
    .restart local v14    # "cx":D
    const/4 v10, 0x5

    aget-wide v16, v38, v10

    .end local v4    # "cy":D
    .restart local v16    # "cy":D
    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    move-wide/from16 v30, v14

    move-wide/from16 v32, v16

    move-wide/from16 v34, p1

    move-wide/from16 v36, p3

    invoke-static/range {v18 .. v37}, Lorg/apache/harmony/awt/gl/Crossing;->crossCubic(DDDDDDDDDD)I

    move-result v10

    add-int v39, v39, v10

    move-wide/from16 v4, v16

    .end local v16    # "cy":D
    .restart local v4    # "cy":D
    move-wide v2, v14

    .line 488
    .end local v14    # "cx":D
    .restart local v2    # "cx":D
    goto/16 :goto_2

    .line 490
    :pswitch_4
    cmpl-double v10, v4, v8

    if-nez v10, :cond_5

    cmpl-double v10, v2, v6

    if-eqz v10, :cond_2

    .line 491
    :cond_5
    move-wide v14, v6

    .end local v2    # "cx":D
    .restart local v14    # "cx":D
    move-wide/from16 v16, v8

    .end local v4    # "cy":D
    .restart local v16    # "cy":D
    move-wide/from16 v10, p1

    move-wide/from16 v12, p3

    invoke-static/range {v2 .. v13}, Lorg/apache/harmony/awt/gl/Crossing;->crossLine(DDDDDD)I

    move-result v10

    add-int v39, v39, v10

    move-wide/from16 v4, v16

    .end local v16    # "cy":D
    .restart local v4    # "cy":D
    move-wide v2, v14

    .end local v14    # "cx":D
    .restart local v2    # "cx":D
    goto/16 :goto_2

    .line 502
    :cond_6
    invoke-interface/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/PathIterator;->next()V

    goto/16 :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static crossQuad(DDDDDDDD)I
    .locals 16
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "cx"    # D
    .param p6, "cy"    # D
    .param p8, "x2"    # D
    .param p10, "y2"    # D
    .param p12, "x"    # D
    .param p14, "y"    # D

    .prologue
    .line 405
    cmpg-double v0, p12, p0

    if-gez v0, :cond_0

    cmpg-double v0, p12, p4

    if-gez v0, :cond_0

    cmpg-double v0, p12, p8

    if-ltz v0, :cond_3

    .line 406
    :cond_0
    cmpl-double v0, p12, p0

    if-lez v0, :cond_1

    cmpl-double v0, p12, p4

    if-lez v0, :cond_1

    cmpl-double v0, p12, p8

    if-gtz v0, :cond_3

    .line 407
    :cond_1
    cmpl-double v0, p14, p2

    if-lez v0, :cond_2

    cmpl-double v0, p14, p6

    if-lez v0, :cond_2

    cmpl-double v0, p14, p10

    if-gtz v0, :cond_3

    .line 408
    :cond_2
    cmpl-double v0, p0, p4

    if-nez v0, :cond_4

    cmpl-double v0, p4, p8

    if-nez v0, :cond_4

    .line 410
    :cond_3
    const/4 v0, 0x0

    .line 428
    :goto_0
    return v0

    .line 414
    :cond_4
    cmpg-double v0, p14, p2

    if-gez v0, :cond_8

    cmpg-double v0, p14, p6

    if-gez v0, :cond_8

    cmpg-double v0, p14, p10

    if-gez v0, :cond_8

    cmpl-double v0, p12, p0

    if-eqz v0, :cond_8

    cmpl-double v0, p12, p8

    if-eqz v0, :cond_8

    .line 415
    cmpg-double v0, p0, p8

    if-gez v0, :cond_6

    .line 416
    cmpg-double v0, p0, p12

    if-gez v0, :cond_5

    cmpg-double v0, p12, p8

    if-gez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 418
    :cond_6
    cmpg-double v0, p8, p12

    if-gez v0, :cond_7

    cmpg-double v0, p12, p0

    if-gez v0, :cond_7

    const/4 v0, -0x1

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 422
    :cond_8
    new-instance v1, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;

    move-wide/from16 v2, p0

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    move-wide/from16 v12, p10

    invoke-direct/range {v1 .. v13}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;-><init>(DDDDDD)V

    .line 423
    .local v1, "c":Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;
    sub-double v14, p12, p0

    .line 424
    .local v14, "px":D
    sub-double v4, p14, p2

    .line 425
    .local v4, "py":D
    const/4 v0, 0x3

    new-array v2, v0, [D

    .line 426
    .local v2, "res":[D
    invoke-virtual {v1, v2, v14, v15}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->solvePoint([DD)I

    move-result v3

    .local v3, "rc":I
    move-wide v6, v4

    .line 428
    invoke-virtual/range {v1 .. v7}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->cross([DIDD)I

    move-result v0

    goto :goto_0
.end method

.method public static crossShape(Lorg/icepdf/index/java/awt/Shape;DD)I
    .locals 1
    .param p0, "s"    # Lorg/icepdf/index/java/awt/Shape;
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 514
    invoke-interface {p0}, Lorg/icepdf/index/java/awt/Shape;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->contains(DD)Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    const/4 v0, 0x0

    .line 517
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lorg/apache/harmony/awt/gl/Crossing;->crossPath(Lorg/icepdf/index/java/awt/geom/PathIterator;DD)I

    move-result v0

    goto :goto_0
.end method

.method static fixRoots([DI)I
    .locals 8
    .param p0, "res"    # [D
    .param p1, "rc"    # I

    .prologue
    .line 139
    const/4 v2, 0x0

    .line 140
    .local v2, "tc":I
    const/4 v0, 0x0

    .local v0, "i":I
    move v3, v2

    .end local v2    # "tc":I
    .local v3, "tc":I
    :goto_0
    if-lt v0, p1, :cond_0

    .line 150
    return v3

    .line 142
    :cond_0
    add-int/lit8 v1, v0, 0x1

    .local v1, "j":I
    :goto_1
    if-lt v1, p1, :cond_1

    .line 147
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "tc":I
    .restart local v2    # "tc":I
    aget-wide v4, p0, v0

    aput-wide v4, p0, v3

    .line 140
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    .end local v2    # "tc":I
    .restart local v3    # "tc":I
    goto :goto_0

    .line 143
    :cond_1
    aget-wide v4, p0, v0

    aget-wide v6, p0, v1

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Lorg/apache/harmony/awt/gl/Crossing;->isZero(D)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 144
    .end local v3    # "tc":I
    .restart local v2    # "tc":I
    goto :goto_2

    .line 142
    .end local v2    # "tc":I
    .restart local v3    # "tc":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static intersectCubic(DDDDDDDDDDDD)I
    .locals 36
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "cx1"    # D
    .param p6, "cy1"    # D
    .param p8, "cx2"    # D
    .param p10, "cy2"    # D
    .param p12, "x2"    # D
    .param p14, "y2"    # D
    .param p16, "rx1"    # D
    .param p18, "ry1"    # D
    .param p20, "rx2"    # D
    .param p22, "ry2"    # D

    .prologue
    .line 746
    cmpg-double v14, p20, p0

    if-gez v14, :cond_0

    cmpg-double v14, p20, p4

    if-gez v14, :cond_0

    cmpg-double v14, p20, p8

    if-gez v14, :cond_0

    cmpg-double v14, p20, p12

    if-ltz v14, :cond_2

    .line 747
    :cond_0
    cmpl-double v14, p16, p0

    if-lez v14, :cond_1

    cmpl-double v14, p16, p4

    if-lez v14, :cond_1

    cmpl-double v14, p16, p8

    if-lez v14, :cond_1

    cmpl-double v14, p16, p12

    if-gtz v14, :cond_2

    .line 748
    :cond_1
    cmpl-double v14, p18, p2

    if-lez v14, :cond_3

    cmpl-double v14, p18, p6

    if-lez v14, :cond_3

    cmpl-double v14, p18, p10

    if-lez v14, :cond_3

    cmpl-double v14, p18, p14

    if-lez v14, :cond_3

    .line 750
    :cond_2
    const/4 v14, 0x0

    .line 811
    :goto_0
    return v14

    .line 754
    :cond_3
    cmpg-double v14, p22, p2

    if-gez v14, :cond_7

    cmpg-double v14, p22, p6

    if-gez v14, :cond_7

    cmpg-double v14, p22, p10

    if-gez v14, :cond_7

    cmpg-double v14, p22, p14

    if-gez v14, :cond_7

    cmpl-double v14, p16, p0

    if-eqz v14, :cond_7

    cmpl-double v14, p16, p12

    if-eqz v14, :cond_7

    .line 755
    cmpg-double v14, p0, p12

    if-gez v14, :cond_5

    .line 756
    cmpg-double v14, p0, p16

    if-gez v14, :cond_4

    cmpg-double v14, p16, p12

    if-gez v14, :cond_4

    const/4 v14, 0x1

    goto :goto_0

    :cond_4
    const/4 v14, 0x0

    goto :goto_0

    .line 758
    :cond_5
    cmpg-double v14, p12, p16

    if-gez v14, :cond_6

    cmpg-double v14, p16, p0

    if-gez v14, :cond_6

    const/4 v14, -0x1

    goto :goto_0

    :cond_6
    const/4 v14, 0x0

    goto :goto_0

    .line 762
    :cond_7
    new-instance v5, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;

    move-wide/from16 v6, p0

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    move-wide/from16 v12, p6

    move-wide/from16 v14, p8

    move-wide/from16 v16, p10

    move-wide/from16 v18, p12

    move-wide/from16 v20, p14

    invoke-direct/range {v5 .. v21}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;-><init>(DDDDDDDD)V

    .line 763
    .local v5, "c":Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;
    sub-double v30, p16, p0

    .line 764
    .local v30, "px1":D
    sub-double v34, p18, p2

    .line 765
    .local v34, "py1":D
    sub-double v32, p20, p0

    .line 766
    .local v32, "px2":D
    sub-double v26, p22, p2

    .line 768
    .local v26, "py2":D
    const/4 v14, 0x3

    new-array v8, v14, [D

    .line 769
    .local v8, "res1":[D
    const/4 v14, 0x3

    new-array v0, v14, [D

    move-object/from16 v18, v0

    .line 770
    .local v18, "res2":[D
    move-wide/from16 v0, v30

    invoke-virtual {v5, v8, v0, v1}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->solvePoint([DD)I

    move-result v9

    .line 771
    .local v9, "rc1":I
    move-object/from16 v0, v18

    move-wide/from16 v1, v32

    invoke-virtual {v5, v0, v1, v2}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->solvePoint([DD)I

    move-result v19

    .line 774
    .local v19, "rc2":I
    if-nez v9, :cond_8

    if-nez v19, :cond_8

    .line 775
    const/4 v14, 0x0

    goto :goto_0

    .line 778
    :cond_8
    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    sub-double v10, v30, v14

    .line 779
    .local v10, "minX":D
    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    add-double v12, v32, v14

    .line 782
    .local v12, "maxX":D
    const/16 v14, 0x28

    new-array v6, v14, [D

    .line 783
    .local v6, "bound":[D
    const/4 v7, 0x0

    .line 785
    .local v7, "bc":I
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v5 .. v15}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 786
    const/16 v24, 0x0

    const/16 v25, 0x1

    move-object v15, v5

    move-object/from16 v16, v6

    move/from16 v17, v7

    move-wide/from16 v20, v10

    move-wide/from16 v22, v12

    invoke-virtual/range {v15 .. v25}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 788
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->solveExtremX([D)I

    move-result v19

    .line 789
    const/16 v24, 0x1

    const/16 v25, 0x2

    move-object v15, v5

    move-object/from16 v16, v6

    move/from16 v17, v7

    move-wide/from16 v20, v10

    move-wide/from16 v22, v12

    invoke-virtual/range {v15 .. v25}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 790
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->solveExtremY([D)I

    move-result v19

    .line 791
    const/16 v24, 0x1

    const/16 v25, 0x4

    move-object v15, v5

    move-object/from16 v16, v6

    move/from16 v17, v7

    move-wide/from16 v20, v10

    move-wide/from16 v22, v12

    invoke-virtual/range {v15 .. v25}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 793
    cmpg-double v14, p16, p0

    if-gez v14, :cond_9

    cmpg-double v14, p0, p20

    if-gez v14, :cond_9

    .line 794
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .local v4, "bc":I
    const-wide/16 v14, 0x0

    aput-wide v14, v6, v7

    .line 795
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    const-wide/16 v14, 0x0

    aput-wide v14, v6, v4

    .line 796
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    const-wide/16 v14, 0x0

    aput-wide v14, v6, v7

    .line 797
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    const-wide/high16 v14, 0x4018000000000000L    # 6.0

    aput-wide v14, v6, v4

    :cond_9
    move v4, v7

    .line 799
    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    cmpg-double v14, p16, p12

    if-gez v14, :cond_a

    cmpg-double v14, p12, p20

    if-gez v14, :cond_a

    .line 800
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    aput-wide v14, v6, v4

    .line 801
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    iget-wide v14, v5, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    aput-wide v14, v6, v7

    .line 802
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    iget-wide v14, v5, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ay:D

    aput-wide v14, v6, v4

    .line 803
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    const-wide/high16 v14, 0x401c000000000000L    # 7.0

    aput-wide v14, v6, v7

    :cond_a
    move v7, v4

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    move-object/from16 v20, v6

    move/from16 v21, v7

    move-wide/from16 v22, v34

    move-wide/from16 v24, v26

    .line 807
    invoke-static/range {v20 .. v25}, Lorg/apache/harmony/awt/gl/Crossing;->crossBound([DIDD)I

    move-result v28

    .line 808
    .local v28, "cross":I
    const/16 v14, 0xfe

    move/from16 v0, v28

    if-eq v0, v14, :cond_b

    move/from16 v14, v28

    .line 809
    goto/16 :goto_0

    :cond_b
    move-object/from16 v21, v5

    move-object/from16 v22, v8

    move/from16 v23, v9

    move-wide/from16 v24, v34

    .line 811
    invoke-virtual/range {v21 .. v27}, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cross([DIDD)I

    move-result v14

    goto/16 :goto_0
.end method

.method public static intersectLine(DDDDDDDD)I
    .locals 14
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D
    .param p8, "rx1"    # D
    .param p10, "ry1"    # D
    .param p12, "rx2"    # D
    .param p14, "ry2"    # D

    .prologue
    .line 606
    cmpg-double v10, p12, p0

    if-gez v10, :cond_0

    cmpg-double v10, p12, p4

    if-ltz v10, :cond_2

    .line 607
    :cond_0
    cmpl-double v10, p8, p0

    if-lez v10, :cond_1

    cmpl-double v10, p8, p4

    if-gtz v10, :cond_2

    .line 608
    :cond_1
    cmpl-double v10, p10, p2

    if-lez v10, :cond_3

    cmpl-double v10, p10, p6

    if-lez v10, :cond_3

    .line 610
    :cond_2
    const/4 v10, 0x0

    .line 665
    :goto_0
    return v10

    .line 614
    :cond_3
    cmpg-double v10, p14, p2

    if-gez v10, :cond_4

    cmpg-double v10, p14, p6

    if-ltz v10, :cond_d

    .line 618
    :cond_4
    cmpl-double v10, p0, p4

    if-nez v10, :cond_5

    .line 619
    const/16 v10, 0xff

    goto :goto_0

    .line 624
    :cond_5
    cmpg-double v10, p0, p4

    if-gez v10, :cond_8

    .line 625
    cmpg-double v10, p0, p8

    if-gez v10, :cond_6

    move-wide/from16 v0, p8

    .line 626
    .local v0, "bx1":D
    :goto_1
    cmpg-double v10, p4, p12

    if-gez v10, :cond_7

    move-wide/from16 v2, p4

    .line 631
    .local v2, "bx2":D
    :goto_2
    sub-double v10, p6, p2

    sub-double v12, p4, p0

    div-double v8, v10, v12

    .line 632
    .local v8, "k":D
    sub-double v10, v0, p0

    mul-double/2addr v10, v8

    add-double v4, v10, p2

    .line 633
    .local v4, "by1":D
    sub-double v10, v2, p0

    mul-double/2addr v10, v8

    add-double v6, v10, p2

    .line 636
    .local v6, "by2":D
    cmpg-double v10, v4, p10

    if-gez v10, :cond_b

    cmpg-double v10, v6, p10

    if-gez v10, :cond_b

    .line 637
    const/4 v10, 0x0

    goto :goto_0

    .end local v0    # "bx1":D
    .end local v2    # "bx2":D
    .end local v4    # "by1":D
    .end local v6    # "by2":D
    .end local v8    # "k":D
    :cond_6
    move-wide v0, p0

    .line 625
    goto :goto_1

    .restart local v0    # "bx1":D
    :cond_7
    move-wide/from16 v2, p12

    .line 626
    goto :goto_2

    .line 628
    .end local v0    # "bx1":D
    :cond_8
    cmpg-double v10, p4, p8

    if-gez v10, :cond_9

    move-wide/from16 v0, p8

    .line 629
    .restart local v0    # "bx1":D
    :goto_3
    cmpg-double v10, p0, p12

    if-gez v10, :cond_a

    move-wide v2, p0

    .restart local v2    # "bx2":D
    :goto_4
    goto :goto_2

    .end local v0    # "bx1":D
    .end local v2    # "bx2":D
    :cond_9
    move-wide/from16 v0, p4

    .line 628
    goto :goto_3

    .restart local v0    # "bx1":D
    :cond_a
    move-wide/from16 v2, p12

    .line 629
    goto :goto_4

    .line 641
    .restart local v2    # "bx2":D
    .restart local v4    # "by1":D
    .restart local v6    # "by2":D
    .restart local v8    # "k":D
    :cond_b
    cmpl-double v10, v4, p14

    if-lez v10, :cond_c

    cmpl-double v10, v6, p14

    if-gtz v10, :cond_d

    .line 643
    :cond_c
    const/16 v10, 0xff

    goto :goto_0

    .line 648
    .end local v0    # "bx1":D
    .end local v2    # "bx2":D
    .end local v4    # "by1":D
    .end local v6    # "by2":D
    .end local v8    # "k":D
    :cond_d
    cmpl-double v10, p0, p4

    if-nez v10, :cond_e

    .line 649
    const/4 v10, 0x0

    goto :goto_0

    .line 653
    :cond_e
    cmpl-double v10, p8, p0

    if-nez v10, :cond_10

    .line 654
    cmpg-double v10, p0, p4

    if-gez v10, :cond_f

    const/4 v10, 0x0

    goto :goto_0

    :cond_f
    const/4 v10, -0x1

    goto :goto_0

    .line 658
    :cond_10
    cmpl-double v10, p8, p4

    if-nez v10, :cond_12

    .line 659
    cmpg-double v10, p0, p4

    if-gez v10, :cond_11

    const/4 v10, 0x1

    goto :goto_0

    :cond_11
    const/4 v10, 0x0

    goto :goto_0

    .line 662
    :cond_12
    cmpg-double v10, p0, p4

    if-gez v10, :cond_14

    .line 663
    cmpg-double v10, p0, p8

    if-gez v10, :cond_13

    cmpg-double v10, p8, p4

    if-gez v10, :cond_13

    const/4 v10, 0x1

    goto/16 :goto_0

    :cond_13
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 665
    :cond_14
    cmpg-double v10, p4, p8

    if-gez v10, :cond_15

    cmpg-double v10, p8, p0

    if-gez v10, :cond_15

    const/4 v10, -0x1

    goto/16 :goto_0

    :cond_15
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method public static intersectPath(Lorg/icepdf/index/java/awt/geom/PathIterator;DDDD)I
    .locals 53
    .param p0, "p"    # Lorg/icepdf/index/java/awt/geom/PathIterator;
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "w"    # D
    .param p7, "h"    # D

    .prologue
    .line 819
    const/16 v52, 0x0

    .line 822
    .local v52, "cross":I
    const-wide/16 v4, 0x0

    .local v4, "cy":D
    move-wide v2, v4

    .local v2, "cx":D
    move-wide v8, v4

    .local v8, "my":D
    move-wide v6, v4

    .line 823
    .local v6, "mx":D
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v0, v0, [D

    move-object/from16 v50, v0

    .line 825
    .local v50, "coords":[D
    move-wide/from16 v10, p1

    .line 826
    .local v10, "rx1":D
    move-wide/from16 v12, p3

    .line 827
    .local v12, "ry1":D
    add-double v14, p1, p5

    .line 828
    .local v14, "rx2":D
    add-double v16, p3, p7

    .line 830
    .local v16, "ry2":D
    :goto_0
    invoke-interface/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/PathIterator;->isDone()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 863
    cmpl-double v18, v4, v8

    if-eqz v18, :cond_7

    .line 864
    invoke-static/range {v2 .. v17}, Lorg/apache/harmony/awt/gl/Crossing;->intersectLine(DDDDDDDD)I

    move-result v51

    .line 865
    .local v51, "count":I
    const/16 v18, 0xff

    move/from16 v0, v51

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 866
    const/16 v18, 0xff

    .line 870
    .end local v51    # "count":I
    :goto_1
    return v18

    .line 831
    :cond_0
    const/16 v51, 0x0

    .line 832
    .restart local v51    # "count":I
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-interface {v0, v1}, Lorg/icepdf/index/java/awt/geom/PathIterator;->currentSegment([D)I

    move-result v18

    packed-switch v18, :pswitch_data_0

    .line 857
    :goto_2
    const/16 v18, 0xff

    move/from16 v0, v51

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 858
    const/16 v18, 0xff

    goto :goto_1

    .line 834
    :pswitch_0
    cmpl-double v18, v2, v6

    if-nez v18, :cond_1

    cmpl-double v18, v4, v8

    if-eqz v18, :cond_2

    .line 835
    :cond_1
    invoke-static/range {v2 .. v17}, Lorg/apache/harmony/awt/gl/Crossing;->intersectLine(DDDDDDDD)I

    move-result v51

    .line 837
    :cond_2
    const/16 v18, 0x0

    aget-wide v2, v50, v18

    move-wide v6, v2

    .line 838
    const/16 v18, 0x1

    aget-wide v4, v50, v18

    move-wide v8, v4

    .line 839
    goto :goto_2

    .line 841
    :pswitch_1
    const/16 v18, 0x0

    aget-wide v22, v50, v18

    .end local v2    # "cx":D
    .local v22, "cx":D
    const/16 v18, 0x1

    aget-wide v24, v50, v18

    .end local v4    # "cy":D
    .local v24, "cy":D
    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    move-wide/from16 v26, v10

    move-wide/from16 v28, v12

    move-wide/from16 v30, v14

    move-wide/from16 v32, v16

    invoke-static/range {v18 .. v33}, Lorg/apache/harmony/awt/gl/Crossing;->intersectLine(DDDDDDDD)I

    move-result v51

    move-wide/from16 v4, v24

    .end local v24    # "cy":D
    .restart local v4    # "cy":D
    move-wide/from16 v2, v22

    .line 842
    .end local v22    # "cx":D
    .restart local v2    # "cx":D
    goto :goto_2

    .line 844
    :pswitch_2
    const/16 v18, 0x0

    aget-wide v30, v50, v18

    const/16 v18, 0x1

    aget-wide v32, v50, v18

    const/16 v18, 0x2

    aget-wide v22, v50, v18

    .end local v2    # "cx":D
    .restart local v22    # "cx":D
    const/16 v18, 0x3

    aget-wide v24, v50, v18

    .end local v4    # "cy":D
    .restart local v24    # "cy":D
    move-wide/from16 v26, v2

    move-wide/from16 v28, v4

    move-wide/from16 v34, v22

    move-wide/from16 v36, v24

    move-wide/from16 v38, v10

    move-wide/from16 v40, v12

    move-wide/from16 v42, v14

    move-wide/from16 v44, v16

    invoke-static/range {v26 .. v45}, Lorg/apache/harmony/awt/gl/Crossing;->intersectQuad(DDDDDDDDDD)I

    move-result v51

    move-wide/from16 v4, v24

    .end local v24    # "cy":D
    .restart local v4    # "cy":D
    move-wide/from16 v2, v22

    .line 845
    .end local v22    # "cx":D
    .restart local v2    # "cx":D
    goto :goto_2

    .line 847
    :pswitch_3
    const/16 v18, 0x0

    aget-wide v30, v50, v18

    const/16 v18, 0x1

    aget-wide v32, v50, v18

    const/16 v18, 0x2

    aget-wide v34, v50, v18

    const/16 v18, 0x3

    aget-wide v36, v50, v18

    const/16 v18, 0x4

    aget-wide v22, v50, v18

    .end local v2    # "cx":D
    .restart local v22    # "cx":D
    const/16 v18, 0x5

    aget-wide v24, v50, v18

    .end local v4    # "cy":D
    .restart local v24    # "cy":D
    move-wide/from16 v26, v2

    move-wide/from16 v28, v4

    move-wide/from16 v38, v22

    move-wide/from16 v40, v24

    move-wide/from16 v42, v10

    move-wide/from16 v44, v12

    move-wide/from16 v46, v14

    move-wide/from16 v48, v16

    invoke-static/range {v26 .. v49}, Lorg/apache/harmony/awt/gl/Crossing;->intersectCubic(DDDDDDDDDDDD)I

    move-result v51

    move-wide/from16 v4, v24

    .end local v24    # "cy":D
    .restart local v4    # "cy":D
    move-wide/from16 v2, v22

    .line 848
    .end local v22    # "cx":D
    .restart local v2    # "cx":D
    goto/16 :goto_2

    .line 850
    :pswitch_4
    cmpl-double v18, v4, v8

    if-nez v18, :cond_3

    cmpl-double v18, v2, v6

    if-eqz v18, :cond_4

    .line 851
    :cond_3
    invoke-static/range {v2 .. v17}, Lorg/apache/harmony/awt/gl/Crossing;->intersectLine(DDDDDDDD)I

    move-result v51

    .line 853
    :cond_4
    move-wide v2, v6

    .line 854
    move-wide v4, v8

    goto/16 :goto_2

    .line 860
    :cond_5
    add-int v52, v52, v51

    .line 861
    invoke-interface/range {p0 .. p0}, Lorg/icepdf/index/java/awt/geom/PathIterator;->next()V

    goto/16 :goto_0

    .line 868
    :cond_6
    add-int v52, v52, v51

    .end local v51    # "count":I
    :cond_7
    move/from16 v18, v52

    .line 870
    goto/16 :goto_1

    .line 832
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static intersectQuad(DDDDDDDDDD)I
    .locals 36
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "cx"    # D
    .param p6, "cy"    # D
    .param p8, "x2"    # D
    .param p10, "y2"    # D
    .param p12, "rx1"    # D
    .param p14, "ry1"    # D
    .param p16, "rx2"    # D
    .param p18, "ry2"    # D

    .prologue
    .line 675
    cmpg-double v14, p16, p0

    if-gez v14, :cond_0

    cmpg-double v14, p16, p4

    if-gez v14, :cond_0

    cmpg-double v14, p16, p8

    if-ltz v14, :cond_2

    .line 676
    :cond_0
    cmpl-double v14, p12, p0

    if-lez v14, :cond_1

    cmpl-double v14, p12, p4

    if-lez v14, :cond_1

    cmpl-double v14, p12, p8

    if-gtz v14, :cond_2

    .line 677
    :cond_1
    cmpl-double v14, p14, p2

    if-lez v14, :cond_3

    cmpl-double v14, p14, p6

    if-lez v14, :cond_3

    cmpl-double v14, p14, p10

    if-lez v14, :cond_3

    .line 679
    :cond_2
    const/4 v14, 0x0

    .line 737
    :goto_0
    return v14

    .line 683
    :cond_3
    cmpg-double v14, p18, p2

    if-gez v14, :cond_7

    cmpg-double v14, p18, p6

    if-gez v14, :cond_7

    cmpg-double v14, p18, p10

    if-gez v14, :cond_7

    cmpl-double v14, p12, p0

    if-eqz v14, :cond_7

    cmpl-double v14, p12, p8

    if-eqz v14, :cond_7

    .line 684
    cmpg-double v14, p0, p8

    if-gez v14, :cond_5

    .line 685
    cmpg-double v14, p0, p12

    if-gez v14, :cond_4

    cmpg-double v14, p12, p8

    if-gez v14, :cond_4

    const/4 v14, 0x1

    goto :goto_0

    :cond_4
    const/4 v14, 0x0

    goto :goto_0

    .line 687
    :cond_5
    cmpg-double v14, p8, p12

    if-gez v14, :cond_6

    cmpg-double v14, p12, p0

    if-gez v14, :cond_6

    const/4 v14, -0x1

    goto :goto_0

    :cond_6
    const/4 v14, 0x0

    goto :goto_0

    .line 691
    :cond_7
    new-instance v5, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;

    move-wide/from16 v6, p0

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    move-wide/from16 v12, p6

    move-wide/from16 v14, p8

    move-wide/from16 v16, p10

    invoke-direct/range {v5 .. v17}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;-><init>(DDDDDD)V

    .line 692
    .local v5, "c":Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;
    sub-double v30, p12, p0

    .line 693
    .local v30, "px1":D
    sub-double v34, p14, p2

    .line 694
    .local v34, "py1":D
    sub-double v32, p16, p0

    .line 695
    .local v32, "px2":D
    sub-double v26, p18, p2

    .line 697
    .local v26, "py2":D
    const/4 v14, 0x3

    new-array v8, v14, [D

    .line 698
    .local v8, "res1":[D
    const/4 v14, 0x3

    new-array v0, v14, [D

    move-object/from16 v18, v0

    .line 699
    .local v18, "res2":[D
    move-wide/from16 v0, v30

    invoke-virtual {v5, v8, v0, v1}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->solvePoint([DD)I

    move-result v9

    .line 700
    .local v9, "rc1":I
    move-object/from16 v0, v18

    move-wide/from16 v1, v32

    invoke-virtual {v5, v0, v1, v2}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->solvePoint([DD)I

    move-result v19

    .line 703
    .local v19, "rc2":I
    if-nez v9, :cond_8

    if-nez v19, :cond_8

    .line 704
    const/4 v14, 0x0

    goto :goto_0

    .line 708
    :cond_8
    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    sub-double v10, v30, v14

    .line 709
    .local v10, "minX":D
    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    add-double v12, v32, v14

    .line 710
    .local v12, "maxX":D
    const/16 v14, 0x1c

    new-array v6, v14, [D

    .line 711
    .local v6, "bound":[D
    const/4 v7, 0x0

    .line 713
    .local v7, "bc":I
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v5 .. v15}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 714
    const/16 v24, 0x0

    const/16 v25, 0x1

    move-object v15, v5

    move-object/from16 v16, v6

    move/from16 v17, v7

    move-wide/from16 v20, v10

    move-wide/from16 v22, v12

    invoke-virtual/range {v15 .. v25}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 716
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->solveExtrem([D)I

    move-result v19

    .line 717
    const/16 v24, 0x1

    const/16 v25, 0x2

    move-object v15, v5

    move-object/from16 v16, v6

    move/from16 v17, v7

    move-wide/from16 v20, v10

    move-wide/from16 v22, v12

    invoke-virtual/range {v15 .. v25}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->addBound([DI[DIDDZI)I

    move-result v7

    .line 719
    cmpg-double v14, p12, p0

    if-gez v14, :cond_9

    cmpg-double v14, p0, p16

    if-gez v14, :cond_9

    .line 720
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .local v4, "bc":I
    const-wide/16 v14, 0x0

    aput-wide v14, v6, v7

    .line 721
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    const-wide/16 v14, 0x0

    aput-wide v14, v6, v4

    .line 722
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    const-wide/16 v14, 0x0

    aput-wide v14, v6, v7

    .line 723
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    const-wide/high16 v14, 0x4010000000000000L    # 4.0

    aput-wide v14, v6, v4

    :cond_9
    move v4, v7

    .line 725
    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    cmpg-double v14, p12, p8

    if-gez v14, :cond_a

    cmpg-double v14, p8, p16

    if-gez v14, :cond_a

    .line 726
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    aput-wide v14, v6, v4

    .line 727
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    iget-wide v14, v5, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ax:D

    aput-wide v14, v6, v7

    .line 728
    add-int/lit8 v7, v4, 0x1

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    iget-wide v14, v5, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ay:D

    aput-wide v14, v6, v4

    .line 729
    add-int/lit8 v4, v7, 0x1

    .end local v7    # "bc":I
    .restart local v4    # "bc":I
    const-wide/high16 v14, 0x4014000000000000L    # 5.0

    aput-wide v14, v6, v7

    :cond_a
    move v7, v4

    .end local v4    # "bc":I
    .restart local v7    # "bc":I
    move-object/from16 v20, v6

    move/from16 v21, v7

    move-wide/from16 v22, v34

    move-wide/from16 v24, v26

    .line 733
    invoke-static/range {v20 .. v25}, Lorg/apache/harmony/awt/gl/Crossing;->crossBound([DIDD)I

    move-result v28

    .line 734
    .local v28, "cross":I
    const/16 v14, 0xfe

    move/from16 v0, v28

    if-eq v0, v14, :cond_b

    move/from16 v14, v28

    .line 735
    goto/16 :goto_0

    :cond_b
    move-object/from16 v21, v5

    move-object/from16 v22, v8

    move/from16 v23, v9

    move-wide/from16 v24, v34

    .line 737
    invoke-virtual/range {v21 .. v27}, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->cross([DIDD)I

    move-result v14

    goto/16 :goto_0
.end method

.method public static intersectShape(Lorg/icepdf/index/java/awt/Shape;DDDD)I
    .locals 11
    .param p0, "s"    # Lorg/icepdf/index/java/awt/Shape;
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "w"    # D
    .param p7, "h"    # D

    .prologue
    .line 877
    invoke-interface {p0}, Lorg/icepdf/index/java/awt/Shape;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v1

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v9}, Lorg/icepdf/index/java/awt/geom/Rectangle2D;->intersects(DDDD)Z

    move-result v0

    if-nez v0, :cond_0

    .line 878
    const/4 v0, 0x0

    .line 880
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lorg/icepdf/index/java/awt/Shape;->getPathIterator(Lorg/icepdf/index/java/awt/geom/AffineTransform;)Lorg/icepdf/index/java/awt/geom/PathIterator;

    move-result-object v1

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-static/range {v1 .. v9}, Lorg/apache/harmony/awt/gl/Crossing;->intersectPath(Lorg/icepdf/index/java/awt/geom/PathIterator;DDDD)I

    move-result v0

    goto :goto_0
.end method

.method public static isInsideEvenOdd(I)Z
    .locals 1
    .param p0, "cross"    # I

    .prologue
    .line 894
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInsideNonZero(I)Z
    .locals 1
    .param p0, "cross"    # I

    .prologue
    .line 887
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isZero(D)Z
    .locals 2
    .param p0, "val"    # D

    .prologue
    .line 524
    const-wide v0, -0x411b074a771c970fL    # -1.0E-5

    cmpg-double v0, v0, p0

    if-gez v0, :cond_0

    const-wide v0, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static solveCubic([D[D)I
    .locals 38
    .param p0, "eqn"    # [D
    .param p1, "res"    # [D

    .prologue
    .line 87
    const/16 v34, 0x3

    aget-wide v20, p0, v34

    .line 88
    .local v20, "d":D
    const-wide/16 v34, 0x0

    cmpl-double v34, v20, v34

    if-nez v34, :cond_0

    .line 89
    invoke-static/range {p0 .. p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveQuad([D[D)I

    move-result v34

    .line 129
    :goto_0
    return v34

    .line 91
    :cond_0
    const/16 v34, 0x2

    aget-wide v34, p0, v34

    div-double v14, v34, v20

    .line 92
    .local v14, "a":D
    const/16 v34, 0x1

    aget-wide v34, p0, v34

    div-double v16, v34, v20

    .line 93
    .local v16, "b":D
    const/16 v34, 0x0

    aget-wide v34, p0, v34

    div-double v18, v34, v20

    .line 94
    .local v18, "c":D
    const/16 v30, 0x0

    .line 96
    .local v30, "rc":I
    mul-double v34, v14, v14

    const-wide/high16 v36, 0x4008000000000000L    # 3.0

    mul-double v36, v36, v16

    sub-double v34, v34, v36

    const-wide/high16 v36, 0x4022000000000000L    # 9.0

    div-double v6, v34, v36

    .line 97
    .local v6, "Q":D
    const-wide/high16 v34, 0x4000000000000000L    # 2.0

    mul-double v34, v34, v14

    mul-double v34, v34, v14

    mul-double v34, v34, v14

    const-wide/high16 v36, 0x4022000000000000L    # 9.0

    mul-double v36, v36, v14

    mul-double v36, v36, v16

    sub-double v34, v34, v36

    const-wide/high16 v36, 0x403b000000000000L    # 27.0

    mul-double v36, v36, v18

    add-double v34, v34, v36

    const-wide/high16 v36, 0x404b000000000000L    # 54.0

    div-double v10, v34, v36

    .line 98
    .local v10, "R":D
    mul-double v34, v6, v6

    mul-double v8, v34, v6

    .line 99
    .local v8, "Q3":D
    mul-double v12, v10, v10

    .line 100
    .local v12, "R2":D
    neg-double v0, v14

    move-wide/from16 v34, v0

    const-wide/high16 v36, 0x4008000000000000L    # 3.0

    div-double v26, v34, v36

    .line 102
    .local v26, "n":D
    cmpg-double v34, v12, v8

    if-gez v34, :cond_1

    .line 103
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v34

    div-double v34, v10, v34

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->acos(D)D

    move-result-wide v34

    const-wide/high16 v36, 0x4008000000000000L    # 3.0

    div-double v32, v34, v36

    .line 104
    .local v32, "t":D
    const-wide v28, 0x4000c152382d7365L    # 2.0943951023931953

    .line 105
    .local v28, "p":D
    const-wide/high16 v34, -0x4000000000000000L    # -2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    mul-double v24, v34, v36

    .line 106
    .local v24, "m":D
    add-int/lit8 v31, v30, 0x1

    .end local v30    # "rc":I
    .local v31, "rc":I
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->cos(D)D

    move-result-wide v34

    mul-double v34, v34, v24

    add-double v34, v34, v26

    aput-wide v34, p1, v30

    .line 107
    add-int/lit8 v30, v31, 0x1

    .end local v31    # "rc":I
    .restart local v30    # "rc":I
    add-double v34, v32, v28

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->cos(D)D

    move-result-wide v34

    mul-double v34, v34, v24

    add-double v34, v34, v26

    aput-wide v34, p1, v31

    .line 108
    add-int/lit8 v31, v30, 0x1

    .end local v30    # "rc":I
    .restart local v31    # "rc":I
    sub-double v34, v32, v28

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->cos(D)D

    move-result-wide v34

    mul-double v34, v34, v24

    add-double v34, v34, v26

    aput-wide v34, p1, v30

    move/from16 v30, v31

    .line 129
    .end local v24    # "m":D
    .end local v28    # "p":D
    .end local v31    # "rc":I
    .end local v32    # "t":D
    .restart local v30    # "rc":I
    :goto_1
    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-static {v0, v1}, Lorg/apache/harmony/awt/gl/Crossing;->fixRoots([DI)I

    move-result v34

    goto/16 :goto_0

    .line 111
    :cond_1
    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v34

    sub-double v36, v12, v8

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    add-double v34, v34, v36

    const-wide v36, 0x3fd5555555555555L    # 0.3333333333333333

    invoke-static/range {v34 .. v37}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 112
    .local v2, "A":D
    const-wide/16 v34, 0x0

    cmpl-double v34, v10, v34

    if-lez v34, :cond_2

    .line 113
    neg-double v2, v2

    .line 116
    :cond_2
    const-wide v34, -0x4224832026284245L    # -1.0E-10

    cmpg-double v34, v34, v2

    if-gez v34, :cond_3

    const-wide v34, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v34, v2, v34

    if-gez v34, :cond_3

    .line 117
    add-int/lit8 v31, v30, 0x1

    .end local v30    # "rc":I
    .restart local v31    # "rc":I
    aput-wide v26, p1, v30

    move/from16 v30, v31

    .line 118
    .end local v31    # "rc":I
    .restart local v30    # "rc":I
    goto :goto_1

    .line 119
    :cond_3
    div-double v4, v6, v2

    .line 120
    .local v4, "B":D
    add-int/lit8 v31, v30, 0x1

    .end local v30    # "rc":I
    .restart local v31    # "rc":I
    add-double v34, v2, v4

    add-double v34, v34, v26

    aput-wide v34, p1, v30

    .line 122
    sub-double v22, v12, v8

    .line 123
    .local v22, "delta":D
    const-wide v34, -0x4224832026284245L    # -1.0E-10

    cmpg-double v34, v34, v22

    if-gez v34, :cond_4

    const-wide v34, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v34, v22, v34

    if-gez v34, :cond_4

    .line 124
    add-int/lit8 v30, v31, 0x1

    .end local v31    # "rc":I
    .restart local v30    # "rc":I
    add-double v34, v2, v4

    move-wide/from16 v0, v34

    neg-double v0, v0

    move-wide/from16 v34, v0

    const-wide/high16 v36, 0x4000000000000000L    # 2.0

    div-double v34, v34, v36

    add-double v34, v34, v26

    aput-wide v34, p1, v31

    goto :goto_1

    .end local v30    # "rc":I
    .restart local v31    # "rc":I
    :cond_4
    move/from16 v30, v31

    .end local v31    # "rc":I
    .restart local v30    # "rc":I
    goto :goto_1
.end method

.method public static solveQuad([D[D)I
    .locals 14
    .param p0, "eqn"    # [D
    .param p1, "res"    # [D

    .prologue
    .line 55
    const/4 v10, 0x2

    aget-wide v0, p0, v10

    .line 56
    .local v0, "a":D
    const/4 v10, 0x1

    aget-wide v2, p0, v10

    .line 57
    .local v2, "b":D
    const/4 v10, 0x0

    aget-wide v4, p0, v10

    .line 58
    .local v4, "c":D
    const/4 v8, 0x0

    .line 59
    .local v8, "rc":I
    const-wide/16 v10, 0x0

    cmpl-double v10, v0, v10

    if-nez v10, :cond_1

    .line 60
    const-wide/16 v10, 0x0

    cmpl-double v10, v2, v10

    if-nez v10, :cond_0

    .line 61
    const/4 v10, -0x1

    .line 77
    :goto_0
    return v10

    .line 63
    :cond_0
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "rc":I
    .local v9, "rc":I
    neg-double v10, v4

    div-double/2addr v10, v2

    aput-wide v10, p1, v8

    move v8, v9

    .line 77
    .end local v9    # "rc":I
    .restart local v8    # "rc":I
    :goto_1
    invoke-static {p1, v8}, Lorg/apache/harmony/awt/gl/Crossing;->fixRoots([DI)I

    move-result v10

    goto :goto_0

    .line 65
    :cond_1
    mul-double v10, v2, v2

    const-wide/high16 v12, 0x4010000000000000L    # 4.0

    mul-double/2addr v12, v0

    mul-double/2addr v12, v4

    sub-double v6, v10, v12

    .line 67
    .local v6, "d":D
    const-wide/16 v10, 0x0

    cmpg-double v10, v6, v10

    if-gez v10, :cond_2

    .line 68
    const/4 v10, 0x0

    goto :goto_0

    .line 70
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 71
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "rc":I
    .restart local v9    # "rc":I
    neg-double v10, v2

    add-double/2addr v10, v6

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    mul-double/2addr v12, v0

    div-double/2addr v10, v12

    aput-wide v10, p1, v8

    .line 73
    const-wide/16 v10, 0x0

    cmpl-double v10, v6, v10

    if-eqz v10, :cond_3

    .line 74
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "rc":I
    .restart local v8    # "rc":I
    neg-double v10, v2

    sub-double/2addr v10, v6

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    mul-double/2addr v12, v0

    div-double/2addr v10, v12

    aput-wide v10, p1, v9

    goto :goto_1

    .end local v8    # "rc":I
    .restart local v9    # "rc":I
    :cond_3
    move v8, v9

    .end local v9    # "rc":I
    .restart local v8    # "rc":I
    goto :goto_1
.end method

.method static sortBound([DI)V
    .locals 10
    .param p0, "bound"    # [D
    .param p1, "bc"    # I

    .prologue
    .line 531
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v3, p1, -0x4

    if-lt v0, v3, :cond_0

    .line 553
    return-void

    .line 532
    :cond_0
    move v2, v0

    .line 533
    .local v2, "k":I
    add-int/lit8 v1, v0, 0x4

    .local v1, "j":I
    :goto_1
    if-lt v1, p1, :cond_2

    .line 538
    if-eq v2, v0, :cond_1

    .line 539
    aget-wide v4, p0, v0

    .line 540
    .local v4, "tmp":D
    aget-wide v6, p0, v2

    aput-wide v6, p0, v0

    .line 541
    aput-wide v4, p0, v2

    .line 542
    add-int/lit8 v3, v0, 0x1

    aget-wide v4, p0, v3

    .line 543
    add-int/lit8 v3, v0, 0x1

    add-int/lit8 v6, v2, 0x1

    aget-wide v6, p0, v6

    aput-wide v6, p0, v3

    .line 544
    add-int/lit8 v3, v2, 0x1

    aput-wide v4, p0, v3

    .line 545
    add-int/lit8 v3, v0, 0x2

    aget-wide v4, p0, v3

    .line 546
    add-int/lit8 v3, v0, 0x2

    add-int/lit8 v6, v2, 0x2

    aget-wide v6, p0, v6

    aput-wide v6, p0, v3

    .line 547
    add-int/lit8 v3, v2, 0x2

    aput-wide v4, p0, v3

    .line 548
    add-int/lit8 v3, v0, 0x3

    aget-wide v4, p0, v3

    .line 549
    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v6, v2, 0x3

    aget-wide v6, p0, v6

    aput-wide v6, p0, v3

    .line 550
    add-int/lit8 v3, v2, 0x3

    aput-wide v4, p0, v3

    .line 531
    .end local v4    # "tmp":D
    :cond_1
    add-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 534
    :cond_2
    aget-wide v6, p0, v2

    aget-wide v8, p0, v1

    cmpl-double v3, v6, v8

    if-lez v3, :cond_3

    .line 535
    move v2, v1

    .line 533
    :cond_3
    add-int/lit8 v1, v1, 0x4

    goto :goto_1
.end method
