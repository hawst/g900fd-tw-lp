.class public Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;
.super Ljava/lang/Object;
.source "Crossing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/harmony/awt/gl/Crossing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QuadCurve"
.end annotation


# instance fields
.field Ax:D

.field Ay:D

.field Bx:D

.field By:D

.field ax:D

.field ay:D

.field bx:D

.field by:D


# direct methods
.method public constructor <init>(DDDDDD)V
    .locals 5
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "cx"    # D
    .param p7, "cy"    # D
    .param p9, "x2"    # D
    .param p11, "y2"    # D

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    sub-double v0, p9, p1

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ax:D

    .line 163
    sub-double v0, p11, p3

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ay:D

    .line 164
    sub-double v0, p5, p1

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    .line 165
    sub-double v0, p7, p3

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->by:D

    .line 167
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Bx:D

    .line 168
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ax:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Bx:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    .line 170
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->by:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->by:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->By:D

    .line 171
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ay:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->By:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ay:D

    .line 172
    return-void
.end method


# virtual methods
.method addBound([DI[DIDDZI)I
    .locals 13
    .param p1, "bound"    # [D
    .param p2, "bc"    # I
    .param p3, "res"    # [D
    .param p4, "rc"    # I
    .param p5, "minX"    # D
    .param p7, "maxX"    # D
    .param p9, "changeId"    # Z
    .param p10, "id"    # I

    .prologue
    .line 231
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "bc":I
    .local v2, "bc":I
    :goto_0
    move/from16 v0, p4

    if-lt v3, v0, :cond_0

    .line 246
    return v2

    .line 232
    :cond_0
    aget-wide v6, p3, v3

    .line 233
    .local v6, "t":D
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpl-double v8, v6, v8

    if-lez v8, :cond_1

    const-wide v8, 0x3ff0000a7c5ac472L    # 1.00001

    cmpg-double v8, v6, v8

    if-gez v8, :cond_1

    .line 234
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Bx:D

    add-double/2addr v8, v10

    mul-double v4, v6, v8

    .line 235
    .local v4, "rx":D
    cmpg-double v8, p5, v4

    if-gtz v8, :cond_1

    cmpg-double v8, v4, p7

    if-gtz v8, :cond_1

    .line 236
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "bc":I
    .restart local p2    # "bc":I
    aput-wide v6, p1, v2

    .line 237
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "bc":I
    .restart local v2    # "bc":I
    aput-wide v4, p1, p2

    .line 238
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "bc":I
    .restart local p2    # "bc":I
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ay:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->By:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    aput-wide v8, p1, v2

    .line 239
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "bc":I
    .restart local v2    # "bc":I
    move/from16 v0, p10

    int-to-double v8, v0

    aput-wide v8, p1, p2

    .line 240
    if-eqz p9, :cond_1

    .line 241
    add-int/lit8 p10, p10, 0x1

    move p2, v2

    .line 231
    .end local v2    # "bc":I
    .end local v4    # "rx":D
    .restart local p2    # "bc":I
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v2, p2

    .end local p2    # "bc":I
    .restart local v2    # "bc":I
    goto :goto_0

    :cond_1
    move p2, v2

    .end local v2    # "bc":I
    .restart local p2    # "bc":I
    goto :goto_1
.end method

.method cross([DIDD)I
    .locals 13
    .param p1, "res"    # [D
    .param p2, "rc"    # I
    .param p3, "py1"    # D
    .param p5, "py2"    # D

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 177
    .local v0, "cross":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p2, :cond_0

    .line 211
    return v0

    .line 178
    :cond_0
    aget-wide v6, p1, v1

    .line 181
    .local v6, "t":D
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpg-double v8, v6, v8

    if-ltz v8, :cond_1

    const-wide v8, 0x3ff0000a7c5ac472L    # 1.00001

    cmpl-double v8, v6, v8

    if-lez v8, :cond_2

    .line 177
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_2
    const-wide v8, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v8, v6, v8

    if-gez v8, :cond_4

    .line 186
    const-wide/16 v8, 0x0

    cmpg-double v8, p3, v8

    if-gez v8, :cond_1

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    const-wide/16 v10, 0x0

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_3

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    :goto_2
    const-wide/16 v10, 0x0

    cmpg-double v8, v8, v10

    if-gez v8, :cond_1

    .line 187
    add-int/lit8 v0, v0, -0x1

    .line 189
    goto :goto_1

    .line 186
    :cond_3
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ax:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    sub-double/2addr v8, v10

    goto :goto_2

    .line 192
    :cond_4
    const-wide v8, 0x3fefffeb074a771dL    # 0.99999

    cmpl-double v8, v6, v8

    if-lez v8, :cond_6

    .line 193
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ay:D

    cmpg-double v8, p3, v8

    if-gez v8, :cond_1

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ax:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_5

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->ax:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    sub-double/2addr v8, v10

    :goto_3
    const-wide/16 v10, 0x0

    cmpl-double v8, v8, v10

    if-lez v8, :cond_1

    .line 194
    add-int/lit8 v0, v0, 0x1

    .line 196
    goto :goto_1

    .line 193
    :cond_5
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    goto :goto_3

    .line 199
    :cond_6
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ay:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->By:D

    add-double/2addr v8, v10

    mul-double v4, v6, v8

    .line 201
    .local v4, "ry":D
    cmpl-double v8, v4, p5

    if-lez v8, :cond_1

    .line 202
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->bx:D

    add-double v2, v8, v10

    .line 204
    .local v2, "rxt":D
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpl-double v8, v2, v8

    if-lez v8, :cond_7

    const-wide v8, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v8, v2, v8

    if-ltz v8, :cond_1

    .line 207
    :cond_7
    const-wide/16 v8, 0x0

    cmpl-double v8, v2, v8

    if-lez v8, :cond_8

    const/4 v8, 0x1

    :goto_4
    add-int/2addr v0, v8

    goto :goto_1

    :cond_8
    const/4 v8, -0x1

    goto :goto_4
.end method

.method solveExtrem([D)I
    .locals 10
    .param p1, "res"    # [D

    .prologue
    const-wide/16 v8, 0x0

    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "rc":I
    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    cmpl-double v2, v2, v8

    if-eqz v2, :cond_0

    .line 222
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "rc":I
    .local v1, "rc":I
    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Bx:D

    neg-double v2, v2

    iget-wide v4, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    iget-wide v6, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    add-double/2addr v4, v6

    div-double/2addr v2, v4

    aput-wide v2, p1, v0

    move v0, v1

    .line 224
    .end local v1    # "rc":I
    .restart local v0    # "rc":I
    :cond_0
    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ay:D

    cmpl-double v2, v2, v8

    if-eqz v2, :cond_1

    .line 225
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "rc":I
    .restart local v1    # "rc":I
    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->By:D

    neg-double v2, v2

    iget-wide v4, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ay:D

    iget-wide v6, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ay:D

    add-double/2addr v4, v6

    div-double/2addr v2, v4

    aput-wide v2, p1, v0

    move v0, v1

    .line 227
    .end local v1    # "rc":I
    .restart local v0    # "rc":I
    :cond_1
    return v0
.end method

.method solvePoint([DD)I
    .locals 4
    .param p1, "res"    # [D
    .param p2, "px"    # D

    .prologue
    .line 215
    const/4 v1, 0x3

    new-array v0, v1, [D

    const/4 v1, 0x0

    neg-double v2, p2

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Bx:D

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$QuadCurve;->Ax:D

    aput-wide v2, v0, v1

    .line 216
    .local v0, "eqn":[D
    invoke-static {v0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveQuad([D[D)I

    move-result v1

    return v1
.end method
