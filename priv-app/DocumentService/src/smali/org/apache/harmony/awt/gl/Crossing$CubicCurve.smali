.class public Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;
.super Ljava/lang/Object;
.source "Crossing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/harmony/awt/gl/Crossing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CubicCurve"
.end annotation


# instance fields
.field Ax:D

.field Ax3:D

.field Ay:D

.field Bx:D

.field Bx2:D

.field By:D

.field Cx:D

.field Cy:D

.field ax:D

.field ay:D

.field bx:D

.field by:D

.field cx:D

.field cy:D


# direct methods
.method public constructor <init>(DDDDDDDD)V
    .locals 5
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "cx1"    # D
    .param p7, "cy1"    # D
    .param p9, "cx2"    # D
    .param p11, "cy2"    # D
    .param p13, "x2"    # D
    .param p15, "y2"    # D

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    sub-double v0, p13, p1

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    .line 262
    sub-double v0, p15, p3

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ay:D

    .line 263
    sub-double v0, p5, p1

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    .line 264
    sub-double v0, p7, p3

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->by:D

    .line 265
    sub-double v0, p9, p1

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    .line 266
    sub-double v0, p11, p3

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cy:D

    .line 268
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    .line 269
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx:D

    .line 270
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx:D

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax:D

    .line 272
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->by:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->by:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->by:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    .line 273
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cy:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cy:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cy:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->By:D

    .line 274
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ay:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->By:D

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ay:D

    .line 276
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax3:D

    .line 277
    iget-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx:D

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx2:D

    .line 278
    return-void
.end method


# virtual methods
.method addBound([DI[DIDDZI)I
    .locals 13
    .param p1, "bound"    # [D
    .param p2, "bc"    # I
    .param p3, "res"    # [D
    .param p4, "rc"    # I
    .param p5, "minX"    # D
    .param p7, "maxX"    # D
    .param p9, "changeId"    # Z
    .param p10, "id"    # I

    .prologue
    .line 341
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, p2

    .end local p2    # "bc":I
    .local v2, "bc":I
    :goto_0
    move/from16 v0, p4

    if-lt v3, v0, :cond_0

    .line 356
    return v2

    .line 342
    :cond_0
    aget-wide v6, p3, v3

    .line 343
    .local v6, "t":D
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpl-double v8, v6, v8

    if-lez v8, :cond_1

    const-wide v8, 0x3ff0000a7c5ac472L    # 1.00001

    cmpg-double v8, v6, v8

    if-gez v8, :cond_1

    .line 344
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    add-double/2addr v8, v10

    mul-double v4, v6, v8

    .line 345
    .local v4, "rx":D
    cmpg-double v8, p5, v4

    if-gtz v8, :cond_1

    cmpg-double v8, v4, p7

    if-gtz v8, :cond_1

    .line 346
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "bc":I
    .restart local p2    # "bc":I
    aput-wide v6, p1, v2

    .line 347
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "bc":I
    .restart local v2    # "bc":I
    aput-wide v4, p1, p2

    .line 348
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "bc":I
    .restart local p2    # "bc":I
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ay:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->By:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    aput-wide v8, p1, v2

    .line 349
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "bc":I
    .restart local v2    # "bc":I
    move/from16 v0, p10

    int-to-double v8, v0

    aput-wide v8, p1, p2

    .line 350
    if-eqz p9, :cond_1

    .line 351
    add-int/lit8 p10, p10, 0x1

    move p2, v2

    .line 341
    .end local v2    # "bc":I
    .end local v4    # "rx":D
    .restart local p2    # "bc":I
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v2, p2

    .end local p2    # "bc":I
    .restart local v2    # "bc":I
    goto :goto_0

    :cond_1
    move p2, v2

    .end local v2    # "bc":I
    .restart local p2    # "bc":I
    goto :goto_1
.end method

.method cross([DIDD)I
    .locals 13
    .param p1, "res"    # [D
    .param p2, "rc"    # I
    .param p3, "py1"    # D
    .param p5, "py2"    # D

    .prologue
    .line 281
    const/4 v0, 0x0

    .line 282
    .local v0, "cross":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p2, :cond_0

    .line 322
    return v0

    .line 283
    :cond_0
    aget-wide v6, p1, v1

    .line 286
    .local v6, "t":D
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpg-double v8, v6, v8

    if-ltz v8, :cond_1

    const-wide v8, 0x3ff0000a7c5ac472L    # 1.00001

    cmpl-double v8, v6, v8

    if-lez v8, :cond_2

    .line 282
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 290
    :cond_2
    const-wide v8, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v8, v6, v8

    if-gez v8, :cond_5

    .line 291
    const-wide/16 v8, 0x0

    cmpg-double v8, p3, v8

    if-gez v8, :cond_1

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    const-wide/16 v10, 0x0

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_3

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    :goto_2
    const-wide/16 v10, 0x0

    cmpg-double v8, v8, v10

    if-gez v8, :cond_1

    .line 292
    add-int/lit8 v0, v0, -0x1

    .line 294
    goto :goto_1

    .line 291
    :cond_3
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_4

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    sub-double/2addr v8, v10

    goto :goto_2

    :cond_4
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    sub-double/2addr v8, v10

    goto :goto_2

    .line 297
    :cond_5
    const-wide v8, 0x3fefffeb074a771dL    # 0.99999

    cmpl-double v8, v6, v8

    if-lez v8, :cond_8

    .line 298
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ay:D

    cmpg-double v8, p3, v8

    if-gez v8, :cond_1

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_6

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    sub-double/2addr v8, v10

    :goto_3
    const-wide/16 v10, 0x0

    cmpl-double v8, v8, v10

    if-lez v8, :cond_1

    .line 299
    add-int/lit8 v0, v0, 0x1

    .line 301
    goto :goto_1

    .line 298
    :cond_6
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_7

    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->cx:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    sub-double/2addr v8, v10

    goto :goto_3

    :cond_7
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->bx:D

    goto :goto_3

    .line 304
    :cond_8
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ay:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->By:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    add-double/2addr v8, v10

    mul-double v4, v6, v8

    .line 306
    .local v4, "ry":D
    cmpl-double v8, v4, p5

    if-lez v8, :cond_1

    .line 307
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax3:D

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx2:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    add-double v2, v8, v10

    .line 309
    .local v2, "rxt":D
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpl-double v8, v2, v8

    if-lez v8, :cond_9

    const-wide v8, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v8, v2, v8

    if-gez v8, :cond_9

    .line 310
    iget-wide v8, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax3:D

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax3:D

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    iget-wide v10, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx2:D

    add-double v2, v8, v10

    .line 312
    const-wide v8, -0x411b074a771c970fL    # -1.0E-5

    cmpg-double v8, v2, v8

    if-ltz v8, :cond_1

    const-wide v8, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpl-double v8, v2, v8

    if-gtz v8, :cond_1

    .line 316
    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->ax:D

    .line 318
    :cond_9
    const-wide/16 v8, 0x0

    cmpl-double v8, v2, v8

    if-lez v8, :cond_a

    const/4 v8, 0x1

    :goto_4
    add-int/2addr v0, v8

    goto/16 :goto_1

    :cond_a
    const/4 v8, -0x1

    goto :goto_4
.end method

.method solveExtremX([D)I
    .locals 4
    .param p1, "res"    # [D

    .prologue
    .line 331
    const/4 v1, 0x3

    new-array v0, v1, [D

    const/4 v1, 0x0

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx2:D

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax3:D

    aput-wide v2, v0, v1

    .line 332
    .local v0, "eqn":[D
    invoke-static {v0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveQuad([D[D)I

    move-result v1

    return v1
.end method

.method solveExtremY([D)I
    .locals 6
    .param p1, "res"    # [D

    .prologue
    .line 336
    const/4 v1, 0x3

    new-array v0, v1, [D

    const/4 v1, 0x0

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cy:D

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->By:D

    iget-wide v4, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->By:D

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ay:D

    iget-wide v4, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ay:D

    add-double/2addr v2, v4

    iget-wide v4, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ay:D

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    .line 337
    .local v0, "eqn":[D
    invoke-static {v0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveQuad([D[D)I

    move-result v1

    return v1
.end method

.method solvePoint([DD)I
    .locals 4
    .param p1, "res"    # [D
    .param p2, "px"    # D

    .prologue
    .line 326
    const/4 v1, 0x4

    new-array v0, v1, [D

    const/4 v1, 0x0

    neg-double v2, p2

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Cx:D

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Bx:D

    aput-wide v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lorg/apache/harmony/awt/gl/Crossing$CubicCurve;->Ax:D

    aput-wide v2, v0, v1

    .line 327
    .local v0, "eqn":[D
    invoke-static {v0, p1}, Lorg/apache/harmony/awt/gl/Crossing;->solveCubic([D[D)I

    move-result v1

    return v1
.end method
