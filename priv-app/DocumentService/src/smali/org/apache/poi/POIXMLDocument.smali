.class public abstract Lorg/apache/poi/POIXMLDocument;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "POIXMLDocument.java"


# static fields
.field public static final DOCUMENT_CREATOR:Ljava/lang/String; = "Apache POI"

.field public static final OLE_OBJECT_REL_TYPE:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject"

.field public static final PACK_OBJECT_REL_TYPE:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package"


# instance fields
.field private pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

.field private properties:Lorg/apache/poi/POIXMLProperties;


# direct methods
.method protected constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V
    .locals 0
    .param p1, "pkg"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 57
    iput-object p1, p0, Lorg/apache/poi/POIXMLDocument;->pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    .line 58
    return-void
.end method

.method public static hasOOXMLHeader(Ljava/io/InputStream;)Z
    .locals 8
    .param p0, "inp"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 110
    invoke-virtual {p0, v4}, Ljava/io/InputStream;->mark(I)V

    .line 112
    new-array v0, v4, [B

    .line 113
    .local v0, "header":[B
    invoke-static {p0, v0}, Lorg/apache/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 116
    instance-of v4, p0, Ljava/io/PushbackInputStream;

    if-eqz v4, :cond_0

    move-object v1, p0

    .line 117
    check-cast v1, Ljava/io/PushbackInputStream;

    .line 118
    .local v1, "pin":Ljava/io/PushbackInputStream;
    invoke-virtual {v1, v0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 125
    .end local v1    # "pin":Ljava/io/PushbackInputStream;
    :goto_0
    aget-byte v4, v0, v3

    sget-object v5, Lorg/apache/poi/poifs/common/POIFSConstants;->OOXML_FILE_HEADER:[B

    aget-byte v5, v5, v3

    if-ne v4, v5, :cond_1

    .line 126
    aget-byte v4, v0, v2

    sget-object v5, Lorg/apache/poi/poifs/common/POIFSConstants;->OOXML_FILE_HEADER:[B

    aget-byte v5, v5, v2

    if-ne v4, v5, :cond_1

    .line 127
    aget-byte v4, v0, v6

    sget-object v5, Lorg/apache/poi/poifs/common/POIFSConstants;->OOXML_FILE_HEADER:[B

    aget-byte v5, v5, v6

    if-ne v4, v5, :cond_1

    .line 128
    aget-byte v4, v0, v7

    sget-object v5, Lorg/apache/poi/poifs/common/POIFSConstants;->OOXML_FILE_HEADER:[B

    aget-byte v5, v5, v7

    if-ne v4, v5, :cond_1

    .line 124
    :goto_1
    return v2

    .line 120
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    goto :goto_0

    :cond_1
    move v2, v3

    .line 124
    goto :goto_1
.end method

.method public static openPackage(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    :try_start_0
    invoke-static {p0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->open(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/OPCPackage;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public abstract getAllEmbedds()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/openxml4j/opc/PackagePart;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation
.end method

.method protected getCorePart()Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lorg/apache/poi/POIXMLDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    return-object v0
.end method

.method public getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/poi/POIXMLDocument;->pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    return-object v0
.end method

.method public getProperties()Lorg/apache/poi/POIXMLProperties;
    .locals 3

    .prologue
    .line 137
    iget-object v1, p0, Lorg/apache/poi/POIXMLDocument;->properties:Lorg/apache/poi/POIXMLProperties;

    if-nez v1, :cond_0

    .line 139
    :try_start_0
    new-instance v1, Lorg/apache/poi/POIXMLProperties;

    iget-object v2, p0, Lorg/apache/poi/POIXMLDocument;->pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-direct {v1, v2}, Lorg/apache/poi/POIXMLProperties;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    iput-object v1, p0, Lorg/apache/poi/POIXMLDocument;->properties:Lorg/apache/poi/POIXMLProperties;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/POIXMLDocument;->properties:Lorg/apache/poi/POIXMLProperties;

    return-object v1

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lorg/apache/poi/POIXMLException;

    invoke-direct {v1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected getRelatedByType(Ljava/lang/String;)[Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 6
    .param p1, "contentType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lorg/apache/poi/POIXMLDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v4

    invoke-virtual {v4, p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v2

    .line 90
    .local v2, "partsC":Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;
    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->size()I

    move-result v4

    new-array v1, v4, [Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 91
    .local v1, "parts":[Lorg/apache/poi/openxml4j/opc/PackagePart;
    const/4 v0, 0x0

    .line 92
    .local v0, "count":I
    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 96
    return-object v1

    .line 92
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 93
    .local v3, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    invoke-virtual {p0}, Lorg/apache/poi/POIXMLDocument;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v5

    invoke-virtual {v5, v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelatedPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v5

    aput-object v5, v1, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final load(Lorg/apache/poi/POIXMLFactory;)V
    .locals 3
    .param p1, "factory"    # Lorg/apache/poi/POIXMLFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 155
    .local v0, "context":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/POIXMLDocumentPart;>;"
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/POIXMLDocument;->read(Lorg/apache/poi/POIXMLFactory;Ljava/util/Map;)V
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    invoke-virtual {p0}, Lorg/apache/poi/POIXMLDocument;->onDocumentRead()V

    .line 160
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 161
    return-void

    .line 156
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
    new-instance v2, Lorg/apache/poi/POIXMLException;

    invoke-direct {v2, v1}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public final write(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 173
    .local v0, "context":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/openxml4j/opc/PackagePart;>;"
    invoke-virtual {p0, v0}, Lorg/apache/poi/POIXMLDocument;->onSave(Ljava/util/Set;)V

    .line 174
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 177
    invoke-virtual {p0}, Lorg/apache/poi/POIXMLDocument;->getProperties()Lorg/apache/poi/POIXMLProperties;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/POIXMLProperties;->commit()V

    .line 179
    invoke-virtual {p0}, Lorg/apache/poi/POIXMLDocument;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->save(Ljava/io/OutputStream;)V

    .line 180
    return-void
.end method
