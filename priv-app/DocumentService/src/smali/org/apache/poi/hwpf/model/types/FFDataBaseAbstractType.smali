.class public abstract Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;
.super Ljava/lang/Object;
.source "FFDataBaseAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field protected static final ITYPETXT_CALC:B = 0x0t

.field public static final ITYPETXT_CURDATE:B = 0x0t

.field public static final ITYPETXT_CURTIME:B = 0x0t

.field public static final ITYPETXT_DATE:B = 0x0t

.field public static final ITYPETXT_NUM:B = 0x0t

.field public static final ITYPETXT_REG:B = 0x0t

.field public static final ITYPE_CHCK:B = 0x1t

.field public static final ITYPE_DROP:B = 0x2t

.field public static final ITYPE_TEXT:B

.field private static final fHasListBox:Lorg/apache/poi/util/BitField;

.field private static final fOwnHelp:Lorg/apache/poi/util/BitField;

.field private static final fOwnStat:Lorg/apache/poi/util/BitField;

.field private static final fProt:Lorg/apache/poi/util/BitField;

.field private static final fRecalc:Lorg/apache/poi/util/BitField;

.field private static final iRes:Lorg/apache/poi/util/BitField;

.field private static final iSize:Lorg/apache/poi/util/BitField;

.field private static final iType:Lorg/apache/poi/util/BitField;

.field private static final iTypeTxt:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_version:J

.field protected field_2_bits:S

.field protected field_3_cch:I

.field protected field_4_hps:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iType:Lorg/apache/poi/util/BitField;

    .line 57
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x7c

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iRes:Lorg/apache/poi/util/BitField;

    .line 58
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fOwnHelp:Lorg/apache/poi/util/BitField;

    .line 59
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fOwnStat:Lorg/apache/poi/util/BitField;

    .line 60
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fProt:Lorg/apache/poi/util/BitField;

    .line 61
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iSize:Lorg/apache/poi/util/BitField;

    .line 62
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x3800

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iTypeTxt:Lorg/apache/poi/util/BitField;

    .line 75
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x4000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fRecalc:Lorg/apache/poi/util/BitField;

    .line 76
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0x8000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fHasListBox:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 112
    const/16 v0, 0xa

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    if-ne p0, p1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v1

    .line 120
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 121
    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 123
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 124
    check-cast v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;

    .line 125
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;
    iget-wide v4, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    iget-wide v6, v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 126
    goto :goto_0

    .line 127
    :cond_4
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 128
    goto :goto_0

    .line 129
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 130
    goto :goto_0

    .line 131
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 132
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 86
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    .line 87
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 88
    add-int/lit8 v0, p2, 0x6

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    .line 89
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    .line 90
    return-void
.end method

.method public getBits()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 200
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    return v0
.end method

.method public getCch()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 218
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    return v0
.end method

.method public getHps()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 236
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    return v0
.end method

.method public getIRes()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 285
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iRes:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getIType()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 265
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iType:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getITypeTxt()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 385
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iTypeTxt:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getVersion()J
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 182
    iget-wide v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 139
    const/16 v0, 0x1f

    .line 140
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 142
    .local v1, "result":I
    iget-wide v2, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    iget-wide v4, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    .line 141
    add-int/lit8 v1, v2, 0x1f

    .line 143
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    add-int v1, v2, v3

    .line 144
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    add-int v1, v2, v3

    .line 145
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    add-int v1, v2, v3

    .line 146
    return v1
.end method

.method public isFHasListBox()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 425
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fHasListBox:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOwnHelp()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 305
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fOwnHelp:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOwnStat()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 325
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fOwnStat:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFProt()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 345
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fProt:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRecalc()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 405
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fRecalc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isISize()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 365
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iSize:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 94
    add-int/lit8 v0, p2, 0x0

    iget-wide v2, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    invoke-static {p1, v0, v2, v3}, Lorg/apache/poi/util/LittleEndian;->putUInt([BIJ)V

    .line 95
    add-int/lit8 v0, p2, 0x4

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 96
    add-int/lit8 v0, p2, 0x6

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 97
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 98
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 103
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->serialize([BI)V

    .line 104
    return-object v0
.end method

.method public setBits(S)V
    .locals 0
    .param p1, "field_2_bits"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 209
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 210
    return-void
.end method

.method public setCch(I)V
    .locals 0
    .param p1, "field_3_cch"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 227
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    .line 228
    return-void
.end method

.method public setFHasListBox(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 415
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fHasListBox:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 416
    return-void
.end method

.method public setFOwnHelp(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 295
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fOwnHelp:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 296
    return-void
.end method

.method public setFOwnStat(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 315
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fOwnStat:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 316
    return-void
.end method

.method public setFProt(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 335
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fProt:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 336
    return-void
.end method

.method public setFRecalc(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 395
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->fRecalc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 396
    return-void
.end method

.method public setHps(I)V
    .locals 0
    .param p1, "field_4_hps"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 245
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    .line 246
    return-void
.end method

.method public setIRes(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 275
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iRes:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 276
    return-void
.end method

.method public setISize(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 355
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iSize:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 356
    return-void
.end method

.method public setIType(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 255
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iType:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 256
    return-void
.end method

.method public setITypeTxt(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 375
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->iTypeTxt:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    .line 376
    return-void
.end method

.method public setVersion(J)V
    .locals 1
    .param p1, "field_1_version"    # J
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 191
    iput-wide p1, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    .line 192
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[FFDataBase]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string/jumbo v1, "    .version              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_1_version:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v1, "    .bits                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_2_bits:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string/jumbo v1, "         .iType                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->getIType()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    const-string/jumbo v1, "         .iRes                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->getIRes()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 160
    const-string/jumbo v1, "         .fOwnHelp                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->isFOwnHelp()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    const-string/jumbo v1, "         .fOwnStat                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->isFOwnStat()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    const-string/jumbo v1, "         .fProt                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->isFProt()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 163
    const-string/jumbo v1, "         .iSize                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->isISize()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    const-string/jumbo v1, "         .iTypeTxt                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->getITypeTxt()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 165
    const-string/jumbo v1, "         .fRecalc                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->isFRecalc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v1, "         .fHasListBox              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->isFHasListBox()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    const-string/jumbo v1, "    .cch                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_3_cch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string/jumbo v1, "    .hps                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;->field_4_hps:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v1, "[/FFDataBase]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
