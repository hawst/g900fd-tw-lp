.class public Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;
.super Ljava/lang/Object;
.source "OfficeDrawingsImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;


# instance fields
.field private final _escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

.field private final _fspaTable:Lorg/apache/poi/hwpf/model/FSPATable;

.field private final _mainStream:[B


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/FSPATable;Lorg/apache/poi/hwpf/model/EscherRecordHolder;[B)V
    .locals 0
    .param p1, "fspaTable"    # Lorg/apache/poi/hwpf/model/FSPATable;
    .param p2, "escherRecordHolder"    # Lorg/apache/poi/hwpf/model/EscherRecordHolder;
    .param p3, "mainStream"    # [B

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_fspaTable:Lorg/apache/poi/hwpf/model/FSPATable;

    .line 50
    iput-object p2, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 51
    iput-object p3, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_mainStream:[B

    .line 52
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Ljava/util/List;
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherSpContainerList(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherBlipRecord;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getBitmapRecord(I)Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v0

    return-object v0
.end method

.method private getBitmapRecord(I)Lorg/apache/poi/ddf/EscherBlipRecord;
    .locals 11
    .param p1, "bitmapIndex"    # I

    .prologue
    const/4 v8, 0x0

    .line 56
    iget-object v9, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 57
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;->getBStoreContainers()Ljava/util/List;

    move-result-object v1

    .line 58
    .local v1, "bContainers":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_1

    :cond_0
    move-object v5, v8

    .line 103
    :goto_0
    return-object v5

    .line 61
    :cond_1
    const/4 v9, 0x0

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 62
    .local v0, "bContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v2

    .line 64
    .local v2, "bitmapRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    if-ge v9, p1, :cond_2

    move-object v5, v8

    .line 65
    goto :goto_0

    .line 67
    :cond_2
    add-int/lit8 v9, p1, -0x1

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherRecord;

    .line 69
    .local v5, "imageRecord":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v9, v5, Lorg/apache/poi/ddf/EscherBlipRecord;

    if-eqz v9, :cond_3

    .line 71
    check-cast v5, Lorg/apache/poi/ddf/EscherBlipRecord;

    goto :goto_0

    .line 74
    :cond_3
    instance-of v9, v5, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v9, :cond_5

    move-object v4, v5

    .line 76
    check-cast v4, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 78
    .local v4, "bseRecord":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v3

    .line 79
    .local v3, "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    if-eqz v3, :cond_4

    move-object v5, v3

    .line 81
    goto :goto_0

    .line 84
    :cond_4
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v9

    if-lez v9, :cond_5

    .line 90
    new-instance v7, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v7}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 91
    .local v7, "recordFactory":Lorg/apache/poi/ddf/EscherRecordFactory;
    iget-object v9, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_mainStream:[B

    .line 92
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v10

    .line 91
    invoke-interface {v7, v9, v10}, Lorg/apache/poi/ddf/EscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v6

    .line 94
    .local v6, "record":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v9, v6, Lorg/apache/poi/ddf/EscherBlipRecord;

    if-eqz v9, :cond_5

    .line 96
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_mainStream:[B

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v9

    invoke-virtual {v6, v8, v9, v7}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 98
    check-cast v6, Lorg/apache/poi/ddf/EscherBlipRecord;

    .end local v6    # "record":Lorg/apache/poi/ddf/EscherRecord;
    move-object v5, v6

    goto :goto_0

    .end local v3    # "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    .end local v4    # "bseRecord":Lorg/apache/poi/ddf/EscherBSERecord;
    .end local v7    # "recordFactory":Lorg/apache/poi/ddf/EscherRecordFactory;
    :cond_5
    move-object v5, v8

    .line 103
    goto :goto_0
.end method

.method private getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 4
    .param p1, "shapeId"    # I

    .prologue
    .line 111
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 112
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;->getSpContainers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 111
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 121
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 112
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 115
    .local v1, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v3, -0xff6

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 116
    .local v0, "escherSpRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v3

    if-ne v3, p1, :cond_0

    goto :goto_0
.end method

.method private getEscherSpContainerList(I)Ljava/util/List;
    .locals 5
    .param p1, "shapeId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 130
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;->getSpgrContainers()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 129
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v1, "recordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    .end local v1    # "recordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    :goto_0
    return-object v1

    .line 130
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 133
    .local v2, "spgrContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v4, -0xff6

    invoke-virtual {v2, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 134
    .local v0, "escherSpRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 137
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private getOfficeDrawing(Lorg/apache/poi/hwpf/model/FSPA;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    .locals 1
    .param p1, "fspa"    # Lorg/apache/poi/hwpf/model/FSPA;

    .prologue
    .line 151
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;-><init>(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;Lorg/apache/poi/hwpf/model/FSPA;)V

    return-object v0
.end method


# virtual methods
.method public getEscherBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOfficeDrawingAt(I)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    .locals 2
    .param p1, "characterPosition"    # I

    .prologue
    .line 350
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_fspaTable:Lorg/apache/poi/hwpf/model/FSPATable;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/model/FSPATable;->getFspaFromCp(I)Lorg/apache/poi/hwpf/model/FSPA;

    move-result-object v0

    .line 351
    .local v0, "fspa":Lorg/apache/poi/hwpf/model/FSPA;
    if-nez v0, :cond_0

    .line 352
    const/4 v1, 0x0

    .line 354
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getOfficeDrawing(Lorg/apache/poi/hwpf/model/FSPA;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    move-result-object v1

    goto :goto_0
.end method

.method public getOfficeDrawings()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 359
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 360
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;>;"
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_fspaTable:Lorg/apache/poi/hwpf/model/FSPATable;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FSPATable;->getShapes()[Lorg/apache/poi/hwpf/model/FSPA;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 364
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    return-object v2

    .line 360
    :cond_0
    aget-object v0, v3, v2

    .line 362
    .local v0, "fspa":Lorg/apache/poi/hwpf/model/FSPA;
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getOfficeDrawing(Lorg/apache/poi/hwpf/model/FSPA;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getOfficeDrawingsArray()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 369
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;>;"
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->_fspaTable:Lorg/apache/poi/hwpf/model/FSPATable;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FSPATable;->getShapes()[Lorg/apache/poi/hwpf/model/FSPA;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 374
    return-object v1

    .line 369
    :cond_0
    aget-object v0, v3, v2

    .line 371
    .local v0, "fspa":Lorg/apache/poi/hwpf/model/FSPA;
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getOfficeDrawing(Lorg/apache/poi/hwpf/model/FSPA;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
