.class public abstract Lorg/apache/poi/hwpf/model/types/PAPAbstractType;
.super Ljava/lang/Object;
.source "PAPAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field protected static final BRCL_DOUBLE:B = 0x2t

.field protected static final BRCL_SHADOW:B = 0x3t

.field protected static final BRCL_SINGLE:B = 0x0t

.field protected static final BRCL_THICK:B = 0x1t

.field protected static final BRCP_BAR_TO_LEFT_OF_PARAGRAPH:B = 0x10t

.field protected static final BRCP_BORDER_ABOVE:B = 0x1t

.field protected static final BRCP_BORDER_BELOW:B = 0x2t

.field protected static final BRCP_BOX_AROUND:B = 0xft

.field protected static final BRCP_NONE:B = 0x0t

.field protected static final FMINHEIGHT_AT_LEAST:Z = true

.field protected static final FMINHEIGHT_EXACT:Z = false

.field protected static final WALIGNFONT_AUTO:B = 0x4t

.field protected static final WALIGNFONT_CENTERED:B = 0x1t

.field protected static final WALIGNFONT_HANGING:B = 0x0t

.field protected static final WALIGNFONT_ROMAN:B = 0x2t

.field protected static final WALIGNFONT_VARIABLE:B = 0x3t

.field private static fBackward:Lorg/apache/poi/util/BitField;

.field private static fRotateFont:Lorg/apache/poi/util/BitField;

.field private static fVertical:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_10_fNoLnn:Z

.field protected field_11_lspd:Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

.field protected field_12_dyaBefore:I

.field protected field_13_dyaAfter:I

.field protected field_14_fInTable:Z

.field protected field_15_finTableW97:Z

.field protected field_16_fTtp:Z

.field protected field_17_dxaAbs:I

.field protected field_18_dyaAbs:I

.field protected field_19_dxaWidth:I

.field protected field_1_istd:I

.field protected field_20_fBrLnAbove:Z

.field protected field_21_fBrLnBelow:Z

.field protected field_22_pcVert:B

.field protected field_23_pcHorz:B

.field protected field_24_wr:B

.field protected field_25_fNoAutoHyph:Z

.field protected field_26_dyaHeight:I

.field protected field_27_fMinHeight:Z

.field protected field_28_dcs:Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

.field protected field_29_dyaFromText:I

.field protected field_2_fSideBySide:Z

.field protected field_30_dxaFromText:I

.field protected field_31_fLocked:Z

.field protected field_32_fWidowControl:Z

.field protected field_33_fKinsoku:Z

.field protected field_34_fWordWrap:Z

.field protected field_35_fOverflowPunct:Z

.field protected field_36_fTopLinePunct:Z

.field protected field_37_fAutoSpaceDE:Z

.field protected field_38_fAutoSpaceDN:Z

.field protected field_39_wAlignFont:I

.field protected field_3_fKeep:Z

.field protected field_40_fontAlign:S

.field protected field_41_lvl:B

.field protected field_42_fBiDi:Z

.field protected field_43_fNumRMIns:Z

.field protected field_44_fCrLf:Z

.field protected field_45_fUsePgsuSettings:Z

.field protected field_46_fAdjustRight:Z

.field protected field_47_itap:I

.field protected field_48_fInnerTableCell:Z

.field protected field_49_fOpenTch:Z

.field protected field_4_fKeepFollow:Z

.field protected field_50_fTtpEmbedded:Z

.field protected field_51_dxcRight:S

.field protected field_52_dxcLeft:S

.field protected field_53_dxcLeft1:S

.field protected field_54_fDyaBeforeAuto:Z

.field protected field_55_fDyaAfterAuto:Z

.field protected field_56_dxaRight:I

.field protected field_57_dxaLeft:I

.field protected field_58_dxaLeft1:I

.field protected field_59_jc:B

.field protected field_5_fPageBreakBefore:Z

.field protected field_60_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_61_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_62_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_63_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_64_brcBetween:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_65_brcBar:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_66_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

.field protected field_67_anld:[B

.field protected field_68_phe:[B

.field protected field_69_fPropRMark:Z

.field protected field_6_brcl:B

.field protected field_70_ibstPropRMark:I

.field protected field_71_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_72_itbdMac:I

.field protected field_73_rgdxaTab:[I

.field protected field_74_rgtbd:[Lorg/apache/poi/hwpf/model/TabDescriptor;

.field protected field_75_numrm:[B

.field protected field_76_ptap:[B

.field protected field_77_fNoAllowOverlap:Z

.field protected field_78_ipgp:J

.field protected field_79_rsid:J

.field protected field_7_brcp:B

.field protected field_8_ilvl:B

.field protected field_9_ilfo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    .line 102
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    .line 103
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_11_lspd:Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    .line 147
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_11_lspd:Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    .line 148
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_28_dcs:Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_32_fWidowControl:Z

    .line 150
    const/16 v0, 0x9

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_41_lvl:B

    .line 151
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_60_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 152
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_61_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 153
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_62_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 154
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_63_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 155
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_64_brcBetween:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 156
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_65_brcBar:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 157
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_66_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 158
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_67_anld:[B

    .line 159
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_68_phe:[B

    .line 160
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_71_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 161
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_73_rgdxaTab:[I

    .line 162
    new-array v0, v1, [Lorg/apache/poi/hwpf/model/TabDescriptor;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_74_rgtbd:[Lorg/apache/poi/hwpf/model/TabDescriptor;

    .line 163
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_75_numrm:[B

    .line 164
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_76_ptap:[B

    .line 165
    return-void
.end method


# virtual methods
.method public getAnld()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1621
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_67_anld:[B

    return-object v0
.end method

.method public getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1585
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_65_brcBar:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcBetween()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1567
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_64_brcBetween:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1531
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_62_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1513
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_61_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1549
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_63_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1495
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_60_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcl()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 440
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_6_brcl:B

    return v0
.end method

.method public getBrcp()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 472
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_7_brcp:B

    return v0
.end method

.method public getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 904
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_28_dcs:Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    return-object v0
.end method

.method public getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1693
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_71_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDxaAbs()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 697
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_17_dxaAbs:I

    return v0
.end method

.method public getDxaFromText()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 940
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_30_dxaFromText:I

    return v0
.end method

.method public getDxaLeft()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1441
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_57_dxaLeft:I

    return v0
.end method

.method public getDxaLeft1()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1459
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_58_dxaLeft1:I

    return v0
.end method

.method public getDxaRight()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1423
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_56_dxaRight:I

    return v0
.end method

.method public getDxaWidth()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 733
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_19_dxaWidth:I

    return v0
.end method

.method public getDxcLeft()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1351
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_52_dxcLeft:S

    return v0
.end method

.method public getDxcLeft1()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1369
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_53_dxcLeft1:S

    return v0
.end method

.method public getDxcRight()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1333
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_51_dxcRight:S

    return v0
.end method

.method public getDyaAbs()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 715
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_18_dyaAbs:I

    return v0
.end method

.method public getDyaAfter()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 625
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_13_dyaAfter:I

    return v0
.end method

.method public getDyaBefore()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 607
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_12_dyaBefore:I

    return v0
.end method

.method public getDyaFromText()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 922
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_29_dyaFromText:I

    return v0
.end method

.method public getDyaHeight()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 859
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_26_dyaHeight:I

    return v0
.end method

.method public getFAdjustRight()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1243
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_46_fAdjustRight:Z

    return v0
.end method

.method public getFAutoSpaceDE()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1066
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_37_fAutoSpaceDE:Z

    return v0
.end method

.method public getFAutoSpaceDN()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1084
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_38_fAutoSpaceDN:Z

    return v0
.end method

.method public getFBiDi()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1171
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_42_fBiDi:Z

    return v0
.end method

.method public getFBrLnAbove()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 751
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_20_fBrLnAbove:Z

    return v0
.end method

.method public getFBrLnBelow()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 769
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_21_fBrLnBelow:Z

    return v0
.end method

.method public getFCrLf()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1207
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_44_fCrLf:Z

    return v0
.end method

.method public getFDyaAfterAuto()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1405
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_55_fDyaAfterAuto:Z

    return v0
.end method

.method public getFDyaBeforeAuto()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1387
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_54_fDyaBeforeAuto:Z

    return v0
.end method

.method public getFInTable()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 643
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_14_fInTable:Z

    return v0
.end method

.method public getFInnerTableCell()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1279
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_48_fInnerTableCell:Z

    return v0
.end method

.method public getFKeep()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 380
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_3_fKeep:Z

    return v0
.end method

.method public getFKeepFollow()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 398
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_4_fKeepFollow:Z

    return v0
.end method

.method public getFKinsoku()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 994
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_33_fKinsoku:Z

    return v0
.end method

.method public getFLocked()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 958
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_31_fLocked:Z

    return v0
.end method

.method public getFMinHeight()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 881
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_27_fMinHeight:Z

    return v0
.end method

.method public getFNoAllowOverlap()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1801
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_77_fNoAllowOverlap:Z

    return v0
.end method

.method public getFNoAutoHyph()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 841
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_25_fNoAutoHyph:Z

    return v0
.end method

.method public getFNoLnn()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 571
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_10_fNoLnn:Z

    return v0
.end method

.method public getFNumRMIns()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1189
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_43_fNumRMIns:Z

    return v0
.end method

.method public getFOpenTch()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1297
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_49_fOpenTch:Z

    return v0
.end method

.method public getFOverflowPunct()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1030
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_35_fOverflowPunct:Z

    return v0
.end method

.method public getFPageBreakBefore()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 416
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_5_fPageBreakBefore:Z

    return v0
.end method

.method public getFPropRMark()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1657
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_69_fPropRMark:Z

    return v0
.end method

.method public getFSideBySide()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 362
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_2_fSideBySide:Z

    return v0
.end method

.method public getFTopLinePunct()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1048
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_36_fTopLinePunct:Z

    return v0
.end method

.method public getFTtp()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 679
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_16_fTtp:Z

    return v0
.end method

.method public getFTtpEmbedded()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1315
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_50_fTtpEmbedded:Z

    return v0
.end method

.method public getFUsePgsuSettings()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1225
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_45_fUsePgsuSettings:Z

    return v0
.end method

.method public getFWidowControl()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 976
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_32_fWidowControl:Z

    return v0
.end method

.method public getFWordWrap()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1012
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_34_fWordWrap:Z

    return v0
.end method

.method public getFinTableW97()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 661
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_15_finTableW97:Z

    return v0
.end method

.method public getFontAlign()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1135
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1675
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_70_ibstPropRMark:I

    return v0
.end method

.method public getIlfo()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 535
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_9_ilfo:I

    return v0
.end method

.method public getIlvl()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 498
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_8_ilvl:B

    return v0
.end method

.method public getIpgp()J
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1819
    iget-wide v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_78_ipgp:J

    return-wide v0
.end method

.method public getIstd()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 344
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_1_istd:I

    return v0
.end method

.method public getItap()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1261
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_47_itap:I

    return v0
.end method

.method public getItbdMac()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1711
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_72_itbdMac:I

    return v0
.end method

.method public getJc()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1477
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_59_jc:B

    return v0
.end method

.method public getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 589
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_11_lspd:Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    return-object v0
.end method

.method public getLvl()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1153
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_41_lvl:B

    return v0
.end method

.method public getNumrm()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1765
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_75_numrm:[B

    return-object v0
.end method

.method public getPcHorz()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 805
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_23_pcHorz:B

    return v0
.end method

.method public getPcVert()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 787
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_22_pcVert:B

    return v0
.end method

.method public getPhe()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1639
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_68_phe:[B

    return-object v0
.end method

.method public getPtap()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1783
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_76_ptap:[B

    return-object v0
.end method

.method public getRgdxaTab()[I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1729
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_73_rgdxaTab:[I

    return-object v0
.end method

.method public getRgtbd()[Lorg/apache/poi/hwpf/model/TabDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1747
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_74_rgtbd:[Lorg/apache/poi/hwpf/model/TabDescriptor;

    return-object v0
.end method

.method public getRsid()J
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1837
    iget-wide v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_79_rsid:J

    return-wide v0
.end method

.method public getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1603
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_66_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    return-object v0
.end method

.method public getWAlignFont()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1109
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_39_wAlignFont:I

    return v0
.end method

.method public getWr()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 823
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_24_wr:B

    return v0
.end method

.method public isFBackward()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1886
    sget-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRotateFont()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1906
    sget-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVertical()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1866
    sget-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public setAnld([B)V
    .locals 0
    .param p1, "field_67_anld"    # [B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1630
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_67_anld:[B

    .line 1631
    return-void
.end method

.method public setBrcBar(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_65_brcBar"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1594
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_65_brcBar:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1595
    return-void
.end method

.method public setBrcBetween(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_64_brcBetween"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1576
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_64_brcBetween:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1577
    return-void
.end method

.method public setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_62_brcBottom"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1540
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_62_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1541
    return-void
.end method

.method public setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_61_brcLeft"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1522
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_61_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1523
    return-void
.end method

.method public setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_63_brcRight"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1558
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_63_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1559
    return-void
.end method

.method public setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_60_brcTop"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1504
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_60_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1505
    return-void
.end method

.method public setBrcl(B)V
    .locals 0
    .param p1, "field_6_brcl"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 456
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_6_brcl:B

    .line 457
    return-void
.end method

.method public setBrcp(B)V
    .locals 0
    .param p1, "field_7_brcp"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 489
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_7_brcp:B

    .line 490
    return-void
.end method

.method public setDcs(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V
    .locals 0
    .param p1, "field_28_dcs"    # Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 913
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_28_dcs:Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    .line 914
    return-void
.end method

.method public setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_71_dttmPropRMark"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1702
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_71_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 1703
    return-void
.end method

.method public setDxaAbs(I)V
    .locals 0
    .param p1, "field_17_dxaAbs"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 706
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_17_dxaAbs:I

    .line 707
    return-void
.end method

.method public setDxaFromText(I)V
    .locals 0
    .param p1, "field_30_dxaFromText"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 949
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_30_dxaFromText:I

    .line 950
    return-void
.end method

.method public setDxaLeft(I)V
    .locals 0
    .param p1, "field_57_dxaLeft"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1450
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_57_dxaLeft:I

    .line 1451
    return-void
.end method

.method public setDxaLeft1(I)V
    .locals 0
    .param p1, "field_58_dxaLeft1"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1468
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_58_dxaLeft1:I

    .line 1469
    return-void
.end method

.method public setDxaRight(I)V
    .locals 0
    .param p1, "field_56_dxaRight"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1432
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_56_dxaRight:I

    .line 1433
    return-void
.end method

.method public setDxaWidth(I)V
    .locals 0
    .param p1, "field_19_dxaWidth"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 742
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_19_dxaWidth:I

    .line 743
    return-void
.end method

.method public setDxcLeft(S)V
    .locals 0
    .param p1, "field_52_dxcLeft"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1360
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_52_dxcLeft:S

    .line 1361
    return-void
.end method

.method public setDxcLeft1(S)V
    .locals 0
    .param p1, "field_53_dxcLeft1"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1378
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_53_dxcLeft1:S

    .line 1379
    return-void
.end method

.method public setDxcRight(S)V
    .locals 0
    .param p1, "field_51_dxcRight"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1342
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_51_dxcRight:S

    .line 1343
    return-void
.end method

.method public setDyaAbs(I)V
    .locals 0
    .param p1, "field_18_dyaAbs"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 724
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_18_dyaAbs:I

    .line 725
    return-void
.end method

.method public setDyaAfter(I)V
    .locals 0
    .param p1, "field_13_dyaAfter"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 634
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_13_dyaAfter:I

    .line 635
    return-void
.end method

.method public setDyaBefore(I)V
    .locals 0
    .param p1, "field_12_dyaBefore"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 616
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_12_dyaBefore:I

    .line 617
    return-void
.end method

.method public setDyaFromText(I)V
    .locals 0
    .param p1, "field_29_dyaFromText"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 931
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_29_dyaFromText:I

    .line 932
    return-void
.end method

.method public setDyaHeight(I)V
    .locals 0
    .param p1, "field_26_dyaHeight"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 868
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_26_dyaHeight:I

    .line 869
    return-void
.end method

.method public setFAdjustRight(Z)V
    .locals 0
    .param p1, "field_46_fAdjustRight"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1252
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_46_fAdjustRight:Z

    .line 1253
    return-void
.end method

.method public setFAutoSpaceDE(Z)V
    .locals 0
    .param p1, "field_37_fAutoSpaceDE"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1075
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_37_fAutoSpaceDE:Z

    .line 1076
    return-void
.end method

.method public setFAutoSpaceDN(Z)V
    .locals 0
    .param p1, "field_38_fAutoSpaceDN"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1093
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_38_fAutoSpaceDN:Z

    .line 1094
    return-void
.end method

.method public setFBackward(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1876
    sget-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    .line 1877
    return-void
.end method

.method public setFBiDi(Z)V
    .locals 0
    .param p1, "field_42_fBiDi"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1180
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_42_fBiDi:Z

    .line 1181
    return-void
.end method

.method public setFBrLnAbove(Z)V
    .locals 0
    .param p1, "field_20_fBrLnAbove"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 760
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_20_fBrLnAbove:Z

    .line 761
    return-void
.end method

.method public setFBrLnBelow(Z)V
    .locals 0
    .param p1, "field_21_fBrLnBelow"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 778
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_21_fBrLnBelow:Z

    .line 779
    return-void
.end method

.method public setFCrLf(Z)V
    .locals 0
    .param p1, "field_44_fCrLf"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1216
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_44_fCrLf:Z

    .line 1217
    return-void
.end method

.method public setFDyaAfterAuto(Z)V
    .locals 0
    .param p1, "field_55_fDyaAfterAuto"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1414
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_55_fDyaAfterAuto:Z

    .line 1415
    return-void
.end method

.method public setFDyaBeforeAuto(Z)V
    .locals 0
    .param p1, "field_54_fDyaBeforeAuto"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1396
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_54_fDyaBeforeAuto:Z

    .line 1397
    return-void
.end method

.method public setFInTable(Z)V
    .locals 0
    .param p1, "field_14_fInTable"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 652
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_14_fInTable:Z

    .line 653
    return-void
.end method

.method public setFInnerTableCell(Z)V
    .locals 0
    .param p1, "field_48_fInnerTableCell"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1288
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_48_fInnerTableCell:Z

    .line 1289
    return-void
.end method

.method public setFKeep(Z)V
    .locals 0
    .param p1, "field_3_fKeep"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 389
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_3_fKeep:Z

    .line 390
    return-void
.end method

.method public setFKeepFollow(Z)V
    .locals 0
    .param p1, "field_4_fKeepFollow"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 407
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_4_fKeepFollow:Z

    .line 408
    return-void
.end method

.method public setFKinsoku(Z)V
    .locals 0
    .param p1, "field_33_fKinsoku"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1003
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_33_fKinsoku:Z

    .line 1004
    return-void
.end method

.method public setFLocked(Z)V
    .locals 0
    .param p1, "field_31_fLocked"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 967
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_31_fLocked:Z

    .line 968
    return-void
.end method

.method public setFMinHeight(Z)V
    .locals 0
    .param p1, "field_27_fMinHeight"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 895
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_27_fMinHeight:Z

    .line 896
    return-void
.end method

.method public setFNoAllowOverlap(Z)V
    .locals 0
    .param p1, "field_77_fNoAllowOverlap"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1810
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_77_fNoAllowOverlap:Z

    .line 1811
    return-void
.end method

.method public setFNoAutoHyph(Z)V
    .locals 0
    .param p1, "field_25_fNoAutoHyph"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 850
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_25_fNoAutoHyph:Z

    .line 851
    return-void
.end method

.method public setFNoLnn(Z)V
    .locals 0
    .param p1, "field_10_fNoLnn"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 580
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_10_fNoLnn:Z

    .line 581
    return-void
.end method

.method public setFNumRMIns(Z)V
    .locals 0
    .param p1, "field_43_fNumRMIns"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1198
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_43_fNumRMIns:Z

    .line 1199
    return-void
.end method

.method public setFOpenTch(Z)V
    .locals 0
    .param p1, "field_49_fOpenTch"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1306
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_49_fOpenTch:Z

    .line 1307
    return-void
.end method

.method public setFOverflowPunct(Z)V
    .locals 0
    .param p1, "field_35_fOverflowPunct"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1039
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_35_fOverflowPunct:Z

    .line 1040
    return-void
.end method

.method public setFPageBreakBefore(Z)V
    .locals 0
    .param p1, "field_5_fPageBreakBefore"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 425
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_5_fPageBreakBefore:Z

    .line 426
    return-void
.end method

.method public setFPropRMark(Z)V
    .locals 0
    .param p1, "field_69_fPropRMark"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1666
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_69_fPropRMark:Z

    .line 1667
    return-void
.end method

.method public setFRotateFont(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1896
    sget-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    .line 1897
    return-void
.end method

.method public setFSideBySide(Z)V
    .locals 0
    .param p1, "field_2_fSideBySide"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 371
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_2_fSideBySide:Z

    .line 372
    return-void
.end method

.method public setFTopLinePunct(Z)V
    .locals 0
    .param p1, "field_36_fTopLinePunct"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1057
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_36_fTopLinePunct:Z

    .line 1058
    return-void
.end method

.method public setFTtp(Z)V
    .locals 0
    .param p1, "field_16_fTtp"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 688
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_16_fTtp:Z

    .line 689
    return-void
.end method

.method public setFTtpEmbedded(Z)V
    .locals 0
    .param p1, "field_50_fTtpEmbedded"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1324
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_50_fTtpEmbedded:Z

    .line 1325
    return-void
.end method

.method public setFUsePgsuSettings(Z)V
    .locals 0
    .param p1, "field_45_fUsePgsuSettings"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1234
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_45_fUsePgsuSettings:Z

    .line 1235
    return-void
.end method

.method public setFVertical(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1856
    sget-object v0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    .line 1857
    return-void
.end method

.method public setFWidowControl(Z)V
    .locals 0
    .param p1, "field_32_fWidowControl"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 985
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_32_fWidowControl:Z

    .line 986
    return-void
.end method

.method public setFWordWrap(Z)V
    .locals 0
    .param p1, "field_34_fWordWrap"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1021
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_34_fWordWrap:Z

    .line 1022
    return-void
.end method

.method public setFinTableW97(Z)V
    .locals 0
    .param p1, "field_15_finTableW97"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 670
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_15_finTableW97:Z

    .line 671
    return-void
.end method

.method public setFontAlign(S)V
    .locals 0
    .param p1, "field_40_fontAlign"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1144
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_40_fontAlign:S

    .line 1145
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_70_ibstPropRMark"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1684
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_70_ibstPropRMark:I

    .line 1685
    return-void
.end method

.method public setIlfo(I)V
    .locals 0
    .param p1, "field_9_ilfo"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 562
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_9_ilfo:I

    .line 563
    return-void
.end method

.method public setIlvl(B)V
    .locals 0
    .param p1, "field_8_ilvl"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 507
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_8_ilvl:B

    .line 508
    return-void
.end method

.method public setIpgp(J)V
    .locals 1
    .param p1, "field_78_ipgp"    # J
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1828
    iput-wide p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_78_ipgp:J

    .line 1829
    return-void
.end method

.method public setIstd(I)V
    .locals 0
    .param p1, "field_1_istd"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 353
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_1_istd:I

    .line 354
    return-void
.end method

.method public setItap(I)V
    .locals 0
    .param p1, "field_47_itap"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1270
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_47_itap:I

    .line 1271
    return-void
.end method

.method public setItbdMac(I)V
    .locals 0
    .param p1, "field_72_itbdMac"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1720
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_72_itbdMac:I

    .line 1721
    return-void
.end method

.method public setJc(B)V
    .locals 0
    .param p1, "field_59_jc"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1486
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_59_jc:B

    .line 1487
    return-void
.end method

.method public setLspd(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V
    .locals 0
    .param p1, "field_11_lspd"    # Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 598
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_11_lspd:Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    .line 599
    return-void
.end method

.method public setLvl(B)V
    .locals 0
    .param p1, "field_41_lvl"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1162
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_41_lvl:B

    .line 1163
    return-void
.end method

.method public setNumrm([B)V
    .locals 0
    .param p1, "field_75_numrm"    # [B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1774
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_75_numrm:[B

    .line 1775
    return-void
.end method

.method public setPcHorz(B)V
    .locals 0
    .param p1, "field_23_pcHorz"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 814
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_23_pcHorz:B

    .line 815
    return-void
.end method

.method public setPcVert(B)V
    .locals 0
    .param p1, "field_22_pcVert"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 796
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_22_pcVert:B

    .line 797
    return-void
.end method

.method public setPhe([B)V
    .locals 0
    .param p1, "field_68_phe"    # [B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1648
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_68_phe:[B

    .line 1649
    return-void
.end method

.method public setPtap([B)V
    .locals 0
    .param p1, "field_76_ptap"    # [B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1792
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_76_ptap:[B

    .line 1793
    return-void
.end method

.method public setRgdxaTab([I)V
    .locals 0
    .param p1, "field_73_rgdxaTab"    # [I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1738
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_73_rgdxaTab:[I

    .line 1739
    return-void
.end method

.method public setRgtbd([Lorg/apache/poi/hwpf/model/TabDescriptor;)V
    .locals 0
    .param p1, "field_74_rgtbd"    # [Lorg/apache/poi/hwpf/model/TabDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1756
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_74_rgtbd:[Lorg/apache/poi/hwpf/model/TabDescriptor;

    .line 1757
    return-void
.end method

.method public setRsid(J)V
    .locals 1
    .param p1, "field_79_rsid"    # J
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1846
    iput-wide p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_79_rsid:J

    .line 1847
    return-void
.end method

.method public setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 0
    .param p1, "field_66_shd"    # Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1612
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_66_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 1613
    return-void
.end method

.method public setWAlignFont(I)V
    .locals 0
    .param p1, "field_39_wAlignFont"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1126
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_39_wAlignFont:I

    .line 1127
    return-void
.end method

.method public setWr(B)V
    .locals 0
    .param p1, "field_24_wr"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 832
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->field_24_wr:B

    .line 833
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[PAP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v1, "    .istd                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v1, "    .fSideBySide          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFSideBySide()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v1, "    .fKeep                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFKeep()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const-string/jumbo v1, "    .fKeepFollow          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFKeepFollow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v1, "    .fPageBreakBefore     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFPageBreakBefore()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v1, "    .brcl                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcl()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v1, "    .brcp                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcp()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v1, "    .ilvl                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getIlvl()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v1, "    .ilfo                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getIlfo()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v1, "    .fNoLnn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFNoLnn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const-string/jumbo v1, "    .lspd                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v1, "    .dyaBefore            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v1, "    .dyaAfter             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaAfter()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v1, "    .fInTable             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFInTable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string/jumbo v1, "    .finTableW97          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFinTableW97()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v1, "    .fTtp                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFTtp()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v1, "    .dxaAbs               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaAbs()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v1, "    .dyaAbs               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaAbs()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    const-string/jumbo v1, "    .dxaWidth             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string/jumbo v1, "    .fBrLnAbove           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFBrLnAbove()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string/jumbo v1, "    .fBrLnBelow           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFBrLnBelow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string/jumbo v1, "    .pcVert               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getPcVert()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string/jumbo v1, "    .pcHorz               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getPcHorz()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    const-string/jumbo v1, "    .wr                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getWr()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string/jumbo v1, "    .fNoAutoHyph          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFNoAutoHyph()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    const-string/jumbo v1, "    .dyaHeight            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string/jumbo v1, "    .fMinHeight           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFMinHeight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    const-string/jumbo v1, "    .dcs                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    const-string/jumbo v1, "    .dyaFromText          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaFromText()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string/jumbo v1, "    .dxaFromText          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaFromText()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string/jumbo v1, "    .fLocked              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFLocked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    const-string/jumbo v1, "    .fWidowControl        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFWidowControl()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    const-string/jumbo v1, "    .fKinsoku             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFKinsoku()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    const-string/jumbo v1, "    .fWordWrap            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFWordWrap()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    const-string/jumbo v1, "    .fOverflowPunct       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFOverflowPunct()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const-string/jumbo v1, "    .fTopLinePunct        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFTopLinePunct()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string/jumbo v1, "    .fAutoSpaceDE         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDE()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string/jumbo v1, "    .fAutoSpaceDN         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDN()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string/jumbo v1, "    .wAlignFont           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getWAlignFont()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string/jumbo v1, "    .fontAlign            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFontAlign()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string/jumbo v1, "         .fVertical                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 253
    const-string/jumbo v1, "         .fBackward                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 254
    const-string/jumbo v1, "         .fRotateFont              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->isFRotateFont()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 255
    const-string/jumbo v1, "    .lvl                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getLvl()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string/jumbo v1, "    .fBiDi                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFBiDi()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string/jumbo v1, "    .fNumRMIns            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFNumRMIns()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string/jumbo v1, "    .fCrLf                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFCrLf()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    const-string/jumbo v1, "    .fUsePgsuSettings     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFUsePgsuSettings()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string/jumbo v1, "    .fAdjustRight         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFAdjustRight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string/jumbo v1, "    .itap                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getItap()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string/jumbo v1, "    .fInnerTableCell      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFInnerTableCell()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string/jumbo v1, "    .fOpenTch             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFOpenTch()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    const-string/jumbo v1, "    .fTtpEmbedded         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFTtpEmbedded()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string/jumbo v1, "    .dxcRight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxcRight()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const-string/jumbo v1, "    .dxcLeft              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxcLeft()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string/jumbo v1, "    .dxcLeft1             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxcLeft1()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    const-string/jumbo v1, "    .fDyaBeforeAuto       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFDyaBeforeAuto()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    const-string/jumbo v1, "    .fDyaAfterAuto        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFDyaAfterAuto()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string/jumbo v1, "    .dxaRight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaRight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string/jumbo v1, "    .dxaLeft              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    const-string/jumbo v1, "    .dxaLeft1             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaLeft1()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const-string/jumbo v1, "    .jc                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getJc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    const-string/jumbo v1, "    .brcTop               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string/jumbo v1, "    .brcLeft              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const-string/jumbo v1, "    .brcBottom            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string/jumbo v1, "    .brcRight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    const-string/jumbo v1, "    .brcBetween           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcBetween()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string/jumbo v1, "    .brcBar               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    const-string/jumbo v1, "    .shd                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const-string/jumbo v1, "    .anld                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getAnld()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const-string/jumbo v1, "    .phe                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getPhe()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string/jumbo v1, "    .fPropRMark           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFPropRMark()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string/jumbo v1, "    .ibstPropRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getIbstPropRMark()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string/jumbo v1, "    .dttmPropRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string/jumbo v1, "    .itbdMac              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getItbdMac()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string/jumbo v1, "    .rgdxaTab             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getRgdxaTab()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const-string/jumbo v1, "    .rgtbd                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getRgtbd()[Lorg/apache/poi/hwpf/model/TabDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    const-string/jumbo v1, "    .numrm                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getNumrm()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    const-string/jumbo v1, "    .ptap                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getPtap()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    const-string/jumbo v1, "    .fNoAllowOverlap      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFNoAllowOverlap()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string/jumbo v1, "    .ipgp                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getIpgp()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    const-string/jumbo v1, "    .rsid                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getRsid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    const-string/jumbo v1, "[/PAP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
