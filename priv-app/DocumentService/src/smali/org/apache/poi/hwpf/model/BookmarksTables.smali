.class public Lorg/apache/poi/hwpf/model/BookmarksTables;
.super Ljava/lang/Object;
.source "BookmarksTables.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

.field private descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

.field private names:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/poi/hwpf/model/BookmarksTables;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 32
    sput-object v0, Lorg/apache/poi/hwpf/model/BookmarksTables;->logger:Lorg/apache/poi/util/POILogger;

    .line 33
    return-void
.end method

.method public constructor <init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V
    .locals 3
    .param p1, "tableStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 37
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-direct {v0, v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    .line 43
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hwpf/model/BookmarksTables;->read([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V

    .line 44
    return-void
.end method

.method private read([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V
    .locals 8
    .param p1, "tableStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .prologue
    .line 117
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcSttbfbkmk()I

    move-result v5

    .line 118
    .local v5, "namesStart":I
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbSttbfbkmk()I

    move-result v4

    .line 120
    .local v4, "namesLength":I
    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    .line 121
    new-instance v6, Ljava/util/ArrayList;

    .line 122
    invoke-static {p1, v5}, Lorg/apache/poi/hwpf/model/SttbUtils;->readSttbfBkmk([BI)[Ljava/lang/String;

    move-result-object v7

    .line 121
    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    .line 124
    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcfbkf()I

    move-result v1

    .line 125
    .local v1, "firstDescriptorsStart":I
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcfbkf()I

    move-result v0

    .line 126
    .local v0, "firstDescriptorsLength":I
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 127
    new-instance v6, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 129
    invoke-static {}, Lorg/apache/poi/hwpf/model/BookmarkFirstDescriptor;->getSize()I

    move-result v7

    invoke-direct {v6, p1, v1, v0, v7}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 127
    iput-object v6, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 131
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcfbkl()I

    move-result v3

    .line 132
    .local v3, "limDescriptorsStart":I
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcfbkl()I

    move-result v2

    .line 133
    .local v2, "limDescriptorsLength":I
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 134
    new-instance v6, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 135
    const/4 v7, 0x0

    invoke-direct {v6, p1, v3, v2, v7}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 134
    iput-object v6, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 136
    :cond_2
    return-void
.end method


# virtual methods
.method public afterDelete(II)V
    .locals 8
    .param p1, "startCp"    # I
    .param p2, "length"    # I

    .prologue
    .line 48
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    neg-int v4, p2

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hwpf/model/PlexOfCps;->adjust(II)V

    .line 49
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    neg-int v4, p2

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hwpf/model/PlexOfCps;->adjust(II)V

    .line 50
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 63
    return-void

    .line 52
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v3, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 53
    .local v2, "startNode":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v3, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    .line 54
    .local v0, "endNode":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v3

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 56
    sget-object v3, Lorg/apache/poi/hwpf/model/BookmarksTables;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v4, 0x1

    const-string/jumbo v5, "Removing bookmark #"

    .line 57
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string/jumbo v7, "..."

    .line 56
    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/model/BookmarksTables;->remove(I)V

    .line 59
    add-int/lit8 v1, v1, -0x1

    .line 50
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public afterInsert(II)V
    .locals 2
    .param p1, "startCp"    # I
    .param p2, "length"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->adjust(II)V

    .line 68
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->adjust(II)V

    .line 69
    return-void
.end method

.method public getBookmarksCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v0

    return v0
.end method

.method public getDescriptorFirst(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorFirstIndex(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)I
    .locals 1
    .param p1, "descriptorFirst"    # Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toPropertiesArray()[Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getDescriptorLim(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorsFirstCount()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v0

    return v0
.end method

.method public getDescriptorsLimCount()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v0

    return v0
.end method

.method public getName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNamesCount()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public remove(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->remove(I)V

    .line 141
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->remove(I)V

    .line 142
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 143
    return-void
.end method

.method public setName(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-void
.end method

.method public writePlcfBkmkf(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 4
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 153
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 155
    :cond_0
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcPlcfbkf(I)V

    .line 156
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbPlcfbkf(I)V

    .line 166
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 161
    .local v1, "start":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsFirst:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 162
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    .line 164
    .local v0, "end":I
    invoke-virtual {p1, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcPlcfbkf(I)V

    .line 165
    sub-int v2, v0, v1

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbPlcfbkf(I)V

    goto :goto_0
.end method

.method public writePlcfBkmkl(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 4
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 171
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 173
    :cond_0
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcPlcfbkl(I)V

    .line 174
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbPlcfbkl(I)V

    .line 184
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 179
    .local v1, "start":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->descriptorsLim:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 180
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    .line 182
    .local v0, "end":I
    invoke-virtual {p1, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcPlcfbkl(I)V

    .line 183
    sub-int v2, v0, v1

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbPlcfbkl(I)V

    goto :goto_0
.end method

.method public writeSttbfBkmk(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 4
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 189
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 191
    :cond_0
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcSttbfbkmk(I)V

    .line 192
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbSttbfbkmk(I)V

    .line 203
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 197
    .local v1, "start":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/BookmarksTables;->names:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-static {v2, p2}, Lorg/apache/poi/hwpf/model/SttbUtils;->writeSttbfBkmk([Ljava/lang/String;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 199
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    .line 201
    .local v0, "end":I
    invoke-virtual {p1, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcSttbfbkmk(I)V

    .line 202
    sub-int v2, v0, v1

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbSttbfbkmk(I)V

    goto :goto_0
.end method
