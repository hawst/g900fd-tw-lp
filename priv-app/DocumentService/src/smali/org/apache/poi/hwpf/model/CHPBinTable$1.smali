.class Lorg/apache/poi/hwpf/model/CHPBinTable$1;
.super Ljava/lang/Object;
.source "CHPBinTable.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/hwpf/model/CHPBinTable;->rebuild(Lorg/apache/poi/hwpf/model/ComplexFileTable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/poi/hwpf/model/CHPX;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/poi/hwpf/model/CHPBinTable;

.field private final synthetic val$chpxToFileOrder:Ljava/util/Map;


# direct methods
.method constructor <init>(Lorg/apache/poi/hwpf/model/CHPBinTable;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/CHPBinTable$1;->this$0:Lorg/apache/poi/hwpf/model/CHPBinTable;

    iput-object p2, p0, Lorg/apache/poi/hwpf/model/CHPBinTable$1;->val$chpxToFileOrder:Ljava/util/Map;

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/model/CHPX;

    check-cast p2, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/CHPBinTable$1;->compare(Lorg/apache/poi/hwpf/model/CHPX;Lorg/apache/poi/hwpf/model/CHPX;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/poi/hwpf/model/CHPX;Lorg/apache/poi/hwpf/model/CHPX;)I
    .locals 3
    .param p1, "o1"    # Lorg/apache/poi/hwpf/model/CHPX;
    .param p2, "o2"    # Lorg/apache/poi/hwpf/model/CHPX;

    .prologue
    .line 203
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/CHPBinTable$1;->val$chpxToFileOrder:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 204
    .local v0, "i1":Ljava/lang/Integer;
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/CHPBinTable$1;->val$chpxToFileOrder:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 205
    .local v1, "i2":Ljava/lang/Integer;
    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v2

    return v2
.end method
