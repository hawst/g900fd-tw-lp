.class public final Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;
.super Ljava/lang/Object;
.source "RevisionMarkAuthorTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private entries:[Ljava/lang/String;


# direct methods
.method public constructor <init>([BII)V
    .locals 1
    .param p1, "tableStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p1, p2}, Lorg/apache/poi/hwpf/model/SttbUtils;->readSttbfRMark([BI)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getAuthor(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 70
    .local v0, "auth":Ljava/lang/String;
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    array-length v1, v1

    if-ge p1, v1, :cond_0

    .line 71
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 73
    :cond_0
    return-object v0
.end method

.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 1
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/poi/hwpf/model/SttbUtils;->writeSttbfRMark([Ljava/lang/String;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 94
    return-void
.end method
