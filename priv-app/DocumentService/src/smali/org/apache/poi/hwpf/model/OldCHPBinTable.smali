.class public final Lorg/apache/poi/hwpf/model/OldCHPBinTable;
.super Lorg/apache/poi/hwpf/model/CHPBinTable;
.source "OldCHPBinTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>([BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 10
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/CHPBinTable;-><init>()V

    .line 49
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v8, 0x2

    invoke-direct {v0, p1, p2, p3, v8}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 51
    .local v0, "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v3

    .line 52
    .local v3, "length":I
    const/4 v7, 0x0

    .local v7, "x":I
    :goto_0
    if-lt v7, v3, :cond_0

    .line 68
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/OldCHPBinTable;->_textRuns:Ljava/util/ArrayList;

    sget-object v9, Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;

    invoke-static {v8, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 69
    return-void

    .line 54
    :cond_0
    invoke-virtual {v0, v7}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v4

    .line 56
    .local v4, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v8

    invoke-static {v8}, Lorg/apache/poi/util/LittleEndian;->getUShort([B)I

    move-result v5

    .line 57
    .local v5, "pageNum":I
    mul-int/lit16 v6, v5, 0x200

    .line 59
    .local v6, "pageOffset":I
    new-instance v1, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;

    invoke-direct {v1, p1, v6, p5}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;-><init>([BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 62
    .local v1, "cfkp":Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->getCHPXs()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 52
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 62
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/CHPX;

    .line 64
    .local v2, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    if-eqz v2, :cond_1

    .line 65
    iget-object v9, p0, Lorg/apache/poi/hwpf/model/OldCHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
