.class public final Lorg/apache/poi/hwpf/usermodel/DateAndTime;
.super Ljava/lang/Object;
.source "DateAndTime.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x4

.field private static final _dom:Lorg/apache/poi/util/BitField;

.field private static final _hours:Lorg/apache/poi/util/BitField;

.field private static final _minutes:Lorg/apache/poi/util/BitField;

.field private static final _months:Lorg/apache/poi/util/BitField;

.field private static final _years:Lorg/apache/poi/util/BitField;


# instance fields
.field private _info:S

.field private _info2:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x3f

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_minutes:Lorg/apache/poi/util/BitField;

    .line 37
    const/16 v0, 0x7c0

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_hours:Lorg/apache/poi/util/BitField;

    .line 38
    const v0, 0xf800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_dom:Lorg/apache/poi/util/BitField;

    .line 40
    const/16 v0, 0xf

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_months:Lorg/apache/poi/util/BitField;

    .line 41
    const/16 v0, 0x1ff0

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_years:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    .line 53
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    .line 54
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 80
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 81
    check-cast v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 82
    .local v0, "dttm":Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 84
    .end local v0    # "dttm":Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    :cond_0
    return v1
.end method

.method public getDate()Ljava/util/Calendar;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 58
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 60
    .local v0, "cal":Ljava/util/Calendar;
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_years:Lorg/apache/poi/util/BitField;

    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    invoke-virtual {v1, v2}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v1

    add-int/lit16 v1, v1, 0x76c

    .line 61
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_months:Lorg/apache/poi/util/BitField;

    iget-short v3, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    invoke-virtual {v2, v3}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 62
    sget-object v3, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_dom:Lorg/apache/poi/util/BitField;

    iget-short v4, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-virtual {v3, v4}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v3

    .line 63
    sget-object v4, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_hours:Lorg/apache/poi/util/BitField;

    iget-short v5, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-virtual {v4, v5}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v4

    .line 64
    sget-object v5, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_minutes:Lorg/apache/poi/util/BitField;

    iget-short v7, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-virtual {v5, v7}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v5

    .line 59
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 67
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 68
    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 95
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    if-nez v0, :cond_0

    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 73
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-static {p1, p2, v0}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 74
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->_info2:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 75
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string/jumbo v0, "[DTTM] EMPTY"

    .line 104
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[DTTM] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->getDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
