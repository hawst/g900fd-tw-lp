.class public final Lorg/apache/poi/hwpf/model/FIBFieldHandler;
.super Ljava/lang/Object;
.source "FIBFieldHandler.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final AUTOSAVESOURCE:I = 0x23

.field public static final BKDEDN:I = 0x44

.field public static final BKDFTN:I = 0x42

.field public static final BKDMOTHER:I = 0x40

.field public static final CLX:I = 0x21

.field public static final CMDS:I = 0x18

.field public static final DGGINFO:I = 0x32

.field public static final DOCUNDO:I = 0x4d

.field public static final DOP:I = 0x1f

.field private static final FIELD_SIZE:I = 0x8

.field public static final FORMFLDSTTBS:I = 0x2d

.field public static final GRPXSTATNOWNERS:I = 0x24

.field public static final MODIFIED:I = 0x57

.field public static final PGDEDN:I = 0x43

.field public static final PGDFTN:I = 0x41

.field public static final PGDMOTHER:I = 0x3f

.field public static final PLCASUMY:I = 0x59

.field public static final PLCDOAHDR:I = 0x27

.field public static final PLCFANDREF:I = 0x4

.field public static final PLCFANDTXT:I = 0x5

.field public static final PLCFATNBKF:I = 0x2a

.field public static final PLCFATNBKL:I = 0x2b

.field public static final PLCFBKF:I = 0x16

.field public static final PLCFBKL:I = 0x17

.field public static final PLCFBTECHPX:I = 0xc

.field public static final PLCFBTELVC:I = 0x56

.field public static final PLCFBTEPAPX:I = 0xd

.field public static final PLCFDOAMOM:I = 0x26

.field public static final PLCFENDREF:I = 0x2e

.field public static final PLCFENDTXT:I = 0x2f

.field public static final PLCFFLDATN:I = 0x13

.field public static final PLCFFLDEDN:I = 0x30

.field public static final PLCFFLDFTN:I = 0x12

.field public static final PLCFFLDHDR:I = 0x11

.field public static final PLCFFLDHDRTXBX:I = 0x3b

.field public static final PLCFFLDMCR:I = 0x14

.field public static final PLCFFLDMOM:I = 0x10

.field public static final PLCFFLDTXBX:I = 0x39

.field public static final PLCFFNDREF:I = 0x2

.field public static final PLCFFNDTXT:I = 0x3

.field public static final PLCFGLSY:I = 0xa

.field public static final PLCFGRAM:I = 0x5a

.field public static final PLCFHDD:I = 0xb

.field public static final PLCFHDRTXBXTXT:I = 0x3a

.field public static final PLCFLST:I = 0x49
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFLVC:I = 0x58

.field public static final PLCFPAD:I = 0x7

.field public static final PLCFPGDEDN:I = 0x31

.field public static final PLCFPGDFTN:I = 0x22

.field public static final PLCFPHE:I = 0x8

.field public static final PLCFSEA:I = 0xe

.field public static final PLCFSED:I = 0x6

.field public static final PLCFSPL:I = 0x37

.field public static final PLCFTXBXBKD:I = 0x4b

.field public static final PLCFTXBXHDRBKD:I = 0x4c

.field public static final PLCFTXBXTXT:I = 0x38

.field public static final PLCFWKB:I = 0x36

.field public static final PLCMCR:I = 0x19

.field public static final PLCOCX:I = 0x55

.field public static final PLCSPAHDR:I = 0x29

.field public static final PLCSPAMOM:I = 0x28

.field public static final PLCUPCRGBUSE:I = 0x51

.field public static final PLCUPCUSP:I = 0x52

.field public static final PLFLFO:I = 0x4a

.field public static final PLFLST:I = 0x49

.field public static final PLGOSL:I = 0x54

.field public static final PMS:I = 0x2c

.field public static final PRDRVR:I = 0x1b

.field public static final PRENVLAND:I = 0x1d

.field public static final PRENVPORT:I = 0x1c

.field public static final RGBUSE:I = 0x4e

.field public static final ROUTESLIP:I = 0x46

.field public static final STSHF:I = 0x1

.field public static final STSHFORIG:I = 0x0

.field public static final STTBAUTOCAPTION:I = 0x35

.field public static final STTBCAPTION:I = 0x34

.field public static final STTBFASSOC:I = 0x20

.field public static final STTBFATNBKMK:I = 0x25

.field public static final STTBFBKMK:I = 0x15

.field public static final STTBFFFN:I = 0xf

.field public static final STTBFINTFLD:I = 0x45

.field public static final STTBFMCR:I = 0x1a

.field public static final STTBFNM:I = 0x48

.field public static final STTBFRMARK:I = 0x33

.field public static final STTBFUSSR:I = 0x5c

.field public static final STTBGLSY:I = 0x9

.field public static final STTBGLSYSTYLE:I = 0x53

.field public static final STTBLISTNAMES:I = 0x5b

.field public static final STTBSAVEDBY:I = 0x47

.field public static final STTBTTMBD:I = 0x3d

.field public static final STWUSER:I = 0x3c

.field public static final UNUSED:I = 0x3e

.field public static final USKF:I = 0x50

.field public static final USP:I = 0x4f

.field public static final WSS:I = 0x1e

.field private static log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _fields:[I

.field private _unknownMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hwpf/model/UnhandledDataStructure;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    const-class v0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->log:Lorg/apache/poi/util/POILogger;

    .line 149
    return-void
.end method

.method constructor <init>([BII[BLjava/util/HashSet;Z)V
    .locals 9
    .param p1, "mainStream"    # [B
    .param p2, "offset"    # I
    .param p3, "cbRgFcLcb"    # I
    .param p4, "tableStream"    # [B
    .param p6, "areKnown"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII[B",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p5, "offsetList":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    .line 158
    mul-int/lit8 v5, p3, 0x2

    new-array v5, v5, [I

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 160
    const/4 v4, 0x0

    .local v4, "x":I
    :goto_0
    if-lt v4, p3, :cond_0

    .line 188
    return-void

    .line 162
    :cond_0
    mul-int/lit8 v5, v4, 0x8

    add-int v2, v5, p2

    .line 163
    .local v2, "fieldOffset":I
    invoke-static {p1, v2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 164
    .local v0, "dsOffset":I
    add-int/lit8 v2, v2, 0x4

    .line 165
    invoke-static {p1, v2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 167
    .local v1, "dsSize":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p5, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    xor-int/2addr v5, p6

    if-eqz v5, :cond_1

    .line 169
    if-lez v1, :cond_1

    .line 171
    add-int v5, v0, v1

    array-length v6, p4

    if-le v5, v6, :cond_2

    .line 173
    sget-object v5, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->log:Lorg/apache/poi/util/POILogger;

    const/4 v6, 0x5

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Unhandled data structure points to outside the buffer. offset = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 174
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", length = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 175
    const-string/jumbo v8, ", buffer length = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, p4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 173
    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 185
    :cond_1
    :goto_1
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v6, v4, 0x2

    aput v0, v5, v6

    .line 186
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v6, v4, 0x2

    add-int/lit8 v6, v6, 0x1

    aput v1, v5, v6

    .line 160
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 179
    :cond_2
    new-instance v3, Lorg/apache/poi/hwpf/model/UnhandledDataStructure;

    invoke-direct {v3, p4, v0, v1}, Lorg/apache/poi/hwpf/model/UnhandledDataStructure;-><init>([BII)V

    .line 181
    .local v3, "unhandled":Lorg/apache/poi/hwpf/model/UnhandledDataStructure;
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private static leftPad(Ljava/lang/String;IC)Ljava/lang/String;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "value"    # I
    .param p2, "padChar"    # C

    .prologue
    .line 255
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, p1, :cond_0

    .line 264
    .end local p0    # "text":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 258
    .restart local p0    # "text":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 259
    .local v1, "result":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, p1, v2

    if-lt v0, v2, :cond_1

    .line 263
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 261
    :cond_1
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public clearFields()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 193
    return-void
.end method

.method public getFieldOffset(I)I
    .locals 2
    .param p1, "field"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public getFieldSize(I)I
    .locals 2
    .param p1, "field"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getFieldsCount()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public setFieldOffset(II)V
    .locals 2
    .param p1, "field"    # I
    .param p2, "offset"    # I

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    aput p2, v0, v1

    .line 208
    return-void
.end method

.method public setFieldSize(II)V
    .locals 2
    .param p1, "field"    # I
    .param p2, "size"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aput p2, v0, v1

    .line 213
    return-void
.end method

.method public sizeInBytes()I
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/16 v7, 0x8

    const/16 v6, 0x20

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 271
    .local v0, "result":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "[FIBFieldHandler]:\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    const-string/jumbo v3, "\tFields:\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string/jumbo v3, "\t"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string/jumbo v3, "Index"

    invoke-static {v3, v7, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string/jumbo v3, "FIB offset"

    const/16 v4, 0xf

    invoke-static {v3, v4, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const-string/jumbo v3, "Offset"

    invoke-static {v3, v7, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    const-string/jumbo v3, "Size"

    invoke-static {v3, v7, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 280
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_0

    .line 305
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 282
    :cond_0
    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 283
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    mul-int/lit8 v3, v2, 0x4

    mul-int/lit8 v3, v3, 0x2

    add-int/lit16 v3, v3, 0x9a

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    .line 284
    invoke-static {v3, v4, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string/jumbo v3, "   0x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    mul-int/lit8 v3, v2, 0x4

    mul-int/lit8 v3, v3, 0x2

    add-int/lit16 v3, v3, 0x9a

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    .line 290
    const/4 v4, 0x4

    const/16 v5, 0x30

    .line 288
    invoke-static {v3, v4, v5}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7, v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->leftPad(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    .line 297
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 296
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/UnhandledDataStructure;

    .line 298
    .local v1, "structure":Lorg/apache/poi/hwpf/model/UnhandledDataStructure;
    if-eqz v1, :cond_1

    .line 300
    const-string/jumbo v3, " => Unknown structure of size "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    iget-object v3, v1, Lorg/apache/poi/hwpf/model/UnhandledDataStructure;->_buf:[B

    array-length v3, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 303
    :cond_1
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 280
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method writeTo([BILorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 6
    .param p1, "mainStream"    # [B
    .param p2, "offset"    # I
    .param p3, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_0

    .line 251
    return-void

    .line 229
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/UnhandledDataStructure;

    .line 230
    .local v1, "ds":Lorg/apache/poi/hwpf/model/UnhandledDataStructure;
    if-eqz v1, :cond_1

    .line 232
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v4, v2, 0x2

    invoke-virtual {p3}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v5

    aput v5, v3, v4

    .line 233
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v3

    invoke-static {p1, p2, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 234
    add-int/lit8 p2, p2, 0x4

    .line 236
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/UnhandledDataStructure;->getBuf()[B

    move-result-object v0

    .line 237
    .local v0, "buf":[B
    invoke-virtual {p3, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 239
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v4, v2, 0x2

    add-int/lit8 v4, v4, 0x1

    array-length v5, v0

    aput v5, v3, v4

    .line 240
    array-length v3, v0

    invoke-static {p1, p2, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 241
    add-int/lit8 p2, p2, 0x4

    .line 227
    .end local v0    # "buf":[B
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 245
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v4, v2, 0x2

    aget v3, v3, v4

    invoke-static {p1, p2, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 246
    add-int/lit8 p2, p2, 0x4

    .line 247
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v4, v2, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    invoke-static {p1, p2, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 248
    add-int/lit8 p2, p2, 0x4

    goto :goto_1
.end method
