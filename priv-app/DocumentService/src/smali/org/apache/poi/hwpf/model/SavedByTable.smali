.class public final Lorg/apache/poi/hwpf/model/SavedByTable;
.super Ljava/lang/Object;
.source "SavedByTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private entries:[Lorg/apache/poi/hwpf/model/SavedByEntry;


# direct methods
.method public constructor <init>([BII)V
    .locals 7
    .param p1, "tableStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1, p2}, Lorg/apache/poi/hwpf/model/SttbUtils;->readSttbSavedBy([BI)[Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "strings":[Ljava/lang/String;
    array-length v3, v2

    div-int/lit8 v1, v3, 0x2

    .line 55
    .local v1, "numEntries":I
    new-array v3, v1, [Lorg/apache/poi/hwpf/model/SavedByEntry;

    iput-object v3, p0, Lorg/apache/poi/hwpf/model/SavedByTable;->entries:[Lorg/apache/poi/hwpf/model/SavedByEntry;

    .line 56
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 60
    return-void

    .line 58
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/SavedByTable;->entries:[Lorg/apache/poi/hwpf/model/SavedByEntry;

    new-instance v4, Lorg/apache/poi/hwpf/model/SavedByEntry;

    mul-int/lit8 v5, v0, 0x2

    aget-object v5, v2, v5

    mul-int/lit8 v6, v0, 0x2

    add-int/lit8 v6, v6, 0x1

    aget-object v6, v2, v6

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/hwpf/model/SavedByEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v0

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/SavedByEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/SavedByTable;->entries:[Lorg/apache/poi/hwpf/model/SavedByEntry;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 8
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/SavedByTable;->entries:[Lorg/apache/poi/hwpf/model/SavedByEntry;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    new-array v3, v4, [Ljava/lang/String;

    .line 83
    .local v3, "toSave":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 84
    .local v0, "counter":I
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/SavedByTable;->entries:[Lorg/apache/poi/hwpf/model/SavedByEntry;

    array-length v6, v5

    const/4 v4, 0x0

    move v1, v0

    .end local v0    # "counter":I
    .local v1, "counter":I
    :goto_0
    if-lt v4, v6, :cond_0

    .line 89
    invoke-static {v3, p1}, Lorg/apache/poi/hwpf/model/SttbUtils;->writeSttbSavedBy([Ljava/lang/String;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 90
    return-void

    .line 84
    :cond_0
    aget-object v2, v5, v4

    .line 86
    .local v2, "entry":Lorg/apache/poi/hwpf/model/SavedByEntry;
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/SavedByEntry;->getUserName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v1

    .line 87
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "counter":I
    .restart local v1    # "counter":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/SavedByEntry;->getSaveLocation()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v0

    .line 84
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
