.class public final Lorg/apache/poi/hwpf/model/DocumentProperties;
.super Lorg/apache/poi/hwpf/model/types/DOPAbstractType;
.source "DocumentProperties.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _preserved:[B


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "tableStream"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 43
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/DOPAbstractType;->getSize()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/model/DocumentProperties;-><init>([BII)V

    .line 44
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 3
    .param p1, "tableStream"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/DOPAbstractType;-><init>()V

    .line 48
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hwpf/model/types/DOPAbstractType;->fillFields([BI)V

    .line 50
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/DOPAbstractType;->getSize()I

    move-result v0

    .line 51
    .local v0, "supportedSize":I
    if-eq p3, v0, :cond_0

    .line 54
    add-int v1, p2, v0

    sub-int v2, p3, v0

    .line 53
    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/DocumentProperties;->_preserved:[B

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/DocumentProperties;->_preserved:[B

    goto :goto_0
.end method


# virtual methods
.method public serialize([BI)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hwpf/model/types/DOPAbstractType;->serialize([BI)V

    .line 66
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {}, Lorg/apache/poi/hwpf/model/DocumentProperties;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 71
    .local v0, "supported":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/DocumentProperties;->serialize([BI)V

    .line 73
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 74
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/DocumentProperties;->_preserved:[B

    invoke-virtual {p1, v1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 75
    return-void
.end method
