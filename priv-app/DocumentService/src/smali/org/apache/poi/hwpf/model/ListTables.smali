.class public final Lorg/apache/poi/hwpf/model/ListTables;
.super Ljava/lang/Object;
.source "ListTables.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final _listMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hwpf/model/ListData;",
            ">;"
        }
    .end annotation
.end field

.field private _plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/poi/hwpf/model/ListTables;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/ListTables;->log:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    .line 48
    return-void
.end method

.method public constructor <init>([BIII)V
    .locals 10
    .param p1, "tableStream"    # [B
    .param p2, "lstOffset"    # I
    .param p3, "fcPlfLfo"    # I
    .param p4, "lcbPlfLfo"    # I

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v8, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    .line 59
    move v5, p2

    .line 61
    .local v5, "offset":I
    invoke-static {p1, v5}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    .line 62
    .local v0, "cLst":I
    add-int/lit8 v5, v5, 0x2

    .line 63
    invoke-static {}, Lorg/apache/poi/hwpf/model/LSTF;->getSize()I

    move-result v8

    mul-int/2addr v8, v0

    add-int v1, v5, v8

    .line 65
    .local v1, "levelOffset":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    if-lt v6, v0, :cond_0

    .line 80
    new-instance v8, Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-direct {v8, p1, p3, p4}, Lorg/apache/poi/hwpf/model/PlfLfo;-><init>([BII)V

    iput-object v8, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    .line 81
    return-void

    .line 67
    :cond_0
    new-instance v2, Lorg/apache/poi/hwpf/model/ListData;

    invoke-direct {v2, p1, v5}, Lorg/apache/poi/hwpf/model/ListData;-><init>([BI)V

    .line 68
    .local v2, "lst":Lorg/apache/poi/hwpf/model/ListData;
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/ListData;->getLsid()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-static {}, Lorg/apache/poi/hwpf/model/LSTF;->getSize()I

    move-result v8

    add-int/2addr v5, v8

    .line 71
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/ListData;->numLevels()I

    move-result v4

    .line 72
    .local v4, "num":I
    const/4 v7, 0x0

    .local v7, "y":I
    :goto_1
    if-lt v7, v4, :cond_1

    .line 65
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 74
    :cond_1
    new-instance v3, Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/model/ListLevel;-><init>()V

    .line 75
    .local v3, "lvl":Lorg/apache/poi/hwpf/model/ListLevel;
    invoke-virtual {v3, p1, v1}, Lorg/apache/poi/hwpf/model/ListLevel;->read([BI)I

    move-result v8

    add-int/2addr v1, v8

    .line 76
    invoke-virtual {v2, v7, v3}, Lorg/apache/poi/hwpf/model/ListData;->setLevel(ILorg/apache/poi/hwpf/model/ListLevel;)V

    .line 72
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addList(Lorg/apache/poi/hwpf/model/ListData;Lorg/apache/poi/hwpf/model/LFO;Lorg/apache/poi/hwpf/model/LFOData;)I
    .locals 3
    .param p1, "lst"    # Lorg/apache/poi/hwpf/model/ListData;
    .param p2, "lfo"    # Lorg/apache/poi/hwpf/model/LFO;
    .param p3, "lfoData"    # Lorg/apache/poi/hwpf/model/LFOData;

    .prologue
    .line 196
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/ListData;->getLsid()I

    move-result v0

    .line 197
    .local v0, "lsid":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 202
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    .line 206
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 207
    const-string/jumbo v2, "LFO and LFOData should be specified both or noone"

    .line 206
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 199
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/ListData;->resetListID()I

    move-result v0

    .line 200
    invoke-virtual {p2, v0}, Lorg/apache/poi/hwpf/model/LFO;->setLsid(I)V

    goto :goto_0

    .line 209
    :cond_1
    if-eqz p2, :cond_2

    .line 211
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v1, p2, p3}, Lorg/apache/poi/hwpf/model/PlfLfo;->add(Lorg/apache/poi/hwpf/model/LFO;Lorg/apache/poi/hwpf/model/LFOData;)V

    .line 213
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    if-ne p0, p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v1

    .line 172
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 173
    goto :goto_0

    .line 174
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 175
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 176
    check-cast v0, Lorg/apache/poi/hwpf/model/ListTables;

    .line 177
    .local v0, "other":Lorg/apache/poi/hwpf/model/ListTables;
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    if-nez v3, :cond_4

    .line 179
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    if-eqz v3, :cond_5

    move v1, v2

    .line 180
    goto :goto_0

    .line 182
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 183
    goto :goto_0

    .line 184
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    if-nez v3, :cond_6

    .line 186
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    if-eqz v3, :cond_0

    move v1, v2

    .line 187
    goto :goto_0

    .line 189
    :cond_6
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/PlfLfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 190
    goto :goto_0
.end method

.method public getLevel(II)Lorg/apache/poi/hwpf/model/ListLevel;
    .locals 6
    .param p1, "lsid"    # I
    .param p2, "level"    # I

    .prologue
    .line 141
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/ListData;

    .line 142
    .local v0, "lst":Lorg/apache/poi/hwpf/model/ListData;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListData;->numLevels()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 143
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListData;->getLevels()[Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v2

    aget-object v1, v2, p2

    .line 147
    :goto_0
    return-object v1

    .line 146
    :cond_0
    sget-object v2, Lorg/apache/poi/hwpf/model/ListTables;->log:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Requested level "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " which was greater than the maximum defined ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListData;->numLevels()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 147
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLfo(I)Lorg/apache/poi/hwpf/model/LFO;
    .locals 1
    .param p1, "ilfo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlfLfo;->getLfo(I)Lorg/apache/poi/hwpf/model/LFO;

    move-result-object v0

    return-object v0
.end method

.method public getLfoData(I)Lorg/apache/poi/hwpf/model/LFOData;
    .locals 1
    .param p1, "ilfo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlfLfo;->getLfoData(I)Lorg/apache/poi/hwpf/model/LFOData;

    move-result-object v0

    return-object v0
.end method

.method public getListData(I)Lorg/apache/poi/hwpf/model/ListData;
    .locals 2
    .param p1, "lsid"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/ListData;

    return-object v0
.end method

.method public getOverrideIndexFromListID(I)I
    .locals 1
    .param p1, "lsid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlfLfo;->getIlfoByLsid(I)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 158
    const/16 v0, 0x1f

    .line 159
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 161
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    if-nez v2, :cond_0

    move v2, v3

    .line 160
    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 162
    mul-int/lit8 v2, v1, 0x1f

    .line 163
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    if-nez v4, :cond_1

    .line 162
    :goto_1
    add-int v1, v2, v3

    .line 164
    return v1

    .line 161
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->hashCode()I

    move-result v2

    goto :goto_0

    .line 163
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PlfLfo;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public writeListDataTo(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 10
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v5

    .line 87
    .local v5, "startOffset":I
    invoke-virtual {p1, v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcPlfLst(I)V

    .line 89
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    .line 92
    .local v1, "listSize":I
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 94
    .local v0, "levelBuf":Ljava/io/ByteArrayOutputStream;
    const/4 v8, 0x2

    new-array v4, v8, [B

    .line 95
    .local v4, "shortHolder":[B
    int-to-short v8, v1

    invoke-static {v4, v8}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 96
    invoke-virtual {p2, v4}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 98
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 113
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v8

    sub-int/2addr v8, v5

    invoke-virtual {p1, v8}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbPlfLst(I)V

    .line 114
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 115
    return-void

    .line 98
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 99
    .local v6, "x":Ljava/lang/Integer;
    iget-object v9, p0, Lorg/apache/poi/hwpf/model/ListTables;->_listMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v9, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/ListData;

    .line 100
    .local v2, "lst":Lorg/apache/poi/hwpf/model/ListData;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/ListData;->toByteArray()[B

    move-result-object v9

    invoke-virtual {p2, v9}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 101
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/ListData;->getLevels()[Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v3

    .line 102
    .local v3, "lvls":[Lorg/apache/poi/hwpf/model/ListLevel;
    const/4 v7, 0x0

    .local v7, "y":I
    :goto_0
    array-length v9, v3

    if-ge v7, v9, :cond_0

    .line 104
    aget-object v9, v3, v7

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/ListLevel;->toByteArray()[B

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 102
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public writeListOverridesTo(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 1
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListTables;->_plfLfo:Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hwpf/model/PlfLfo;->writeTo(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 121
    return-void
.end method
