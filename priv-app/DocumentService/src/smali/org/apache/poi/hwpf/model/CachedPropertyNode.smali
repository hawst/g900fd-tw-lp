.class public final Lorg/apache/poi/hwpf/model/CachedPropertyNode;
.super Lorg/apache/poi/hwpf/model/PropertyNode;
.source "CachedPropertyNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/poi/hwpf/model/PropertyNode",
        "<",
        "Lorg/apache/poi/hwpf/model/CachedPropertyNode;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected _propCache:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected fillCache(Ljava/lang/Object;)V
    .locals 1
    .param p1, "ref"    # Ljava/lang/Object;

    .prologue
    .line 38
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/CachedPropertyNode;->_propCache:Ljava/lang/ref/SoftReference;

    .line 39
    return-void
.end method

.method protected getCacheContents()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CachedPropertyNode;->_propCache:Ljava/lang/ref/SoftReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CachedPropertyNode;->_propCache:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CachedPropertyNode;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method
