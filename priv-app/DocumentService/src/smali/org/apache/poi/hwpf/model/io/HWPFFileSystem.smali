.class public final Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;
.super Ljava/lang/Object;
.source "HWPFFileSystem.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field _streams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->_streams:Ljava/util/Map;

    .line 33
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->_streams:Ljava/util/Map;

    const-string/jumbo v1, "WordDocument"

    new-instance v2, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->_streams:Ljava/util/Map;

    const-string/jumbo v1, "1Table"

    new-instance v2, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->_streams:Ljava/util/Map;

    const-string/jumbo v1, "Data"

    new-instance v2, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method


# virtual methods
.method public getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->_streams:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    return-object v0
.end method
