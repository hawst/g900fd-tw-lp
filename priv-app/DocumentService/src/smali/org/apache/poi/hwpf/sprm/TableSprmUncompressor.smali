.class public final Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;
.super Lorg/apache/poi/hwpf/sprm/SprmUncompressor;
.source "TableSprmUncompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 36
    return-void
.end method

.method static unCompressTAPOperation(Lorg/apache/poi/hwpf/usermodel/TableProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    .locals 31
    .param p0, "newTAP"    # Lorg/apache/poi/hwpf/usermodel/TableProperties;
    .param p1, "sprm"    # Lorg/apache/poi/hwpf/sprm/SprmOperation;

    .prologue
    .line 120
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v27

    packed-switch v27, :pswitch_data_0

    .line 343
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 123
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v27

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setJc(S)V

    goto :goto_0

    .line 127
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v20

    .line 128
    .local v20, "rgdxaCenter":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getItcMac()S

    move-result v17

    .line 129
    .local v17, "itcMac":S
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v27

    const/16 v28, 0x0

    aget-short v28, v20, v28

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getDxaGapHalf()I

    move-result v29

    add-int v28, v28, v29

    sub-int v5, v27, v28

    .line 130
    .local v5, "adjust":I
    const/16 v26, 0x0

    .local v26, "x":I
    :goto_1
    move/from16 v0, v26

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    .line 132
    aget-short v27, v20, v26

    add-int v27, v27, v5

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    aput-short v27, v20, v26

    .line 130
    add-int/lit8 v26, v26, 0x1

    goto :goto_1

    .line 138
    .end local v5    # "adjust":I
    .end local v17    # "itcMac":S
    .end local v20    # "rgdxaCenter":[S
    .end local v26    # "x":I
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v20

    .line 139
    .restart local v20    # "rgdxaCenter":[S
    if-eqz v20, :cond_1

    .line 141
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getDxaGapHalf()I

    move-result v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v28

    sub-int v5, v27, v28

    .line 142
    .restart local v5    # "adjust":I
    const/16 v27, 0x0

    aget-short v28, v20, v27

    add-int v28, v28, v5

    move/from16 v0, v28

    int-to-short v0, v0

    move/from16 v28, v0

    aput-short v28, v20, v27

    .line 144
    .end local v5    # "adjust":I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setDxaGapHalf(I)V

    goto :goto_0

    .line 148
    .end local v20    # "rgdxaCenter":[S
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v27

    invoke-static/range {v27 .. v27}, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->getFlag(I)Z

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setFCantSplit(Z)V

    goto :goto_0

    .line 151
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v27

    invoke-static/range {v27 .. v27}, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->getFlag(I)Z

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setFTableHeader(Z)V

    goto/16 :goto_0

    .line 155
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v6

    .line 156
    .local v6, "buf":[B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v18

    .line 157
    .local v18, "offset":I
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 158
    add-int/lit8 v18, v18, 0x4

    .line 159
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 160
    add-int/lit8 v18, v18, 0x4

    .line 161
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 162
    add-int/lit8 v18, v18, 0x4

    .line 163
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 164
    add-int/lit8 v18, v18, 0x4

    .line 165
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcHorizontal(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 166
    add-int/lit8 v18, v18, 0x4

    .line 167
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcVertical(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 175
    .end local v6    # "buf":[B
    .end local v18    # "offset":I
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setDyaRowHeight(I)V

    goto/16 :goto_0

    .line 179
    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v12

    .line 180
    .local v12, "grpprl":[B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v18

    .line 181
    .restart local v18    # "offset":I
    aget-byte v17, v12, v18

    .line 182
    .restart local v17    # "itcMac":S
    add-int/lit8 v27, v17, 0x1

    move/from16 v0, v27

    new-array v0, v0, [S

    move-object/from16 v20, v0

    .line 183
    .restart local v20    # "rgdxaCenter":[S
    move/from16 v0, v17

    new-array v0, v0, [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-object/from16 v21, v0

    .line 185
    .local v21, "rgtc":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setItcMac(S)V

    .line 186
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenter([S)V

    .line 187
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgtc([Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;)V

    .line 190
    const/16 v26, 0x0

    .restart local v26    # "x":I
    :goto_2
    move/from16 v0, v26

    move/from16 v1, v17

    if-lt v0, v1, :cond_2

    .line 196
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v27

    add-int v27, v27, v18

    add-int/lit8 v9, v27, -0x6

    .line 197
    .local v9, "endOfSprm":I
    add-int/lit8 v27, v17, 0x1

    mul-int/lit8 v27, v27, 0x2

    add-int/lit8 v27, v27, 0x1

    add-int v22, v18, v27

    .line 199
    .local v22, "startOfTCs":I
    move/from16 v0, v22

    if-ge v0, v9, :cond_3

    const/4 v13, 0x1

    .line 201
    .local v13, "hasTCs":Z
    :goto_3
    const/16 v26, 0x0

    :goto_4
    move/from16 v0, v26

    move/from16 v1, v17

    if-lt v0, v1, :cond_4

    .line 211
    mul-int/lit8 v27, v17, 0x2

    add-int/lit8 v27, v27, 0x1

    add-int v27, v27, v18

    move/from16 v0, v27

    invoke-static {v12, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v27

    aput-short v27, v20, v17

    goto/16 :goto_0

    .line 192
    .end local v9    # "endOfSprm":I
    .end local v13    # "hasTCs":Z
    .end local v22    # "startOfTCs":I
    :cond_2
    mul-int/lit8 v27, v26, 0x2

    add-int/lit8 v27, v27, 0x1

    add-int v27, v27, v18

    move/from16 v0, v27

    invoke-static {v12, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v27

    aput-short v27, v20, v26

    .line 190
    add-int/lit8 v26, v26, 0x1

    goto :goto_2

    .line 199
    .restart local v9    # "endOfSprm":I
    .restart local v22    # "startOfTCs":I
    :cond_3
    const/4 v13, 0x0

    goto :goto_3

    .line 204
    .restart local v13    # "hasTCs":Z
    :cond_4
    if-eqz v13, :cond_5

    add-int/lit8 v27, v17, 0x1

    mul-int/lit8 v27, v27, 0x2

    add-int/lit8 v27, v27, 0x1

    mul-int/lit8 v28, v26, 0x14

    add-int v27, v27, v28

    add-int v27, v27, v18

    array-length v0, v12

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_5

    .line 206
    add-int/lit8 v27, v17, 0x1

    mul-int/lit8 v27, v27, 0x2

    add-int/lit8 v27, v27, 0x1

    mul-int/lit8 v28, v26, 0x14

    add-int v27, v27, v28

    add-int v27, v27, v18

    .line 205
    move/from16 v0, v27

    invoke-static {v12, v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->convertBytesToTC([BI)Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v27

    aput-object v27, v21, v26

    .line 201
    :goto_5
    add-int/lit8 v26, v26, 0x1

    goto :goto_4

    .line 208
    :cond_5
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-direct/range {v27 .. v27}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;-><init>()V

    aput-object v27, v21, v26

    goto :goto_5

    .line 259
    .end local v9    # "endOfSprm":I
    .end local v12    # "grpprl":[B
    .end local v13    # "hasTCs":Z
    .end local v17    # "itcMac":S
    .end local v18    # "offset":I
    .end local v20    # "rgdxaCenter":[S
    .end local v21    # "rgtc":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .end local v22    # "startOfTCs":I
    .end local v26    # "x":I
    :pswitch_9
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v19

    .line 260
    .local v19, "param":I
    const/high16 v27, -0x1000000

    and-int v27, v27, v19

    shr-int/lit8 v14, v27, 0x18

    .line 261
    .local v14, "index":I
    const/high16 v27, 0xff0000

    and-int v27, v27, v19

    shr-int/lit8 v8, v27, 0x10

    .line 262
    .local v8, "count":I
    const v27, 0xffff

    and-int v25, v19, v27

    .line 263
    .local v25, "width":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getItcMac()S

    move-result v17

    .line 265
    .local v17, "itcMac":I
    add-int v27, v17, v8

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [S

    move-object/from16 v20, v0

    .line 266
    .restart local v20    # "rgdxaCenter":[S
    add-int v27, v17, v8

    move/from16 v0, v27

    new-array v0, v0, [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-object/from16 v21, v0

    .line 267
    .restart local v21    # "rgtc":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    move/from16 v0, v17

    if-lt v14, v0, :cond_6

    .line 269
    move/from16 v14, v17

    .line 270
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v27

    const/16 v28, 0x0

    const/16 v29, 0x0

    .line 271
    add-int/lit8 v30, v17, 0x1

    .line 270
    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v20

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 272
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v27

    const/16 v28, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v21

    move/from16 v3, v29

    move/from16 v4, v17

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    :goto_6
    move/from16 v26, v14

    .restart local v26    # "x":I
    :goto_7
    add-int v27, v14, v8

    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_7

    .line 293
    add-int v27, v14, v8

    add-int v28, v14, v8

    add-int/lit8 v28, v28, -0x1

    aget-short v28, v20, v28

    add-int v28, v28, v25

    move/from16 v0, v28

    int-to-short v0, v0

    move/from16 v28, v0

    .line 292
    aput-short v28, v20, v27

    goto/16 :goto_0

    .line 277
    .end local v26    # "x":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v27

    const/16 v28, 0x0

    const/16 v29, 0x0

    .line 278
    add-int/lit8 v30, v14, 0x1

    .line 277
    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v20

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 279
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v27

    add-int/lit8 v28, v14, 0x1

    .line 280
    add-int v29, v14, v8

    sub-int v30, v17, v14

    .line 279
    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v20

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 282
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v27

    const/16 v28, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v21

    move/from16 v3, v29

    invoke-static {v0, v1, v2, v3, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 283
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v27

    add-int v28, v14, v8

    .line 284
    sub-int v29, v17, v14

    .line 283
    move-object/from16 v0, v27

    move-object/from16 v1, v21

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-static {v0, v14, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_6

    .line 289
    .restart local v26    # "x":I
    :cond_7
    new-instance v27, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-direct/range {v27 .. v27}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;-><init>()V

    aput-object v27, v21, v26

    .line 290
    add-int/lit8 v27, v26, -0x1

    aget-short v27, v20, v27

    add-int v27, v27, v25

    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    aput-short v27, v20, v26

    .line 287
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_7

    .line 312
    .end local v8    # "count":I
    .end local v14    # "index":I
    .end local v17    # "itcMac":I
    .end local v19    # "param":I
    .end local v20    # "rgdxaCenter":[S
    .end local v21    # "rgtc":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .end local v25    # "width":I
    .end local v26    # "x":I
    :pswitch_a
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v28

    aget-byte v15, v27, v28

    .line 313
    .local v15, "itcFirst":B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v28

    add-int/lit8 v28, v28, 0x1

    aget-byte v16, v27, v28

    .line 314
    .local v16, "itcLim":B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v28

    add-int/lit8 v28, v28, 0x2

    aget-byte v11, v27, v28

    .line 315
    .local v11, "grfbrc":B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v28

    add-int/lit8 v28, v28, 0x3

    aget-byte v10, v27, v28

    .line 316
    .local v10, "ftsWidth":B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v27

    .line 317
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v28

    add-int/lit8 v28, v28, 0x4

    .line 316
    invoke-static/range {v27 .. v28}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v24

    .line 319
    .local v24, "wWidth":S
    move v7, v15

    .local v7, "c":I
    :goto_8
    move/from16 v0, v16

    if-ge v7, v0, :cond_0

    .line 320
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v27

    aget-object v23, v27, v7

    .line 322
    .local v23, "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    and-int/lit8 v27, v11, 0x1

    if-eqz v27, :cond_8

    .line 323
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setFtsCellPaddingTop(B)V

    .line 324
    invoke-virtual/range {v23 .. v24}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setWCellPaddingTop(S)V

    .line 326
    :cond_8
    and-int/lit8 v27, v11, 0x2

    if-eqz v27, :cond_9

    .line 327
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setFtsCellPaddingLeft(B)V

    .line 328
    invoke-virtual/range {v23 .. v24}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setWCellPaddingLeft(S)V

    .line 330
    :cond_9
    and-int/lit8 v27, v11, 0x4

    if-eqz v27, :cond_a

    .line 331
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setFtsCellPaddingBottom(B)V

    .line 332
    invoke-virtual/range {v23 .. v24}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setWCellPaddingBottom(S)V

    .line 334
    :cond_a
    and-int/lit8 v27, v11, 0x8

    if-eqz v27, :cond_b

    .line 335
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setFtsCellPaddingRight(B)V

    .line 336
    invoke-virtual/range {v23 .. v24}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setWCellPaddingRight(S)V

    .line 319
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public static uncompressTAP(Lorg/apache/poi/hwpf/sprm/SprmBuffer;)Lorg/apache/poi/hwpf/usermodel/TableProperties;
    .locals 14
    .param p0, "sprmBuffer"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .prologue
    const/4 v13, 0x5

    .line 70
    const/16 v0, -0x29f8

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->findSprm(S)Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v11

    .line 71
    .local v11, "sprmOperation":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    if-eqz v11, :cond_1

    .line 73
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v7

    .line 74
    .local v7, "grpprl":[B
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    .line 75
    .local v10, "offset":I
    aget-byte v8, v7, v10

    .line 76
    .local v8, "itcMac":S
    new-instance v12, Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-direct {v12, v8}, Lorg/apache/poi/hwpf/usermodel/TableProperties;-><init>(S)V

    .line 85
    .end local v7    # "grpprl":[B
    .end local v8    # "itcMac":S
    .end local v10    # "offset":I
    .local v12, "tableProperties":Lorg/apache/poi/hwpf/usermodel/TableProperties;
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->iterator()Lorg/apache/poi/hwpf/sprm/SprmIterator;

    move-result-object v9

    .local v9, "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_0
    :goto_1
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    return-object v12

    .line 80
    .end local v9    # "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .end local v12    # "tableProperties":Lorg/apache/poi/hwpf/usermodel/TableProperties;
    :cond_1
    sget-object v0, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    .line 81
    const-string/jumbo v1, "Some table rows didn\'t specify number of columns in SPRMs"

    .line 80
    invoke-virtual {v0, v13, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 82
    new-instance v12, Lorg/apache/poi/hwpf/usermodel/TableProperties;

    const/4 v0, 0x1

    invoke-direct {v12, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;-><init>(S)V

    .restart local v12    # "tableProperties":Lorg/apache/poi/hwpf/usermodel/TableProperties;
    goto :goto_0

    .line 87
    .restart local v9    # "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_2
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v3

    .line 93
    .local v3, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v0

    if-ne v0, v13, :cond_0

    .line 97
    :try_start_0
    invoke-static {v12, v3}, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->unCompressTAPOperation(Lorg/apache/poi/hwpf/usermodel/TableProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 99
    :catch_0
    move-exception v5

    .line 101
    .local v5, "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v0, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x7

    const-string/jumbo v2, "Unable to apply "

    .line 102
    const-string/jumbo v4, ": "

    move-object v6, v5

    .line 101
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static uncompressTAP([BI)Lorg/apache/poi/hwpf/usermodel/TableProperties;
    .locals 9
    .param p0, "grpprl"    # [B
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 42
    new-instance v7, Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-direct {v7}, Lorg/apache/poi/hwpf/usermodel/TableProperties;-><init>()V

    .line 44
    .local v7, "newProperties":Lorg/apache/poi/hwpf/usermodel/TableProperties;
    new-instance v8, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-direct {v8, p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 46
    .local v8, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_0
    :goto_0
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    return-object v7

    .line 48
    :cond_1
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v3

    .line 52
    .local v3, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 55
    :try_start_0
    invoke-static {v7, v3}, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->unCompressTAPOperation(Lorg/apache/poi/hwpf/usermodel/TableProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v5

    .line 57
    .local v5, "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v0, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x7

    const-string/jumbo v2, "Unable to apply "

    .line 58
    const-string/jumbo v4, ": "

    move-object v6, v5

    .line 57
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
