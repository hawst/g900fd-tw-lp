.class public final Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;
.super Lorg/apache/poi/hwpf/model/types/SHD80AbstractType;
.source "ShadingDescriptor80.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/SHD80AbstractType;-><init>()V

    .line 33
    return-void
.end method

.method public constructor <init>(S)V
    .locals 0
    .param p1, "value"    # S

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/SHD80AbstractType;-><init>()V

    .line 44
    iput-short p1, p0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->field_1_value:S

    .line 45
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/SHD80AbstractType;-><init>()V

    .line 38
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->fillFields([BI)V

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 54
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->field_1_value:S

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 60
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->serialize([BI)V

    .line 61
    return-object v0
.end method

.method public toShadingDescriptor()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>()V

    .line 67
    .local v0, "result":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getIcoFore()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/model/Colorref;->valueOfIco(I)Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->setCvFore(Lorg/apache/poi/hwpf/model/Colorref;)V

    .line 68
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getIcoBack()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/model/Colorref;->valueOfIco(I)Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->setCvBack(Lorg/apache/poi/hwpf/model/Colorref;)V

    .line 69
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getIpat()B

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->setIpat(I)V

    .line 70
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string/jumbo v0, "[SHD80] EMPTY"

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[SHD80] (icoFore: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getIcoFore()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; icoBack: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getIcoBack()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; iPat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->getIpat()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
