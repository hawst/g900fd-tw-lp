.class public final Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
.super Lorg/apache/poi/hwpf/model/types/PAPAbstractType;
.source "ParagraphProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private jcLogical:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->jcLogical:Z

    .line 30
    const/16 v0, 0x54

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setAnld([B)V

    .line 31
    const/16 v0, 0xc

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setPhe([B)V

    .line 32
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 37
    .local v0, "pp":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getAnld()[B

    move-result-object v1

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setAnld([B)V

    .line 38
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 39
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 40
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 41
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 42
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBetween()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBetween(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 43
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBar(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 44
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->clone()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDcs(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V

    .line 45
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setLspd(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V

    .line 46
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 47
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getPhe()[B

    move-result-object v1

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setPhe([B)V

    .line 48
    return-object v0
.end method

.method public getBarBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getDropCap()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v0

    return-object v0
.end method

.method public getFirstLineIndent()I
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaLeft1()I

    move-result v0

    return v0
.end method

.method public getFontAlignment()I
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getWAlignFont()I

    move-result v0

    return v0
.end method

.method public getIndentFromLeft()I
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result v0

    return v0
.end method

.method public getIndentFromRight()I
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDxaRight()I

    move-result v0

    return v0
.end method

.method public getJustification()I
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->jcLogical:Z

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFBiDi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v0

    .line 104
    :goto_0
    return v0

    .line 93
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 100
    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v0

    goto :goto_0

    .line 96
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 98
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v0

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getLineSpacing()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getShading()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getSpacingAfter()I
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaAfter()I

    move-result v0

    return v0
.end method

.method public getSpacingBefore()I
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    move-result v0

    return v0
.end method

.method public getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public isAutoHyphenated()Z
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFNoAutoHyph()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isBackward()Z
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    move-result v0

    return v0
.end method

.method public isKinsoku()Z
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFKinsoku()Z

    move-result v0

    return v0
.end method

.method public isLineNotNumbered()Z
    .locals 1

    .prologue
    .line 159
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFNoLnn()Z

    move-result v0

    return v0
.end method

.method public isSideBySide()Z
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFSideBySide()Z

    move-result v0

    return v0
.end method

.method public isVertical()Z
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    move-result v0

    return v0
.end method

.method public isWidowControlled()Z
    .locals 1

    .prologue
    .line 174
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFWidowControl()Z

    move-result v0

    return v0
.end method

.method public isWordWrapped()Z
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFWordWrap()Z

    move-result v0

    return v0
.end method

.method public keepOnPage()Z
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFKeep()Z

    move-result v0

    return v0
.end method

.method public keepWithNext()Z
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFKeepFollow()Z

    move-result v0

    return v0
.end method

.method public pageBreakBefore()Z
    .locals 1

    .prologue
    .line 194
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->getFPageBreakBefore()Z

    move-result v0

    return v0
.end method

.method public setAutoHyphenated(Z)V
    .locals 1
    .param p1, "auto"    # Z

    .prologue
    .line 199
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, v0}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFNoAutoHyph(Z)V

    .line 200
    return-void

    .line 199
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setBackward(Z)V
    .locals 0
    .param p1, "bward"    # Z

    .prologue
    .line 204
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFBackward(Z)V

    .line 205
    return-void
.end method

.method public setBarBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "bar"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 209
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setBrcBar(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 210
    return-void
.end method

.method public setBottomBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "bottom"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 214
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 215
    return-void
.end method

.method public setDropCap(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V
    .locals 0
    .param p1, "dcs"    # Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    .prologue
    .line 219
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setDcs(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V

    .line 220
    return-void
.end method

.method public setFirstLineIndent(I)V
    .locals 0
    .param p1, "first"    # I

    .prologue
    .line 224
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setDxaLeft1(I)V

    .line 225
    return-void
.end method

.method public setFontAlignment(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 229
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setWAlignFont(I)V

    .line 230
    return-void
.end method

.method public setIndentFromLeft(I)V
    .locals 0
    .param p1, "dxaLeft"    # I

    .prologue
    .line 234
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setDxaLeft(I)V

    .line 235
    return-void
.end method

.method public setIndentFromRight(I)V
    .locals 0
    .param p1, "dxaRight"    # I

    .prologue
    .line 239
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setDxaRight(I)V

    .line 240
    return-void
.end method

.method public setJustification(B)V
    .locals 1
    .param p1, "jc"    # B

    .prologue
    .line 244
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setJc(B)V

    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->jcLogical:Z

    .line 246
    return-void
.end method

.method public setJustificationLogical(B)V
    .locals 1
    .param p1, "jc"    # B

    .prologue
    .line 250
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setJc(B)V

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->jcLogical:Z

    .line 252
    return-void
.end method

.method public setKeepOnPage(Z)V
    .locals 0
    .param p1, "fKeep"    # Z

    .prologue
    .line 256
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFKeep(Z)V

    .line 257
    return-void
.end method

.method public setKeepWithNext(Z)V
    .locals 0
    .param p1, "fKeepFollow"    # Z

    .prologue
    .line 261
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFKeepFollow(Z)V

    .line 262
    return-void
.end method

.method public setKinsoku(Z)V
    .locals 0
    .param p1, "kinsoku"    # Z

    .prologue
    .line 266
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFKinsoku(Z)V

    .line 267
    return-void
.end method

.method public setLeftBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "left"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 271
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 272
    return-void
.end method

.method public setLineNotNumbered(Z)V
    .locals 0
    .param p1, "fNoLnn"    # Z

    .prologue
    .line 276
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFNoLnn(Z)V

    .line 277
    return-void
.end method

.method public setLineSpacing(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V
    .locals 0
    .param p1, "lspd"    # Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    .prologue
    .line 281
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setLspd(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V

    .line 282
    return-void
.end method

.method public setPageBreakBefore(Z)V
    .locals 0
    .param p1, "fPageBreak"    # Z

    .prologue
    .line 286
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFPageBreakBefore(Z)V

    .line 287
    return-void
.end method

.method public setRightBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "right"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 291
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 292
    return-void
.end method

.method public setShading(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 0
    .param p1, "shd"    # Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .prologue
    .line 296
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 297
    return-void
.end method

.method public setSideBySide(Z)V
    .locals 0
    .param p1, "fSideBySide"    # Z

    .prologue
    .line 301
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFSideBySide(Z)V

    .line 302
    return-void
.end method

.method public setSpacingAfter(I)V
    .locals 0
    .param p1, "after"    # I

    .prologue
    .line 306
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setDyaAfter(I)V

    .line 307
    return-void
.end method

.method public setSpacingBefore(I)V
    .locals 0
    .param p1, "before"    # I

    .prologue
    .line 311
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setDyaBefore(I)V

    .line 312
    return-void
.end method

.method public setTopBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "top"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 316
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 317
    return-void
.end method

.method public setVertical(Z)V
    .locals 0
    .param p1, "vertical"    # Z

    .prologue
    .line 321
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFVertical(Z)V

    .line 322
    return-void
.end method

.method public setWidowControl(Z)V
    .locals 0
    .param p1, "widowControl"    # Z

    .prologue
    .line 326
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFWidowControl(Z)V

    .line 327
    return-void
.end method

.method public setWordWrapped(Z)V
    .locals 0
    .param p1, "wrap"    # Z

    .prologue
    .line 331
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/PAPAbstractType;->setFWordWrap(Z)V

    .line 332
    return-void
.end method
