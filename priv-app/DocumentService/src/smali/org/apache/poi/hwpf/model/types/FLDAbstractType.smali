.class public abstract Lorg/apache/poi/hwpf/model/types/FLDAbstractType;
.super Ljava/lang/Object;
.source "FLDAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static ch:Lorg/apache/poi/util/BitField;

.field private static fDiffer:Lorg/apache/poi/util/BitField;

.field private static fHasSep:Lorg/apache/poi/util/BitField;

.field private static fLocked:Lorg/apache/poi/util/BitField;

.field private static fNested:Lorg/apache/poi/util/BitField;

.field private static fPrivateResult:Lorg/apache/poi/util/BitField;

.field private static fResultDirty:Lorg/apache/poi/util/BitField;

.field private static fResultEdited:Lorg/apache/poi/util/BitField;

.field private static fZombieEmbed:Lorg/apache/poi/util/BitField;

.field private static reserved:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_chHolder:B

.field protected field_2_flt:B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x40

    .line 41
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1f

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->ch:Lorg/apache/poi/util/BitField;

    .line 42
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xe0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->reserved:Lorg/apache/poi/util/BitField;

    .line 44
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fDiffer:Lorg/apache/poi/util/BitField;

    .line 45
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fZombieEmbed:Lorg/apache/poi/util/BitField;

    .line 46
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fResultDirty:Lorg/apache/poi/util/BitField;

    .line 47
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fResultEdited:Lorg/apache/poi/util/BitField;

    .line 48
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fLocked:Lorg/apache/poi/util/BitField;

    .line 49
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fPrivateResult:Lorg/apache/poi/util/BitField;

    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fNested:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fHasSep:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x6

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 60
    add-int/lit8 v0, p2, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    .line 61
    add-int/lit8 v0, p2, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 62
    return-void
.end method

.method public getCh()B
    .locals 2

    .prologue
    .line 164
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->ch:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getChHolder()B
    .locals 1

    .prologue
    .line 119
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    return v0
.end method

.method public getFlt()B
    .locals 1

    .prologue
    .line 135
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    return v0
.end method

.method public getReserved()B
    .locals 2

    .prologue
    .line 184
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->reserved:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public isFDiffer()Z
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fDiffer:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHasSep()Z
    .locals 2

    .prologue
    .line 349
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fHasSep:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLocked()Z
    .locals 2

    .prologue
    .line 287
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fLocked:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNested()Z
    .locals 2

    .prologue
    .line 329
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fNested:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPrivateResult()Z
    .locals 2

    .prologue
    .line 308
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fPrivateResult:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFResultDirty()Z
    .locals 2

    .prologue
    .line 246
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fResultDirty:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFResultEdited()Z
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fResultEdited:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFZombieEmbed()Z
    .locals 2

    .prologue
    .line 225
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fZombieEmbed:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 66
    add-int/lit8 v0, p2, 0x0

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    aput-byte v1, p1, v0

    .line 67
    add-int/lit8 v0, p2, 0x1

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    aput-byte v1, p1, v0

    .line 68
    return-void
.end method

.method public setCh(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 152
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->ch:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    .line 154
    return-void
.end method

.method public setChHolder(B)V
    .locals 0
    .param p1, "field_1_chHolder"    # B

    .prologue
    .line 127
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    .line 128
    return-void
.end method

.method public setFDiffer(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 193
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fDiffer:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 195
    return-void
.end method

.method public setFHasSep(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 338
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fHasSep:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 340
    return-void
.end method

.method public setFLocked(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 276
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fLocked:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 278
    return-void
.end method

.method public setFNested(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 318
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fNested:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 320
    return-void
.end method

.method public setFPrivateResult(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 297
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fPrivateResult:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 299
    return-void
.end method

.method public setFResultDirty(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 235
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fResultDirty:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 237
    return-void
.end method

.method public setFResultEdited(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 256
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fResultEdited:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 258
    return-void
.end method

.method public setFZombieEmbed(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 214
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->fZombieEmbed:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 216
    return-void
.end method

.method public setFlt(B)V
    .locals 0
    .param p1, "field_2_flt"    # B

    .prologue
    .line 143
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_2_flt:B

    .line 144
    return-void
.end method

.method public setReserved(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 173
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->reserved:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->field_1_chHolder:B

    .line 175
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 72
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 74
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[FLD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    const-string/jumbo v1, "    .chHolder             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->getChHolder()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    const-string/jumbo v1, "         .ch                       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->getCh()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 80
    const-string/jumbo v1, "         .reserved                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 81
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->getReserved()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 83
    const-string/jumbo v1, "    .flt                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->getFlt()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    const-string/jumbo v1, "         .fDiffer                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 86
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFDiffer()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 87
    const-string/jumbo v1, "         .fZombieEmbed             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 88
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFZombieEmbed()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 89
    const-string/jumbo v1, "         .fResultDirty             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 90
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFResultDirty()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 91
    const-string/jumbo v1, "         .fResultEdited            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 92
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFResultEdited()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 93
    const-string/jumbo v1, "         .fLocked                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 94
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFLocked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 95
    const-string/jumbo v1, "         .fPrivateResult           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 96
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFPrivateResult()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 97
    const-string/jumbo v1, "         .fNested                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 98
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFNested()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 99
    const-string/jumbo v1, "         .fHasSep                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 100
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;->isFHasSep()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 102
    const-string/jumbo v1, "[/FLD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
