.class public final enum Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;
.super Ljava/lang/Enum;
.source "OfficeDrawing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VerticalPositioning"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

.field public static final enum BOTTOM:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

.field public static final enum CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

.field public static final enum INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

.field public static final enum OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

.field public static final enum TOP:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    const-string/jumbo v1, "ABSOLUTE"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;-><init>(Ljava/lang/String;I)V

    .line 89
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    .line 91
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    const-string/jumbo v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;-><init>(Ljava/lang/String;I)V

    .line 94
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->BOTTOM:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    .line 96
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    const-string/jumbo v1, "CENTER"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;-><init>(Ljava/lang/String;I)V

    .line 99
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    .line 101
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    const-string/jumbo v1, "INSIDE"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;-><init>(Ljava/lang/String;I)V

    .line 105
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    .line 107
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    const-string/jumbo v1, "OUTSIDE"

    invoke-direct {v0, v1, v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;-><init>(Ljava/lang/String;I)V

    .line 111
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    .line 113
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    const-string/jumbo v1, "TOP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;-><init>(Ljava/lang/String;I)V

    .line 116
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->TOP:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    .line 83
    const/4 v0, 0x6

    new-array v0, v0, [Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->BOTTOM:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->TOP:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
