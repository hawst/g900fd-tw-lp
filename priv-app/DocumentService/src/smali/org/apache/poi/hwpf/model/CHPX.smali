.class public final Lorg/apache/poi/hwpf/model/CHPX;
.super Lorg/apache/poi/hwpf/model/BytePropertyNode;
.source "CHPX.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/poi/hwpf/model/BytePropertyNode",
        "<",
        "Lorg/apache/poi/hwpf/model/CHPX;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Lorg/apache/poi/hwpf/sprm/SprmBuffer;)V
    .locals 1
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .param p4, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 48
    invoke-interface {p3, p2}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->lookIndexBackward(I)I

    move-result v0

    invoke-direct {p0, p1, v0, p3, p4}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;[B)V
    .locals 3
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .param p4, "grpprl"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 42
    invoke-interface {p3, p2}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->lookIndexBackward(I)I

    move-result v0

    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v2, 0x0

    invoke-direct {v1, p4, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, v0, p3, v1}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method constructor <init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V
    .locals 0
    .param p1, "charStart"    # I
    .param p2, "charEnd"    # I
    .param p3, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILjava/lang/Object;)V

    .line 54
    return-void
.end method


# virtual methods
.method public getCharacterProperties(Lorg/apache/poi/hwpf/model/StyleSheet;S)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 4
    .param p1, "ss"    # Lorg/apache/poi/hwpf/model/StyleSheet;
    .param p2, "istd"    # S

    .prologue
    .line 68
    if-nez p1, :cond_0

    .line 71
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-direct {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;-><init>()V

    .line 77
    :goto_0
    return-object v1

    .line 74
    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 76
    .local v0, "baseStyle":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v2

    const/4 v3, 0x0

    .line 75
    invoke-static {p1, v0, v2, v3}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->uncompressCHP(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BI)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v1

    .line 77
    .local v1, "props":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    goto :goto_0
.end method

.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "CHPX from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 82
    const-string/jumbo v1, " (in bytes "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/CHPX;->getStartBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/CHPX;->getEndBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
