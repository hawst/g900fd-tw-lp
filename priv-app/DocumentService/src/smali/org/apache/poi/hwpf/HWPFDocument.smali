.class public final Lorg/apache/poi/hwpf/HWPFDocument;
.super Lorg/apache/poi/hwpf/HWPFDocumentCore;
.source "HWPFDocument.java"


# static fields
.field static final PROPERTY_PRESERVE_BIN_TABLES:Ljava/lang/String; = "org.apache.poi.hwpf.preserveBinTables"

.field private static final STREAM_DATA:Ljava/lang/String; = "Data"

.field private static final STREAM_TABLE_0:Ljava/lang/String; = "0Table"

.field private static final STREAM_TABLE_1:Ljava/lang/String; = "1Table"

.field private static final TAG:Ljava/lang/String; = "HWPFDocument"


# instance fields
.field protected _bookmarks:Lorg/apache/poi/hwpf/usermodel/Bookmarks;

.field protected _bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

.field protected _cft:Lorg/apache/poi/hwpf/model/ComplexFileTable;

.field protected _dataStream:[B

.field protected _dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

.field protected _endnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

.field protected _endnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

.field protected _escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

.field protected _fields:Lorg/apache/poi/hwpf/usermodel/Fields;

.field protected _fieldsTables:Lorg/apache/poi/hwpf/model/FieldsTables;

.field protected _footnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

.field protected _footnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

.field private _fspaHeaders:Lorg/apache/poi/hwpf/model/FSPATable;

.field private _fspaMain:Lorg/apache/poi/hwpf/model/FSPATable;

.field protected _officeArts:Lorg/apache/poi/hwpf/model/ShapesTable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _officeDrawingsHeaders:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

.field protected _officeDrawingsMain:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

.field protected _pictures:Lorg/apache/poi/hwpf/model/PicturesTable;

.field protected _tableStream:[B

.field protected _tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

.field protected _text:Ljava/lang/StringBuilder;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 157
    invoke-direct {p0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;-><init>()V

    .line 83
    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 85
    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 139
    new-instance v0, Lorg/apache/poi/hwpf/model/NotesTables;

    sget-object v1, Lorg/apache/poi/hwpf/model/NoteType;->ENDNOTE:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/NotesTables;-><init>(Lorg/apache/poi/hwpf/model/NoteType;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    .line 142
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;

    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/usermodel/NotesImpl;-><init>(Lorg/apache/poi/hwpf/model/NotesTables;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

    .line 145
    new-instance v0, Lorg/apache/poi/hwpf/model/NotesTables;

    sget-object v1, Lorg/apache/poi/hwpf/model/NoteType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/NotesTables;-><init>(Lorg/apache/poi/hwpf/model/NoteType;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    .line 148
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;

    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/usermodel/NotesImpl;-><init>(Lorg/apache/poi/hwpf/model/NotesTables;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\r"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 159
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "istream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 173
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 25
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    invoke-direct/range {p0 .. p1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 83
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 85
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 139
    new-instance v2, Lorg/apache/poi/hwpf/model/NotesTables;

    sget-object v3, Lorg/apache/poi/hwpf/model/NoteType;->ENDNOTE:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/model/NotesTables;-><init>(Lorg/apache/poi/hwpf/model/NoteType;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    .line 142
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/NotesImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/usermodel/NotesImpl;-><init>(Lorg/apache/poi/hwpf/model/NotesTables;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

    .line 145
    new-instance v2, Lorg/apache/poi/hwpf/model/NotesTables;

    sget-object v3, Lorg/apache/poi/hwpf/model/NoteType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/model/NotesTables;-><init>(Lorg/apache/poi/hwpf/model/NoteType;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    .line 148
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/NotesImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/usermodel/NotesImpl;-><init>(Lorg/apache/poi/hwpf/model/NotesTables;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FibBase;->getNFib()I

    move-result v2

    const/16 v3, 0x6a

    if-ge v2, v3, :cond_0

    .line 224
    new-instance v2, Lorg/apache/poi/hwpf/OldWordFileFormatException;

    .line 225
    const-string/jumbo v3, "The document is too old - Word 95 or older. Try HWPFOldDocument instead?"

    .line 224
    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/OldWordFileFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 229
    :cond_0
    const-string/jumbo v22, "0Table"

    .line 230
    .local v22, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FibBase;->isFWhichTblStm()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    const-string/jumbo v22, "1Table"

    .line 237
    :cond_1
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v24

    check-cast v24, Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    .local v24, "tableProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface/range {v24 .. v24}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v2

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    .line 247
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    invoke-virtual {v2, v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    .line 249
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->fillVariableFields([B[B)V

    .line 253
    :try_start_1
    const-string/jumbo v2, "Data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v16

    .line 252
    check-cast v16, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 254
    .local v16, "dataProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface/range {v16 .. v16}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v2

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStream:[B

    .line 255
    const-string/jumbo v2, "Data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStream:[B

    invoke-virtual {v2, v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 263
    .end local v16    # "dataProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    :goto_0
    const/16 v19, 0x0

    .line 269
    .local v19, "fcMin":I
    new-instance v2, Lorg/apache/poi/hwpf/model/ComplexFileTable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcClx()I

    move-result v5

    .line 270
    move/from16 v0, v19

    invoke-direct {v2, v3, v4, v5, v0}, Lorg/apache/poi/hwpf/model/ComplexFileTable;-><init>([B[BII)V

    .line 269
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/poi/hwpf/model/ComplexFileTable;

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/poi/hwpf/model/ComplexFileTable;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v7

    .line 275
    .local v7, "_tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    new-instance v2, Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    .line 276
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcfbteChpx()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcfbteChpx()I

    move-result v6

    invoke-direct/range {v2 .. v7}, Lorg/apache/poi/hwpf/model/CHPBinTable;-><init>([B[BIILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 275
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_cbt:Lorg/apache/poi/hwpf/model/CHPBinTable;

    .line 277
    new-instance v8, Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStream:[B

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcfbtePapx()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcfbtePapx()I

    move-result v13

    move-object v14, v7

    invoke-direct/range {v8 .. v14}, Lorg/apache/poi/hwpf/model/PAPBinTable;-><init>([B[B[BIILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 277
    move-object/from16 v0, p0

    iput-object v8, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_pbt:Lorg/apache/poi/hwpf/model/PAPBinTable;

    .line 279
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getText()Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 286
    const/16 v23, 0x0

    .line 289
    .local v23, "preserveBinTables":Z
    :try_start_2
    const-string/jumbo v2, "org.apache.poi.hwpf.preserveBinTables"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 288
    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v23

    .line 293
    :goto_1
    if-nez v23, :cond_2

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_pbt:Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/poi/hwpf/model/ComplexFileTable;

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/model/PAPBinTable;->rebuild(Ljava/lang/StringBuilder;Lorg/apache/poi/hwpf/model/ComplexFileTable;)V

    .line 316
    :cond_2
    new-instance v2, Lorg/apache/poi/hwpf/model/FSPATable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .line 317
    sget-object v5, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->HEADER:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/hwpf/model/FSPATable;-><init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FSPADocumentPart;)V

    .line 316
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaHeaders:Lorg/apache/poi/hwpf/model/FSPATable;

    .line 318
    new-instance v2, Lorg/apache/poi/hwpf/model/FSPATable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    sget-object v5, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/hwpf/model/FSPATable;-><init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FSPADocumentPart;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaMain:Lorg/apache/poi/hwpf/model/FSPATable;

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcDggInfo()I

    move-result v2

    if-eqz v2, :cond_4

    .line 321
    new-instance v2, Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    .line 322
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcDggInfo()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbDggInfo()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;-><init>([BII)V

    .line 321
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 328
    :goto_2
    new-instance v8, Lorg/apache/poi/hwpf/model/PicturesTable;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStream:[B

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    .line 329
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaMain:Lorg/apache/poi/hwpf/model/FSPATable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v13}, Lorg/apache/poi/hwpf/model/PicturesTable;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;[B[BLorg/apache/poi/hwpf/model/FSPATable;Lorg/apache/poi/hwpf/model/EscherRecordHolder;)V

    .line 328
    move-object/from16 v0, p0

    iput-object v8, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_pictures:Lorg/apache/poi/hwpf/model/PicturesTable;

    .line 331
    new-instance v2, Lorg/apache/poi/hwpf/model/ShapesTable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hwpf/model/ShapesTable;-><init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_officeArts:Lorg/apache/poi/hwpf/model/ShapesTable;

    .line 334
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaHeaders:Lorg/apache/poi/hwpf/model/FSPATable;

    .line 335
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;-><init>(Lorg/apache/poi/hwpf/model/FSPATable;Lorg/apache/poi/hwpf/model/EscherRecordHolder;[B)V

    .line 334
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_officeDrawingsHeaders:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    .line 336
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaMain:Lorg/apache/poi/hwpf/model/FSPATable;

    .line 337
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;-><init>(Lorg/apache/poi/hwpf/model/FSPATable;Lorg/apache/poi/hwpf/model/EscherRecordHolder;[B)V

    .line 336
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_officeDrawingsMain:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    .line 339
    new-instance v8, Lorg/apache/poi/hwpf/model/SectionTable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcfsed()I

    move-result v11

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcfsed()I

    move-result v12

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    sget-object v3, Lorg/apache/poi/hwpf/model/SubdocumentType;->MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I

    move-result v15

    move/from16 v13, v19

    move-object v14, v7

    invoke-direct/range {v8 .. v15}, Lorg/apache/poi/hwpf/model/SectionTable;-><init>([B[BIIILorg/apache/poi/hwpf/model/TextPieceTable;I)V

    .line 339
    move-object/from16 v0, p0

    iput-object v8, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_st:Lorg/apache/poi/hwpf/model/SectionTable;

    .line 342
    new-instance v2, Lorg/apache/poi/hwpf/model/StyleSheet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcStshf()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hwpf/model/StyleSheet;-><init>([BI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_ss:Lorg/apache/poi/hwpf/model/StyleSheet;

    .line 343
    new-instance v2, Lorg/apache/poi/hwpf/model/FontTable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcSttbfffn()I

    move-result v4

    .line 344
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbSttbfffn()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/hwpf/model/FontTable;-><init>([BII)V

    .line 343
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_ft:Lorg/apache/poi/hwpf/model/FontTable;

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlfLst()I

    move-result v21

    .line 349
    .local v21, "listOffset":I
    if-eqz v21, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlfLst()I

    move-result v2

    if-eqz v2, :cond_3

    .line 350
    new-instance v2, Lorg/apache/poi/hwpf/model/ListTables;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlfLfo()I

    move-result v4

    .line 351
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlfLfo()I

    move-result v5

    move/from16 v0, v21

    invoke-direct {v2, v3, v0, v4, v5}, Lorg/apache/poi/hwpf/model/ListTables;-><init>([BIII)V

    .line 350
    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_lt:Lorg/apache/poi/hwpf/model/ListTables;

    .line 377
    :cond_3
    return-void

    .line 238
    .end local v7    # "_tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    .end local v19    # "fcMin":I
    .end local v21    # "listOffset":I
    .end local v23    # "preserveBinTables":Z
    .end local v24    # "tableProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    :catch_0
    move-exception v20

    .line 239
    .local v20, "fnfe":Ljava/io/FileNotFoundException;
    new-instance v2, Ljava/lang/IllegalStateException;

    .line 240
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Table Stream \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 241
    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 242
    const-string/jumbo v4, "\' wasn\'t found - Either the document is corrupt, or is Word95 (or earlier)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 240
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 239
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 257
    .end local v20    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v24    # "tableProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    :catch_1
    move-exception v17

    .line 258
    .local v17, "e":Ljava/io/FileNotFoundException;
    const/4 v2, 0x0

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStream:[B

    goto/16 :goto_0

    .line 290
    .end local v17    # "e":Ljava/io/FileNotFoundException;
    .restart local v7    # "_tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    .restart local v19    # "fcMin":I
    .restart local v23    # "preserveBinTables":Z
    :catch_2
    move-exception v18

    .line 291
    .local v18, "exc":Ljava/lang/Exception;
    const-string/jumbo v2, "HWPFDocument"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Exception in HWPFDocument: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 324
    .end local v18    # "exc":Ljava/lang/Exception;
    :cond_4
    new-instance v2, Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    goto/16 :goto_2
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "pfilesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 204
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "pfilesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 186
    return-void
.end method

.method private getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 7
    .param p1, "subdocument"    # Lorg/apache/poi/hwpf/model/SubdocumentType;

    .prologue
    .line 471
    const/4 v2, 0x0

    .line 472
    .local v2, "startCp":I
    sget-object v4, Lorg/apache/poi/hwpf/model/SubdocumentType;->ORDERED:[Lorg/apache/poi/hwpf/model/SubdocumentType;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 479
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    .line 480
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Subdocument type not supported: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 479
    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 472
    :cond_0
    aget-object v1, v4, v3

    .line 473
    .local v1, "previos":Lorg/apache/poi/hwpf/model/SubdocumentType;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/HWPFDocument;->getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-result-object v6

    .line 474
    invoke-virtual {v6, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I

    move-result v0

    .line 475
    .local v0, "length":I
    if-ne p1, v1, :cond_1

    .line 476
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/Range;

    add-int v4, v2, v0

    invoke-direct {v3, v2, v4, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V

    return-object v3

    .line 477
    :cond_1
    add-int/2addr v2, v0

    .line 472
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public characterLength()I
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    return v0
.end method

.method public closeStreams()V
    .locals 4

    .prologue
    .line 384
    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    if-eqz v1, :cond_0

    .line 386
    :try_start_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    if-eqz v1, :cond_1

    .line 394
    :try_start_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 400
    :cond_1
    :goto_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    if-eqz v1, :cond_2

    .line 402
    :try_start_2
    iget-object v1, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 407
    :cond_2
    :goto_2
    return-void

    .line 387
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "HWPFDocument"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 395
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 396
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v1, "HWPFDocument"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 403
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 404
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v1, "HWPFDocument"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public delete(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 958
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Range;

    add-int v1, p1, p2

    invoke-direct {v0, p1, v1, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 959
    .local v0, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/Range;->delete()V

    .line 960
    return-void
.end method

.method public getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_bookmarks:Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    return-object v0
.end method

.method public getCommentsRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 507
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ANNOTATION:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getDataStream()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 941
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_dataStream:[B

    return-object v0
.end method

.method public getEndnoteRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 498
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENDNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getEndnotes()Lorg/apache/poi/hwpf/usermodel/Notes;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_endnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

    return-object v0
.end method

.method public getEscherRecordHolder()Lorg/apache/poi/hwpf/model/EscherRecordHolder;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 578
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    return-object v0
.end method

.method public getEscherRecordList(I)Ljava/util/List;
    .locals 9
    .param p1, "recordId"    # I

    .prologue
    .line 969
    iget-object v8, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_escherRecordHolder:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 970
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;->getEscherRecords()Ljava/util/List;

    move-result-object v3

    .line 972
    .local v3, "escherRecordsList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 974
    .local v7, "list":Ljava/util/List;
    const/4 v8, 0x1

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/ddf/EscherRecord;

    .line 975
    invoke-virtual {v8}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 999
    return-object v7

    .line 976
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 977
    .local v2, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v8

    if-ne v8, p1, :cond_2

    .line 978
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 980
    :cond_2
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 982
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "childIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 983
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 984
    .local v0, "childEscherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v8

    if-ne v8, p1, :cond_4

    .line 985
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 987
    :cond_4
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 989
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_5
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 990
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherRecord;

    .line 991
    .local v4, "found":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v8

    if-ne v8, p1, :cond_5

    .line 992
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getFSPAHeaderTable()Lorg/apache/poi/hwpf/model/FSPATable;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaHeaders:Lorg/apache/poi/hwpf/model/FSPATable;

    return-object v0
.end method

.method public getFSPATable()Lorg/apache/poi/hwpf/model/FSPATable;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_fspaMain:Lorg/apache/poi/hwpf/model/FSPATable;

    return-object v0
.end method

.method public getFields()Lorg/apache/poi/hwpf/usermodel/Fields;
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_fields:Lorg/apache/poi/hwpf/usermodel/Fields;

    return-object v0
.end method

.method public getFieldsTables()Lorg/apache/poi/hwpf/model/FieldsTables;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 629
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_fieldsTables:Lorg/apache/poi/hwpf/model/FieldsTables;

    return-object v0
.end method

.method public getFootnoteRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 489
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getFootnotes()Lorg/apache/poi/hwpf/usermodel/Notes;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_footnotes:Lorg/apache/poi/hwpf/usermodel/Notes;

    return-object v0
.end method

.method public getHeaderStoryRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 524
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getMainTextboxRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 516
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getOfficeDrawingsHeaders()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_officeDrawingsHeaders:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    return-object v0
.end method

.method public getOfficeDrawingsMain()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_officeDrawingsMain:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    return-object v0
.end method

.method public getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 3

    .prologue
    .line 425
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Range;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-direct {v0, v1, v2, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getPicturesTable()Lorg/apache/poi/hwpf/model/PicturesTable;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_pictures:Lorg/apache/poi/hwpf/model/PicturesTable;

    return-object v0
.end method

.method public getRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 467
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange(Lorg/apache/poi/hwpf/model/SubdocumentType;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getShapesTable()Lorg/apache/poi/hwpf/model/ShapesTable;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 589
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_officeArts:Lorg/apache/poi/hwpf/model/ShapesTable;

    return-object v0
.end method

.method public getTableStream()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 946
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_tableStream:[B

    return-object v0
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 417
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/poi/hwpf/model/ComplexFileTable;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v0

    return-object v0
.end method

.method public registerList(Lorg/apache/poi/hwpf/usermodel/HWPFList;)I
    .locals 4
    .param p1, "list"    # Lorg/apache/poi/hwpf/usermodel/HWPFList;

    .prologue
    .line 950
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_lt:Lorg/apache/poi/hwpf/model/ListTables;

    if-nez v0, :cond_0

    .line 951
    new-instance v0, Lorg/apache/poi/hwpf/model/ListTables;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/ListTables;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_lt:Lorg/apache/poi/hwpf/model/ListTables;

    .line 953
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocument;->_lt:Lorg/apache/poi/hwpf/model/ListTables;

    .line 954
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getListData()Lorg/apache/poi/hwpf/model/ListData;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLFO()Lorg/apache/poi/hwpf/model/LFO;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLFOData()Lorg/apache/poi/hwpf/model/LFOData;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/ListTables;->addList(Lorg/apache/poi/hwpf/model/ListData;Lorg/apache/poi/hwpf/model/LFO;Lorg/apache/poi/hwpf/model/LFOData;)I

    move-result v0

    .line 953
    return v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 937
    return-void
.end method
