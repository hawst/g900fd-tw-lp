.class public Lorg/apache/poi/hwpf/model/SectionTable;
.super Ljava/lang/Object;
.source "SectionTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final SED_SIZE:I = 0xc

.field private static final _logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected _sections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/poi/hwpf/model/SectionTable;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/SectionTable;->_logger:Lorg/apache/poi/util/POILogger;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 48
    return-void
.end method

.method public constructor <init>([B[BIIILorg/apache/poi/hwpf/model/TextPieceTable;I)V
    .locals 22
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "size"    # I
    .param p5, "fcMin"    # I
    .param p6, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;
    .param p7, "mainLength"    # I

    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 55
    new-instance v15, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/16 v19, 0xc

    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, v19

    invoke-direct {v15, v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 59
    .local v15, "sedPlex":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v15}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v8

    .line 61
    .local v8, "length":I
    const/16 v18, 0x0

    .local v18, "x":I
    :goto_0
    move/from16 v0, v18

    if-lt v0, v8, :cond_1

    .line 91
    move/from16 v9, p7

    .line 92
    .local v9, "mainEndsAt":I
    const/4 v10, 0x0

    .line 93
    .local v10, "matchAt":Z
    const/4 v11, 0x0

    .line 94
    .local v11, "matchHalf":Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v7, v0, :cond_3

    .line 102
    if-nez v10, :cond_0

    if-eqz v11, :cond_0

    .line 103
    sget-object v19, Lorg/apache/poi/hwpf/model/SectionTable;->_logger:Lorg/apache/poi/util/POILogger;

    const/16 v20, 0x5

    const-string/jumbo v21, "Your document seemed to be mostly unicode, but the section definition was in bytes! Trying anyway, but things may well go wrong!"

    invoke-virtual/range {v19 .. v21}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 104
    const/4 v7, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v7, v0, :cond_7

    .line 117
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;

    invoke-static/range {v19 .. v20}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 118
    return-void

    .line 63
    .end local v7    # "i":I
    .end local v9    # "mainEndsAt":I
    .end local v10    # "matchAt":Z
    .end local v11    # "matchHalf":Z
    :cond_1
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v12

    .line 64
    .local v12, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    new-instance v14, Lorg/apache/poi/hwpf/model/SectionDescriptor;

    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v14, v0, v1}, Lorg/apache/poi/hwpf/model/SectionDescriptor;-><init>([BI)V

    .line 66
    .local v14, "sed":Lorg/apache/poi/hwpf/model/SectionDescriptor;
    invoke-virtual {v14}, Lorg/apache/poi/hwpf/model/SectionDescriptor;->getFc()I

    move-result v6

    .line 69
    .local v6, "fileOffset":I
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v17

    .line 70
    .local v17, "startAt":I
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v5

    .line 73
    .local v5, "endAt":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v6, v0, :cond_2

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    new-instance v20, Lorg/apache/poi/hwpf/model/SEPX;

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v17

    move-object/from16 v2, v21

    invoke-direct {v0, v14, v1, v5, v2}, Lorg/apache/poi/hwpf/model/SEPX;-><init>(Lorg/apache/poi/hwpf/model/SectionDescriptor;II[B)V

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    :goto_3
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0

    .line 80
    :cond_2
    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v16

    .line 81
    .local v16, "sepxSize":I
    move/from16 v0, v16

    new-array v4, v0, [B

    .line 82
    .local v4, "buf":[B
    add-int/lit8 v6, v6, 0x2

    .line 83
    const/16 v19, 0x0

    array-length v0, v4

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v6, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    new-instance v20, Lorg/apache/poi/hwpf/model/SEPX;

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-direct {v0, v14, v1, v5, v4}, Lorg/apache/poi/hwpf/model/SEPX;-><init>(Lorg/apache/poi/hwpf/model/SectionDescriptor;II[B)V

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 95
    .end local v4    # "buf":[B
    .end local v5    # "endAt":I
    .end local v6    # "fileOffset":I
    .end local v12    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .end local v14    # "sed":Lorg/apache/poi/hwpf/model/SectionDescriptor;
    .end local v16    # "sepxSize":I
    .end local v17    # "startAt":I
    .restart local v7    # "i":I
    .restart local v9    # "mainEndsAt":I
    .restart local v10    # "matchAt":Z
    .restart local v11    # "matchHalf":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/SEPX;

    .line 96
    .local v13, "s":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v19

    move/from16 v0, v19

    if-ne v0, v9, :cond_5

    .line 97
    const/4 v10, 0x1

    .line 94
    :cond_4
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 98
    :cond_5
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v19

    move/from16 v0, v19

    if-eq v0, v9, :cond_6

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v19

    add-int/lit8 v20, v9, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 99
    :cond_6
    const/4 v11, 0x1

    goto :goto_4

    .line 105
    .end local v13    # "s":Lorg/apache/poi/hwpf/model/SEPX;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/SEPX;

    .line 106
    .restart local v13    # "s":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-virtual {v15, v7}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v12

    .line 110
    .restart local v12    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v17

    .line 111
    .restart local v17    # "startAt":I
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v5

    .line 112
    .restart local v5    # "endAt":I
    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/apache/poi/hwpf/model/SEPX;->setStart(I)V

    .line 113
    invoke-virtual {v13, v5}, Lorg/apache/poi/hwpf/model/SEPX;->setEnd(I)V

    .line 104
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public adjustForInsert(II)V
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 122
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 123
    .local v1, "size":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/SEPX;

    .line 124
    .local v0, "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/SEPX;->setEnd(I)V

    .line 126
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 132
    return-void

    .line 128
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/SEPX;

    .line 129
    .restart local v0    # "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/SEPX;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/SEPX;->setStart(I)V

    .line 130
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/SEPX;->setEnd(I)V

    .line 126
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getSections()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    return-object v0
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;I)V
    .locals 3
    .param p1, "sys"    # Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;
    .param p2, "fcMin"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 170
    const-string/jumbo v2, "WordDocument"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    .line 171
    .local v0, "docStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    const-string/jumbo v2, "1Table"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v1

    .line 173
    .local v1, "tableStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/SectionTable;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 174
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 12
    .param p1, "wordDocumentStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v2

    .line 181
    .local v2, "offset":I
    iget-object v9, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 182
    .local v1, "len":I
    new-instance v3, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/16 v9, 0xc

    invoke-direct {v3, v9}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    .line 184
    .local v3, "plex":Lorg/apache/poi/hwpf/model/PlexOfCps;
    const/4 v8, 0x0

    .local v8, "x":I
    :goto_0
    if-lt v8, v1, :cond_0

    .line 223
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v9

    invoke-virtual {p2, v9}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 224
    return-void

    .line 186
    :cond_0
    iget-object v9, p0, Lorg/apache/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/hwpf/model/SEPX;

    .line 187
    .local v6, "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/SEPX;->getGrpprl()[B

    move-result-object v0

    .line 191
    .local v0, "grpprl":[B
    const/4 v9, 0x2

    new-array v7, v9, [B

    .line 192
    .local v7, "shortBuf":[B
    array-length v9, v0

    int-to-short v9, v9

    invoke-static {v7, v9}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 194
    invoke-virtual {p1, v7}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 195
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 198
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/SEPX;->getSectionDescriptor()Lorg/apache/poi/hwpf/model/SectionDescriptor;

    move-result-object v5

    .line 199
    .local v5, "sed":Lorg/apache/poi/hwpf/model/SectionDescriptor;
    invoke-virtual {v5, v2}, Lorg/apache/poi/hwpf/model/SectionDescriptor;->setFc(I)V

    .line 204
    new-instance v4, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 205
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/SEPX;->getStart()I

    move-result v9

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v10

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/SectionDescriptor;->toByteArray()[B

    move-result-object v11

    .line 204
    invoke-direct {v4, v9, v10, v11}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    .line 219
    .local v4, "property":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/PlexOfCps;->addProperty(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 221
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v2

    .line 184
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method
