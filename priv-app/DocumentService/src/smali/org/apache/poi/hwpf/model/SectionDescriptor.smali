.class public final Lorg/apache/poi/hwpf/model/SectionDescriptor;
.super Ljava/lang/Object;
.source "SectionDescriptor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private fcMpr:I

.field private fcSepx:I

.field private fn:S

.field private fnMpr:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fn:S

    .line 62
    add-int/lit8 p2, p2, 0x2

    .line 63
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcSepx:I

    .line 64
    add-int/lit8 p2, p2, 0x4

    .line 65
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    .line 66
    add-int/lit8 p2, p2, 0x2

    .line 67
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcMpr:I

    .line 68
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 83
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/model/SectionDescriptor;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 84
    check-cast v0, Lorg/apache/poi/hwpf/model/SectionDescriptor;

    .line 85
    .local v0, "sed":Lorg/apache/poi/hwpf/model/SectionDescriptor;
    iget-short v2, v0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fn:S

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fn:S

    if-ne v2, v3, :cond_0

    iget-short v2, v0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 87
    .end local v0    # "sed":Lorg/apache/poi/hwpf/model/SectionDescriptor;
    :cond_0
    return v1
.end method

.method public getFc()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcSepx:I

    return v0
.end method

.method public setFc(I)V
    .locals 0
    .param p1, "fc"    # I

    .prologue
    .line 77
    iput p1, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcSepx:I

    .line 78
    return-void
.end method

.method public toByteArray()[B
    .locals 3

    .prologue
    .line 92
    const/4 v1, 0x0

    .line 93
    .local v1, "offset":I
    const/16 v2, 0xc

    new-array v0, v2, [B

    .line 95
    .local v0, "buf":[B
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fn:S

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 96
    add-int/lit8 v1, v1, 0x2

    .line 97
    iget v2, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcSepx:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 98
    add-int/lit8 v1, v1, 0x4

    .line 99
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 100
    add-int/lit8 v1, v1, 0x2

    .line 101
    iget v2, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcMpr:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 103
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[SED] (fn: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fn:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; fcSepx: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcSepx:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; fnMpr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 110
    const-string/jumbo v1, "; fcMpr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/model/SectionDescriptor;->fcMpr:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
