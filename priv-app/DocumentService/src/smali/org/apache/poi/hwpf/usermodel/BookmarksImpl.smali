.class public Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;
.super Ljava/lang/Object;
.source "BookmarksImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/Bookmarks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;
    }
.end annotation


# instance fields
.field private final bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

.field private sortedDescriptors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/GenericPropertyNode;",
            ">;>;"
        }
    .end annotation
.end field

.field private sortedStartPositions:[I


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/BookmarksTables;)V
    .locals 1
    .param p1, "bookmarksTables"    # Lorg/apache/poi/hwpf/model/BookmarksTables;

    .prologue
    const/4 v0, 0x0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedDescriptors:Ljava/util/Map;

    .line 127
    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedStartPositions:[I

    .line 131
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    .line 132
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->reset()V

    .line 133
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    return-object v0
.end method

.method private getBookmark(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)Lorg/apache/poi/hwpf/usermodel/Bookmark;
    .locals 2
    .param p1, "first"    # Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 149
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;-><init>(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;Lorg/apache/poi/hwpf/model/GenericPropertyNode;Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;)V

    return-object v0
.end method

.method private reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 219
    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedDescriptors:Ljava/util/Map;

    .line 220
    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedStartPositions:[I

    .line 221
    return-void
.end method

.method private updateSortedDescriptors()V
    .locals 12

    .prologue
    .line 225
    iget-object v10, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedDescriptors:Ljava/util/Map;

    if-eqz v10, :cond_0

    .line 258
    :goto_0
    return-void

    .line 228
    :cond_0
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 229
    .local v8, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;>;"
    const/4 v1, 0x0

    .local v1, "b":I
    :goto_1
    iget-object v10, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    invoke-virtual {v10}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorsFirstCount()I

    move-result v10

    if-lt v1, v10, :cond_1

    .line 243
    const/4 v2, 0x0

    .line 244
    .local v2, "counter":I
    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v10

    new-array v5, v10, [I

    .line 246
    .local v5, "indices":[I
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 245
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_3

    .line 254
    invoke-static {v5}, Ljava/util/Arrays;->sort([I)V

    .line 256
    iput-object v8, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedDescriptors:Ljava/util/Map;

    .line 257
    iput-object v5, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedStartPositions:[I

    goto :goto_0

    .line 231
    .end local v2    # "counter":I
    .end local v5    # "indices":[I
    :cond_1
    iget-object v10, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    .line 232
    invoke-virtual {v10, v1}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorFirst(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v7

    .line 233
    .local v7, "property":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 234
    .local v6, "positionKey":Ljava/lang/Integer;
    invoke-interface {v8, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 235
    .local v0, "atPositionList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    if-nez v0, :cond_2

    .line 237
    new-instance v0, Ljava/util/LinkedList;

    .end local v0    # "atPositionList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 238
    .restart local v0    # "atPositionList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    invoke-interface {v8, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    :cond_2
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 246
    .end local v0    # "atPositionList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    .end local v6    # "positionKey":Ljava/lang/Integer;
    .end local v7    # "property":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .restart local v2    # "counter":I
    .restart local v5    # "indices":[I
    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 248
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;>;"
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "counter":I
    .local v3, "counter":I
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    aput v10, v5, v2

    .line 249
    new-instance v9, Ljava/util/ArrayList;

    .line 250
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Collection;

    .line 249
    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 251
    .local v9, "updated":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    sget-object v10, Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;

    invoke-static {v9, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 252
    invoke-interface {v4, v9}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    .end local v3    # "counter":I
    .restart local v2    # "counter":I
    goto :goto_2
.end method


# virtual methods
.method afterDelete(II)V
    .locals 1
    .param p1, "startCp"    # I
    .param p2, "length"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hwpf/model/BookmarksTables;->afterDelete(II)V

    .line 138
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->reset()V

    .line 139
    return-void
.end method

.method afterInsert(II)V
    .locals 1
    .param p1, "startCp"    # I
    .param p2, "length"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hwpf/model/BookmarksTables;->afterInsert(II)V

    .line 144
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->reset()V

    .line 145
    return-void
.end method

.method public getBookmark(I)Lorg/apache/poi/hwpf/usermodel/Bookmark;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 154
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    .line 155
    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorFirst(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    .line 156
    .local v0, "first":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->getBookmark(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)Lorg/apache/poi/hwpf/usermodel/Bookmark;

    move-result-object v1

    return-object v1
.end method

.method public getBookmarksAt(I)Ljava/util/List;
    .locals 5
    .param p1, "startCp"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->updateSortedDescriptors()V

    .line 163
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedDescriptors:Ljava/util/Map;

    .line 164
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 163
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 165
    .local v1, "nodes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 173
    :goto_0
    return-object v3

    .line 168
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 169
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 173
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 169
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 171
    .local v0, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->getBookmark(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)Lorg/apache/poi/hwpf/usermodel/Bookmark;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getBookmarksCount()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorsFirstCount()I

    move-result v0

    return v0
.end method

.method public getBookmarksStartedBetween(II)Ljava/util/Map;
    .locals 7
    .param p1, "startInclusive"    # I
    .param p2, "endExclusive"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->updateSortedDescriptors()V

    .line 186
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedStartPositions:[I

    invoke-static {v6, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v4

    .line 188
    .local v4, "startLookupIndex":I
    if-gez v4, :cond_0

    .line 189
    add-int/lit8 v6, v4, 0x1

    neg-int v4, v6

    .line 190
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedStartPositions:[I

    invoke-static {v6, p2}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 192
    .local v0, "endLookupIndex":I
    if-gez v0, :cond_1

    .line 193
    add-int/lit8 v6, v0, 0x1

    neg-int v0, v6

    .line 195
    :cond_1
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 196
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;>;"
    move v1, v4

    .local v1, "lookupIndex":I
    :goto_0
    if-lt v1, v0, :cond_3

    .line 209
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    return-object v6

    .line 198
    :cond_3
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->sortedStartPositions:[I

    aget v3, v6, v1

    .line 199
    .local v3, "s":I
    if-ge v3, p1, :cond_5

    .line 196
    :cond_4
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    :cond_5
    if-ge v3, p2, :cond_2

    .line 204
    invoke-virtual {p0, v3}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->getBookmarksAt(I)Ljava/util/List;

    move-result-object v5

    .line 205
    .local v5, "startedAt":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    if-eqz v5, :cond_4

    .line 206
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public remove(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/BookmarksTables;->remove(I)V

    .line 215
    return-void
.end method
