.class public Lorg/apache/poi/hwpf/model/LFOData;
.super Ljava/lang/Object;
.source "LFOData.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _cp:I

.field private _rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v0, p0, Lorg/apache/poi/hwpf/model/LFOData;->_cp:I

    .line 43
    new-array v0, v0, [Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    .line 44
    return-void
.end method

.method constructor <init>([BII)V
    .locals 4
    .param p1, "buf"    # [B
    .param p2, "startOffset"    # I
    .param p3, "cLfolvl"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    move v0, p2

    .line 50
    .local v0, "offset":I
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/LFOData;->_cp:I

    .line 51
    add-int/lit8 v0, v0, 0x4

    .line 53
    new-array v2, p3, [Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    .line 54
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    if-lt v1, p3, :cond_0

    .line 59
    return-void

    .line 56
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    new-instance v3, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    invoke-direct {v3, p1, v0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;-><init>([BI)V

    aput-object v3, v2, v1

    .line 57
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->getSizeInBytes()I

    move-result v2

    add-int/2addr v0, v2

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCp()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/poi/hwpf/model/LFOData;->_cp:I

    return v0
.end method

.method public getRgLfoLvl()[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    return-object v0
.end method

.method public getSizeInBytes()I
    .locals 6

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "result":I
    add-int/lit8 v1, v1, 0x4

    .line 76
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 79
    return v1

    .line 76
    :cond_0
    aget-object v0, v3, v2

    .line 77
    .local v0, "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->getSizeInBytes()I

    move-result v5

    add-int/2addr v1, v5

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 5
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget v1, p0, Lorg/apache/poi/hwpf/model/LFOData;->_cp:I

    invoke-static {v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt(ILjava/io/OutputStream;)V

    .line 85
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/LFOData;->_rgLfoLvl:[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 89
    return-void

    .line 85
    :cond_0
    aget-object v0, v2, v1

    .line 87
    .local v0, "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->toByteArray()[B

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
