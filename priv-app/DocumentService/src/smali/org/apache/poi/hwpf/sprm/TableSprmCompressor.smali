.class public final Lorg/apache/poi/hwpf/sprm/TableSprmCompressor;
.super Ljava/lang/Object;
.source "TableSprmCompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method public static compressTableProperty(Lorg/apache/poi/hwpf/usermodel/TableProperties;)[B
    .locals 13
    .param p0, "newTAP"    # Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .prologue
    .line 39
    const/4 v7, 0x0

    .line 40
    .local v7, "size":I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v8, "sprmList":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getJc()S

    move-result v10

    if-eqz v10, :cond_0

    .line 44
    const/16 v10, 0x5400

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getJc()S

    move-result v11

    const/4 v12, 0x0

    invoke-static {v10, v11, v12, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 46
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getFCantSplit()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 48
    const/16 v10, 0x3403

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v10, v11, v12, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 50
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getFTableHeader()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 52
    const/16 v10, 0x3404

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v10, v11, v12, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 54
    :cond_2
    const/16 v10, 0x18

    new-array v0, v10, [B

    .line 55
    .local v0, "brcBuf":[B
    const/4 v6, 0x0

    .line 56
    .local v6, "offset":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v10

    invoke-virtual {v10, v0, v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 57
    add-int/lit8 v6, v6, 0x4

    .line 58
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v10

    invoke-virtual {v10, v0, v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 59
    add-int/lit8 v6, v6, 0x4

    .line 60
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v10

    invoke-virtual {v10, v0, v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 61
    add-int/lit8 v6, v6, 0x4

    .line 62
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v10

    invoke-virtual {v10, v0, v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 63
    add-int/lit8 v6, v6, 0x4

    .line 64
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcHorizontal()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v10

    invoke-virtual {v10, v0, v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 65
    add-int/lit8 v6, v6, 0x4

    .line 66
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcVertical()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v10

    invoke-virtual {v10, v0, v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 67
    const/16 v10, 0x18

    new-array v3, v10, [B

    .line 68
    .local v3, "compare":[B
    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v10

    if-nez v10, :cond_3

    .line 70
    const/16 v10, -0x29fb

    const/4 v11, 0x0

    invoke-static {v10, v11, v0, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 72
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getDyaRowHeight()I

    move-result v10

    if-eqz v10, :cond_4

    .line 74
    const/16 v10, -0x6bf9

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getDyaRowHeight()I

    move-result v11

    const/4 v12, 0x0

    invoke-static {v10, v11, v12, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 76
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getItcMac()S

    move-result v10

    if-lez v10, :cond_5

    .line 78
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getItcMac()S

    move-result v5

    .line 79
    .local v5, "itcMac":I
    add-int/lit8 v10, v5, 0x1

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, 0x1

    mul-int/lit8 v11, v5, 0x14

    add-int/2addr v10, v11

    new-array v1, v10, [B

    .line 80
    .local v1, "buf":[B
    const/4 v10, 0x0

    int-to-byte v11, v5

    aput-byte v11, v1, v10

    .line 82
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v4

    .line 83
    .local v4, "dxaCenters":[S
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    array-length v10, v4

    if-lt v9, v10, :cond_7

    .line 89
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    .line 90
    .local v2, "cellDescriptors":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    const/4 v9, 0x0

    :goto_1
    array-length v10, v2

    if-lt v9, v10, :cond_8

    .line 95
    const/16 v10, -0x29f8

    invoke-static {v10, v1, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSpecialSprm(S[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 107
    .end local v1    # "buf":[B
    .end local v2    # "cellDescriptors":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .end local v4    # "dxaCenters":[S
    .end local v5    # "itcMac":I
    .end local v9    # "x":I
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getTlp()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getTlp()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_6

    .line 109
    const/4 v10, 0x4

    new-array v1, v10, [B

    .line 110
    .restart local v1    # "buf":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getTlp()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v1, v11}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->serialize([BI)V

    .line 111
    const/16 v10, 0x740a

    const/4 v11, 0x0

    invoke-static {v10, v11, v1, v8}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v10

    add-int/2addr v7, v10

    .line 114
    .end local v1    # "buf":[B
    :cond_6
    invoke-static {v8, v7}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->getGrpprl(Ljava/util/List;I)[B

    move-result-object v10

    return-object v10

    .line 85
    .restart local v1    # "buf":[B
    .restart local v4    # "dxaCenters":[S
    .restart local v5    # "itcMac":I
    .restart local v9    # "x":I
    :cond_7
    mul-int/lit8 v10, v9, 0x2

    add-int/lit8 v10, v10, 0x1

    .line 86
    aget-short v11, v4, v9

    .line 85
    invoke-static {v1, v10, v11}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 92
    .restart local v2    # "cellDescriptors":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :cond_8
    aget-object v10, v2, v9

    .line 93
    add-int/lit8 v11, v5, 0x1

    mul-int/lit8 v11, v11, 0x2

    add-int/lit8 v11, v11, 0x1

    mul-int/lit8 v12, v9, 0x14

    add-int/2addr v11, v12

    .line 92
    invoke-virtual {v10, v1, v11}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->serialize([BI)V

    .line 90
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method
