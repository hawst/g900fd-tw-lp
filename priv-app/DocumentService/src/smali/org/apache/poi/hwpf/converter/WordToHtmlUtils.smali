.class public Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;
.super Lorg/apache/poi/hwpf/converter/AbstractWordUtils;
.source "WordToHtmlUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;-><init>()V

    return-void
.end method

.method public static addBold(ZLjava/lang/StringBuilder;)V
    .locals 2
    .param p0, "bold"    # Z
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "font-weight:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p0, :cond_0

    const-string/jumbo v0, "bold"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    return-void

    .line 32
    :cond_0
    const-string/jumbo v0, "normal"

    goto :goto_0
.end method

.method public static addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p0, "borderCode"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .param p1, "where"    # Ljava/lang/String;
    .param p2, "style"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v2, 0x20

    .line 38
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    const-string/jumbo v0, "border:"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    :goto_1
    const-string/jumbo v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getLineWidth()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_3

    .line 53
    const-string/jumbo v0, "thin"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    :goto_2
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 57
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->getBorderType(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->getColor(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    const/16 v0, 0x3b

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 47
    :cond_2
    const-string/jumbo v0, "border-"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 55
    :cond_3
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->getBorderWidth(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public static addCharactersProperties(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p0, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 66
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-static {v0, v1, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 68
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isCapitalized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string/jumbo v0, "text-transform:uppercase;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getIco24()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "color:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getIco24()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->getColor24(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 75
    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isHighlighted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "background-color:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getHighlightedColor()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->getColor(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isStrikeThrough()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    const-string/jumbo v0, "text-decoration:line-through;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isShadowed()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "text-shadow:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v1

    div-int/lit8 v1, v1, 0x18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 89
    const-string/jumbo v1, "pt;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSmallCaps()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 93
    const-string/jumbo v0, "font-variant:small-caps;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getSubSuperScriptIndex()S

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 97
    const-string/jumbo v0, "vertical-align:super;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string/jumbo v0, "font-size:smaller;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_6
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getSubSuperScriptIndex()S

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 102
    const-string/jumbo v0, "vertical-align:sub;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string/jumbo v0, "font-size:smaller;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getUnderlineCode()I

    move-result v0

    if-lez v0, :cond_8

    .line 107
    const-string/jumbo v0, "text-decoration:underline;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isVanished()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 111
    const-string/jumbo v0, "visibility:hidden;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_9
    return-void
.end method

.method public static addFontFamily(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p0, "fontFamily"    # Ljava/lang/String;
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 118
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "font-family:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static addFontSize(ILjava/lang/StringBuilder;)V
    .locals 2
    .param p0, "fontSize"    # I
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "font-size:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "pt;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    return-void
.end method

.method private static addIndent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V
    .locals 3
    .param p0, "style"    # Ljava/lang/StringBuilder;
    .param p1, "cssName"    # Ljava/lang/String;
    .param p2, "twipsValue"    # I

    .prologue
    .line 143
    if-nez p2, :cond_0

    .line 147
    :goto_0
    return-void

    .line 146
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    int-to-float v1, p2

    const/high16 v2, 0x44b40000    # 1440.0f

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "in;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static addIndent(Lorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p0, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 131
    const-string/jumbo v0, "text-indent"

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFirstLineIndent()I

    move-result v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addIndent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    .line 133
    const-string/jumbo v0, "margin-left"

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromLeft()I

    move-result v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addIndent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    .line 134
    const-string/jumbo v0, "margin-right"

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromRight()I

    move-result v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addIndent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    .line 136
    const-string/jumbo v0, "margin-top"

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingBefore()I

    move-result v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addIndent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    .line 137
    const-string/jumbo v0, "margin-bottom"

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingAfter()I

    move-result v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addIndent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    .line 138
    return-void
.end method

.method public static addJustification(Lorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p0, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 152
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getJustification()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->getJustification(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "justification":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "text-align:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_0
    return-void
.end method

.method public static addParagraphProperties(Lorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p0, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 160
    invoke-static {p0, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addIndent(Lorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/StringBuilder;)V

    .line 161
    invoke-static {p0, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addJustification(Lorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/StringBuilder;)V

    .line 163
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "bottom"

    invoke-static {v0, v1, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 164
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "left"

    invoke-static {v0, v1, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 165
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "right"

    invoke-static {v0, v1, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 166
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "top"

    invoke-static {v0, v1, p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 168
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->pageBreakBefore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    const-string/jumbo v0, "break-before:page;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "hyphenate:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isAutoHyphenated()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "auto"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->keepOnPage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    const-string/jumbo v0, "keep-together.within-page:always;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->keepWithNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    const-string/jumbo v0, "keep-with-next.within-page:always;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_2
    return-void

    .line 174
    :cond_3
    const-string/jumbo v0, "none"

    goto :goto_0
.end method

.method public static addTableCellProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Lorg/apache/poi/hwpf/usermodel/TableCell;ZZZZLjava/lang/StringBuilder;)V
    .locals 7
    .param p0, "tableRow"    # Lorg/apache/poi/hwpf/usermodel/TableRow;
    .param p1, "tableCell"    # Lorg/apache/poi/hwpf/usermodel/TableCell;
    .param p2, "toppest"    # Z
    .param p3, "bottomest"    # Z
    .param p4, "leftest"    # Z
    .param p5, "rightest"    # Z
    .param p6, "style"    # Ljava/lang/StringBuilder;

    .prologue
    const/high16 v6, 0x44b40000    # 1440.0f

    .line 191
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "width:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 192
    const-string/jumbo v5, "in;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 191
    invoke-virtual {p6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "padding-start:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getGapHalf()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "in;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 193
    invoke-virtual {p6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "padding-end:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getGapHalf()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "in;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 195
    invoke-virtual {p6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 199
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v3

    .line 202
    .local v3, "top":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 203
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_2

    .line 204
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    .line 207
    .local v0, "bottom":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 208
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_4

    .line 209
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    .line 211
    .local v1, "left":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_2
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 212
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_6

    .line 213
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    .line 216
    .local v2, "right":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_3
    const-string/jumbo v4, "bottom"

    invoke-static {v0, v4, p6}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 217
    const-string/jumbo v4, "left"

    invoke-static {v1, v4, p6}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 218
    const-string/jumbo v4, "right"

    invoke-static {v2, v4, p6}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 219
    const-string/jumbo v4, "top"

    invoke-static {v3, v4, p6}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 220
    return-void

    .line 200
    .end local v0    # "bottom":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .end local v1    # "left":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .end local v2    # "right":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .end local v3    # "top":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v3

    goto :goto_0

    .line 201
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getHorizontalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v3

    goto :goto_0

    .line 204
    .restart local v3    # "top":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    goto :goto_1

    .line 205
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getHorizontalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    goto :goto_1

    .line 209
    .restart local v0    # "bottom":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_4
    if-eqz p4, :cond_5

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    goto :goto_2

    .line 210
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getVerticalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    goto :goto_2

    .line 213
    .restart local v1    # "left":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_6
    if-eqz p5, :cond_7

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    goto :goto_3

    .line 214
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getVerticalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    goto :goto_3
.end method

.method public static addTableRowProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p0, "tableRow"    # Lorg/apache/poi/hwpf/usermodel/TableRow;
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 225
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRowHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "height:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRowHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x44b40000    # 1440.0f

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "in;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->cantSplit()Z

    move-result v0

    if-nez v0, :cond_1

    .line 232
    const-string/jumbo v0, "keep-together:always;"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    :cond_1
    return-void
.end method

.method static compactSpans(Lorg/w3c/dom/Element;)V
    .locals 1
    .param p0, "pElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 238
    const-string/jumbo v0, "span"

    invoke-static {p0, v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->compactChildNodesR(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    .line 239
    return-void
.end method
