.class public final Lorg/apache/poi/hwpf/model/StyleDescription;
.super Ljava/lang/Object;
.source "StyleDescription.java"

# interfaces
.implements Lorg/apache/poi/hwpf/model/HDFType;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final CHARACTER_STYLE:I = 0x2

.field private static final PARAGRAPH_STYLE:I = 0x1

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _baseLength:I

.field _chp:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field _name:Ljava/lang/String;

.field _pap:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private _stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

.field private _stdfPost2000:Lorg/apache/poi/hwpf/model/StdfPost2000;

.field _upxs:[Lorg/apache/poi/hwpf/model/UPX;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/poi/hwpf/model/StyleDescription;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/StyleDescription;->logger:Lorg/apache/poi/util/POILogger;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method public constructor <init>([BIIZ)V
    .locals 16
    .param p1, "std"    # [B
    .param p2, "baseLength"    # I
    .param p3, "offset"    # I
    .param p4, "word9"    # Z

    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 64
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/hwpf/model/StyleDescription;->_baseLength:I

    .line 65
    add-int v6, p3, p2

    .line 67
    .local v6, "nameStart":I
    const/4 v7, 0x0

    .line 68
    .local v7, "readStdfPost2000":Z
    const/16 v12, 0x12

    move/from16 v0, p2

    if-ne v0, v12, :cond_1

    .line 70
    const/4 v7, 0x1

    .line 83
    :goto_0
    new-instance v12, Lorg/apache/poi/hwpf/model/StdfBase;

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-direct {v12, v0, v1}, Lorg/apache/poi/hwpf/model/StdfBase;-><init>([BI)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    .line 84
    invoke-static {}, Lorg/apache/poi/hwpf/model/StdfBase;->getSize()I

    move-result v12

    add-int p3, p3, v12

    .line 86
    if-eqz v7, :cond_0

    .line 88
    new-instance v12, Lorg/apache/poi/hwpf/model/StdfPost2000;

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-direct {v12, v0, v1}, Lorg/apache/poi/hwpf/model/StdfPost2000;-><init>([BI)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfPost2000:Lorg/apache/poi/hwpf/model/StdfPost2000;

    .line 95
    :cond_0
    const/4 v5, 0x0

    .line 96
    .local v5, "nameLength":I
    const/4 v4, 0x1

    .line 97
    .local v4, "multiplier":I
    if-eqz p4, :cond_3

    .line 99
    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .line 100
    const/4 v4, 0x2

    .line 101
    add-int/lit8 v6, v6, 0x2

    .line 110
    :goto_1
    :try_start_0
    new-instance v12, Ljava/lang/String;

    mul-int v13, v5, v4

    const-string/jumbo v14, "UTF-16LE"

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v6, v13, v14}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_2
    add-int/lit8 v12, v5, 0x1

    mul-int/2addr v12, v4

    add-int v3, v12, v6

    .line 122
    .local v3, "grupxStart":I
    move v10, v3

    .line 123
    .local v10, "varOffset":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/StdfBase;->getCupx()B

    move-result v2

    .line 124
    .local v2, "countOfUPX":I
    new-array v12, v2, [Lorg/apache/poi/hwpf/model/UPX;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    .line 125
    const/4 v11, 0x0

    .local v11, "x":I
    :goto_3
    if-lt v11, v2, :cond_4

    .line 145
    return-void

    .line 72
    .end local v2    # "countOfUPX":I
    .end local v3    # "grupxStart":I
    .end local v4    # "multiplier":I
    .end local v5    # "nameLength":I
    .end local v10    # "varOffset":I
    .end local v11    # "x":I
    :cond_1
    const/16 v12, 0xa

    move/from16 v0, p2

    if-ne v0, v12, :cond_2

    .line 74
    const/4 v7, 0x0

    .line 75
    goto :goto_0

    .line 78
    :cond_2
    sget-object v12, Lorg/apache/poi/hwpf/model/StyleDescription;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x5

    .line 79
    const-string/jumbo v14, "Style definition has non-standard size of "

    .line 80
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    .line 78
    invoke-virtual {v12, v13, v14, v15}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    .restart local v4    # "multiplier":I
    .restart local v5    # "nameLength":I
    :cond_3
    aget-byte v5, p1, v6

    goto :goto_1

    .line 127
    .restart local v2    # "countOfUPX":I
    .restart local v3    # "grupxStart":I
    .restart local v10    # "varOffset":I
    .restart local v11    # "x":I
    :cond_4
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v9

    .line 128
    .local v9, "upxSize":I
    add-int/lit8 v10, v10, 0x2

    .line 130
    new-array v8, v9, [B

    .line 131
    .local v8, "upx":[B
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10, v8, v12, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    new-instance v13, Lorg/apache/poi/hwpf/model/UPX;

    invoke-direct {v13, v8}, Lorg/apache/poi/hwpf/model/UPX;-><init>([B)V

    aput-object v13, v12, v11

    .line 133
    add-int/2addr v10, v9

    .line 137
    rem-int/lit8 v12, v9, 0x2

    const/4 v13, 0x1

    if-ne v12, v13, :cond_5

    .line 139
    add-int/lit8 v10, v10, 0x1

    .line 125
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 112
    .end local v2    # "countOfUPX":I
    .end local v3    # "grupxStart":I
    .end local v8    # "upx":[B
    .end local v9    # "upxSize":I
    .end local v10    # "varOffset":I
    .end local v11    # "x":I
    :catch_0
    move-exception v12

    goto :goto_2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 263
    if-ne p0, p1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v1

    .line 265
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 266
    goto :goto_0

    .line 267
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 268
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 269
    check-cast v0, Lorg/apache/poi/hwpf/model/StyleDescription;

    .line 270
    .local v0, "other":Lorg/apache/poi/hwpf/model/StyleDescription;
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 272
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 273
    goto :goto_0

    .line 275
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 276
    goto :goto_0

    .line 277
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    if-nez v3, :cond_6

    .line 279
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    if-eqz v3, :cond_7

    move v1, v2

    .line 280
    goto :goto_0

    .line 282
    :cond_6
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/StdfBase;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 283
    goto :goto_0

    .line 284
    :cond_7
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 285
    goto :goto_0
.end method

.method public getBaseStyle()I
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StdfBase;->getIstdBase()S

    move-result v0

    return v0
.end method

.method public getCHP()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_chp:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    return-object v0
.end method

.method public getCHPX()[B
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 152
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/StdfBase;->getStk()B

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 163
    :cond_0
    :goto_0
    return-object v0

    .line 155
    :pswitch_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    array-length v1, v1

    if-le v1, v2, :cond_0

    .line 157
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/UPX;->getUPX()[B

    move-result-object v0

    goto :goto_0

    .line 161
    :pswitch_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/UPX;->getUPX()[B

    move-result-object v0

    goto :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPAP()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_pap:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    return-object v0
.end method

.method public getPAPX()[B
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StdfBase;->getStk()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 174
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 172
    :pswitch_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/UPX;->getUPX()[B

    move-result-object v0

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 251
    const/16 v0, 0x1f

    .line 252
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 253
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 254
    mul-int/lit8 v2, v1, 0x1f

    .line 255
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    if-nez v4, :cond_1

    .line 254
    :goto_1
    add-int v1, v2, v3

    .line 256
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 257
    return v1

    .line 253
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 255
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/StdfBase;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method setCHP(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)V
    .locals 0
    .param p1, "chp"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 195
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_chp:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .line 196
    return-void
.end method

.method setPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)V
    .locals 0
    .param p1, "pap"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 190
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_pap:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 191
    return-void
.end method

.method public toByteArray()[B
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 208
    iget v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_baseLength:I

    add-int/lit8 v6, v6, 0x2

    iget-object v7, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v7, v7, 0x2

    add-int v3, v6, v7

    .line 212
    .local v3, "size":I
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    aget-object v6, v6, v8

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/UPX;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    .line 213
    const/4 v5, 0x1

    .local v5, "x":I
    :goto_0
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    array-length v6, v6

    if-lt v5, v6, :cond_0

    .line 220
    new-array v0, v3, [B

    .line 221
    .local v0, "buf":[B
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v6, v0, v8}, Lorg/apache/poi/hwpf/model/StdfBase;->serialize([BI)V

    .line 223
    iget v2, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_baseLength:I

    .line 225
    .local v2, "offset":I
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 226
    .local v1, "letters":[C
    iget v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_baseLength:I

    array-length v7, v1

    int-to-short v7, v7

    invoke-static {v0, v6, v7}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 227
    add-int/lit8 v2, v2, 0x2

    .line 228
    const/4 v5, 0x0

    :goto_1
    array-length v6, v1

    if-lt v5, v6, :cond_1

    .line 234
    add-int/lit8 v2, v2, 0x2

    .line 236
    const/4 v5, 0x0

    :goto_2
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    array-length v6, v6

    if-lt v5, v6, :cond_2

    .line 245
    return-object v0

    .line 215
    .end local v0    # "buf":[B
    .end local v1    # "letters":[C
    .end local v2    # "offset":I
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    add-int/lit8 v7, v5, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/UPX;->size()I

    move-result v6

    rem-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    .line 216
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/UPX;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    .line 213
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 230
    .restart local v0    # "buf":[B
    .restart local v1    # "letters":[C
    .restart local v2    # "offset":I
    :cond_1
    aget-char v6, v1, v5

    int-to-short v6, v6

    invoke-static {v0, v2, v6}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 231
    add-int/lit8 v2, v2, 0x2

    .line 228
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 238
    :cond_2
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/UPX;->size()I

    move-result v6

    int-to-short v4, v6

    .line 239
    .local v4, "upxSize":S
    invoke-static {v0, v2, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 240
    add-int/lit8 v2, v2, 0x2

    .line 241
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/UPX;->getUPX()[B

    move-result-object v6

    invoke-static {v6, v8, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    rem-int/lit8 v6, v4, 0x2

    add-int/2addr v6, v4

    add-int/2addr v2, v6

    .line 236
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    .local v0, "result":Ljava/lang/StringBuilder;
    const-string/jumbo v2, "[STD]: \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string/jumbo v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\nStdfBase:\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfBase:Lorg/apache/poi/hwpf/model/StdfBase;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "\n"

    .line 297
    const-string/jumbo v4, "\n    "

    .line 296
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\nStdfPost2000:\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_stdfPost2000:Lorg/apache/poi/hwpf/model/StdfPost2000;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 299
    const-string/jumbo v3, "\n"

    const-string/jumbo v4, "\n    "

    .line 298
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleDescription;->_upxs:[Lorg/apache/poi/hwpf/model/UPX;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 304
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 300
    :cond_0
    aget-object v1, v3, v2

    .line 302
    .local v1, "upx":Lorg/apache/poi/hwpf/model/UPX;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "\nUPX:\t"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "\n"

    const-string/jumbo v7, "\n    "

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
