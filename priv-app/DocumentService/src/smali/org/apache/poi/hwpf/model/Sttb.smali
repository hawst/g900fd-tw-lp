.class public Lorg/apache/poi/hwpf/model/Sttb;
.super Ljava/lang/Object;
.source "Sttb.java"


# instance fields
.field private final _cDataLength:I

.field private _cbExtra:I

.field private _data:[Ljava/lang/String;

.field private _extraData:[[B

.field private final _fExtend:Z


# direct methods
.method public constructor <init>(I[BI)V
    .locals 1
    .param p1, "cDataLength"    # I
    .param p2, "buffer"    # [B
    .param p3, "startOffset"    # I

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/model/Sttb;->_fExtend:Z

    .line 59
    iput p1, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    .line 60
    invoke-virtual {p0, p2, p3}, Lorg/apache/poi/hwpf/model/Sttb;->fillFields([BI)V

    .line 61
    return-void
.end method

.method public constructor <init>(I[Ljava/lang/String;)V
    .locals 1
    .param p1, "cDataLength"    # I
    .param p2, "data"    # [Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/model/Sttb;->_fExtend:Z

    .line 65
    iput p1, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    .line 67
    array-length v0, p2

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2, v0}, Lorg/apache/poi/util/ArrayUtil;->copyOf([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    .line 71
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 54
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[BI)V

    .line 55
    return-void
.end method


# virtual methods
.method public fillFields([BI)V
    .locals 7
    .param p1, "buffer"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 75
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    .line 76
    .local v2, "ffff":S
    add-int/lit8 v4, p2, 0x2

    .line 78
    .local v4, "offset":I
    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    .line 81
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    .line 82
    const-string/jumbo v6, "Non-extended character Pascal strings are not supported right now. Please, contact POI developers for update."

    .line 81
    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 87
    :cond_0
    iget v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    .line 89
    .local v0, "cData":I
    :goto_0
    iget v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    add-int/2addr v4, v5

    .line 91
    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v5

    iput v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    .line 92
    add-int/lit8 v4, v4, 0x2

    .line 94
    new-array v5, v0, [Ljava/lang/String;

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    .line 95
    new-array v5, v0, [[B

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    .line 97
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v0, :cond_2

    .line 112
    return-void

    .line 88
    .end local v0    # "cData":I
    .end local v3    # "i":I
    :cond_1
    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    goto :goto_0

    .line 99
    .restart local v0    # "cData":I
    .restart local v3    # "i":I
    :cond_2
    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 100
    .local v1, "cchData":I
    add-int/lit8 v4, v4, 0x2

    .line 102
    if-gez v1, :cond_3

    .line 97
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 105
    :cond_3
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    invoke-static {p1, v4, v1}, Lorg/apache/poi/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 106
    mul-int/lit8 v5, v1, 0x2

    add-int/2addr v4, v5

    .line 108
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    .line 109
    iget v6, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    invoke-static {p1, v4, v6}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v6

    .line 108
    aput-object v6, v5, v3

    .line 110
    iget v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    add-int/2addr v4, v5

    goto :goto_2
.end method

.method public getData()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 6

    .prologue
    .line 130
    const/4 v1, 0x2

    .line 133
    .local v1, "size":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    add-int/2addr v1, v2

    .line 136
    add-int/lit8 v1, v1, 0x2

    .line 141
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 142
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_2

    .line 167
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 169
    iget v2, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v3, v3

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 172
    :cond_1
    return v1

    .line 142
    :cond_2
    aget-object v0, v3, v2

    .line 145
    .local v0, "data":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x2

    .line 147
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v1, v5

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public serialize([BI)I
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 251
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Sttb;->serialize()[B

    move-result-object v0

    .line 252
    .local v0, "bs":[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    array-length v1, v0

    return v1
.end method

.method public serialize()[B
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v8, -0x1

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v7, 0x0

    .line 177
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Sttb;->getSize()I

    move-result v4

    new-array v0, v4, [B

    .line 179
    .local v0, "buffer":[B
    invoke-static {v0, v7, v8}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 181
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v4, v4

    if-nez v4, :cond_3

    .line 183
    :cond_0
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    if-ne v4, v6, :cond_2

    .line 185
    invoke-static {v0, v5, v7}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 186
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    invoke-static {v0, v9, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 246
    :cond_1
    :goto_0
    return-object v0

    .line 190
    :cond_2
    invoke-static {v0, v5, v7}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 191
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    invoke-static {v0, v6, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    goto :goto_0

    .line 196
    :cond_3
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cDataLength:I

    if-ne v4, v6, :cond_5

    .line 198
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v5, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 199
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    invoke-static {v0, v9, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 200
    const/16 v3, 0x8

    .line 209
    .local v3, "offset":I
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 211
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    aget-object v1, v4, v2

    .line 212
    .local v1, "entry":Ljava/lang/String;
    if-nez v1, :cond_6

    .line 215
    aput-byte v8, v0, v3

    .line 216
    add-int/lit8 v4, v3, 0x1

    aput-byte v7, v0, v4

    .line 217
    add-int/lit8 v3, v3, 0x2

    .line 209
    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 204
    .end local v1    # "entry":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "offset":I
    :cond_5
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_data:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v5, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 205
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    invoke-static {v0, v6, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 206
    const/4 v3, 0x6

    .restart local v3    # "offset":I
    goto :goto_1

    .line 223
    .restart local v1    # "entry":Ljava/lang/String;
    .restart local v2    # "i":I
    :cond_6
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v0, v3, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 224
    add-int/lit8 v3, v3, 0x2

    .line 226
    invoke-static {v1, v0, v3}, Lorg/apache/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 227
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 235
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    if-eqz v4, :cond_4

    .line 237
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    aget-object v4, v4, v2

    if-eqz v4, :cond_7

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    aget-object v4, v4, v2

    array-length v4, v4

    if-eqz v4, :cond_7

    .line 239
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    aget-object v4, v4, v2

    .line 240
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/Sttb;->_extraData:[[B

    aget-object v5, v5, v2

    array-length v5, v5

    iget v6, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 239
    invoke-static {v4, v7, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    :cond_7
    iget v4, p0, Lorg/apache/poi/hwpf/model/Sttb;->_cbExtra:I

    add-int/2addr v3, v4

    goto :goto_3
.end method
