.class public final Lorg/apache/poi/hwpf/usermodel/CharacterRun;
.super Lorg/apache/poi/hwpf/usermodel/Range;
.source "CharacterRun.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_BRC:S = 0x6865s

.field public static final SPRM_CHARSCALE:S = 0x4852s

.field public static final SPRM_CPG:S = 0x486bs

.field public static final SPRM_DISPFLDRMARK:S = -0x359es

.field public static final SPRM_DTTMRMARK:S = 0x6805s

.field public static final SPRM_DTTMRMARKDEL:S = 0x6864s

.field public static final SPRM_DXASPACE:S = -0x77c0s

.field public static final SPRM_FBOLD:S = 0x835s

.field public static final SPRM_FCAPS:S = 0x83bs

.field public static final SPRM_FDATA:S = 0x806s

.field public static final SPRM_FDSTRIKE:S = 0x2a53s

.field public static final SPRM_FELID:S = 0x486es

.field public static final SPRM_FEMBOSS:S = 0x858s

.field public static final SPRM_FFLDVANISH:S = 0x802s

.field public static final SPRM_FIMPRINT:S = 0x854s

.field public static final SPRM_FITALIC:S = 0x836s

.field public static final SPRM_FOBJ:S = 0x856s

.field public static final SPRM_FOLE2:S = 0x80as

.field public static final SPRM_FOUTLINE:S = 0x838s

.field public static final SPRM_FRMARK:S = 0x801s

.field public static final SPRM_FRMARKDEL:S = 0x800s

.field public static final SPRM_FSHADOW:S = 0x839s

.field public static final SPRM_FSMALLCAPS:S = 0x83as

.field public static final SPRM_FSPEC:S = 0x855s

.field public static final SPRM_FSTRIKE:S = 0x837s

.field public static final SPRM_FVANISH:S = 0x83cs

.field public static final SPRM_HIGHLIGHT:S = 0x2a0cs

.field public static final SPRM_HPS:S = 0x4a43s

.field public static final SPRM_HPSKERN:S = 0x484bs

.field public static final SPRM_HPSPOS:S = 0x4845s

.field public static final SPRM_IBSTRMARK:S = 0x4804s

.field public static final SPRM_IBSTRMARKDEL:S = 0x4863s

.field public static final SPRM_ICO:S = 0x2a42s

.field public static final SPRM_IDCTHINT:S = 0x286fs

.field public static final SPRM_IDSIRMARKDEL:S = 0x4867s

.field public static final SPRM_ISS:S = 0x2a48s

.field public static final SPRM_ISTD:S = 0x4a30s

.field public static final SPRM_KUL:S = 0x2a3es

.field public static final SPRM_LID:S = 0x4a41s

.field public static final SPRM_NONFELID:S = 0x486ds

.field public static final SPRM_OBJLOCATION:S = 0x680es

.field public static final SPRM_PICLOCATION:S = 0x6a03s

.field public static final SPRM_PROPRMARK:S = -0x35a9s

.field public static final SPRM_RGFTCASCII:S = 0x4a4fs

.field public static final SPRM_RGFTCFAREAST:S = 0x4a50s

.field public static final SPRM_RGFTCNOTFAREAST:S = 0x4a51s

.field public static final SPRM_SFXTEXT:S = 0x2859s

.field public static final SPRM_SHD:S = 0x4866s

.field public static final SPRM_SYMBOL:S = 0x6a09s

.field public static final SPRM_YSRI:S = 0x484es


# instance fields
.field _chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

.field _props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;


# direct methods
.method constructor <init>(Lorg/apache/poi/hwpf/model/CHPX;Lorg/apache/poi/hwpf/model/StyleSheet;SLorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "chpx"    # Lorg/apache/poi/hwpf/model/CHPX;
    .param p2, "ss"    # Lorg/apache/poi/hwpf/model/StyleSheet;
    .param p3, "istd"    # S
    .param p4, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 100
    iget v0, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 101
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v2

    .line 100
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 101
    invoke-direct {p0, v0, v1, p4}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 102
    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/hwpf/model/CHPX;->getCharacterProperties(Lorg/apache/poi/hwpf/model/StyleSheet;S)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .line 103
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/CHPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 104
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 515
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 516
    .local v0, "cp":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    iget-object v2, v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 517
    iget-object v2, v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMarkDel()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v1

    .line 518
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 517
    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmRMarkDel(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 519
    iget-object v2, v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v1

    .line 520
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 519
    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 521
    iget-object v2, v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .line 522
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmDispFldRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 521
    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmDispFldRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 523
    iget-object v2, v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getXstDispFldRMark()[B

    move-result-object v1

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setXstDispFldRMark([B)V

    .line 524
    iget-object v1, v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 526
    return-object v0
.end method

.method public cloneProperties()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 504
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    return-object v0
.end method

.method public getBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getBrc()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getCharacterSpacing()I
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDxaSpace()I

    move-result v0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIco()B

    move-result v0

    return v0
.end method

.method public getDropDownListDefaultItemIndex()Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 614
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v3, :cond_0

    .line 615
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_text:Ljava/lang/StringBuilder;

    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_start:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 616
    .local v0, "c":C
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 617
    new-instance v1, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;

    .line 618
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/HWPFDocument;->getDataStream()[B

    move-result-object v3

    .line 619
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v4

    .line 617
    invoke-direct {v1, v3, v4}, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;-><init>([BI)V

    .line 620
    .local v1, "data":Lorg/apache/poi/hwpf/model/NilPICFAndBinData;
    new-instance v2, Lorg/apache/poi/hwpf/model/FFData;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->getBinData()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hwpf/model/FFData;-><init>([BI)V

    .line 622
    .local v2, "ffData":Lorg/apache/poi/hwpf/model/FFData;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FFData;->getDefaultDropDownItemIndex()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 625
    .end local v0    # "c":C
    .end local v1    # "data":Lorg/apache/poi/hwpf/model/NilPICFAndBinData;
    .end local v2    # "ffData":Lorg/apache/poi/hwpf/model/FFData;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getDropDownListValues()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 598
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_0

    .line 599
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_text:Ljava/lang/StringBuilder;

    iget v5, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_start:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 600
    .local v0, "c":C
    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    .line 601
    new-instance v1, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;

    .line 602
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocument;->getDataStream()[B

    move-result-object v4

    .line 603
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v5

    .line 601
    invoke-direct {v1, v4, v5}, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;-><init>([BI)V

    .line 604
    .local v1, "data":Lorg/apache/poi/hwpf/model/NilPICFAndBinData;
    new-instance v2, Lorg/apache/poi/hwpf/model/FFData;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->getBinData()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lorg/apache/poi/hwpf/model/FFData;-><init>([BI)V

    .line 606
    .local v2, "ffData":Lorg/apache/poi/hwpf/model/FFData;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FFData;->getDropList()[Ljava/lang/String;

    move-result-object v3

    .line 610
    .end local v0    # "c":C
    .end local v1    # "data":Lorg/apache/poi/hwpf/model/NilPICFAndBinData;
    .end local v2    # "ffData":Lorg/apache/poi/hwpf/model/FFData;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getFontTable()Lorg/apache/poi/hwpf/model/FontTable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 403
    const/4 v0, 0x0

    .line 405
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getFontTable()Lorg/apache/poi/hwpf/model/FontTable;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcAscii()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FontTable;->getMainFont(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v0

    return v0
.end method

.method public getHighlightedColor()B
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIcoHighlight()B

    move-result v0

    return v0
.end method

.method public getIco24()I
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIco24()I

    move-result v0

    return v0
.end method

.method public getKerning()I
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsKern()I

    move-result v0

    return v0
.end method

.method public getLanguageCode()I
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getLidDefault()I

    move-result v0

    return v0
.end method

.method public getObjOffset()I
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcObj()I

    move-result v0

    return v0
.end method

.method public getPicOffset()I
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcPic()I

    move-result v0

    return v0
.end method

.method public getShading()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getStyleIndex()I
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIstd()I

    move-result v0

    return v0
.end method

.method public getSubSuperScriptIndex()S
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIss()B

    move-result v0

    return v0
.end method

.method public getSymbolCharacter()C
    .locals 2

    .prologue
    .line 552
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSymbol()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getXchSym()I

    move-result v0

    int-to-char v0, v0

    return v0

    .line 555
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not a symbol CharacterRun"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSymbolFont()Lorg/apache/poi/hwpf/model/Ffn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 568
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSymbol()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 569
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getFontTable()Lorg/apache/poi/hwpf/model/FontTable;

    move-result-object v2

    if-nez v2, :cond_1

    .line 579
    :cond_0
    :goto_0
    return-object v1

    .line 573
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getFontTable()Lorg/apache/poi/hwpf/model/FontTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FontTable;->getFontNames()[Lorg/apache/poi/hwpf/model/Ffn;

    move-result-object v0

    .line 576
    .local v0, "fontNames":[Lorg/apache/poi/hwpf/model/Ffn;
    array-length v2, v0

    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcSym()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 579
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcSym()I

    move-result v1

    aget-object v1, v0, v1

    goto :goto_0

    .line 581
    .end local v0    # "fontNames":[Lorg/apache/poi/hwpf/model/Ffn;
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Not a symbol CharacterRun"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getUnderlineCode()I
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getKul()B

    move-result v0

    return v0
.end method

.method public getVerticalOffset()I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v0

    return v0
.end method

.method public isBold()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFBold()Z

    move-result v0

    return v0
.end method

.method public isCapitalized()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFCaps()Z

    move-result v0

    return v0
.end method

.method public isData()Z
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFData()Z

    move-result v0

    return v0
.end method

.method public isDoubleStrikeThrough()Z
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFDStrike()Z

    move-result v0

    return v0
.end method

.method public isEmbossed()Z
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFEmboss()Z

    move-result v0

    return v0
.end method

.method public isFldVanished()Z
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFFldVanish()Z

    move-result v0

    return v0
.end method

.method public isHighlighted()Z
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFHighlight()Z

    move-result v0

    return v0
.end method

.method public isImprinted()Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFImprint()Z

    move-result v0

    return v0
.end method

.method public isItalic()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFItalic()Z

    move-result v0

    return v0
.end method

.method public isMarkedDeleted()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMarkDel()Z

    move-result v0

    return v0
.end method

.method public isMarkedInserted()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMark()Z

    move-result v0

    return v0
.end method

.method public isObj()Z
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFObj()Z

    move-result v0

    return v0
.end method

.method public isOle2()Z
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOle2()Z

    move-result v0

    return v0
.end method

.method public isOutlined()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOutline()Z

    move-result v0

    return v0
.end method

.method public isShadowed()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFShadow()Z

    move-result v0

    return v0
.end method

.method public isSmallCaps()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSmallCaps()Z

    move-result v0

    return v0
.end method

.method public isSpecialCharacter()Z
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v0

    return v0
.end method

.method public isStrikeThrough()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFStrike()Z

    move-result v0

    return v0
.end method

.method public isSymbol()Z
    .locals 2

    .prologue
    .line 540
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVanished()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFVanish()Z

    move-result v0

    return v0
.end method

.method public markDeleted(Z)V
    .locals 3
    .param p1, "mark"    # Z

    .prologue
    .line 120
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFRMarkDel(Z)V

    .line 122
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 123
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x800

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 125
    return-void

    .line 122
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public markInserted(Z)V
    .locals 3
    .param p1, "mark"    # Z

    .prologue
    .line 216
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFRMark(Z)V

    .line 218
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 219
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x801

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 221
    return-void

    .line 218
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBold(Z)V
    .locals 3
    .param p1, "bold"    # Z

    .prologue
    .line 132
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFBold(Z)V

    .line 134
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 135
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x835

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 137
    return-void

    .line 134
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCapitalized(Z)V
    .locals 3
    .param p1, "caps"    # Z

    .prologue
    .line 192
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFCaps(Z)V

    .line 194
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 195
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x83b

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 197
    return-void

    .line 194
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCharacterSpacing(I)V
    .locals 2
    .param p1, "twips"    # I

    .prologue
    .line 320
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDxaSpace(I)V

    .line 322
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x77c0

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 324
    return-void
.end method

.method public setColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIco(B)V

    .line 352
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2a42

    int-to-byte v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 353
    return-void
.end method

.method public setData(Z)V
    .locals 3
    .param p1, "data"    # Z

    .prologue
    .line 455
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFData(Z)V

    .line 457
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 458
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x856

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 459
    return-void

    .line 457
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDoubleStrikethrough(Z)V
    .locals 3
    .param p1, "dstrike"    # Z

    .prologue
    .line 276
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFDStrike(Z)V

    .line 278
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 279
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x2a53

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 281
    return-void

    .line 278
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEmbossed(Z)V
    .locals 3
    .param p1, "emboss"    # Z

    .prologue
    .line 252
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFEmboss(Z)V

    .line 254
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 255
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x858

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 257
    return-void

    .line 254
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setFldVanish(Z)V
    .locals 3
    .param p1, "fldVanish"    # Z

    .prologue
    .line 168
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFFldVanish(Z)V

    .line 170
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 171
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x802

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 173
    return-void

    .line 170
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setFontSize(I)V
    .locals 3
    .param p1, "halfPoints"    # I

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    .line 311
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4a43

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 313
    return-void
.end method

.method public setFtcAscii(I)V
    .locals 3
    .param p1, "ftcAscii"    # I

    .prologue
    .line 284
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcAscii(I)V

    .line 286
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4a4f

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 288
    return-void
.end method

.method public setFtcFE(I)V
    .locals 3
    .param p1, "ftcFE"    # I

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcFE(I)V

    .line 293
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4a50

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 295
    return-void
.end method

.method public setFtcOther(I)V
    .locals 3
    .param p1, "ftcOther"    # I

    .prologue
    .line 298
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcOther(I)V

    .line 300
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4a51

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 302
    return-void
.end method

.method public setHighlighted(B)V
    .locals 2
    .param p1, "color"    # B

    .prologue
    .line 382
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFHighlight(Z)V

    .line 383
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIcoHighlight(B)V

    .line 384
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2a0c

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 385
    return-void
.end method

.method public setIco24(I)V
    .locals 1
    .param p1, "colour24"    # I

    .prologue
    .line 492
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIco24(I)V

    .line 493
    return-void
.end method

.method public setImprinted(Z)V
    .locals 3
    .param p1, "imprint"    # Z

    .prologue
    .line 264
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFImprint(Z)V

    .line 266
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 267
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x854

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 269
    return-void

    .line 266
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setItalic(Z)V
    .locals 3
    .param p1, "italic"    # Z

    .prologue
    .line 144
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFItalic(Z)V

    .line 146
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 147
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x836

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 149
    return-void

    .line 146
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setKerning(I)V
    .locals 3
    .param p1, "kern"    # I

    .prologue
    .line 369
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHpsKern(I)V

    .line 370
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x484b

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 371
    return-void
.end method

.method public setObj(Z)V
    .locals 3
    .param p1, "obj"    # Z

    .prologue
    .line 430
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFObj(Z)V

    .line 432
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 433
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x856

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 434
    return-void

    .line 432
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setObjOffset(I)V
    .locals 2
    .param p1, "obj"    # I

    .prologue
    .line 477
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFcObj(I)V

    .line 478
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x680e

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 479
    return-void
.end method

.method public setOle2(Z)V
    .locals 3
    .param p1, "ole"    # Z

    .prologue
    .line 466
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFOle2(Z)V

    .line 468
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 469
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x856

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 470
    return-void

    .line 468
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutline(Z)V
    .locals 3
    .param p1, "outlined"    # Z

    .prologue
    .line 156
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFOutline(Z)V

    .line 158
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 159
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x838

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 161
    return-void

    .line 158
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPicOffset(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 441
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFcPic(I)V

    .line 442
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6a03

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 443
    return-void
.end method

.method public setShading(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 3
    .param p1, "shd"    # Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .prologue
    .line 393
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 395
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4866

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->serialize()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->addSprm(S[B)V

    .line 396
    return-void
.end method

.method public setShadow(Z)V
    .locals 3
    .param p1, "shadow"    # Z

    .prologue
    .line 240
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFShadow(Z)V

    .line 242
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 243
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x839

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 245
    return-void

    .line 242
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSmallCaps(Z)V
    .locals 3
    .param p1, "smallCaps"    # Z

    .prologue
    .line 180
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSmallCaps(Z)V

    .line 182
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 183
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x83a

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 185
    return-void

    .line 182
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSpecialCharacter(Z)V
    .locals 3
    .param p1, "spec"    # Z

    .prologue
    .line 419
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSpec(Z)V

    .line 421
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 422
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x855

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 423
    return-void

    .line 421
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSubSuperScriptIndex(S)V
    .locals 2
    .param p1, "iss"    # S

    .prologue
    .line 331
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDxaSpace(I)V

    .line 333
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x77c0

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 335
    return-void
.end method

.method public setUnderlineCode(I)V
    .locals 3
    .param p1, "kul"    # I

    .prologue
    .line 342
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setKul(B)V

    .line 343
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2a3e

    int-to-byte v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 344
    return-void
.end method

.method public setVanished(Z)V
    .locals 3
    .param p1, "vanish"    # Z

    .prologue
    .line 204
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFVanish(Z)V

    .line 206
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 207
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x83c

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 209
    return-void

    .line 206
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVerticalOffset(I)V
    .locals 3
    .param p1, "hpsPos"    # I

    .prologue
    .line 360
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHpsPos(S)V

    .line 361
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4845

    int-to-byte v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 362
    return-void
.end method

.method public strikeThrough(Z)V
    .locals 3
    .param p1, "strike"    # Z

    .prologue
    .line 228
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_props:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFStrike(Z)V

    .line 230
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v0, v1

    .line 231
    .local v0, "newVal":B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->_chpx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x837

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 233
    return-void

    .line 230
    .end local v0    # "newVal":B
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 593
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v0

    .line 594
    .local v0, "text":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "CharacterRun of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " characters - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public type()I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    return v0
.end method
