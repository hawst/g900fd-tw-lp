.class public final enum Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
.super Ljava/lang/Enum;
.source "FieldsDocumentPart.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hwpf/model/FieldsDocumentPart;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final enum ANNOTATIONS:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field public static final enum ENDNOTES:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field public static final enum FOOTNOTES:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field public static final enum HEADER:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field public static final enum HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field public static final enum MAIN:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

.field public static final enum TEXTBOX:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;


# instance fields
.field private final fibFieldsField:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 24
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "ANNOTATIONS"

    .line 27
    const/16 v2, 0x13

    invoke-direct {v0, v1, v4, v2}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->ANNOTATIONS:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 29
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "ENDNOTES"

    .line 32
    const/16 v2, 0x30

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->ENDNOTES:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 34
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "FOOTNOTES"

    .line 37
    const/16 v2, 0x12

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->FOOTNOTES:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 39
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "HEADER"

    .line 42
    const/16 v2, 0x11

    invoke-direct {v0, v1, v7, v2}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->HEADER:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 44
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "HEADER_TEXTBOX"

    .line 47
    const/16 v2, 0x3b

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 49
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "MAIN"

    const/4 v2, 0x5

    .line 52
    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 54
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    const-string/jumbo v1, "TEXTBOX"

    const/4 v2, 0x6

    .line 57
    const/16 v3, 0x39

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->TEXTBOX:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    sget-object v1, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->ANNOTATIONS:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->ENDNOTES:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->FOOTNOTES:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->HEADER:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v1, v0, v7

    sget-object v1, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->TEXTBOX:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->ENUM$VALUES:[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "fibHandlerField"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput p3, p0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->fibFieldsField:I

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->ENUM$VALUES:[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getFibFieldsField()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->fibFieldsField:I

    return v0
.end method
