.class public final Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
.super Lorg/apache/poi/hwpf/model/types/CHPAbstractType;
.source "CharacterProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_BRC:S = 0x6865s

.field public static final SPRM_CCV:S = 0x6870s

.field public static final SPRM_CHARSCALE:S = 0x4852s

.field public static final SPRM_CPG:S = 0x486bs

.field public static final SPRM_DISPFLDRMARK:S = -0x359es

.field public static final SPRM_DTTMRMARK:S = 0x6805s

.field public static final SPRM_DTTMRMARKDEL:S = 0x6864s

.field public static final SPRM_DXASPACE:S = -0x77c0s

.field public static final SPRM_FBOLD:S = 0x835s

.field public static final SPRM_FCAPS:S = 0x83bs

.field public static final SPRM_FDATA:S = 0x806s

.field public static final SPRM_FDSTRIKE:S = 0x2a53s

.field public static final SPRM_FELID:S = 0x486es

.field public static final SPRM_FEMBOSS:S = 0x858s

.field public static final SPRM_FFLDVANISH:S = 0x802s

.field public static final SPRM_FIMPRINT:S = 0x854s

.field public static final SPRM_FITALIC:S = 0x836s

.field public static final SPRM_FOBJ:S = 0x856s

.field public static final SPRM_FOLE2:S = 0x80as

.field public static final SPRM_FOUTLINE:S = 0x838s

.field public static final SPRM_FRMARK:S = 0x801s

.field public static final SPRM_FRMARKDEL:S = 0x800s

.field public static final SPRM_FSHADOW:S = 0x839s

.field public static final SPRM_FSMALLCAPS:S = 0x83as

.field public static final SPRM_FSPEC:S = 0x855s

.field public static final SPRM_FSTRIKE:S = 0x837s

.field public static final SPRM_FVANISH:S = 0x83cs

.field public static final SPRM_HIGHLIGHT:S = 0x2a0cs

.field public static final SPRM_HPS:S = 0x4a43s

.field public static final SPRM_HPSKERN:S = 0x484bs

.field public static final SPRM_HPSPOS:S = 0x4845s

.field public static final SPRM_IBSTRMARK:S = 0x4804s

.field public static final SPRM_IBSTRMARKDEL:S = 0x4863s

.field public static final SPRM_ICO:S = 0x2a42s

.field public static final SPRM_IDCTHINT:S = 0x286fs

.field public static final SPRM_IDSIRMARKDEL:S = 0x4867s

.field public static final SPRM_ISS:S = 0x2a48s

.field public static final SPRM_ISTD:S = 0x4a30s

.field public static final SPRM_KUL:S = 0x2a3es

.field public static final SPRM_LID:S = 0x4a41s

.field public static final SPRM_NONFELID:S = 0x486ds

.field public static final SPRM_OBJLOCATION:S = 0x680es

.field public static final SPRM_PICLOCATION:S = 0x6a03s

.field public static final SPRM_PROPRMARK:S = -0x35a9s

.field public static final SPRM_RGFTCASCII:S = 0x4a4fs

.field public static final SPRM_RGFTCFAREAST:S = 0x4a50s

.field public static final SPRM_RGFTCNOTFAREAST:S = 0x4a51s

.field public static final SPRM_SFXTEXT:S = 0x2859s

.field public static final SPRM_SHD:S = 0x4866s

.field public static final SPRM_SYMBOL:S = 0x6a09s

.field public static final SPRM_YSRI:S = 0x484es


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;-><init>()V

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFUsePgsuSettings(Z)V

    .line 91
    const/16 v0, 0x24

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setXstDispFldRMark([B)V

    .line 92
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 4

    .prologue
    .line 377
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .line 379
    .local v0, "cp":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Colorref;->clone()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setCv(Lorg/apache/poi/hwpf/model/Colorref;)V

    .line 380
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 381
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMarkDel()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmRMarkDel(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 382
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 383
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmDispFldRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmDispFldRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 384
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getXstDispFldRMark()[B

    move-result-object v2

    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setXstDispFldRMark([B)V

    .line 385
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 386
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getBrc()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setBrc(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    return-object v0

    .line 390
    .end local v0    # "cp":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    :catch_0
    move-exception v1

    .line 392
    .local v1, "exc":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    .line 393
    const-string/jumbo v3, "Impossible CloneNotSupportedException occured"

    .line 392
    invoke-direct {v2, v3, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getCharacterSpacing()I
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDxaSpace()I

    move-result v0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 277
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIco()B

    move-result v0

    return v0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v0

    return v0
.end method

.method public getIco24()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 320
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Colorref;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 321
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Colorref;->getValue()I

    move-result v0

    .line 362
    :goto_0
    :pswitch_0
    return v0

    .line 324
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIco()B

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 329
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 331
    :pswitch_2
    const/high16 v0, 0xff0000

    goto :goto_0

    .line 333
    :pswitch_3
    const v0, 0xffff00

    goto :goto_0

    .line 335
    :pswitch_4
    const v0, 0xff00

    goto :goto_0

    .line 337
    :pswitch_5
    const v0, 0xff00ff

    goto :goto_0

    .line 339
    :pswitch_6
    const/16 v0, 0xff

    goto :goto_0

    .line 341
    :pswitch_7
    const v0, 0xffff

    goto :goto_0

    .line 343
    :pswitch_8
    const v0, 0xffffff

    goto :goto_0

    .line 345
    :pswitch_9
    const/high16 v0, 0x800000

    goto :goto_0

    .line 347
    :pswitch_a
    const v0, 0x808000

    goto :goto_0

    .line 349
    :pswitch_b
    const v0, 0x8000

    goto :goto_0

    .line 351
    :pswitch_c
    const v0, 0x800080

    goto :goto_0

    .line 353
    :pswitch_d
    const/16 v0, 0x80

    goto :goto_0

    .line 355
    :pswitch_e
    const v0, 0x8080

    goto :goto_0

    .line 357
    :pswitch_f
    const v0, 0x808080

    goto :goto_0

    .line 359
    :pswitch_10
    const v0, 0xc0c0c0

    goto :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getKerning()I
    .locals 1

    .prologue
    .line 297
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsKern()I

    move-result v0

    return v0
.end method

.method public getSubSuperScriptIndex()S
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIss()B

    move-result v0

    return v0
.end method

.method public getUnderlineCode()I
    .locals 1

    .prologue
    .line 267
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getKul()B

    move-result v0

    return v0
.end method

.method public getVerticalOffset()I
    .locals 1

    .prologue
    .line 287
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    move-result v0

    return v0
.end method

.method public isBold()Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFBold()Z

    move-result v0

    return v0
.end method

.method public isCapitalized()Z
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFCaps()Z

    move-result v0

    return v0
.end method

.method public isDoubleStrikeThrough()Z
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFDStrike()Z

    move-result v0

    return v0
.end method

.method public isEmbossed()Z
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFEmboss()Z

    move-result v0

    return v0
.end method

.method public isFldVanished()Z
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFFldVanish()Z

    move-result v0

    return v0
.end method

.method public isHighlighted()Z
    .locals 1

    .prologue
    .line 307
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFHighlight()Z

    move-result v0

    return v0
.end method

.method public isImprinted()Z
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFImprint()Z

    move-result v0

    return v0
.end method

.method public isItalic()Z
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFItalic()Z

    move-result v0

    return v0
.end method

.method public isMarkedDeleted()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMarkDel()Z

    move-result v0

    return v0
.end method

.method public isMarkedInserted()Z
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMark()Z

    move-result v0

    return v0
.end method

.method public isOutlined()Z
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOutline()Z

    move-result v0

    return v0
.end method

.method public isShadowed()Z
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFShadow()Z

    move-result v0

    return v0
.end method

.method public isSmallCaps()Z
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSmallCaps()Z

    move-result v0

    return v0
.end method

.method public isStrikeThrough()Z
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFStrike()Z

    move-result v0

    return v0
.end method

.method public isVanished()Z
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFVanish()Z

    move-result v0

    return v0
.end method

.method public markDeleted(Z)V
    .locals 0
    .param p1, "mark"    # Z

    .prologue
    .line 101
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFRMarkDel(Z)V

    .line 102
    return-void
.end method

.method public markInserted(Z)V
    .locals 0
    .param p1, "mark"    # Z

    .prologue
    .line 181
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFRMark(Z)V

    .line 182
    return-void
.end method

.method public setBold(Z)V
    .locals 0
    .param p1, "bold"    # Z

    .prologue
    .line 111
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFBold(Z)V

    .line 112
    return-void
.end method

.method public setCapitalized(Z)V
    .locals 0
    .param p1, "caps"    # Z

    .prologue
    .line 161
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFCaps(Z)V

    .line 162
    return-void
.end method

.method public setCharacterSpacing(I)V
    .locals 0
    .param p1, "twips"    # I

    .prologue
    .line 252
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 253
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 282
    int-to-byte v0, p1

    invoke-super {p0, v0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setIco(B)V

    .line 283
    return-void
.end method

.method public setDoubleStrikeThrough(Z)V
    .locals 0
    .param p1, "dstrike"    # Z

    .prologue
    .line 232
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFDStrike(Z)V

    .line 233
    return-void
.end method

.method public setEmbossed(Z)V
    .locals 0
    .param p1, "emboss"    # Z

    .prologue
    .line 212
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFEmboss(Z)V

    .line 213
    return-void
.end method

.method public setFldVanish(Z)V
    .locals 0
    .param p1, "fldVanish"    # Z

    .prologue
    .line 141
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFFldVanish(Z)V

    .line 142
    return-void
.end method

.method public setFontSize(I)V
    .locals 0
    .param p1, "halfPoints"    # I

    .prologue
    .line 242
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 243
    return-void
.end method

.method public setHighlighted(B)V
    .locals 0
    .param p1, "color"    # B

    .prologue
    .line 312
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setIcoHighlight(B)V

    .line 313
    return-void
.end method

.method public setIco24(I)V
    .locals 2
    .param p1, "colour24"    # I

    .prologue
    .line 370
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xffffff

    and-int/2addr v1, p1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setCv(Lorg/apache/poi/hwpf/model/Colorref;)V

    .line 371
    return-void
.end method

.method public setImprinted(Z)V
    .locals 0
    .param p1, "imprint"    # Z

    .prologue
    .line 222
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFImprint(Z)V

    .line 223
    return-void
.end method

.method public setItalic(Z)V
    .locals 0
    .param p1, "italic"    # Z

    .prologue
    .line 121
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFItalic(Z)V

    .line 122
    return-void
.end method

.method public setKerning(I)V
    .locals 0
    .param p1, "kern"    # I

    .prologue
    .line 302
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setHpsKern(I)V

    .line 303
    return-void
.end method

.method public setOutline(Z)V
    .locals 0
    .param p1, "outlined"    # Z

    .prologue
    .line 131
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFOutline(Z)V

    .line 132
    return-void
.end method

.method public setShadow(Z)V
    .locals 0
    .param p1, "shadow"    # Z

    .prologue
    .line 201
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFShadow(Z)V

    .line 203
    return-void
.end method

.method public setSmallCaps(Z)V
    .locals 0
    .param p1, "smallCaps"    # Z

    .prologue
    .line 151
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFSmallCaps(Z)V

    .line 152
    return-void
.end method

.method public setSubSuperScriptIndex(S)V
    .locals 0
    .param p1, "iss"    # S

    .prologue
    .line 262
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 263
    return-void
.end method

.method public setUnderlineCode(I)V
    .locals 1
    .param p1, "kul"    # I

    .prologue
    .line 272
    int-to-byte v0, p1

    invoke-super {p0, v0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setKul(B)V

    .line 273
    return-void
.end method

.method public setVanished(Z)V
    .locals 0
    .param p1, "vanish"    # Z

    .prologue
    .line 171
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFVanish(Z)V

    .line 173
    return-void
.end method

.method public setVerticalOffset(I)V
    .locals 1
    .param p1, "hpsPos"    # I

    .prologue
    .line 292
    int-to-short v0, p1

    invoke-super {p0, v0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setHpsPos(S)V

    .line 293
    return-void
.end method

.method public strikeThrough(Z)V
    .locals 0
    .param p1, "strike"    # Z

    .prologue
    .line 191
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->setFStrike(Z)V

    .line 192
    return-void
.end method
