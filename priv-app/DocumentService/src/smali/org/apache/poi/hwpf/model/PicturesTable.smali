.class public final Lorg/apache/poi/hwpf/model/PicturesTable;
.super Ljava/lang/Object;
.source "PicturesTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field static final BLOCK_TYPE_OFFSET:I = 0xe

.field static final MM_MODE_TYPE_OFFSET:I = 0x6

.field static final TYPE_HORIZONTAL_LINE:I = 0xe

.field static final TYPE_IMAGE:I = 0x8

.field static final TYPE_IMAGE_PASTED_FROM_CLIPBOARD:I = 0xa

.field static final TYPE_IMAGE_PASTED_FROM_CLIPBOARD_WORD2000:I = 0x2

.field static final TYPE_IMAGE_WORD2000:I

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _dataStream:[B

.field private _dgg:Lorg/apache/poi/hwpf/model/EscherRecordHolder;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private _document:Lorg/apache/poi/hwpf/HWPFDocument;

.field private _mainStream:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lorg/apache/poi/hwpf/model/PicturesTable;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 57
    sput-object v0, Lorg/apache/poi/hwpf/model/PicturesTable;->logger:Lorg/apache/poi/util/POILogger;

    .line 66
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/HWPFDocument;[B[B)V
    .locals 0
    .param p1, "_document"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "_dataStream"    # [B
    .param p3, "_mainStream"    # [B

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_document:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 99
    iput-object p2, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    .line 100
    iput-object p3, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_mainStream:[B

    .line 101
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/HWPFDocument;[B[BLorg/apache/poi/hwpf/model/FSPATable;Lorg/apache/poi/hwpf/model/EscherRecordHolder;)V
    .locals 0
    .param p1, "_document"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "_dataStream"    # [B
    .param p3, "_mainStream"    # [B
    .param p4, "fspa"    # Lorg/apache/poi/hwpf/model/FSPATable;
    .param p5, "dgg"    # Lorg/apache/poi/hwpf/model/EscherRecordHolder;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_document:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 89
    iput-object p2, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    .line 90
    iput-object p3, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_mainStream:[B

    .line 92
    iput-object p5, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dgg:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    .line 93
    return-void
.end method

.method private static getBlockType([BI)S
    .locals 1
    .param p0, "dataStream"    # [B
    .param p1, "pictOffset"    # I

    .prologue
    .line 144
    add-int/lit8 v0, p1, 0xe

    invoke-static {p0, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method private static getMmMode([BI)S
    .locals 1
    .param p0, "dataStream"    # [B
    .param p1, "pictOffset"    # I

    .prologue
    .line 148
    add-int/lit8 v0, p1, 0x6

    invoke-static {p0, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method private isBlockContainsHorizontalLine(I)Z
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    invoke-static {v0, p1}, Lorg/apache/poi/hwpf/model/PicturesTable;->getBlockType([BI)S

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    invoke-static {v0, p1}, Lorg/apache/poi/hwpf/model/PicturesTable;->getMmMode([BI)S

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isBlockContainsImage(I)Z
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    invoke-static {v0, p1}, Lorg/apache/poi/hwpf/model/PicturesTable;->getBlockType([BI)S

    move-result v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    invoke-static {v1, p1}, Lorg/apache/poi/hwpf/model/PicturesTable;->getMmMode([BI)S

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hwpf/model/PicturesTable;->isPictureRecognized(SS)Z

    move-result v0

    return v0
.end method

.method private isPictureRecognized(SS)Z
    .locals 2
    .param p1, "blockType"    # S
    .param p2, "mappingModeOfMETAFILEPICT"    # S

    .prologue
    const/16 v1, 0x64

    .line 140
    const/16 v0, 0x8

    if-eq p1, v0, :cond_2

    const/16 v0, 0xa

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_0

    if-eq p2, v1, :cond_2

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    if-eq p2, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private searchForPictures(Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Picture;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .local p2, "pictures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Picture;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 214
    return-void

    .line 175
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 176
    .local v3, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v8, v3, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v8, :cond_1

    move-object v2, v3

    .line 177
    check-cast v2, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 178
    .local v2, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v1

    .line 179
    .local v1, "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    if-eqz v1, :cond_2

    .line 181
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/Picture;

    invoke-direct {v8, v1}, Lorg/apache/poi/hwpf/usermodel/Picture;-><init>(Lorg/apache/poi/ddf/EscherBlipRecord;)V

    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    .end local v1    # "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    .end local v2    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    :cond_1
    :goto_1
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v8

    invoke-direct {p0, v8, p2}, Lorg/apache/poi/hwpf/model/PicturesTable;->searchForPictures(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 183
    .restart local v1    # "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    .restart local v2    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    :cond_2
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v8

    if-lez v8, :cond_1

    .line 189
    :try_start_0
    new-instance v6, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v6}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 191
    .local v6, "recordFactory":Lorg/apache/poi/ddf/EscherRecordFactory;
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_mainStream:[B

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v9

    .line 190
    invoke-interface {v6, v8, v9}, Lorg/apache/poi/ddf/EscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    .line 193
    .local v5, "record":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v8, v5, Lorg/apache/poi/ddf/EscherBlipRecord;

    if-eqz v8, :cond_1

    .line 195
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_mainStream:[B

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v9

    invoke-virtual {v5, v8, v9, v6}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 197
    move-object v0, v5

    check-cast v0, Lorg/apache/poi/ddf/EscherBlipRecord;

    move-object v1, v0

    .line 198
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/Picture;

    invoke-direct {v8, v1}, Lorg/apache/poi/hwpf/usermodel/Picture;-><init>(Lorg/apache/poi/ddf/EscherBlipRecord;)V

    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 201
    .end local v5    # "record":Lorg/apache/poi/ddf/EscherRecord;
    .end local v6    # "recordFactory":Lorg/apache/poi/ddf/EscherRecordFactory;
    :catch_0
    move-exception v4

    .line 203
    .local v4, "exc":Ljava/lang/Exception;
    sget-object v8, Lorg/apache/poi/hwpf/model/PicturesTable;->logger:Lorg/apache/poi/util/POILogger;

    .line 204
    const/4 v9, 0x5

    .line 205
    const-string/jumbo v10, "Unable to load picture from BLIB record at offset #"

    .line 206
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 203
    invoke-virtual {v8, v9, v10, v11, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;
    .locals 3
    .param p1, "run"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p2, "fillBytes"    # Z

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasHorizontalLine(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    :cond_0
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Picture;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v1

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dataStream:[B

    invoke-direct {v0, v1, v2, p2}, Lorg/apache/poi/hwpf/usermodel/Picture;-><init>(I[BZ)V

    .line 164
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAllPictures()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Picture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v2, "pictures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hwpf/usermodel/Picture;>;"
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_document:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/HWPFDocument;->getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    .line 226
    .local v3, "range":Lorg/apache/poi/hwpf/usermodel/Range;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v5

    if-lt v0, v5, :cond_0

    .line 239
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PicturesTable;->_dgg:Lorg/apache/poi/hwpf/model/EscherRecordHolder;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/EscherRecordHolder;->getEscherRecords()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5, v2}, Lorg/apache/poi/hwpf/model/PicturesTable;->searchForPictures(Ljava/util/List;Ljava/util/List;)V

    .line 241
    return-object v2

    .line 227
    :cond_0
    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v4

    .line 229
    .local v4, "run":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    if-nez v4, :cond_2

    .line 226
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lorg/apache/poi/hwpf/model/PicturesTable;->extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;

    move-result-object v1

    .line 234
    .local v1, "picture":Lorg/apache/poi/hwpf/usermodel/Picture;
    if-eqz v1, :cond_1

    .line 235
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public hasEscherPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z
    .locals 2
    .param p1, "run"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    .line 122
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isObj()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isOle2()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isData()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\u0008"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x1

    .line 125
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHorizontalLine(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z
    .locals 2
    .param p1, "run"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    .line 133
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "\u0001"

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/model/PicturesTable;->isBlockContainsHorizontalLine(I)Z

    move-result v0

    .line 136
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z
    .locals 3
    .param p1, "run"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    const/4 v0, 0x0

    .line 108
    if-nez p1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isObj()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isOle2()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isData()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    const-string/jumbo v1, "\u0001"

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "\u0001\u0015"

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/model/PicturesTable;->isBlockContainsImage(I)Z

    move-result v0

    goto :goto_0
.end method
