.class public Lorg/apache/poi/hwpf/usermodel/NotesImpl;
.super Ljava/lang/Object;
.source "NotesImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/Notes;


# instance fields
.field private anchorToIndexMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final notesTables:Lorg/apache/poi/hwpf/model/NotesTables;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/NotesTables;)V
    .locals 1
    .param p1, "notesTables"    # Lorg/apache/poi/hwpf/model/NotesTables;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->anchorToIndexMap:Ljava/util/Map;

    .line 37
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    .line 38
    return-void
.end method

.method private updateAnchorToIndexMap()V
    .locals 5

    .prologue
    .line 74
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->anchorToIndexMap:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 84
    :goto_0
    return-void

    .line 77
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 78
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/NotesTables;->getDescriptorsCount()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 83
    iput-object v2, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->anchorToIndexMap:Ljava/util/Map;

    goto :goto_0

    .line 80
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-virtual {v3, v1}, Lorg/apache/poi/hwpf/model/NotesTables;->getDescriptor(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v0

    .line 81
    .local v0, "anchorPosition":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getNoteAnchorPosition(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/NotesTables;->getDescriptor(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v0

    return v0
.end method

.method public getNoteIndexByAnchorPosition(I)I
    .locals 3
    .param p1, "anchorPosition"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->updateAnchorToIndexMap()V

    .line 49
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->anchorToIndexMap:Ljava/util/Map;

    .line 50
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 51
    .local v0, "index":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 52
    const/4 v1, -0x1

    .line 54
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getNoteTextEndOffset(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/NotesTables;->getTextPosition(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v0

    return v0
.end method

.method public getNoteTextStartOffset(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/NotesTables;->getTextPosition(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v0

    return v0
.end method

.method public getNotesCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/NotesImpl;->notesTables:Lorg/apache/poi/hwpf/model/NotesTables;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/NotesTables;->getDescriptorsCount()I

    move-result v0

    return v0
.end method
