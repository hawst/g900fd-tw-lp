.class public final Lorg/apache/poi/hwpf/model/FieldDescriptor;
.super Lorg/apache/poi/hwpf/model/types/FLDAbstractType;
.source "FieldDescriptor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final FIELD_BEGIN_MARK:I = 0x13

.field public static final FIELD_END_MARK:I = 0x15

.field public static final FIELD_SEPARATOR_MARK:I = 0x14


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FLDAbstractType;-><init>()V

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->fillFields([BI)V

    .line 38
    return-void
.end method


# virtual methods
.method public getBoundaryType()I
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    return v0
.end method

.method public getFieldType()I
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 49
    const-string/jumbo v1, "This field is only defined for begin marks."

    .line 48
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getFlt()B

    move-result v0

    return v0
.end method

.method public isHasSep()Z
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 104
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 105
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 104
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFHasSep()Z

    move-result v0

    return v0
.end method

.method public isLocked()Z
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 80
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 81
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 80
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFLocked()Z

    move-result v0

    return v0
.end method

.method public isNested()Z
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 96
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 97
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 96
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFNested()Z

    move-result v0

    return v0
.end method

.method public isPrivateResult()Z
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 88
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 89
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 88
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFPrivateResult()Z

    move-result v0

    return v0
.end method

.method public isResultDirty()Z
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 65
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 64
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFResultDirty()Z

    move-result v0

    return v0
.end method

.method public isResultEdited()Z
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 73
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 72
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFResultEdited()Z

    move-result v0

    return v0
.end method

.method public isZombieEmbed()Z
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getCh()B

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 57
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 56
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFZombieEmbed()Z

    move-result v0

    return v0
.end method
