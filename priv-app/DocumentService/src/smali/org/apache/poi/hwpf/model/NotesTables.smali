.class public Lorg/apache/poi/hwpf/model/NotesTables;
.super Ljava/lang/Object;
.source "NotesTables.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

.field private final noteType:Lorg/apache/poi/hwpf/model/NoteType;

.field private textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/NoteType;)V
    .locals 5
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 33
    invoke-static {}, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->getSize()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 37
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-direct {v0, v4}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 41
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    .line 42
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 43
    new-instance v1, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    const/4 v2, 0x1

    new-array v3, v4, [B

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->addProperty(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/model/NoteType;[BLorg/apache/poi/hwpf/model/FileInformationBlock;)V
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;
    .param p2, "tableStream"    # [B
    .param p3, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 33
    invoke-static {}, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->getSize()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 37
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 49
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    .line 50
    invoke-direct {p0, p2, p3}, Lorg/apache/poi/hwpf/model/NotesTables;->read([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V

    .line 51
    return-void
.end method

.method private read([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V
    .locals 6
    .param p1, "tableStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .prologue
    .line 70
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p2, v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesDescriptorsOffset(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v1

    .line 71
    .local v1, "referencesStart":I
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p2, v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesDescriptorsSize(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v0

    .line 73
    .local v0, "referencesLength":I
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 74
    new-instance v4, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 75
    invoke-static {}, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->getSize()I

    move-result v5

    invoke-direct {v4, p1, v1, v0, v5}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 74
    iput-object v4, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 77
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p2, v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesTextPositionsOffset(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v3

    .line 78
    .local v3, "textPositionsStart":I
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p2, v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesTextPositionsSize(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v2

    .line 80
    .local v2, "textPositionsLength":I
    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 81
    new-instance v4, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 82
    const/4 v5, 0x0

    invoke-direct {v4, p1, v3, v2, v5}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 81
    iput-object v4, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 83
    :cond_1
    return-void
.end method


# virtual methods
.method public getDescriptor(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptorsCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v0

    return v0
.end method

.method public getTextPosition(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v0

    return-object v0
.end method

.method public writeRef(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 4
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 90
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesDescriptorsOffset(Lorg/apache/poi/hwpf/model/NoteType;I)V

    .line 91
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesDescriptorsSize(Lorg/apache/poi/hwpf/model/NoteType;I)V

    .line 101
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 96
    .local v1, "start":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->descriptors:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 97
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    .line 99
    .local v0, "end":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p1, v2, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesDescriptorsOffset(Lorg/apache/poi/hwpf/model/NoteType;I)V

    .line 100
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    sub-int v3, v0, v1

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesDescriptorsSize(Lorg/apache/poi/hwpf/model/NoteType;I)V

    goto :goto_0
.end method

.method public writeTxt(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 4
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 108
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesTextPositionsOffset(Lorg/apache/poi/hwpf/model/NoteType;I)V

    .line 109
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesTextPositionsSize(Lorg/apache/poi/hwpf/model/NoteType;I)V

    .line 119
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 114
    .local v1, "start":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->textPositions:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 115
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    .line 117
    .local v0, "end":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    invoke-virtual {p1, v2, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesTextPositionsOffset(Lorg/apache/poi/hwpf/model/NoteType;I)V

    .line 118
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/NotesTables;->noteType:Lorg/apache/poi/hwpf/model/NoteType;

    sub-int v3, v0, v1

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setNotesTextPositionsSize(Lorg/apache/poi/hwpf/model/NoteType;I)V

    goto :goto_0
.end method
