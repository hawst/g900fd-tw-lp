.class public final Lorg/apache/poi/hwpf/model/Hyphenation;
.super Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;
.source "Hyphenation.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>(S)V
    .locals 2
    .param p1, "hres"    # S

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;-><init>()V

    .line 40
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 41
    .local v0, "data":[B
    invoke-static {v0, p1}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 42
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/Hyphenation;->fillFields([BI)V

    .line 43
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Hyphenation;->clone()Lorg/apache/poi/hwpf/model/Hyphenation;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/model/Hyphenation;
    .locals 2

    .prologue
    .line 49
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/Hyphenation;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 51
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p0, p1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 63
    goto :goto_0

    .line 64
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 65
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 66
    check-cast v0, Lorg/apache/poi/hwpf/model/Hyphenation;

    .line 67
    .local v0, "other":Lorg/apache/poi/hwpf/model/Hyphenation;
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_1_hres:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_1_hres:B

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 68
    goto :goto_0

    .line 69
    :cond_4
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_2_chHres:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_2_chHres:B

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 70
    goto :goto_0
.end method

.method public getValue()S
    .locals 2

    .prologue
    .line 76
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 77
    .local v0, "data":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/Hyphenation;->serialize([BI)V

    .line 78
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getShort([B)S

    move-result v1

    return v1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 84
    const/16 v0, 0x1f

    .line 85
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 86
    .local v1, "result":I
    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_1_hres:B

    add-int/lit8 v1, v2, 0x1f

    .line 87
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_2_chHres:B

    add-int v1, v2, v3

    .line 88
    return v1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 93
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_1_hres:B

    if-nez v0, :cond_0

    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/Hyphenation;->field_2_chHres:B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Hyphenation;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string/jumbo v0, "[HRESI] EMPTY"

    .line 102
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
