.class public final Lorg/apache/poi/hwpf/model/ParagraphHeight;
.super Ljava/lang/Object;
.source "ParagraphHeight.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private dxaCol:I

.field private dymLineOrHeight:I

.field private infoField:S

.field private reserved:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->infoField:S

    .line 42
    add-int/lit8 p2, p2, 0x2

    .line 43
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->reserved:S

    .line 44
    add-int/lit8 p2, p2, 0x2

    .line 45
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dxaCol:I

    .line 46
    add-int/lit8 p2, p2, 0x4

    .line 47
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    .line 48
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 79
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/model/ParagraphHeight;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 80
    check-cast v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;

    .line 82
    .local v0, "ph":Lorg/apache/poi/hwpf/model/ParagraphHeight;
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->infoField:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->infoField:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->reserved:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->reserved:S

    if-ne v2, v3, :cond_0

    .line 83
    iget v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dxaCol:I

    iget v3, v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dxaCol:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    iget v3, v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    if-ne v2, v3, :cond_0

    .line 82
    const/4 v1, 0x1

    .line 85
    .end local v0    # "ph":Lorg/apache/poi/hwpf/model/ParagraphHeight;
    :cond_0
    return v1
.end method

.method protected toByteArray()[B
    .locals 3

    .prologue
    .line 63
    const/16 v2, 0xc

    new-array v0, v2, [B

    .line 64
    .local v0, "buf":[B
    const/4 v1, 0x0

    .line 65
    .local v1, "offset":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->infoField:S

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 66
    add-int/lit8 v1, v1, 0x2

    .line 67
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->reserved:S

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 68
    add-int/lit8 v1, v1, 0x2

    .line 69
    iget v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dxaCol:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 70
    add-int/lit8 v1, v1, 0x4

    .line 71
    iget v2, p0, Lorg/apache/poi/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 73
    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/ParagraphHeight;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 59
    return-void
.end method
