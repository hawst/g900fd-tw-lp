.class Lorg/apache/poi/hwpf/model/Stshif;
.super Lorg/apache/poi/hwpf/model/types/StshifAbstractType;
.source "Stshif.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;-><init>()V

    .line 35
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/Stshif;->fillFields([BI)V

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p0, p1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 44
    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 46
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 47
    check-cast v0, Lorg/apache/poi/hwpf/model/Stshif;

    .line 48
    .local v0, "other":Lorg/apache/poi/hwpf/model/Stshif;
    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_1_cstd:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_1_cstd:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 49
    goto :goto_0

    .line 50
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_2_cbSTDBaseInFile:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_2_cbSTDBaseInFile:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 51
    goto :goto_0

    .line 52
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_3_info3:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_3_info3:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_4_stiMaxWhenSaved:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_4_stiMaxWhenSaved:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 55
    goto :goto_0

    .line 56
    :cond_7
    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_5_istdMaxFixedWhenSaved:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_5_istdMaxFixedWhenSaved:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 57
    goto :goto_0

    .line 58
    :cond_8
    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_6_nVerBuiltInNamesWhenSaved:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_6_nVerBuiltInNamesWhenSaved:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 59
    goto :goto_0

    .line 60
    :cond_9
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_7_ftcAsci:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_7_ftcAsci:S

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 61
    goto :goto_0

    .line 62
    :cond_a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_8_ftcFE:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_8_ftcFE:S

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 63
    goto :goto_0

    .line 64
    :cond_b
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_9_ftcOther:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/Stshif;->field_9_ftcOther:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 72
    const/16 v0, 0x1f

    .line 73
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 74
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_1_cstd:I

    add-int/lit8 v1, v2, 0x1f

    .line 75
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_2_cbSTDBaseInFile:I

    add-int v1, v2, v3

    .line 76
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_3_info3:I

    add-int v1, v2, v3

    .line 77
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_4_stiMaxWhenSaved:I

    add-int v1, v2, v3

    .line 78
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_5_istdMaxFixedWhenSaved:I

    add-int v1, v2, v3

    .line 79
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_6_nVerBuiltInNamesWhenSaved:I

    add-int v1, v2, v3

    .line 80
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_7_ftcAsci:S

    add-int v1, v2, v3

    .line 81
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_8_ftcFE:S

    add-int v1, v2, v3

    .line 82
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Stshif;->field_9_ftcOther:S

    add-int v1, v2, v3

    .line 83
    return v1
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 88
    invoke-static {}, Lorg/apache/poi/hwpf/model/Stshif;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 89
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/Stshif;->serialize([BI)V

    .line 90
    return-object v0
.end method
