.class public final Lorg/apache/poi/hwpf/sprm/CharacterSprmCompressor;
.super Ljava/lang/Object;
.source "CharacterSprmCompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static compressCharacterProperty(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)[B
    .locals 10
    .param p0, "newCHP"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .param p1, "oldCHP"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v2, "sprmList":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v1, 0x0

    .line 38
    .local v1, "size":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMarkDel()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMarkDel()Z

    move-result v6

    if-eq v5, v6, :cond_1

    .line 40
    const/4 v3, 0x0

    .line 41
    .local v3, "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMarkDel()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 43
    const/4 v3, 0x1

    .line 45
    :cond_0
    const/16 v5, 0x800

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 47
    .end local v3    # "value":I
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMark()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMark()Z

    move-result v6

    if-eq v5, v6, :cond_3

    .line 49
    const/4 v3, 0x0

    .line 50
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFRMark()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 52
    const/4 v3, 0x1

    .line 54
    :cond_2
    const/16 v5, 0x801

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 56
    .end local v3    # "value":I
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFFldVanish()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFFldVanish()Z

    move-result v6

    if-eq v5, v6, :cond_5

    .line 58
    const/4 v3, 0x0

    .line 59
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFFldVanish()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 61
    const/4 v3, 0x1

    .line 63
    :cond_4
    const/16 v5, 0x802

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 65
    .end local v3    # "value":I
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v6

    if-ne v5, v6, :cond_6

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcPic()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcPic()I

    move-result v6

    if-eq v5, v6, :cond_7

    .line 67
    :cond_6
    const/16 v5, 0x6a03

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcPic()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 69
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIbstRMark()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIbstRMark()I

    move-result v6

    if-eq v5, v6, :cond_8

    .line 71
    const/16 v5, 0x4804

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIbstRMark()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 73
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 75
    new-array v0, v9, [B

    .line 76
    .local v0, "buf":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v5

    invoke-virtual {v5, v0, v8}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->serialize([BI)V

    .line 78
    const/16 v5, 0x6805

    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 80
    .end local v0    # "buf":[B
    :cond_9
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFData()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFData()Z

    move-result v6

    if-eq v5, v6, :cond_b

    .line 82
    const/4 v3, 0x0

    .line 83
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFData()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 85
    const/4 v3, 0x1

    .line 87
    :cond_a
    const/16 v5, 0x806

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 89
    .end local v3    # "value":I
    :cond_b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcSym()I

    move-result v5

    if-eqz v5, :cond_c

    .line 91
    new-array v4, v9, [B

    .line 92
    .local v4, "varParam":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcSym()I

    move-result v5

    int-to-short v5, v5

    invoke-static {v4, v8, v5}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 93
    const/4 v5, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getXchSym()I

    move-result v6

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 95
    const/16 v5, 0x6a09

    invoke-static {v5, v8, v4, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 97
    .end local v4    # "varParam":[B
    :cond_c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOle2()Z

    move-result v5

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOle2()Z

    move-result v6

    if-eq v5, v6, :cond_e

    .line 99
    const/4 v3, 0x0

    .line 100
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOle2()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 102
    const/4 v3, 0x1

    .line 104
    :cond_d
    const/16 v5, 0x80a

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 106
    .end local v3    # "value":I
    :cond_e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIcoHighlight()B

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIcoHighlight()B

    move-result v6

    if-eq v5, v6, :cond_f

    .line 108
    const/16 v5, 0x2a0c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIcoHighlight()B

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 110
    :cond_f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcObj()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcObj()I

    move-result v6

    if-eq v5, v6, :cond_10

    .line 112
    const/16 v5, 0x680e

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFcObj()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 114
    :cond_10
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIstd()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIstd()I

    move-result v6

    if-eq v5, v6, :cond_11

    .line 116
    const/16 v5, 0x4a30

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIstd()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 118
    :cond_11
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFBold()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFBold()Z

    move-result v6

    if-eq v5, v6, :cond_13

    .line 120
    const/4 v3, 0x0

    .line 121
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFBold()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 123
    const/4 v3, 0x1

    .line 125
    :cond_12
    const/16 v5, 0x835

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 127
    .end local v3    # "value":I
    :cond_13
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFItalic()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFItalic()Z

    move-result v6

    if-eq v5, v6, :cond_15

    .line 129
    const/4 v3, 0x0

    .line 130
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFItalic()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 132
    const/4 v3, 0x1

    .line 134
    :cond_14
    const/16 v5, 0x836

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 136
    .end local v3    # "value":I
    :cond_15
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFStrike()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFStrike()Z

    move-result v6

    if-eq v5, v6, :cond_17

    .line 138
    const/4 v3, 0x0

    .line 139
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFStrike()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 141
    const/4 v3, 0x1

    .line 143
    :cond_16
    const/16 v5, 0x837

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 145
    .end local v3    # "value":I
    :cond_17
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOutline()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOutline()Z

    move-result v6

    if-eq v5, v6, :cond_19

    .line 147
    const/4 v3, 0x0

    .line 148
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOutline()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 150
    const/4 v3, 0x1

    .line 152
    :cond_18
    const/16 v5, 0x838

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 154
    .end local v3    # "value":I
    :cond_19
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFShadow()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFShadow()Z

    move-result v6

    if-eq v5, v6, :cond_1b

    .line 156
    const/4 v3, 0x0

    .line 157
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFShadow()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 159
    const/4 v3, 0x1

    .line 161
    :cond_1a
    const/16 v5, 0x839

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 163
    .end local v3    # "value":I
    :cond_1b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSmallCaps()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSmallCaps()Z

    move-result v6

    if-eq v5, v6, :cond_1d

    .line 165
    const/4 v3, 0x0

    .line 166
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSmallCaps()Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 168
    const/4 v3, 0x1

    .line 170
    :cond_1c
    const/16 v5, 0x83a

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 172
    .end local v3    # "value":I
    :cond_1d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFCaps()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFCaps()Z

    move-result v6

    if-eq v5, v6, :cond_1f

    .line 174
    const/4 v3, 0x0

    .line 175
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFCaps()Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 177
    const/4 v3, 0x1

    .line 179
    :cond_1e
    const/16 v5, 0x83b

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 181
    .end local v3    # "value":I
    :cond_1f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFVanish()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFVanish()Z

    move-result v6

    if-eq v5, v6, :cond_21

    .line 183
    const/4 v3, 0x0

    .line 184
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFVanish()Z

    move-result v5

    if-eqz v5, :cond_20

    .line 186
    const/4 v3, 0x1

    .line 188
    :cond_20
    const/16 v5, 0x83c

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 190
    .end local v3    # "value":I
    :cond_21
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getKul()B

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getKul()B

    move-result v6

    if-eq v5, v6, :cond_22

    .line 192
    const/16 v5, 0x2a3e

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getKul()B

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 194
    :cond_22
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDxaSpace()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDxaSpace()I

    move-result v6

    if-eq v5, v6, :cond_23

    .line 196
    const/16 v5, -0x77c0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getDxaSpace()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 198
    :cond_23
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIco()B

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIco()B

    move-result v6

    if-eq v5, v6, :cond_24

    .line 200
    const/16 v5, 0x2a42

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getIco()B

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 202
    :cond_24
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v6

    if-eq v5, v6, :cond_25

    .line 204
    const/16 v5, 0x4a43

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 206
    :cond_25
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v6

    if-eq v5, v6, :cond_26

    .line 208
    const/16 v5, 0x4845

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 210
    :cond_26
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsKern()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsKern()I

    move-result v6

    if-eq v5, v6, :cond_27

    .line 212
    const/16 v5, 0x484b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsKern()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 214
    :cond_27
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHresi()Lorg/apache/poi/hwpf/model/Hyphenation;

    move-result-object v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHresi()Lorg/apache/poi/hwpf/model/Hyphenation;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/poi/hwpf/model/Hyphenation;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_28

    .line 216
    const/16 v5, 0x484e

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHresi()Lorg/apache/poi/hwpf/model/Hyphenation;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/Hyphenation;->getValue()S

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 218
    :cond_28
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcAscii()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcAscii()I

    move-result v6

    if-eq v5, v6, :cond_29

    .line 220
    const/16 v5, 0x4a4f

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcAscii()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 222
    :cond_29
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcFE()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcFE()I

    move-result v6

    if-eq v5, v6, :cond_2a

    .line 224
    const/16 v5, 0x4a50

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcFE()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 226
    :cond_2a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcOther()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcOther()I

    move-result v6

    if-eq v5, v6, :cond_2b

    .line 228
    const/16 v5, 0x4a51

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getFtcOther()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 231
    :cond_2b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFDStrike()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFDStrike()Z

    move-result v6

    if-eq v5, v6, :cond_2d

    .line 233
    const/4 v3, 0x0

    .line 234
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFDStrike()Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 236
    const/4 v3, 0x1

    .line 238
    :cond_2c
    const/16 v5, 0x2a53

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 240
    .end local v3    # "value":I
    :cond_2d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFImprint()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFImprint()Z

    move-result v6

    if-eq v5, v6, :cond_2f

    .line 242
    const/4 v3, 0x0

    .line 243
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFImprint()Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 245
    const/4 v3, 0x1

    .line 247
    :cond_2e
    const/16 v5, 0x854

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 249
    .end local v3    # "value":I
    :cond_2f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v6

    if-eq v5, v6, :cond_31

    .line 251
    const/4 v3, 0x0

    .line 252
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v5

    if-eqz v5, :cond_30

    .line 254
    const/4 v3, 0x1

    .line 256
    :cond_30
    const/16 v5, 0x855

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 258
    .end local v3    # "value":I
    :cond_31
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFObj()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFObj()Z

    move-result v6

    if-eq v5, v6, :cond_33

    .line 260
    const/4 v3, 0x0

    .line 261
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFObj()Z

    move-result v5

    if-eqz v5, :cond_32

    .line 263
    const/4 v3, 0x1

    .line 265
    :cond_32
    const/16 v5, 0x856

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 267
    .end local v3    # "value":I
    :cond_33
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFEmboss()Z

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFEmboss()Z

    move-result v6

    if-eq v5, v6, :cond_35

    .line 269
    const/4 v3, 0x0

    .line 270
    .restart local v3    # "value":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFEmboss()Z

    move-result v5

    if-eqz v5, :cond_34

    .line 272
    const/4 v3, 0x1

    .line 274
    :cond_34
    const/16 v5, 0x858

    invoke-static {v5, v3, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 276
    .end local v3    # "value":I
    :cond_35
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getSfxtText()B

    move-result v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getSfxtText()B

    move-result v6

    if-eq v5, v6, :cond_36

    .line 278
    const/16 v5, 0x2859

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getSfxtText()B

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 280
    :cond_36
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v5

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/poi/hwpf/model/Colorref;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_37

    .line 283
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/Colorref;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_37

    .line 284
    const/16 v5, 0x6870

    .line 285
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/Colorref;->getValue()I

    move-result v6

    invoke-static {v5, v6, v7, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v5

    add-int/2addr v1, v5

    .line 288
    :cond_37
    invoke-static {v2, v1}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->getGrpprl(Ljava/util/List;I)[B

    move-result-object v5

    return-object v5
.end method
