.class public final Lorg/apache/poi/hwpf/model/FileInformationBlock;
.super Ljava/lang/Object;
.source "FileInformationBlock.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _cbRgFcLcb:I

.field private _cslw:I

.field private _csw:I

.field private _cswNew:I

.field private _fibBase:Lorg/apache/poi/hwpf/model/FibBase;

.field private _fibRgCswNew:[B

.field private _fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

.field private _fibRgW:Lorg/apache/poi/hwpf/model/FibRgW97;

.field private _fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

.field private _nFibNew:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    .line 53
    const-class v0, Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 52
    sput-object v0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->logger:Lorg/apache/poi/util/POILogger;

    .line 53
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([B)V
    .locals 6
    .param p1, "mainDocument"    # [B

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v1, 0x0

    .line 71
    .local v1, "offset":I
    new-instance v2, Lorg/apache/poi/hwpf/model/FibBase;

    invoke-direct {v2, p1, v1}, Lorg/apache/poi/hwpf/model/FibBase;-><init>([BI)V

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    .line 72
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibBase;->getSize()I

    move-result v1

    .line 73
    sget-boolean v2, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const/16 v2, 0x20

    if-eq v1, v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 75
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FibBase;->isFEncrypted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    new-instance v2, Lorg/apache/poi/EncryptedDocumentException;

    .line 78
    const-string/jumbo v3, "Cannot process encrypted word file"

    .line 77
    invoke-direct {v2, v3}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 81
    :cond_1
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_csw:I

    .line 82
    add-int/lit8 v1, v1, 0x2

    .line 83
    sget-boolean v2, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    const/16 v2, 0x22

    if-eq v1, v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 85
    :cond_2
    new-instance v2, Lorg/apache/poi/hwpf/model/FibRgW97;

    invoke-direct {v2, p1, v1}, Lorg/apache/poi/hwpf/model/FibRgW97;-><init>([BI)V

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgW:Lorg/apache/poi/hwpf/model/FibRgW97;

    .line 86
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgW97;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 87
    sget-boolean v2, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    const/16 v2, 0x3e

    if-eq v1, v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 89
    :cond_3
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cslw:I

    .line 90
    add-int/lit8 v1, v1, 0x2

    .line 91
    sget-boolean v2, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    const/16 v2, 0x40

    if-eq v1, v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 93
    :cond_4
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FibBase;->getNFib()I

    move-result v2

    const/16 v3, 0x69

    if-ge v2, v3, :cond_5

    .line 95
    new-instance v2, Lorg/apache/poi/hwpf/model/FibRgLw95;

    invoke-direct {v2, p1, v1}, Lorg/apache/poi/hwpf/model/FibRgLw95;-><init>([BI)V

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    .line 96
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgLw97;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 99
    const/16 v2, 0x4a

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    .line 102
    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    mul-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 104
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    .line 105
    add-int/lit8 v1, v1, 0x2

    .line 107
    iput v4, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    .line 108
    iput v5, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_nFibNew:I

    .line 109
    new-array v2, v4, [B

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgCswNew:[B

    .line 148
    :goto_0
    return-void

    .line 114
    :cond_5
    new-instance v2, Lorg/apache/poi/hwpf/model/FibRgLw97;

    invoke-direct {v2, p1, v1}, Lorg/apache/poi/hwpf/model/FibRgLw97;-><init>([BI)V

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    .line 115
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgLw97;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 116
    sget-boolean v2, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    const/16 v2, 0x98

    if-eq v1, v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 118
    :cond_6
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    .line 119
    add-int/lit8 v1, v1, 0x2

    .line 120
    sget-boolean v2, Lorg/apache/poi/hwpf/model/FileInformationBlock;->$assertionsDisabled:Z

    if-nez v2, :cond_7

    const/16 v2, 0x9a

    if-eq v1, v2, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 123
    :cond_7
    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    mul-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 125
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    .line 126
    add-int/lit8 v1, v1, 0x2

    .line 128
    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    if-eqz v2, :cond_8

    .line 130
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_nFibNew:I

    .line 131
    add-int/lit8 v1, v1, 0x2

    .line 134
    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v0, v2, 0x2

    .line 136
    .local v0, "fibRgCswNewLength":I
    new-array v2, v0, [B

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgCswNew:[B

    .line 137
    invoke-static {p1, v1, v0}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    .line 138
    add-int/2addr v1, v0

    .line 146
    .end local v0    # "fibRgCswNewLength":I
    :goto_1
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCbRgFcLcb()V

    .line 147
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCswNew()V

    goto :goto_0

    .line 142
    :cond_8
    iput v5, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_nFibNew:I

    .line 143
    new-array v2, v4, [B

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgCswNew:[B

    goto :goto_1
.end method

.method private assertCbRgFcLcb()V
    .locals 4

    .prologue
    .line 152
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNFib()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 170
    :goto_0
    return-void

    .line 155
    :sswitch_0
    const-string/jumbo v0, "0x00C1"

    const/16 v1, 0x5d

    const-string/jumbo v2, "0x005D"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCbRgFcLcb(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 158
    :sswitch_1
    const-string/jumbo v0, "0x00D9"

    const/16 v1, 0x6c

    const-string/jumbo v2, "0x006C"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCbRgFcLcb(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 161
    :sswitch_2
    const-string/jumbo v0, "0x0101"

    const/16 v1, 0x88

    const-string/jumbo v2, "0x0088"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCbRgFcLcb(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 164
    :sswitch_3
    const-string/jumbo v0, "0x010C"

    const/16 v1, 0xa4

    const-string/jumbo v2, "0x00A4"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCbRgFcLcb(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 167
    :sswitch_4
    const-string/jumbo v0, "0x0112"

    const/16 v1, 0xb7

    const-string/jumbo v2, "0x00B7"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCbRgFcLcb(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 152
    :sswitch_data_0
    .sparse-switch
        0xc1 -> :sswitch_0
        0xd9 -> :sswitch_1
        0x101 -> :sswitch_2
        0x10c -> :sswitch_3
        0x112 -> :sswitch_4
    .end sparse-switch
.end method

.method private static assertCbRgFcLcb(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 7
    .param p0, "strNFib"    # Ljava/lang/String;
    .param p1, "expectedCbRgFcLcb"    # I
    .param p2, "strCbRgFcLcb"    # Ljava/lang/String;
    .param p3, "cbRgFcLcb"    # I

    .prologue
    .line 176
    if-ne p3, p1, :cond_0

    .line 182
    :goto_0
    return-void

    .line 179
    :cond_0
    sget-object v0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    const-string/jumbo v2, "Since FIB.nFib == "

    .line 180
    const-string/jumbo v4, " value of FIB.cbRgFcLcb MUST be "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ", not 0x"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 181
    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    move-object v3, p0

    .line 179
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private assertCswNew()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 186
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNFib()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 204
    :goto_0
    return-void

    .line 189
    :sswitch_0
    const-string/jumbo v0, "0x00C1"

    const/4 v1, 0x0

    const-string/jumbo v2, "0x0000"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCswNew(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 192
    :sswitch_1
    const-string/jumbo v0, "0x00D9"

    const-string/jumbo v1, "0x0002"

    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    invoke-static {v0, v3, v1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCswNew(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 195
    :sswitch_2
    const-string/jumbo v0, "0x0101"

    const-string/jumbo v1, "0x0002"

    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    invoke-static {v0, v3, v1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCswNew(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 198
    :sswitch_3
    const-string/jumbo v0, "0x010C"

    const-string/jumbo v1, "0x0002"

    iget v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    invoke-static {v0, v3, v1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCswNew(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 201
    :sswitch_4
    const-string/jumbo v0, "0x0112"

    const/4 v1, 0x5

    const-string/jumbo v2, "0x0005"

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->assertCswNew(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 186
    nop

    :sswitch_data_0
    .sparse-switch
        0xc1 -> :sswitch_0
        0xd9 -> :sswitch_1
        0x101 -> :sswitch_2
        0x10c -> :sswitch_3
        0x112 -> :sswitch_4
    .end sparse-switch
.end method

.method private static assertCswNew(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 7
    .param p0, "strNFib"    # Ljava/lang/String;
    .param p1, "expectedCswNew"    # I
    .param p2, "strExpectedCswNew"    # Ljava/lang/String;
    .param p3, "cswNew"    # I

    .prologue
    .line 210
    if-ne p3, p1, :cond_0

    .line 216
    :goto_0
    return-void

    .line 213
    :cond_0
    sget-object v0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    const-string/jumbo v2, "Since FIB.nFib == "

    .line 214
    const-string/jumbo v4, " value of FIB.cswNew MUST be "

    .line 215
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ", not 0x"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    move-object v3, p0

    .line 213
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public clearOffsetsSizes()V
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->clearFields()V

    .line 777
    return-void
.end method

.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1113
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public fillVariableFields([B[B)V
    .locals 9
    .param p1, "mainDocument"    # [B
    .param p2, "tableStream"    # [B

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 225
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 226
    .local v5, "knownFieldSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 227
    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 228
    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 229
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 230
    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 231
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 232
    const/16 v1, 0x49

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 233
    const/16 v1, 0x4a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 240
    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 241
    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 242
    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-static {}, Lorg/apache/poi/hwpf/model/NoteType;->values()[Lorg/apache/poi/hwpf/model/NoteType;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-lt v0, v2, :cond_1

    .line 253
    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 254
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 255
    const/16 v0, 0x47

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 256
    const/16 v0, 0x57

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 258
    new-instance v0, Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v2, 0x9a

    iget v3, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    move-object v1, p1

    move-object v4, p2

    .line 259
    invoke-direct/range {v0 .. v6}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;-><init>([BII[BLjava/util/HashSet;Z)V

    .line 258
    iput-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    .line 260
    return-void

    .line 236
    :cond_0
    aget-object v8, v2, v1

    .line 237
    .local v8, "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->getFibFieldsField()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    .end local v8    # "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    :cond_1
    aget-object v7, v1, v0

    .line 248
    .local v7, "noteType":Lorg/apache/poi/hwpf/model/NoteType;
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/NoteType;->getFibDescriptorsFieldIndex()I

    move-result v3

    .line 247
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 250
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/NoteType;->getFibTextPositionsFieldIndex()I

    move-result v3

    .line 249
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 245
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getCbMac()I
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    invoke-interface {v0}, Lorg/apache/poi/hwpf/model/FibRgLw;->getCbMac()I

    move-result v0

    return v0
.end method

.method public getFSPAPlcfLength(Lorg/apache/poi/hwpf/model/FSPADocumentPart;)I
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    .prologue
    .line 975
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getFSPAPlcfOffset(Lorg/apache/poi/hwpf/model/FSPADocumentPart;)I
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    .prologue
    .line 970
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcClx()I
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcDggInfo()I
    .locals 2

    .prologue
    .line 1002
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcDop()I
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfLst()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 463
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfbkf()I
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfbkl()I
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfbteChpx()I
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfbtePapx()I
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldAtn()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 802
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldEdn()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 826
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldFtn()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 850
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldHdr()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 874
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldHdrtxbx()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 898
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldMom()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 922
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldTxbx()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 946
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x39

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfsed()I
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcspaMom()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 991
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlfLfo()I
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlfLst()I
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcStshf()I
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbSavedBy()I
    .locals 2

    .prologue
    .line 695
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbfRMark()I
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbfbkmk()I
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbfffn()I
    .locals 2

    .prologue
    .line 634
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFibBase()Lorg/apache/poi/hwpf/model/FibBase;
    .locals 1

    .prologue
    .line 1106
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    return-object v0
.end method

.method public getFieldsPlcfLength(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)I
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .prologue
    .line 786
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getFieldsPlcfOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)I
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .prologue
    .line 781
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getLcbClx()I
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbDggInfo()I
    .locals 2

    .prologue
    .line 1007
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbDop()I
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfLst()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 491
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfbkf()I
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfbkl()I
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfbteChpx()I
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfbtePapx()I
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldAtn()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 808
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldEdn()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 832
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldFtn()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 856
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldHdr()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 880
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldHdrtxbx()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 904
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldMom()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 928
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldTxbx()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 952
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x39

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfsed()I
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcspaMom()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 997
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlfLfo()I
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlfLst()I
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbStshf()I
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbSavedBy()I
    .locals 2

    .prologue
    .line 700
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbfRMark()I
    .locals 2

    .prologue
    .line 659
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbfbkmk()I
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbfffn()I
    .locals 2

    .prologue
    .line 639
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getModifiedHigh()I
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getModifiedLow()I
    .locals 2

    .prologue
    .line 715
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getNFib()I
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FibBase;->getNFib()I

    move-result v0

    .line 337
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_nFibNew:I

    goto :goto_0
.end method

.method public getNotesDescriptorsOffset(Lorg/apache/poi/hwpf/model/NoteType;)I
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;

    .prologue
    .line 1012
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    .line 1013
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibDescriptorsFieldIndex()I

    move-result v1

    .line 1012
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getNotesDescriptorsSize(Lorg/apache/poi/hwpf/model/NoteType;)I
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;

    .prologue
    .line 1024
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    .line 1025
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibDescriptorsFieldIndex()I

    move-result v1

    .line 1024
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getNotesTextPositionsOffset(Lorg/apache/poi/hwpf/model/NoteType;)I
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;

    .prologue
    .line 1036
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    .line 1037
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibTextPositionsFieldIndex()I

    move-result v1

    .line 1036
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getNotesTextPositionsSize(Lorg/apache/poi/hwpf/model/NoteType;)I
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;

    .prologue
    .line 1048
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    .line 1049
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibTextPositionsFieldIndex()I

    move-result v1

    .line 1048
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getPlcfHddOffset()I
    .locals 2

    .prologue
    .line 677
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getPlcfHddSize()I
    .locals 2

    .prologue
    .line 684
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getSize()I
    .locals 2

    .prologue
    .line 1099
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibBase;->getSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgW97;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    .line 1100
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgLw97;->getSize()I

    move-result v1

    .line 1099
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    .line 1101
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->sizeInBytes()I

    move-result v1

    .line 1099
    add-int/2addr v0, v1

    return v0
.end method

.method public getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I
    .locals 2
    .param p1, "type"    # Lorg/apache/poi/hwpf/model/SubdocumentType;

    .prologue
    .line 755
    if-nez p1, :cond_0

    .line 756
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "argument \'type\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 758
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    invoke-interface {v0, p1}, Lorg/apache/poi/hwpf/model/FibRgLw;->getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I

    move-result v0

    return v0
.end method

.method public setCbMac(I)V
    .locals 1
    .param p1, "cbMac"    # I

    .prologue
    .line 747
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    invoke-interface {v0, p1}, Lorg/apache/poi/hwpf/model/FibRgLw;->setCbMac(I)V

    .line 748
    return-void
.end method

.method public setFSPAPlcfLength(Lorg/apache/poi/hwpf/model/FSPADocumentPart;I)V
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FSPADocumentPart;
    .param p2, "length"    # I

    .prologue
    .line 985
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 986
    return-void
.end method

.method public setFSPAPlcfOffset(Lorg/apache/poi/hwpf/model/FSPADocumentPart;I)V
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FSPADocumentPart;
    .param p2, "offset"    # I

    .prologue
    .line 980
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 981
    return-void
.end method

.method public setFcClx(I)V
    .locals 2
    .param p1, "fcClx"    # I

    .prologue
    .line 392
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 393
    return-void
.end method

.method public setFcDop(I)V
    .locals 2
    .param p1, "fcDop"    # I

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 348
    return-void
.end method

.method public setFcPlcfLst(I)V
    .locals 2
    .param p1, "fcPlcfLst"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 502
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 503
    return-void
.end method

.method public setFcPlcfbkf(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 578
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 579
    return-void
.end method

.method public setFcPlcfbkl(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 606
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x17

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 607
    return-void
.end method

.method public setFcPlcfbteChpx(I)V
    .locals 2
    .param p1, "fcPlcfBteChpx"    # I

    .prologue
    .line 412
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 413
    return-void
.end method

.method public setFcPlcfbtePapx(I)V
    .locals 2
    .param p1, "fcPlcfBtePapx"    # I

    .prologue
    .line 432
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 433
    return-void
.end method

.method public setFcPlcffldAtn(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 814
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 815
    return-void
.end method

.method public setFcPlcffldEdn(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 838
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x30

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 839
    return-void
.end method

.method public setFcPlcffldFtn(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 862
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 863
    return-void
.end method

.method public setFcPlcffldHdr(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 886
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 887
    return-void
.end method

.method public setFcPlcffldHdrtxbx(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 910
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 911
    return-void
.end method

.method public setFcPlcffldMom(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 934
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 935
    return-void
.end method

.method public setFcPlcffldTxbx(I)V
    .locals 2
    .param p1, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 958
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x39

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 959
    return-void
.end method

.method public setFcPlcfsed(I)V
    .locals 2
    .param p1, "fcPlcfSed"    # I

    .prologue
    .line 452
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 453
    return-void
.end method

.method public setFcPlfLfo(I)V
    .locals 2
    .param p1, "fcPlfLfo"    # I

    .prologue
    .line 624
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 625
    return-void
.end method

.method public setFcPlfLst(I)V
    .locals 2
    .param p1, "fcPlfLst"    # I

    .prologue
    .line 507
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 508
    return-void
.end method

.method public setFcStshf(I)V
    .locals 2
    .param p1, "fcStshf"    # I

    .prologue
    .line 372
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 373
    return-void
.end method

.method public setFcSttbSavedBy(I)V
    .locals 2
    .param p1, "fcSttbSavedBy"    # I

    .prologue
    .line 705
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 706
    return-void
.end method

.method public setFcSttbfRMark(I)V
    .locals 2
    .param p1, "fcSttbfRMark"    # I

    .prologue
    .line 664
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 665
    return-void
.end method

.method public setFcSttbfbkmk(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 550
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 551
    return-void
.end method

.method public setFcSttbfffn(I)V
    .locals 2
    .param p1, "fcSttbFffn"    # I

    .prologue
    .line 644
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 645
    return-void
.end method

.method public setFieldsPlcfLength(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)V
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .param p2, "length"    # I

    .prologue
    .line 796
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 797
    return-void
.end method

.method public setFieldsPlcfOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)V
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .param p2, "offset"    # I

    .prologue
    .line 791
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->getFibFieldsField()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 792
    return-void
.end method

.method public setLcbClx(I)V
    .locals 2
    .param p1, "lcbClx"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 398
    return-void
.end method

.method public setLcbDop(I)V
    .locals 2
    .param p1, "lcbDop"    # I

    .prologue
    .line 357
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 358
    return-void
.end method

.method public setLcbPlcfLst(I)V
    .locals 2
    .param p1, "lcbPlcfLst"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 513
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 514
    return-void
.end method

.method public setLcbPlcfbkf(I)V
    .locals 2
    .param p1, "length"    # I

    .prologue
    .line 591
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 592
    return-void
.end method

.method public setLcbPlcfbkl(I)V
    .locals 2
    .param p1, "length"    # I

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x17

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 620
    return-void
.end method

.method public setLcbPlcfbteChpx(I)V
    .locals 2
    .param p1, "lcbPlcfBteChpx"    # I

    .prologue
    .line 417
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 418
    return-void
.end method

.method public setLcbPlcfbtePapx(I)V
    .locals 2
    .param p1, "lcbPlcfBtePapx"    # I

    .prologue
    .line 437
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 438
    return-void
.end method

.method public setLcbPlcffldAtn(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 820
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 821
    return-void
.end method

.method public setLcbPlcffldEdn(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 844
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x30

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 845
    return-void
.end method

.method public setLcbPlcffldFtn(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 868
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 869
    return-void
.end method

.method public setLcbPlcffldHdr(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 892
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 893
    return-void
.end method

.method public setLcbPlcffldHdrtxbx(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 916
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 917
    return-void
.end method

.method public setLcbPlcffldMom(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 940
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 941
    return-void
.end method

.method public setLcbPlcffldTxbx(I)V
    .locals 2
    .param p1, "size"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 964
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x39

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 965
    return-void
.end method

.method public setLcbPlcfsed(I)V
    .locals 2
    .param p1, "lcbPlcfSed"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 458
    return-void
.end method

.method public setLcbPlfLfo(I)V
    .locals 2
    .param p1, "lcbPlfLfo"    # I

    .prologue
    .line 629
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 630
    return-void
.end method

.method public setLcbPlfLst(I)V
    .locals 2
    .param p1, "lcbPlfLst"    # I

    .prologue
    .line 518
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 519
    return-void
.end method

.method public setLcbStshf(I)V
    .locals 2
    .param p1, "lcbStshf"    # I

    .prologue
    .line 377
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 378
    return-void
.end method

.method public setLcbSttbSavedBy(I)V
    .locals 2
    .param p1, "fcSttbSavedBy"    # I

    .prologue
    .line 710
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 711
    return-void
.end method

.method public setLcbSttbfRMark(I)V
    .locals 2
    .param p1, "lcbSttbfRMark"    # I

    .prologue
    .line 669
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 670
    return-void
.end method

.method public setLcbSttbfbkmk(I)V
    .locals 2
    .param p1, "length"    # I

    .prologue
    .line 563
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 564
    return-void
.end method

.method public setLcbSttbfffn(I)V
    .locals 2
    .param p1, "lcbSttbFffn"    # I

    .prologue
    .line 649
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 650
    return-void
.end method

.method public setModifiedHigh(I)V
    .locals 2
    .param p1, "modifiedHigh"    # I

    .prologue
    .line 730
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 731
    return-void
.end method

.method public setModifiedLow(I)V
    .locals 2
    .param p1, "modifiedLow"    # I

    .prologue
    .line 725
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 726
    return-void
.end method

.method public setNotesDescriptorsOffset(Lorg/apache/poi/hwpf/model/NoteType;I)V
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;
    .param p2, "offset"    # I

    .prologue
    .line 1018
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibDescriptorsFieldIndex()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 1020
    return-void
.end method

.method public setNotesDescriptorsSize(Lorg/apache/poi/hwpf/model/NoteType;I)V
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;
    .param p2, "offset"    # I

    .prologue
    .line 1030
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibDescriptorsFieldIndex()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 1032
    return-void
.end method

.method public setNotesTextPositionsOffset(Lorg/apache/poi/hwpf/model/NoteType;I)V
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;
    .param p2, "offset"    # I

    .prologue
    .line 1042
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibTextPositionsFieldIndex()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 1044
    return-void
.end method

.method public setNotesTextPositionsSize(Lorg/apache/poi/hwpf/model/NoteType;I)V
    .locals 2
    .param p1, "noteType"    # Lorg/apache/poi/hwpf/model/NoteType;
    .param p2, "offset"    # I

    .prologue
    .line 1054
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/NoteType;->getFibTextPositionsFieldIndex()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 1056
    return-void
.end method

.method public setPlcfHddOffset(I)V
    .locals 2
    .param p1, "fcPlcfHdd"    # I

    .prologue
    .line 687
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 688
    return-void
.end method

.method public setPlcfHddSize(I)V
    .locals 2
    .param p1, "lcbPlcfHdd"    # I

    .prologue
    .line 690
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 691
    return-void
.end method

.method public setSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;I)V
    .locals 3
    .param p1, "type"    # Lorg/apache/poi/hwpf/model/SubdocumentType;
    .param p2, "length"    # I

    .prologue
    .line 763
    if-nez p1, :cond_0

    .line 764
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "argument \'type\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 765
    :cond_0
    if-gez p2, :cond_1

    .line 766
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 767
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Subdocument length can\'t be less than 0 (passed value is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 768
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "). "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "If there is no subdocument "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 769
    const-string/jumbo v2, "length must be set to zero."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 767
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 766
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 771
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    invoke-interface {v0, p1, p2}, Lorg/apache/poi/hwpf/model/FibRgLw;->setSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;I)V

    .line 772
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 265
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    .local v4, "stringBuilder":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 267
    const-string/jumbo v7, "[FIB2]\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string/jumbo v7, "\tSubdocuments info:\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    invoke-static {}, Lorg/apache/poi/hwpf/model/SubdocumentType;->values()[Lorg/apache/poi/hwpf/model/SubdocumentType;

    move-result-object v8

    array-length v9, v8

    move v7, v6

    :goto_0
    if-lt v7, v9, :cond_0

    .line 277
    const-string/jumbo v7, "\tFields PLCF info:\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v8

    array-length v9, v8

    move v7, v6

    :goto_1
    if-lt v7, v9, :cond_1

    .line 288
    const-string/jumbo v7, "\tNotes PLCF info:\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    invoke-static {}, Lorg/apache/poi/hwpf/model/NoteType;->values()[Lorg/apache/poi/hwpf/model/NoteType;

    move-result-object v8

    array-length v9, v8

    move v7, v6

    :goto_2
    if-lt v7, v9, :cond_2

    .line 306
    iget-object v7, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 309
    :try_start_0
    const-string/jumbo v7, "\tJava reflection info:\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-class v7, Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v7}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v7

    array-length v8, v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    if-lt v6, v8, :cond_3

    .line 328
    :goto_4
    const-string/jumbo v6, "[/FIB2]\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 269
    :cond_0
    aget-object v5, v8, v7

    .line 271
    .local v5, "type":Lorg/apache/poi/hwpf/model/SubdocumentType;
    const-string/jumbo v10, "\t\t"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    const-string/jumbo v10, " has length of "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    invoke-virtual {p0, v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 275
    const-string/jumbo v10, " char(s)\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 278
    .end local v5    # "type":Lorg/apache/poi/hwpf/model/SubdocumentType;
    :cond_1
    aget-object v3, v8, v7

    .line 280
    .local v3, "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    const-string/jumbo v10, "\t\t"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    const-string/jumbo v10, ": PLCF starts at "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {p0, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFieldsPlcfOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 284
    const-string/jumbo v10, " and have length of "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {p0, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFieldsPlcfLength(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 286
    const-string/jumbo v10, "\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 289
    .end local v3    # "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    :cond_2
    aget-object v2, v8, v7

    .line 291
    .local v2, "noteType":Lorg/apache/poi/hwpf/model/NoteType;
    const-string/jumbo v10, "\t\t"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    const-string/jumbo v10, ": descriptions starts "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesDescriptorsOffset(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 295
    const-string/jumbo v10, " and have length of "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesDescriptorsSize(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 297
    const-string/jumbo v10, " bytes\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string/jumbo v10, "\t\t"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 300
    const-string/jumbo v10, ": text positions starts "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesTextPositionsOffset(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 302
    const-string/jumbo v10, " and have length of "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getNotesTextPositionsSize(Lorg/apache/poi/hwpf/model/NoteType;)I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 304
    const-string/jumbo v10, " bytes\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 310
    .end local v2    # "noteType":Lorg/apache/poi/hwpf/model/NoteType;
    :cond_3
    :try_start_1
    aget-object v1, v7, v6

    .line 312
    .local v1, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "get"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 313
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v9

    invoke-static {v9}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 314
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v9

    invoke-static {v9}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v9

    if-nez v9, :cond_4

    .line 315
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v9

    array-length v9, v9

    if-lez v9, :cond_5

    .line 310
    :cond_4
    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    .line 317
    :cond_5
    const-string/jumbo v9, "\t\t"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string/jumbo v9, " => "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 321
    const-string/jumbo v9, "\n"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    .line 324
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 326
    .local v0, "exc":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "(exc: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4
.end method

.method public writeTo([BLorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 4
    .param p1, "mainStream"    # [B
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1061
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->getFieldsCount()I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    .line 1063
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibBase:Lorg/apache/poi/hwpf/model/FibBase;

    invoke-virtual {v1, p1, v3}, Lorg/apache/poi/hwpf/model/FibBase;->serialize([BI)V

    .line 1064
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibBase;->getSize()I

    move-result v0

    .line 1066
    .local v0, "offset":I
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_csw:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 1067
    add-int/lit8 v0, v0, 0x2

    .line 1069
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgW:Lorg/apache/poi/hwpf/model/FibRgW97;

    invoke-virtual {v1, p1, v0}, Lorg/apache/poi/hwpf/model/FibRgW97;->serialize([BI)V

    .line 1070
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgW97;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 1072
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cslw:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 1073
    add-int/lit8 v0, v0, 0x2

    .line 1075
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgLw:Lorg/apache/poi/hwpf/model/FibRgLw;

    check-cast v1, Lorg/apache/poi/hwpf/model/FibRgLw97;

    invoke-virtual {v1, p1, v0}, Lorg/apache/poi/hwpf/model/FibRgLw97;->serialize([BI)V

    .line 1076
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgLw97;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 1078
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 1079
    add-int/lit8 v0, v0, 0x2

    .line 1081
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {v1, p1, v0, p2}, Lorg/apache/poi/hwpf/model/FIBFieldHandler;->writeTo([BILorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 1082
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cbRgFcLcb:I

    mul-int/lit8 v1, v1, 0x4

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1084
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 1085
    add-int/lit8 v0, v0, 0x2

    .line 1086
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_cswNew:I

    if-eqz v1, :cond_0

    .line 1088
    iget v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_nFibNew:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 1089
    add-int/lit8 v0, v0, 0x2

    .line 1091
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgCswNew:[B

    .line 1092
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgCswNew:[B

    array-length v2, v2

    .line 1091
    invoke-static {v1, v3, p1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1093
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FileInformationBlock;->_fibRgCswNew:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 1095
    :cond_0
    return-void
.end method
