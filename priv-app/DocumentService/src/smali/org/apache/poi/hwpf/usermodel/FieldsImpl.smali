.class public Lorg/apache/poi/hwpf/usermodel/FieldsImpl;
.super Ljava/lang/Object;
.source "FieldsImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/Fields;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _fieldsByOffset:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/poi/hwpf/model/FieldsDocumentPart;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hwpf/usermodel/FieldImpl;",
            ">;>;"
        }
    .end annotation
.end field

.field private comparator:Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/FieldsTables;)V
    .locals 7
    .param p1, "fieldsTables"    # Lorg/apache/poi/hwpf/model/FieldsTables;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;-><init>(Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;)V

    iput-object v2, p0, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->comparator:Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;

    .line 104
    new-instance v2, Ljava/util/HashMap;

    .line 105
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v3

    array-length v3, v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 104
    iput-object v2, p0, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->_fieldsByOffset:Ljava/util/Map;

    .line 107
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 112
    return-void

    .line 107
    :cond_0
    aget-object v0, v3, v2

    .line 109
    .local v0, "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/FieldsTables;->getFieldsPLCF(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Ljava/util/ArrayList;

    move-result-object v1

    .line 110
    .local v1, "plexOfCps":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PlexOfField;>;"
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->_fieldsByOffset:Ljava/util/Map;

    invoke-direct {p0, v1}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->parseFieldStructure(Ljava/util/List;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static binarySearch(Ljava/util/List;III)I
    .locals 9
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I
    .param p3, "requiredStartOffset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PlexOfField;",
            ">;III)I"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PlexOfField;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7, p1, p2}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->checkIndexForBinarySearch(III)V

    .line 51
    move v3, p1

    .local v3, "low":I
    const/4 v4, -0x1

    .local v4, "mid":I
    add-int/lit8 v0, p2, -0x1

    .local v0, "high":I
    const/4 v6, 0x0

    .line 52
    .local v6, "result":I
    :goto_0
    if-le v3, v0, :cond_0

    .line 70
    if-gez v4, :cond_5

    .line 72
    move v2, p2

    .line 73
    .local v2, "insertPoint":I
    move v1, p1

    .local v1, "index":I
    :goto_1
    if-lt v1, p2, :cond_3

    .line 80
    neg-int v7, v2

    add-int/lit8 v7, v7, -0x1

    .line 82
    .end local v1    # "index":I
    .end local v2    # "insertPoint":I
    :goto_2
    return v7

    .line 54
    :cond_0
    add-int v7, v3, v0

    ushr-int/lit8 v4, v7, 0x1

    .line 55
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v5

    .line 57
    .local v5, "midStart":I
    if-ne v5, p3, :cond_1

    move v7, v4

    .line 59
    goto :goto_2

    .line 61
    :cond_1
    if-ge v5, p3, :cond_2

    .line 63
    add-int/lit8 v3, v4, 0x1

    .line 64
    goto :goto_0

    .line 67
    :cond_2
    add-int/lit8 v0, v4, -0x1

    goto :goto_0

    .line 75
    .end local v5    # "midStart":I
    .restart local v1    # "index":I
    .restart local v2    # "insertPoint":I
    :cond_3
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v7

    if-ge p3, v7, :cond_4

    .line 77
    move v2, v1

    .line 73
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 82
    .end local v1    # "index":I
    .end local v2    # "insertPoint":I
    :cond_5
    neg-int v8, v4

    if-ltz v6, :cond_6

    const/4 v7, 0x1

    :goto_3
    sub-int v7, v8, v7

    goto :goto_2

    :cond_6
    const/4 v7, 0x2

    goto :goto_3
.end method

.method private static checkIndexForBinarySearch(III)V
    .locals 1
    .param p0, "length"    # I
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 88
    if-le p1, p2, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 92
    :cond_0
    if-lt p0, p2, :cond_1

    if-gez p1, :cond_2

    .line 94
    :cond_1
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 96
    :cond_2
    return-void
.end method

.method private parseFieldStructure(Ljava/util/List;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PlexOfField;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hwpf/usermodel/FieldImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "plexOfFields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PlexOfField;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 150
    :cond_1
    return-object v2

    .line 139
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->comparator:Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;

    invoke-static {p1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 140
    new-instance v1, Ljava/util/ArrayList;

    .line 141
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x3

    add-int/lit8 v3, v3, 0x1

    .line 140
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 142
    .local v1, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/FieldImpl;>;"
    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {p0, p1, v3, v4, v1}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->parseFieldStructureImpl(Ljava/util/List;IILjava/util/List;)V

    .line 144
    new-instance v2, Ljava/util/HashMap;

    .line 145
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 144
    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 146
    .local v2, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lorg/apache/poi/hwpf/usermodel/FieldImpl;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;

    .line 148
    .local v0, "field":Lorg/apache/poi/hwpf/usermodel/FieldImpl;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getFieldStartOffset()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private parseFieldStructureImpl(Ljava/util/List;IILjava/util/List;)V
    .locals 10
    .param p2, "startOffsetInclusive"    # I
    .param p3, "endOffsetExclusive"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PlexOfField;",
            ">;II",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/FieldImpl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "plexOfFields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PlexOfField;>;"
    .local p4, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/FieldImpl;>;"
    move v3, p2

    .line 158
    .local v3, "next":I
    :goto_0
    if-lt v3, p3, :cond_0

    .line 264
    return-void

    .line 160
    :cond_0
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hwpf/model/PlexOfField;

    .line 161
    .local v7, "startPlexOfField":Lorg/apache/poi/hwpf/model/PlexOfField;
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v8

    const/16 v9, 0x13

    if-eq v8, v9, :cond_1

    .line 164
    add-int/lit8 v3, v3, 0x1

    .line 165
    goto :goto_0

    .line 172
    :cond_1
    add-int/lit8 v8, v3, 0x1

    .line 173
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcEnd()I

    move-result v9

    .line 172
    invoke-static {p1, v8, p3, v9}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->binarySearch(Ljava/util/List;III)I

    move-result v4

    .line 174
    .local v4, "nextNodePositionInList":I
    if-gez v4, :cond_2

    .line 180
    add-int/lit8 v3, v3, 0x1

    .line 181
    goto :goto_0

    .line 184
    :cond_2
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/PlexOfField;

    .line 186
    .local v5, "nextPlexOfField":Lorg/apache/poi/hwpf/model/PlexOfField;
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 259
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 190
    :pswitch_0
    move-object v6, v5

    .line 194
    .local v6, "separatorPlexOfField":Lorg/apache/poi/hwpf/model/PlexOfField;
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcEnd()I

    move-result v8

    .line 192
    invoke-static {p1, v4, p3, v8}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->binarySearch(Ljava/util/List;III)I

    move-result v0

    .line 195
    .local v0, "endNodePositionInList":I
    if-gez v0, :cond_3

    .line 201
    add-int/lit8 v3, v3, 0x1

    .line 202
    goto :goto_0

    .line 205
    :cond_3
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/PlexOfField;

    .line 207
    .local v1, "endPlexOfField":Lorg/apache/poi/hwpf/model/PlexOfField;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v8

    const/16 v9, 0x15

    if-eq v8, v9, :cond_4

    .line 210
    add-int/lit8 v3, v3, 0x1

    .line 211
    goto :goto_0

    .line 214
    :cond_4
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/FieldImpl;

    invoke-direct {v2, v7, v6, v1}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;-><init>(Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;)V

    .line 216
    .local v2, "field":Lorg/apache/poi/hwpf/usermodel/FieldImpl;
    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    .line 220
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v8, v9, :cond_5

    .line 222
    add-int/lit8 v8, v3, 0x1

    invoke-direct {p0, p1, v8, v4, p4}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->parseFieldStructureImpl(Ljava/util/List;IILjava/util/List;)V

    .line 225
    :cond_5
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    .line 226
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v8, v9, :cond_6

    .line 229
    add-int/lit8 v8, v4, 0x1

    .line 228
    invoke-direct {p0, p1, v8, v0, p4}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->parseFieldStructureImpl(Ljava/util/List;IILjava/util/List;)V

    .line 233
    :cond_6
    add-int/lit8 v3, v0, 0x1

    .line 235
    goto/16 :goto_0

    .line 240
    .end local v0    # "endNodePositionInList":I
    .end local v1    # "endPlexOfField":Lorg/apache/poi/hwpf/model/PlexOfField;
    .end local v2    # "field":Lorg/apache/poi/hwpf/usermodel/FieldImpl;
    .end local v6    # "separatorPlexOfField":Lorg/apache/poi/hwpf/model/PlexOfField;
    :pswitch_1
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/FieldImpl;

    const/4 v8, 0x0

    invoke-direct {v2, v7, v8, v5}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;-><init>(Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;)V

    .line 242
    .restart local v2    # "field":Lorg/apache/poi/hwpf/usermodel/FieldImpl;
    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    .line 246
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v8, v9, :cond_7

    .line 248
    add-int/lit8 v8, v3, 0x1

    invoke-direct {p0, p1, v8, v4, p4}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->parseFieldStructureImpl(Ljava/util/List;IILjava/util/List;)V

    .line 252
    :cond_7
    add-int/lit8 v3, v4, 0x1

    .line 253
    goto/16 :goto_0

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic getFieldByStartOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)Lorg/apache/poi/hwpf/usermodel/Field;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->getFieldByStartOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)Lorg/apache/poi/hwpf/usermodel/FieldImpl;

    move-result-object v0

    return-object v0
.end method

.method public getFieldByStartOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)Lorg/apache/poi/hwpf/usermodel/FieldImpl;
    .locals 2
    .param p1, "documentPart"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .param p2, "offset"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->_fieldsByOffset:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 127
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lorg/apache/poi/hwpf/usermodel/FieldImpl;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    :cond_0
    const/4 v1, 0x0

    .line 130
    :goto_0
    return-object v1

    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/FieldImpl;

    goto :goto_0
.end method

.method public getFields(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Ljava/util/Collection;
    .locals 2
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/model/FieldsDocumentPart;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Field;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/FieldsImpl;->_fieldsByOffset:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 117
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lorg/apache/poi/hwpf/usermodel/FieldImpl;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    .line 120
    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    goto :goto_0
.end method
