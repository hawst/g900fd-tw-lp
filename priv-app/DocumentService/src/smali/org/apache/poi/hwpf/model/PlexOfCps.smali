.class public final Lorg/apache/poi/hwpf/model/PlexOfCps;
.super Ljava/lang/Object;
.source "PlexOfCps.java"


# instance fields
.field private _cbStruct:I

.field private _iMac:I

.field private _props:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/GenericPropertyNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "sizeOfStruct"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 45
    iput p1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 46
    return-void
.end method

.method public constructor <init>([BIII)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "start"    # I
    .param p3, "cb"    # I
    .param p4, "cbStruct"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    add-int/lit8 v1, p3, -0x4

    add-int/lit8 v2, p4, 0x4

    div-int/2addr v1, v2

    iput v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    .line 61
    iput p4, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    iget v2, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 64
    const/4 v0, 0x0

    .local v0, "x":I
    :goto_0
    iget v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    if-lt v0, v1, :cond_0

    .line 68
    return-void

    .line 66
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I[BI)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getIntOffset(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 165
    mul-int/lit8 v0, p1, 0x4

    return v0
.end method

.method private getProperty(I[BI)Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 6
    .param p1, "index"    # I
    .param p2, "buf"    # [B
    .param p3, "offset"    # I

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getIntOffset(I)I

    move-result v3

    add-int/2addr v3, p3

    invoke-static {p2, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 154
    .local v1, "start":I
    add-int/lit8 v3, p1, 0x1

    invoke-direct {p0, v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getIntOffset(I)I

    move-result v3

    add-int/2addr v3, p3

    invoke-static {p2, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 156
    .local v0, "end":I
    iget v3, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    new-array v2, v3, [B

    .line 157
    .local v2, "struct":[B
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getStructOffset(I)I

    move-result v3

    add-int/2addr v3, p3

    const/4 v4, 0x0

    .line 158
    iget v5, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 157
    invoke-static {p2, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    new-instance v3, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-direct {v3, v1, v0, v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    return-object v3
.end method

.method private getStructOffset(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 190
    iget v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    mul-int/2addr v1, p1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public addProperty(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V
    .locals 1
    .param p1, "node"    # Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    .line 109
    return-void
.end method

.method adjust(II)V
    .locals 3
    .param p1, "startCp"    # I
    .param p2, "shift"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 73
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 98
    return-void

    .line 73
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 75
    .local v0, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v2

    if-le v2, p1, :cond_2

    .line 77
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v2

    add-int/2addr v2, p2

    if-ge v2, p1, :cond_3

    .line 79
    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->setStart(I)V

    .line 86
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v2

    if-lt v2, p1, :cond_0

    .line 88
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v2

    add-int/2addr v2, p2

    if-ge v2, p1, :cond_4

    .line 90
    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->setEnd(I)V

    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->setStart(I)V

    goto :goto_1

    .line 94
    :cond_4
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->setEnd(I)V

    goto :goto_0
.end method

.method public getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    return v0
.end method

.method remove(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 114
    iget v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    .line 115
    return-void
.end method

.method public toByteArray()[B
    .locals 11

    .prologue
    .line 119
    iget-object v7, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 120
    .local v4, "size":I
    add-int/lit8 v7, v4, 0x1

    mul-int/lit8 v2, v7, 0x4

    .line 121
    .local v2, "cpBufSize":I
    iget v7, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    mul-int v5, v7, v4

    .line 122
    .local v5, "structBufSize":I
    add-int v1, v2, v5

    .line 124
    .local v1, "bufSize":I
    new-array v0, v1, [B

    .line 126
    .local v0, "buf":[B
    const/4 v3, 0x0

    .line 127
    .local v3, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    if-lt v6, v4, :cond_1

    .line 141
    if-eqz v3, :cond_0

    .line 143
    mul-int/lit8 v7, v4, 0x4

    .line 144
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v8

    .line 143
    invoke-static {v0, v7, v8}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 147
    :cond_0
    return-object v0

    .line 129
    :cond_1
    iget-object v7, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    check-cast v3, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 132
    .restart local v3    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    mul-int/lit8 v7, v6, 0x4

    .line 133
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v8

    .line 132
    invoke-static {v0, v7, v8}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 136
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v7

    const/4 v8, 0x0

    .line 137
    iget v9, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    mul-int/2addr v9, v6

    add-int/2addr v9, v2

    iget v10, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 136
    invoke-static {v7, v8, v0, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method toPropertiesArray()[Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 198
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "PLCF (cbStruct: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_cbStruct:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; iMac: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/model/PlexOfCps;->_iMac:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
