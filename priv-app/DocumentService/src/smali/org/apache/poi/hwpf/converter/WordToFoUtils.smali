.class public Lorg/apache/poi/hwpf/converter/WordToFoUtils;
.super Lorg/apache/poi/hwpf/converter/AbstractWordUtils;
.source "WordToFoUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;-><init>()V

    return-void
.end method

.method static compactInlines(Lorg/w3c/dom/Element;)V
    .locals 1
    .param p0, "blockElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 33
    const-string/jumbo v0, "fo:inline"

    invoke-static {p0, v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->compactChildNodesR(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public static setBold(Lorg/w3c/dom/Element;Z)V
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "bold"    # Z

    .prologue
    .line 38
    const-string/jumbo v1, "font-weight"

    if-eqz p1, :cond_0

    const-string/jumbo v0, "bold"

    :goto_0
    invoke-interface {p0, v1, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void

    .line 38
    :cond_0
    const-string/jumbo v0, "normal"

    goto :goto_0
.end method

.method public static setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "borderCode"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .param p2, "where"    # Ljava/lang/String;

    .prologue
    .line 44
    if-nez p0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "element is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    :cond_1
    :goto_0
    return-void

    .line 50
    :cond_2
    invoke-static {p2}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    const-string/jumbo v0, "border-style"

    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getBorderType(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string/jumbo v0, "border-color"

    .line 54
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getColor(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string/jumbo v0, "border-width"

    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getBorderWidth(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "border-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-style"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getBorderType(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "border-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-color"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getColor(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "border-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-width"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getBorderWidth(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static setCharactersProperties(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V
    .locals 4
    .param p0, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p1, "inline"    # Lorg/w3c/dom/Element;

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .local v0, "textDecorations":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-static {p1, v1, v2}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getIco24()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 77
    const-string/jumbo v1, "color"

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getIco24()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getColor24(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isCapitalized()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    const-string/jumbo v1, "text-transform"

    const-string/jumbo v2, "uppercase"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isHighlighted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    const-string/jumbo v1, "background-color"

    .line 94
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getHighlightedColor()B

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getColor(I)Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isStrikeThrough()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 98
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 99
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_3
    const-string/jumbo v1, "line-through"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isShadowed()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 104
    const-string/jumbo v1, "text-shadow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v3

    div-int/lit8 v3, v3, 0x18

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 105
    const-string/jumbo v3, "pt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 104
    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSmallCaps()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 109
    const-string/jumbo v1, "font-variant"

    const-string/jumbo v2, "small-caps"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_6
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getSubSuperScriptIndex()S

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 113
    const-string/jumbo v1, "baseline-shift"

    const-string/jumbo v2, "super"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string/jumbo v1, "font-size"

    const-string/jumbo v2, "smaller"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getSubSuperScriptIndex()S

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_8

    .line 118
    const-string/jumbo v1, "baseline-shift"

    const-string/jumbo v2, "sub"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string/jumbo v1, "font-size"

    const-string/jumbo v2, "smaller"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getUnderlineCode()I

    move-result v1

    if-lez v1, :cond_a

    .line 123
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 124
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_9
    const-string/jumbo v1, "underline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isVanished()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 129
    const-string/jumbo v1, "visibility"

    const-string/jumbo v2, "hidden"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_c

    .line 133
    const-string/jumbo v1, "text-decoration"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_c
    return-void
.end method

.method public static setFontFamily(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .locals 1
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "fontFamily"    # Ljava/lang/String;

    .prologue
    .line 140
    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 143
    :cond_0
    const-string/jumbo v0, "font-family"

    invoke-interface {p0, v0, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setFontSize(Lorg/w3c/dom/Element;I)V
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "fontSize"    # I

    .prologue
    .line 148
    const-string/jumbo v0, "font-size"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method public static setIndent(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lorg/w3c/dom/Element;)V
    .locals 3
    .param p0, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p1, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 153
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFirstLineIndent()I

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const-string/jumbo v0, "text-indent"

    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFirstLineIndent()I

    move-result v2

    .line 158
    div-int/lit8 v2, v2, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 157
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromLeft()I

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    const-string/jumbo v0, "start-indent"

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromLeft()I

    move-result v2

    .line 166
    div-int/lit8 v2, v2, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 167
    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 165
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromRight()I

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    const-string/jumbo v0, "end-indent"

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromRight()I

    move-result v2

    .line 174
    div-int/lit8 v2, v2, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 175
    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingBefore()I

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    const-string/jumbo v0, "space-before"

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingBefore()I

    move-result v2

    div-int/lit8 v2, v2, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 182
    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingAfter()I

    move-result v0

    if-eqz v0, :cond_4

    .line 186
    const-string/jumbo v0, "space-after"

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getSpacingAfter()I

    move-result v2

    div-int/lit8 v2, v2, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 188
    const-string/jumbo v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_4
    return-void
.end method

.method public static setItalic(Lorg/w3c/dom/Element;Z)V
    .locals 2
    .param p0, "element"    # Lorg/w3c/dom/Element;
    .param p1, "italic"    # Z

    .prologue
    .line 194
    const-string/jumbo v1, "font-style"

    if-eqz p1, :cond_0

    const-string/jumbo v0, "italic"

    :goto_0
    invoke-interface {p0, v1, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-void

    .line 194
    :cond_0
    const-string/jumbo v0, "normal"

    goto :goto_0
.end method

.method public static setJustification(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lorg/w3c/dom/Element;)V
    .locals 2
    .param p0, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p1, "element"    # Lorg/w3c/dom/Element;

    .prologue
    .line 200
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getJustification()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getJustification(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "justification":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const-string/jumbo v1, "text-align"

    invoke-interface {p1, v1, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_0
    return-void
.end method

.method public static setLanguage(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V
    .locals 2
    .param p0, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p1, "inline"    # Lorg/w3c/dom/Element;

    .prologue
    .line 208
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getLanguageCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getLanguageCode()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->getLanguage(I)Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "language":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    const-string/jumbo v1, "language"

    invoke-interface {p1, v1, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .end local v0    # "language":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static setParagraphProperties(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lorg/w3c/dom/Element;)V
    .locals 2
    .param p0, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p1, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 219
    invoke-static {p0, p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setIndent(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lorg/w3c/dom/Element;)V

    .line 220
    invoke-static {p0, p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setJustification(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lorg/w3c/dom/Element;)V

    .line 222
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "bottom"

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 223
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "left"

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "right"

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 225
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    const-string/jumbo v1, "top"

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->pageBreakBefore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    const-string/jumbo v0, "break-before"

    const-string/jumbo v1, "page"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_0
    const-string/jumbo v0, "hyphenate"

    .line 233
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isAutoHyphenated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->keepOnPage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    const-string/jumbo v0, "keep-together.within-page"

    const-string/jumbo v1, "always"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->keepWithNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    const-string/jumbo v0, "keep-with-next.within-page"

    const-string/jumbo v1, "always"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_2
    const-string/jumbo v0, "linefeed-treatment"

    const-string/jumbo v1, "preserve"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string/jumbo v0, "white-space-collapse"

    const-string/jumbo v1, "false"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    return-void
.end method

.method public static setPictureProperties(Lorg/apache/poi/hwpf/usermodel/Picture;Lorg/w3c/dom/Element;)V
    .locals 9
    .param p0, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;
    .param p1, "graphicElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 252
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getHorizontalScalingFactor()I

    move-result v0

    .line 253
    .local v0, "horizontalScale":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getVerticalScalingFactor()I

    move-result v5

    .line 255
    .local v5, "verticalScale":I
    if-lez v0, :cond_3

    .line 258
    const-string/jumbo v6, "content-width"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v8

    .line 259
    mul-int/2addr v8, v0

    div-int/lit16 v8, v8, 0x3e8

    div-int/lit8 v8, v8, 0x14

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 260
    const-string/jumbo v8, "pt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 258
    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :goto_0
    if-lez v5, :cond_4

    .line 268
    const-string/jumbo v6, "content-height"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v8

    .line 269
    mul-int/2addr v8, v5

    div-int/lit16 v8, v8, 0x3e8

    div-int/lit8 v8, v8, 0x14

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    const-string/jumbo v8, "pt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 268
    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :goto_1
    if-lez v0, :cond_0

    if-gtz v5, :cond_5

    .line 277
    :cond_0
    const-string/jumbo v6, "scaling"

    const-string/jumbo v7, "uniform"

    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :goto_2
    const-string/jumbo v6, "vertical-align"

    const-string/jumbo v7, "text-bottom"

    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropTop()I

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropRight()I

    move-result v6

    if-nez v6, :cond_1

    .line 287
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropBottom()I

    move-result v6

    if-nez v6, :cond_1

    .line 288
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropLeft()I

    move-result v6

    if-eqz v6, :cond_2

    .line 290
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropTop()I

    move-result v6

    div-int/lit8 v4, v6, 0x14

    .line 291
    .local v4, "rectTop":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropRight()I

    move-result v6

    div-int/lit8 v3, v6, 0x14

    .line 292
    .local v3, "rectRight":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropBottom()I

    move-result v6

    div-int/lit8 v1, v6, 0x14

    .line 293
    .local v1, "rectBottom":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropLeft()I

    move-result v6

    div-int/lit8 v2, v6, 0x14

    .line 294
    .local v2, "rectLeft":I
    const-string/jumbo v6, "clip"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "rect("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "pt, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 295
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "pt, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "pt, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 296
    const-string/jumbo v8, "pt)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 294
    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string/jumbo v6, "overflow"

    const-string/jumbo v7, "hidden"

    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .end local v1    # "rectBottom":I
    .end local v2    # "rectLeft":I
    .end local v3    # "rectRight":I
    .end local v4    # "rectTop":I
    :cond_2
    return-void

    .line 263
    :cond_3
    const-string/jumbo v6, "content-width"

    .line 264
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v8

    div-int/lit8 v8, v8, 0x14

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "pt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 263
    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 272
    :cond_4
    const-string/jumbo v6, "content-height"

    .line 273
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v8

    div-int/lit8 v8, v8, 0x14

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "pt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 272
    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 281
    :cond_5
    const-string/jumbo v6, "scaling"

    const-string/jumbo v7, "non-uniform"

    invoke-interface {p1, v6, v7}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static setTableCellProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Lorg/apache/poi/hwpf/usermodel/TableCell;Lorg/w3c/dom/Element;ZZZZ)V
    .locals 8
    .param p0, "tableRow"    # Lorg/apache/poi/hwpf/usermodel/TableRow;
    .param p1, "tableCell"    # Lorg/apache/poi/hwpf/usermodel/TableCell;
    .param p2, "element"    # Lorg/w3c/dom/Element;
    .param p3, "toppest"    # Z
    .param p4, "bottomest"    # Z
    .param p5, "leftest"    # Z
    .param p6, "rightest"    # Z

    .prologue
    const/high16 v7, 0x44b40000    # 1440.0f

    .line 305
    const-string/jumbo v4, "width"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 306
    const-string/jumbo v6, "in"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 305
    invoke-interface {p2, v4, v5}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string/jumbo v4, "padding-start"

    .line 308
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getGapHalf()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "in"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 307
    invoke-interface {p2, v4, v5}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string/jumbo v4, "padding-end"

    .line 310
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getGapHalf()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "in"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 309
    invoke-interface {p2, v4, v5}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 313
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_0

    .line 314
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v3

    .line 316
    .local v3, "top":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 317
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_2

    .line 318
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    .line 321
    .local v0, "bottom":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 322
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_4

    .line 323
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    .line 325
    .local v1, "left":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_2
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 326
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v4

    if-eqz v4, :cond_6

    .line 327
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    .line 330
    .local v2, "right":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :goto_3
    const-string/jumbo v4, "bottom"

    invoke-static {p2, v0, v4}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 331
    const-string/jumbo v4, "left"

    invoke-static {p2, v1, v4}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 332
    const-string/jumbo v4, "right"

    invoke-static {p2, v2, v4}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 333
    const-string/jumbo v4, "top"

    invoke-static {p2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBorder(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/BorderCode;Ljava/lang/String;)V

    .line 334
    return-void

    .line 314
    .end local v0    # "bottom":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .end local v1    # "left":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .end local v2    # "right":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .end local v3    # "top":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v3

    goto :goto_0

    .line 315
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getHorizontalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v3

    goto :goto_0

    .line 318
    .restart local v3    # "top":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    goto :goto_1

    .line 319
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getHorizontalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    goto :goto_1

    .line 323
    .restart local v0    # "bottom":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_4
    if-eqz p5, :cond_5

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    goto :goto_2

    .line 324
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getVerticalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    goto :goto_2

    .line 327
    .restart local v1    # "left":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_6
    if-eqz p6, :cond_7

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    goto :goto_3

    .line 328
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getVerticalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    goto :goto_3
.end method

.method public static setTableRowProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Lorg/w3c/dom/Element;)V
    .locals 4
    .param p0, "tableRow"    # Lorg/apache/poi/hwpf/usermodel/TableRow;
    .param p1, "tableRowElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 339
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRowHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 341
    const-string/jumbo v0, "height"

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getRowHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x44b40000    # 1440.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "in"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 341
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->cantSplit()Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    const-string/jumbo v0, "keep-together.within-column"

    .line 347
    const-string/jumbo v1, "always"

    .line 346
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_1
    return-void
.end method
