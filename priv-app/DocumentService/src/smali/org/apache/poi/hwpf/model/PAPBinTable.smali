.class public Lorg/apache/poi/hwpf/model/PAPBinTable;
.super Ljava/lang/Object;
.source "PAPBinTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected _paragraphs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/poi/hwpf/model/PAPBinTable;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->$assertionsDisabled:Z

    .line 55
    const-class v0, Lorg/apache/poi/hwpf/model/PAPBinTable;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 54
    sput-object v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    .line 55
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 61
    return-void
.end method

.method public constructor <init>([B[B[BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 7
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "dataStream"    # [B
    .param p4, "offset"    # I
    .param p5, "size"    # I
    .param p6, "fcMin"    # I
    .param p7, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 73
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lorg/apache/poi/hwpf/model/PAPBinTable;-><init>([B[B[BIILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 74
    return-void
.end method

.method public constructor <init>([B[B[BIILorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 21
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "dataStream"    # [B
    .param p4, "offset"    # I
    .param p5, "size"    # I
    .param p6, "charIndexTranslator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;

    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 83
    .local v18, "start":J
    new-instance v11, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v4, 0x4

    move-object/from16 v0, p2

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-direct {v11, v0, v1, v2, v4}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 85
    .local v11, "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v12

    .line 86
    .local v12, "length":I
    const/16 v20, 0x0

    .local v20, "x":I
    :goto_0
    const/16 v4, 0xfa

    if-le v12, v4, :cond_4

    const/16 v4, 0xfa

    :goto_1
    move/from16 v0, v20

    if-lt v0, v4, :cond_1

    .line 105
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "PAPX tables loaded in "

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v18

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms ("

    .line 107
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string/jumbo v10, " elements)"

    .line 105
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 109
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x5

    const-string/jumbo v6, "PAPX FKPs are empty"

    invoke-virtual {v4, v5, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 112
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    new-instance v5, Lorg/apache/poi/hwpf/model/PAPX;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v9, 0x2

    invoke-direct {v8, v9}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    invoke-direct {v5, v6, v7, v8}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    :cond_0
    return-void

    .line 87
    :cond_1
    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v13

    .line 89
    .local v13, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v14

    .line 90
    .local v14, "pageNum":I
    mul-int/lit16 v15, v14, 0x200

    .line 93
    .local v15, "pageOffset":I
    new-instance v17, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v2, v15, v3}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;-><init>([B[BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 97
    .local v17, "pfkp":Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getPAPXs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 86
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 97
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/hwpf/model/PAPX;

    .line 99
    .local v16, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    if-eqz v16, :cond_2

    .line 100
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .end local v13    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .end local v14    # "pageNum":I
    .end local v15    # "pageOffset":I
    .end local v16    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    .end local v17    # "pfkp":Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
    :cond_4
    move v4, v12

    .line 86
    goto/16 :goto_1
.end method

.method static rebuild(Ljava/lang/StringBuilder;Lorg/apache/poi/hwpf/model/ComplexFileTable;Ljava/util/List;)V
    .locals 41
    .param p0, "docText"    # Ljava/lang/StringBuilder;
    .param p1, "complexFileTable"    # Lorg/apache/poi/hwpf/model/ComplexFileTable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lorg/apache/poi/hwpf/model/ComplexFileTable;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p2, "paragraphs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 127
    .local v38, "start":J
    if-eqz p1, :cond_1

    .line 129
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getGrpprls()[Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v35

    .line 132
    .local v35, "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v4

    .line 133
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 132
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 171
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    .line 172
    const-string/jumbo v6, "Merged (?) with PAPX from complex file table in "

    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v38

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 174
    const-string/jumbo v8, " ms ("

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 175
    const-string/jumbo v10, " elements in total)"

    .line 171
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 176
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 179
    .end local v35    # "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_1
    new-instance v27, Ljava/util/ArrayList;

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 181
    .local v27, "oldPapxSortedByEndPos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    sget-object v4, Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;

    .line 180
    move-object/from16 v0, v27

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 183
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "PAPX sorted by end position in "

    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v38

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms"

    .line 183
    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 185
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 187
    new-instance v31, Ljava/util/IdentityHashMap;

    invoke-direct/range {v31 .. v31}, Ljava/util/IdentityHashMap;-><init>()V

    .line 189
    .local v31, "papxToFileOrder":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/hwpf/model/PAPX;Ljava/lang/Integer;>;"
    const/4 v14, 0x0

    .line 190
    .local v14, "counter":I
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_7

    .line 195
    new-instance v29, Lorg/apache/poi/hwpf/model/PAPBinTable$1;

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PAPBinTable$1;-><init>(Ljava/util/Map;)V

    .line 205
    .local v29, "papxFileOrderComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/poi/hwpf/model/PAPX;>;"
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "PAPX\'s order map created in "

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v38

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms"

    .line 205
    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 207
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 209
    new-instance v25, Ljava/util/LinkedList;

    invoke-direct/range {v25 .. v25}, Ljava/util/LinkedList;-><init>()V

    .line 210
    .local v25, "newPapxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    const/16 v23, 0x0

    .line 211
    .local v23, "lastParStart":I
    const/16 v22, 0x0

    .line 212
    .local v22, "lastPapxIndex":I
    const/4 v13, 0x0

    .local v13, "charIndex":I
    :goto_2
    invoke-virtual/range {p0 .. p0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lt v13, v4, :cond_8

    .line 305
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->clear()V

    .line 306
    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 308
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "PAPX rebuilded from document text in "

    .line 309
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v38

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms ("

    .line 310
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string/jumbo v10, " elements)"

    .line 308
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 311
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 312
    return-void

    .line 133
    .end local v13    # "charIndex":I
    .end local v14    # "counter":I
    .end local v22    # "lastPapxIndex":I
    .end local v23    # "lastParStart":I
    .end local v25    # "newPapxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .end local v27    # "oldPapxSortedByEndPos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .end local v29    # "papxFileOrderComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .end local v31    # "papxToFileOrder":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/hwpf/model/PAPX;Ljava/lang/Integer;>;"
    .restart local v35    # "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 135
    .local v40, "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getPrm()Lorg/apache/poi/hwpf/model/PropertyModifier;

    move-result-object v33

    .line 136
    .local v33, "prm":Lorg/apache/poi/hwpf/model/PropertyModifier;
    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 138
    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/hwpf/model/PropertyModifier;->getIgrpprl()S

    move-result v20

    .line 140
    .local v20, "igrpprl":I
    if-ltz v20, :cond_3

    move-object/from16 v0, v35

    array-length v5, v0

    move/from16 v0, v20

    if-lt v0, v5, :cond_4

    .line 142
    :cond_3
    sget-object v5, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v6, 0x5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 143
    const-string/jumbo v8, "\'s PRM references to unknown grpprl"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 142
    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 147
    :cond_4
    const/16 v19, 0x0

    .line 148
    .local v19, "hasPap":Z
    aget-object v34, v35, v20

    .line 149
    .local v34, "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-virtual/range {v34 .. v34}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->iterator()Lorg/apache/poi/hwpf/sprm/SprmIterator;

    move-result-object v21

    .line 150
    .local v21, "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_5
    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_6

    .line 160
    :goto_3
    if-eqz v19, :cond_0

    .line 162
    new-instance v26, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v5, 0x2

    move-object/from16 v0, v26

    invoke-direct {v0, v5}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 163
    .local v26, "newSprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-virtual/range {v34 .. v34}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->append([B)V

    .line 165
    new-instance v28, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v5

    .line 166
    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v6

    .line 165
    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-direct {v0, v5, v6, v1}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 167
    .local v28, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 152
    .end local v26    # "newSprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    :cond_6
    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v36

    .line 153
    .local v36, "sprmOperation":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 155
    const/16 v19, 0x1

    .line 156
    goto :goto_3

    .line 190
    .end local v19    # "hasPap":Z
    .end local v20    # "igrpprl":I
    .end local v21    # "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .end local v33    # "prm":Lorg/apache/poi/hwpf/model/PropertyModifier;
    .end local v34    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v35    # "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v36    # "sprmOperation":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    .end local v40    # "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    .restart local v14    # "counter":I
    .restart local v27    # "oldPapxSortedByEndPos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .restart local v31    # "papxToFileOrder":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/hwpf/model/PAPX;Ljava/lang/Integer;>;"
    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lorg/apache/poi/hwpf/model/PAPX;

    .line 192
    .restart local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "counter":I
    .local v15, "counter":I
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v14, v15

    .end local v15    # "counter":I
    .restart local v14    # "counter":I
    goto/16 :goto_1

    .line 214
    .end local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    .restart local v13    # "charIndex":I
    .restart local v22    # "lastPapxIndex":I
    .restart local v23    # "lastParStart":I
    .restart local v25    # "newPapxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .restart local v29    # "papxFileOrderComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/poi/hwpf/model/PAPX;>;"
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v12

    .line 215
    .local v12, "c":C
    const/16 v4, 0xd

    if-eq v12, v4, :cond_9

    const/4 v4, 0x7

    if-eq v12, v4, :cond_9

    const/16 v4, 0xc

    if-eq v12, v4, :cond_9

    .line 212
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 218
    :cond_9
    move/from16 v37, v23

    .line 219
    .local v37, "startInclusive":I
    add-int/lit8 v17, v13, 0x1

    .line 221
    .local v17, "endExclusive":I
    const/4 v11, 0x0

    .line 222
    .local v11, "broken":Z
    new-instance v32, Ljava/util/LinkedList;

    invoke-direct/range {v32 .. v32}, Ljava/util/LinkedList;-><init>()V

    .line 223
    .local v32, "papxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    move/from16 v30, v22

    .line 224
    .local v30, "papxIndex":I
    :goto_5
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v4

    .line 223
    move/from16 v0, v30

    if-lt v0, v4, :cond_b

    .line 242
    :goto_6
    if-nez v11, :cond_a

    .line 244
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v22, v4, -0x1

    .line 247
    :cond_a
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_e

    .line 249
    sget-object v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x5

    const-string/jumbo v6, "Paragraph ["

    .line 250
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string/jumbo v8, "; "

    .line 251
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 252
    const-string/jumbo v10, ") has no PAPX. Creating new one."

    .line 249
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 254
    new-instance v28, Lorg/apache/poi/hwpf/model/PAPX;

    .line 255
    new-instance v4, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 254
    move-object/from16 v0, v28

    move/from16 v1, v37

    move/from16 v2, v17

    invoke-direct {v0, v1, v2, v4}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 256
    .restart local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    move/from16 v23, v17

    .line 259
    goto :goto_4

    .line 226
    .end local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    :cond_b
    const/4 v11, 0x0

    .line 227
    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lorg/apache/poi/hwpf/model/PAPX;

    .line 229
    .restart local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    sget-boolean v4, Lorg/apache/poi/hwpf/model/PAPBinTable;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    if-eqz v37, :cond_c

    .line 230
    add-int/lit8 v4, v30, 0x1

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_c

    .line 231
    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v4

    move/from16 v0, v37

    if-gt v4, v0, :cond_c

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 233
    :cond_c
    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le v4, v13, :cond_d

    .line 235
    move/from16 v22, v30

    .line 236
    const/4 v11, 0x1

    .line 237
    goto :goto_6

    .line 240
    :cond_d
    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_5

    .line 262
    .end local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    :cond_e
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_f

    .line 265
    const/4 v4, 0x0

    move-object/from16 v0, v32

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/poi/hwpf/model/PAPX;

    .line 266
    .local v18, "existing":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v4

    move/from16 v0, v37

    if-ne v4, v0, :cond_f

    .line 267
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v4

    move/from16 v0, v17

    if-ne v4, v0, :cond_f

    .line 269
    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    move/from16 v23, v17

    .line 271
    goto/16 :goto_4

    .line 276
    .end local v18    # "existing":Lorg/apache/poi/hwpf/model/PAPX;
    :cond_f
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 278
    const/16 v34, 0x0

    .line 279
    .restart local v34    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_11

    .line 299
    new-instance v24, Lorg/apache/poi/hwpf/model/PAPX;

    move-object/from16 v0, v24

    move/from16 v1, v37

    move/from16 v2, v17

    move-object/from16 v3, v34

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 300
    .local v24, "newPapx":Lorg/apache/poi/hwpf/model/PAPX;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    move/from16 v23, v17

    goto/16 :goto_4

    .line 279
    .end local v24    # "newPapx":Lorg/apache/poi/hwpf/model/PAPX;
    :cond_11
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lorg/apache/poi/hwpf/model/PAPX;

    .line 281
    .restart local v28    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v5

    if-eqz v5, :cond_10

    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v5

    array-length v5, v5

    if-eqz v5, :cond_10

    .line 284
    if-nez v34, :cond_12

    .line 287
    :try_start_0
    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    move-result-object v34

    .end local v34    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    check-cast v34, Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v34    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    goto :goto_7

    .line 289
    .end local v34    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :catch_0
    move-exception v16

    .line 292
    .local v16, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v4, Ljava/lang/Error;

    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 296
    .end local v16    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v34    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_12
    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v5

    const/4 v6, 0x2

    move-object/from16 v0, v34

    invoke-virtual {v0, v5, v6}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->append([BI)V

    goto :goto_7
.end method


# virtual methods
.method public adjustForDelete(III)V
    .locals 6
    .param p1, "listIndex"    # I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 367
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 368
    .local v3, "size":I
    add-int v1, p2, p3

    .line 369
    .local v1, "endMark":I
    move v0, p1

    .line 371
    .local v0, "endIndex":I
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .line 372
    .local v2, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    if-lt v5, v1, :cond_0

    .line 376
    if-ne p1, v0, :cond_1

    .line 378
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .line 379
    .restart local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v1

    add-int/2addr v5, p2

    invoke-virtual {v2, v5}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 395
    :goto_1
    add-int/lit8 v4, v0, 0x1

    .local v4, "x":I
    :goto_2
    if-lt v4, v3, :cond_3

    .line 401
    return-void

    .line 374
    .end local v4    # "x":I
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .restart local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    goto :goto_0

    .line 383
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .line 384
    .restart local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v2, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 385
    add-int/lit8 v4, p1, 0x1

    .restart local v4    # "x":I
    :goto_3
    if-lt v4, v0, :cond_2

    .line 391
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .line 392
    .restart local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v1

    add-int/2addr v5, p2

    invoke-virtual {v2, v5}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    goto :goto_1

    .line 387
    :cond_2
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .line 388
    .restart local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v2, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setStart(I)V

    .line 389
    invoke-virtual {v2, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 385
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 397
    :cond_3
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/poi/hwpf/model/PAPX;

    .line 398
    .restart local v2    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v2, v5}, Lorg/apache/poi/hwpf/model/PAPX;->setStart(I)V

    .line 399
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v2, v5}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 395
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public adjustForInsert(II)V
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 406
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 407
    .local v1, "size":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    .line 408
    .local v0, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 410
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 416
    return-void

    .line 412
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    .line 413
    .restart local v0    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/PAPX;->setStart(I)V

    .line 414
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 410
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getParagraphs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public insert(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V
    .locals 10
    .param p1, "listIndex"    # I
    .param p2, "cpStart"    # I
    .param p3, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .prologue
    const/4 v9, 0x0

    .line 317
    new-instance v5, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-direct {v5, v9, v9, p3}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 320
    .local v5, "forInsert":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v5, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setStart(I)V

    .line 321
    invoke-virtual {v5, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 323
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne p1, v6, :cond_0

    .line 325
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    :goto_0
    return-void

    .line 329
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/PAPX;

    .line 330
    .local v3, "currentPap":Lorg/apache/poi/hwpf/model/PAPX;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v6

    if-ge v6, p2, :cond_1

    .line 332
    const/4 v2, 0x0

    .line 335
    .local v2, "clonedBuf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :try_start_0
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_1
    new-instance v1, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-direct {v1, v9, v9, v2}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 349
    .local v1, "clone":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v1, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setStart(I)V

    .line 350
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v6

    invoke-virtual {v1, v6}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 352
    invoke-virtual {v3, p2}, Lorg/apache/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 354
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    add-int/lit8 v7, p1, 0x1

    invoke-virtual {v6, v7, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 355
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    add-int/lit8 v7, p1, 0x2

    invoke-virtual {v6, v7, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 337
    .end local v1    # "clone":Lorg/apache/poi/hwpf/model/PAPX;
    :catch_0
    move-exception v4

    .line 339
    .local v4, "exc":Ljava/lang/CloneNotSupportedException;
    const-string/jumbo v6, "DocumentService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Exception: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 359
    .end local v2    # "clonedBuf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v4    # "exc":Ljava/lang/CloneNotSupportedException;
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v6, p1, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public rebuild(Ljava/lang/StringBuilder;Lorg/apache/poi/hwpf/model/ComplexFileTable;)V
    .locals 1
    .param p1, "docText"    # Ljava/lang/StringBuilder;
    .param p2, "complexFileTable"    # Lorg/apache/poi/hwpf/model/ComplexFileTable;

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-static {p1, p2, v0}, Lorg/apache/poi/hwpf/model/PAPBinTable;->rebuild(Ljava/lang/StringBuilder;Lorg/apache/poi/hwpf/model/ComplexFileTable;Ljava/util/List;)V

    .line 120
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;Lorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 3
    .param p1, "sys"    # Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;
    .param p2, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 428
    const-string/jumbo v2, "WordDocument"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v1

    .line 429
    .local v1, "wordDocumentStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    const-string/jumbo v2, "1Table"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    .line 431
    .local v0, "tableStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    invoke-virtual {p0, v1, v0, p2}, Lorg/apache/poi/hwpf/model/PAPBinTable;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 432
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 18
    .param p1, "wordDocumentStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 439
    new-instance v2, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-direct {v2, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    .line 442
    .local v2, "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v4

    .line 443
    .local v4, "docOffset":I
    rem-int/lit16 v8, v4, 0x200

    .line 444
    .local v8, "mod":I
    if-eqz v8, :cond_0

    .line 446
    rsub-int v0, v8, 0x200

    move/from16 v16, v0

    move/from16 v0, v16

    new-array v10, v0, [B

    .line 447
    .local v10, "padding":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 451
    .end local v10    # "padding":[B
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v4

    .line 452
    div-int/lit16 v11, v4, 0x200

    .line 457
    .local v11, "pageNum":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    .line 457
    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/hwpf/model/PAPX;

    .line 458
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v16

    .line 457
    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v6

    .line 460
    .local v6, "endingFc":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 463
    .local v9, "overflow":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hwpf/model/PAPX;>;"
    :goto_0
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/hwpf/model/PAPX;

    .line 466
    .local v15, "startingProp":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v15}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v16

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v14

    .line 468
    .local v14, "start":I
    new-instance v13, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;

    invoke-direct {v13}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;-><init>()V

    .line 469
    .local v13, "pfkp":Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
    invoke-virtual {v13, v9}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->fill(Ljava/util/List;)V

    .line 471
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v13, v0, v1}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->toByteArray(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/CharIndexTranslator;)[B

    move-result-object v3

    .line 472
    .local v3, "bufFkp":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 473
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getOverflow()Ljava/util/ArrayList;

    move-result-object v9

    .line 475
    move v5, v6

    .line 476
    .local v5, "end":I
    if-eqz v9, :cond_1

    .line 479
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v16

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v5

    .line 482
    :cond_1
    const/16 v16, 0x4

    move/from16 v0, v16

    new-array v7, v0, [B

    .line 483
    .local v7, "intHolder":[B
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "pageNum":I
    .local v12, "pageNum":I
    invoke-static {v7, v11}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 484
    new-instance v16, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-object/from16 v0, v16

    invoke-direct {v0, v14, v5, v7}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->addProperty(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 487
    if-nez v9, :cond_2

    .line 488
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v16

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 489
    return-void

    :cond_2
    move v11, v12

    .end local v12    # "pageNum":I
    .restart local v11    # "pageNum":I
    goto :goto_0
.end method
