.class public abstract Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;
.super Ljava/lang/Object;
.source "GrfhicAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final fHtmlBuiltInBullet:Lorg/apache/poi/util/BitField;

.field private static final fHtmlChecked:Lorg/apache/poi/util/BitField;

.field private static final fHtmlFirstLineMismatch:Lorg/apache/poi/util/BitField;

.field private static final fHtmlHangingIndentBeneathNumber:Lorg/apache/poi/util/BitField;

.field private static final fHtmlListTextNotSharpDot:Lorg/apache/poi/util/BitField;

.field private static final fHtmlNotPeriod:Lorg/apache/poi/util/BitField;

.field private static final fHtmlTabLeftIndentMismatch:Lorg/apache/poi/util/BitField;

.field private static final fHtmlUnsupported:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_grfhic:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlChecked:Lorg/apache/poi/util/BitField;

    .line 47
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlUnsupported:Lorg/apache/poi/util/BitField;

    .line 48
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlListTextNotSharpDot:Lorg/apache/poi/util/BitField;

    .line 49
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlNotPeriod:Lorg/apache/poi/util/BitField;

    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlFirstLineMismatch:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlTabLeftIndentMismatch:Lorg/apache/poi/util/BitField;

    .line 52
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlHangingIndentBeneathNumber:Lorg/apache/poi/util/BitField;

    .line 53
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlBuiltInBullet:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    if-ne p0, p1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 90
    goto :goto_0

    .line 91
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 92
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 93
    check-cast v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;

    .line 94
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 95
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 61
    add-int/lit8 v0, p2, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 62
    return-void
.end method

.method public getGrfhic()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 134
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 102
    const/16 v0, 0x1f

    .line 103
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 104
    .local v1, "result":I
    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    add-int/lit8 v1, v2, 0x1f

    .line 105
    return v1
.end method

.method public isFHtmlBuiltInBullet()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 303
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlBuiltInBullet:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlChecked()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 163
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlChecked:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlFirstLineMismatch()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 243
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlFirstLineMismatch:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlHangingIndentBeneathNumber()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 283
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlHangingIndentBeneathNumber:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlListTextNotSharpDot()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 203
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlListTextNotSharpDot:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlNotPeriod()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 223
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlNotPeriod:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlTabLeftIndentMismatch()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 263
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlTabLeftIndentMismatch:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlUnsupported()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlUnsupported:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 66
    add-int/lit8 v0, p2, 0x0

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    aput-byte v1, p1, v0

    .line 67
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 72
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->serialize([BI)V

    .line 73
    return-object v0
.end method

.method public setFHtmlBuiltInBullet(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 293
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlBuiltInBullet:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 294
    return-void
.end method

.method public setFHtmlChecked(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 153
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlChecked:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 154
    return-void
.end method

.method public setFHtmlFirstLineMismatch(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 233
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlFirstLineMismatch:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 234
    return-void
.end method

.method public setFHtmlHangingIndentBeneathNumber(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 273
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlHangingIndentBeneathNumber:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 274
    return-void
.end method

.method public setFHtmlListTextNotSharpDot(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 193
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlListTextNotSharpDot:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 194
    return-void
.end method

.method public setFHtmlNotPeriod(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 213
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlNotPeriod:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 214
    return-void
.end method

.method public setFHtmlTabLeftIndentMismatch(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 253
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlTabLeftIndentMismatch:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 254
    return-void
.end method

.method public setFHtmlUnsupported(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 173
    sget-object v0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->fHtmlUnsupported:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 174
    return-void
.end method

.method public setGrfhic(B)V
    .locals 0
    .param p1, "field_1_grfhic"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 143
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    .line 144
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[Grfhic]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v1, "    .grfhic               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->field_1_grfhic:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v1, "         .fHtmlChecked             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 116
    const-string/jumbo v1, "         .fHtmlUnsupported         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlUnsupported()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v1, "         .fHtmlListTextNotSharpDot     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlListTextNotSharpDot()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v1, "         .fHtmlNotPeriod           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlNotPeriod()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    const-string/jumbo v1, "         .fHtmlFirstLineMismatch     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlFirstLineMismatch()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 120
    const-string/jumbo v1, "         .fHtmlTabLeftIndentMismatch     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlTabLeftIndentMismatch()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 121
    const-string/jumbo v1, "         .fHtmlHangingIndentBeneathNumber     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlHangingIndentBeneathNumber()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v1, "         .fHtmlBuiltInBullet       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;->isFHtmlBuiltInBullet()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 124
    const-string/jumbo v1, "[/Grfhic]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
