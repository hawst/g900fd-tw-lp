.class public final Lorg/apache/poi/hwpf/model/ShapesTable;
.super Ljava/lang/Object;
.source "ShapesTable.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _shapes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Shape;",
            ">;"
        }
    .end annotation
.end field

.field private _shapesVisibili:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Shape;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V
    .locals 7
    .param p1, "tblStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 34
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcspaMom()I

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcspaMom()I

    move-result v5

    const/16 v6, 0x1a

    .line 33
    invoke-direct {v0, p1, v4, v5, v6}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 36
    .local v0, "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lorg/apache/poi/hwpf/model/ShapesTable;->_shapes:Ljava/util/List;

    .line 37
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lorg/apache/poi/hwpf/model/ShapesTable;->_shapesVisibili:Ljava/util/List;

    .line 40
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 48
    return-void

    .line 41
    :cond_0
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 43
    .local v2, "nodo":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/Shape;

    invoke-direct {v3, v2}, Lorg/apache/poi/hwpf/usermodel/Shape;-><init>(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 44
    .local v3, "sh":Lorg/apache/poi/hwpf/usermodel/Shape;
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/ShapesTable;->_shapes:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/Shape;->isWithinDocument()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 46
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/ShapesTable;->_shapesVisibili:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getAllShapes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Shape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ShapesTable;->_shapes:Ljava/util/List;

    return-object v0
.end method

.method public getVisibleShapes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Shape;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ShapesTable;->_shapesVisibili:Ljava/util/List;

    return-object v0
.end method
