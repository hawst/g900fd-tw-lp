.class public Lorg/apache/poi/hwpf/usermodel/Paragraph;
.super Lorg/apache/poi/hwpf/usermodel/Range;
.source "Paragraph.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_ANLD:S = -0x39c2s

.field public static final SPRM_AUTOSPACEDE:S = 0x2437s

.field public static final SPRM_AUTOSPACEDN:S = 0x2438s

.field public static final SPRM_BRCBAR:S = 0x6629s

.field public static final SPRM_BRCBOTTOM:S = 0x6426s

.field public static final SPRM_BRCL:S = 0x2408s

.field public static final SPRM_BRCLEFT:S = 0x6425s

.field public static final SPRM_BRCP:S = 0x2409s

.field public static final SPRM_BRCRIGHT:S = 0x6427s

.field public static final SPRM_BRCTOP:S = 0x6424s

.field public static final SPRM_CHGTABS:S = -0x39ebs

.field public static final SPRM_CHGTABSPAPX:S = -0x39f3s

.field public static final SPRM_CRLF:S = 0x2444s

.field public static final SPRM_DCS:S = 0x442cs

.field public static final SPRM_DXAABS:S = -0x7be8s

.field public static final SPRM_DXAFROMTEXT:S = -0x7bd1s

.field public static final SPRM_DXALEFT:S = -0x7bf1s

.field public static final SPRM_DXALEFT1:S = -0x7befs

.field public static final SPRM_DXARIGHT:S = -0x7bf2s

.field public static final SPRM_DXAWIDTH:S = -0x7be6s

.field public static final SPRM_DYAABS:S = -0x7be7s

.field public static final SPRM_DYAAFTER:S = -0x5becs

.field public static final SPRM_DYABEFORE:S = -0x5beds

.field public static final SPRM_DYAFROMTEXT:S = -0x7bd2s

.field public static final SPRM_DYALINE:S = 0x6412s

.field public static final SPRM_FADJUSTRIGHT:S = 0x2448s

.field public static final SPRM_FBIDI:S = 0x2441s

.field public static final SPRM_FINTABLE:S = 0x2416s

.field public static final SPRM_FKEEP:S = 0x2405s

.field public static final SPRM_FKEEPFOLLOW:S = 0x2406s

.field public static final SPRM_FKINSOKU:S = 0x2433s

.field public static final SPRM_FLOCKED:S = 0x2430s

.field public static final SPRM_FNOAUTOHYPH:S = 0x242as

.field public static final SPRM_FNOLINENUMB:S = 0x240cs

.field public static final SPRM_FNUMRMLNS:S = 0x2443s

.field public static final SPRM_FOVERFLOWPUNCT:S = 0x2435s

.field public static final SPRM_FPAGEBREAKBEFORE:S = 0x2407s

.field public static final SPRM_FRAMETEXTFLOW:S = 0x443as

.field public static final SPRM_FSIDEBYSIDE:S = 0x2404s

.field public static final SPRM_FTOPLINEPUNCT:S = 0x2436s

.field public static final SPRM_FTTP:S = 0x2417s

.field public static final SPRM_FWIDOWCONTROL:S = 0x2431s

.field public static final SPRM_FWORDWRAP:S = 0x2434s

.field public static final SPRM_ILFO:S = 0x460bs

.field public static final SPRM_ILVL:S = 0x260as

.field public static final SPRM_JC:S = 0x2403s

.field public static final SPRM_NUMRM:S = -0x39bbs

.field public static final SPRM_OUTLVL:S = 0x2640s

.field public static final SPRM_PC:S = 0x261bs

.field public static final SPRM_PROPRMARK:S = -0x39c1s

.field public static final SPRM_RULER:S = -0x39ces

.field public static final SPRM_SHD:S = -0x39b3s

.field public static final SPRM_SHD80:S = 0x442ds

.field public static final SPRM_USEPGSUSETTINGS:S = 0x2447s

.field public static final SPRM_WALIGNFONT:S = 0x4439s

.field public static final SPRM_WHEIGHTABS:S = 0x442bs

.field public static final SPRM_WR:S = 0x2423s

.field private static log:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected _istd:S

.field protected _papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

.field protected _props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->log:Lorg/apache/poi/util/POILogger;

    .line 96
    return-void
.end method

.method protected constructor <init>(IILorg/apache/poi/hwpf/usermodel/Table;)V
    .locals 3
    .param p1, "startIdxInclusive"    # I
    .param p2, "endIdxExclusive"    # I
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/Table;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 170
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->initAll()V

    .line 171
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_paragraphs:Ljava/util/List;

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_parEnd:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    .line 172
    .local v0, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/PAPX;->getParagraphProperties(Lorg/apache/poi/hwpf/model/StyleSheet;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 173
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 174
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v1

    iput-short v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 175
    return-void
.end method

.method constructor <init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "properties"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 200
    iget v0, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 201
    iget v1, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v2

    .line 200
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 201
    invoke-direct {p0, v0, v1, p3}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 202
    iput-object p2, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 203
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 204
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 205
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 180
    iget v0, p2, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 181
    iget v1, p2, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v2

    .line 180
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 181
    invoke-direct {p0, v0, v1, p2}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 182
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/PAPX;->getParagraphProperties(Lorg/apache/poi/hwpf/model/StyleSheet;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 183
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 184
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 185
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/Range;I)V
    .locals 3
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p3, "start"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 190
    iget v0, p2, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p2, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 191
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v2

    .line 190
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 191
    invoke-direct {p0, v0, v1, p2}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 192
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/PAPX;->getParagraphProperties(Lorg/apache/poi/hwpf/model/StyleSheet;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 193
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 194
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 195
    return-void
.end method

.method private getFrameTextFlow()S
    .locals 2

    .prologue
    .line 638
    const/4 v0, 0x0

    .line 639
    .local v0, "retVal":S
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFVertical()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 641
    const/4 v1, 0x1

    int-to-short v0, v1

    .line 643
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFBackward()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 645
    or-int/lit8 v1, v0, 0x2

    int-to-short v0, v1

    .line 647
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFRotateFont()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 649
    or-int/lit8 v1, v0, 0x4

    int-to-short v0, v1

    .line 651
    :cond_2
    return v0
.end method

.method static newParagraph(Lorg/apache/poi/hwpf/usermodel/Range;Lorg/apache/poi/hwpf/model/PAPX;)Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .locals 13
    .param p0, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    const/4 v12, 0x2

    .line 101
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .line 102
    .local v0, "doc":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getListTables()Lorg/apache/poi/hwpf/model/ListTables;

    move-result-object v4

    .line 103
    .local v4, "listTables":Lorg/apache/poi/hwpf/model/ListTables;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v6

    .line 105
    .local v6, "styleSheet":Lorg/apache/poi/hwpf/model/StyleSheet;
    new-instance v5, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-direct {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 106
    .local v5, "properties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v7

    invoke-virtual {v5, v7}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIstd(I)V

    .line 108
    invoke-static {v6, p1, v5}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->newParagraph_applyStyleProperties(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v5

    .line 111
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v7

    .line 110
    invoke-static {v5, v7, v12}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v5

    .line 113
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v4, :cond_0

    .line 115
    const/4 v2, 0x0

    .line 118
    .local v2, "lfo":Lorg/apache/poi/hwpf/model/LFO;
    :try_start_0
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v7

    invoke-virtual {v4, v7}, Lorg/apache/poi/hwpf/model/ListTables;->getLfo(I)Lorg/apache/poi/hwpf/model/LFO;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 125
    :goto_0
    if-eqz v2, :cond_0

    .line 127
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/LFO;->getLsid()I

    move-result v7

    .line 128
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlvl()B

    move-result v8

    .line 127
    invoke-virtual {v4, v7, v8}, Lorg/apache/poi/hwpf/model/ListTables;->getLevel(II)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v3

    .line 130
    .local v3, "listLevel":Lorg/apache/poi/hwpf/model/ListLevel;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/ListLevel;->getGrpprlPapx()[B

    move-result-object v7

    if-eqz v7, :cond_0

    .line 133
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/ListLevel;->getGrpprlPapx()[B

    move-result-object v7

    const/4 v8, 0x0

    .line 132
    invoke-static {v5, v7, v8}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v5

    .line 135
    invoke-static {v6, p1, v5}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->newParagraph_applyStyleProperties(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v5

    .line 138
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v7

    .line 137
    invoke-static {v5, v7, v12}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v5

    .line 143
    .end local v2    # "lfo":Lorg/apache/poi/hwpf/model/LFO;
    .end local v3    # "listLevel":Lorg/apache/poi/hwpf/model/ListLevel;
    :cond_0
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v7

    if-lez v7, :cond_1

    .line 144
    new-instance v7, Lorg/apache/poi/hwpf/usermodel/ListEntry;

    invoke-direct {v7, p1, v5, p0}, Lorg/apache/poi/hwpf/usermodel/ListEntry;-><init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 146
    :goto_1
    return-object v7

    .line 120
    .restart local v2    # "lfo":Lorg/apache/poi/hwpf/model/LFO;
    :catch_0
    move-exception v1

    .line 122
    .local v1, "exc":Ljava/util/NoSuchElementException;
    sget-object v7, Lorg/apache/poi/hwpf/usermodel/Paragraph;->log:Lorg/apache/poi/util/POILogger;

    const/4 v8, 0x5

    const-string/jumbo v9, "Paragraph refers to LFO #"

    .line 123
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string/jumbo v11, " that does not exists"

    .line 122
    invoke-virtual {v7, v8, v9, v10, v11}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 146
    .end local v1    # "exc":Ljava/util/NoSuchElementException;
    .end local v2    # "lfo":Lorg/apache/poi/hwpf/model/LFO;
    :cond_1
    new-instance v7, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    invoke-direct {v7, p1, v5, p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;-><init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/Range;)V

    goto :goto_1
.end method

.method protected static newParagraph_applyStyleProperties(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .locals 3
    .param p0, "styleSheet"    # Lorg/apache/poi/hwpf/model/StyleSheet;
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "properties"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .prologue
    .line 152
    if-nez p0, :cond_0

    .line 157
    .end local p2    # "properties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :goto_0
    return-object p2

    .line 155
    .restart local p2    # "properties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v1

    .line 156
    .local v1, "style":I
    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/model/StyleSheet;->getPAPX(I)[B

    move-result-object v0

    .line 157
    .local v0, "grpprl":[B
    const/4 v2, 0x2

    invoke-static {p2, v0, v2}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object p2

    goto :goto_0
.end method

.method private setTableRowEnd(Z)V
    .locals 2
    .param p1, "val"    # Z

    .prologue
    .line 571
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFTtp(Z)V

    .line 572
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2417

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 573
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 629
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    .line 630
    .local v0, "p":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 632
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 633
    return-object v0
.end method

.method public cloneProperties()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .locals 2

    .prologue
    .line 620
    :try_start_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 621
    :catch_0
    move-exception v0

    .line 622
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getBarBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getDropCap()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v0

    return-object v0
.end method

.method public getFirstLineIndent()I
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft1()I

    move-result v0

    return v0
.end method

.method public getFontAlignment()I
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWAlignFont()I

    move-result v0

    return v0
.end method

.method public getIlfo()I
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v0

    return v0
.end method

.method public getIlvl()I
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlvl()B

    move-result v0

    return v0
.end method

.method public getIndentFromLeft()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft()I

    move-result v0

    return v0
.end method

.method public getIndentFromRight()I
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaRight()I

    move-result v0

    return v0
.end method

.method public getJustification()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v0

    return v0
.end method

.method public getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getLineSpacing()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getList()Lorg/apache/poi/hwpf/usermodel/HWPFList;
    .locals 4

    .prologue
    .line 599
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlfo()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlfo()I

    move-result v1

    const v2, 0xf801

    if-ne v1, v2, :cond_1

    .line 601
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Paragraph not in list"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 603
    :cond_1
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/HWPFList;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v1

    .line 604
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getListTables()Lorg/apache/poi/hwpf/model/ListTables;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlfo()I

    move-result v3

    .line 603
    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/HWPFList;-><init>(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/model/ListTables;I)V

    .line 605
    .local v0, "hwpfList":Lorg/apache/poi/hwpf/usermodel/HWPFList;
    return-object v0
.end method

.method public getLvl()I
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLvl()B

    move-result v0

    return v0
.end method

.method public getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getShading()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getSpacingAfter()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAfter()I

    move-result v0

    return v0
.end method

.method public getSpacingBefore()I
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaBefore()I

    move-result v0

    return v0
.end method

.method public getStyleIndex()S
    .locals 1

    .prologue
    .line 209
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_istd:S

    return v0
.end method

.method public getTabStopsNumber()I
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItbdMac()I

    move-result v0

    return v0
.end method

.method public getTabStopsPositions()[I
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgdxaTab()[I

    move-result-object v0

    return-object v0
.end method

.method public getTableLevel()I
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItap()I

    move-result v0

    return v0
.end method

.method public getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public isAutoHyphenated()Z
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoAutoHyph()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isBackward()Z
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFBackward()Z

    move-result v0

    return v0
.end method

.method public isEmbeddedCellMark()Z
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInnerTableCell()Z

    move-result v0

    return v0
.end method

.method public isInList()Z
    .locals 2

    .prologue
    .line 610
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlfo()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlfo()I

    move-result v0

    const v1, 0xf801

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInTable()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInTable()Z

    move-result v0

    return v0
.end method

.method public isKinsoku()Z
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKinsoku()Z

    move-result v0

    return v0
.end method

.method public isLineNotNumbered()Z
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoLnn()Z

    move-result v0

    return v0
.end method

.method public isSideBySide()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFSideBySide()Z

    move-result v0

    return v0
.end method

.method public isTableRowEnd()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtp()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtpEmbedded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isVertical()Z
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFVertical()Z

    move-result v0

    return v0
.end method

.method public isWidowControlled()Z
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWidowControl()Z

    move-result v0

    return v0
.end method

.method public isWordWrapped()Z
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWordWrap()Z

    move-result v0

    return v0
.end method

.method public keepOnPage()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeep()Z

    move-result v0

    return v0
.end method

.method public keepWithNext()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeepFollow()Z

    move-result v0

    return v0
.end method

.method public pageBreakBefore()Z
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPageBreakBefore()Z

    move-result v0

    return v0
.end method

.method public setAutoHyphenated(Z)V
    .locals 4
    .param p1, "autoHyph"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 319
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFNoAutoHyph(Z)V

    .line 320
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v3, 0x242a

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v3, v1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 321
    return-void

    :cond_0
    move v0, v2

    .line 319
    goto :goto_0

    :cond_1
    move v1, v2

    .line 320
    goto :goto_1
.end method

.method public setBackward(Z)V
    .locals 3
    .param p1, "bward"    # Z

    .prologue
    .line 451
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFBackward(Z)V

    .line 452
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x443a

    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFrameTextFlow()S

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 453
    return-void
.end method

.method public setBarBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 3
    .param p1, "bar"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 506
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBar(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 507
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6629

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 508
    return-void
.end method

.method public setBottomBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 3
    .param p1, "bottom"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 484
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 485
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6426

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 486
    return-void
.end method

.method public setDropCap(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V
    .locals 3
    .param p1, "dcs"    # Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    .prologue
    .line 529
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDcs(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V

    .line 530
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x442c

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->toShort()S

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 531
    return-void
.end method

.method public setFirstLineIndent(I)V
    .locals 3
    .param p1, "first"    # I

    .prologue
    .line 363
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft1(I)V

    .line 364
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x7bef

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 365
    return-void
.end method

.method public setFontAlignment(I)V
    .locals 3
    .param p1, "align"    # I

    .prologue
    .line 429
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setWAlignFont(I)V

    .line 430
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x4439

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 431
    return-void
.end method

.method public setIndentFromLeft(I)V
    .locals 3
    .param p1, "dxaLeft"    # I

    .prologue
    .line 352
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft(I)V

    .line 353
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x7bf1

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 354
    return-void
.end method

.method public setIndentFromRight(I)V
    .locals 3
    .param p1, "dxaRight"    # I

    .prologue
    .line 341
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaRight(I)V

    .line 342
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x7bf2

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 343
    return-void
.end method

.method public setJustification(B)V
    .locals 2
    .param p1, "jc"    # B

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setJc(B)V

    .line 254
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2403

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 255
    return-void
.end method

.method public setKeepOnPage(Z)V
    .locals 2
    .param p1, "fKeep"    # Z

    .prologue
    .line 264
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFKeep(Z)V

    .line 265
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2405

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 266
    return-void
.end method

.method public setKeepWithNext(Z)V
    .locals 2
    .param p1, "fKeepFollow"    # Z

    .prologue
    .line 275
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFKeepFollow(Z)V

    .line 276
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2406

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 277
    return-void
.end method

.method public setKinsoku(Z)V
    .locals 2
    .param p1, "kinsoku"    # Z

    .prologue
    .line 407
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFKinsoku(Z)V

    .line 408
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2433

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 409
    return-void
.end method

.method public setLeftBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 3
    .param p1, "left"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 473
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 474
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6425

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 475
    return-void
.end method

.method public setLineNotNumbered(Z)V
    .locals 2
    .param p1, "fNoLnn"    # Z

    .prologue
    .line 297
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFNoLnn(Z)V

    .line 298
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x240c

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 299
    return-void
.end method

.method public setLineSpacing(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V
    .locals 3
    .param p1, "lspd"    # Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    .prologue
    .line 374
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setLspd(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V

    .line 375
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6412

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 376
    return-void
.end method

.method public setPageBreakBefore(Z)V
    .locals 2
    .param p1, "fPageBreak"    # Z

    .prologue
    .line 286
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFPageBreakBefore(Z)V

    .line 287
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2407

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 288
    return-void
.end method

.method public setRightBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 3
    .param p1, "right"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 495
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 496
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6427

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 497
    return-void
.end method

.method public setShading(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 3
    .param p1, "shd"    # Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .prologue
    .line 517
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 519
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x39b3

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->serialize()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->addSprm(S[B)V

    .line 520
    return-void
.end method

.method public setSideBySide(Z)V
    .locals 2
    .param p1, "fSideBySide"    # Z

    .prologue
    .line 308
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFSideBySide(Z)V

    .line 309
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2404

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 310
    return-void
.end method

.method public setSpacingAfter(I)V
    .locals 3
    .param p1, "after"    # I

    .prologue
    .line 396
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaAfter(I)V

    .line 397
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x5bec

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 398
    return-void
.end method

.method public setSpacingBefore(I)V
    .locals 3
    .param p1, "before"    # I

    .prologue
    .line 385
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaBefore(I)V

    .line 386
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x5bed

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 387
    return-void
.end method

.method setTableRowEnd(Lorg/apache/poi/hwpf/usermodel/TableProperties;)V
    .locals 2
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .prologue
    .line 564
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->setTableRowEnd(Z)V

    .line 565
    invoke-static {p1}, Lorg/apache/poi/hwpf/sprm/TableSprmCompressor;->compressTableProperty(Lorg/apache/poi/hwpf/usermodel/TableProperties;)[B

    move-result-object v0

    .line 566
    .local v0, "grpprl":[B
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->append([B)V

    .line 567
    return-void
.end method

.method public setTopBorder(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 3
    .param p1, "top"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 462
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 463
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x6424

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 464
    return-void
.end method

.method public setVertical(Z)V
    .locals 3
    .param p1, "vertical"    # Z

    .prologue
    .line 440
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFVertical(Z)V

    .line 441
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x443a

    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFrameTextFlow()S

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 442
    return-void
.end method

.method public setWidowControl(Z)V
    .locals 2
    .param p1, "widowControl"    # Z

    .prologue
    .line 330
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFWidowControl(Z)V

    .line 331
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2431

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 332
    return-void
.end method

.method public setWordWrapped(Z)V
    .locals 2
    .param p1, "wrap"    # Z

    .prologue
    .line 418
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_props:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFWordWrap(Z)V

    .line 419
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2434

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 420
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 657
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Paragraph ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getStartOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getEndOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method
