.class public abstract Lorg/apache/poi/hwpf/model/types/SHDAbstractType;
.super Ljava/lang/Object;
.source "SHDAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

.field protected field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

.field protected field_3_ipat:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Colorref;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    .line 51
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Colorref;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    .line 52
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0xa

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p0, p1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v1

    .line 88
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 89
    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 91
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 92
    check-cast v0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;

    .line 93
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/SHDAbstractType;
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 94
    goto :goto_0

    .line 95
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 96
    goto :goto_0

    .line 97
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 98
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 56
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    add-int/lit8 v1, p2, 0x0

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    .line 57
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    add-int/lit8 v1, p2, 0x4

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    .line 58
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    .line 59
    return-void
.end method

.method public getCvBack()Lorg/apache/poi/hwpf/model/Colorref;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    return-object v0
.end method

.method public getCvFore()Lorg/apache/poi/hwpf/model/Colorref;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    return-object v0
.end method

.method public getIpat()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 170
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 105
    const/16 v0, 0x1f

    .line 106
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 107
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Colorref;->hashCode()I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 108
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/Colorref;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 109
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    add-int v1, v2, v3

    .line 110
    return v1
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    add-int/lit8 v1, p2, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Colorref;->serialize([BI)V

    .line 64
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    add-int/lit8 v1, p2, 0x4

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Colorref;->serialize([BI)V

    .line 65
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 66
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 71
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->serialize([BI)V

    .line 72
    return-object v0
.end method

.method public setCvBack(Lorg/apache/poi/hwpf/model/Colorref;)V
    .locals 0
    .param p1, "field_2_cvBack"    # Lorg/apache/poi/hwpf/model/Colorref;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 161
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_2_cvBack:Lorg/apache/poi/hwpf/model/Colorref;

    .line 162
    return-void
.end method

.method public setCvFore(Lorg/apache/poi/hwpf/model/Colorref;)V
    .locals 0
    .param p1, "field_1_cvFore"    # Lorg/apache/poi/hwpf/model/Colorref;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 143
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_1_cvFore:Lorg/apache/poi/hwpf/model/Colorref;

    .line 144
    return-void
.end method

.method public setIpat(I)V
    .locals 0
    .param p1, "field_3_ipat"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 179
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->field_3_ipat:I

    .line 180
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[SHD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v1, "    .cvFore               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->getCvFore()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string/jumbo v1, "    .cvBack               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->getCvBack()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string/jumbo v1, "    .ipat                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;->getIpat()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string/jumbo v1, "[/SHD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
