.class public final enum Lorg/apache/poi/hwpf/model/FSPADocumentPart;
.super Ljava/lang/Enum;
.source "FSPADocumentPart.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hwpf/model/FSPADocumentPart;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hwpf/model/FSPADocumentPart;

.field public static final enum HEADER:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

.field public static final enum MAIN:Lorg/apache/poi/hwpf/model/FSPADocumentPart;


# instance fields
.field private final fibFieldsField:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    const-string/jumbo v1, "HEADER"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/poi/hwpf/model/FSPADocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->HEADER:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    .line 25
    new-instance v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    const-string/jumbo v1, "MAIN"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v4, v2}, Lorg/apache/poi/hwpf/model/FSPADocumentPart;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    sget-object v1, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->HEADER:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->ENUM$VALUES:[Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "fibHandlerField"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->fibFieldsField:I

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/FSPADocumentPart;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/hwpf/model/FSPADocumentPart;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->ENUM$VALUES:[Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getFibFieldsField()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lorg/apache/poi/hwpf/model/FSPADocumentPart;->fibFieldsField:I

    return v0
.end method
