.class public Lorg/apache/poi/hwpf/model/TabDescriptor;
.super Lorg/apache/poi/hwpf/model/types/TBDAbstractType;
.source "TabDescriptor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;-><init>()V

    .line 36
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/TabDescriptor;->fillFields([BI)V

    .line 37
    return-void
.end method


# virtual methods
.method public toByteArray()[B
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lorg/apache/poi/hwpf/model/TabDescriptor;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 42
    .local v0, "buf":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/TabDescriptor;->serialize([BI)V

    .line 43
    return-object v0
.end method
