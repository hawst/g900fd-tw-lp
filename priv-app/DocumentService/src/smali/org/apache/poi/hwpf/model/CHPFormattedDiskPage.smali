.class public final Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;
.super Lorg/apache/poi/hwpf/model/FormattedDiskPage;
.source "CHPFormattedDiskPage.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final FC_SIZE:I = 0x4


# instance fields
.field private _chpxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field

.field private _overFlow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/FormattedDiskPage;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    .line 55
    return-void
.end method

.method public constructor <init>([BIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 0
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "fcMin"    # I
    .param p4, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p4}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;-><init>([BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 70
    return-void
.end method

.method public constructor <init>([BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 14
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;

    .prologue
    .line 79
    invoke-direct/range {p0 .. p2}, Lorg/apache/poi/hwpf/model/FormattedDiskPage;-><init>([BI)V

    .line 49
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    .line 81
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    iget v6, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_crun:I

    if-lt v5, v6, :cond_0

    .line 98
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0, v5}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->getStart(I)I

    move-result v2

    .line 84
    .local v2, "bytesStartAt":I
    invoke-virtual {p0, v5}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->getEnd(I)I

    move-result v1

    .line 90
    .local v1, "bytesEndAt":I
    move-object/from16 v0, p3

    invoke-interface {v0, v2, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getCharIndexRanges(II)[[I

    move-result-object v7

    array-length v8, v7

    const/4 v6, 0x0

    :goto_1
    if-lt v6, v8, :cond_1

    .line 81
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 90
    :cond_1
    aget-object v4, v7, v6

    .line 93
    .local v4, "range":[I
    new-instance v3, Lorg/apache/poi/hwpf/model/CHPX;

    const/4 v9, 0x0

    aget v9, v4, v9

    const/4 v10, 0x1

    aget v10, v4, v10

    new-instance v11, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 94
    invoke-virtual {p0, v5}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->getGrpprl(I)[B

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v11, v12, v13}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 93
    invoke-direct {v3, v9, v10, v11}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 95
    .local v3, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    iget-object v9, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method


# virtual methods
.method public fill(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "filler":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 113
    return-void
.end method

.method public getCHPX(I)Lorg/apache/poi/hwpf/model/CHPX;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    return-object v0
.end method

.method public getCHPXs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getGrpprl(I)[B
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v6, 0x0

    .line 128
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_offset:I

    iget v5, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_crun:I

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v5, v5, 0x4

    add-int/2addr v5, p1

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 131
    .local v1, "chpxOffset":I
    if-nez v1, :cond_0

    .line 133
    new-array v0, v6, [B

    .line 141
    :goto_0
    return-object v0

    .line 136
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_offset:I

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v2

    .line 138
    .local v2, "size":I
    new-array v0, v2, [B

    .line 140
    .local v0, "chpx":[B
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_offset:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v4, v1

    invoke-static {v3, v4, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public getOverflow()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected toByteArray(Lorg/apache/poi/hwpf/model/CharIndexTranslator;)[B
    .locals 14
    .param p1, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;

    .prologue
    .line 156
    const/16 v11, 0x200

    new-array v0, v11, [B

    .line 157
    .local v0, "buf":[B
    iget-object v11, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 158
    .local v8, "size":I
    const/16 v5, 0x1ff

    .line 159
    .local v5, "grpprlOffset":I
    const/4 v7, 0x0

    .line 160
    .local v7, "offsetOffset":I
    const/4 v2, 0x0

    .line 163
    .local v2, "fcOffset":I
    const/4 v9, 0x6

    .line 165
    .local v9, "totalSize":I
    const/4 v6, 0x0

    .line 166
    .local v6, "index":I
    :goto_0
    if-lt v6, v8, :cond_2

    .line 190
    :goto_1
    if-eq v6, v8, :cond_0

    .line 192
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    .line 193
    iget-object v11, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    iget-object v12, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v12, v6, v8}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 197
    :cond_0
    const/16 v11, 0x1ff

    int-to-byte v12, v6

    aput-byte v12, v0, v11

    .line 199
    mul-int/lit8 v11, v6, 0x4

    add-int/lit8 v7, v11, 0x4

    .line 202
    const/4 v1, 0x0

    .line 203
    .local v1, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    const/4 v10, 0x0

    .local v10, "x":I
    :goto_2
    if-lt v10, v6, :cond_5

    .line 222
    if-eqz v1, :cond_1

    .line 225
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v11

    invoke-interface {p1, v11}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v11

    .line 224
    invoke-static {v0, v2, v11}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 228
    :cond_1
    return-object v0

    .line 168
    .end local v1    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v10    # "x":I
    :cond_2
    iget-object v11, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v11

    array-length v4, v11

    .line 172
    .local v4, "grpprlLength":I
    add-int/lit8 v11, v4, 0x6

    add-int/2addr v9, v11

    .line 176
    rem-int/lit8 v11, v6, 0x2

    add-int/lit16 v11, v11, 0x1ff

    if-le v9, v11, :cond_3

    .line 178
    add-int/lit8 v11, v4, 0x6

    sub-int/2addr v9, v11

    .line 179
    goto :goto_1

    .line 183
    :cond_3
    add-int/lit8 v11, v4, 0x1

    rem-int/lit8 v11, v11, 0x2

    if-lez v11, :cond_4

    .line 185
    add-int/lit8 v9, v9, 0x1

    .line 166
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 205
    .end local v4    # "grpprlLength":I
    .restart local v1    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .restart local v10    # "x":I
    :cond_5
    iget-object v11, p0, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v1, Lorg/apache/poi/hwpf/model/CHPX;

    .line 206
    .restart local v1    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v3

    .line 209
    .local v3, "grpprl":[B
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v11

    invoke-interface {p1, v11}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v11

    .line 208
    invoke-static {v0, v2, v11}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 211
    array-length v11, v3

    add-int/lit8 v11, v11, 0x1

    sub-int/2addr v5, v11

    .line 212
    rem-int/lit8 v11, v5, 0x2

    sub-int/2addr v5, v11

    .line 213
    div-int/lit8 v11, v5, 0x2

    int-to-byte v11, v11

    aput-byte v11, v0, v7

    .line 214
    array-length v11, v3

    int-to-byte v11, v11

    aput-byte v11, v0, v5

    .line 215
    const/4 v11, 0x0

    add-int/lit8 v12, v5, 0x1

    array-length v13, v3

    invoke-static {v3, v11, v0, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 217
    add-int/lit8 v7, v7, 0x1

    .line 218
    add-int/lit8 v2, v2, 0x4

    .line 203
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method protected toByteArray(Lorg/apache/poi/hwpf/model/CharIndexTranslator;I)[B
    .locals 1
    .param p1, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .param p2, "fcMin"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->toByteArray(Lorg/apache/poi/hwpf/model/CharIndexTranslator;)[B

    move-result-object v0

    return-object v0
.end method
