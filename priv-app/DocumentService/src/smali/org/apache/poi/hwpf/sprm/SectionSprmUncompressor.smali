.class public final Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;
.super Lorg/apache/poi/hwpf/sprm/SprmUncompressor;
.source "SectionSprmUncompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/poi/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 29
    return-void
.end method

.method static unCompressSEPOperation(Lorg/apache/poi/hwpf/usermodel/SectionProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    .locals 5
    .param p0, "newSEP"    # Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    .param p1, "sprm"    # Lorg/apache/poi/hwpf/sprm/SprmOperation;

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 215
    :goto_0
    :pswitch_0
    return-void

    .line 59
    :pswitch_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setCnsPgn(B)V

    goto :goto_0

    .line 62
    :pswitch_2
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setIHeadingPgn(B)V

    goto :goto_0

    .line 65
    :pswitch_3
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    new-array v0, v2, [B

    .line 66
    .local v0, "buf":[B
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    array-length v4, v0

    invoke-static {v2, v3, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setOlstAnm([B)V

    goto :goto_0

    .line 76
    .end local v0    # "buf":[B
    :pswitch_4
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFEvenlySpaced(Z)V

    goto :goto_0

    .line 79
    :pswitch_5
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFUnlocked(Z)V

    goto :goto_0

    .line 82
    :pswitch_6
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDmBinFirst(I)V

    goto :goto_0

    .line 85
    :pswitch_7
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDmBinOther(I)V

    goto :goto_0

    .line 88
    :pswitch_8
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setBkc(B)V

    goto :goto_0

    .line 91
    :pswitch_9
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFTitlePage(Z)V

    goto :goto_0

    .line 94
    :pswitch_a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setCcolM1(I)V

    goto :goto_0

    .line 97
    :pswitch_b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDxaColumns(I)V

    goto :goto_0

    .line 100
    :pswitch_c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFAutoPgn(Z)V

    goto/16 :goto_0

    .line 103
    :pswitch_d
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setNfcPgn(B)V

    goto/16 :goto_0

    .line 106
    :pswitch_e
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDyaPgn(I)V

    goto/16 :goto_0

    .line 109
    :pswitch_f
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDxaPgn(I)V

    goto/16 :goto_0

    .line 112
    :pswitch_10
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFPgnRestart(Z)V

    goto/16 :goto_0

    .line 115
    :pswitch_11
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFEndNote(Z)V

    goto/16 :goto_0

    .line 118
    :pswitch_12
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setLnc(B)V

    goto/16 :goto_0

    .line 121
    :pswitch_13
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setGrpfIhdt(B)V

    goto/16 :goto_0

    .line 124
    :pswitch_14
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setNLnnMod(I)V

    goto/16 :goto_0

    .line 127
    :pswitch_15
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDxaLnn(I)V

    goto/16 :goto_0

    .line 130
    :pswitch_16
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDyaHdrTop(I)V

    goto/16 :goto_0

    .line 133
    :pswitch_17
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDyaHdrBottom(I)V

    goto/16 :goto_0

    .line 136
    :pswitch_18
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFLBetween(Z)V

    goto/16 :goto_0

    .line 139
    :pswitch_19
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setVjc(B)V

    goto/16 :goto_0

    .line 142
    :pswitch_1a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setLnnMin(I)V

    goto/16 :goto_0

    .line 145
    :pswitch_1b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setPgnStart(I)V

    goto/16 :goto_0

    .line 148
    :pswitch_1c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDmOrientPage(Z)V

    goto/16 :goto_0

    .line 155
    :pswitch_1d
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setXaPage(I)V

    goto/16 :goto_0

    .line 158
    :pswitch_1e
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setYaPage(I)V

    goto/16 :goto_0

    .line 161
    :pswitch_1f
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 164
    :pswitch_20
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDxaRight(I)V

    goto/16 :goto_0

    .line 167
    :pswitch_21
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDyaTop(I)V

    goto/16 :goto_0

    .line 170
    :pswitch_22
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDyaBottom(I)V

    goto/16 :goto_0

    .line 173
    :pswitch_23
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDzaGutter(I)V

    goto/16 :goto_0

    .line 176
    :pswitch_24
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDmPaperReq(I)V

    goto/16 :goto_0

    .line 179
    :pswitch_25
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setFPropMark(Z)V

    goto/16 :goto_0

    .line 188
    :pswitch_26
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 191
    :pswitch_27
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 194
    :pswitch_28
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 197
    :pswitch_29
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 200
    :pswitch_2a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setPgbProp(I)V

    goto/16 :goto_0

    .line 203
    :pswitch_2b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDxtCharSpace(I)V

    goto/16 :goto_0

    .line 206
    :pswitch_2c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setDyaLinePitch(I)V

    goto/16 :goto_0

    .line 209
    :pswitch_2d
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->setWTextFlow(I)V

    goto/16 :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_2d
    .end packed-switch
.end method

.method public static uncompressSEP([BI)Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    .locals 4
    .param p0, "grpprl"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 32
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;-><init>()V

    .line 34
    .local v0, "newProperties":Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-direct {v2, p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 36
    .local v2, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 42
    return-object v0

    .line 38
    :cond_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v1

    .line 39
    .local v1, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-static {v0, v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->unCompressSEPOperation(Lorg/apache/poi/hwpf/usermodel/SectionProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V

    goto :goto_0
.end method
