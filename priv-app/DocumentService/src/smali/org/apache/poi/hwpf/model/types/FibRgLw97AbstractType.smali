.class public abstract Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;
.super Ljava/lang/Object;
.source "FibRgLw97AbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected field_10_ccpTxbx:I

.field protected field_11_ccpHdrTxbx:I

.field protected field_12_reserved4:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_13_reserved5:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_14_reserved6:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_15_reserved7:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_16_reserved8:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_17_reserved9:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_18_reserved10:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_19_reserved11:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_1_cbMac:I

.field protected field_20_reserved12:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_21_reserved13:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_22_reserved14:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_2_reserved1:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_3_reserved2:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_4_ccpText:I

.field protected field_5_ccpFtn:I

.field protected field_6_ccpHdd:I

.field protected field_7_reserved3:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_8_ccpAtn:I

.field protected field_9_ccpEdn:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 147
    const/16 v0, 0x58

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 153
    if-ne p0, p1, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v1

    .line 155
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 156
    goto :goto_0

    .line 157
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 158
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 159
    check-cast v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;

    .line 160
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 161
    goto :goto_0

    .line 162
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 163
    goto :goto_0

    .line 164
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 165
    goto :goto_0

    .line 166
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 167
    goto :goto_0

    .line 168
    :cond_7
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 169
    goto :goto_0

    .line 170
    :cond_8
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 171
    goto :goto_0

    .line 172
    :cond_9
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 173
    goto :goto_0

    .line 174
    :cond_a
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 175
    goto :goto_0

    .line 176
    :cond_b
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 177
    goto :goto_0

    .line 178
    :cond_c
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 179
    goto :goto_0

    .line 180
    :cond_d
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 181
    goto :goto_0

    .line 182
    :cond_e
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 183
    goto :goto_0

    .line 184
    :cond_f
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 185
    goto :goto_0

    .line 186
    :cond_10
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    if-eq v3, v4, :cond_11

    move v1, v2

    .line 187
    goto/16 :goto_0

    .line 188
    :cond_11
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    if-eq v3, v4, :cond_12

    move v1, v2

    .line 189
    goto/16 :goto_0

    .line 190
    :cond_12
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    if-eq v3, v4, :cond_13

    move v1, v2

    .line 191
    goto/16 :goto_0

    .line 192
    :cond_13
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    if-eq v3, v4, :cond_14

    move v1, v2

    .line 193
    goto/16 :goto_0

    .line 194
    :cond_14
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    if-eq v3, v4, :cond_15

    move v1, v2

    .line 195
    goto/16 :goto_0

    .line 196
    :cond_15
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    if-eq v3, v4, :cond_16

    move v1, v2

    .line 197
    goto/16 :goto_0

    .line 198
    :cond_16
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 199
    goto/16 :goto_0

    .line 200
    :cond_17
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    if-eq v3, v4, :cond_18

    move v1, v2

    .line 201
    goto/16 :goto_0

    .line 202
    :cond_18
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 203
    goto/16 :goto_0
.end method

.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 85
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    .line 86
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    .line 87
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    .line 88
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    .line 89
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    .line 90
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    .line 91
    add-int/lit8 v0, p2, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    .line 92
    add-int/lit8 v0, p2, 0x1c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    .line 93
    add-int/lit8 v0, p2, 0x20

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    .line 94
    add-int/lit8 v0, p2, 0x24

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    .line 95
    add-int/lit8 v0, p2, 0x28

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    .line 96
    add-int/lit8 v0, p2, 0x2c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    .line 97
    add-int/lit8 v0, p2, 0x30

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    .line 98
    add-int/lit8 v0, p2, 0x34

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    .line 99
    add-int/lit8 v0, p2, 0x38

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    .line 100
    add-int/lit8 v0, p2, 0x3c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    .line 101
    add-int/lit8 v0, p2, 0x40

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    .line 102
    add-int/lit8 v0, p2, 0x44

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    .line 103
    add-int/lit8 v0, p2, 0x48

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    .line 104
    add-int/lit8 v0, p2, 0x4c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    .line 105
    add-int/lit8 v0, p2, 0x50

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    .line 106
    add-int/lit8 v0, p2, 0x54

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    .line 107
    return-void
.end method

.method public getCbMac()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 296
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    return v0
.end method

.method public getCcpAtn()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 422
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    return v0
.end method

.method public getCcpEdn()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 440
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    return v0
.end method

.method public getCcpFtn()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 368
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    return v0
.end method

.method public getCcpHdd()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 386
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    return v0
.end method

.method public getCcpHdrTxbx()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 476
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    return v0
.end method

.method public getCcpText()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 350
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    return v0
.end method

.method public getCcpTxbx()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 458
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    return v0
.end method

.method public getReserved1()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 314
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    return v0
.end method

.method public getReserved10()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 602
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    return v0
.end method

.method public getReserved11()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 620
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    return v0
.end method

.method public getReserved12()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 638
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    return v0
.end method

.method public getReserved13()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 656
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    return v0
.end method

.method public getReserved14()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 674
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    return v0
.end method

.method public getReserved2()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 332
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    return v0
.end method

.method public getReserved3()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 404
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    return v0
.end method

.method public getReserved4()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 494
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    return v0
.end method

.method public getReserved5()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 512
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    return v0
.end method

.method public getReserved6()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 530
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    return v0
.end method

.method public getReserved7()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 548
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    return v0
.end method

.method public getReserved8()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 566
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    return v0
.end method

.method public getReserved9()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 584
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 210
    const/16 v0, 0x1f

    .line 211
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 212
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    add-int/lit8 v1, v2, 0x1f

    .line 213
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    add-int v1, v2, v3

    .line 214
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    add-int v1, v2, v3

    .line 215
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    add-int v1, v2, v3

    .line 216
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    add-int v1, v2, v3

    .line 217
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    add-int v1, v2, v3

    .line 218
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    add-int v1, v2, v3

    .line 219
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    add-int v1, v2, v3

    .line 220
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    add-int v1, v2, v3

    .line 221
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    add-int v1, v2, v3

    .line 222
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    add-int v1, v2, v3

    .line 223
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    add-int v1, v2, v3

    .line 224
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    add-int v1, v2, v3

    .line 225
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    add-int v1, v2, v3

    .line 226
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    add-int v1, v2, v3

    .line 227
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    add-int v1, v2, v3

    .line 228
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    add-int v1, v2, v3

    .line 229
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    add-int v1, v2, v3

    .line 230
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    add-int v1, v2, v3

    .line 231
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    add-int v1, v2, v3

    .line 232
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    add-int v1, v2, v3

    .line 233
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    add-int v1, v2, v3

    .line 234
    return v1
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 111
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 112
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 113
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 114
    add-int/lit8 v0, p2, 0xc

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 115
    add-int/lit8 v0, p2, 0x10

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 116
    add-int/lit8 v0, p2, 0x14

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 117
    add-int/lit8 v0, p2, 0x18

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 118
    add-int/lit8 v0, p2, 0x1c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 119
    add-int/lit8 v0, p2, 0x20

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 120
    add-int/lit8 v0, p2, 0x24

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 121
    add-int/lit8 v0, p2, 0x28

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 122
    add-int/lit8 v0, p2, 0x2c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 123
    add-int/lit8 v0, p2, 0x30

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 124
    add-int/lit8 v0, p2, 0x34

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 125
    add-int/lit8 v0, p2, 0x38

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 126
    add-int/lit8 v0, p2, 0x3c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 127
    add-int/lit8 v0, p2, 0x40

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 128
    add-int/lit8 v0, p2, 0x44

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 129
    add-int/lit8 v0, p2, 0x48

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 130
    add-int/lit8 v0, p2, 0x4c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 131
    add-int/lit8 v0, p2, 0x50

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 132
    add-int/lit8 v0, p2, 0x54

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 133
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 137
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 138
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->serialize([BI)V

    .line 139
    return-object v0
.end method

.method public setCbMac(I)V
    .locals 0
    .param p1, "field_1_cbMac"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 305
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_1_cbMac:I

    .line 306
    return-void
.end method

.method public setCcpAtn(I)V
    .locals 0
    .param p1, "field_8_ccpAtn"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 431
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_8_ccpAtn:I

    .line 432
    return-void
.end method

.method public setCcpEdn(I)V
    .locals 0
    .param p1, "field_9_ccpEdn"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 449
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_9_ccpEdn:I

    .line 450
    return-void
.end method

.method public setCcpFtn(I)V
    .locals 0
    .param p1, "field_5_ccpFtn"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 377
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_5_ccpFtn:I

    .line 378
    return-void
.end method

.method public setCcpHdd(I)V
    .locals 0
    .param p1, "field_6_ccpHdd"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 395
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_6_ccpHdd:I

    .line 396
    return-void
.end method

.method public setCcpHdrTxbx(I)V
    .locals 0
    .param p1, "field_11_ccpHdrTxbx"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 485
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_11_ccpHdrTxbx:I

    .line 486
    return-void
.end method

.method public setCcpText(I)V
    .locals 0
    .param p1, "field_4_ccpText"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 359
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_4_ccpText:I

    .line 360
    return-void
.end method

.method public setCcpTxbx(I)V
    .locals 0
    .param p1, "field_10_ccpTxbx"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 467
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_10_ccpTxbx:I

    .line 468
    return-void
.end method

.method public setReserved1(I)V
    .locals 0
    .param p1, "field_2_reserved1"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 323
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_2_reserved1:I

    .line 324
    return-void
.end method

.method public setReserved10(I)V
    .locals 0
    .param p1, "field_18_reserved10"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 611
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_18_reserved10:I

    .line 612
    return-void
.end method

.method public setReserved11(I)V
    .locals 0
    .param p1, "field_19_reserved11"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 629
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_19_reserved11:I

    .line 630
    return-void
.end method

.method public setReserved12(I)V
    .locals 0
    .param p1, "field_20_reserved12"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 647
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_20_reserved12:I

    .line 648
    return-void
.end method

.method public setReserved13(I)V
    .locals 0
    .param p1, "field_21_reserved13"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 665
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_21_reserved13:I

    .line 666
    return-void
.end method

.method public setReserved14(I)V
    .locals 0
    .param p1, "field_22_reserved14"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 683
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_22_reserved14:I

    .line 684
    return-void
.end method

.method public setReserved2(I)V
    .locals 0
    .param p1, "field_3_reserved2"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 341
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_3_reserved2:I

    .line 342
    return-void
.end method

.method public setReserved3(I)V
    .locals 0
    .param p1, "field_7_reserved3"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 413
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_7_reserved3:I

    .line 414
    return-void
.end method

.method public setReserved4(I)V
    .locals 0
    .param p1, "field_12_reserved4"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 503
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_12_reserved4:I

    .line 504
    return-void
.end method

.method public setReserved5(I)V
    .locals 0
    .param p1, "field_13_reserved5"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 521
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_13_reserved5:I

    .line 522
    return-void
.end method

.method public setReserved6(I)V
    .locals 0
    .param p1, "field_14_reserved6"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 539
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_14_reserved6:I

    .line 540
    return-void
.end method

.method public setReserved7(I)V
    .locals 0
    .param p1, "field_15_reserved7"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 557
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_15_reserved7:I

    .line 558
    return-void
.end method

.method public setReserved8(I)V
    .locals 0
    .param p1, "field_16_reserved8"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 575
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_16_reserved8:I

    .line 576
    return-void
.end method

.method public setReserved9(I)V
    .locals 0
    .param p1, "field_17_reserved9"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 593
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->field_17_reserved9:I

    .line 594
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[FibRgLw97]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const-string/jumbo v1, "    .cbMac                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCbMac()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const-string/jumbo v1, "    .reserved1            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved1()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string/jumbo v1, "    .reserved2            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string/jumbo v1, "    .ccpText              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpText()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string/jumbo v1, "    .ccpFtn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpFtn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string/jumbo v1, "    .ccpHdd               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpHdd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    const-string/jumbo v1, "    .reserved3            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved3()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string/jumbo v1, "    .ccpAtn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpAtn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string/jumbo v1, "    .ccpEdn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string/jumbo v1, "    .ccpTxbx              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpTxbx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string/jumbo v1, "    .ccpHdrTxbx           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getCcpHdrTxbx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    const-string/jumbo v1, "    .reserved4            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved4()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string/jumbo v1, "    .reserved5            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved5()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string/jumbo v1, "    .reserved6            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved6()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string/jumbo v1, "    .reserved7            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved7()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string/jumbo v1, "    .reserved8            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved8()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    const-string/jumbo v1, "    .reserved9            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved9()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string/jumbo v1, "    .reserved10           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved10()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const-string/jumbo v1, "    .reserved11           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved11()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string/jumbo v1, "    .reserved12           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved12()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    const-string/jumbo v1, "    .reserved13           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved13()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    const-string/jumbo v1, "    .reserved14           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw97AbstractType;->getReserved14()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const-string/jumbo v1, "[/FibRgLw97]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
