.class public final Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;
.super Ljava/lang/Object;
.source "SectionSprmCompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;-><init>()V

    sput-object v0, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public static compressSectionProperty(Lorg/apache/poi/hwpf/usermodel/SectionProperties;)[B
    .locals 9
    .param p0, "newSEP"    # Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 37
    const/4 v2, 0x0

    .line 38
    .local v2, "size":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v3, "sprmList":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCnsPgn()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCnsPgn()B

    move-result v7

    if-eq v4, v7, :cond_0

    .line 42
    const/16 v4, 0x3000

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCnsPgn()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 44
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getIHeadingPgn()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getIHeadingPgn()B

    move-result v7

    if-eq v4, v7, :cond_1

    .line 46
    const/16 v4, 0x3001

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getIHeadingPgn()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 48
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getOlstAnm()[B

    move-result-object v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getOlstAnm()[B

    move-result-object v7

    invoke-static {v4, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_2

    .line 50
    const/16 v4, -0x2dfe

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getOlstAnm()[B

    move-result-object v7

    invoke-static {v4, v6, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 52
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v7

    if-eq v4, v7, :cond_3

    .line 54
    const/16 v7, 0x3005

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v4

    if-eqz v4, :cond_2f

    move v4, v5

    :goto_0
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 56
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFUnlocked()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFUnlocked()Z

    move-result v7

    if-eq v4, v7, :cond_4

    .line 58
    const/16 v7, 0x3006

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFUnlocked()Z

    move-result v4

    if-eqz v4, :cond_30

    move v4, v5

    :goto_1
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 60
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmBinFirst()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmBinFirst()I

    move-result v7

    if-eq v4, v7, :cond_5

    .line 62
    const/16 v4, 0x5007

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmBinFirst()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 64
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmBinOther()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmBinOther()I

    move-result v7

    if-eq v4, v7, :cond_6

    .line 66
    const/16 v4, 0x5008

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmBinOther()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 68
    :cond_6
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBkc()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBkc()B

    move-result v7

    if-eq v4, v7, :cond_7

    .line 70
    const/16 v4, 0x3009

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBkc()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 72
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFTitlePage()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFTitlePage()Z

    move-result v7

    if-eq v4, v7, :cond_8

    .line 74
    const/16 v7, 0x300a

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFTitlePage()Z

    move-result v4

    if-eqz v4, :cond_31

    move v4, v5

    :goto_2
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 76
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v7

    if-eq v4, v7, :cond_9

    .line 78
    const/16 v4, 0x500b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 80
    :cond_9
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v7

    if-eq v4, v7, :cond_a

    .line 82
    const/16 v4, -0x6ff4

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 84
    :cond_a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFAutoPgn()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFAutoPgn()Z

    move-result v7

    if-eq v4, v7, :cond_b

    .line 86
    const/16 v7, 0x300d

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFAutoPgn()Z

    move-result v4

    if-eqz v4, :cond_32

    move v4, v5

    :goto_3
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 88
    :cond_b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getNfcPgn()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getNfcPgn()B

    move-result v7

    if-eq v4, v7, :cond_c

    .line 90
    const/16 v4, 0x300e

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getNfcPgn()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 92
    :cond_c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaPgn()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaPgn()I

    move-result v7

    if-eq v4, v7, :cond_d

    .line 94
    const/16 v4, -0x4ff1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaPgn()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 96
    :cond_d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaPgn()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaPgn()I

    move-result v7

    if-eq v4, v7, :cond_e

    .line 98
    const/16 v4, -0x4ff0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaPgn()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 100
    :cond_e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFPgnRestart()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFPgnRestart()Z

    move-result v7

    if-eq v4, v7, :cond_f

    .line 102
    const/16 v7, 0x3011

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFPgnRestart()Z

    move-result v4

    if-eqz v4, :cond_33

    move v4, v5

    :goto_4
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 104
    :cond_f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEndNote()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEndNote()Z

    move-result v7

    if-eq v4, v7, :cond_10

    .line 106
    const/16 v7, 0x3012

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEndNote()Z

    move-result v4

    if-eqz v4, :cond_34

    move v4, v5

    :goto_5
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 108
    :cond_10
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getLnc()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getLnc()B

    move-result v7

    if-eq v4, v7, :cond_11

    .line 110
    const/16 v4, 0x3013

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getLnc()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 112
    :cond_11
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getGrpfIhdt()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getGrpfIhdt()B

    move-result v7

    if-eq v4, v7, :cond_12

    .line 114
    const/16 v4, 0x3014

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getGrpfIhdt()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 116
    :cond_12
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getNLnnMod()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getNLnnMod()I

    move-result v7

    if-eq v4, v7, :cond_13

    .line 118
    const/16 v4, 0x5015

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getNLnnMod()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 120
    :cond_13
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLnn()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLnn()I

    move-result v7

    if-eq v4, v7, :cond_14

    .line 122
    const/16 v4, -0x6fea

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLnn()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 124
    :cond_14
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v7

    if-eq v4, v7, :cond_15

    .line 126
    const/16 v4, -0x4fe9

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 128
    :cond_15
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v7

    if-eq v4, v7, :cond_16

    .line 130
    const/16 v4, -0x4fe8

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 132
    :cond_16
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFLBetween()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFLBetween()Z

    move-result v7

    if-eq v4, v7, :cond_17

    .line 134
    const/16 v7, 0x3019

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFLBetween()Z

    move-result v4

    if-eqz v4, :cond_35

    move v4, v5

    :goto_6
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 136
    :cond_17
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getVjc()B

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getVjc()B

    move-result v7

    if-eq v4, v7, :cond_18

    .line 138
    const/16 v4, 0x301a

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getVjc()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 140
    :cond_18
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getLnnMin()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getLnnMin()I

    move-result v7

    if-eq v4, v7, :cond_19

    .line 142
    const/16 v4, 0x501b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getLnnMin()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 144
    :cond_19
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getPgnStart()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getPgnStart()I

    move-result v7

    if-eq v4, v7, :cond_1a

    .line 146
    const/16 v4, 0x501c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getPgnStart()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 148
    :cond_1a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmOrientPage()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmOrientPage()Z

    move-result v7

    if-eq v4, v7, :cond_1b

    .line 150
    const/16 v7, 0x301d

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmOrientPage()Z

    move-result v4

    if-eqz v4, :cond_36

    move v4, v5

    :goto_7
    invoke-static {v7, v4, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 152
    :cond_1b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v7

    if-eq v4, v7, :cond_1c

    .line 154
    const/16 v4, -0x4fe1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 156
    :cond_1c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v7

    if-eq v4, v7, :cond_1d

    .line 158
    const/16 v4, -0x4fe0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 160
    :cond_1d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v7

    if-eq v4, v7, :cond_1e

    .line 162
    const/16 v4, -0x4fdf

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 164
    :cond_1e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v7

    if-eq v4, v7, :cond_1f

    .line 166
    const/16 v4, -0x4fde

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 168
    :cond_1f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v7

    if-eq v4, v7, :cond_20

    .line 170
    const/16 v4, -0x6fdd

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 172
    :cond_20
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v7

    if-eq v4, v7, :cond_21

    .line 174
    const/16 v4, -0x6fdc

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 176
    :cond_21
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDzaGutter()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDzaGutter()I

    move-result v7

    if-eq v4, v7, :cond_22

    .line 178
    const/16 v4, -0x4fdb

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDzaGutter()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 180
    :cond_22
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmPaperReq()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmPaperReq()I

    move-result v7

    if-eq v4, v7, :cond_23

    .line 182
    const/16 v4, 0x5026

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDmPaperReq()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 184
    :cond_23
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFPropMark()Z

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFPropMark()Z

    move-result v7

    if-ne v4, v7, :cond_24

    .line 185
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getIbstPropRMark()I

    move-result v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getIbstPropRMark()I

    move-result v7

    if-ne v4, v7, :cond_24

    .line 186
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v4

    sget-object v7, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_25

    .line 188
    :cond_24
    const/4 v4, 0x7

    new-array v0, v4, [B

    .line 189
    .local v0, "buf":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFPropMark()Z

    move-result v4

    if-eqz v4, :cond_37

    :goto_8
    int-to-byte v4, v5

    aput-byte v4, v0, v6

    .line 190
    const/4 v1, 0x1

    .line 191
    .local v1, "offset":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getIbstPropRMark()I

    move-result v4

    int-to-short v4, v4

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 192
    add-int/lit8 v1, v1, 0x2

    .line 193
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->serialize([BI)V

    .line 194
    const/16 v4, -0x2dd9

    const/4 v5, -0x1

    invoke-static {v4, v5, v0, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 196
    .end local v0    # "buf":[B
    .end local v1    # "offset":I
    :cond_25
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 198
    const/16 v4, 0x702b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 200
    :cond_26
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_27

    .line 202
    const/16 v4, 0x702c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 204
    :cond_27
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_28

    .line 206
    const/16 v4, 0x702d

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 208
    :cond_28
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_29

    .line 210
    const/16 v4, 0x702e

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 212
    :cond_29
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getPgbProp()I

    move-result v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getPgbProp()I

    move-result v5

    if-eq v4, v5, :cond_2a

    .line 214
    const/16 v4, 0x522f

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getPgbProp()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 216
    :cond_2a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxtCharSpace()I

    move-result v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxtCharSpace()I

    move-result v5

    if-eq v4, v5, :cond_2b

    .line 218
    const/16 v4, 0x7030

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxtCharSpace()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 220
    :cond_2b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaLinePitch()I

    move-result v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaLinePitch()I

    move-result v5

    if-eq v4, v5, :cond_2c

    .line 222
    const/16 v4, -0x6fcf

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaLinePitch()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 224
    :cond_2c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getClm()I

    move-result v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getClm()I

    move-result v5

    if-eq v4, v5, :cond_2d

    .line 226
    const/16 v4, 0x5032

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getClm()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 228
    :cond_2d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getWTextFlow()I

    move-result v4

    sget-object v5, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getWTextFlow()I

    move-result v5

    if-eq v4, v5, :cond_2e

    .line 230
    const/16 v4, 0x5033

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getWTextFlow()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 233
    :cond_2e
    invoke-static {v3, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->getGrpprl(Ljava/util/List;I)[B

    move-result-object v4

    return-object v4

    :cond_2f
    move v4, v6

    .line 54
    goto/16 :goto_0

    :cond_30
    move v4, v6

    .line 58
    goto/16 :goto_1

    :cond_31
    move v4, v6

    .line 74
    goto/16 :goto_2

    :cond_32
    move v4, v6

    .line 86
    goto/16 :goto_3

    :cond_33
    move v4, v6

    .line 102
    goto/16 :goto_4

    :cond_34
    move v4, v6

    .line 106
    goto/16 :goto_5

    :cond_35
    move v4, v6

    .line 134
    goto/16 :goto_6

    :cond_36
    move v4, v6

    .line 150
    goto/16 :goto_7

    .restart local v0    # "buf":[B
    :cond_37
    move v5, v6

    .line 189
    goto/16 :goto_8
.end method
