.class public abstract Lorg/apache/poi/hwpf/model/PropertyNode;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;,
        Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/poi/hwpf/model/PropertyNode",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<TT;>;",
        "Ljava/lang/Cloneable;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final _logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected _buf:Ljava/lang/Object;

.field private _cpEnd:I

.field private _cpStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/PropertyNode;->_logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method protected constructor <init>(IILjava/lang/Object;)V
    .locals 4
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "buf"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    const/4 v3, 0x5

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput p1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 84
    iput p2, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 85
    iput-object p3, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 87
    iget v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-gez v0, :cond_0

    .line 88
    sget-object v0, Lorg/apache/poi/hwpf/model/PropertyNode;->_logger:Lorg/apache/poi/util/POILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "A property claimed to start before zero, at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "! Resetting it to zero, and hoping for the best"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 92
    :cond_0
    iget v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-ge v0, v1, :cond_1

    .line 94
    sget-object v0, Lorg/apache/poi/hwpf/model/PropertyNode;->_logger:Lorg/apache/poi/util/POILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "A property claimed to end ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 95
    const-string/jumbo v2, ") before start! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 96
    const-string/jumbo v2, "Resetting end to start, and hoping for the best"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v3, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 97
    iget v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    iput v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 99
    :cond_1
    return-void
.end method


# virtual methods
.method public adjustForDelete(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 134
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    add-int v0, p1, p2

    .line 136
    .local v0, "end":I
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-le v1, p1, :cond_0

    .line 139
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-ge v1, v0, :cond_2

    .line 141
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-lt v0, v1, :cond_1

    move v1, p1

    :goto_0
    iput v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 142
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 149
    :cond_0
    :goto_1
    return-void

    .line 141
    :cond_1
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    sub-int/2addr v1, p2

    goto :goto_0

    .line 145
    :cond_2
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    sub-int/2addr v1, p2

    iput v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 146
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    sub-int/2addr v1, p2

    iput v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    goto :goto_1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyNode;->clone()Lorg/apache/poi/hwpf/model/PropertyNode;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/model/PropertyNode;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PropertyNode;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/PropertyNode;->compareTo(Lorg/apache/poi/hwpf/model/PropertyNode;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/hwpf/model/PropertyNode;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 193
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    .local p1, "o":Lorg/apache/poi/hwpf/model/PropertyNode;, "TT;"
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    .line 194
    .local v0, "cpEnd":I
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-ne v1, v0, :cond_0

    .line 196
    const/4 v1, 0x0

    .line 204
    :goto_0
    return v1

    .line 198
    :cond_0
    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-ge v1, v0, :cond_1

    .line 200
    const/4 v1, -0x1

    goto :goto_0

    .line 204
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    const/4 v1, 0x0

    .line 167
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/model/PropertyNode;

    if-eqz v2, :cond_0

    .line 168
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/PropertyNode;->limitsAreEqual(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    check-cast p1, Lorg/apache/poi/hwpf/model/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v0, p1, Lorg/apache/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 171
    .local v0, "testBuf":Ljava/lang/Object;
    instance-of v1, v0, [B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_1

    .line 173
    check-cast v0, [B

    .end local v0    # "testBuf":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    .line 179
    :cond_0
    :goto_0
    return v1

    .line 175
    .restart local v0    # "testBuf":Ljava/lang/Object;
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 119
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    iget v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 106
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    iget v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 161
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    iget v0, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected limitsAreEqual(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 153
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    move-object v0, p1

    check-cast v0, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v0

    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-ne v0, v1, :cond_0

    .line 154
    check-cast p1, Lorg/apache/poi/hwpf/model/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    iget v1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-ne v0, v1, :cond_0

    .line 153
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnd(I)V
    .locals 0
    .param p1, "end"    # I

    .prologue
    .line 124
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    iput p1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 125
    return-void
.end method

.method public setStart(I)V
    .locals 0
    .param p1, "start"    # I

    .prologue
    .line 111
    .local p0, "this":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<TT;>;"
    iput p1, p0, Lorg/apache/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 112
    return-void
.end method
