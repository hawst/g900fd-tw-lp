.class public Lorg/apache/poi/hwpf/converter/DefaultFontReplacer;
.super Ljava/lang/Object;
.source "DefaultFontReplacer.java"

# interfaces
.implements Lorg/apache/poi/hwpf/converter/FontReplacer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    .locals 5
    .param p1, "original"    # Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    iget-object v1, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 28
    iget-object v0, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 30
    .local v0, "fontName":Ljava/lang/String;
    const-string/jumbo v1, " Regular"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const-string/jumbo v1, " Regular"

    .line 31
    invoke-static {v0, v1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->substringBeforeLast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    :cond_0
    const-string/jumbo v1, " \u041f\u043e\u043b\u0443\u0436\u0438\u0440\u043d\u044b\u0439"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    .line 38
    const-string/jumbo v2, " \u041f\u043e\u043b\u0443\u0436\u0438\u0440\u043d\u044b\u0439"

    .line 37
    invoke-static {v0, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->substringBeforeLast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 39
    const-string/jumbo v2, " Bold"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_1
    const-string/jumbo v1, " \u041f\u043e\u043b\u0443\u0436\u0438\u0440\u043d\u044b\u0439 \u041a\u0443\u0440\u0441\u0438\u0432"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    .line 46
    const-string/jumbo v2, " \u041f\u043e\u043b\u0443\u0436\u0438\u0440\u043d\u044b\u0439 \u041a\u0443\u0440\u0441\u0438\u0432"

    .line 44
    invoke-static {v0, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->substringBeforeLast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 47
    const-string/jumbo v2, " Bold Italic"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    :cond_2
    const-string/jumbo v1, " \u041a\u0443\u0440\u0441\u0438\u0432"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    .line 51
    const-string/jumbo v2, " \u041a\u0443\u0440\u0441\u0438\u0432"

    invoke-static {v0, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->substringBeforeLast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " Italic"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    :cond_3
    iput-object v0, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 56
    .end local v0    # "fontName":Ljava/lang/String;
    :cond_4
    iget-object v1, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 58
    const-string/jumbo v1, "Times Regular"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 59
    const-string/jumbo v1, "Times-Regular"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 60
    const-string/jumbo v1, "Times Roman"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 62
    :cond_5
    const-string/jumbo v1, "Times"

    iput-object v1, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 63
    iput-boolean v3, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    .line 64
    iput-boolean v3, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 66
    :cond_6
    const-string/jumbo v1, "Times Bold"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 67
    const-string/jumbo v1, "Times-Bold"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 69
    :cond_7
    const-string/jumbo v1, "Times"

    iput-object v1, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 70
    iput-boolean v4, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    .line 71
    iput-boolean v3, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 73
    :cond_8
    const-string/jumbo v1, "Times Italic"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 74
    const-string/jumbo v1, "Times-Italic"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 76
    :cond_9
    const-string/jumbo v1, "Times"

    iput-object v1, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 77
    iput-boolean v3, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    .line 78
    iput-boolean v4, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 80
    :cond_a
    const-string/jumbo v1, "Times Bold Italic"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 81
    const-string/jumbo v1, "Times-BoldItalic"

    iget-object v2, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 83
    :cond_b
    const-string/jumbo v1, "Times"

    iput-object v1, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 84
    iput-boolean v4, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    .line 85
    iput-boolean v4, p1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 89
    :cond_c
    return-object p1
.end method
