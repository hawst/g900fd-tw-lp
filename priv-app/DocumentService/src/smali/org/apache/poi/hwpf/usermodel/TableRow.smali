.class public final Lorg/apache/poi/hwpf/usermodel/TableRow;
.super Lorg/apache/poi/hwpf/usermodel/Range;
.source "TableRow.java"


# static fields
.field private static final SPRM_DXAGAPHALF:S = -0x69fes

.field private static final SPRM_DYAROWHEIGHT:S = -0x6bf9s

.field private static final SPRM_FCANTSPLIT:S = 0x3403s

.field private static final SPRM_FTABLEHEADER:S = 0x3404s

.field private static final SPRM_TJC:S = 0x5400s

.field private static final TABLE_CELL_MARK:C = '\u0007'

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _cells:[Lorg/apache/poi/hwpf/usermodel/TableCell;

.field private _cellsFound:Z

.field _levelNum:I

.field private _papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

.field private _tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/poi/hwpf/usermodel/TableRow;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 30
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/TableRow;->logger:Lorg/apache/poi/util/POILogger;

    .line 39
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hwpf/usermodel/Table;I)V
    .locals 2
    .param p1, "startIdxInclusive"    # I
    .param p2, "endIdxExclusive"    # I
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/Table;
    .param p4, "levelNum"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 42
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cellsFound:Z

    .line 53
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numParagraphs()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 54
    .local v0, "last":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    iget-object v1, v0, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 55
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/TableSprmUncompressor;->uncompressTAP(Lorg/apache/poi/hwpf/sprm/SprmBuffer;)Lorg/apache/poi/hwpf/usermodel/TableProperties;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 56
    iput p4, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_levelNum:I

    .line 57
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->initCells()V

    .line 58
    return-void
.end method

.method private initCells()V
    .locals 15

    .prologue
    .line 123
    iget-boolean v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cellsFound:Z

    if-eqz v2, :cond_0

    .line 201
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getItcMac()S

    move-result v9

    .line 128
    .local v9, "expectedCellsCount":S
    const/4 v1, 0x0

    .line 129
    .local v1, "lastCellStart":I
    new-instance v8, Ljava/util/ArrayList;

    .line 130
    add-int/lit8 v2, v9, 0x1

    .line 129
    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 131
    .local v8, "cells":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/TableCell;>;"
    const/4 v11, 0x0

    .local v11, "p":I
    :goto_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numParagraphs()I

    move-result v2

    if-lt v11, v2, :cond_4

    .line 159
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numParagraphs()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 161
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 162
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    array-length v2, v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_b

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 163
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    aget-object v5, v2, v3

    .line 164
    .local v5, "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :goto_2
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    if-eqz v2, :cond_c

    .line 165
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    array-length v2, v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_c

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 166
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    aget-short v6, v2, v3

    .line 167
    .local v6, "leftEdge":S
    :goto_3
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    if-eqz v2, :cond_d

    .line 168
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    array-length v2, v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-le v2, v3, :cond_d

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 169
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    aget-short v13, v2, v3

    .line 171
    .local v13, "rightEdge":S
    :goto_4
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/TableCell;

    .line 172
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numParagraphs()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_levelNum:I

    .line 173
    sub-int v7, v13, v6

    move-object v3, p0

    .line 171
    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hwpf/usermodel/TableCell;-><init>(IILorg/apache/poi/hwpf/usermodel/TableRow;ILorg/apache/poi/hwpf/usermodel/TableCellDescriptor;II)V

    .line 174
    .local v0, "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    .end local v0    # "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    .end local v5    # "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .end local v6    # "leftEdge":S
    .end local v13    # "rightEdge":S
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 179
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/poi/hwpf/usermodel/TableCell;

    .line 180
    .local v10, "lastCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-virtual {v10}, Lorg/apache/poi/hwpf/usermodel/TableCell;->numParagraphs()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 181
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isTableRowEnd()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 184
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v8, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 188
    .end local v10    # "lastCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v9, :cond_3

    .line 190
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/TableRow;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    .line 191
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Number of found table cells ("

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 192
    const-string/jumbo v7, ") for table row ["

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getStartOffset()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, "c; "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 193
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getEndOffset()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 194
    const-string/jumbo v7, "c] not equals to stored property value "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 195
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 191
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 190
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 196
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setItcMac(S)V

    .line 199
    :cond_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hwpf/usermodel/TableCell;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hwpf/usermodel/TableCell;

    iput-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cells:[Lorg/apache/poi/hwpf/usermodel/TableCell;

    .line 200
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cellsFound:Z

    goto/16 :goto_0

    .line 133
    :cond_4
    invoke-virtual {p0, v11}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v12

    .line 134
    .local v12, "paragraph":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->text()Ljava/lang/String;

    move-result-object v14

    .line 136
    .local v14, "s":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v14, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_6

    .line 137
    :cond_5
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isEmbeddedCellMark()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 138
    :cond_6
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v2

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_levelNum:I

    if-ne v2, v3, :cond_7

    .line 140
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 141
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    array-length v2, v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_8

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 142
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    aget-object v5, v2, v3

    .line 143
    .restart local v5    # "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :goto_5
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    if-eqz v2, :cond_9

    .line 144
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    array-length v2, v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_9

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 145
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    aget-short v6, v2, v3

    .line 146
    .restart local v6    # "leftEdge":S
    :goto_6
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    if-eqz v2, :cond_a

    .line 147
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    array-length v2, v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-le v2, v3, :cond_a

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 148
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    aget-short v13, v2, v3

    .line 150
    .restart local v13    # "rightEdge":S
    :goto_7
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/TableCell;

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v2

    .line 151
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getStartOffset()I

    move-result v1

    .end local v1    # "lastCellStart":I
    invoke-virtual {p0, v11}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v2

    .line 152
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getEndOffset()I

    move-result v2

    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_levelNum:I

    .line 153
    sub-int v7, v13, v6

    move-object v3, p0

    .line 150
    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hwpf/usermodel/TableCell;-><init>(IILorg/apache/poi/hwpf/usermodel/TableRow;ILorg/apache/poi/hwpf/usermodel/TableCellDescriptor;II)V

    .line 154
    .restart local v0    # "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    add-int/lit8 v1, v11, 0x1

    .line 131
    .end local v0    # "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    .end local v5    # "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .end local v6    # "leftEdge":S
    .end local v13    # "rightEdge":S
    .restart local v1    # "lastCellStart":I
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 142
    :cond_8
    new-instance v5, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-direct {v5}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;-><init>()V

    goto :goto_5

    .line 145
    .restart local v5    # "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :cond_9
    const/4 v6, 0x0

    goto :goto_6

    .line 148
    .restart local v6    # "leftEdge":S
    :cond_a
    const/4 v13, 0x0

    goto :goto_7

    .line 163
    .end local v5    # "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .end local v6    # "leftEdge":S
    .end local v12    # "paragraph":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .end local v14    # "s":Ljava/lang/String;
    :cond_b
    new-instance v5, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-direct {v5}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;-><init>()V

    goto/16 :goto_2

    .line 166
    .restart local v5    # "tableCellDescriptor":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :cond_c
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 169
    .restart local v6    # "leftEdge":S
    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_4
.end method


# virtual methods
.method public cantSplit()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getFCantSplit()Z

    move-result v0

    return v0
.end method

.method public getBarBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "not applicable for TableRow"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBottomBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->initCells()V

    .line 78
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cells:[Lorg/apache/poi/hwpf/usermodel/TableCell;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getGapHalf()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getDxaGapHalf()I

    move-result v0

    return v0
.end method

.method public getHorizontalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcHorizontal()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getLeftBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getRightBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getRowHeight()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getDyaRowHeight()I

    move-result v0

    return v0
.end method

.method public getRowJustification()I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getJc()S

    move-result v0

    return v0
.end method

.method public getTopBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getVerticalBorder()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcVertical()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public isTableHeader()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getFTableHeader()Z

    move-result v0

    return v0
.end method

.method public numCells()I
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->initCells()V

    .line 211
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cells:[Lorg/apache/poi/hwpf/usermodel/TableCell;

    array-length v0, v0

    return v0
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_cellsFound:Z

    .line 218
    return-void
.end method

.method public setCantSplit(Z)V
    .locals 3
    .param p1, "cantSplit"    # Z

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setFCantSplit(Z)V

    .line 223
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x3403

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 224
    return-void

    .line 223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGapHalf(I)V
    .locals 3
    .param p1, "dxaGapHalf"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setDxaGapHalf(I)V

    .line 229
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x69fe

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 230
    return-void
.end method

.method public setRowHeight(I)V
    .locals 3
    .param p1, "dyaRowHeight"    # I

    .prologue
    .line 234
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setDyaRowHeight(I)V

    .line 235
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, -0x6bf9

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 236
    return-void
.end method

.method public setRowJustification(I)V
    .locals 3
    .param p1, "jc"    # I

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setJc(S)V

    .line 241
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x5400

    int-to-short v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 242
    return-void
.end method

.method public setTableHeader(Z)V
    .locals 3
    .param p1, "tableHeader"    # Z

    .prologue
    .line 246
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_tprops:Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setFTableHeader(Z)V

    .line 247
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/TableRow;->_papx:Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/16 v2, 0x3404

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 248
    return-void

    .line 247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
