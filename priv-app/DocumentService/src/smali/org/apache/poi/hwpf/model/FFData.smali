.class public Lorg/apache/poi/hwpf/model/FFData;
.super Ljava/lang/Object;
.source "FFData.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _base:Lorg/apache/poi/hwpf/model/FFDataBase;

.field private _hsttbDropList:Lorg/apache/poi/hwpf/model/Sttb;

.field private _wDef:Ljava/lang/Integer;

.field private _xstzEntryMcr:Lorg/apache/poi/hwpf/model/Xstz;

.field private _xstzExitMcr:Lorg/apache/poi/hwpf/model/Xstz;

.field private _xstzHelpText:Lorg/apache/poi/hwpf/model/Xstz;

.field private _xstzName:Lorg/apache/poi/hwpf/model/Xstz;

.field private _xstzStatText:Lorg/apache/poi/hwpf/model/Xstz;

.field private _xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

.field private _xstzTextFormat:Lorg/apache/poi/hwpf/model/Xstz;


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FFData;->fillFields([BI)V

    .line 90
    return-void
.end method


# virtual methods
.method public fillFields([BI)V
    .locals 5
    .param p1, "std"    # [B
    .param p2, "startOffset"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 94
    move v0, p2

    .line 96
    .local v0, "offset":I
    new-instance v1, Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/FFDataBase;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    .line 97
    invoke-static {}, Lorg/apache/poi/hwpf/model/FFDataBase;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzName:Lorg/apache/poi/hwpf/model/Xstz;

    .line 100
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzName:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    if-nez v1, :cond_2

    .line 104
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

    .line 105
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 113
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    if-ne v1, v3, :cond_3

    .line 116
    :cond_0
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 115
    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_wDef:Ljava/lang/Integer;

    .line 117
    add-int/lit8 v0, v0, 0x2

    .line 124
    :goto_1
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextFormat:Lorg/apache/poi/hwpf/model/Xstz;

    .line 125
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextFormat:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzHelpText:Lorg/apache/poi/hwpf/model/Xstz;

    .line 128
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzHelpText:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzStatText:Lorg/apache/poi/hwpf/model/Xstz;

    .line 131
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzStatText:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzEntryMcr:Lorg/apache/poi/hwpf/model/Xstz;

    .line 134
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzEntryMcr:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    new-instance v1, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xstz;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzExitMcr:Lorg/apache/poi/hwpf/model/Xstz;

    .line 137
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzExitMcr:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    if-ne v1, v3, :cond_1

    .line 141
    new-instance v1, Lorg/apache/poi/hwpf/model/Sttb;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Sttb;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_hsttbDropList:Lorg/apache/poi/hwpf/model/Sttb;

    .line 142
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_hsttbDropList:Lorg/apache/poi/hwpf/model/Sttb;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Sttb;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_1
    return-void

    .line 109
    :cond_2
    iput-object v4, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

    goto :goto_0

    .line 121
    :cond_3
    iput-object v4, p0, Lorg/apache/poi/hwpf/model/FFData;->_wDef:Ljava/lang/Integer;

    goto :goto_1
.end method

.method public getDefaultDropDownItemIndex()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FFData;->_wDef:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getDropList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FFData;->_hsttbDropList:Lorg/apache/poi/hwpf/model/Sttb;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->getData()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 161
    invoke-static {}, Lorg/apache/poi/hwpf/model/FFDataBase;->getSize()I

    move-result v0

    .line 163
    .local v0, "size":I
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzName:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    if-nez v1, :cond_0

    .line 167
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 171
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    if-ne v1, v3, :cond_2

    .line 173
    :cond_1
    add-int/lit8 v0, v0, 0x2

    .line 176
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextFormat:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzHelpText:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzStatText:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzEntryMcr:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzExitMcr:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xstz;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v1

    if-ne v1, v3, :cond_3

    .line 184
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FFData;->_hsttbDropList:Lorg/apache/poi/hwpf/model/Sttb;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Sttb;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_3
    return v0
.end method

.method public getTextDef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Xstz;->getAsJavaString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public serialize()[B
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 197
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FFData;->getSize()I

    move-result v2

    new-array v0, v2, [B

    .line 198
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 200
    .local v1, "offset":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/FFDataBase;->serialize([BI)V

    .line 201
    invoke-static {}, Lorg/apache/poi/hwpf/model/FFDataBase;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 203
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzName:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 205
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v2

    if-nez v2, :cond_0

    .line 207
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextDef:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 210
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 211
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v2

    if-ne v2, v4, :cond_2

    .line 213
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_wDef:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 214
    add-int/lit8 v1, v1, 0x2

    .line 217
    :cond_2
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzTextFormat:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 218
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzHelpText:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 219
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzStatText:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 220
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzEntryMcr:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 221
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_xstzExitMcr:Lorg/apache/poi/hwpf/model/Xstz;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xstz;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 223
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_base:Lorg/apache/poi/hwpf/model/FFDataBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FFDataBase;->getIType()B

    move-result v2

    if-ne v2, v4, :cond_3

    .line 225
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FFData;->_hsttbDropList:Lorg/apache/poi/hwpf/model/Sttb;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Sttb;->serialize([BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 228
    :cond_3
    return-object v0
.end method
