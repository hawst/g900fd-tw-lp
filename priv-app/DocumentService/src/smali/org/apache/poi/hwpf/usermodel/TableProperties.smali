.class public final Lorg/apache/poi/hwpf/usermodel/TableProperties;
.super Lorg/apache/poi/hwpf/model/types/TAPAbstractType;
.source "TableProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;-><init>()V

    .line 27
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setTlp(Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;)V

    .line 28
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setShdTable(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 29
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 30
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcHorizontal(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 31
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 32
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 33
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 34
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcVertical(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 35
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgbrcInsideDefault_0(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 36
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgbrcInsideDefault_1(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 37
    new-array v0, v1, [S

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenter([S)V

    .line 38
    new-array v0, v1, [S

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenterPrint([S)V

    .line 39
    new-array v0, v1, [Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgshd([Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 40
    new-array v0, v1, [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgtc([Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;)V

    .line 41
    return-void
.end method

.method public constructor <init>(S)V
    .locals 4
    .param p1, "columns"    # S

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;-><init>()V

    .line 47
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setItcMac(S)V

    .line 48
    new-array v2, p1, [Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgshd([Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 50
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 55
    new-array v0, p1, [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    .line 56
    .local v0, "tableCellDescriptors":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    const/4 v1, 0x0

    :goto_1
    if-lt v1, p1, :cond_1

    .line 60
    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgtc([Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;)V

    .line 62
    new-array v2, p1, [S

    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenter([S)V

    .line 63
    new-array v2, p1, [S

    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenterPrint([S)V

    .line 64
    return-void

    .line 52
    .end local v0    # "tableCellDescriptors":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    new-instance v3, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>()V

    aput-object v3, v2, v1

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    .restart local v0    # "tableCellDescriptors":[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    :cond_1
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;-><init>()V

    aput-object v2, v0, v1

    .line 56
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/TableProperties;

    .line 70
    .local v0, "tap":Lorg/apache/poi/hwpf/usermodel/TableProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getTlp()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->clone()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setTlp(Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;)V

    .line 71
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgshd([Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 72
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 77
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 78
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcHorizontal()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcHorizontal(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 79
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 81
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 82
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getBrcVertical()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setBrcVertical(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 84
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getShdTable()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setShdTable(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 86
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgbrcInsideDefault_0()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    .line 87
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 86
    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgbrcInsideDefault_0(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 88
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgbrcInsideDefault_1()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    .line 89
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 88
    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgbrcInsideDefault_1(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 91
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenter()[S

    move-result-object v2

    invoke-virtual {v2}, [S->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [S

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenter([S)V

    .line 92
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgdxaCenterPrint()[S

    move-result-object v2

    invoke-virtual {v2}, [S->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [S

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgdxaCenterPrint([S)V

    .line 94
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->setRgtc([Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;)V

    .line 95
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 99
    return-object v0

    .line 74
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v3

    aput-object v3, v2, v1

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 97
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    aput-object v2, v3, v1

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
