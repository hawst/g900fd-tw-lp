.class public Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;
.super Ljava/lang/Object;
.source "AbstractWordUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/converter/AbstractWordUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NumberingState"
.end annotation


# instance fields
.field private final levels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;

    .line 229
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;

    return-object v0
.end method
