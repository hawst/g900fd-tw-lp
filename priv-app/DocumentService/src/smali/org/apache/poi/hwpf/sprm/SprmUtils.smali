.class public final Lorg/apache/poi/hwpf/sprm/SprmUtils;
.super Ljava/lang/Object;
.source "SprmUtils.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static addSpecialSprm(S[BLjava/util/List;)I
    .locals 4
    .param p0, "instruction"    # S
    .param p1, "varParam"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S[B",
            "Ljava/util/List",
            "<[B>;)I"
        }
    .end annotation

    .prologue
    .line 47
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<[B>;"
    array-length v1, p1

    add-int/lit8 v1, v1, 0x4

    new-array v0, v1, [B

    .line 48
    .local v0, "sprm":[B
    const/4 v1, 0x0

    const/4 v2, 0x4

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    invoke-static {v0, p0}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 50
    const/4 v1, 0x2

    array-length v2, p1

    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 51
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    array-length v1, v0

    return v1
.end method

.method public static addSprm(SI[BLjava/util/List;)I
    .locals 8
    .param p0, "instruction"    # S
    .param p1, "param"    # I
    .param p2, "varParam"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI[B",
            "Ljava/util/List",
            "<[B>;)I"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v5, 0x2

    .line 63
    const v4, 0xe000

    and-int/2addr v4, p0

    shr-int/lit8 v2, v4, 0xd

    .line 65
    .local v2, "type":I
    const/4 v0, 0x0

    .line 66
    .local v0, "sprm":[B
    packed-switch v2, :pswitch_data_0

    .line 106
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 110
    :goto_1
    return v3

    .line 70
    :pswitch_0
    new-array v0, v6, [B

    .line 71
    int-to-byte v4, p1

    aput-byte v4, v0, v5

    goto :goto_0

    .line 74
    :pswitch_1
    new-array v0, v7, [B

    .line 75
    int-to-short v4, p1

    invoke-static {v0, v5, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    goto :goto_0

    .line 78
    :pswitch_2
    const/4 v4, 0x6

    new-array v0, v4, [B

    .line 79
    invoke-static {v0, v5, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    goto :goto_0

    .line 83
    :pswitch_3
    new-array v0, v7, [B

    .line 84
    int-to-short v4, p1

    invoke-static {v0, v5, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    goto :goto_0

    .line 88
    :pswitch_4
    if-eqz p2, :cond_0

    .line 89
    array-length v4, p2

    add-int/lit8 v4, v4, 0x3

    new-array v0, v4, [B

    .line 90
    array-length v4, p2

    int-to-byte v4, v4

    aput-byte v4, v0, v5

    .line 91
    array-length v4, p2

    invoke-static {p2, v3, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 95
    :pswitch_5
    const/4 v4, 0x5

    new-array v0, v4, [B

    .line 97
    new-array v1, v7, [B

    .line 98
    .local v1, "temp":[B
    invoke-static {v1, v3, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 99
    invoke-static {v1, v3, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 108
    .end local v1    # "temp":[B
    :cond_1
    invoke-static {v0, v3, p0}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 109
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    array-length v3, v0

    goto :goto_1

    .line 66
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static addSprm(SZLjava/util/List;)I
    .locals 2
    .param p0, "instruction"    # S
    .param p1, "param"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SZ",
            "Ljava/util/List",
            "<[B>;)I"
        }
    .end annotation

    .prologue
    .line 58
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<[B>;"
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1, p2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static convertBrcToInt([S)I
    .locals 3
    .param p0, "brc"    # [S

    .prologue
    .line 132
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 133
    .local v0, "buf":[B
    const/4 v1, 0x0

    aget-short v1, p0, v1

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 134
    const/4 v1, 0x2

    const/4 v2, 0x1

    aget-short v2, p0, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 135
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    return v1
.end method

.method public static getGrpprl(Ljava/util/List;I)[B
    .locals 6
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;I)[B"
        }
    .end annotation

    .prologue
    .local p0, "sprmList":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v5, 0x0

    .line 116
    new-array v0, p1, [B

    .line 117
    .local v0, "grpprl":[B
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 118
    .local v2, "listSize":I
    const/4 v1, 0x0

    .line 119
    .local v1, "index":I
    :goto_0
    if-gez v2, :cond_0

    .line 126
    return-object v0

    .line 121
    :cond_0
    invoke-interface {p0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 122
    .local v3, "sprm":[B
    array-length v4, v3

    invoke-static {v3, v5, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    array-length v4, v3

    add-int/2addr v1, v4

    .line 119
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public static shortArrayToByteArray([S)[B
    .locals 4
    .param p0, "convert"    # [S

    .prologue
    .line 35
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x2

    new-array v0, v2, [B

    .line 37
    .local v0, "buf":[B
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    array-length v2, p0

    if-lt v1, v2, :cond_0

    .line 42
    return-object v0

    .line 39
    :cond_0
    mul-int/lit8 v2, v1, 0x2

    aget-short v3, p0, v1

    invoke-static {v0, v2, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
