.class public Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;
.super Ljava/lang/Object;
.source "PICFAndOfficeArtData.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _blipRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation
.end field

.field private _cchPicName:S

.field private _picf:Lorg/apache/poi/hwpf/model/PICF;

.field private _shape:Lorg/apache/poi/ddf/EscherContainerRecord;

.field private _stPicName:[B


# direct methods
.method public constructor <init>([BI)V
    .locals 7
    .param p1, "dataStream"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    move v3, p2

    .line 46
    .local v3, "offset":I
    new-instance v5, Lorg/apache/poi/hwpf/model/PICF;

    invoke-direct {v5, p1, v3}, Lorg/apache/poi/hwpf/model/PICF;-><init>([BI)V

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    .line 47
    invoke-static {}, Lorg/apache/poi/hwpf/model/PICF;->getSize()I

    move-result v5

    add-int/2addr v3, v5

    .line 49
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PICF;->getMm()S

    move-result v5

    const/16 v6, 0x66

    if-ne v5, v6, :cond_0

    .line 51
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getUByte([BI)S

    move-result v5

    iput-short v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_cchPicName:S

    .line 52
    add-int/lit8 v3, v3, 0x1

    .line 55
    iget-short v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_cchPicName:S

    .line 54
    invoke-static {p1, v3, v5}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v5

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_stPicName:[B

    .line 56
    iget-short v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_cchPicName:S

    add-int/2addr v3, v5

    .line 59
    :cond_0
    new-instance v1, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v1}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 60
    .local v1, "escherRecordFactory":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    new-instance v5, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v5}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_shape:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 61
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_shape:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v5, p1, v3, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    move-result v4

    .line 63
    .local v4, "recordSize":I
    add-int/2addr v3, v4

    .line 65
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_blipRecords:Ljava/util/List;

    .line 66
    :goto_0
    sub-int v5, v3, p2

    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PICF;->getLcb()I

    move-result v6

    if-lt v5, v6, :cond_2

    .line 81
    :cond_1
    return-void

    .line 68
    :cond_2
    invoke-virtual {v1, p1, v3}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    .line 70
    .local v2, "nextRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v5

    const/16 v6, -0xff9

    if-eq v5, v6, :cond_3

    .line 71
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v5

    const/16 v6, -0xfe8

    if-lt v5, v6, :cond_1

    .line 72
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v5

    const/16 v6, -0xee9

    if-gt v5, v6, :cond_1

    .line 75
    :cond_3
    invoke-virtual {v2, p1, v3, v1}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    move-result v0

    .line 77
    .local v0, "blipRecordSize":I
    add-int/2addr v3, v0

    .line 79
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_blipRecords:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public getBlipRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_blipRecords:Ljava/util/List;

    return-object v0
.end method

.method public getPicf()Lorg/apache/poi/hwpf/model/PICF;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    return-object v0
.end method

.method public getShape()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_shape:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getStPicName()[B
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->_stPicName:[B

    return-object v0
.end method
