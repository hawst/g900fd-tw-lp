.class public Lorg/apache/poi/hwpf/model/FibBase;
.super Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;
.source "FibBase.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;-><init>()V

    .line 43
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FibBase;->fillFields([BI)V

    .line 44
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-ne p0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 55
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 56
    check-cast v0, Lorg/apache/poi/hwpf/model/FibBase;

    .line 57
    .local v0, "other":Lorg/apache/poi/hwpf/model/FibBase;
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_10_flags2:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_10_flags2:B

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 58
    goto :goto_0

    .line 59
    :cond_4
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_11_Chs:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_11_Chs:S

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 60
    goto :goto_0

    .line 61
    :cond_5
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_12_chsTables:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_12_chsTables:S

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 62
    goto :goto_0

    .line 63
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_13_fcMin:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_13_fcMin:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 64
    goto :goto_0

    .line 65
    :cond_7
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_14_fcMac:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_14_fcMac:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 66
    goto :goto_0

    .line 67
    :cond_8
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_1_wIdent:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_1_wIdent:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 68
    goto :goto_0

    .line 69
    :cond_9
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_2_nFib:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_2_nFib:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 70
    goto :goto_0

    .line 71
    :cond_a
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_3_unused:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_3_unused:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 72
    goto :goto_0

    .line 73
    :cond_b
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_4_lid:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_4_lid:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 74
    goto :goto_0

    .line 75
    :cond_c
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_5_pnNext:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_5_pnNext:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 76
    goto :goto_0

    .line 77
    :cond_d
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_6_flags1:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_6_flags1:S

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 78
    goto :goto_0

    .line 79
    :cond_e
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_7_nFibBack:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_7_nFibBack:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 80
    goto :goto_0

    .line 81
    :cond_f
    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_8_lKey:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_8_lKey:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 82
    goto :goto_0

    .line 83
    :cond_10
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_9_envr:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/FibBase;->field_9_envr:B

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 84
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 92
    const/16 v0, 0x1f

    .line 93
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 94
    .local v1, "result":I
    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_10_flags2:B

    add-int/lit8 v1, v2, 0x1f

    .line 95
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_11_Chs:S

    add-int v1, v2, v3

    .line 96
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_12_chsTables:S

    add-int v1, v2, v3

    .line 97
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_13_fcMin:I

    add-int v1, v2, v3

    .line 98
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_14_fcMac:I

    add-int v1, v2, v3

    .line 99
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_1_wIdent:I

    add-int v1, v2, v3

    .line 100
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_2_nFib:I

    add-int v1, v2, v3

    .line 101
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_3_unused:I

    add-int v1, v2, v3

    .line 102
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_4_lid:I

    add-int v1, v2, v3

    .line 103
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_5_pnNext:I

    add-int v1, v2, v3

    .line 104
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_6_flags1:S

    add-int v1, v2, v3

    .line 105
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_7_nFibBack:I

    add-int v1, v2, v3

    .line 106
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_8_lKey:I

    add-int v1, v2, v3

    .line 107
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/FibBase;->field_9_envr:B

    add-int v1, v2, v3

    .line 108
    return v1
.end method
