.class Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;
.super Ljava/lang/Object;
.source "TextPieceTable.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/model/TextPieceTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FCComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/poi/hwpf/model/TextPiece;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;)V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/model/TextPiece;

    check-cast p2, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;->compare(Lorg/apache/poi/hwpf/model/TextPiece;Lorg/apache/poi/hwpf/model/TextPiece;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/poi/hwpf/model/TextPiece;Lorg/apache/poi/hwpf/model/TextPiece;)I
    .locals 2
    .param p1, "textPiece"    # Lorg/apache/poi/hwpf/model/TextPiece;
    .param p2, "textPiece1"    # Lorg/apache/poi/hwpf/model/TextPiece;

    .prologue
    .line 523
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v0

    iget v0, v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 524
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v1

    iget v1, v1, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    if-le v0, v1, :cond_0

    .line 526
    const/4 v0, 0x1

    .line 535
    :goto_0
    return v0

    .line 528
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v0

    iget v0, v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 529
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v1

    iget v1, v1, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    if-ge v0, v1, :cond_1

    .line 531
    const/4 v0, -0x1

    goto :goto_0

    .line 535
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
