.class public final Lorg/apache/poi/hwpf/usermodel/ListEntry;
.super Lorg/apache/poi/hwpf/usermodel/Paragraph;
.source "ListEntry.java"


# direct methods
.method constructor <init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 0
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "properties"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/usermodel/Paragraph;-><init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 30
    return-void
.end method

.method constructor <init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/Range;Lorg/apache/poi/hwpf/model/ListTables;)V
    .locals 0
    .param p1, "papx"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p3, "tables"    # Lorg/apache/poi/hwpf/model/ListTables;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/Paragraph;-><init>(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 36
    return-void
.end method


# virtual methods
.method public type()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x4

    return v0
.end method
