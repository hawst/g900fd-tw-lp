.class public final Lorg/apache/poi/hwpf/sprm/SprmIterator;
.super Ljava/lang/Object;
.source "SprmIterator.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _grpprl:[B

.field _offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_grpprl:[B

    .line 37
    iput p2, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_offset:I

    .line 38
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_offset:I

    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_grpprl:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lorg/apache/poi/hwpf/sprm/SprmOperation;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;

    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_grpprl:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_offset:I

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;-><init>([BI)V

    .line 49
    .local v0, "op":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_offset:I

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmIterator;->_offset:I

    .line 50
    return-object v0
.end method
