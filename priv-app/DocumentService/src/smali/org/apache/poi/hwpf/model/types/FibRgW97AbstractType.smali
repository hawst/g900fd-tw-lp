.class public abstract Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;
.super Ljava/lang/Object;
.source "FibRgW97AbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected field_10_reserved10:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_11_reserved11:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_12_reserved12:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_13_reserved13:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_14_lidFE:S

.field protected field_1_reserved1:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_2_reserved2:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_3_reserved3:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_4_reserved4:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_5_reserved5:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_6_reserved6:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_7_reserved7:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_8_reserved8:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_9_reserved9:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 123
    const/16 v0, 0x1c

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 77
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_1_reserved1:S

    .line 78
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_2_reserved2:S

    .line 79
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_3_reserved3:S

    .line 80
    add-int/lit8 v0, p2, 0x6

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_4_reserved4:S

    .line 81
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_5_reserved5:S

    .line 82
    add-int/lit8 v0, p2, 0xa

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_6_reserved6:S

    .line 83
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_7_reserved7:S

    .line 84
    add-int/lit8 v0, p2, 0xe

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_8_reserved8:S

    .line 85
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_9_reserved9:S

    .line 86
    add-int/lit8 v0, p2, 0x12

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_10_reserved10:S

    .line 87
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_11_reserved11:S

    .line 88
    add-int/lit8 v0, p2, 0x16

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_12_reserved12:S

    .line 89
    add-int/lit8 v0, p2, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_13_reserved13:S

    .line 90
    add-int/lit8 v0, p2, 0x1a

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_14_lidFE:S

    .line 91
    return-void
.end method

.method public getLidFE()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 403
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_14_lidFE:S

    return v0
.end method

.method public getReserved1()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 169
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_1_reserved1:S

    return v0
.end method

.method public getReserved10()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 331
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_10_reserved10:S

    return v0
.end method

.method public getReserved11()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 349
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_11_reserved11:S

    return v0
.end method

.method public getReserved12()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 367
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_12_reserved12:S

    return v0
.end method

.method public getReserved13()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 385
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_13_reserved13:S

    return v0
.end method

.method public getReserved2()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 187
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_2_reserved2:S

    return v0
.end method

.method public getReserved3()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 205
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_3_reserved3:S

    return v0
.end method

.method public getReserved4()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 223
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_4_reserved4:S

    return v0
.end method

.method public getReserved5()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 241
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_5_reserved5:S

    return v0
.end method

.method public getReserved6()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 259
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_6_reserved6:S

    return v0
.end method

.method public getReserved7()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 277
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_7_reserved7:S

    return v0
.end method

.method public getReserved8()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 295
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_8_reserved8:S

    return v0
.end method

.method public getReserved9()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 313
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_9_reserved9:S

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 95
    add-int/lit8 v0, p2, 0x0

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_1_reserved1:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 96
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_2_reserved2:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 97
    add-int/lit8 v0, p2, 0x4

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_3_reserved3:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 98
    add-int/lit8 v0, p2, 0x6

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_4_reserved4:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 99
    add-int/lit8 v0, p2, 0x8

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_5_reserved5:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 100
    add-int/lit8 v0, p2, 0xa

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_6_reserved6:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 101
    add-int/lit8 v0, p2, 0xc

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_7_reserved7:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 102
    add-int/lit8 v0, p2, 0xe

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_8_reserved8:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 103
    add-int/lit8 v0, p2, 0x10

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_9_reserved9:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 104
    add-int/lit8 v0, p2, 0x12

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_10_reserved10:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 105
    add-int/lit8 v0, p2, 0x14

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_11_reserved11:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 106
    add-int/lit8 v0, p2, 0x16

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_12_reserved12:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 107
    add-int/lit8 v0, p2, 0x18

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_13_reserved13:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 108
    add-int/lit8 v0, p2, 0x1a

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_14_lidFE:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 109
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 113
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 114
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->serialize([BI)V

    .line 115
    return-object v0
.end method

.method public setLidFE(S)V
    .locals 0
    .param p1, "field_14_lidFE"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 412
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_14_lidFE:S

    .line 413
    return-void
.end method

.method public setReserved1(S)V
    .locals 0
    .param p1, "field_1_reserved1"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 178
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_1_reserved1:S

    .line 179
    return-void
.end method

.method public setReserved10(S)V
    .locals 0
    .param p1, "field_10_reserved10"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 340
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_10_reserved10:S

    .line 341
    return-void
.end method

.method public setReserved11(S)V
    .locals 0
    .param p1, "field_11_reserved11"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 358
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_11_reserved11:S

    .line 359
    return-void
.end method

.method public setReserved12(S)V
    .locals 0
    .param p1, "field_12_reserved12"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 376
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_12_reserved12:S

    .line 377
    return-void
.end method

.method public setReserved13(S)V
    .locals 0
    .param p1, "field_13_reserved13"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 394
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_13_reserved13:S

    .line 395
    return-void
.end method

.method public setReserved2(S)V
    .locals 0
    .param p1, "field_2_reserved2"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 196
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_2_reserved2:S

    .line 197
    return-void
.end method

.method public setReserved3(S)V
    .locals 0
    .param p1, "field_3_reserved3"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 214
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_3_reserved3:S

    .line 215
    return-void
.end method

.method public setReserved4(S)V
    .locals 0
    .param p1, "field_4_reserved4"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 232
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_4_reserved4:S

    .line 233
    return-void
.end method

.method public setReserved5(S)V
    .locals 0
    .param p1, "field_5_reserved5"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 250
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_5_reserved5:S

    .line 251
    return-void
.end method

.method public setReserved6(S)V
    .locals 0
    .param p1, "field_6_reserved6"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 268
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_6_reserved6:S

    .line 269
    return-void
.end method

.method public setReserved7(S)V
    .locals 0
    .param p1, "field_7_reserved7"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 286
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_7_reserved7:S

    .line 287
    return-void
.end method

.method public setReserved8(S)V
    .locals 0
    .param p1, "field_8_reserved8"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 304
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_8_reserved8:S

    .line 305
    return-void
.end method

.method public setReserved9(S)V
    .locals 0
    .param p1, "field_9_reserved9"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 322
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->field_9_reserved9:S

    .line 323
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[FibRgW97]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string/jumbo v1, "    .reserved1            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved1()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string/jumbo v1, "    .reserved2            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved2()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    const-string/jumbo v1, "    .reserved3            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved3()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const-string/jumbo v1, "    .reserved4            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved4()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string/jumbo v1, "    .reserved5            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved5()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string/jumbo v1, "    .reserved6            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved6()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string/jumbo v1, "    .reserved7            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved7()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string/jumbo v1, "    .reserved8            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved8()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v1, "    .reserved9            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved9()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string/jumbo v1, "    .reserved10           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved10()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, "    .reserved11           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved11()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v1, "    .reserved12           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved12()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string/jumbo v1, "    .reserved13           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getReserved13()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v1, "    .lidFE                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;->getLidFE()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string/jumbo v1, "[/FibRgW97]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
