.class public final enum Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;
.super Ljava/lang/Enum;
.source "OfficeDrawing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HorizontalPositioning"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

.field public static final enum CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

.field public static final enum INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

.field public static final enum LEFT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

.field public static final enum OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

.field public static final enum RIGHT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    const-string/jumbo v1, "ABSOLUTE"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;-><init>(Ljava/lang/String;I)V

    .line 46
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    .line 48
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    const-string/jumbo v1, "CENTER"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;-><init>(Ljava/lang/String;I)V

    .line 52
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    .line 54
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    const-string/jumbo v1, "INSIDE"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;-><init>(Ljava/lang/String;I)V

    .line 58
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    .line 60
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    const-string/jumbo v1, "LEFT"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;-><init>(Ljava/lang/String;I)V

    .line 64
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->LEFT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    .line 66
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    const-string/jumbo v1, "OUTSIDE"

    invoke-direct {v0, v1, v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;-><init>(Ljava/lang/String;I)V

    .line 70
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    .line 72
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    const-string/jumbo v1, "RIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;-><init>(Ljava/lang/String;I)V

    .line 76
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->RIGHT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->LEFT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->RIGHT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
