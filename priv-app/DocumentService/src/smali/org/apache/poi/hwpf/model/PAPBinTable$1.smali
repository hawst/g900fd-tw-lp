.class Lorg/apache/poi/hwpf/model/PAPBinTable$1;
.super Ljava/lang/Object;
.source "PAPBinTable.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/hwpf/model/PAPBinTable;->rebuild(Ljava/lang/StringBuilder;Lorg/apache/poi/hwpf/model/ComplexFileTable;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/poi/hwpf/model/PAPX;",
        ">;"
    }
.end annotation


# instance fields
.field private final synthetic val$papxToFileOrder:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/PAPBinTable$1;->val$papxToFileOrder:Ljava/util/Map;

    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/model/PAPX;

    check-cast p2, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/PAPBinTable$1;->compare(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/model/PAPX;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/poi/hwpf/model/PAPX;Lorg/apache/poi/hwpf/model/PAPX;)I
    .locals 3
    .param p1, "o1"    # Lorg/apache/poi/hwpf/model/PAPX;
    .param p2, "o2"    # Lorg/apache/poi/hwpf/model/PAPX;

    .prologue
    .line 199
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PAPBinTable$1;->val$papxToFileOrder:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 200
    .local v0, "i1":Ljava/lang/Integer;
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PAPBinTable$1;->val$papxToFileOrder:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 201
    .local v1, "i2":Ljava/lang/Integer;
    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v2

    return v2
.end method
