.class public Lorg/apache/poi/hwpf/model/FieldsTables;
.super Ljava/lang/Object;
.source "FieldsTables.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final FLD_SIZE:I = 0x2

.field public static final PLCFFLDATN:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFFLDEDN:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFFLDFTN:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFFLDHDR:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFFLDHDRTXBX:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFFLDMOM:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PLCFFLDTXBX:I = 0x6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private _tables:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/poi/hwpf/model/FieldsDocumentPart;",
            "Lorg/apache/poi/hwpf/model/PlexOfCps;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;)V
    .locals 6
    .param p1, "tableStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v2, Ljava/util/HashMap;

    .line 101
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v3

    array-length v3, v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 100
    iput-object v2, p0, Lorg/apache/poi/hwpf/model/FieldsTables;->_tables:Ljava/util/Map;

    .line 103
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 108
    return-void

    .line 103
    :cond_0
    aget-object v0, v3, v2

    .line 105
    .local v0, "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/model/FieldsTables;->readPLCF([BLorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Lorg/apache/poi/hwpf/model/PlexOfCps;

    move-result-object v1

    .line 106
    .local v1, "plexOfCps":Lorg/apache/poi/hwpf/model/PlexOfCps;
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FieldsTables;->_tables:Ljava/util/Map;

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private readPLCF([BLorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Lorg/apache/poi/hwpf/model/PlexOfCps;
    .locals 4
    .param p1, "tableStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p3, "documentPart"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .prologue
    .line 124
    invoke-virtual {p2, p3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFieldsPlcfOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)I

    move-result v1

    .line 125
    .local v1, "start":I
    invoke-virtual {p2, p3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFieldsPlcfLength(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)I

    move-result v0

    .line 127
    .local v0, "length":I
    if-lez v1, :cond_0

    if-gtz v0, :cond_1

    .line 128
    :cond_0
    const/4 v2, 0x0

    .line 130
    :goto_0
    return-object v2

    :cond_1
    new-instance v2, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v3, 0x2

    invoke-direct {v2, p1, v1, v0, v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    goto :goto_0
.end method

.method private savePlex(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FieldsDocumentPart;Lorg/apache/poi/hwpf/model/PlexOfCps;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)I
    .locals 4
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .param p3, "plexOfCps"    # Lorg/apache/poi/hwpf/model/PlexOfCps;
    .param p4, "outputStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 137
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 139
    :cond_0
    invoke-virtual {p4}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v3

    invoke-virtual {p1, p2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFieldsPlcfOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)V

    .line 140
    invoke-virtual {p1, p2, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFieldsPlcfLength(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)V

    .line 154
    :goto_0
    return v1

    .line 144
    :cond_1
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v0

    .line 146
    .local v0, "data":[B
    invoke-virtual {p4}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v2

    .line 147
    .local v2, "start":I
    array-length v1, v0

    .line 149
    .local v1, "length":I
    invoke-virtual {p4, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 151
    invoke-virtual {p1, p2, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFieldsPlcfOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)V

    .line 152
    invoke-virtual {p1, p2, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFieldsPlcfLength(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)V

    goto :goto_0
.end method

.method private static toArrayList(Lorg/apache/poi/hwpf/model/PlexOfCps;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "plexOfCps"    # Lorg/apache/poi/hwpf/model/PlexOfCps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/model/PlexOfCps;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PlexOfField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    if-nez p0, :cond_1

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    :cond_0
    return-object v0

    .line 84
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    .line 85
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v4

    .line 84
    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 86
    .local v0, "fields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hwpf/model/PlexOfField;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 88
    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v3

    .line 89
    .local v3, "propNode":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    new-instance v2, Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-direct {v2, v3}, Lorg/apache/poi/hwpf/model/PlexOfField;-><init>(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 90
    .local v2, "plex":Lorg/apache/poi/hwpf/model/PlexOfField;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getFieldsPLCF(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "partIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PlexOfField;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 118
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/model/FieldsTables;->getFieldsPLCF(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getFieldsPLCF(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/model/FieldsDocumentPart;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PlexOfField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FieldsTables;->_tables:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-static {v0}, Lorg/apache/poi/hwpf/model/FieldsTables;->toArrayList(Lorg/apache/poi/hwpf/model/PlexOfCps;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public write(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 6
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 165
    return-void

    .line 160
    :cond_0
    aget-object v0, v3, v2

    .line 162
    .local v0, "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FieldsTables;->_tables:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 163
    .local v1, "plexOfCps":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/poi/hwpf/model/FieldsTables;->savePlex(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FieldsDocumentPart;Lorg/apache/poi/hwpf/model/PlexOfCps;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)I

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
