.class public final Lorg/apache/poi/hwpf/dev/HWPFLister;
.super Ljava/lang/Object;
.source "HWPFLister.java"


# instance fields
.field private final _doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

.field private paragraphs:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V
    .locals 0
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .prologue
    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 322
    iput-object p1, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .line 324
    invoke-direct {p0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->buildParagraphs()V

    .line 325
    return-void
.end method

.method private buildParagraphs()V
    .locals 7

    .prologue
    .line 329
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->paragraphs:Ljava/util/LinkedHashMap;

    .line 331
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 332
    .local v2, "part":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getDocumentText()Ljava/lang/String;

    move-result-object v3

    .line 333
    .local v3, "text":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "charIndex":I
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 343
    return-void

    .line 335
    :cond_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 336
    .local v0, "c":C
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 337
    const/16 v4, 0xd

    if-eq v0, v4, :cond_1

    const/4 v4, 0x7

    if-eq v0, v4, :cond_1

    const/16 v4, 0xc

    if-ne v0, v4, :cond_2

    .line 339
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->paragraphs:Ljava/util/LinkedHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 333
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private dumpBookmarks()V
    .locals 4

    .prologue
    .line 347
    iget-object v3, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v3, v3, Lorg/apache/poi/hwpf/HWPFDocument;

    if-nez v3, :cond_1

    .line 361
    :cond_0
    return-void

    .line 353
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v2, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 354
    .local v2, "document":Lorg/apache/poi/hwpf/HWPFDocument;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocument;->getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    move-result-object v1

    .line 355
    .local v1, "bookmarks":Lorg/apache/poi/hwpf/usermodel/Bookmarks;
    const/4 v0, 0x0

    .local v0, "b":I
    :goto_0
    invoke-interface {v1}, Lorg/apache/poi/hwpf/usermodel/Bookmarks;->getBookmarksCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 357
    invoke-interface {v1, v0}, Lorg/apache/poi/hwpf/usermodel/Bookmarks;->getBookmark(I)Lorg/apache/poi/hwpf/usermodel/Bookmark;

    .line 355
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private dumpDop()V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v0, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-nez v0, :cond_0

    .line 418
    :cond_0
    return-void
.end method

.method private dumpEscher()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v0, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;

    if-eqz v0, :cond_0

    .line 429
    :cond_0
    return-void
.end method

.method private dumpFields()V
    .locals 7

    .prologue
    .line 441
    iget-object v2, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v2, v2, Lorg/apache/poi/hwpf/HWPFDocument;

    if-nez v2, :cond_1

    .line 457
    :cond_0
    return-void

    .line 447
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v0, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 449
    .local v0, "document":Lorg/apache/poi/hwpf/HWPFDocument;
    invoke-static {}, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->values()[Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 452
    .local v1, "part":Lorg/apache/poi/hwpf/model/FieldsDocumentPart;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getFields()Lorg/apache/poi/hwpf/usermodel/Fields;

    move-result-object v2

    invoke-interface {v2, v1}, Lorg/apache/poi/hwpf/usermodel/Fields;->getFields(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 449
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 452
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/Field;

    goto :goto_1
.end method

.method private dumpFileSystem(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)Ljava/lang/String;
    .locals 6
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .prologue
    .line 471
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 472
    .local v3, "result":Ljava/lang/StringBuilder;
    const-string/jumbo v4, "+ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v2

    .line 475
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 482
    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 477
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 478
    .local v0, "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "\n"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpFileSystem(Lorg/apache/poi/poifs/filesystem/Entry;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 479
    .local v1, "entryToString":Ljava/lang/String;
    const-string/jumbo v4, "\n"

    const-string/jumbo v5, "\n+---"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 480
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private dumpFileSystem(Lorg/apache/poi/poifs/filesystem/Entry;)Ljava/lang/String;
    .locals 1
    .param p1, "entry"    # Lorg/apache/poi/poifs/filesystem/Entry;

    .prologue
    .line 488
    instance-of v0, p1, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    if-eqz v0, :cond_0

    .line 489
    check-cast p1, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .end local p1    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpFileSystem(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)Ljava/lang/String;

    move-result-object v0

    .line 491
    :goto_0
    return-object v0

    .restart local p1    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_0
    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private dumpOfficeDrawings()V
    .locals 3

    .prologue
    .line 496
    iget-object v1, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v1, v1, Lorg/apache/poi/hwpf/HWPFDocument;

    if-nez v1, :cond_1

    .line 524
    :cond_0
    return-void

    .line 502
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v0, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 504
    .local v0, "document":Lorg/apache/poi/hwpf/HWPFDocument;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getOfficeDrawingsHeaders()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 508
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getOfficeDrawingsHeaders()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;->getOfficeDrawings()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 507
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 508
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    goto :goto_0
.end method

.method private dumpPictures()V
    .locals 3

    .prologue
    .line 673
    iget-object v1, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v1, v1, Lorg/apache/poi/hwpf/HWPFOldDocument;

    if-eqz v1, :cond_1

    .line 685
    :cond_0
    return-void

    .line 679
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v1, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocument;->getPicturesTable()Lorg/apache/poi/hwpf/model/PicturesTable;

    move-result-object v1

    .line 680
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/PicturesTable;->getAllPictures()Ljava/util/List;

    move-result-object v0

    .line 681
    .local v0, "allPictures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Picture;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/Picture;

    goto :goto_0
.end method

.method private dumpStyles()V
    .locals 6

    .prologue
    .line 689
    iget-object v3, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v3, v3, Lorg/apache/poi/hwpf/HWPFOldDocument;

    if-eqz v3, :cond_1

    .line 715
    :cond_0
    return-void

    .line 694
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v0, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 696
    .local v0, "hwpfDocument":Lorg/apache/poi/hwpf/HWPFDocument;
    const/4 v1, 0x0

    .local v1, "s":I
    :goto_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->numStyles()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 698
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v3

    .line 699
    invoke-virtual {v3, v1}, Lorg/apache/poi/hwpf/model/StyleSheet;->getStyleDescription(I)Lorg/apache/poi/hwpf/model/StyleDescription;

    move-result-object v2

    .line 700
    .local v2, "styleDescription":Lorg/apache/poi/hwpf/model/StyleDescription;
    if-nez v2, :cond_3

    .line 696
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 707
    :cond_3
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAPX()[B

    move-result-object v3

    if-eqz v3, :cond_4

    .line 708
    new-instance v3, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAPX()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 709
    const-string/jumbo v4, "Style\'s PAP SPRM: "

    .line 708
    invoke-virtual {p0, v3, v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    .line 711
    :cond_4
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHPX()[B

    move-result-object v3

    if-eqz v3, :cond_2

    .line 712
    new-instance v3, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHPX()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 713
    const-string/jumbo v4, "Style\'s CHP SPRM: "

    .line 712
    invoke-virtual {p0, v3, v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 2
    .param p0, "docFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 87
    .local v0, "istream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-static {v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->loadDoc(Ljava/io/InputStream;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 91
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 87
    return-object v1

    .line 90
    :catchall_0
    move-exception v1

    .line 91
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 92
    throw v1
.end method

.method private static loadDoc(Ljava/io/InputStream;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 3
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v1

    .line 102
    .local v1, "poifsFileSystem":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    :try_start_0
    new-instance v2, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v2, v1}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    :try_end_0
    .catch Lorg/apache/poi/hwpf/OldWordFileFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-object v2

    .line 104
    :catch_0
    move-exception v0

    .line 106
    .local v0, "exc":Lorg/apache/poi/hwpf/OldWordFileFormatException;
    new-instance v2, Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-direct {v2, v1}, Lorg/apache/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 27
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v24, v0

    if-nez v24, :cond_0

    .line 114
    sget-object v24, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v25, "Use:"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 115
    sget-object v24, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v25, "\tHWPFLister <filename>\n\t\t[--dop]\n\t\t[--textPieces] [--textPiecesText]\n\t\t[--chpx] [--chpxProperties] [--chpxSprms]\n\t\t[--papx] [--papxProperties] [--papxSprms]\n\t\t[--paragraphs] [--paragraphsText]\n\t\t[--bookmarks]\n\t\t[--escher]\n\t\t[--fields]\n\t\t[--pictures]\n\t\t[--officeDrawings]\n\t\t[--styles]\n\t\t[--writereadback]\n"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 124
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Ljava/lang/System;->exit(I)V

    .line 127
    :cond_0
    const/4 v10, 0x0

    .line 129
    .local v10, "outputDop":Z
    const/16 v21, 0x0

    .line 130
    .local v21, "outputTextPieces":Z
    const/16 v22, 0x0

    .line 132
    .local v22, "outputTextPiecesText":Z
    const/4 v7, 0x0

    .line 133
    .local v7, "outputChpx":Z
    const/4 v8, 0x0

    .line 134
    .local v8, "outputChpxProperties":Z
    const/4 v9, 0x0

    .line 136
    .local v9, "outputChpxSprms":Z
    const/16 v17, 0x0

    .line 137
    .local v17, "outputParagraphs":Z
    const/16 v18, 0x0

    .line 139
    .local v18, "outputParagraphsText":Z
    const/4 v14, 0x0

    .line 140
    .local v14, "outputPapx":Z
    const/16 v16, 0x0

    .line 141
    .local v16, "outputPapxSprms":Z
    const/4 v15, 0x0

    .line 143
    .local v15, "outputPapxProperties":Z
    const/4 v6, 0x0

    .line 144
    .local v6, "outputBookmarks":Z
    const/4 v11, 0x0

    .line 145
    .local v11, "outputEscher":Z
    const/4 v12, 0x0

    .line 146
    .local v12, "outputFields":Z
    const/16 v19, 0x0

    .line 147
    .local v19, "outputPictures":Z
    const/4 v13, 0x0

    .line 148
    .local v13, "outputOfficeDrawings":Z
    const/16 v20, 0x0

    .line 150
    .local v20, "outputStyles":Z
    const/16 v23, 0x0

    .line 152
    .local v23, "writereadback":Z
    invoke-static/range {p0 .. p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v24

    const/16 v25, 0x1

    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v26, v0

    invoke-interface/range {v24 .. v26}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_1
    :goto_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_f

    .line 198
    new-instance v24, Ljava/io/File;

    const/16 v25, 0x0

    aget-object v25, p0, v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v24 .. v24}, Lorg/apache/poi/hwpf/dev/HWPFLister;->loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v2

    .line 199
    .local v2, "doc":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    if-eqz v23, :cond_2

    .line 200
    invoke-static {v2}, Lorg/apache/poi/hwpf/dev/HWPFLister;->writeOutAndReadBack(Lorg/apache/poi/hwpf/HWPFDocumentCore;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v2

    .line 204
    :cond_2
    const-string/jumbo v24, "org.apache.poi.hwpf.preserveBinTables"

    .line 205
    sget-object v25, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v25

    .line 204
    invoke-static/range {v24 .. v25}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 206
    const-string/jumbo v24, "org.apache.poi.hwpf.preserveTextTable"

    .line 207
    sget-object v25, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v25

    .line 206
    invoke-static/range {v24 .. v25}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 209
    new-instance v24, Ljava/io/File;

    const/16 v25, 0x0

    aget-object v25, p0, v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v24 .. v24}, Lorg/apache/poi/hwpf/dev/HWPFLister;->loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v5

    .line 210
    .local v5, "original":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    if-eqz v23, :cond_3

    .line 211
    invoke-static {v5}, Lorg/apache/poi/hwpf/dev/HWPFLister;->writeOutAndReadBack(Lorg/apache/poi/hwpf/HWPFDocumentCore;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v5

    .line 214
    :cond_3
    new-instance v3, Lorg/apache/poi/hwpf/dev/HWPFLister;

    invoke-direct {v3, v5}, Lorg/apache/poi/hwpf/dev/HWPFLister;-><init>(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 215
    .local v3, "listerOriginal":Lorg/apache/poi/hwpf/dev/HWPFLister;
    new-instance v4, Lorg/apache/poi/hwpf/dev/HWPFLister;

    invoke-direct {v4, v2}, Lorg/apache/poi/hwpf/dev/HWPFLister;-><init>(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 218
    .local v4, "listerRebuilded":Lorg/apache/poi/hwpf/dev/HWPFLister;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpFileSystem()V

    .line 221
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpFIB()V

    .line 223
    if-eqz v10, :cond_4

    .line 226
    invoke-direct {v3}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpDop()V

    .line 229
    :cond_4
    if-eqz v21, :cond_5

    .line 232
    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpTextPieces(Z)V

    .line 235
    :cond_5
    if-eqz v7, :cond_6

    .line 238
    invoke-virtual {v3, v8, v9}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpChpx(ZZ)V

    .line 241
    invoke-virtual {v4, v8, v9}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpChpx(ZZ)V

    .line 244
    :cond_6
    if-eqz v14, :cond_7

    .line 247
    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpPapx(ZZ)V

    .line 250
    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpPapx(ZZ)V

    .line 253
    :cond_7
    if-eqz v17, :cond_8

    .line 256
    const/16 v24, 0x1

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpParagraphs(Z)V

    .line 259
    move/from16 v0, v18

    invoke-virtual {v4, v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpParagraphsDom(Z)V

    .line 262
    :cond_8
    if-eqz v6, :cond_9

    .line 265
    invoke-direct {v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpBookmarks()V

    .line 268
    :cond_9
    if-eqz v11, :cond_a

    .line 271
    invoke-direct {v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpEscher()V

    .line 274
    :cond_a
    if-eqz v12, :cond_b

    .line 277
    invoke-direct {v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpFields()V

    .line 280
    :cond_b
    if-eqz v13, :cond_c

    .line 283
    invoke-direct {v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpOfficeDrawings()V

    .line 286
    :cond_c
    if-eqz v19, :cond_d

    .line 289
    invoke-direct {v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpPictures()V

    .line 292
    :cond_d
    if-eqz v20, :cond_e

    .line 295
    invoke-direct {v4}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpStyles()V

    .line 297
    :cond_e
    return-void

    .line 152
    .end local v2    # "doc":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .end local v3    # "listerOriginal":Lorg/apache/poi/hwpf/dev/HWPFLister;
    .end local v4    # "listerRebuilded":Lorg/apache/poi/hwpf/dev/HWPFLister;
    .end local v5    # "original":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    :cond_f
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 154
    .local v1, "arg":Ljava/lang/String;
    const-string/jumbo v25, "--dop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 155
    const/4 v10, 0x1

    .line 157
    :cond_10
    const-string/jumbo v25, "--textPieces"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 158
    const/16 v21, 0x1

    .line 159
    :cond_11
    const-string/jumbo v25, "--textPiecesText"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 160
    const/16 v22, 0x1

    .line 162
    :cond_12
    const-string/jumbo v25, "--chpx"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 163
    const/4 v7, 0x1

    .line 164
    :cond_13
    const-string/jumbo v25, "--chpxProperties"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 165
    const/4 v8, 0x1

    .line 166
    :cond_14
    const-string/jumbo v25, "--chpxSprms"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_15

    .line 167
    const/4 v9, 0x1

    .line 169
    :cond_15
    const-string/jumbo v25, "--paragraphs"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 170
    const/16 v17, 0x1

    .line 171
    :cond_16
    const-string/jumbo v25, "--paragraphsText"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_17

    .line 172
    const/16 v18, 0x1

    .line 174
    :cond_17
    const-string/jumbo v25, "--papx"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_18

    .line 175
    const/4 v14, 0x1

    .line 176
    :cond_18
    const-string/jumbo v25, "--papxProperties"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_19

    .line 177
    const/4 v15, 0x1

    .line 178
    :cond_19
    const-string/jumbo v25, "--papxSprms"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1a

    .line 179
    const/16 v16, 0x1

    .line 181
    :cond_1a
    const-string/jumbo v25, "--bookmarks"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1b

    .line 182
    const/4 v6, 0x1

    .line 183
    :cond_1b
    const-string/jumbo v25, "--escher"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1c

    .line 184
    const/4 v11, 0x1

    .line 185
    :cond_1c
    const-string/jumbo v25, "--fields"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1d

    .line 186
    const/4 v12, 0x1

    .line 187
    :cond_1d
    const-string/jumbo v25, "--pictures"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1e

    .line 188
    const/16 v19, 0x1

    .line 189
    :cond_1e
    const-string/jumbo v25, "--officeDrawings"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1f

    .line 190
    const/4 v13, 0x1

    .line 191
    :cond_1f
    const-string/jumbo v25, "--styles"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_20

    .line 192
    const/16 v20, 0x1

    .line 194
    :cond_20
    const-string/jumbo v25, "--writereadback"

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 195
    const/16 v23, 0x1

    goto/16 :goto_0
.end method

.method private static writeOutAndReadBack(Lorg/apache/poi/hwpf/HWPFDocumentCore;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 4
    .param p0, "original"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .prologue
    .line 304
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x1000

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 305
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->write(Ljava/io/OutputStream;)V

    .line 306
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .line 307
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 306
    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 308
    .local v0, "bais":Ljava/io/ByteArrayInputStream;
    invoke-static {v0}, Lorg/apache/poi/hwpf/dev/HWPFLister;->loadDoc(Ljava/io/InputStream;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 310
    .end local v0    # "bais":Ljava/io/ByteArrayInputStream;
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v2

    .line 312
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method


# virtual methods
.method public dumpChpx(ZZ)V
    .locals 12
    .param p1, "withProperties"    # Z
    .param p2, "withSprms"    # Z

    .prologue
    const/4 v6, 0x0

    .line 365
    iget-object v5, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/CHPBinTable;->getTextRuns()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 407
    return-void

    .line 365
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/CHPX;

    .line 375
    .local v1, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    if-eqz p2, :cond_2

    .line 377
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v5

    invoke-direct {v2, v5, v6}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 378
    .local v2, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 387
    .end local v2    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_2
    new-instance v5, Lorg/apache/poi/hwpf/dev/HWPFLister$1;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v8

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v9

    .line 388
    iget-object v10, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v10}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v10

    .line 387
    invoke-direct {v5, p0, v8, v9, v10}, Lorg/apache/poi/hwpf/dev/HWPFLister$1;-><init>(Lorg/apache/poi/hwpf/dev/HWPFLister;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 394
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/dev/HWPFLister$1;->text()Ljava/lang/String;

    move-result-object v4

    .line 395
    .local v4, "text":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 396
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    array-length v9, v8

    move v5, v6

    :goto_1
    if-ge v5, v9, :cond_0

    aget-char v0, v8, v5

    .line 398
    .local v0, "c":C
    const/16 v10, 0x1e

    if-ge v0, v10, :cond_4

    .line 400
    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "\\0x"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 380
    .end local v0    # "c":C
    .end local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "text":Ljava/lang/String;
    .restart local v2    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_3
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    goto :goto_0

    .line 402
    .end local v2    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .restart local v0    # "c":C
    .restart local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v4    # "text":Ljava/lang/String;
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public dumpFIB()V
    .locals 0

    .prologue
    .line 437
    return-void
.end method

.method public dumpFileSystem()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 461
    const-class v1, Lorg/apache/poi/POIDocument;

    .line 462
    const-string/jumbo v2, "directory"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 463
    .local v0, "field":Ljava/lang/reflect/Field;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 467
    return-void
.end method

.method public dumpPapx(ZZ)V
    .locals 24
    .param p1, "withProperties"    # Z
    .param p2, "withSprms"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    instance-of v0, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 533
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v6, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 535
    .local v6, "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    const-class v19, Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .line 536
    const-string/jumbo v20, "_mainStream"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 537
    .local v7, "fMainStream":Ljava/lang/reflect/Field;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    .line 540
    .local v9, "mainStream":[B
    new-instance v5, Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocument;->getTableStream()[B

    move-result-object v19

    .line 541
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocument;->getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFcPlcfbtePapx()I

    move-result v20

    .line 542
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocument;->getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getLcbPlcfbtePapx()I

    move-result v21

    const/16 v22, 0x4

    .line 540
    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v5, v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 544
    .local v5, "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 546
    .local v15, "papxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v8

    .line 547
    .local v8, "length":I
    const/16 v18, 0x0

    .local v18, "x":I
    :goto_0
    move/from16 v0, v18

    if-lt v0, v8, :cond_2

    .line 575
    invoke-static {v15}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 577
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_5

    .line 588
    .end local v5    # "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    .end local v6    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    .end local v7    # "fMainStream":Ljava/lang/reflect/Field;
    .end local v8    # "length":I
    .end local v9    # "mainStream":[B
    .end local v15    # "papxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .end local v18    # "x":I
    :cond_1
    const-class v19, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    .line 589
    const-string/jumbo v20, "newParagraph"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-class v23, Lorg/apache/poi/hwpf/usermodel/Range;

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-class v23, Lorg/apache/poi/hwpf/model/PAPX;

    aput-object v23, v21, v22

    .line 588
    invoke-virtual/range {v19 .. v21}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 590
    .local v10, "newParagraph":Ljava/lang/reflect/Method;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 591
    const-class v19, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    .line 592
    const-string/jumbo v20, "_props"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 593
    .local v4, "_props":Ljava/lang/reflect/Field;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/model/PAPBinTable;->getParagraphs()Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_6

    .line 612
    return-void

    .line 549
    .end local v4    # "_props":Ljava/lang/reflect/Field;
    .end local v10    # "newParagraph":Ljava/lang/reflect/Method;
    .restart local v5    # "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    .restart local v6    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    .restart local v7    # "fMainStream":Ljava/lang/reflect/Field;
    .restart local v8    # "length":I
    .restart local v9    # "mainStream":[B
    .restart local v15    # "papxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .restart local v18    # "x":I
    :cond_2
    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v11

    .line 551
    .local v11, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v12

    .line 552
    .local v12, "pageNum":I
    mul-int/lit16 v13, v12, 0x200

    .line 555
    .local v13, "pageOffset":I
    new-instance v16, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;

    .line 556
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocument;->getDataStream()[B

    move-result-object v19

    .line 557
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocument;->getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v20

    .line 555
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v9, v1, v13, v2}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;-><init>([B[BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 561
    .local v16, "pfkp":Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getPAPXs()Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_3
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_4

    .line 547
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0

    .line 561
    :cond_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/poi/hwpf/model/PAPX;

    .line 564
    .local v14, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-interface {v15, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    if-eqz v14, :cond_3

    if-eqz p2, :cond_3

    .line 567
    new-instance v17, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    .line 568
    invoke-virtual {v14}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v20

    const/16 v21, 0x2

    .line 567
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 569
    .local v17, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    const-string/jumbo v20, "*** "

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    goto :goto_3

    .line 577
    .end local v11    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    .end local v12    # "pageNum":I
    .end local v13    # "pageOffset":I
    .end local v14    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    .end local v16    # "pfkp":Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
    .end local v17    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/poi/hwpf/model/PAPX;

    .line 580
    .restart local v14    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    if-eqz v14, :cond_0

    if-eqz p2, :cond_0

    .line 582
    new-instance v17, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v14}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v20

    const/16 v21, 0x2

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 583
    .restart local v17    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    const-string/jumbo v20, "*** "

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 595
    .end local v5    # "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    .end local v6    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    .end local v7    # "fMainStream":Ljava/lang/reflect/Field;
    .end local v8    # "length":I
    .end local v9    # "mainStream":[B
    .end local v14    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    .end local v15    # "papxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    .end local v17    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .end local v18    # "x":I
    .restart local v4    # "_props":Ljava/lang/reflect/Field;
    .restart local v10    # "newParagraph":Ljava/lang/reflect/Method;
    :cond_6
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/poi/hwpf/model/PAPX;

    .line 599
    .restart local v14    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    if-eqz p1, :cond_7

    .line 601
    const/16 v19, 0x0

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    .line 602
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object v14, v21, v22

    .line 601
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    .line 608
    :cond_7
    new-instance v17, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v14}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v19

    const/16 v21, 0x2

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 609
    .restart local v17    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    const-string/jumbo v19, "\t"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method protected dumpParagraphLevels(Lorg/apache/poi/hwpf/model/ListTables;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)V
    .locals 5
    .param p1, "listTables"    # Lorg/apache/poi/hwpf/model/ListTables;
    .param p2, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .prologue
    const/4 v4, 0x0

    .line 720
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v2

    if-eqz v2, :cond_1

    .line 722
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v2

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/ListTables;->getLfo(I)Lorg/apache/poi/hwpf/model/LFO;

    move-result-object v0

    .line 728
    .local v0, "lfo":Lorg/apache/poi/hwpf/model/LFO;
    if-eqz v0, :cond_1

    .line 730
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFO;->getLsid()I

    move-result v2

    .line 731
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlvl()B

    move-result v3

    .line 730
    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/hwpf/model/ListTables;->getLevel(II)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v1

    .line 735
    .local v1, "listLevel":Lorg/apache/poi/hwpf/model/ListLevel;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListLevel;->getGrpprlPapx()[B

    move-result-object v2

    if-eqz v2, :cond_0

    .line 739
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListLevel;->getGrpprlPapx()[B

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 740
    const-string/jumbo v3, "* "

    .line 738
    invoke-virtual {p0, v2, v3}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    .line 743
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListLevel;->getGrpprlPapx()[B

    move-result-object v2

    if-eqz v2, :cond_1

    .line 747
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListLevel;->getGrpprlChpx()[B

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 748
    const-string/jumbo v3, "* "

    .line 746
    invoke-virtual {p0, v2, v3}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    .line 752
    .end local v0    # "lfo":Lorg/apache/poi/hwpf/model/LFO;
    .end local v1    # "listLevel":Lorg/apache/poi/hwpf/model/ListLevel;
    :cond_1
    return-void
.end method

.method public dumpParagraphs(Z)V
    .locals 9
    .param p1, "dumpAssotiatedPapx"    # Z

    .prologue
    .line 616
    iget-object v5, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->paragraphs:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 646
    return-void

    .line 616
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 618
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 622
    .local v0, "endOfParagraphCharOffset":Ljava/lang/Integer;
    if-eqz p1, :cond_0

    .line 624
    const/4 v2, 0x0

    .line 625
    .local v2, "hasAssotiatedPapx":Z
    iget-object v6, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PAPBinTable;->getParagraphs()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/PAPX;

    .line 627
    .local v3, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-gt v7, v8, :cond_2

    .line 628
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 629
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 631
    const/4 v2, 0x1

    .line 634
    new-instance v4, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    .line 635
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v7

    const/4 v8, 0x2

    .line 634
    invoke-direct {v4, v7, v8}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 636
    .local v4, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    const-string/jumbo v7, "** "

    invoke-virtual {p0, v4, v7}, Lorg/apache/poi/hwpf/dev/HWPFLister;->dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dumpParagraphsDom(Z)V
    .locals 4
    .param p1, "withText"    # Z

    .prologue
    .line 659
    iget-object v3, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v2

    .line 660
    .local v2, "range":Lorg/apache/poi/hwpf/usermodel/Range;
    const/4 v0, 0x0

    .local v0, "p":I
    :goto_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 669
    return-void

    .line 662
    :cond_0
    invoke-virtual {v2, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v1

    .line 665
    .local v1, "paragraph":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    if-eqz p1, :cond_1

    .line 660
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected dumpSprms(Lorg/apache/poi/hwpf/sprm/SprmIterator;Ljava/lang/String;)V
    .locals 1
    .param p1, "sprmIt"    # Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .param p2, "linePrefix"    # Ljava/lang/String;

    .prologue
    .line 650
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 655
    return-void

    .line 652
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    goto :goto_0
.end method

.method public dumpTextPieces(Z)V
    .locals 2
    .param p1, "withText"    # Z

    .prologue
    .line 756
    iget-object v0, p0, Lorg/apache/poi/hwpf/dev/HWPFLister;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 765
    return-void

    .line 756
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/TextPiece;

    goto :goto_0
.end method
