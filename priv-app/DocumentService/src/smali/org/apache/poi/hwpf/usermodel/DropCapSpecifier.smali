.class public final Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
.super Ljava/lang/Object;
.source "DropCapSpecifier.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static _lines:Lorg/apache/poi/util/BitField;

.field private static _type:Lorg/apache/poi/util/BitField;


# instance fields
.field private _fdct:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0xf8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_lines:Lorg/apache/poi/util/BitField;

    .line 35
    const/4 v0, 0x7

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_type:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    .line 40
    return-void
.end method

.method public constructor <init>(S)V
    .locals 0
    .param p1, "fdct"    # S

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-short p1, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    .line 50
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 44
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;-><init>(S)V

    .line 45
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->clone()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;-><init>(S)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    if-ne p0, p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 64
    goto :goto_0

    .line 65
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 66
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 67
    check-cast v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    .line 68
    .local v0, "other":Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;
    iget-short v3, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 69
    goto :goto_0
.end method

.method public getCountOfLinesToDrop()B
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_lines:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getDropCapType()B
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_type:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 86
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 91
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCountOfLinesToDrop(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 96
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_lines:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    .line 97
    return-void
.end method

.method public setDropCapType(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 101
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_type:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    .line 102
    return-void
.end method

.method public toShort()S
    .locals 1

    .prologue
    .line 106
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->_fdct:S

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    const-string/jumbo v0, "[DCS] EMPTY"

    .line 115
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[DCS] (type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->getDropCapType()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 116
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->getCountOfLinesToDrop()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
