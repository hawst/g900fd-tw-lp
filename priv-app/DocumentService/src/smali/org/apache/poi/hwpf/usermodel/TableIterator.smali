.class public final Lorg/apache/poi/hwpf/usermodel/TableIterator;
.super Ljava/lang/Object;
.source "TableIterator.java"


# instance fields
.field _index:I

.field _levelNum:I

.field _range:Lorg/apache/poi/hwpf/usermodel/Range;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 1
    .param p1, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hwpf/usermodel/TableIterator;-><init>(Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 37
    return-void
.end method

.method constructor <init>(Lorg/apache/poi/hwpf/usermodel/Range;I)V
    .locals 1
    .param p1, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p2, "levelNum"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    .line 31
    iput p2, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_levelNum:I

    .line 32
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 4

    .prologue
    .line 42
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v0

    .line 43
    .local v0, "numParagraphs":I
    :goto_0
    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    if-lt v2, v0, :cond_0

    .line 51
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 45
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v1

    .line 46
    .local v1, "paragraph":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v2

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_levelNum:I

    if-ne v2, v3, :cond_1

    .line 48
    const/4 v2, 0x1

    goto :goto_1

    .line 43
    :cond_1
    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    goto :goto_0
.end method

.method public next()Lorg/apache/poi/hwpf/usermodel/Table;
    .locals 9

    .prologue
    .line 56
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v1

    .line 57
    .local v1, "numParagraphs":I
    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    .line 58
    .local v3, "startIndex":I
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    .line 60
    .local v0, "endIndex":I
    :goto_0
    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    if-lt v4, v1, :cond_0

    .line 69
    :goto_1
    new-instance v4, Lorg/apache/poi/hwpf/usermodel/Table;

    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v5, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getStartOffset()I

    move-result v5

    .line 70
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v7}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getEndOffset()I

    move-result v6

    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 71
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_levelNum:I

    .line 69
    invoke-direct {v4, v5, v6, v7, v8}, Lorg/apache/poi/hwpf/usermodel/Table;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;I)V

    return-object v4

    .line 62
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_range:Lorg/apache/poi/hwpf/usermodel/Range;

    iget v5, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v2

    .line 63
    .local v2, "paragraph":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v4

    iget v5, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_levelNum:I

    if-ge v4, v5, :cond_2

    .line 65
    :cond_1
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    .line 66
    goto :goto_1

    .line 60
    :cond_2
    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/poi/hwpf/usermodel/TableIterator;->_index:I

    goto :goto_0
.end method
