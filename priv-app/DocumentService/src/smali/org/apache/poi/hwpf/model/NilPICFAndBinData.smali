.class public Lorg/apache/poi/hwpf/model/NilPICFAndBinData;
.super Ljava/lang/Object;
.source "NilPICFAndBinData.java"


# static fields
.field private static final log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _binData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 27
    sput-object v0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->log:Lorg/apache/poi/util/POILogger;

    .line 28
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->fillFields([BI)V

    .line 35
    return-void
.end method


# virtual methods
.method public fillFields([BI)V
    .locals 9
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 39
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    .line 41
    .local v2, "lcb":I
    add-int/lit8 v3, p2, 0x4

    .line 40
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    .line 43
    .local v1, "cbHeader":I
    const/16 v3, 0x44

    if-eq v1, v3, :cond_0

    .line 45
    sget-object v3, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->log:Lorg/apache/poi/util/POILogger;

    const/4 v4, 0x5

    const-string/jumbo v5, "NilPICFAndBinData at offset "

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 46
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, " cbHeader 0x"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 47
    const-string/jumbo v8, " != 0x44"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 46
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 45
    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 51
    :cond_0
    sub-int v0, v2, v1

    .line 52
    .local v0, "binaryLength":I
    add-int v3, p2, v1

    .line 53
    add-int v4, p2, v1

    add-int/2addr v4, v0

    .line 52
    invoke-static {p1, v3, v4}, Lorg/apache/poi/util/ArrayUtil;->copyOfRange([BII)[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    .line 54
    return-void
.end method

.method public getBinData()[B
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    return-object v0
.end method

.method public serialize([BI)I
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x44

    invoke-static {p1, p2, v0}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 72
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    const/4 v1, 0x0

    add-int/lit8 v2, p2, 0x44

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    array-length v3, v3

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x44

    return v0
.end method

.method public serialize()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x44

    new-array v0, v1, [B

    .line 64
    .local v0, "bs":[B
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x44

    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 65
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    const/16 v2, 0x44

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/NilPICFAndBinData;->_binData:[B

    array-length v3, v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 66
    return-object v0
.end method
