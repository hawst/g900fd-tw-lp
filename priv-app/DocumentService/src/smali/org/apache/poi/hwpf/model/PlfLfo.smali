.class public Lorg/apache/poi/hwpf/model/PlfLfo;
.super Ljava/lang/Object;
.source "PlfLfo.java"


# static fields
.field private static log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _lfoMac:I

.field private _rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

.field private _rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/poi/hwpf/model/PlfLfo;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/PlfLfo;->log:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method constructor <init>([BII)V
    .locals 10
    .param p1, "tableStream"    # [B
    .param p2, "fcPlfLfo"    # I
    .param p3, "lcbPlfLfo"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    move v4, p2

    .line 68
    .local v4, "offset":I
    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v2

    .line 69
    .local v2, "lfoMacLong":J
    add-int/lit8 v4, v4, 0x4

    .line 71
    const-wide/32 v6, 0x7fffffff

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 73
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    .line 74
    const-string/jumbo v7, "Apache POI doesn\'t support rgLfo/rgLfoData size large than 2147483647 elements"

    .line 73
    invoke-direct {v6, v7}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 78
    :cond_0
    long-to-int v6, v2

    iput v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    .line 79
    iget v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    new-array v6, v6, [Lorg/apache/poi/hwpf/model/LFO;

    iput-object v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    .line 80
    iget v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    new-array v6, v6, [Lorg/apache/poi/hwpf/model/LFOData;

    iput-object v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    .line 87
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    iget v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-lt v5, v6, :cond_2

    .line 99
    const/4 v5, 0x0

    :goto_1
    iget v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-lt v5, v6, :cond_3

    .line 107
    sub-int v6, v4, p2

    if-eq v6, p3, :cond_1

    .line 109
    sget-object v6, Lorg/apache/poi/hwpf/model/PlfLfo;->log:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Actual size of PlfLfo is "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 110
    sub-int v9, v4, p2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " bytes, but expected "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 111
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 109
    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 113
    :cond_1
    return-void

    .line 89
    :cond_2
    new-instance v0, Lorg/apache/poi/hwpf/model/LFO;

    invoke-direct {v0, p1, v4}, Lorg/apache/poi/hwpf/model/LFO;-><init>([BI)V

    .line 90
    .local v0, "lfo":Lorg/apache/poi/hwpf/model/LFO;
    invoke-static {}, Lorg/apache/poi/hwpf/model/LFO;->getSize()I

    move-result v6

    add-int/2addr v4, v6

    .line 91
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    aput-object v0, v6, v5

    .line 87
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "lfo":Lorg/apache/poi/hwpf/model/LFO;
    :cond_3
    new-instance v1, Lorg/apache/poi/hwpf/model/LFOData;

    .line 102
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/LFO;->getClfolvl()B

    move-result v6

    .line 101
    invoke-direct {v1, p1, v4, v6}, Lorg/apache/poi/hwpf/model/LFOData;-><init>([BII)V

    .line 103
    .local v1, "lfoData":Lorg/apache/poi/hwpf/model/LFOData;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LFOData;->getSizeInBytes()I

    move-result v6

    add-int/2addr v4, v6

    .line 104
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    aput-object v1, v6, v5

    .line 99
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method


# virtual methods
.method add(Lorg/apache/poi/hwpf/model/LFO;Lorg/apache/poi/hwpf/model/LFOData;)V
    .locals 3
    .param p1, "lfo"    # Lorg/apache/poi/hwpf/model/LFO;
    .param p2, "lfoData"    # Lorg/apache/poi/hwpf/model/LFOData;

    .prologue
    .line 117
    iget v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    add-int/lit8 v0, v1, 0x1

    .line 119
    .local v0, "newLfoMac":I
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    new-array v2, v0, [Lorg/apache/poi/hwpf/model/LFO;

    invoke-static {v1, v2}, Lorg/apache/poi/util/ArrayUtil;->copyOf([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/poi/hwpf/model/LFO;

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    .line 120
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    iget v2, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    add-int/lit8 v2, v2, 0x1

    aput-object p1, v1, v2

    .line 122
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    iget v2, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Lorg/apache/poi/hwpf/model/LFOData;

    invoke-static {v1, v2}, Lorg/apache/poi/util/ArrayUtil;->copyOf([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/poi/hwpf/model/LFOData;

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    .line 123
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    iget v2, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    add-int/lit8 v2, v2, 0x1

    aput-object p2, v1, v2

    .line 125
    iput v0, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    .line 126
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 131
    if-ne p0, p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v1

    .line 133
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 134
    goto :goto_0

    .line 135
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 136
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 137
    check-cast v0, Lorg/apache/poi/hwpf/model/PlfLfo;

    .line 138
    .local v0, "other":Lorg/apache/poi/hwpf/model/PlfLfo;
    iget v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 139
    goto :goto_0

    .line 140
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 141
    goto :goto_0

    .line 142
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 143
    goto :goto_0
.end method

.method public getIlfoByLsid(I)I
    .locals 4
    .param p1, "lsid"    # I

    .prologue
    .line 158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-lt v0, v1, :cond_0

    .line 165
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "LFO with lsid "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 166
    const-string/jumbo v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165
    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LFO;->getLsid()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 162
    add-int/lit8 v1, v0, 0x1

    return v1

    .line 158
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getLfo(I)Lorg/apache/poi/hwpf/model/LFO;
    .locals 3
    .param p1, "ilfo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 171
    if-lez p1, :cond_0

    iget v0, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-le p1, v0, :cond_1

    .line 173
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "LFO with ilfo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 174
    const-string/jumbo v2, " not found. lfoMac is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getLfoData(I)Lorg/apache/poi/hwpf/model/LFOData;
    .locals 3
    .param p1, "ilfo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 181
    if-lez p1, :cond_0

    iget v0, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-le p1, v0, :cond_1

    .line 183
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "LFOData with ilfo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 184
    const-string/jumbo v2, " not found. lfoMac is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getLfoMac()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 192
    const/16 v0, 0x1f

    .line 193
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 194
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    add-int/lit8 v1, v2, 0x1f

    .line 195
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 196
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 197
    return v1
.end method

.method writeTo(Lorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 6
    .param p1, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p2, "outputStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v2

    .line 204
    .local v2, "offset":I
    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setFcPlfLfo(I)V

    .line 206
    iget v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    int-to-long v4, v3

    invoke-static {v4, v5, p2}, Lorg/apache/poi/util/LittleEndian;->putUInt(JLjava/io/OutputStream;)V

    .line 208
    invoke-static {}, Lorg/apache/poi/hwpf/model/LFO;->getSize()I

    move-result v3

    iget v4, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    mul-int/2addr v3, v4

    new-array v0, v3, [B

    .line 209
    .local v0, "bs":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-lt v1, v3, :cond_0

    .line 213
    const/4 v3, 0x0

    invoke-static {}, Lorg/apache/poi/hwpf/model/LFO;->getSize()I

    move-result v4

    iget v5, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    mul-int/2addr v4, v5

    invoke-virtual {p2, v0, v3, v4}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([BII)V

    .line 215
    const/4 v1, 0x0

    :goto_1
    iget v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_lfoMac:I

    if-lt v1, v3, :cond_1

    .line 219
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setLcbPlfLfo(I)V

    .line 220
    return-void

    .line 211
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfo:[Lorg/apache/poi/hwpf/model/LFO;

    aget-object v3, v3, v1

    invoke-static {}, Lorg/apache/poi/hwpf/model/LFO;->getSize()I

    move-result v4

    mul-int/2addr v4, v1

    invoke-virtual {v3, v0, v4}, Lorg/apache/poi/hwpf/model/LFO;->serialize([BI)V

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlfLfo;->_rgLfoData:[Lorg/apache/poi/hwpf/model/LFOData;

    aget-object v3, v3, v1

    invoke-virtual {v3, p2}, Lorg/apache/poi/hwpf/model/LFOData;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
