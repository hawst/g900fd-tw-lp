.class public final Lorg/apache/poi/hwpf/model/FSPA;
.super Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;
.source "FSPA.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final FSPA_SIZE:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lorg/apache/poi/hwpf/model/FSPA;->getSize()I

    move-result v0

    sput v0, Lorg/apache/poi/hwpf/model/FSPA;->FSPA_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;-><init>()V

    .line 40
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FSPA;->fillFields([BI)V

    .line 41
    return-void
.end method


# virtual methods
.method public toByteArray()[B
    .locals 2

    .prologue
    .line 45
    sget v1, Lorg/apache/poi/hwpf/model/FSPA;->FSPA_SIZE:I

    new-array v0, v1, [B

    .line 46
    .local v0, "buf":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/FSPA;->serialize([BI)V

    .line 47
    return-object v0
.end method
