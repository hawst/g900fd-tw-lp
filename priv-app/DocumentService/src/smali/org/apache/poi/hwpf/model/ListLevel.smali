.class public final Lorg/apache/poi/hwpf/model/ListLevel;
.super Ljava/lang/Object;
.source "ListLevel.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _grpprlChpx:[B

.field private _grpprlPapx:[B

.field private _lvlf:Lorg/apache/poi/hwpf/model/LVLF;

.field private _xst:Lorg/apache/poi/hwpf/model/Xst;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 40
    sput-object v0, Lorg/apache/poi/hwpf/model/ListLevel;->logger:Lorg/apache/poi/util/POILogger;

    .line 41
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Xst;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 66
    return-void
.end method

.method public constructor <init>(III[B[BLjava/lang/String;)V
    .locals 2
    .param p1, "startAt"    # I
    .param p2, "numberFormatCode"    # I
    .param p3, "alignment"    # I
    .param p4, "numberProperties"    # [B
    .param p5, "entryProperties"    # [B
    .param p6, "numberText"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Xst;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 95
    new-instance v0, Lorg/apache/poi/hwpf/model/LVLF;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/LVLF;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    .line 96
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/ListLevel;->setStartAt(I)V

    .line 97
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    int-to-byte v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/LVLF;->setNfc(B)V

    .line 98
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    int-to-byte v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/LVLF;->setJc(B)V

    .line 99
    iput-object p4, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    .line 100
    iput-object p5, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    .line 101
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0, p6}, Lorg/apache/poi/hwpf/model/Xst;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 102
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 3
    .param p1, "level"    # I
    .param p2, "numbered"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Xst;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 76
    new-instance v0, Lorg/apache/poi/hwpf/model/LVLF;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/LVLF;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    .line 77
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/model/ListLevel;->setStartAt(I)V

    .line 78
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    .line 79
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    .line 81
    if-eqz p2, :cond_0

    .line 83
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getRgbxchNums()[B

    move-result-object v0

    aput-byte v2, v0, v1

    .line 84
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    int-to-char v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Xst;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    const-string/jumbo v1, "\u2022"

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Xst;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    goto :goto_0
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "startOffset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Xst;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 71
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/ListLevel;->read([BI)I

    .line 72
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 107
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/model/ListLevel;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 108
    check-cast v0, Lorg/apache/poi/hwpf/model/ListLevel;

    .line 109
    .local v0, "lvl":Lorg/apache/poi/hwpf/model/ListLevel;
    iget-object v2, v0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/LVLF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, v0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    iget-object v2, v0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    iget-object v2, v0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/Xst;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    const/4 v1, 0x1

    .line 114
    .end local v0    # "lvl":Lorg/apache/poi/hwpf/model/ListLevel;
    :cond_0
    return v1
.end method

.method public getAlignment()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getJc()B

    move-result v0

    return v0
.end method

.method public getGrpprlChpx()[B
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    return-object v0
.end method

.method public getGrpprlPapx()[B
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    return-object v0
.end method

.method public getLevelProperties()[B
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    return-object v0
.end method

.method public getNumberFormat()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getNfc()B

    move-result v0

    return v0
.end method

.method public getNumberText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Xst;->getAsJavaString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSizeInBytes()I
    .locals 2

    .prologue
    .line 155
    invoke-static {}, Lorg/apache/poi/hwpf/model/LVLF;->getSize()I

    move-result v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlChpx()S

    move-result v1

    add-int/2addr v0, v1

    .line 156
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlPapx()S

    move-result v1

    .line 155
    add-int/2addr v0, v1

    .line 156
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xst;->getSize()I

    move-result v1

    .line 155
    add-int/2addr v0, v1

    return v0
.end method

.method public getStartAt()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getIStartAt()I

    move-result v0

    return v0
.end method

.method public getTypeOfCharFollowingTheNumber()B
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getIxchFollow()B

    move-result v0

    return v0
.end method

.method read([BI)I
    .locals 8
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    const/4 v2, 0x0

    .line 174
    move v7, p2

    .line 176
    .local v7, "offset":I
    new-instance v0, Lorg/apache/poi/hwpf/model/LVLF;

    invoke-direct {v0, p1, v7}, Lorg/apache/poi/hwpf/model/LVLF;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    .line 177
    invoke-static {}, Lorg/apache/poi/hwpf/model/LVLF;->getSize()I

    move-result v0

    add-int/2addr v7, v0

    .line 179
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlPapx()S

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    .line 180
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlPapx()S

    move-result v1

    invoke-static {p1, v7, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 181
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlPapx()S

    move-result v0

    add-int/2addr v7, v0

    .line 183
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlChpx()S

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    .line 184
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlChpx()S

    move-result v1

    invoke-static {p1, v7, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 185
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getCbGrpprlChpx()S

    move-result v0

    add-int/2addr v7, v0

    .line 187
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0, p1, v7}, Lorg/apache/poi/hwpf/model/Xst;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 188
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Xst;->getSize()I

    move-result v0

    add-int/2addr v7, v0

    .line 196
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LVLF;->getNfc()B

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_0

    .line 198
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Xst;->getCch()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 200
    sget-object v0, Lorg/apache/poi/hwpf/model/ListLevel;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    const-string/jumbo v2, "LVL at offset "

    .line 201
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 202
    const-string/jumbo v4, " has nfc == 0x17 (bullets), but cch != 1 ("

    .line 203
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/Xst;->getCch()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string/jumbo v6, ")"

    .line 200
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 207
    :cond_0
    sub-int v0, v7, p2

    return v0
.end method

.method public setAlignment(I)V
    .locals 2
    .param p1, "alignment"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/LVLF;->setJc(B)V

    .line 213
    return-void
.end method

.method public setLevelProperties([B)V
    .locals 0
    .param p1, "grpprl"    # [B

    .prologue
    .line 217
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    .line 218
    return-void
.end method

.method public setNumberFormat(I)V
    .locals 2
    .param p1, "numberFormatCode"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/LVLF;->setNfc(B)V

    .line 223
    return-void
.end method

.method public setNumberProperties([B)V
    .locals 0
    .param p1, "grpprl"    # [B

    .prologue
    .line 227
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    .line 228
    return-void
.end method

.method public setStartAt(I)V
    .locals 1
    .param p1, "startAt"    # I

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/LVLF;->setIStartAt(I)V

    .line 233
    return-void
.end method

.method public setTypeOfCharFollowingTheNumber(B)V
    .locals 1
    .param p1, "value"    # B

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/model/LVLF;->setIxchFollow(B)V

    .line 238
    return-void
.end method

.method public toByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 242
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/ListLevel;->getSizeInBytes()I

    move-result v2

    new-array v0, v2, [B

    .line 243
    .local v0, "buf":[B
    const/4 v1, 0x0

    .line 245
    .local v1, "offset":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    array-length v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/LVLF;->setCbGrpprlChpx(S)V

    .line 246
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    array-length v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/LVLF;->setCbGrpprlPapx(S)V

    .line 247
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/LVLF;->serialize([BI)V

    .line 248
    invoke-static {}, Lorg/apache/poi/hwpf/model/LVLF;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 250
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    array-length v3, v3

    invoke-static {v2, v4, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    array-length v2, v2

    add-int/2addr v1, v2

    .line 252
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    array-length v3, v3

    invoke-static {v2, v4, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    array-length v2, v2

    add-int/2addr v1, v2

    .line 255
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hwpf/model/Xst;->serialize([BI)V

    .line 256
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Xst;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 258
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "LVL: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_lvlf:Lorg/apache/poi/hwpf/model/LVLF;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\n"

    const-string/jumbo v3, "\n    "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 265
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 266
    const-string/jumbo v1, "PAPX\'s grpprl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlPapx:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 267
    const-string/jumbo v1, "CHPX\'s grpprl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_grpprlChpx:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 268
    const-string/jumbo v1, "xst: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListLevel;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 264
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
