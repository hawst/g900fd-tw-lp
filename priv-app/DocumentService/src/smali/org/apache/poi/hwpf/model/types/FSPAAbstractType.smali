.class public abstract Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;
.super Ljava/lang/Object;
.source "FSPAAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static bx:Lorg/apache/poi/util/BitField;

.field private static by:Lorg/apache/poi/util/BitField;

.field private static fAnchorLock:Lorg/apache/poi/util/BitField;

.field private static fBelowText:Lorg/apache/poi/util/BitField;

.field private static fHdr:Lorg/apache/poi/util/BitField;

.field private static fRcaSimple:Lorg/apache/poi/util/BitField;

.field private static wr:Lorg/apache/poi/util/BitField;

.field private static wrk:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_spid:I

.field protected field_2_xaLeft:I

.field protected field_3_yaTop:I

.field protected field_4_xaRight:I

.field protected field_5_yaBottom:I

.field protected field_6_flags:S

.field protected field_7_cTxbx:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fHdr:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->bx:Lorg/apache/poi/util/BitField;

    .line 52
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->by:Lorg/apache/poi/util/BitField;

    .line 53
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1e0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->wr:Lorg/apache/poi/util/BitField;

    .line 54
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1e00

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->wrk:Lorg/apache/poi/util/BitField;

    .line 55
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fRcaSimple:Lorg/apache/poi/util/BitField;

    .line 56
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x4000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fBelowText:Lorg/apache/poi/util/BitField;

    .line 57
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0x8000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fAnchorLock:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 91
    const/16 v0, 0x1a

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 66
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_1_spid:I

    .line 67
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_2_xaLeft:I

    .line 68
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_3_yaTop:I

    .line 69
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_4_xaRight:I

    .line 70
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_5_yaBottom:I

    .line 71
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 72
    add-int/lit8 v0, p2, 0x16

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_7_cTxbx:I

    .line 73
    return-void
.end method

.method public getBx()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 288
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->bx:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getBy()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 308
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->by:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getCTxbx()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 239
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_7_cTxbx:I

    return v0
.end method

.method public getFlags()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 221
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    return v0
.end method

.method public getSpid()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 131
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_1_spid:I

    return v0
.end method

.method public getWr()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 328
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->wr:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getWrk()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 348
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->wrk:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getXaLeft()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_2_xaLeft:I

    return v0
.end method

.method public getXaRight()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 185
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_4_xaRight:I

    return v0
.end method

.method public getYaBottom()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 203
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_5_yaBottom:I

    return v0
.end method

.method public getYaTop()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 167
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_3_yaTop:I

    return v0
.end method

.method public isFAnchorLock()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 408
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fAnchorLock:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBelowText()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 388
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fBelowText:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHdr()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 268
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fHdr:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRcaSimple()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 368
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fRcaSimple:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 77
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_1_spid:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 78
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_2_xaLeft:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 79
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_3_yaTop:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 80
    add-int/lit8 v0, p2, 0xc

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_4_xaRight:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 81
    add-int/lit8 v0, p2, 0x10

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_5_yaBottom:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 82
    add-int/lit8 v0, p2, 0x14

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    add-int/lit8 v0, p2, 0x16

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_7_cTxbx:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 84
    return-void
.end method

.method public setBx(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 278
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->bx:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 279
    return-void
.end method

.method public setBy(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 298
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->by:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 299
    return-void
.end method

.method public setCTxbx(I)V
    .locals 0
    .param p1, "field_7_cTxbx"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 248
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_7_cTxbx:I

    .line 249
    return-void
.end method

.method public setFAnchorLock(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 398
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fAnchorLock:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 399
    return-void
.end method

.method public setFBelowText(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 378
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fBelowText:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 379
    return-void
.end method

.method public setFHdr(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 258
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fHdr:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 259
    return-void
.end method

.method public setFRcaSimple(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 358
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->fRcaSimple:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 359
    return-void
.end method

.method public setFlags(S)V
    .locals 0
    .param p1, "field_6_flags"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 230
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 231
    return-void
.end method

.method public setSpid(I)V
    .locals 0
    .param p1, "field_1_spid"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 140
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_1_spid:I

    .line 141
    return-void
.end method

.method public setWr(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 318
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->wr:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 319
    return-void
.end method

.method public setWrk(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 338
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->wrk:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_6_flags:S

    .line 339
    return-void
.end method

.method public setXaLeft(I)V
    .locals 0
    .param p1, "field_2_xaLeft"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 158
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_2_xaLeft:I

    .line 159
    return-void
.end method

.method public setXaRight(I)V
    .locals 0
    .param p1, "field_4_xaRight"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 194
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_4_xaRight:I

    .line 195
    return-void
.end method

.method public setYaBottom(I)V
    .locals 0
    .param p1, "field_5_yaBottom"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 212
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_5_yaBottom:I

    .line 213
    return-void
.end method

.method public setYaTop(I)V
    .locals 0
    .param p1, "field_3_yaTop"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 176
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->field_3_yaTop:I

    .line 177
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[FSPA]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string/jumbo v1, "    .spid                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getSpid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    const-string/jumbo v1, "    .xaLeft               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getXaLeft()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    const-string/jumbo v1, "    .yaTop                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getYaTop()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string/jumbo v1, "    .xaRight              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getXaRight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string/jumbo v1, "    .yaBottom             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getYaBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string/jumbo v1, "    .flags                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getFlags()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string/jumbo v1, "         .fHdr                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->isFHdr()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v1, "         .bx                       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getBx()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v1, "         .by                       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getBy()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v1, "         .wr                       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getWr()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v1, "         .wrk                      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getWrk()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v1, "         .fRcaSimple               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->isFRcaSimple()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 116
    const-string/jumbo v1, "         .fBelowText               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->isFBelowText()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v1, "         .fAnchorLock              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->isFAnchorLock()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v1, "    .cTxbx                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FSPAAbstractType;->getCTxbx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string/jumbo v1, "[/FSPA]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
