.class public final Lorg/apache/poi/hwpf/model/PAPX;
.super Lorg/apache/poi/hwpf/model/BytePropertyNode;
.source "PAPX.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/poi/hwpf/model/BytePropertyNode",
        "<",
        "Lorg/apache/poi/hwpf/model/PAPX;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;


# direct methods
.method public constructor <init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Lorg/apache/poi/hwpf/sprm/SprmBuffer;[B)V
    .locals 1
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .param p4, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .param p5, "dataStream"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 66
    new-instance v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/ParagraphHeight;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    .line 67
    invoke-direct {p0, p4, p5}, Lorg/apache/poi/hwpf/model/PAPX;->findHuge(Lorg/apache/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object p4

    .line 68
    if-eqz p4, :cond_0

    .line 69
    iput-object p4, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    .line 70
    :cond_0
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;[BLorg/apache/poi/hwpf/model/ParagraphHeight;[B)V
    .locals 3
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .param p4, "papx"    # [B
    .param p5, "phe"    # Lorg/apache/poi/hwpf/model/ParagraphHeight;
    .param p6, "dataStream"    # [B

    .prologue
    const/4 v2, 0x2

    .line 45
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p4, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, p2, p3, v1}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 46
    iput-object p5, p0, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    .line 47
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p4, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, v1, p6}, Lorg/apache/poi/hwpf/model/PAPX;->findHuge(Lorg/apache/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    .line 48
    .local v0, "buf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    if-eqz v0, :cond_0

    .line 49
    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    .line 50
    :cond_0
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V
    .locals 1
    .param p1, "charStart"    # I
    .param p2, "charEnd"    # I
    .param p3, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILjava/lang/Object;)V

    .line 75
    new-instance v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/ParagraphHeight;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    .line 76
    return-void
.end method

.method public constructor <init>(II[BLorg/apache/poi/hwpf/model/ParagraphHeight;[B)V
    .locals 3
    .param p1, "charStart"    # I
    .param p2, "charEnd"    # I
    .param p3, "papx"    # [B
    .param p4, "phe"    # Lorg/apache/poi/hwpf/model/ParagraphHeight;
    .param p5, "dataStream"    # [B

    .prologue
    const/4 v2, 0x2

    .line 55
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p3, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/poi/hwpf/model/BytePropertyNode;-><init>(IILjava/lang/Object;)V

    .line 56
    iput-object p4, p0, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    .line 57
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p3, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, v1, p5}, Lorg/apache/poi/hwpf/model/PAPX;->findHuge(Lorg/apache/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    .line 58
    .local v0, "buf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    if-eqz v0, :cond_0

    .line 59
    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    .line 60
    :cond_0
    return-void
.end method

.method private findHuge(Lorg/apache/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .locals 10
    .param p1, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .param p2, "datastream"    # [B

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 80
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    .line 81
    .local v0, "grpprl":[B
    array-length v5, v0

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    if-eqz p2, :cond_1

    .line 83
    new-instance v4, Lorg/apache/poi/hwpf/sprm/SprmOperation;

    invoke-direct {v4, v0, v7}, Lorg/apache/poi/hwpf/sprm/SprmOperation;-><init>([BI)V

    .line 84
    .local v4, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v5

    const/16 v6, 0x45

    if-eq v5, v6, :cond_0

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v5

    const/16 v6, 0x46

    if-ne v5, v6, :cond_1

    .line 85
    :cond_0
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    .line 87
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v3

    .line 88
    .local v3, "hugeGrpprlOffset":I
    add-int/lit8 v5, v3, 0x1

    array-length v6, p2

    if-ge v5, v6, :cond_1

    .line 90
    invoke-static {p2, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 91
    .local v1, "grpprlSize":I
    add-int v5, v3, v1

    array-length v6, p2

    if-ge v5, v6, :cond_1

    .line 93
    add-int/lit8 v5, v1, 0x2

    new-array v2, v5, [B

    .line 95
    .local v2, "hugeGrpprl":[B
    aget-byte v5, v0, v8

    aput-byte v5, v2, v8

    aget-byte v5, v0, v9

    aput-byte v5, v2, v9

    .line 97
    add-int/lit8 v5, v3, 0x2

    invoke-static {p2, v5, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    new-instance v5, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v5, v2, v7}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 104
    .end local v1    # "grpprlSize":I
    .end local v2    # "hugeGrpprl":[B
    .end local v3    # "hugeGrpprlOffset":I
    .end local v4    # "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    :goto_0
    return-object v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 161
    if-eqz p1, :cond_0

    instance-of v1, p1, Lorg/apache/poi/hwpf/model/PAPX;

    if-eqz v1, :cond_0

    .line 162
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/BytePropertyNode;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    check-cast p1, Lorg/apache/poi/hwpf/model/PAPX;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/ParagraphHeight;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 168
    :cond_0
    return v0
.end method

.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 118
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    goto :goto_0
.end method

.method public getIstd()S
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v1

    .line 126
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v0

    .line 127
    .local v0, "buf":[B
    array-length v2, v0

    if-eqz v2, :cond_0

    .line 131
    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 133
    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v1

    int-to-short v1, v1

    goto :goto_0

    .line 135
    :cond_2
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getShort([B)S

    move-result v1

    goto :goto_0
.end method

.method public getParagraphHeight()Lorg/apache/poi/hwpf/model/ParagraphHeight;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_phe:Lorg/apache/poi/hwpf/model/ParagraphHeight;

    return-object v0
.end method

.method public getParagraphProperties(Lorg/apache/poi/hwpf/model/StyleSheet;)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .locals 5
    .param p1, "ss"    # Lorg/apache/poi/hwpf/model/StyleSheet;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 147
    if-nez p1, :cond_0

    .line 149
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 155
    :goto_0
    return-object v2

    .line 152
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v1

    .line 153
    .local v1, "istd":S
    invoke-virtual {p1, v1}, Lorg/apache/poi/hwpf/model/StyleSheet;->getParagraphStyle(I)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v0

    .line 154
    .local v0, "baseStyle":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v2

    .line 155
    .local v2, "props":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    goto :goto_0
.end method

.method public getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "PAPX from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " (in bytes "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 174
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getStartBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PAPX;->getEndBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
