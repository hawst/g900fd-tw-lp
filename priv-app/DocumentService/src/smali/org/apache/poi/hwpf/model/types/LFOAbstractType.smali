.class public abstract Lorg/apache/poi/hwpf/model/types/LFOAbstractType;
.super Ljava/lang/Object;
.source "LFOAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected field_1_lsid:I

.field protected field_2_unused1:I

.field protected field_3_unused2:I

.field protected field_4_clfolvl:B

.field protected field_5_ibstFltAutoNum:B

.field protected field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

.field protected field_7_unused3:B


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Grfhic;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 53
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 89
    const/16 v0, 0x10

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    if-ne p0, p1, :cond_1

    .line 121
    :cond_0
    :goto_0
    return v1

    .line 97
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 98
    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 100
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 101
    check-cast v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;

    .line 102
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/LFOAbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 103
    goto :goto_0

    .line 104
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 105
    goto :goto_0

    .line 106
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 107
    goto :goto_0

    .line 108
    :cond_6
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 109
    goto :goto_0

    .line 110
    :cond_7
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 111
    goto :goto_0

    .line 112
    :cond_8
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-nez v3, :cond_9

    .line 114
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-eqz v3, :cond_a

    move v1, v2

    .line 115
    goto :goto_0

    .line 117
    :cond_9
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Grfhic;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 118
    goto :goto_0

    .line 119
    :cond_a
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 120
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 57
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    .line 58
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    .line 59
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    .line 60
    add-int/lit8 v0, p2, 0xc

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    .line 61
    add-int/lit8 v0, p2, 0xd

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    .line 62
    new-instance v0, Lorg/apache/poi/hwpf/model/Grfhic;

    add-int/lit8 v1, p2, 0xe

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Grfhic;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 63
    add-int/lit8 v0, p2, 0xf

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    .line 64
    return-void
.end method

.method public getClfolvl()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 224
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    return v0
.end method

.method public getGrfhic()Lorg/apache/poi/hwpf/model/Grfhic;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    return-object v0
.end method

.method public getIbstFltAutoNum()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 242
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    return v0
.end method

.method public getLsid()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 170
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    return v0
.end method

.method public getUnused1()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 188
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    return v0
.end method

.method public getUnused2()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 206
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    return v0
.end method

.method public getUnused3()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 278
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 127
    const/16 v0, 0x1f

    .line 128
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 129
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    add-int/lit8 v1, v2, 0x1f

    .line 130
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    add-int v1, v2, v3

    .line 131
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    add-int v1, v2, v3

    .line 132
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    add-int v1, v2, v3

    .line 133
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    add-int v1, v2, v3

    .line 134
    mul-int/lit8 v3, v1, 0x1f

    .line 135
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 134
    :goto_0
    add-int v1, v3, v2

    .line 136
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    add-int v1, v2, v3

    .line 137
    return v1

    .line 135
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Grfhic;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 68
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 69
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 70
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 71
    add-int/lit8 v0, p2, 0xc

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    aput-byte v1, p1, v0

    .line 72
    add-int/lit8 v0, p2, 0xd

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    aput-byte v1, p1, v0

    .line 73
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    add-int/lit8 v1, p2, 0xe

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Grfhic;->serialize([BI)V

    .line 74
    add-int/lit8 v0, p2, 0xf

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    aput-byte v1, p1, v0

    .line 75
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 80
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->serialize([BI)V

    .line 81
    return-object v0
.end method

.method public setClfolvl(B)V
    .locals 0
    .param p1, "field_4_clfolvl"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 233
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    .line 234
    return-void
.end method

.method public setGrfhic(Lorg/apache/poi/hwpf/model/Grfhic;)V
    .locals 0
    .param p1, "field_6_grfhic"    # Lorg/apache/poi/hwpf/model/Grfhic;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 269
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 270
    return-void
.end method

.method public setIbstFltAutoNum(B)V
    .locals 0
    .param p1, "field_5_ibstFltAutoNum"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 251
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    .line 252
    return-void
.end method

.method public setLsid(I)V
    .locals 0
    .param p1, "field_1_lsid"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 179
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    .line 180
    return-void
.end method

.method public setUnused1(I)V
    .locals 0
    .param p1, "field_2_unused1"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 197
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    .line 198
    return-void
.end method

.method public setUnused2(I)V
    .locals 0
    .param p1, "field_3_unused2"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 215
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    .line 216
    return-void
.end method

.method public setUnused3(B)V
    .locals 0
    .param p1, "field_7_unused3"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 287
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    .line 288
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[LFO]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, "    .lsid                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_1_lsid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v1, "    .unused1              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_2_unused1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v1, "    .unused2              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_3_unused2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string/jumbo v1, "    .clfolvl              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_4_clfolvl:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string/jumbo v1, "    .ibstFltAutoNum       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_5_ibstFltAutoNum:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v1, "    .grfhic               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-nez v1, :cond_0

    const-string/jumbo v1, "null"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v1, "    .unused3              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_7_unused3:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    const-string/jumbo v1, "[/LFO]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 156
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;->field_6_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Grfhic;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "\n"

    const-string/jumbo v4, "\n    "

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
