.class Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;
.super Ljava/lang/Object;
.source "AbstractWordConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/converter/AbstractWordConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeadFieldBoundaries"
.end annotation


# instance fields
.field final beginMark:I

.field final endMark:I

.field final separatorMark:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "beginMark"    # I
    .param p2, "separatorMark"    # I
    .param p3, "endMark"    # I

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;->beginMark:I

    .line 71
    iput p2, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;->separatorMark:I

    .line 72
    iput p3, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;->endMark:I

    .line 73
    return-void
.end method
