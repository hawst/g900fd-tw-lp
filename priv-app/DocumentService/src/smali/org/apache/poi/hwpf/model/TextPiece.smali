.class public Lorg/apache/poi/hwpf/model/TextPiece;
.super Lorg/apache/poi/hwpf/model/PropertyNode;
.source "TextPiece.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/poi/hwpf/model/PropertyNode",
        "<",
        "Lorg/apache/poi/hwpf/model/TextPiece;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _pd:Lorg/apache/poi/hwpf/model/PieceDescriptor;

.field private _usesUnicode:Z


# direct methods
.method public constructor <init>(II[BLorg/apache/poi/hwpf/model/PieceDescriptor;)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "text"    # [B
    .param p4, "pd"    # Lorg/apache/poi/hwpf/model/PieceDescriptor;

    .prologue
    .line 66
    invoke-static {p3, p4}, Lorg/apache/poi/hwpf/model/TextPiece;->buildInitSB([BLorg/apache/poi/hwpf/model/PieceDescriptor;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 67
    invoke-virtual {p4}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    .line 68
    iput-object p4, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/poi/hwpf/model/PieceDescriptor;

    .line 71
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 72
    .local v0, "textLength":I
    sub-int v1, p2, p1

    if-eq v1, v0, :cond_0

    .line 73
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Told we\'re for characters "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", but actually covers "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " characters!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :cond_0
    if-ge p2, p1, :cond_1

    .line 76
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Told we\'re of negative size! start="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " end="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :cond_1
    return-void
.end method

.method public constructor <init>(II[BLorg/apache/poi/hwpf/model/PieceDescriptor;I)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "text"    # [B
    .param p4, "pd"    # Lorg/apache/poi/hwpf/model/PieceDescriptor;
    .param p5, "cpStart"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/hwpf/model/TextPiece;-><init>(II[BLorg/apache/poi/hwpf/model/PieceDescriptor;)V

    .line 54
    return-void
.end method

.method private static buildInitSB([BLorg/apache/poi/hwpf/model/PieceDescriptor;)Ljava/lang/StringBuilder;
    .locals 4
    .param p0, "text"    # [B
    .param p1, "pd"    # Lorg/apache/poi/hwpf/model/PieceDescriptor;

    .prologue
    .line 86
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "UTF-16LE"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .local v1, "str":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    return-object v2

    .line 89
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "Cp1252"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v1    # "str":Ljava/lang/String;
    goto :goto_0

    .line 91
    .end local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Your Java is broken! It doesn\'t know about basic, required character encodings!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public adjustForDelete(II)V
    .locals 9
    .param p1, "start"    # I
    .param p2, "length"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 163
    move v5, p2

    .line 165
    .local v5, "numChars":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v4

    .line 166
    .local v4, "myStart":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    .line 167
    .local v3, "myEnd":I
    add-int v2, p1, v5

    .line 170
    .local v2, "end":I
    if-gt p1, v3, :cond_0

    if-lt v2, v4, :cond_0

    .line 173
    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 174
    .local v7, "overlapStart":I
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 176
    .local v6, "overlapEnd":I
    sub-int v1, v7, v4

    .line 177
    .local v1, "bufStart":I
    sub-int v0, v6, v4

    .line 178
    .local v0, "bufEnd":I
    iget-object v8, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v8, Ljava/lang/StringBuilder;

    invoke-virtual {v8, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 186
    .end local v0    # "bufEnd":I
    .end local v1    # "bufStart":I
    .end local v6    # "overlapEnd":I
    .end local v7    # "overlapStart":I
    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hwpf/model/PropertyNode;->adjustForDelete(II)V

    .line 187
    return-void
.end method

.method public bytesLength()I
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v1

    sub-int v1, v0, v1

    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    mul-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public characterLength()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 195
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 207
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/model/TextPiece;

    if-eqz v2, :cond_0

    .line 208
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/TextPiece;->limitsAreEqual(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 210
    check-cast v0, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 211
    .local v0, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    iget-boolean v2, v0, Lorg/apache/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/poi/hwpf/model/PieceDescriptor;

    iget-object v3, v0, Lorg/apache/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/poi/hwpf/model/PieceDescriptor;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    const/4 v1, 0x1

    .line 216
    .end local v0    # "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    :cond_0
    return v1
.end method

.method public getCP()I
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v0

    return v0
.end method

.method public getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/poi/hwpf/model/PieceDescriptor;

    return-object v0
.end method

.method public getRawBytes()[B
    .locals 3

    .prologue
    .line 124
    :try_start_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-boolean v1, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    if-eqz v1, :cond_0

    .line 125
    const-string/jumbo v1, "UTF-16LE"

    .line 124
    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    return-object v1

    .line 125
    :cond_0
    const-string/jumbo v1, "Cp1252"
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "ignore":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Your Java is broken! It doesn\'t know about basic, required character encodings!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getStringBuffer()Ljava/lang/StringBuffer;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public getStringBuilder()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v0, Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public isUnicode()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    return v0
.end method

.method public substring(II)Ljava/lang/String;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v0, Ljava/lang/StringBuilder;

    .line 143
    .local v0, "buf":Ljava/lang/StringBuilder;
    if-gez p1, :cond_0

    .line 144
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Can\'t request a substring before 0 - asked for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 146
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-le p2, v1, :cond_1

    .line 147
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " out of range 0 -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 149
    :cond_1
    if-ge p2, p1, :cond_2

    .line 150
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Asked for text from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", which has an end before the start!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 152
    :cond_2
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "TextPiece from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 231
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
