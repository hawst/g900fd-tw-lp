.class public Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;
.super Ljava/lang/Object;
.source "HtmlDocumentFacade.java"


# instance fields
.field protected final body:Lorg/w3c/dom/Element;

.field protected final document:Lorg/w3c/dom/Document;

.field protected final head:Lorg/w3c/dom/Element;

.field protected final html:Lorg/w3c/dom/Element;

.field private stylesheet:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private stylesheetElement:Lorg/w3c/dom/Element;

.field protected title:Lorg/w3c/dom/Element;

.field protected titleText:Lorg/w3c/dom/Text;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 3
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheet:Ljava/util/Map;

    .line 46
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    .line 48
    const-string/jumbo v0, "html"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->html:Lorg/w3c/dom/Element;

    .line 49
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->html:Lorg/w3c/dom/Element;

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 51
    const-string/jumbo v0, "body"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    .line 52
    const-string/jumbo v0, "head"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    .line 53
    const-string/jumbo v0, "style"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheetElement:Lorg/w3c/dom/Element;

    .line 54
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheetElement:Lorg/w3c/dom/Element;

    const-string/jumbo v1, "type"

    const-string/jumbo v2, "text/css"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->html:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 57
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->html:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 58
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheetElement:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 60
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    const-string/jumbo v1, "b"

    const-string/jumbo v2, "white-space-collapsing:preserve;"

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method


# virtual methods
.method public addAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 65
    const-string/jumbo v0, "author"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addMeta(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public addDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string/jumbo v0, "description"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addMeta(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public addKeywords(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string/jumbo v0, "keywords"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addMeta(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public addMeta(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "meta"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 81
    .local v0, "meta":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "name"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string/jumbo v1, "content"

    invoke-interface {v0, v1, p2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 84
    return-void
.end method

.method public addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "element"    # Lorg/w3c/dom/Element;
    .param p2, "classNamePrefix"    # Ljava/lang/String;
    .param p3, "style"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string/jumbo v3, "class"

    invoke-interface {p1, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, "exising":Ljava/lang/String;
    invoke-virtual {p0, p2, p3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "addition":Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v2, v0

    .line 93
    .local v2, "newClassValue":Ljava/lang/String;
    :goto_0
    const-string/jumbo v3, "class"

    invoke-interface {p1, v3, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return-void

    .line 92
    .end local v2    # "newClassValue":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected buildStylesheet(Ljava/util/Map;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "prefixToMapOfStyles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 114
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 115
    .local v5, "stylesheetText":Ljava/lang/String;
    return-object v5

    .line 100
    .end local v5    # "stylesheetText":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 102
    .local v0, "byPrefix":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 104
    .local v1, "byStyle":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 105
    .local v4, "style":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 107
    .local v2, "className":Ljava/lang/String;
    const-string/jumbo v8, "."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string/jumbo v8, "{"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v8, "}\n"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public createBlock()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "div"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createBookmark(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "a"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 126
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "name"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-object v0
.end method

.method public createHeader1()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "h1"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createHeader2()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "h2"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createHyperlink(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 3
    .param p1, "internalDestination"    # Ljava/lang/String;

    .prologue
    .line 142
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "a"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 143
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "href"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    return-object v0
.end method

.method public createImage(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 3
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "img"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 150
    .local v0, "result":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "src"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return-object v0
.end method

.method public createLineBreak()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "br"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createListItem()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "li"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createOption(Ljava/lang/String;Z)Lorg/w3c/dom/Element;
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "selected"    # Z

    .prologue
    .line 166
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "option"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 167
    .local v0, "result":Lorg/w3c/dom/Element;
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 168
    if-eqz p2, :cond_0

    .line 170
    const-string/jumbo v1, "selected"

    const-string/jumbo v2, "selected"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    return-object v0
.end method

.method public createParagraph()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "p"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createSelect()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 182
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "select"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 183
    .local v0, "result":Lorg/w3c/dom/Element;
    return-object v0
.end method

.method public createTable()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "table"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableBody()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "tbody"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableCell()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "td"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableColumn()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "col"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableColumnGroup()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "colgroup"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableHeader()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "thead"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableHeaderCell()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "th"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableRow()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "tr"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createText(Ljava/lang/String;)Lorg/w3c/dom/Text;
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 228
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    return-object v0
.end method

.method public createUnorderedList()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "ul"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public getBody()Lorg/w3c/dom/Element;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method public getHead()Lorg/w3c/dom/Element;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    return-object v0
.end method

.method public getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "classNamePrefix"    # Ljava/lang/String;
    .param p2, "style"    # Ljava/lang/String;

    .prologue
    .line 253
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheet:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 254
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheet:Ljava/util/Map;

    new-instance v4, Ljava/util/LinkedHashMap;

    .line 255
    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 254
    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheet:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 258
    .local v2, "styleToClassName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 259
    .local v0, "knownClass":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 264
    .end local v0    # "knownClass":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 262
    .restart local v0    # "knownClass":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "newClassName":Ljava/lang/String;
    invoke-interface {v2, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 264
    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    if-nez v0, :cond_0

    .line 270
    const/4 v0, 0x0

    .line 272
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    invoke-interface {v0}, Lorg/w3c/dom/Text;->getTextContent()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "titleText"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 277
    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->removeChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 280
    iput-object v2, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    .line 281
    iput-object v2, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    .line 284
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    if-nez v0, :cond_1

    .line 286
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "title"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    .line 287
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    .line 288
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 289
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->head:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->title:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 292
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Text;->setData(Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method public updateStylesheet()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheetElement:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->stylesheet:Ljava/util/Map;

    invoke-virtual {p0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->buildStylesheet(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->setTextContent(Ljava/lang/String;)V

    .line 298
    return-void
.end method
