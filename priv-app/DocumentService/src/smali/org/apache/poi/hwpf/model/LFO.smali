.class public Lorg/apache/poi/hwpf/model/LFO;
.super Lorg/apache/poi/hwpf/model/types/LFOAbstractType;
.source "LFO.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/LFOAbstractType;-><init>()V

    .line 40
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/LFO;->fillFields([BI)V

    .line 41
    return-void
.end method
