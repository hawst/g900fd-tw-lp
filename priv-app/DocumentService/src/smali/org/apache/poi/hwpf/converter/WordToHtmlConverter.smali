.class public Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;
.super Lorg/apache/poi/hwpf/converter/AbstractWordConverter;
.source "WordToHtmlConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;
    }
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final blocksProperies:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;",
            ">;"
        }
    .end annotation
.end field

.field private final htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

.field private notes:Lorg/w3c/dom/Element;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-class v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 87
    sput-object v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 88
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;)V
    .locals 1
    .param p1, "htmlDocumentFacade"    # Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .prologue
    .line 199
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 181
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    .line 201
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 202
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 1
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 181
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    .line 196
    new-instance v0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;-><init>(Lorg/w3c/dom/Document;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 197
    return-void
.end method

.method private static getSectionStyle(Lorg/apache/poi/hwpf/usermodel/Section;)Ljava/lang/String;
    .locals 9
    .param p0, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;

    .prologue
    const/high16 v8, 0x44b40000    # 1440.0f

    .line 93
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginLeft()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v6, v8

    .line 94
    .local v2, "leftMargin":F
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginRight()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v6, v8

    .line 95
    .local v3, "rightMargin":F
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginTop()I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v8

    .line 96
    .local v5, "topMargin":F
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginBottom()I

    move-result v6

    int-to-float v6, v6

    div-float v0, v6, v8

    .line 98
    .local v0, "bottomMargin":F
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "margin: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 99
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "in;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 98
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 101
    .local v4, "style":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getNumColumns()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_0

    .line 103
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "column-count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getNumColumns()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 104
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->isColumnsEvenlySpaced()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 106
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getDistanceBetweenColumns()I

    move-result v6

    int-to-float v6, v6

    div-float v1, v6, v8

    .line 108
    .local v1, "distance":F
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "column-gap: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "in;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 115
    .end local v1    # "distance":F
    :cond_0
    :goto_0
    return-object v4

    .line 112
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "column-gap: 0.25in;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 12
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 129
    array-length v8, p0

    const/4 v9, 0x2

    if-ge v8, v9, :cond_1

    .line 131
    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 132
    const-string/jumbo v9, "Usage: WordToHtmlConverter <inputFile.doc> <saveTo.html>"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    const/4 v3, 0x0

    .line 142
    .local v3, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v8, Ljava/io/File;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->process(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 144
    .local v0, "doc":Lorg/w3c/dom/Document;
    new-instance v4, Ljava/io/FileWriter;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v4, v8}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    .end local v3    # "out":Ljava/io/FileWriter;
    .local v4, "out":Ljava/io/FileWriter;
    :try_start_1
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v0}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 146
    .local v1, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v6, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v6, v4}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 148
    .local v6, "streamResult":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v7

    .line 149
    .local v7, "tf":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v7}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v5

    .line 151
    .local v5, "serializer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v8, "encoding"

    const-string/jumbo v9, "UTF-8"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v8, "indent"

    const-string/jumbo v9, "yes"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v8, "method"

    const-string/jumbo v9, "html"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v5, v1, v6}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 162
    if-eqz v4, :cond_3

    .line 164
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 165
    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 157
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_0
    move-exception v2

    .line 159
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 162
    if-eqz v3, :cond_0

    .line 164
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 165
    :catch_1
    move-exception v2

    .line 166
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 161
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 162
    :goto_2
    if-eqz v3, :cond_2

    .line 164
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 168
    :cond_2
    :goto_3
    throw v8

    .line 165
    :catch_2
    move-exception v2

    .line 166
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 165
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .restart local v4    # "out":Ljava/io/FileWriter;
    .restart local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .restart local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .restart local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_3
    move-exception v2

    .line 166
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_0

    .line 161
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v3    # "out":Ljava/io/FileWriter;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .line 157
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_1
.end method

.method static process(Ljava/io/File;)Lorg/w3c/dom/Document;
    .locals 3
    .param p0, "docFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 173
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    .line 174
    .local v0, "wordDocument":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    new-instance v1, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;

    .line 175
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 176
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    .line 174
    invoke-direct {v1, v2}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;-><init>(Lorg/w3c/dom/Document;)V

    .line 177
    .local v1, "wordToHtmlConverter":Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;
    invoke-virtual {v1, v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 178
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected afterProcess()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getBody()Lorg/w3c/dom/Element;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 210
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->updateStylesheet()V

    .line 211
    return-void
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method protected outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V
    .locals 8
    .param p1, "pElement"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 222
    iget-object v5, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    iget-object v5, v5, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v6, "span"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 223
    .local v1, "span":Lorg/w3c/dom/Element;
    invoke-interface {p1, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    .local v2, "style":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;

    .line 227
    .local v0, "blockProperies":Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getCharacterRunTriplet(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    move-result-object v4

    .line 229
    .local v4, "triplet":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    iget-object v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-static {v5}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 230
    iget-object v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 231
    iget-object v6, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;->pFontName:Ljava/lang/String;

    .line 230
    invoke-static {v5, v6}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    .line 231
    if-nez v5, :cond_0

    .line 233
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "font-family:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iget v6, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;->pFontSize:I

    if-eq v5, v6, :cond_1

    .line 237
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "font-size:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "pt;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    :cond_1
    iget-boolean v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    if-eqz v5, :cond_2

    .line 241
    const-string/jumbo v5, "font-weight:bold;"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_2
    iget-boolean v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    if-eqz v5, :cond_3

    .line 245
    const-string/jumbo v5, "font-style:italic;"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_3
    invoke-static {p2, v2}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addCharactersProperties(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/StringBuilder;)V

    .line 249
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_4

    .line 250
    iget-object v5, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    const-string/jumbo v6, "s"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v1, v6, v7}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_4
    iget-object v5, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5, p3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v3

    .line 253
    .local v3, "textNode":Lorg/w3c/dom/Text;
    invoke-interface {v1, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 254
    return-void
.end method

.method protected processBookmarks(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/util/List;)V
    .locals 6
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/HWPFDocumentCore;",
            "Lorg/w3c/dom/Element;",
            "Lorg/apache/poi/hwpf/usermodel/Range;",
            "I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p5, "rangeBookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    move-object v2, p2

    .line 262
    .local v2, "parent":Lorg/w3c/dom/Element;
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 270
    if-eqz p3, :cond_0

    .line 271
    invoke-virtual {p0, p1, p4, p3, v2}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 272
    :cond_0
    return-void

    .line 262
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/Bookmark;

    .line 264
    .local v0, "bookmark":Lorg/apache/poi/hwpf/usermodel/Bookmark;
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 265
    invoke-interface {v0}, Lorg/apache/poi/hwpf/usermodel/Bookmark;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBookmark(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 266
    .local v1, "bookmarkElement":Lorg/w3c/dom/Element;
    invoke-interface {v2, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 267
    move-object v2, v1

    goto :goto_0
.end method

.method protected processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
    .locals 2
    .param p1, "summaryInformation"    # Lorg/apache/poi/hpsf/SummaryInformation;

    .prologue
    .line 278
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->setTitle(Ljava/lang/String;)V

    .line 281
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addAuthor(Ljava/lang/String;)V

    .line 284
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addKeywords(Ljava/lang/String;)V

    .line 287
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 288
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 289
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addDescription(Ljava/lang/String;)V

    .line 290
    :cond_3
    return-void
.end method

.method public processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 0
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 295
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 296
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->afterProcess()V

    .line 297
    return-void
.end method

.method protected processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;Ljava/lang/String;Lorg/w3c/dom/Element;)V
    .locals 2
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "officeDrawing"    # Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 317
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v1, p4}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createImage(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 318
    .local v0, "img":Lorg/w3c/dom/Element;
    invoke-interface {p5, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 319
    return-void
.end method

.method protected processDropDownList(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;[Ljava/lang/String;I)V
    .locals 5
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "values"    # [Ljava/lang/String;
    .param p4, "defaultIndex"    # I

    .prologue
    .line 303
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createSelect()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 304
    .local v1, "select":Lorg/w3c/dom/Element;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p3

    if-lt v0, v2, :cond_0

    .line 309
    invoke-interface {p1, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 310
    return-void

    .line 306
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    aget-object v4, p3, v0

    .line 307
    if-ne p4, v0, :cond_1

    const/4 v2, 0x1

    .line 306
    :goto_1
    invoke-virtual {v3, v4, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createOption(Ljava/lang/String;Z)Lorg/w3c/dom/Element;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected processEndnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 6
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "noteIndex"    # I
    .param p3, "block"    # Lorg/w3c/dom/Element;
    .param p4, "endnoteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 325
    const-string/jumbo v2, "end"

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processNoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;Ljava/lang/String;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 327
    return-void
.end method

.method protected processFootnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 6
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "noteIndex"    # I
    .param p3, "block"    # Lorg/w3c/dom/Element;
    .param p4, "footnoteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 333
    const-string/jumbo v2, "foot"

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processNoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;Ljava/lang/String;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 335
    return-void
.end method

.method protected processHyperlink(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
    .locals 2
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "textRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 342
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v1, p5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createHyperlink(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 343
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 345
    if-eqz p3, :cond_0

    .line 346
    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 348
    :cond_0
    return-void
.end method

.method protected processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;Ljava/lang/String;)V
    .locals 21
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;
    .param p4, "imageSourcePath"    # Ljava/lang/String;

    .prologue
    .line 353
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getHorizontalScalingFactor()I

    move-result v3

    .line 354
    .local v3, "aspectRatioX":I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getVerticalScalingFactor()I

    move-result v4

    .line 356
    .local v4, "aspectRatioY":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 366
    .local v14, "style":Ljava/lang/StringBuilder;
    if-lez v3, :cond_1

    .line 368
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v17

    mul-int v17, v17, v3

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x447a0000    # 1000.0f

    div-float v17, v17, v18

    .line 369
    const/high16 v18, 0x44b40000    # 1440.0f

    .line 368
    div-float v11, v17, v18

    .line 370
    .local v11, "imageWidth":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropRight()I

    move-result v17

    mul-int v17, v17, v3

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x447a0000    # 1000.0f

    div-float v17, v17, v18

    .line 371
    const/high16 v18, 0x44b40000    # 1440.0f

    .line 370
    div-float v7, v17, v18

    .line 372
    .local v7, "cropRight":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropLeft()I

    move-result v17

    mul-int v17, v17, v3

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x447a0000    # 1000.0f

    div-float v17, v17, v18

    .line 373
    const/high16 v18, 0x44b40000    # 1440.0f

    .line 372
    div-float v6, v17, v18

    .line 382
    .local v6, "cropLeft":F
    :goto_0
    if-lez v4, :cond_2

    .line 384
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v17

    mul-int v17, v17, v4

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x447a0000    # 1000.0f

    div-float v17, v17, v18

    .line 385
    const/high16 v18, 0x44b40000    # 1440.0f

    .line 384
    div-float v10, v17, v18

    .line 386
    .local v10, "imageHeight":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropTop()I

    move-result v17

    mul-int v17, v17, v4

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x447a0000    # 1000.0f

    div-float v17, v17, v18

    .line 387
    const/high16 v18, 0x44b40000    # 1440.0f

    .line 386
    div-float v8, v17, v18

    .line 388
    .local v8, "cropTop":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropBottom()I

    move-result v17

    mul-int v17, v17, v4

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x447a0000    # 1000.0f

    div-float v17, v17, v18

    .line 389
    const/high16 v18, 0x44b40000    # 1440.0f

    .line 388
    div-float v5, v17, v18

    .line 399
    .local v5, "cropBottom":F
    :goto_1
    const/16 v17, 0x0

    cmpl-float v17, v8, v17

    if-nez v17, :cond_0

    const/16 v17, 0x0

    cmpl-float v17, v7, v17

    if-nez v17, :cond_0

    const/16 v17, 0x0

    cmpl-float v17, v5, v17

    if-nez v17, :cond_0

    const/16 v17, 0x0

    cmpl-float v17, v6, v17

    if-eqz v17, :cond_3

    .line 402
    :cond_0
    const/16 v17, 0x0

    sub-float v18, v11, v6

    sub-float v18, v18, v7

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(FF)F

    move-result v16

    .line 403
    .local v16, "visibleWidth":F
    const/16 v17, 0x0

    sub-float v18, v10, v8

    .line 404
    sub-float v18, v18, v5

    .line 403
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(FF)F

    move-result v15

    .line 406
    .local v15, "visibleHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v13

    .line 407
    .local v13, "root":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    const-string/jumbo v18, "d"

    .line 408
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "vertical-align:text-bottom;width:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 409
    const-string/jumbo v20, "in;height:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "in;"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 408
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 407
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v13, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v12

    .line 413
    .local v12, "inner":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    const-string/jumbo v18, "d"

    .line 414
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "position:relative;width:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "in;height:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 415
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "in;overflow:hidden;"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 414
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 413
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    invoke-interface {v13, v12}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createImage(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v9

    .line 419
    .local v9, "image":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    const-string/jumbo v18, "i"

    .line 420
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "position:absolute;left:-"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ";top:-"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 421
    const-string/jumbo v20, ";width:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "in;height:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 422
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "in;"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 420
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 419
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v9, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-interface {v12, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 425
    const-string/jumbo v17, "overflow:hidden;"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    .end local v9    # "image":Lorg/w3c/dom/Element;
    .end local v12    # "inner":Lorg/w3c/dom/Element;
    .end local v15    # "visibleHeight":F
    .end local v16    # "visibleWidth":F
    :goto_2
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 435
    return-void

    .line 377
    .end local v5    # "cropBottom":F
    .end local v6    # "cropLeft":F
    .end local v7    # "cropRight":F
    .end local v8    # "cropTop":F
    .end local v10    # "imageHeight":F
    .end local v11    # "imageWidth":F
    .end local v13    # "root":Lorg/w3c/dom/Element;
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x44b40000    # 1440.0f

    div-float v11, v17, v18

    .line 378
    .restart local v11    # "imageWidth":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropRight()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x44b40000    # 1440.0f

    div-float v7, v17, v18

    .line 379
    .restart local v7    # "cropRight":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaCropLeft()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x44b40000    # 1440.0f

    div-float v6, v17, v18

    .restart local v6    # "cropLeft":F
    goto/16 :goto_0

    .line 393
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x44b40000    # 1440.0f

    div-float v10, v17, v18

    .line 394
    .restart local v10    # "imageHeight":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropTop()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x44b40000    # 1440.0f

    div-float v8, v17, v18

    .line 395
    .restart local v8    # "cropTop":F
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaCropBottom()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x44b40000    # 1440.0f

    div-float v5, v17, v18

    .restart local v5    # "cropBottom":F
    goto/16 :goto_1

    .line 429
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createImage(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v13

    .line 430
    .restart local v13    # "root":Lorg/w3c/dom/Element;
    const-string/jumbo v17, "style"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string/jumbo v19, "width:"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "in;height:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 431
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "in;vertical-align:text-bottom;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 430
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v13, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method protected processImageWithoutPicturesManager(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V
    .locals 3
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;

    .prologue
    .line 442
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->document:Lorg/w3c/dom/Document;

    .line 443
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Image link to \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 444
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestFullFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' can be here"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 443
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createComment(Ljava/lang/String;)Lorg/w3c/dom/Comment;

    move-result-object v0

    .line 442
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 445
    return-void
.end method

.method protected processLineBreak(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;)V
    .locals 1
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    .line 450
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createLineBreak()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 451
    return-void
.end method

.method protected processNoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;Ljava/lang/String;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 13
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "noteIndex"    # I
    .param p4, "block"    # Lorg/w3c/dom/Element;
    .param p5, "noteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 456
    add-int/lit8 v9, p3, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 457
    .local v7, "textIndex":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 458
    const-string/jumbo v10, "a"

    const-string/jumbo v11, "vertical-align:super;font-size:smaller;"

    .line 457
    invoke-virtual {v9, v10, v11}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 459
    .local v8, "textIndexClass":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v10, "note_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 460
    .local v4, "forwardNoteLink":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v10, "note_back_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 462
    .local v2, "backwardNoteLink":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "#"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 463
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 462
    invoke-virtual {v9, v10}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createHyperlink(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 464
    .local v1, "anchor":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "name"

    invoke-interface {v1, v9, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string/jumbo v9, "class"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 466
    const-string/jumbo v11, "noteanchor"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 465
    invoke-interface {v1, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    invoke-interface {v1, v7}, Lorg/w3c/dom/Element;->setTextContent(Ljava/lang/String;)V

    .line 468
    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 470
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    if-nez v9, :cond_0

    .line 472
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v9

    iput-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    .line 473
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    const-string/jumbo v10, "class"

    const-string/jumbo v11, "notes"

    invoke-interface {v9, v10, v11}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_0
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v5

    .line 477
    .local v5, "note":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "class"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "note"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->notes:Lorg/w3c/dom/Element;

    invoke-interface {v9, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 480
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v9, v4}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBookmark(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 481
    .local v3, "bookmark":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "href"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "#"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-interface {v3, v7}, Lorg/w3c/dom/Element;->setTextContent(Ljava/lang/String;)V

    .line 483
    const-string/jumbo v9, "class"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 484
    const-string/jumbo v11, "noteindex"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 483
    invoke-interface {v3, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-interface {v5, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 486
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v9

    invoke-interface {v5, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 488
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v9

    const-string/jumbo v10, "span"

    invoke-interface {v9, v10}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v6

    .line 489
    .local v6, "span":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "class"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "notetext"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 492
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    new-instance v10, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;

    const-string/jumbo v11, ""

    const/4 v12, -0x1

    invoke-direct {v10, v11, v12}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v9, v10}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 495
    const/high16 v9, -0x80000000

    :try_start_0
    move-object/from16 v0, p5

    invoke-virtual {p0, p1, v9, v0, v6}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    invoke-virtual {v9}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 501
    return-void

    .line 498
    :catchall_0
    move-exception v9

    .line 499
    iget-object v10, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 500
    throw v9
.end method

.method protected processPageBreak(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;)V
    .locals 1
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;

    .prologue
    .line 506
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createLineBreak()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 507
    return-void
.end method

.method protected processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
    .locals 4
    .param p1, "hwpfDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "textRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "pageref"    # Ljava/lang/String;

    .prologue
    .line 513
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createHyperlink(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 514
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 516
    if-eqz p3, :cond_0

    .line 517
    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 519
    :cond_0
    return-void
.end method

.method protected processParagraph(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;ILorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/String;)V
    .locals 22
    .param p1, "hwpfDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "parentElement"    # Lorg/w3c/dom/Element;
    .param p3, "currentTableLevel"    # I
    .param p4, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p5, "bulletText"    # Ljava/lang/String;

    .prologue
    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createParagraph()Lorg/w3c/dom/Element;

    move-result-object v9

    .line 526
    .local v9, "pElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 528
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 529
    .local v14, "style":Ljava/lang/StringBuilder;
    move-object/from16 v0, p4

    invoke-static {v0, v14}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addParagraphProperties(Lorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/StringBuilder;)V

    .line 531
    invoke-virtual/range {p4 .. p4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->numCharacterRuns()I

    move-result v4

    .line 533
    .local v4, "charRuns":I
    if-nez v4, :cond_0

    .line 612
    :goto_0
    return-void

    .line 541
    :cond_0
    const/16 v17, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v5

    .line 542
    .local v5, "characterRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    if-eqz v5, :cond_3

    .line 544
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getCharacterRunTriplet(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    move-result-object v16

    .line 545
    .local v16, "triplet":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v17

    div-int/lit8 v11, v17, 0x2

    .line 546
    .local v11, "pFontSize":I
    move-object/from16 v0, v16

    iget-object v10, v0, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 547
    .local v10, "pFontName":Ljava/lang/String;
    invoke-static {v10, v14}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addFontFamily(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 548
    invoke-static {v11, v14}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addFontSize(ILjava/lang/StringBuilder;)V

    .line 555
    .end local v16    # "triplet":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    move-object/from16 v17, v0

    new-instance v18, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;

    move-object/from16 v0, v18

    invoke-direct {v0, v10, v11}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter$BlockProperies;-><init>(Ljava/lang/String;I)V

    invoke-virtual/range {v17 .. v18}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    :try_start_0
    invoke-static/range {p5 .. p5}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 561
    const-string/jumbo v17, "\t"

    move-object/from16 v0, p5

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 567
    const/high16 v6, 0x44340000    # 720.0f

    .line 568
    .local v6, "defaultTab":F
    invoke-virtual/range {p4 .. p4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIndentFromLeft()I

    move-result v17

    .line 569
    invoke-virtual/range {p4 .. p4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getFirstLineIndent()I

    move-result v18

    .line 568
    add-int v17, v17, v18

    add-int/lit8 v17, v17, 0x14

    move/from16 v0, v17

    int-to-float v7, v0

    .line 573
    .local v7, "firstLinePosition":F
    const/high16 v17, 0x44340000    # 720.0f

    .line 572
    div-float v17, v7, v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v18

    .line 573
    const-wide v20, 0x4086800000000000L    # 720.0

    .line 572
    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v8, v0

    .line 575
    .local v8, "nextStop":F
    sub-float v13, v8, v7

    .line 577
    .local v13, "spanMinWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v17

    .line 578
    const-string/jumbo v18, "span"

    invoke-interface/range {v17 .. v18}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v12

    .line 579
    .local v12, "span":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    .line 580
    const-string/jumbo v18, "s"

    .line 581
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "display: inline-block; text-indent: 0; min-width: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 582
    const/high16 v20, 0x44b40000    # 1440.0f

    div-float v20, v13, v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 583
    const-string/jumbo v20, "in;"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 581
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 580
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    invoke-interface {v9, v12}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 586
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    .line 587
    const/16 v19, 0x0

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, p5

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 588
    const/16 v19, 0x200b

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 589
    const/16 v19, 0xa0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 586
    invoke-virtual/range {v17 .. v18}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v15

    .line 590
    .local v15, "textNode":Lorg/w3c/dom/Text;
    invoke-interface {v12, v15}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 600
    .end local v6    # "defaultTab":F
    .end local v7    # "firstLinePosition":F
    .end local v8    # "nextStop":F
    .end local v12    # "span":Lorg/w3c/dom/Element;
    .end local v13    # "spanMinWidth":F
    .end local v15    # "textNode":Lorg/w3c/dom/Text;
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3, v9}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 608
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_2

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    const-string/jumbo v18, "p"

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v9, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    :cond_2
    invoke-static {v9}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->compactSpans(Lorg/w3c/dom/Element;)V

    goto/16 :goto_0

    .line 552
    .end local v10    # "pFontName":Ljava/lang/String;
    .end local v11    # "pFontSize":I
    :cond_3
    const/4 v11, -0x1

    .line 553
    .restart local v11    # "pFontSize":I
    const-string/jumbo v10, ""

    .restart local v10    # "pFontName":Ljava/lang/String;
    goto/16 :goto_1

    .line 594
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v17, v0

    .line 595
    const/16 v18, 0x0

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, p5

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 594
    invoke-virtual/range {v17 .. v18}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v15

    .line 596
    .restart local v15    # "textNode":Lorg/w3c/dom/Text;
    invoke-interface {v9, v15}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 604
    .end local v15    # "textNode":Lorg/w3c/dom/Text;
    :catchall_0
    move-exception v17

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->blocksProperies:Ljava/util/Stack;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 606
    throw v17
.end method

.method protected processSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;I)V
    .locals 4
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;
    .param p3, "sectionCounter"    # I

    .prologue
    .line 618
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 619
    .local v0, "div":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    const-string/jumbo v2, "d"

    invoke-static {p2}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getSectionStyle(Lorg/apache/poi/hwpf/usermodel/Section;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    iget-object v1, v1, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 622
    const/high16 v1, -0x80000000

    invoke-virtual {p0, p1, v0, p2, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 623
    return-void
.end method

.method protected processSingleSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;)V
    .locals 4
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;

    .prologue
    .line 629
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    iget-object v1, v1, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    const-string/jumbo v2, "b"

    .line 630
    invoke-static {p2}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getSectionStyle(Lorg/apache/poi/hwpf/usermodel/Section;)Ljava/lang/String;

    move-result-object v3

    .line 629
    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->body:Lorg/w3c/dom/Element;

    .line 633
    const/high16 v1, -0x80000000

    .line 632
    invoke-virtual {p0, p1, v0, p2, v1}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 634
    return-void
.end method

.method protected processTable(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Table;)V
    .locals 34
    .param p1, "hwpfDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;
    .param p3, "table"    # Lorg/apache/poi/hwpf/usermodel/Table;

    .prologue
    .line 639
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableHeader()Lorg/w3c/dom/Element;

    move-result-object v30

    .line 640
    .local v30, "tableHeader":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableBody()Lorg/w3c/dom/Element;

    move-result-object v27

    .line 643
    .local v27, "tableBody":Lorg/w3c/dom/Element;
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->buildTableCellEdgesArray(Lorg/apache/poi/hwpf/usermodel/Table;)[I

    move-result-object v12

    .line 644
    .local v12, "tableCellEdges":[I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->numRows()I

    move-result v33

    .line 646
    .local v33, "tableRows":I
    const/high16 v24, -0x80000000

    .line 647
    .local v24, "maxColumns":I
    const/4 v13, 0x0

    .local v13, "r":I
    :goto_0
    move/from16 v0, v33

    if-lt v13, v0, :cond_1

    .line 652
    const/4 v13, 0x0

    :goto_1
    move/from16 v0, v33

    if-lt v13, v0, :cond_2

    .line 737
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTable()Lorg/w3c/dom/Element;

    move-result-object v29

    .line 740
    .local v29, "tableElement":Lorg/w3c/dom/Element;
    const-string/jumbo v5, "class"

    .line 741
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 742
    const-string/jumbo v7, "t"

    .line 743
    const-string/jumbo v8, "table-layout:fixed;border-collapse:collapse;border-spacing:0;"

    .line 742
    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 739
    move-object/from16 v0, v29

    invoke-interface {v0, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    invoke-interface/range {v30 .. v30}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 746
    invoke-interface/range {v29 .. v30}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 748
    :cond_0
    invoke-interface/range {v27 .. v27}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 750
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 751
    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 759
    :goto_2
    return-void

    .line 649
    .end local v29    # "tableElement":Lorg/w3c/dom/Element;
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v5

    move/from16 v0, v24

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v24

    .line 647
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 654
    :cond_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v3

    .line 656
    .local v3, "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v31

    .line 657
    .local v31, "tableRowElement":Lorg/w3c/dom/Element;
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    .line 658
    .local v32, "tableRowStyle":Ljava/lang/StringBuilder;
    move-object/from16 v0, v32

    invoke-static {v3, v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addTableRowProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Ljava/lang/StringBuilder;)V

    .line 661
    const/16 v23, 0x0

    .line 662
    .local v23, "currentEdgeIndex":I
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v25

    .line 663
    .local v25, "rowCells":I
    const/4 v14, 0x0

    .local v14, "c":I
    :goto_3
    move/from16 v0, v25

    if-lt v14, v0, :cond_4

    .line 723
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 724
    const-string/jumbo v5, "class"

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 725
    const-string/jumbo v7, "r"

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 724
    move-object/from16 v0, v31

    invoke-interface {v0, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_3
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/TableRow;->isTableHeader()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 729
    invoke-interface/range {v30 .. v31}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 652
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 665
    :cond_4
    invoke-virtual {v3, v14}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v4

    .line 667
    .local v4, "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isVerticallyMerged()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 668
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isFirstVerticallyMerged()Z

    move-result v5

    if-nez v5, :cond_6

    .line 671
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v12, v1, v4}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v5

    add-int v23, v23, v5

    .line 663
    :cond_5
    :goto_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 676
    :cond_6
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/TableRow;->isTableHeader()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 678
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 679
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableHeaderCell()Lorg/w3c/dom/Element;

    move-result-object v28

    .line 685
    .local v28, "tableCellElement":Lorg/w3c/dom/Element;
    :goto_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 687
    .local v9, "tableCellStyle":Ljava/lang/StringBuilder;
    if-nez v13, :cond_c

    const/4 v5, 0x1

    :goto_7
    add-int/lit8 v6, v33, -0x1

    if-ne v13, v6, :cond_d

    const/4 v6, 0x1

    :goto_8
    if-nez v14, :cond_e

    const/4 v7, 0x1

    :goto_9
    add-int/lit8 v8, v25, -0x1

    if-ne v14, v8, :cond_f

    const/4 v8, 0x1

    .line 686
    :goto_a
    invoke-static/range {v3 .. v9}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->addTableCellProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Lorg/apache/poi/hwpf/usermodel/TableCell;ZZZZLjava/lang/StringBuilder;)V

    .line 690
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v12, v1, v4}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v22

    .line 692
    .local v22, "colSpan":I
    add-int v23, v23, v22

    .line 694
    if-eqz v22, :cond_5

    .line 697
    const/4 v5, 0x1

    move/from16 v0, v22

    if-eq v0, v5, :cond_7

    .line 698
    const-string/jumbo v5, "colspan"

    .line 699
    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 698
    move-object/from16 v0, v28

    invoke-interface {v0, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object/from16 v10, p0

    move-object/from16 v11, p3

    move-object v15, v4

    .line 701
    invoke-virtual/range {v10 .. v15}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->getNumberRowsSpanned(Lorg/apache/poi/hwpf/usermodel/Table;[IIILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v26

    .line 703
    .local v26, "rowSpan":I
    const/4 v5, 0x1

    move/from16 v0, v26

    if-le v0, v5, :cond_8

    .line 704
    const-string/jumbo v5, "rowspan"

    .line 705
    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 704
    move-object/from16 v0, v28

    invoke-interface {v0, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_8
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getTableLevel()I

    move-result v5

    .line 707
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v4, v5}, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 710
    invoke-interface/range {v28 .. v28}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-nez v5, :cond_9

    .line 712
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 713
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createParagraph()Lorg/w3c/dom/Element;

    move-result-object v5

    .line 712
    move-object/from16 v0, v28

    invoke-interface {v0, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 715
    :cond_9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_a

    .line 716
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 717
    invoke-interface/range {v28 .. v28}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v6

    .line 718
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 716
    move-object/from16 v0, v28

    invoke-virtual {v5, v0, v6, v7}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    :cond_a
    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto/16 :goto_5

    .line 683
    .end local v9    # "tableCellStyle":Ljava/lang/StringBuilder;
    .end local v22    # "colSpan":I
    .end local v26    # "rowSpan":I
    .end local v28    # "tableCellElement":Lorg/w3c/dom/Element;
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v28

    .restart local v28    # "tableCellElement":Lorg/w3c/dom/Element;
    goto/16 :goto_6

    .line 687
    .restart local v9    # "tableCellStyle":Ljava/lang/StringBuilder;
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_8

    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_9

    :cond_f
    const/4 v8, 0x0

    goto/16 :goto_a

    .line 733
    .end local v4    # "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    .end local v9    # "tableCellStyle":Ljava/lang/StringBuilder;
    .end local v28    # "tableCellElement":Lorg/w3c/dom/Element;
    :cond_10
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto/16 :goto_4

    .line 755
    .end local v3    # "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    .end local v14    # "c":I
    .end local v23    # "currentEdgeIndex":I
    .end local v25    # "rowCells":I
    .end local v31    # "tableRowElement":Lorg/w3c/dom/Element;
    .end local v32    # "tableRowStyle":Ljava/lang/StringBuilder;
    .restart local v29    # "tableElement":Lorg/w3c/dom/Element;
    :cond_11
    sget-object v15, Lorg/apache/poi/hwpf/converter/WordToHtmlConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/16 v16, 0x5

    const-string/jumbo v17, "Table without body starting at ["

    .line 756
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getStartOffset()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    const-string/jumbo v19, "; "

    .line 757
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getEndOffset()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    const-string/jumbo v21, ")"

    .line 755
    invoke-virtual/range {v15 .. v21}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2
.end method
