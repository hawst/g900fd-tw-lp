.class public final Lorg/apache/poi/hwpf/usermodel/Picture;
.super Ljava/lang/Object;
.source "Picture.java"


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$poi$hwpf$usermodel$PictureType:[I

.field public static final BMP:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COMPRESSED1:[B

.field public static final COMPRESSED2:[B

.field public static final EMF:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GIF:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IHDR:[B

.field public static final JPG:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PNG:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIFF:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIFF1:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WMF1:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WMF2:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _blipRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation
.end field

.field private _picf:Lorg/apache/poi/hwpf/model/PICF;

.field private _picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

.field private content:[B

.field private dataBlockStartOfsset:I

.field private height:I

.field private width:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$poi$hwpf$usermodel$PictureType()[I
    .locals 3

    .prologue
    .line 45
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->$SWITCH_TABLE$org$apache$poi$hwpf$usermodel$PictureType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/hwpf/usermodel/PictureType;->values()[Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->GIF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->$SWITCH_TABLE$org$apache$poi$hwpf$usermodel$PictureType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 48
    new-array v0, v5, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->BMP:[B

    .line 50
    new-array v0, v2, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->COMPRESSED1:[B

    .line 52
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->COMPRESSED2:[B

    .line 55
    new-array v0, v6, [B

    aput-byte v4, v0, v3

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->EMF:[B

    .line 58
    new-array v0, v2, [B

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->GIF:[B

    .line 59
    new-array v0, v6, [B

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->IHDR:[B

    .line 61
    new-array v0, v5, [B

    fill-array-data v0, :array_5

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->JPG:[B

    .line 63
    const-class v0, Lorg/apache/poi/hwpf/usermodel/Picture;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 62
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->log:Lorg/apache/poi/util/POILogger;

    .line 65
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_6

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->PNG:[B

    .line 68
    new-array v0, v6, [B

    const/16 v1, 0x49

    aput-byte v1, v0, v3

    const/16 v1, 0x49

    aput-byte v1, v0, v4

    const/16 v1, 0x2a

    aput-byte v1, v0, v5

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->TIFF:[B

    .line 71
    new-array v0, v6, [B

    const/16 v1, 0x4d

    aput-byte v1, v0, v3

    const/16 v1, 0x4d

    aput-byte v1, v0, v4

    const/16 v1, 0x2a

    aput-byte v1, v0, v2

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->TIFF1:[B

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [B

    const/16 v1, -0x29

    aput-byte v1, v0, v3

    const/16 v1, -0x33

    aput-byte v1, v0, v4

    const/16 v1, -0x3a

    aput-byte v1, v0, v5

    .line 74
    const/16 v1, -0x66

    aput-byte v1, v0, v2

    .line 73
    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->WMF1:[B

    .line 77
    const/4 v0, 0x6

    new-array v0, v0, [B

    aput-byte v4, v0, v3

    const/16 v1, 0x9

    aput-byte v1, v0, v5

    const/4 v1, 0x5

    aput-byte v2, v0, v1

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/Picture;->WMF2:[B

    return-void

    .line 48
    :array_0
    .array-data 1
        0x42t
        0x4dt
    .end array-data

    .line 50
    nop

    :array_1
    .array-data 1
        -0x2t
        0x78t
        -0x26t
    .end array-data

    .line 52
    :array_2
    .array-data 1
        -0x2t
        0x78t
        -0x64t
    .end array-data

    .line 58
    :array_3
    .array-data 1
        0x47t
        0x49t
        0x46t
    .end array-data

    .line 59
    :array_4
    .array-data 1
        0x49t
        0x48t
        0x44t
        0x52t
    .end array-data

    .line 61
    :array_5
    .array-data 1
        -0x1t
        -0x28t
    .end array-data

    .line 65
    nop

    :array_6
    .array-data 1
        -0x77t
        0x50t
        0x4et
        0x47t
        0xdt
        0xat
        0x1at
        0xat
    .end array-data
.end method

.method public constructor <init>(I[BZ)V
    .locals 1
    .param p1, "dataBlockStartOfsset"    # I
    .param p2, "_dataStream"    # [B
    .param p3, "fillBytes"    # Z

    .prologue
    const/4 v0, -0x1

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->height:I

    .line 116
    iput v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->width:I

    .line 135
    new-instance v0, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    .line 136
    invoke-direct {v0, p2, p1}, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;-><init>([BI)V

    .line 135
    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    .line 137
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->getPicf()Lorg/apache/poi/hwpf/model/PICF;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    .line 139
    iput p1, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->dataBlockStartOfsset:I

    .line 141
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->getBlipRecords()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->getBlipRecords()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    .line 145
    :cond_0
    if-eqz p3, :cond_1

    .line 146
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->fillImageContent()V

    .line 148
    :cond_1
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ddf/EscherBlipRecord;)V
    .locals 2
    .param p1, "blipRecord"    # Lorg/apache/poi/ddf/EscherBlipRecord;

    .prologue
    const/4 v0, -0x1

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->height:I

    .line 116
    iput v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->width:I

    .line 125
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/ddf/EscherBlipRecord;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    .line 126
    return-void
.end method

.method private fillImageContent()V
    .locals 11

    .prologue
    const/16 v8, 0x20

    const/16 v10, 0x10

    const/4 v9, 0x0

    .line 152
    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    if-eqz v7, :cond_1

    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    array-length v7, v7

    if-lez v7, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getRawContent()[B

    move-result-object v5

    .line 163
    .local v5, "rawContent":[B
    sget-object v7, Lorg/apache/poi/hwpf/usermodel/Picture;->COMPRESSED1:[B

    invoke-static {v5, v7, v8}, Lorg/apache/poi/hwpf/usermodel/Picture;->matchSignature([B[BI)Z

    move-result v7

    if-nez v7, :cond_2

    .line 164
    sget-object v7, Lorg/apache/poi/hwpf/usermodel/Picture;->COMPRESSED2:[B

    invoke-static {v5, v7, v8}, Lorg/apache/poi/hwpf/usermodel/Picture;->matchSignature([B[BI)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 168
    :cond_2
    :try_start_0
    new-instance v2, Ljava/util/zip/InflaterInputStream;

    .line 169
    new-instance v7, Ljava/io/ByteArrayInputStream;

    const/16 v8, 0x21

    .line 170
    array-length v9, v5

    add-int/lit8 v9, v9, -0x21

    .line 169
    invoke-direct {v7, v5, v8, v9}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 168
    invoke-direct {v2, v7}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 171
    .local v2, "in":Ljava/util/zip/InflaterInputStream;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 172
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    const/16 v7, 0x1000

    new-array v0, v7, [B

    .line 174
    .local v0, "buf":[B
    :goto_1
    invoke-virtual {v2, v0}, Ljava/util/zip/InflaterInputStream;->read([B)I

    move-result v6

    .local v6, "readBytes":I
    if-gtz v6, :cond_3

    .line 178
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iput-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 180
    .end local v0    # "buf":[B
    .end local v2    # "in":Ljava/util/zip/InflaterInputStream;
    .end local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "readBytes":I
    :catch_0
    move-exception v1

    .line 186
    .local v1, "e":Ljava/io/IOException;
    sget-object v7, Lorg/apache/poi/hwpf/usermodel/Picture;->log:Lorg/apache/poi/util/POILogger;

    const/4 v8, 0x3

    .line 187
    const-string/jumbo v9, "Possibly corrupt compression or non-compressed data"

    .line 186
    invoke-virtual {v7, v8, v9, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 176
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "buf":[B
    .restart local v2    # "in":Ljava/util/zip/InflaterInputStream;
    .restart local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "readBytes":I
    :cond_3
    const/4 v7, 0x0

    :try_start_1
    invoke-virtual {v3, v0, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 194
    .end local v0    # "buf":[B
    .end local v2    # "in":Ljava/util/zip/InflaterInputStream;
    .end local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "readBytes":I
    :cond_4
    iput-object v5, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    .line 198
    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    invoke-static {v7, v10}, Lorg/apache/poi/util/PngUtils;->matchesPngHeader([BI)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 200
    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    array-length v7, v7

    add-int/lit8 v7, v7, -0x10

    new-array v4, v7, [B

    .line 201
    .local v4, "png":[B
    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    array-length v8, v4

    invoke-static {v7, v10, v4, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    iput-object v4, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    goto :goto_0
.end method

.method private fillJPGWidthHeight()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 224
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getContent()[B

    move-result-object v2

    .line 226
    .local v2, "jpegContent":[B
    const/4 v4, 0x2

    .line 227
    .local v4, "pointer":I
    aget-byte v1, v2, v4

    .line 228
    .local v1, "firstByte":I
    const/4 v6, 0x3

    aget-byte v5, v2, v6

    .line 229
    .local v5, "secondByte":I
    array-length v0, v2

    .line 230
    .local v0, "endOfPicture":I
    :goto_0
    add-int/lit8 v6, v0, -0x1

    if-lt v4, v6, :cond_1

    .line 269
    :cond_0
    :goto_1
    return-void

    .line 234
    :cond_1
    aget-byte v1, v2, v4

    .line 235
    add-int/lit8 v6, v4, 0x1

    aget-byte v5, v2, v6

    .line 236
    add-int/lit8 v4, v4, 0x2

    .line 238
    if-eq v1, v8, :cond_2

    add-int/lit8 v6, v0, -0x1

    .line 232
    if-lt v4, v6, :cond_1

    .line 240
    :cond_2
    if-ne v1, v8, :cond_4

    add-int/lit8 v6, v0, -0x1

    if-ge v4, v6, :cond_4

    .line 242
    const/16 v6, -0x27

    if-eq v5, v6, :cond_0

    const/16 v6, -0x26

    if-eq v5, v6, :cond_0

    .line 246
    and-int/lit16 v6, v5, 0xf0

    const/16 v7, 0xc0

    if-ne v6, v7, :cond_3

    .line 247
    const/16 v6, -0x3c

    if-eq v5, v6, :cond_3

    .line 248
    const/16 v6, -0x38

    if-eq v5, v6, :cond_3

    .line 249
    const/16 v6, -0x34

    if-eq v5, v6, :cond_3

    .line 251
    add-int/lit8 v4, v4, 0x5

    .line 252
    invoke-static {v2, v4}, Lorg/apache/poi/hwpf/usermodel/Picture;->getBigEndianShort([BI)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->height:I

    .line 253
    add-int/lit8 v6, v4, 0x2

    invoke-static {v2, v6}, Lorg/apache/poi/hwpf/usermodel/Picture;->getBigEndianShort([BI)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->width:I

    goto :goto_1

    .line 258
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 259
    add-int/lit8 v4, v4, 0x1

    .line 260
    invoke-static {v2, v4}, Lorg/apache/poi/hwpf/usermodel/Picture;->getBigEndianShort([BI)I

    move-result v3

    .line 261
    .local v3, "length":I
    add-int/2addr v4, v3

    .line 263
    goto :goto_0

    .line 266
    .end local v3    # "length":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private fillWidthHeight()V
    .locals 3

    .prologue
    .line 289
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v0

    .line 291
    .local v0, "pictureType":Lorg/apache/poi/hwpf/usermodel/PictureType;
    invoke-static {}, Lorg/apache/poi/hwpf/usermodel/Picture;->$SWITCH_TABLE$org$apache$poi$hwpf$usermodel$PictureType()[I

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/PictureType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 303
    :goto_0
    :pswitch_0
    return-void

    .line 294
    :pswitch_1
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->fillJPGWidthHeight()V

    goto :goto_0

    .line 297
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->fillPNGWidthHeight()V

    goto :goto_0

    .line 291
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static getBigEndianInt([BI)I
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 82
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 83
    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    .line 82
    add-int/2addr v0, v1

    .line 84
    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    .line 82
    add-int/2addr v0, v1

    .line 84
    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 82
    add-int/2addr v0, v1

    return v0
.end method

.method private static getBigEndianShort([BI)I
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 89
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    return v0
.end method

.method private static matchSignature([B[BI)Z
    .locals 4
    .param p0, "pictureData"    # [B
    .param p1, "signature"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 95
    array-length v2, p0

    if-ge p2, v2, :cond_1

    const/4 v1, 0x1

    .line 96
    .local v1, "matched":Z
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    add-int v2, v0, p2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 97
    array-length v2, p1

    .line 96
    if-lt v0, v2, :cond_2

    .line 105
    :cond_0
    :goto_2
    return v1

    .line 95
    .end local v0    # "i":I
    .end local v1    # "matched":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 99
    .restart local v0    # "i":I
    .restart local v1    # "matched":Z
    :cond_2
    add-int v2, v0, p2

    aget-byte v2, p0, v2

    aget-byte v3, p1, v0

    if-eq v2, v3, :cond_3

    .line 101
    const/4 v1, 0x0

    .line 102
    goto :goto_2

    .line 97
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method fillPNGWidthHeight()V
    .locals 4

    .prologue
    .line 273
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getContent()[B

    move-result-object v2

    .line 278
    .local v2, "pngContent":[B
    sget-object v3, Lorg/apache/poi/hwpf/usermodel/Picture;->PNG:[B

    array-length v3, v3

    add-int/lit8 v0, v3, 0x4

    .line 279
    .local v0, "HEADER_START":I
    sget-object v3, Lorg/apache/poi/hwpf/usermodel/Picture;->IHDR:[B

    invoke-static {v2, v3, v0}, Lorg/apache/poi/hwpf/usermodel/Picture;->matchSignature([B[BI)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 281
    add-int/lit8 v1, v0, 0x4

    .line 282
    .local v1, "IHDR_CHUNK_WIDTH":I
    invoke-static {v2, v1}, Lorg/apache/poi/hwpf/usermodel/Picture;->getBigEndianInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->width:I

    .line 283
    add-int/lit8 v3, v1, 0x4

    invoke-static {v2, v3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getBigEndianInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->height:I

    .line 285
    .end local v1    # "IHDR_CHUNK_WIDTH":I
    :cond_0
    return-void
.end method

.method public getAspectRatioX()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getMx()I

    move-result v0

    div-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public getAspectRatioY()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getMy()I

    move-result v0

    div-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public getBlipRecordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 651
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    return-object v0
.end method

.method public getContent()[B
    .locals 1

    .prologue
    .line 330
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->fillImageContent()V

    .line 331
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->content:[B

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 8

    .prologue
    .line 519
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;->getShape()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 531
    const/4 v4, 0x0

    :goto_0
    return-object v4

    .line 519
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 520
    .local v2, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v5, v2, Lorg/apache/poi/ddf/EscherOptRecord;

    if-eqz v5, :cond_0

    move-object v1, v2

    .line 521
    check-cast v1, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 522
    .local v1, "escherOptRecord":Lorg/apache/poi/ddf/EscherOptRecord;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherProperty;

    .line 523
    .local v3, "property":Lorg/apache/poi/ddf/EscherProperty;
    const/16 v6, 0x381

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v7

    if-ne v6, v7, :cond_2

    .line 524
    check-cast v3, Lorg/apache/poi/ddf/EscherComplexProperty;

    .end local v3    # "property":Lorg/apache/poi/ddf/EscherProperty;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v0

    .line 525
    .local v0, "complexData":[B
    const/4 v4, 0x0

    array-length v5, v0

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, -0x1

    invoke-static {v0, v4, v5}, Lorg/apache/poi/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getDxaCropLeft()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 358
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getDxaReserved1()S

    move-result v0

    return v0
.end method

.method public getDxaCropRight()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getDxaReserved2()S

    move-result v0

    return v0
.end method

.method public getDxaGoal()I
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getDxaGoal()S

    move-result v0

    return v0
.end method

.method public getDyaCropBottom()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 387
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getDyaReserved2()S

    move-result v0

    return v0
.end method

.method public getDyaCropTop()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 396
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getDyaReserved1()S

    move-result v0

    return v0
.end method

.method public getDyaGoal()I
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getDyaGoal()S

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 416
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->height:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 418
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->fillWidthHeight()V

    .line 420
    :cond_0
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->height:I

    return v0
.end method

.method public getHeightInTwips()I
    .locals 2

    .prologue
    .line 425
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getAspectRatioY()I

    move-result v1

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    return v0
.end method

.method public getHorizontalScalingFactor()I
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getMx()I

    move-result v0

    return v0
.end method

.method public getLeftVlaue()I
    .locals 1

    .prologue
    .line 647
    const/4 v0, 0x0

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/PictureType;->getMime()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPICFAndOfficeArtData()Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picfAndOfficeArtData:Lorg/apache/poi/hwpf/model/PICFAndOfficeArtData;

    return-object v0
.end method

.method public getRawContent()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 455
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 456
    :cond_0
    new-array v1, v3, [B

    .line 470
    :goto_0
    return-object v1

    .line 459
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 460
    .local v0, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v1, v0, Lorg/apache/poi/ddf/EscherBlipRecord;

    if-eqz v1, :cond_2

    .line 462
    check-cast v0, Lorg/apache/poi/ddf/EscherBlipRecord;

    .end local v0    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBlipRecord;->getPicturedata()[B

    move-result-object v1

    goto :goto_0

    .line 465
    .restart local v0    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    :cond_2
    instance-of v1, v0, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v1, :cond_3

    .line 467
    check-cast v0, Lorg/apache/poi/ddf/EscherBSERecord;

    .end local v0    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v1

    .line 468
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherBlipRecord;->getPicturedata()[B

    move-result-object v1

    goto :goto_0

    .line 470
    .restart local v0    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    :cond_3
    new-array v1, v3, [B

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 479
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getContent()[B

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getStartOffset()I
    .locals 1

    .prologue
    .line 488
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->dataBlockStartOfsset:I

    return v0
.end method

.method public getVerticalScalingFactor()I
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_picf:Lorg/apache/poi/hwpf/model/PICF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PICF;->getMy()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 505
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 507
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->fillWidthHeight()V

    .line 509
    :cond_0
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->width:I

    return v0
.end method

.method public getWidthInTwips()I
    .locals 2

    .prologue
    .line 535
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getAspectRatioX()I

    move-result v1

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    return v0
.end method

.method public suggestFileExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public suggestFullFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 558
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestFileExtension()Ljava/lang/String;

    move-result-object v0

    .line 559
    .local v0, "fileExt":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->dataBlockStartOfsset:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 560
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 559
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 560
    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method public suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;
    .locals 4

    .prologue
    .line 565
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 566
    :cond_0
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 618
    :goto_0
    return-object v2

    .line 569
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/Picture;->_blipRecords:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 570
    .local v1, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 618
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    :sswitch_0
    move-object v0, v1

    .line 574
    check-cast v0, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 575
    .local v0, "bseRecord":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipTypeWin32()B

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 598
    :pswitch_0
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 578
    :pswitch_1
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 580
    :pswitch_2
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 582
    :pswitch_3
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 584
    :pswitch_4
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 586
    :pswitch_5
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 588
    :pswitch_6
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 590
    :pswitch_7
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 592
    :pswitch_8
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 594
    :pswitch_9
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 596
    :pswitch_a
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 602
    .end local v0    # "bseRecord":Lorg/apache/poi/ddf/EscherBSERecord;
    :sswitch_1
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 604
    :sswitch_2
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 606
    :sswitch_3
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 608
    :sswitch_4
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 610
    :sswitch_5
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 612
    :sswitch_6
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 614
    :sswitch_7
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 616
    :sswitch_8
    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    goto :goto_0

    .line 570
    :sswitch_data_0
    .sparse-switch
        -0xff9 -> :sswitch_0
        -0xfe6 -> :sswitch_1
        -0xfe5 -> :sswitch_2
        -0xfe4 -> :sswitch_3
        -0xfe3 -> :sswitch_4
        -0xfe2 -> :sswitch_5
        -0xfe1 -> :sswitch_6
        -0xfd7 -> :sswitch_7
        -0xfd6 -> :sswitch_8
    .end sparse-switch

    .line 575
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public writeImageContent(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 634
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Picture;->getContent()[B

    move-result-object v0

    .line 635
    .local v0, "content":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 637
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 639
    :cond_0
    return-void
.end method
