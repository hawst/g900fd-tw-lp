.class public final Lorg/apache/poi/hwpf/usermodel/BorderCode;
.super Ljava/lang/Object;
.source "BorderCode.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x4

.field private static final _brcType:Lorg/apache/poi/util/BitField;

.field private static final _dptLineWidth:Lorg/apache/poi/util/BitField;

.field private static final _dptSpace:Lorg/apache/poi/util/BitField;

.field private static final _fFrame:Lorg/apache/poi/util/BitField;

.field private static final _fShadow:Lorg/apache/poi/util/BitField;

.field private static final _ico:Lorg/apache/poi/util/BitField;


# instance fields
.field private _info:S

.field private _info2:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 34
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_dptLineWidth:Lorg/apache/poi/util/BitField;

    .line 35
    const v0, 0xff00

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_brcType:Lorg/apache/poi/util/BitField;

    .line 38
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_ico:Lorg/apache/poi/util/BitField;

    .line 39
    const/16 v0, 0x1f00

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_dptSpace:Lorg/apache/poi/util/BitField;

    .line 40
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_fShadow:Lorg/apache/poi/util/BitField;

    .line 41
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_fFrame:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    .line 50
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    .line 51
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 74
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 75
    check-cast v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 76
    .local v0, "brc":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 78
    .end local v0    # "brc":Lorg/apache/poi/hwpf/usermodel/BorderCode;
    :cond_0
    return v1
.end method

.method public getBorderType()I
    .locals 2

    .prologue
    .line 128
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_brcType:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getColor()S
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_ico:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getLineWidth()I
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_dptLineWidth:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getSpace()I
    .locals 2

    .prologue
    .line 171
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_dptSpace:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 68
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    if-nez v0, :cond_0

    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-eqz v0, :cond_1

    :cond_0
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isFrame()Z
    .locals 2

    .prologue
    .line 194
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_fFrame:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShadow()Z
    .locals 2

    .prologue
    .line 183
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_fShadow:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 55
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-static {p1, p2, v0}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 56
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 57
    return-void
.end method

.method public setBorderType(I)V
    .locals 2
    .param p1, "borderType"    # I

    .prologue
    .line 132
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_brcType:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    .line 133
    return-void
.end method

.method public setColor(S)V
    .locals 2
    .param p1, "color"    # S

    .prologue
    .line 160
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_ico:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    .line 161
    return-void
.end method

.method public setFrame(Z)V
    .locals 3
    .param p1, "frame"    # Z

    .prologue
    .line 198
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_fFrame:Lorg/apache/poi/util/BitField;

    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/util/BitField;->setValue(II)I

    .line 199
    return-void

    .line 198
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLineWidth(I)V
    .locals 2
    .param p1, "lineWidth"    # I

    .prologue
    .line 95
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_dptLineWidth:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    .line 96
    return-void
.end method

.method public setShadow(Z)V
    .locals 3
    .param p1, "shadow"    # Z

    .prologue
    .line 187
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_fShadow:Lorg/apache/poi/util/BitField;

    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/util/BitField;->setValue(II)I

    .line 188
    return-void

    .line 187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSpace(I)V
    .locals 2
    .param p1, "space"    # I

    .prologue
    .line 175
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_dptSpace:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    .line 176
    return-void
.end method

.method public toInt()I
    .locals 2

    .prologue
    .line 61
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 62
    .local v0, "buf":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 63
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    const-string/jumbo v1, "[BRC] EMPTY"

    .line 229
    :goto_0
    return-object v1

    .line 207
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 209
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[BRC]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    const-string/jumbo v1, "        .dptLineWidth         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 212
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getLineWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 214
    const-string/jumbo v1, "        .brcType              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 215
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    const-string/jumbo v1, "        .ico                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 218
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getColor()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 220
    const-string/jumbo v1, "        .dptSpace             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getSpace()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    const-string/jumbo v1, "        .fShadow              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 224
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->isShadow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 226
    const-string/jumbo v1, "        .fFrame               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 227
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->isFrame()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method
