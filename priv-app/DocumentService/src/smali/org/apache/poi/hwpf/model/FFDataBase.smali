.class public Lorg/apache/poi/hwpf/model/FFDataBase;
.super Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;
.source "FFDataBase.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;-><init>()V

    .line 43
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FFDataBaseAbstractType;-><init>()V

    .line 47
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FFDataBase;->fillFields([BI)V

    .line 48
    return-void
.end method
