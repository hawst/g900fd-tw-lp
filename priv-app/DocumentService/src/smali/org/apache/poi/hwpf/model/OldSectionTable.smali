.class public final Lorg/apache/poi/hwpf/model/OldSectionTable;
.super Lorg/apache/poi/hwpf/model/SectionTable;
.source "OldSectionTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>([BII)V
    .locals 16
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Lorg/apache/poi/hwpf/model/SectionTable;-><init>()V

    .line 49
    new-instance v9, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/16 v14, 0xc

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v9, v0, v1, v2, v14}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 51
    .local v9, "sedPlex":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v9}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v6

    .line 53
    .local v6, "length":I
    const/4 v13, 0x0

    .local v13, "x":I
    :goto_0
    if-lt v13, v6, :cond_0

    .line 84
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hwpf/model/OldSectionTable;->_sections:Ljava/util/ArrayList;

    sget-object v15, Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;

    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 85
    return-void

    .line 55
    :cond_0
    invoke-virtual {v9, v13}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v7

    .line 56
    .local v7, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    new-instance v8, Lorg/apache/poi/hwpf/model/SectionDescriptor;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v8, v14, v15}, Lorg/apache/poi/hwpf/model/SectionDescriptor;-><init>([BI)V

    .line 58
    .local v8, "sed":Lorg/apache/poi/hwpf/model/SectionDescriptor;
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/SectionDescriptor;->getFc()I

    move-result v5

    .line 59
    .local v5, "fileOffset":I
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v12

    .line 60
    .local v12, "startAt":I
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v4

    .line 64
    .local v4, "endAt":I
    const/4 v14, -0x1

    if-ne v5, v14, :cond_1

    .line 66
    new-instance v10, Lorg/apache/poi/hwpf/model/SEPX;

    const/4 v14, 0x0

    new-array v14, v14, [B

    invoke-direct {v10, v8, v12, v4, v14}, Lorg/apache/poi/hwpf/model/SEPX;-><init>(Lorg/apache/poi/hwpf/model/SectionDescriptor;II[B)V

    .line 82
    .local v10, "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hwpf/model/OldSectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 71
    .end local v10    # "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    :cond_1
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v11

    .line 76
    .local v11, "sepxSize":I
    add-int/lit8 v14, v11, 0x2

    new-array v3, v14, [B

    .line 77
    .local v3, "buf":[B
    add-int/lit8 v5, v5, 0x2

    .line 78
    const/4 v14, 0x0

    array-length v15, v3

    move-object/from16 v0, p1

    invoke-static {v0, v5, v3, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    new-instance v10, Lorg/apache/poi/hwpf/model/SEPX;

    invoke-direct {v10, v8, v12, v4, v3}, Lorg/apache/poi/hwpf/model/SEPX;-><init>(Lorg/apache/poi/hwpf/model/SectionDescriptor;II[B)V

    .restart local v10    # "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    goto :goto_1
.end method

.method public constructor <init>([BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 0
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/model/OldSectionTable;-><init>([BII)V

    .line 45
    return-void
.end method
