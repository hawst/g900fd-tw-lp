.class Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;
.super Ljava/lang/Object;
.source "OfficeDrawingsImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getOfficeDrawing(Lorg/apache/poi/hwpf/model/FSPA;)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

.field private final synthetic val$fspa:Lorg/apache/poi/hwpf/model/FSPA;


# direct methods
.method constructor <init>(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;Lorg/apache/poi/hwpf/model/FSPA;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    iput-object p2, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getTertiaryPropertyValue(II)I
    .locals 6
    .param p1, "propertyId"    # I
    .param p2, "defaultValue"    # I

    .prologue
    .line 278
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getShapeId()I

    move-result v5

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-static {v4, v5}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 279
    .local v2, "shapeDescription":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v2, :cond_1

    .line 293
    .end local p2    # "defaultValue":I
    :cond_0
    :goto_0
    return p2

    .line 283
    .restart local p2    # "defaultValue":I
    :cond_1
    const/16 v4, -0xede

    invoke-virtual {v2, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherTertiaryOptRecord;

    .line 284
    .local v1, "escherTertiaryOptRecord":Lorg/apache/poi/ddf/EscherTertiaryOptRecord;
    if-eqz v1, :cond_0

    .line 288
    invoke-virtual {v1, p1}, Lorg/apache/poi/ddf/EscherTertiaryOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 289
    .local v0, "escherProperty":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v0, :cond_0

    .line 291
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v3

    .local v3, "value":I
    move p2, v3

    .line 293
    goto :goto_0
.end method


# virtual methods
.method public getEscherBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 232
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getShapeId()I

    move-result v6

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-static {v5, v6}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    .line 233
    .local v3, "shapeDescription":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v3, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-object v4

    .line 237
    :cond_1
    const/16 v5, -0xff5

    invoke-virtual {v3, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 238
    .local v1, "escherOptRecord":Lorg/apache/poi/ddf/EscherOptRecord;
    if-eqz v1, :cond_0

    .line 242
    const/16 v5, 0x104

    invoke-virtual {v1, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 243
    .local v2, "escherProperty":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v2, :cond_0

    .line 246
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v0

    .line 247
    .local v0, "bitmapIndex":I
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getBitmapRecord(I)Lorg/apache/poi/ddf/EscherBlipRecord;
    invoke-static {v4, v0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$2(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v4

    goto :goto_0
.end method

.method public getHorizontalPositioning()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;
    .locals 3

    .prologue
    .line 155
    .line 156
    const/16 v1, 0x38f

    const/4 v2, -0x1

    .line 155
    invoke-direct {p0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getTertiaryPropertyValue(II)I

    move-result v0

    .line 158
    .local v0, "value":I
    packed-switch v0, :pswitch_data_0

    .line 174
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    :goto_0
    return-object v1

    .line 161
    :pswitch_0
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    goto :goto_0

    .line 163
    :pswitch_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->LEFT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    goto :goto_0

    .line 165
    :pswitch_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    goto :goto_0

    .line 167
    :pswitch_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->RIGHT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    goto :goto_0

    .line 169
    :pswitch_4
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    goto :goto_0

    .line 171
    :pswitch_5
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;->OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;

    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getHorizontalRelative()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;
    .locals 3

    .prologue
    .line 179
    .line 180
    const/16 v1, 0x390

    const/4 v2, -0x1

    .line 179
    invoke-direct {p0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getTertiaryPropertyValue(II)I

    move-result v0

    .line 182
    .local v0, "value":I
    add-int/lit8 v1, v0, 0x1

    packed-switch v1, :pswitch_data_0

    .line 194
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;->TEXT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;

    :goto_0
    return-object v1

    .line 185
    :pswitch_0
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;->MARGIN:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;

    goto :goto_0

    .line 187
    :pswitch_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;->PAGE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;

    goto :goto_0

    .line 189
    :pswitch_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;->TEXT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;

    goto :goto_0

    .line 191
    :pswitch_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;->CHAR:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getOfficeArtSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getShapeId()I

    move-result v1

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-static {v0, v1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    return-object v0
.end method

.method public getOfficeArtSpContainerList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getShapeId()I

    move-result v1

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherSpContainerList(I)Ljava/util/List;
    invoke-static {v0, v1}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$1(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPictureData()[B
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 208
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getShapeId()I

    move-result v7

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getEscherShapeRecordContainer(I)Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-static {v6, v7}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v4

    .line 209
    .local v4, "shapeDescription":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v4, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-object v5

    .line 213
    :cond_1
    const/16 v6, -0xff5

    invoke-virtual {v4, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 214
    .local v2, "escherOptRecord":Lorg/apache/poi/ddf/EscherOptRecord;
    if-eqz v2, :cond_0

    .line 218
    const/16 v6, 0x104

    invoke-virtual {v2, v6}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 219
    .local v3, "escherProperty":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v3, :cond_0

    .line 222
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v0

    .line 223
    .local v0, "bitmapIndex":I
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->this$0:Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;

    # invokes: Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->getBitmapRecord(I)Lorg/apache/poi/ddf/EscherBlipRecord;
    invoke-static {v6, v0}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;->access$2(Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl;I)Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v1

    .line 224
    .local v1, "escherBlipRecord":Lorg/apache/poi/ddf/EscherBlipRecord;
    if-eqz v1, :cond_0

    .line 227
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherBlipRecord;->getPicturedata()[B

    move-result-object v5

    goto :goto_0
.end method

.method public getRectangleBottom()I
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getYaBottom()I

    move-result v0

    return v0
.end method

.method public getRectangleLeft()I
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getXaLeft()I

    move-result v0

    return v0
.end method

.method public getRectangleRight()I
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getXaRight()I

    move-result v0

    return v0
.end method

.method public getRectangleTop()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getYaTop()I

    move-result v0

    return v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FSPA;->getSpid()I

    move-result v0

    return v0
.end method

.method public getVerticalPositioning()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;
    .locals 3

    .prologue
    .line 298
    .line 299
    const/16 v1, 0x391

    const/4 v2, -0x1

    .line 298
    invoke-direct {p0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getTertiaryPropertyValue(II)I

    move-result v0

    .line 301
    .local v0, "value":I
    packed-switch v0, :pswitch_data_0

    .line 317
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    :goto_0
    return-object v1

    .line 304
    :pswitch_0
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->ABSOLUTE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    goto :goto_0

    .line 306
    :pswitch_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->TOP:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    goto :goto_0

    .line 308
    :pswitch_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->CENTER:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    goto :goto_0

    .line 310
    :pswitch_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->BOTTOM:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    goto :goto_0

    .line 312
    :pswitch_4
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->INSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    goto :goto_0

    .line 314
    :pswitch_5
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;->OUTSIDE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalPositioning;

    goto :goto_0

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getVerticalRelativeElement()Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;
    .locals 3

    .prologue
    .line 322
    .line 323
    const/16 v1, 0x392

    const/4 v2, -0x1

    .line 322
    invoke-direct {p0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->getTertiaryPropertyValue(II)I

    move-result v0

    .line 325
    .local v0, "value":I
    add-int/lit8 v1, v0, 0x1

    packed-switch v1, :pswitch_data_0

    .line 337
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;->TEXT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;

    :goto_0
    return-object v1

    .line 328
    :pswitch_0
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;->MARGIN:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;

    goto :goto_0

    .line 330
    :pswitch_1
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;->PAGE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;

    goto :goto_0

    .line 332
    :pswitch_2
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;->TEXT:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;

    goto :goto_0

    .line 334
    :pswitch_3
    sget-object v1, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;->LINE:Lorg/apache/poi/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;

    goto :goto_0

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "OfficeDrawingImpl: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/OfficeDrawingsImpl$1;->val$fspa:Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/FSPA;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
