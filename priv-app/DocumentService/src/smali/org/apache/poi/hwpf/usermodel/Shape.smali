.class public final Lorg/apache/poi/hwpf/usermodel/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _bottom:I

.field _id:I

.field _inDoc:Z

.field _left:I

.field _right:I

.field _top:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V
    .locals 2
    .param p1, "nodo"    # Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v0

    .line 38
    .local v0, "contenuto":[B
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_id:I

    .line 39
    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_left:I

    .line 40
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_top:I

    .line 41
    const/16 v1, 0xc

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_right:I

    .line 42
    const/16 v1, 0x10

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_bottom:I

    .line 43
    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_left:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_right:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_top:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_bottom:I

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_inDoc:Z

    .line 45
    return-void

    .line 43
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_bottom:I

    return v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_bottom:I

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_top:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_id:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_left:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_top:I

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_right:I

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_left:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isWithinDocument()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/Shape;->_inDoc:Z

    return v0
.end method
