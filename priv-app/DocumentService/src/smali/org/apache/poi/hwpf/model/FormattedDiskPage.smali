.class public abstract Lorg/apache/poi/hwpf/model/FormattedDiskPage;
.super Ljava/lang/Object;
.source "FormattedDiskPage.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected _crun:I

.field protected _fkp:[B

.field protected _offset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    add-int/lit16 v0, p2, 0x1ff

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_crun:I

    .line 62
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_fkp:[B

    .line 63
    iput p2, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_offset:I

    .line 64
    return-void
.end method


# virtual methods
.method protected getEnd(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_fkp:[B

    iget v1, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_offset:I

    add-int/lit8 v2, p1, 0x1

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method protected abstract getGrpprl(I)[B
.end method

.method protected getStart(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_fkp:[B

    iget v1, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_offset:I

    mul-int/lit8 v2, p1, 0x4

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/poi/hwpf/model/FormattedDiskPage;->_crun:I

    return v0
.end method
