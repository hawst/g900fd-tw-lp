.class public abstract Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;
.super Ljava/lang/Object;
.source "HRESIAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final HRES_ADD_LETTER_BEFORE:B = 0x2t

.field public static final HRES_CHANGE_LETTER_AFTER:B = 0x5t

.field public static final HRES_CHANGE_LETTER_BEFORE:B = 0x3t

.field public static final HRES_DELETE_BEFORE_CHANGE_BEFORE:B = 0x6t

.field public static final HRES_DELETE_LETTER_BEFORE:B = 0x4t

.field public static final HRES_NO:B = 0x0t

.field public static final HRES_NORMAL:B = 0x1t


# instance fields
.field protected field_1_hres:B

.field protected field_2_chHres:B


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x6

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 54
    add-int/lit8 v0, p2, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_1_hres:B

    .line 55
    add-int/lit8 v0, p2, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_2_chHres:B

    .line 56
    return-void
.end method

.method public getChHres()B
    .locals 1

    .prologue
    .line 125
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_2_chHres:B

    return v0
.end method

.method public getHres()B
    .locals 1

    .prologue
    .line 99
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_1_hres:B

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 60
    add-int/lit8 v0, p2, 0x0

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_1_hres:B

    aput-byte v1, p1, v0

    .line 61
    add-int/lit8 v0, p2, 0x1

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_2_chHres:B

    aput-byte v1, p1, v0

    .line 62
    return-void
.end method

.method public setChHres(B)V
    .locals 0
    .param p1, "field_2_chHres"    # B

    .prologue
    .line 133
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_2_chHres:B

    .line 134
    return-void
.end method

.method public setHres(B)V
    .locals 0
    .param p1, "field_1_hres"    # B

    .prologue
    .line 117
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->field_1_hres:B

    .line 118
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[HRESI]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v1, "    .hres                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->getHres()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v1, "    .chHres               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/HRESIAbstractType;->getChHres()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string/jumbo v1, "[/HRESI]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
