.class public abstract Lorg/apache/poi/hwpf/converter/AbstractWordConverter;
.super Ljava/lang/Object;
.source "AbstractWordConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;,
        Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;
    }
.end annotation


# static fields
.field private static final BEL_MARK:B = 0x7t

.field private static final FIELD_BEGIN_MARK:B = 0x13t

.field private static final FIELD_END_MARK:B = 0x15t

.field private static final FIELD_SEPARATOR_MARK:B = 0x14t

.field private static final PATTERN_HYPERLINK_EXTERNAL:Ljava/util/regex/Pattern;

.field private static final PATTERN_HYPERLINK_LOCAL:Ljava/util/regex/Pattern;

.field private static final PATTERN_PAGEREF:Ljava/util/regex/Pattern;

.field private static final SPECCHAR_AUTONUMBERED_FOOTNOTE_REFERENCE:B = 0x2t

.field private static final SPECCHAR_DRAWN_OBJECT:B = 0x8t

.field protected static final UNICODECHAR_NONBREAKING_HYPHEN:C = '\u2011'

.field protected static final UNICODECHAR_NO_BREAK_SPACE:C = '\u00a0'

.field protected static final UNICODECHAR_ZERO_WIDTH_SPACE:C = '\u200b'

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final bookmarkStack:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

.field private log:Lorg/apache/poi/util/POILogger;

.field private numberingState:Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

.field private picturesManager:Lorg/apache/poi/hwpf/converter/PicturesManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    const-class v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 124
    sput-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 128
    const-string/jumbo v0, "^[ \\t\\r\\n]*HYPERLINK \"(.*)\".*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 127
    sput-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_HYPERLINK_EXTERNAL:Ljava/util/regex/Pattern;

    .line 131
    const-string/jumbo v0, "^[ \\t\\r\\n]*HYPERLINK \\\\l \"(.*)\"[ ](.*)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 130
    sput-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_HYPERLINK_LOCAL:Ljava/util/regex/Pattern;

    .line 134
    const-string/jumbo v0, "^[ \\t\\r\\n]*PAGEREF ([^ ]*)[ \\t\\r\\n]*\\\\h.*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 133
    sput-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_PAGEREF:Ljava/util/regex/Pattern;

    .line 144
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->bookmarkStack:Ljava/util/Set;

    .line 173
    new-instance v0, Lorg/apache/poi/hwpf/converter/DefaultFontReplacer;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/converter/DefaultFontReplacer;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

    .line 175
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->log:Lorg/apache/poi/util/POILogger;

    .line 177
    new-instance v0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->numberingState:Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

    .line 59
    return-void
.end method

.method private static addToStructures(Ljava/util/List;Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;)V
    .locals 4
    .param p1, "structure"    # Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;",
            ">;",
            "Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149
    .local p0, "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 150
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 168
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_1
    return-void

    .line 152
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    .line 154
    .local v0, "another":Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;
    iget v2, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v3, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-gt v2, v3, :cond_3

    .line 155
    iget v2, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    iget v3, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-ge v2, v3, :cond_1

    .line 160
    :cond_3
    iget v2, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v3, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-ge v2, v3, :cond_4

    iget v2, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v3, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    if-lt v2, v3, :cond_6

    .line 161
    :cond_4
    iget v2, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v3, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-ge v2, v3, :cond_5

    iget v2, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    iget v3, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    if-le v2, v3, :cond_6

    .line 162
    :cond_5
    iget v2, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v3, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-gt v2, v3, :cond_0

    iget v2, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    iget v3, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    if-ge v2, v3, :cond_0

    .line 164
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method private processOle2(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)Z
    .locals 9
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "block"    # Lorg/w3c/dom/Element;

    .prologue
    const/4 v1, 0x5

    const/4 v8, 0x0

    .line 1016
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getObjectsPool()Lorg/apache/poi/hwpf/usermodel/ObjectsPool;

    move-result-object v0

    .line 1017
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1016
    invoke-interface {v0, v2}, Lorg/apache/poi/hwpf/usermodel/ObjectsPool;->getObjectById(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v7

    .line 1018
    .local v7, "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    if-nez v7, :cond_0

    .line 1020
    sget-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v2, "Referenced OLE2 object \'"

    .line 1021
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1022
    const-string/jumbo v4, "\' not found in ObjectPool"

    .line 1020
    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v8

    .line 1036
    :goto_0
    return v0

    .line 1028
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, p3, v7}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processOle2(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/w3c/dom/Element;Lorg/apache/poi/poifs/filesystem/Entry;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1030
    :catch_0
    move-exception v5

    .line 1032
    .local v5, "exc":Ljava/lang/Exception;
    sget-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 1033
    const-string/jumbo v2, "Unable to convert internal OLE2 object \'"

    .line 1034
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getPicOffset()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string/jumbo v4, "\': "

    move-object v6, v5

    .line 1032
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    move v0, v8

    .line 1036
    goto :goto_0
.end method

.method private tryDeadField_lookupFieldSeparatorEnd(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;I)[I
    .locals 9
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p3, "beginMark"    # I

    .prologue
    .line 1168
    const/4 v5, -0x1

    .line 1169
    .local v5, "separatorMark":I
    const/4 v2, -0x1

    .line 1170
    .local v2, "endMark":I
    add-int/lit8 v0, p3, 0x1

    .local v0, "c":I
    :goto_0
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v7

    if-lt v0, v7, :cond_1

    .line 1215
    :goto_1
    const/4 v7, -0x1

    if-eq v5, v7, :cond_0

    const/4 v7, -0x1

    if-ne v2, v7, :cond_8

    .line 1216
    :cond_0
    const/4 v7, 0x0

    .line 1218
    :goto_2
    return-object v7

    .line 1172
    :cond_1
    invoke-virtual {p2, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v1

    .line 1174
    .local v1, "characterRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v6

    .line 1175
    .local v6, "text":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    array-length v7, v7

    if-nez v7, :cond_3

    .line 1170
    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1178
    :cond_3
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    const/4 v8, 0x0

    aget-byte v3, v7, v8

    .line 1179
    .local v3, "firstByte":B
    const/16 v7, 0x13

    if-ne v3, v7, :cond_4

    .line 1181
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->tryDeadField_lookupFieldSeparatorEnd(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;I)[I

    move-result-object v4

    .line 1183
    .local v4, "nested":[I
    if-eqz v4, :cond_2

    .line 1185
    const/4 v7, 0x1

    aget v0, v4, v7

    .line 1187
    goto :goto_3

    .line 1190
    .end local v4    # "nested":[I
    :cond_4
    const/16 v7, 0x14

    if-ne v3, v7, :cond_6

    .line 1192
    const/4 v7, -0x1

    if-eq v5, v7, :cond_5

    .line 1195
    const/4 v7, 0x0

    goto :goto_2

    .line 1198
    :cond_5
    move v5, v0

    .line 1199
    goto :goto_3

    .line 1202
    :cond_6
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    const/4 v8, 0x0

    aget-byte v7, v7, v8

    const/16 v8, 0x15

    if-ne v7, v8, :cond_2

    .line 1204
    const/4 v7, -0x1

    if-eq v2, v7, :cond_7

    .line 1207
    const/4 v7, 0x0

    goto :goto_2

    .line 1210
    :cond_7
    move v2, v0

    .line 1211
    goto :goto_1

    .line 1218
    .end local v1    # "characterRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .end local v3    # "firstByte":B
    .end local v6    # "text":Ljava/lang/String;
    :cond_8
    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v5, v7, v8

    const/4 v8, 0x1

    aput v2, v7, v8

    goto :goto_2
.end method


# virtual methods
.method protected afterProcess()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method protected getCharacterRunTriplet(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    .locals 3
    .param p1, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    .line 193
    new-instance v0, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;-><init>()V

    .line 194
    .local v0, "original":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isBold()Z

    move-result v2

    iput-boolean v2, v0, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    .line 195
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isItalic()Z

    move-result v2

    iput-boolean v2, v0, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 196
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 197
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->getFontReplacer()Lorg/apache/poi/hwpf/converter/FontReplacer;

    move-result-object v2

    invoke-interface {v2, v0}, Lorg/apache/poi/hwpf/converter/FontReplacer;->update(Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    move-result-object v1

    .line 198
    .local v1, "updated":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    return-object v1
.end method

.method public abstract getDocument()Lorg/w3c/dom/Document;
.end method

.method public getFontReplacer()Lorg/apache/poi/hwpf/converter/FontReplacer;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

    return-object v0
.end method

.method protected getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I
    .locals 5
    .param p1, "tableCellEdges"    # [I
    .param p2, "currentEdgeIndex"    # I
    .param p3, "tableCell"    # Lorg/apache/poi/hwpf/usermodel/TableCell;

    .prologue
    .line 211
    move v2, p2

    .line 212
    .local v2, "nextEdgeIndex":I
    const/4 v1, 0x0

    .line 213
    .local v1, "colSpan":I
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getLeftEdge()I

    move-result v3

    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v4

    add-int v0, v3, v4

    .line 214
    .local v0, "cellRightEdge":I
    :goto_0
    aget v3, p1, v2

    if-lt v3, v0, :cond_0

    .line 219
    return v1

    .line 216
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 217
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected getNumberRowsSpanned(Lorg/apache/poi/hwpf/usermodel/Table;[IIILorg/apache/poi/hwpf/usermodel/TableCell;)I
    .locals 11
    .param p1, "table"    # Lorg/apache/poi/hwpf/usermodel/Table;
    .param p2, "tableCellEdges"    # [I
    .param p3, "currentRowIndex"    # I
    .param p4, "currentColumnIndex"    # I
    .param p5, "tableCell"    # Lorg/apache/poi/hwpf/usermodel/TableCell;

    .prologue
    .line 226
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isFirstVerticallyMerged()Z

    move-result v10

    if-nez v10, :cond_1

    .line 227
    const/4 v2, 0x1

    .line 272
    :cond_0
    return v2

    .line 229
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Table;->numRows()I

    move-result v8

    .line 231
    .local v8, "numRows":I
    const/4 v2, 0x1

    .line 232
    .local v2, "count":I
    add-int/lit8 v9, p3, 0x1

    .local v9, "r1":I
    :goto_0
    if-ge v9, v8, :cond_0

    .line 234
    invoke-virtual {p1, v9}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v6

    .line 235
    .local v6, "nextRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v10

    if-ge p4, v10, :cond_0

    .line 239
    const/4 v4, 0x0

    .line 240
    .local v4, "hasCells":Z
    const/4 v3, 0x0

    .line 241
    .local v3, "currentEdgeIndex":I
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_1
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v10

    if-lt v0, v10, :cond_2

    .line 263
    :goto_2
    if-nez v4, :cond_6

    .line 232
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 243
    :cond_2
    invoke-virtual {v6, v0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v7

    .line 244
    .local v7, "nextTableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isVerticallyMerged()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 245
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isFirstVerticallyMerged()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 247
    :cond_3
    invoke-virtual {p0, p2, v3, v7}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v1

    .line 249
    .local v1, "colSpan":I
    add-int/2addr v3, v1

    .line 251
    if-eqz v1, :cond_5

    .line 253
    const/4 v4, 0x1

    .line 254
    goto :goto_2

    .line 260
    .end local v1    # "colSpan":I
    :cond_4
    invoke-virtual {p0, p2, v3, v7}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v10

    add-int/2addr v3, v10

    .line 241
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 266
    .end local v7    # "nextTableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    :cond_6
    invoke-virtual {v6, p4}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v5

    .line 267
    .local v5, "nextCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isVerticallyMerged()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 268
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isFirstVerticallyMerged()Z

    move-result v10

    if-nez v10, :cond_0

    .line 270
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public getPicturesManager()Lorg/apache/poi/hwpf/converter/PicturesManager;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->picturesManager:Lorg/apache/poi/hwpf/converter/PicturesManager;

    return-object v0
.end method

.method protected abstract outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V
.end method

.method protected abstract processBookmarks(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/HWPFDocumentCore;",
            "Lorg/w3c/dom/Element;",
            "Lorg/apache/poi/hwpf/usermodel/Range;",
            "I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;)V"
        }
    .end annotation
.end method

.method protected processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z
    .locals 47
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentTableLevel"    # I
    .param p3, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 295
    if-nez p3, :cond_1

    .line 296
    const/16 v33, 0x0

    .line 632
    :cond_0
    :goto_0
    return v33

    .line 298
    :cond_1
    const/16 v33, 0x0

    .line 306
    .local v33, "haveAnyText":Z
    new-instance v44, Ljava/util/LinkedList;

    invoke-direct/range {v44 .. v44}, Ljava/util/LinkedList;-><init>()V

    .line 307
    .local v44, "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_4

    move-object/from16 v31, p1

    .line 309
    check-cast v31, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 311
    .local v31, "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/hwpf/HWPFDocument;->getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    move-result-object v4

    .line 312
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v5

    .line 313
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v6

    .line 312
    invoke-interface {v4, v5, v6}, Lorg/apache/poi/hwpf/usermodel/Bookmarks;->getBookmarksStartedBetween(II)Ljava/util/Map;

    move-result-object v38

    .line 315
    .local v38, "rangeBookmarks":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;>;"
    if-eqz v38, :cond_3

    .line 317
    invoke-interface/range {v38 .. v38}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 329
    :cond_3
    const/16 v41, -0x1

    .line 330
    .local v41, "skipUntil":I
    const/16 v24, 0x0

    .local v24, "c":I
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v4

    move/from16 v0, v24

    if-lt v0, v4, :cond_7

    .line 369
    .end local v24    # "c":I
    .end local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    .end local v38    # "rangeBookmarks":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;>;"
    .end local v41    # "skipUntil":I
    :cond_4
    new-instance v45, Ljava/util/ArrayList;

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 370
    .end local v44    # "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    .local v45, "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    invoke-static/range {v45 .. v45}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 372
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v37

    .line 373
    .local v37, "previous":I
    invoke-interface/range {v45 .. v45}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_c

    .line 449
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v4

    move/from16 v0, v37

    if-eq v0, v4, :cond_15

    .line 451
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v4

    move/from16 v0, v37

    if-le v0, v4, :cond_13

    .line 453
    sget-object v15, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/16 v16, 0x5

    const-string/jumbo v17, "Latest structure in "

    .line 454
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " ended at #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string/jumbo v20, " after range boundaries ["

    .line 455
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 456
    const-string/jumbo v22, ")"

    move-object/from16 v18, p3

    .line 453
    invoke-virtual/range {v15 .. v22}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 457
    const/16 v33, 0x1

    goto/16 :goto_0

    .line 317
    .end local v37    # "previous":I
    .end local v45    # "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    .restart local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    .restart local v38    # "rangeBookmarks":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;>;"
    .restart local v44    # "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/util/List;

    .line 319
    .local v34, "lists":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    invoke-interface/range {v34 .. v34}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/poi/hwpf/usermodel/Bookmark;

    .line 321
    .local v26, "bookmark":Lorg/apache/poi/hwpf/usermodel/Bookmark;
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->bookmarkStack:Ljava/util/Set;

    move-object/from16 v0, v26

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 322
    new-instance v6, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    .line 323
    move-object/from16 v0, v26

    invoke-direct {v6, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;-><init>(Lorg/apache/poi/hwpf/usermodel/Bookmark;)V

    .line 322
    move-object/from16 v0, v44

    invoke-static {v0, v6}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->addToStructures(Ljava/util/List;Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;)V

    goto :goto_3

    .line 332
    .end local v26    # "bookmark":Lorg/apache/poi/hwpf/usermodel/Bookmark;
    .end local v34    # "lists":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    .restart local v24    # "c":I
    .restart local v41    # "skipUntil":I
    :cond_7
    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v29

    .line 333
    .local v29, "characterRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    if-nez v29, :cond_8

    .line 334
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 335
    :cond_8
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v4

    move/from16 v0, v41

    if-ge v4, v0, :cond_a

    .line 330
    :cond_9
    :goto_4
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_1

    .line 337
    :cond_a
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v46

    .line 338
    .local v46, "text":Ljava/lang/String;
    if-eqz v46, :cond_9

    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    .line 339
    const/4 v4, 0x0

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x13

    if-ne v4, v5, :cond_9

    move-object/from16 v4, p1

    .line 342
    check-cast v4, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocument;->getFields()Lorg/apache/poi/hwpf/usermodel/Fields;

    move-result-object v4

    .line 343
    sget-object v5, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 344
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v6

    .line 343
    invoke-interface {v4, v5, v6}, Lorg/apache/poi/hwpf/usermodel/Fields;->getFieldByStartOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)Lorg/apache/poi/hwpf/usermodel/Field;

    move-result-object v19

    .line 345
    .local v19, "aliveField":Lorg/apache/poi/hwpf/usermodel/Field;
    if-eqz v19, :cond_b

    .line 347
    new-instance v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;-><init>(Lorg/apache/poi/hwpf/usermodel/Field;)V

    move-object/from16 v0, v44

    invoke-static {v0, v4}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->addToStructures(Ljava/util/List;Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;)V

    goto :goto_4

    .line 351
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->tryDeadField_lookupFieldSeparatorEnd(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;I)[I

    move-result-object v39

    .line 353
    .local v39, "separatorEnd":[I
    if-eqz v39, :cond_9

    .line 357
    new-instance v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    new-instance v5, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;

    .line 358
    const/4 v6, 0x0

    aget v6, v39, v6

    const/4 v8, 0x1

    aget v8, v39, v8

    move/from16 v0, v24

    invoke-direct {v5, v0, v6, v8}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;-><init>(III)V

    .line 359
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v6

    .line 361
    const/4 v8, 0x1

    aget v8, v39, v8

    .line 360
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v8

    .line 362
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getEndOffset()I

    move-result v8

    .line 357
    invoke-direct {v4, v5, v6, v8}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;II)V

    .line 355
    move-object/from16 v0, v44

    invoke-static {v0, v4}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->addToStructures(Ljava/util/List;Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;)V

    .line 363
    const/4 v4, 0x1

    aget v24, v39, v4

    goto :goto_4

    .line 373
    .end local v19    # "aliveField":Lorg/apache/poi/hwpf/usermodel/Field;
    .end local v24    # "c":I
    .end local v29    # "characterRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .end local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    .end local v38    # "rangeBookmarks":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;>;"
    .end local v39    # "separatorEnd":[I
    .end local v41    # "skipUntil":I
    .end local v44    # "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    .end local v46    # "text":Ljava/lang/String;
    .restart local v37    # "previous":I
    .restart local v45    # "structures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;>;"
    :cond_c
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    .line 375
    .local v43, "structure":Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;
    move-object/from16 v0, v43

    iget v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    move/from16 v0, v37

    if-eq v4, v0, :cond_d

    .line 377
    new-instance v7, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$1;

    move-object/from16 v0, v43

    iget v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, p3

    invoke-direct {v7, v0, v1, v4, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$1;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 385
    .local v7, "subrange":Lorg/apache/poi/hwpf/usermodel/Range;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v7, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 389
    .end local v7    # "subrange":Lorg/apache/poi/hwpf/usermodel/Range;
    :cond_d
    move-object/from16 v0, v43

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    instance-of v4, v4, Lorg/apache/poi/hwpf/usermodel/Bookmark;

    if-eqz v4, :cond_10

    .line 392
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .local v9, "bookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    move-object/from16 v4, p1

    .line 393
    check-cast v4, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 394
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocument;->getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    move-result-object v4

    .line 395
    move-object/from16 v0, v43

    iget v5, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    .line 396
    move-object/from16 v0, v43

    iget v6, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    add-int/lit8 v6, v6, 0x1

    .line 395
    invoke-interface {v4, v5, v6}, Lorg/apache/poi/hwpf/usermodel/Bookmarks;->getBookmarksStartedBetween(II)Ljava/util/Map;

    move-result-object v4

    .line 396
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 397
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 393
    :cond_e
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_f

    .line 406
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->bookmarkStack:Ljava/util/Set;

    invoke-interface {v4, v9}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 409
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v4

    move-object/from16 v0, v43

    iget v5, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v32

    .line 410
    .local v32, "end":I
    new-instance v7, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$2;

    move-object/from16 v0, v43

    iget v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    move-object/from16 v0, p0

    move/from16 v1, v32

    move-object/from16 v2, p3

    invoke-direct {v7, v0, v4, v1, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$2;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .restart local v7    # "subrange":Lorg/apache/poi/hwpf/usermodel/Range;
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p4

    move/from16 v8, p2

    .line 419
    invoke-virtual/range {v4 .. v9}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processBookmarks(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->bookmarkStack:Ljava/util/Set;

    invoke-interface {v4, v9}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 446
    .end local v7    # "subrange":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v9    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    .end local v32    # "end":I
    :goto_6
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v4

    move-object/from16 v0, v43

    iget v5, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v37

    goto/16 :goto_2

    .line 397
    .restart local v9    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    :cond_f
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/poi/hwpf/usermodel/Bookmark;

    .line 399
    .restart local v26    # "bookmark":Lorg/apache/poi/hwpf/usermodel/Bookmark;
    invoke-interface/range {v26 .. v26}, Lorg/apache/poi/hwpf/usermodel/Bookmark;->getStart()I

    move-result v5

    move-object/from16 v0, v43

    iget v6, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-ne v5, v6, :cond_e

    .line 400
    invoke-interface/range {v26 .. v26}, Lorg/apache/poi/hwpf/usermodel/Bookmark;->getEnd()I

    move-result v5

    move-object/from16 v0, v43

    iget v6, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    if-ne v5, v6, :cond_e

    .line 402
    move-object/from16 v0, v26

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 423
    .end local v26    # "bookmark":Lorg/apache/poi/hwpf/usermodel/Bookmark;
    :catchall_0
    move-exception v4

    .line 424
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->bookmarkStack:Ljava/util/Set;

    invoke-interface {v5, v9}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 425
    throw v4

    .line 427
    .end local v9    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    :cond_10
    move-object/from16 v0, v43

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    instance-of v4, v4, Lorg/apache/poi/hwpf/usermodel/Field;

    if-eqz v4, :cond_11

    .line 429
    move-object/from16 v0, v43

    iget-object v14, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    check-cast v14, Lorg/apache/poi/hwpf/usermodel/Field;

    .local v14, "field":Lorg/apache/poi/hwpf/usermodel/Field;
    move-object/from16 v11, p1

    .line 430
    check-cast v11, Lorg/apache/poi/hwpf/HWPFDocument;

    move-object/from16 v10, p0

    move-object/from16 v12, p3

    move/from16 v13, p2

    move-object/from16 v15, p4

    invoke-virtual/range {v10 .. v15}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processField(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/Range;ILorg/apache/poi/hwpf/usermodel/Field;Lorg/w3c/dom/Element;)V

    goto :goto_6

    .line 433
    .end local v14    # "field":Lorg/apache/poi/hwpf/usermodel/Field;
    :cond_11
    move-object/from16 v0, v43

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    instance-of v4, v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;

    if-eqz v4, :cond_12

    .line 435
    move-object/from16 v0, v43

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    move-object/from16 v27, v0

    check-cast v27, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;

    .line 437
    .local v27, "boundaries":Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;
    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;->beginMark:I

    move/from16 v20, v0

    .line 438
    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;->separatorMark:I

    move/from16 v21, v0

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;->endMark:I

    move/from16 v22, v0

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v17, p4

    move-object/from16 v18, p3

    move/from16 v19, p2

    .line 436
    invoke-virtual/range {v15 .. v22}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDeadField(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;IIII)V

    goto/16 :goto_6

    .line 442
    .end local v27    # "boundaries":Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;
    :cond_12
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "NYI: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 443
    move-object/from16 v0, v43

    iget-object v6, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 442
    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 460
    .end local v43    # "structure":Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;
    :cond_13
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v4

    move/from16 v0, v37

    if-ge v0, v4, :cond_14

    .line 462
    new-instance v7, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$3;

    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v37

    move-object/from16 v2, p3

    invoke-direct {v7, v0, v1, v4, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$3;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 471
    .restart local v7    # "subrange":Lorg/apache/poi/hwpf/usermodel/Range;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v7, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 474
    .end local v7    # "subrange":Lorg/apache/poi/hwpf/usermodel/Range;
    :cond_14
    const/16 v33, 0x1

    goto/16 :goto_0

    .line 477
    :cond_15
    const/16 v24, 0x0

    .restart local v24    # "c":I
    :goto_7
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v4

    move/from16 v0, v24

    if-ge v0, v4, :cond_0

    .line 479
    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v29

    .line 481
    .restart local v29    # "characterRun":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    if-nez v29, :cond_16

    .line 482
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 484
    :cond_16
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_19

    move-object/from16 v4, p1

    .line 485
    check-cast v4, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocument;->getPicturesTable()Lorg/apache/poi/hwpf/model/PicturesTable;

    move-result-object v4

    .line 486
    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Lorg/apache/poi/hwpf/model/PicturesTable;->hasPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Z

    move-result v4

    if-eqz v4, :cond_19

    move-object/from16 v35, p1

    .line 488
    check-cast v35, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 489
    .local v35, "newFormat":Lorg/apache/poi/hwpf/HWPFDocument;
    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hwpf/HWPFDocument;->getPicturesTable()Lorg/apache/poi/hwpf/model/PicturesTable;

    move-result-object v4

    .line 490
    const/4 v5, 0x1

    .line 489
    move-object/from16 v0, v29

    invoke-virtual {v4, v0, v5}, Lorg/apache/poi/hwpf/model/PicturesTable;->extractPicture(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Z)Lorg/apache/poi/hwpf/usermodel/Picture;

    move-result-object v36

    .line 492
    .local v36, "picture":Lorg/apache/poi/hwpf/usermodel/Picture;
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_18

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v4, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V

    .line 477
    .end local v35    # "newFormat":Lorg/apache/poi/hwpf/HWPFDocument;
    .end local v36    # "picture":Lorg/apache/poi/hwpf/usermodel/Picture;
    :cond_17
    :goto_9
    add-int/lit8 v24, v24, 0x1

    goto :goto_7

    .line 492
    .restart local v35    # "newFormat":Lorg/apache/poi/hwpf/HWPFDocument;
    .restart local v36    # "picture":Lorg/apache/poi/hwpf/usermodel/Picture;
    :cond_18
    const/4 v4, 0x0

    goto :goto_8

    .line 497
    .end local v35    # "newFormat":Lorg/apache/poi/hwpf/HWPFDocument;
    .end local v36    # "picture":Lorg/apache/poi/hwpf/usermodel/Picture;
    :cond_19
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    move-result-object v46

    .line 498
    .restart local v46    # "text":Ljava/lang/String;
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    if-eqz v4, :cond_17

    .line 501
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 503
    const/4 v4, 0x0

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1a

    .line 504
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_1a

    move-object/from16 v31, p1

    .line 506
    check-cast v31, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 507
    .restart local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processNoteAnchor(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V

    goto :goto_9

    .line 510
    .end local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    :cond_1a
    const/4 v4, 0x0

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1b

    .line 511
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_1b

    move-object/from16 v31, p1

    .line 513
    check-cast v31, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 514
    .restart local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V

    goto :goto_9

    .line 517
    .end local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    :cond_1b
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isOle2()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 518
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_1c

    move-object/from16 v31, p1

    .line 520
    check-cast v31, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 521
    .restart local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processOle2(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)Z

    goto :goto_9

    .line 524
    .end local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    :cond_1c
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSymbol()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 525
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_1d

    move-object/from16 v31, p1

    .line 527
    check-cast v31, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 528
    .restart local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processSymbol(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V

    goto/16 :goto_9

    .line 533
    .end local v31    # "doc":Lorg/apache/poi/hwpf/HWPFDocument;
    :cond_1d
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    aget-byte v4, v4, v5

    const/16 v5, 0x13

    if-ne v4, v5, :cond_21

    .line 535
    move-object/from16 v0, p1

    instance-of v4, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v4, :cond_20

    move-object/from16 v4, p1

    .line 537
    check-cast v4, Lorg/apache/poi/hwpf/HWPFDocument;

    .line 538
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocument;->getFields()Lorg/apache/poi/hwpf/usermodel/Fields;

    move-result-object v4

    .line 539
    sget-object v5, Lorg/apache/poi/hwpf/model/FieldsDocumentPart;->MAIN:Lorg/apache/poi/hwpf/model/FieldsDocumentPart;

    .line 540
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v6

    .line 538
    invoke-interface {v4, v5, v6}, Lorg/apache/poi/hwpf/usermodel/Fields;->getFieldByStartOffset(Lorg/apache/poi/hwpf/model/FieldsDocumentPart;I)Lorg/apache/poi/hwpf/usermodel/Field;

    move-result-object v19

    .line 541
    .restart local v19    # "aliveField":Lorg/apache/poi/hwpf/usermodel/Field;
    if-eqz v19, :cond_20

    move-object/from16 v16, p1

    .line 543
    check-cast v16, Lorg/apache/poi/hwpf/HWPFDocument;

    move-object/from16 v15, p0

    move-object/from16 v17, p3

    move/from16 v18, p2

    move-object/from16 v20, p4

    invoke-virtual/range {v15 .. v20}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processField(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/Range;ILorg/apache/poi/hwpf/usermodel/Field;Lorg/w3c/dom/Element;)V

    .line 546
    invoke-interface/range {v19 .. v19}, Lorg/apache/poi/hwpf/usermodel/Field;->getFieldEndOffset()I

    move-result v30

    .line 547
    .local v30, "continueAfter":I
    :goto_a
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v4

    move/from16 v0, v24

    if-ge v0, v4, :cond_1e

    .line 548
    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getEndOffset()I

    move-result v4

    .line 547
    move/from16 v0, v30

    if-le v4, v0, :cond_1f

    .line 551
    :cond_1e
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v4

    move/from16 v0, v24

    if-ge v0, v4, :cond_17

    .line 552
    add-int/lit8 v24, v24, -0x1

    .line 554
    goto/16 :goto_9

    .line 549
    :cond_1f
    add-int/lit8 v24, v24, 0x1

    goto :goto_a

    .end local v19    # "aliveField":Lorg/apache/poi/hwpf/usermodel/Field;
    .end local v30    # "continueAfter":I
    :cond_20
    move-object/from16 v20, p0

    move-object/from16 v21, p1

    move-object/from16 v22, p3

    move/from16 v23, p2

    move-object/from16 v25, p4

    .line 558
    invoke-virtual/range {v20 .. v25}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->tryDeadField(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;IILorg/w3c/dom/Element;)I

    move-result v40

    .line 561
    .local v40, "skipTo":I
    move/from16 v0, v40

    move/from16 v1, v24

    if-eq v0, v1, :cond_17

    .line 563
    move/from16 v24, v40

    .line 564
    goto/16 :goto_9

    .line 569
    .end local v40    # "skipTo":I
    :cond_21
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    aget-byte v4, v4, v5

    const/16 v5, 0x14

    if-eq v4, v5, :cond_17

    .line 574
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    aget-byte v4, v4, v5

    const/16 v5, 0x15

    if-eq v4, v5, :cond_17

    .line 580
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    move-result v4

    if-nez v4, :cond_17

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isObj()Z

    move-result v4

    if-nez v4, :cond_17

    .line 581
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isOle2()Z

    move-result v4

    if-nez v4, :cond_17

    .line 586
    const-string/jumbo v4, "\r"

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_22

    .line 587
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x7

    if-ne v4, v5, :cond_23

    const/high16 v4, -0x80000000

    move/from16 v0, p2

    if-eq v0, v4, :cond_23

    .line 588
    :cond_22
    const/4 v4, 0x0

    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v46

    .line 592
    :cond_23
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    .line 593
    .local v42, "stringBuilder":Ljava/lang/StringBuilder;
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_b
    if-lt v4, v6, :cond_25

    .line 621
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_24

    .line 624
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 623
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v4}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V

    .line 625
    const/4 v4, 0x0

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 629
    :cond_24
    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2c

    const/4 v4, 0x1

    :goto_c
    or-int v33, v33, v4

    goto/16 :goto_9

    .line 593
    :cond_25
    aget-char v28, v5, v4

    .line 595
    .local v28, "charChar":C
    const/16 v8, 0xb

    move/from16 v0, v28

    if-ne v0, v8, :cond_28

    .line 597
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_26

    .line 600
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 599
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v8}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V

    .line 601
    const/4 v8, 0x0

    move-object/from16 v0, v42

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 603
    :cond_26
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processLineBreak(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;)V

    .line 593
    :cond_27
    :goto_d
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 605
    :cond_28
    const/16 v8, 0x1e

    move/from16 v0, v28

    if-ne v0, v8, :cond_29

    .line 608
    const/16 v8, 0x2011

    move-object/from16 v0, v42

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_d

    .line 610
    :cond_29
    const/16 v8, 0x1f

    move/from16 v0, v28

    if-ne v0, v8, :cond_2a

    .line 613
    const/16 v8, 0x200b

    move-object/from16 v0, v42

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_d

    .line 615
    :cond_2a
    const/16 v8, 0x20

    move/from16 v0, v28

    if-ge v0, v8, :cond_2b

    const/16 v8, 0x9

    move/from16 v0, v28

    if-eq v0, v8, :cond_2b

    .line 616
    const/16 v8, 0xa

    move/from16 v0, v28

    if-eq v0, v8, :cond_2b

    const/16 v8, 0xd

    move/from16 v0, v28

    if-ne v0, v8, :cond_27

    .line 618
    :cond_2b
    move-object/from16 v0, v42

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_d

    .line 629
    .end local v28    # "charChar":C
    :cond_2c
    const/4 v4, 0x0

    goto :goto_c
.end method

.method protected processDeadField(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;IIII)V
    .locals 14
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "beginMark"    # I
    .param p6, "separatorMark"    # I
    .param p7, "endMark"    # I

    .prologue
    .line 639
    add-int/lit8 v2, p5, 0x1

    move/from16 v0, p6

    if-ge v2, v0, :cond_1

    add-int/lit8 v2, p6, 0x1

    move/from16 v0, p7

    if-ge v2, v0, :cond_1

    .line 641
    new-instance v11, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$4;

    .line 642
    add-int/lit8 v2, p5, 0x1

    .line 641
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v2

    .line 642
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v2

    .line 643
    add-int/lit8 v3, p6, -0x1

    .line 642
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v3

    .line 643
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getEndOffset()I

    move-result v3

    .line 641
    move-object/from16 v0, p3

    invoke-direct {v11, p0, v2, v3, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$4;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 651
    .local v11, "formulaRange":Lorg/apache/poi/hwpf/usermodel/Range;
    new-instance v5, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$5;

    .line 652
    add-int/lit8 v2, p6, 0x1

    .line 651
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v2

    .line 652
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v2

    .line 653
    add-int/lit8 v3, p7, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getEndOffset()I

    move-result v3

    .line 651
    move-object/from16 v0, p3

    invoke-direct {v5, p0, v2, v3, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$5;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 661
    .local v5, "valueRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/Range;->text()Ljava/lang/String;

    move-result-object v10

    .line 662
    .local v10, "formula":Ljava/lang/String;
    sget-object v2, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_HYPERLINK_LOCAL:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 663
    .local v13, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 665
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    .local v7, "localref":Ljava/lang/String;
    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move/from16 v6, p4

    .line 666
    invoke-virtual/range {v2 .. v7}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V

    .line 697
    .end local v5    # "valueRange":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v7    # "localref":Ljava/lang/String;
    .end local v10    # "formula":Ljava/lang/String;
    .end local v11    # "formulaRange":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v13    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    :goto_0
    return-void

    .line 672
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unsupported field type: \n"

    invoke-direct {v9, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 673
    .local v9, "debug":Ljava/lang/StringBuilder;
    move/from16 v12, p5

    .local v12, "i":I
    :goto_1
    move/from16 v0, p7

    if-le v12, v0, :cond_2

    .line 679
    sget-object v2, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 681
    new-instance v8, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$6;

    move-object/from16 v0, p3

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v2

    .line 682
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    move/from16 v1, p7

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v3

    .line 683
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v3

    .line 681
    move-object/from16 v0, p3

    invoke-direct {v8, p0, v2, v3, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$6;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 693
    .local v8, "deadFieldValueSubrage":Lorg/apache/poi/hwpf/usermodel/Range;
    add-int/lit8 v2, p6, 0x1

    move/from16 v0, p7

    if-ge v2, v0, :cond_0

    .line 694
    move/from16 v0, p4

    move-object/from16 v1, p2

    invoke-virtual {p0, p1, v0, v8, v1}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    goto :goto_0

    .line 675
    .end local v8    # "deadFieldValueSubrage":Lorg/apache/poi/hwpf/usermodel/Range;
    :cond_2
    const-string/jumbo v2, "\t"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 676
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 677
    const-string/jumbo v2, "\n"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673
    add-int/lit8 v12, v12, 0x1

    goto :goto_1
.end method

.method public processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V
    .locals 6
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .prologue
    .line 704
    .line 705
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getSummaryInformation()Lorg/apache/poi/hpsf/SummaryInformation;

    move-result-object v2

    .line 706
    .local v2, "summaryInformation":Lorg/apache/poi/hpsf/SummaryInformation;
    if-eqz v2, :cond_0

    .line 708
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 718
    .end local v2    # "summaryInformation":Lorg/apache/poi/hpsf/SummaryInformation;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    .line 720
    .local v0, "docRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/Range;->numSections()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 722
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->getSection(I)Lorg/apache/poi/hwpf/usermodel/Section;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processSingleSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;)V

    .line 723
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->afterProcess()V

    .line 729
    :goto_1
    return-void

    .line 711
    .end local v0    # "docRange":Lorg/apache/poi/hwpf/usermodel/Range;
    :catch_0
    move-exception v1

    .line 713
    .local v1, "exc":Ljava/lang/Exception;
    sget-object v3, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v4, 0x5

    .line 714
    const-string/jumbo v5, "Unable to process document summary information: "

    .line 713
    invoke-virtual {v3, v4, v5, v1, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 727
    .end local v1    # "exc":Ljava/lang/Exception;
    .restart local v0    # "docRange":Lorg/apache/poi/hwpf/usermodel/Range;
    :cond_1
    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 728
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->afterProcess()V

    goto :goto_1
.end method

.method protected abstract processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
.end method

.method protected processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 2
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 737
    const/4 v0, 0x0

    .local v0, "s":I
    :goto_0
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/Range;->numSections()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 741
    return-void

    .line 739
    :cond_0
    invoke-virtual {p2, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getSection(I)Lorg/apache/poi/hwpf/usermodel/Section;

    move-result-object v1

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;I)V

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected abstract processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;Ljava/lang/String;Lorg/w3c/dom/Element;)V
.end method

.method protected processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V
    .locals 12
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "block"    # Lorg/w3c/dom/Element;

    .prologue
    const/high16 v6, 0x44b40000    # 1440.0f

    .line 746
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->getPicturesManager()Lorg/apache/poi/hwpf/converter/PicturesManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 750
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getOfficeDrawingsMain()Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;

    move-result-object v0

    .line 751
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v3

    invoke-interface {v0, v3}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;->getOfficeDrawingAt(I)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;

    move-result-object v9

    .line 752
    .local v9, "officeDrawing":Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    if-nez v9, :cond_2

    .line 754
    sget-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Characters #"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 755
    const-string/jumbo v7, " references missing drawn object"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 754
    invoke-virtual {v0, v3, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 759
    :cond_2
    invoke-interface {v9}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getPictureData()[B

    move-result-object v1

    .line 760
    .local v1, "pictureData":[B
    if-eqz v1, :cond_0

    .line 764
    invoke-interface {v9}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleRight()I

    move-result v0

    .line 765
    invoke-interface {v9}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleLeft()I

    move-result v3

    .line 764
    sub-int/2addr v0, v3

    int-to-float v0, v0

    div-float v4, v0, v6

    .line 766
    .local v4, "width":F
    invoke-interface {v9}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleBottom()I

    move-result v0

    .line 767
    invoke-interface {v9}, Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;->getRectangleTop()I

    move-result v3

    .line 766
    sub-int/2addr v0, v3

    int-to-float v0, v0

    div-float v5, v0, v6

    .line 769
    .local v5, "height":F
    invoke-static {v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->findMatchingType([B)Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v2

    .line 770
    .local v2, "type":Lorg/apache/poi/hwpf/usermodel/PictureType;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->getPicturesManager()Lorg/apache/poi/hwpf/converter/PicturesManager;

    move-result-object v0

    .line 772
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "s"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, "."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 771
    invoke-interface/range {v0 .. v5}, Lorg/apache/poi/hwpf/converter/PicturesManager;->savePicture([BLorg/apache/poi/hwpf/usermodel/PictureType;Ljava/lang/String;FF)Ljava/lang/String;

    move-result-object v10

    .local v10, "path":Ljava/lang/String;
    move-object v6, p0

    move-object v7, p1

    move-object v8, p2

    move-object v11, p3

    .line 775
    invoke-virtual/range {v6 .. v11}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;Ljava/lang/String;Lorg/w3c/dom/Element;)V

    goto :goto_0
.end method

.method protected processDropDownList(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;[Ljava/lang/String;I)V
    .locals 1
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "values"    # [Ljava/lang/String;
    .param p4, "defaultIndex"    # I

    .prologue
    .line 785
    aget-object v0, p3, p4

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V

    .line 786
    return-void
.end method

.method protected abstract processEndnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
.end method

.method protected processField(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/Range;ILorg/apache/poi/hwpf/usermodel/Field;Lorg/w3c/dom/Element;)V
    .locals 26
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "parentRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p3, "currentTableLevel"    # I
    .param p4, "field"    # Lorg/apache/poi/hwpf/usermodel/Field;
    .param p5, "currentBlock"    # Lorg/w3c/dom/Element;

    .prologue
    .line 795
    invoke-interface/range {p4 .. p4}, Lorg/apache/poi/hwpf/usermodel/Field;->getType()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 908
    :cond_0
    sget-object v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " contains "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 909
    const-string/jumbo v7, " with unsupported type or format"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 908
    invoke-virtual {v4, v5, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 911
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->secondSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 910
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, p5

    invoke-virtual {v0, v1, v2, v4, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 912
    :cond_1
    :goto_0
    return-void

    .line 799
    :sswitch_0
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->firstSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v19

    .line 800
    .local v19, "firstSubrange":Lorg/apache/poi/hwpf/usermodel/Range;
    if-eqz v19, :cond_0

    .line 802
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/usermodel/Range;->text()Ljava/lang/String;

    move-result-object v20

    .line 803
    .local v20, "formula":Ljava/lang/String;
    sget-object v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_PAGEREF:Ljava/util/regex/Pattern;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v21

    .line 804
    .local v21, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual/range {v21 .. v21}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 806
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 808
    .local v9, "pageref":Ljava/lang/String;
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->secondSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v7

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p5

    move/from16 v8, p3

    .line 807
    invoke-virtual/range {v4 .. v9}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V

    goto :goto_0

    .line 817
    .end local v9    # "pageref":Ljava/lang/String;
    .end local v19    # "firstSubrange":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v20    # "formula":Ljava/lang/String;
    .end local v21    # "matcher":Ljava/util/regex/Matcher;
    :sswitch_1
    invoke-interface/range {p4 .. p4}, Lorg/apache/poi/hwpf/usermodel/Field;->hasSeparator()Z

    move-result v4

    if-nez v4, :cond_2

    .line 819
    sget-object v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " contains "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 820
    const-string/jumbo v7, " with \'Embedded Object\' but without separator mark"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 819
    invoke-virtual {v4, v5, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 825
    :cond_2
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->getMarkSeparatorCharacterRun(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v23

    .line 827
    .local v23, "separator":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->isOle2()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 830
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processOle2(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)Z

    move-result v22

    .line 834
    .local v22, "processed":Z
    if-nez v22, :cond_1

    .line 837
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->secondSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 836
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, p5

    invoke-virtual {v0, v1, v2, v4, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    goto/16 :goto_0

    .line 847
    .end local v22    # "processed":Z
    .end local v23    # "separator":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    :sswitch_2
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->firstSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v18

    .line 849
    .local v18, "fieldContent":Lorg/apache/poi/hwpf/usermodel/Range;
    if-eqz v18, :cond_1

    .line 852
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 851
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v16

    .line 853
    .local v16, "cr":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getDropDownListValues()[Ljava/lang/String;

    move-result-object v25

    .line 854
    .local v25, "values":[Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getDropDownListDefaultItemIndex()Ljava/lang/Integer;

    move-result-object v17

    .line 856
    .local v17, "defIndex":Ljava/lang/Integer;
    if-eqz v25, :cond_0

    .line 859
    if-nez v17, :cond_3

    const/4 v4, -0x1

    .line 858
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, v16

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDropDownList(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;[Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 859
    :cond_3
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_1

    .line 866
    .end local v16    # "cr":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .end local v17    # "defIndex":Ljava/lang/Integer;
    .end local v18    # "fieldContent":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v25    # "values":[Ljava/lang/String;
    :sswitch_3
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->firstSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v19

    .line 867
    .restart local v19    # "firstSubrange":Lorg/apache/poi/hwpf/usermodel/Range;
    if-eqz v19, :cond_0

    .line 869
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/usermodel/Range;->text()Ljava/lang/String;

    move-result-object v20

    .line 870
    .restart local v20    # "formula":Ljava/lang/String;
    sget-object v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_HYPERLINK_EXTERNAL:Ljava/util/regex/Pattern;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v21

    .line 871
    .restart local v21    # "matcher":Ljava/util/regex/Matcher;
    invoke-virtual/range {v21 .. v21}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 873
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    .line 875
    .local v15, "hyperlink":Ljava/lang/String;
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Field;->secondSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v13

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p5

    move/from16 v14, p3

    .line 874
    invoke-virtual/range {v10 .. v15}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processHyperlink(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 879
    .end local v15    # "hyperlink":Ljava/lang/String;
    :cond_4
    sget-object v4, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->PATTERN_HYPERLINK_LOCAL:Ljava/util/regex/Pattern;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    .line 880
    invoke-virtual/range {v21 .. v21}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 882
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    .line 883
    .restart local v15    # "hyperlink":Ljava/lang/String;
    const/4 v13, 0x0

    .line 884
    .local v13, "textRange":Lorg/apache/poi/hwpf/usermodel/Range;
    const/4 v4, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v24

    .line 885
    .local v24, "text":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 887
    new-instance v13, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$7;

    .end local v13    # "textRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v4

    .line 888
    const/4 v5, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->start(I)I

    move-result v5

    add-int/2addr v4, v5

    .line 889
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v5

    .line 890
    const/4 v6, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->end(I)I

    move-result v6

    .line 889
    add-int/2addr v5, v6

    .line 887
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v13, v0, v4, v5, v1}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$7;-><init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter;IILorg/apache/poi/hwpf/usermodel/Range;)V

    .restart local v13    # "textRange":Lorg/apache/poi/hwpf/usermodel/Range;
    :cond_5
    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p5

    move/from16 v14, p3

    .line 899
    invoke-virtual/range {v10 .. v15}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 795
    nop

    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_0
        0x3a -> :sswitch_1
        0x53 -> :sswitch_2
        0x58 -> :sswitch_3
    .end sparse-switch
.end method

.method protected abstract processFootnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
.end method

.method protected abstract processHyperlink(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
.end method

.method protected processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V
    .locals 9
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    const/high16 v2, 0x44b40000    # 1440.0f

    .line 925
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->getPicturesManager()Lorg/apache/poi/hwpf/converter/PicturesManager;

    move-result-object v0

    .line 926
    .local v0, "fileManager":Lorg/apache/poi/hwpf/converter/PicturesManager;
    if-eqz v0, :cond_2

    .line 928
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getHorizontalScalingFactor()I

    move-result v6

    .line 929
    .local v6, "aspectRatioX":I
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getVerticalScalingFactor()I

    move-result v7

    .line 931
    .local v7, "aspectRatioY":I
    if-lez v6, :cond_0

    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v1

    .line 932
    mul-int/2addr v1, v6

    int-to-float v1, v1

    div-float/2addr v1, v3

    div-float v4, v1, v2

    .line 934
    .local v4, "imageWidth":F
    :goto_0
    if-lez v7, :cond_1

    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v1

    .line 935
    mul-int/2addr v1, v7

    int-to-float v1, v1

    div-float/2addr v1, v3

    div-float v5, v1, v2

    .line 938
    .local v5, "imageHeight":F
    :goto_1
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getContent()[B

    move-result-object v1

    .line 939
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestPictureType()Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v2

    .line 940
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestFullFileName()Ljava/lang/String;

    move-result-object v3

    .line 938
    invoke-interface/range {v0 .. v5}, Lorg/apache/poi/hwpf/converter/PicturesManager;->savePicture([BLorg/apache/poi/hwpf/usermodel/PictureType;Ljava/lang/String;FF)Ljava/lang/String;

    move-result-object v8

    .line 942
    .local v8, "url":Ljava/lang/String;
    invoke-static {v8}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 944
    invoke-virtual {p0, p1, p2, p3, v8}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;Ljava/lang/String;)V

    .line 951
    .end local v4    # "imageWidth":F
    .end local v5    # "imageHeight":F
    .end local v6    # "aspectRatioX":I
    .end local v7    # "aspectRatioY":I
    .end local v8    # "url":Ljava/lang/String;
    :goto_2
    return-void

    .line 933
    .restart local v6    # "aspectRatioX":I
    .restart local v7    # "aspectRatioY":I
    :cond_0
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDxaGoal()I

    move-result v1

    int-to-float v1, v1

    div-float v4, v1, v2

    goto :goto_0

    .line 936
    .restart local v4    # "imageWidth":F
    :cond_1
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->getDyaGoal()I

    move-result v1

    int-to-float v1, v1

    div-float v5, v1, v2

    goto :goto_1

    .line 949
    .end local v4    # "imageWidth":F
    .end local v6    # "aspectRatioX":I
    .end local v7    # "aspectRatioY":I
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processImageWithoutPicturesManager(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V

    goto :goto_2
.end method

.method protected abstract processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;Ljava/lang/String;)V
.end method

.method protected abstract processImageWithoutPicturesManager(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation
.end method

.method protected abstract processLineBreak(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;)V
.end method

.method protected processNoteAnchor(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V
    .locals 11
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "block"    # Lorg/w3c/dom/Element;

    .prologue
    const/4 v10, -0x1

    .line 967
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getFootnotes()Lorg/apache/poi/hwpf/usermodel/Notes;

    move-result-object v3

    .line 970
    .local v3, "footnotes":Lorg/apache/poi/hwpf/usermodel/Notes;
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v9

    .line 969
    invoke-interface {v3, v9}, Lorg/apache/poi/hwpf/usermodel/Notes;->getNoteIndexByAnchorPosition(I)I

    move-result v4

    .line 971
    .local v4, "noteIndex":I
    if-eq v4, v10, :cond_1

    .line 973
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getFootnoteRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v2

    .line 974
    .local v2, "footnoteRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v8

    .line 976
    .local v8, "rangeStartOffset":I
    invoke-interface {v3, v4}, Lorg/apache/poi/hwpf/usermodel/Notes;->getNoteTextStartOffset(I)I

    move-result v7

    .line 978
    .local v7, "noteTextStartOffset":I
    invoke-interface {v3, v4}, Lorg/apache/poi/hwpf/usermodel/Notes;->getNoteTextEndOffset(I)I

    move-result v5

    .line 980
    .local v5, "noteTextEndOffset":I
    new-instance v6, Lorg/apache/poi/hwpf/usermodel/Range;

    .line 981
    add-int v9, v8, v7

    .line 982
    add-int v10, v8, v5

    .line 980
    invoke-direct {v6, v9, v10, p1}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 984
    .local v6, "noteTextRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {p0, p1, v4, p3, v6}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processFootnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 1011
    .end local v2    # "footnoteRange":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v5    # "noteTextEndOffset":I
    .end local v6    # "noteTextRange":Lorg/apache/poi/hwpf/usermodel/Range;
    .end local v7    # "noteTextStartOffset":I
    .end local v8    # "rangeStartOffset":I
    :cond_0
    :goto_0
    return-void

    .line 990
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getEndnotes()Lorg/apache/poi/hwpf/usermodel/Notes;

    move-result-object v1

    .line 992
    .local v1, "endnotes":Lorg/apache/poi/hwpf/usermodel/Notes;
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getStartOffset()I

    move-result v9

    .line 991
    invoke-interface {v1, v9}, Lorg/apache/poi/hwpf/usermodel/Notes;->getNoteIndexByAnchorPosition(I)I

    move-result v4

    .line 993
    if-eq v4, v10, :cond_0

    .line 995
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getEndnoteRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    .line 996
    .local v0, "endnoteRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v8

    .line 998
    .restart local v8    # "rangeStartOffset":I
    invoke-interface {v1, v4}, Lorg/apache/poi/hwpf/usermodel/Notes;->getNoteTextStartOffset(I)I

    move-result v7

    .line 1000
    .restart local v7    # "noteTextStartOffset":I
    invoke-interface {v1, v4}, Lorg/apache/poi/hwpf/usermodel/Notes;->getNoteTextEndOffset(I)I

    move-result v5

    .line 1002
    .restart local v5    # "noteTextEndOffset":I
    new-instance v6, Lorg/apache/poi/hwpf/usermodel/Range;

    .line 1003
    add-int v9, v8, v7

    .line 1004
    add-int v10, v8, v5

    .line 1002
    invoke-direct {v6, v9, v10, p1}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 1006
    .restart local v6    # "noteTextRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {p0, p1, v4, p3, v6}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processEndnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V

    goto :goto_0
.end method

.method protected processOle2(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/w3c/dom/Element;Lorg/apache/poi/poifs/filesystem/Entry;)Z
    .locals 1
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "block"    # Lorg/w3c/dom/Element;
    .param p3, "entry"    # Lorg/apache/poi/poifs/filesystem/Entry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1044
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract processPageBreak(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;)V
.end method

.method protected abstract processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
.end method

.method protected abstract processParagraph(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;ILorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/String;)V
.end method

.method protected processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V
    .locals 22
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;
    .param p3, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I

    .prologue
    .line 1061
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v19

    .line 1062
    .local v19, "paragraphs":I
    const/16 v18, 0x0

    .local v18, "p":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_0

    .line 1121
    return-void

    .line 1064
    :cond_0
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v8

    .line 1066
    .local v8, "paragraph":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1067
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v4

    move/from16 v0, p4

    if-eq v4, v0, :cond_3

    .line 1069
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v4

    move/from16 v0, p4

    if-ge v4, v0, :cond_1

    .line 1070
    new-instance v4, Ljava/lang/IllegalStateException;

    .line 1071
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Trying to process table cell with higher level ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1072
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1073
    const-string/jumbo v6, ") than current table level ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1074
    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1075
    const-string/jumbo v6, ") as inner table part"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1071
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1070
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1077
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Lorg/apache/poi/hwpf/usermodel/Range;->getTable(Lorg/apache/poi/hwpf/usermodel/Paragraph;)Lorg/apache/poi/hwpf/usermodel/Table;

    move-result-object v21

    .line 1078
    .local v21, "table":Lorg/apache/poi/hwpf/usermodel/Table;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processTable(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Table;)V

    .line 1080
    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hwpf/usermodel/Table;->numParagraphs()I

    move-result v4

    add-int v18, v18, v4

    .line 1081
    add-int/lit8 v18, v18, -0x1

    .line 1062
    .end local v21    # "table":Lorg/apache/poi/hwpf/usermodel/Table;
    :cond_2
    :goto_1
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 1085
    :cond_3
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->text()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "\u000c"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1087
    invoke-virtual/range {p0 .. p2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processPageBreak(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;)V

    .line 1090
    :cond_4
    const/16 v20, 0x0

    .line 1091
    .local v20, "processed":Z
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInList()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1095
    :try_start_0
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getList()Lorg/apache/poi/hwpf/usermodel/HWPFList;

    move-result-object v17

    .line 1098
    .local v17, "hwpfList":Lorg/apache/poi/hwpf/usermodel/HWPFList;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->numberingState:Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;

    .line 1099
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getIlvl()I

    move-result v5

    int-to-char v5, v5

    .line 1097
    move-object/from16 v0, v17

    invoke-static {v4, v0, v5}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->getBulletText(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;Lorg/apache/poi/hwpf/usermodel/HWPFList;C)Ljava/lang/String;

    move-result-object v9

    .local v9, "label":Ljava/lang/String;
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v7, p4

    .line 1101
    invoke-virtual/range {v4 .. v9}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processParagraph(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;ILorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1103
    const/16 v20, 0x1

    .line 1114
    .end local v9    # "label":Ljava/lang/String;
    .end local v17    # "hwpfList":Lorg/apache/poi/hwpf/usermodel/HWPFList;
    :cond_5
    :goto_2
    if-nez v20, :cond_2

    .line 1117
    const-string/jumbo v15, ""

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move/from16 v13, p4

    move-object v14, v8

    .line 1116
    invoke-virtual/range {v10 .. v15}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processParagraph(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;ILorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/String;)V

    goto :goto_1

    .line 1105
    :catch_0
    move-exception v16

    .line 1107
    .local v16, "exc":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->log:Lorg/apache/poi/util/POILogger;

    .line 1108
    const/4 v5, 0x5

    .line 1109
    const-string/jumbo v6, "Can\'t process paragraph as list entry, will be processed without list information"

    .line 1107
    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v6, v0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method protected abstract processSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;I)V
.end method

.method protected processSingleSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;)V
    .locals 1
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;

    .prologue
    .line 1129
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;I)V

    .line 1130
    return-void
.end method

.method protected processSymbol(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V
    .locals 0
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 1136
    return-void
.end method

.method protected abstract processTable(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Table;)V
.end method

.method public setFontReplacer(Lorg/apache/poi/hwpf/converter/FontReplacer;)V
    .locals 0
    .param p1, "fontReplacer"    # Lorg/apache/poi/hwpf/converter/FontReplacer;

    .prologue
    .line 1143
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

    .line 1144
    return-void
.end method

.method public setPicturesManager(Lorg/apache/poi/hwpf/converter/PicturesManager;)V
    .locals 0
    .param p1, "fileManager"    # Lorg/apache/poi/hwpf/converter/PicturesManager;

    .prologue
    .line 1148
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->picturesManager:Lorg/apache/poi/hwpf/converter/PicturesManager;

    .line 1149
    return-void
.end method

.method protected tryDeadField(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;IILorg/w3c/dom/Element;)I
    .locals 10
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p3, "currentTableLevel"    # I
    .param p4, "beginMark"    # I
    .param p5, "currentBlock"    # Lorg/w3c/dom/Element;

    .prologue
    const/4 v9, 0x1

    .line 1154
    invoke-direct {p0, p1, p2, p4}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->tryDeadField_lookupFieldSeparatorEnd(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;I)[I

    move-result-object v8

    .line 1156
    .local v8, "separatorEnd":[I
    if-nez v8, :cond_0

    .line 1162
    .end local p4    # "beginMark":I
    :goto_0
    return p4

    .line 1160
    .restart local p4    # "beginMark":I
    :cond_0
    const/4 v0, 0x0

    aget v6, v8, v0

    aget v7, v8, v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 1159
    invoke-virtual/range {v0 .. v7}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDeadField(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;IIII)V

    .line 1162
    aget p4, v8, v9

    goto :goto_0
.end method
