.class public final Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
.super Lorg/apache/poi/hwpf/model/FormattedDiskPage;
.source "PAPFormattedDiskPage.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final BX_SIZE:I = 0xd

.field private static final FC_SIZE:I = 0x4


# instance fields
.field private _overFlow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field

.field private _papxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/FormattedDiskPage;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    .line 64
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "dataStream"    # [B

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;-><init>()V

    .line 60
    return-void
.end method

.method public constructor <init>([B[BIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 0
    .param p1, "documentStream"    # [B
    .param p2, "dataStream"    # [B
    .param p3, "offset"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3, p5}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;-><init>([B[BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 77
    return-void
.end method

.method public constructor <init>([B[BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 14
    .param p1, "documentStream"    # [B
    .param p2, "dataStream"    # [B
    .param p3, "offset"    # I
    .param p4, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;

    .prologue
    .line 85
    move/from16 v0, p3

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hwpf/model/FormattedDiskPage;-><init>([BI)V

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    .line 86
    const/4 v10, 0x0

    .local v10, "x":I
    :goto_0
    iget v2, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_crun:I

    if-lt v10, v2, :cond_0

    .line 106
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    .line 107
    return-void

    .line 88
    :cond_0
    invoke-virtual {p0, v10}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getStart(I)I

    move-result v8

    .line 89
    .local v8, "bytesStartAt":I
    invoke-virtual {p0, v10}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getEnd(I)I

    move-result v7

    .line 98
    .local v7, "bytesEndAt":I
    move-object/from16 v0, p4

    invoke-interface {v0, v8, v7}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getCharIndexRanges(II)[[I

    move-result-object v12

    array-length v13, v12

    const/4 v2, 0x0

    move v11, v2

    :goto_1
    if-lt v11, v13, :cond_1

    .line 86
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 98
    :cond_1
    aget-object v9, v12, v11

    .line 101
    .local v9, "range":[I
    new-instance v1, Lorg/apache/poi/hwpf/model/PAPX;

    const/4 v2, 0x0

    aget v2, v9, v2

    const/4 v3, 0x1

    aget v3, v9, v3

    invoke-virtual {p0, v10}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getGrpprl(I)[B

    move-result-object v4

    .line 102
    invoke-direct {p0, v10}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getParagraphHeight(I)Lorg/apache/poi/hwpf/model/ParagraphHeight;

    move-result-object v5

    move-object/from16 v6, p2

    .line 101
    invoke-direct/range {v1 .. v6}, Lorg/apache/poi/hwpf/model/PAPX;-><init>(II[BLorg/apache/poi/hwpf/model/ParagraphHeight;[B)V

    .line 103
    .local v1, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_1
.end method

.method private getParagraphHeight(I)Lorg/apache/poi/hwpf/model/ParagraphHeight;
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 365
    iget v2, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_crun:I

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v3, v3, 0x4

    mul-int/lit8 v4, p1, 0xd

    add-int/2addr v3, v4

    add-int v1, v2, v3

    .line 367
    .local v1, "pheOffset":I
    new-instance v0, Lorg/apache/poi/hwpf/model/ParagraphHeight;

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    invoke-direct {v0, v2, v1}, Lorg/apache/poi/hwpf/model/ParagraphHeight;-><init>([BI)V

    .line 369
    .local v0, "phe":Lorg/apache/poi/hwpf/model/ParagraphHeight;
    return-object v0
.end method


# virtual methods
.method public fill(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "filler":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/PAPX;>;"
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 117
    return-void
.end method

.method protected getGrpprl(I)[B
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 159
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    iget v5, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_crun:I

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v5, v5, 0x4

    mul-int/lit8 v6, p1, 0xd

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 160
    .local v1, "papxOffset":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 161
    .local v2, "size":I
    if-nez v2, :cond_0

    .line 163
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 170
    :goto_0
    new-array v0, v2, [B

    .line 171
    .local v0, "papx":[B
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v4, v1

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    return-object v0

    .line 167
    .end local v0    # "papx":[B
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method getOverflow()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPAPX(I)Lorg/apache/poi/hwpf/model/PAPX;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    return-object v0
.end method

.method public getPAPXs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected toByteArray(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/CharIndexTranslator;)[B
    .locals 27
    .param p1, "dataStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p2, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    const/16 v24, 0x200

    move/from16 v0, v24

    new-array v5, v0, [B

    .line 189
    .local v5, "buf":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v21

    .line 190
    .local v21, "size":I
    const/4 v13, 0x0

    .line 191
    .local v13, "grpprlOffset":I
    const/4 v6, 0x0

    .line 192
    .local v6, "bxOffset":I
    const/4 v10, 0x0

    .line 193
    .local v10, "fcOffset":I
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 196
    .local v17, "lastGrpprl":[B
    const/16 v22, 0x4

    .line 198
    .local v22, "totalSize":I
    const/4 v15, 0x0

    .line 199
    .local v15, "index":I
    :goto_0
    move/from16 v0, v21

    if-lt v15, v0, :cond_2

    .line 247
    :goto_1
    move/from16 v0, v21

    if-eq v15, v0, :cond_0

    .line 249
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v15, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 254
    :cond_0
    const/16 v24, 0x1ff

    int-to-byte v0, v15

    move/from16 v25, v0

    aput-byte v25, v5, v24

    .line 256
    mul-int/lit8 v24, v15, 0x4

    add-int/lit8 v6, v24, 0x4

    .line 257
    const/16 v13, 0x1ff

    .line 259
    const/16 v18, 0x0

    .line 260
    .local v18, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 261
    const/16 v23, 0x0

    .local v23, "x":I
    :goto_2
    move/from16 v0, v23

    if-lt v0, v15, :cond_7

    .line 351
    if-eqz v18, :cond_1

    .line 353
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v24

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v24

    .line 352
    move/from16 v0, v24

    invoke-static {v5, v10, v0}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 355
    :cond_1
    return-object v5

    .line 201
    .end local v18    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    .end local v23    # "x":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v11

    .line 202
    .local v11, "grpprl":[B
    array-length v12, v11

    .line 205
    .local v12, "grpprlLength":I
    const/16 v24, 0x1e8

    move/from16 v0, v24

    if-le v12, v0, :cond_3

    .line 207
    const/16 v12, 0x8

    .line 213
    :cond_3
    const/4 v4, 0x0

    .line 214
    .local v4, "addition":I
    move-object/from16 v0, v17

    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v24

    if-nez v24, :cond_4

    .line 216
    add-int/lit8 v24, v12, 0x11

    add-int/lit8 v4, v24, 0x1

    .line 223
    :goto_3
    add-int v22, v22, v4

    .line 228
    rem-int/lit8 v24, v15, 0x2

    move/from16 v0, v24

    add-int/lit16 v0, v0, 0x1ff

    move/from16 v24, v0

    move/from16 v0, v22

    move/from16 v1, v24

    if-le v0, v1, :cond_5

    .line 230
    sub-int v22, v22, v4

    .line 231
    goto/16 :goto_1

    .line 220
    :cond_4
    const/16 v4, 0x11

    goto :goto_3

    .line 235
    :cond_5
    rem-int/lit8 v24, v12, 0x2

    if-lez v24, :cond_6

    .line 237
    add-int/lit8 v22, v22, 0x1

    .line 243
    :goto_4
    move-object/from16 v17, v11

    .line 199
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 241
    :cond_6
    add-int/lit8 v22, v22, 0x2

    goto :goto_4

    .line 263
    .end local v4    # "addition":I
    .end local v11    # "grpprl":[B
    .end local v12    # "grpprlLength":I
    .restart local v18    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    .restart local v23    # "x":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    check-cast v18, Lorg/apache/poi/hwpf/model/PAPX;

    .line 264
    .restart local v18    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PAPX;->getParagraphHeight()Lorg/apache/poi/hwpf/model/ParagraphHeight;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hwpf/model/ParagraphHeight;->toByteArray()[B

    move-result-object v19

    .line 265
    .local v19, "phe":[B
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v11

    .line 268
    .restart local v11    # "grpprl":[B
    array-length v0, v11

    move/from16 v24, v0

    const/16 v25, 0x1e8

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_8

    .line 297
    array-length v0, v11

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x2

    move/from16 v0, v24

    new-array v14, v0, [B

    .line 298
    .local v14, "hugePapx":[B
    const/16 v24, 0x2

    const/16 v25, 0x0

    array-length v0, v11

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v11, v0, v14, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 299
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v9

    .line 300
    .local v9, "dataStreamOffset":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 303
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-static {v11, v0}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v16

    .line 305
    .local v16, "istd":I
    const/16 v24, 0x8

    move/from16 v0, v24

    new-array v11, v0, [B

    .line 306
    const/16 v24, 0x0

    move/from16 v0, v24

    move/from16 v1, v16

    invoke-static {v11, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 307
    const/16 v24, 0x2

    const/16 v25, 0x6646

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-static {v11, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 308
    const/16 v24, 0x4

    move/from16 v0, v24

    invoke-static {v11, v0, v9}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 311
    .end local v9    # "dataStreamOffset":I
    .end local v14    # "hugePapx":[B
    .end local v16    # "istd":I
    :cond_8
    move-object/from16 v0, v17

    invoke-static {v0, v11}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v20

    .line 312
    .local v20, "same":Z
    if-nez v20, :cond_9

    .line 314
    array-length v0, v11

    move/from16 v24, v0

    array-length v0, v11

    move/from16 v25, v0

    rem-int/lit8 v25, v25, 0x2

    rsub-int/lit8 v25, v25, 0x2

    add-int v24, v24, v25

    sub-int v13, v13, v24

    .line 315
    rem-int/lit8 v24, v13, 0x2

    sub-int v13, v13, v24

    .line 319
    :cond_9
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v24

    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v24

    .line 318
    move/from16 v0, v24

    invoke-static {v5, v10, v0}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 320
    div-int/lit8 v24, v13, 0x2

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v5, v6

    .line 321
    const/16 v24, 0x0

    add-int/lit8 v25, v6, 0x1

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v19

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v5, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 328
    if-nez v20, :cond_a

    .line 330
    move v7, v13

    .line 331
    .local v7, "copyOffset":I
    array-length v0, v11

    move/from16 v24, v0

    rem-int/lit8 v24, v24, 0x2

    if-lez v24, :cond_b

    .line 333
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "copyOffset":I
    .local v8, "copyOffset":I
    array-length v0, v11

    move/from16 v24, v0

    add-int/lit8 v24, v24, 0x1

    div-int/lit8 v24, v24, 0x2

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v5, v7

    move v7, v8

    .line 340
    .end local v8    # "copyOffset":I
    .restart local v7    # "copyOffset":I
    :goto_5
    const/16 v24, 0x0

    array-length v0, v11

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-static {v11, v0, v5, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 341
    move-object/from16 v17, v11

    .line 344
    .end local v7    # "copyOffset":I
    :cond_a
    add-int/lit8 v6, v6, 0xd

    .line 345
    add-int/lit8 v10, v10, 0x4

    .line 261
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_2

    .line 337
    .restart local v7    # "copyOffset":I
    :cond_b
    add-int/lit8 v7, v7, 0x1

    array-length v0, v11

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v5, v7

    .line 338
    add-int/lit8 v7, v7, 0x1

    goto :goto_5
.end method
