.class public final Lorg/apache/poi/hwpf/model/ComplexFileTable;
.super Ljava/lang/Object;
.source "ComplexFileTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final GRPPRL_TYPE:B = 0x1t

.field private static final TEXT_PIECE_TABLE_TYPE:B = 0x2t


# instance fields
.field private _grpprls:[Lorg/apache/poi/hwpf/sprm/SprmBuffer;

.field protected _tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/TextPieceTable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    .line 44
    return-void
.end method

.method public constructor <init>([B[BII)V
    .locals 10
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "fcMin"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 51
    .local v9, "sprmBuffers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/sprm/SprmBuffer;>;"
    :goto_0
    aget-byte v0, p2, p3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 62
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ComplexFileTable;->_grpprls:[Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 64
    aget-byte v0, p2, p3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 66
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "The text piece table is corrupted"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 54
    invoke-static {p2, p3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v7

    .line 55
    .local v7, "size":I
    add-int/lit8 p3, p3, 0x2

    .line 56
    invoke-static {p2, p3, v7}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v6

    .line 57
    .local v6, "bs":[B
    add-int/2addr p3, v7

    .line 59
    new-instance v8, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v8, v6, v2, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BZI)V

    .line 60
    .local v8, "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    .end local v6    # "bs":[B
    .end local v7    # "size":I
    .end local v8    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_1
    add-int/lit8 p3, p3, 0x1

    invoke-static {p2, p3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    .line 69
    .local v4, "pieceTableSize":I
    add-int/lit8 p3, p3, 0x4

    .line 70
    new-instance v0, Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hwpf/model/TextPieceTable;-><init>([B[BIII)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    .line 71
    return-void
.end method


# virtual methods
.method public getGrpprls()[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ComplexFileTable;->_grpprls:[Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method

.method public getTextPieceTable()Lorg/apache/poi/hwpf/model/TextPieceTable;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    return-object v0
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;)V
    .locals 3
    .param p1, "sys"    # Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 86
    const-string/jumbo v2, "WordDocument"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    .line 87
    .local v0, "docStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    const-string/jumbo v2, "1Table"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v1

    .line 89
    .local v1, "tableStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 90
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 3
    .param p1, "wordDocumentStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write(I)V

    .line 97
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hwpf/model/TextPieceTable;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)[B

    move-result-object v1

    .line 99
    .local v1, "table":[B
    const/4 v2, 0x4

    new-array v0, v2, [B

    .line 100
    .local v0, "numHolder":[B
    array-length v2, v1

    invoke-static {v0, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 101
    invoke-virtual {p2, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 102
    invoke-virtual {p2, v1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 103
    return-void
.end method
