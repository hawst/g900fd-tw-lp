.class public final Lorg/apache/poi/hwpf/model/UPX;
.super Ljava/lang/Object;
.source "UPX.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _upx:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "upx"    # [B

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/UPX;->_upx:[B

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 46
    if-eqz p1, :cond_0

    instance-of v1, p1, Lorg/apache/poi/hwpf/model/UPX;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 47
    check-cast v0, Lorg/apache/poi/hwpf/model/UPX;

    .line 48
    .local v0, "upx":Lorg/apache/poi/hwpf/model/UPX;
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/UPX;->_upx:[B

    iget-object v2, v0, Lorg/apache/poi/hwpf/model/UPX;->_upx:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    .line 50
    .end local v0    # "upx":Lorg/apache/poi/hwpf/model/UPX;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUPX()[B
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/UPX;->_upx:[B

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/UPX;->_upx:[B

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[UPX] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/UPX;->_upx:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
