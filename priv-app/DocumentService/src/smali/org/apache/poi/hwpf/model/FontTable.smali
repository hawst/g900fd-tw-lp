.class public final Lorg/apache/poi/hwpf/model/FontTable;
.super Ljava/lang/Object;
.source "FontTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final _logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _extraDataSz:S

.field private _fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

.field private _stringCount:S

.field private lcbSttbfffn:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/poi/hwpf/model/FontTable;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/FontTable;->_logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>([BII)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "lcbSttbfffn"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    .line 54
    iput p3, p0, Lorg/apache/poi/hwpf/model/FontTable;->lcbSttbfffn:I

    .line 57
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    .line 58
    add-int/lit8 p2, p2, 0x2

    .line 59
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_extraDataSz:S

    .line 60
    add-int/lit8 p2, p2, 0x2

    .line 62
    iget-short v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    new-array v1, v1, [Lorg/apache/poi/hwpf/model/Ffn;

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-short v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    if-lt v0, v1, :cond_0

    .line 69
    return-void

    .line 66
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    new-instance v2, Lorg/apache/poi/hwpf/model/Ffn;

    invoke-direct {v2, p1, p2}, Lorg/apache/poi/hwpf/model/Ffn;-><init>([BI)V

    aput-object v2, v1, v0

    .line 67
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Ffn;->getSize()I

    move-result v1

    add-int/2addr p2, v1

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 143
    if-eqz p1, :cond_4

    instance-of v3, p1, Lorg/apache/poi/hwpf/model/FontTable;

    if-eqz v3, :cond_4

    .line 144
    const/4 v2, 0x1

    .local v2, "retVal":Z
    move-object v3, p1

    .line 146
    check-cast v3, Lorg/apache/poi/hwpf/model/FontTable;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FontTable;->getStringCount()S

    move-result v3

    iget-short v4, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    if-ne v3, v4, :cond_3

    move-object v3, p1

    .line 148
    check-cast v3, Lorg/apache/poi/hwpf/model/FontTable;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FontTable;->getExtraDataSz()S

    move-result v3

    iget-short v4, p0, Lorg/apache/poi/hwpf/model/FontTable;->_extraDataSz:S

    if-ne v3, v4, :cond_2

    .line 150
    check-cast p1, Lorg/apache/poi/hwpf/model/FontTable;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/FontTable;->getFontNames()[Lorg/apache/poi/hwpf/model/Ffn;

    move-result-object v0

    .line 151
    .local v0, "fontNamesNew":[Lorg/apache/poi/hwpf/model/Ffn;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    if-lt v1, v3, :cond_0

    .line 166
    .end local v0    # "fontNamesNew":[Lorg/apache/poi/hwpf/model/Ffn;
    .end local v1    # "i":I
    .end local v2    # "retVal":Z
    :goto_1
    return v2

    .line 153
    .restart local v0    # "fontNamesNew":[Lorg/apache/poi/hwpf/model/Ffn;
    .restart local v1    # "i":I
    .restart local v2    # "retVal":Z
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    aget-object v3, v3, v1

    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Ffn;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 154
    const/4 v2, 0x0

    .line 151
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    .end local v0    # "fontNamesNew":[Lorg/apache/poi/hwpf/model/Ffn;
    .end local v1    # "i":I
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_2
    const/4 v2, 0x0

    .line 159
    goto :goto_1

    .line 161
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 166
    .end local v2    # "retVal":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getAltFont(I)Ljava/lang/String;
    .locals 3
    .param p1, "chpFtc"    # I

    .prologue
    .line 104
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    if-lt p1, v0, :cond_0

    .line 106
    sget-object v0, Lorg/apache/poi/hwpf/model/FontTable;->_logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x3

    const-string/jumbo v2, "Mismatch in chpFtc with stringCount"

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 107
    const/4 v0, 0x0

    .line 110
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Ffn;->getAltFontName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getExtraDataSz()S
    .locals 1

    .prologue
    .line 78
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_extraDataSz:S

    return v0
.end method

.method public getFontNames()[Lorg/apache/poi/hwpf/model/Ffn;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    return-object v0
.end method

.method public getMainFont(I)Ljava/lang/String;
    .locals 3
    .param p1, "chpFtc"    # I

    .prologue
    .line 92
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    if-lt p1, v0, :cond_0

    .line 93
    sget-object v0, Lorg/apache/poi/hwpf/model/FontTable;->_logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x3

    const-string/jumbo v2, "Mismatch in chpFtc with stringCount"

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 94
    const/4 v0, 0x0

    .line 98
    :goto_0
    return-object v0

    .line 95
    :cond_0
    if-ltz p1, :cond_1

    .line 96
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Ffn;->getMainFontName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Ffn;->getMainFontName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->lcbSttbfffn:I

    return v0
.end method

.method public getStringCount()S
    .locals 1

    .prologue
    .line 73
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    return v0
.end method

.method public setStringCount(S)V
    .locals 0
    .param p1, "stringCount"    # S

    .prologue
    .line 115
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    .line 116
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;)V
    .locals 2
    .param p1, "sys"    # Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 121
    const-string/jumbo v1, "1Table"

    invoke-virtual {p1, v1}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    .line 122
    .local v0, "tableStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/model/FontTable;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V

    .line 123
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 3
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    const/4 v2, 0x2

    new-array v0, v2, [B

    .line 128
    .local v0, "buf":[B
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/FontTable;->_stringCount:S

    invoke-static {v0, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 129
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 130
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/FontTable;->_extraDataSz:S

    invoke-static {v0, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 131
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 133
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 138
    return-void

    .line 135
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FontTable;->_fontNames:[Lorg/apache/poi/hwpf/model/Ffn;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Ffn;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 133
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
