.class public Lorg/apache/poi/hwpf/converter/TextDocumentFacade;
.super Ljava/lang/Object;
.source "TextDocumentFacade.java"


# instance fields
.field protected final body:Lorg/w3c/dom/Element;

.field protected final document:Lorg/w3c/dom/Document;

.field protected final head:Lorg/w3c/dom/Element;

.field protected final root:Lorg/w3c/dom/Element;

.field protected title:Lorg/w3c/dom/Element;

.field protected titleText:Lorg/w3c/dom/Text;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    .line 39
    const-string/jumbo v0, "html"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->root:Lorg/w3c/dom/Element;

    .line 40
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->root:Lorg/w3c/dom/Element;

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 42
    const-string/jumbo v0, "body"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->body:Lorg/w3c/dom/Element;

    .line 43
    const-string/jumbo v0, "head"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    .line 45
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->root:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 46
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->root:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->body:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 48
    const-string/jumbo v0, "title"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    .line 49
    const-string/jumbo v0, ""

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    .line 50
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 51
    return-void
.end method


# virtual methods
.method public addAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string/jumbo v0, "Author"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->addMeta(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public addDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string/jumbo v0, "Description"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->addMeta(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public addKeywords(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 65
    const-string/jumbo v0, "Keywords"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->addMeta(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public addMeta(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 70
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v4, "meta"

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 72
    .local v0, "meta":Lorg/w3c/dom/Element;
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v4, "name"

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 73
    .local v1, "metaName":Lorg/w3c/dom/Element;
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v3

    invoke-interface {v1, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 74
    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 76
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v4, "value"

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 77
    .local v2, "metaValue":Lorg/w3c/dom/Element;
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 78
    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 80
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 81
    return-void
.end method

.method public createBlock()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "div"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createHeader1()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 90
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "h1"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 91
    .local v0, "result":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "        "

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 92
    return-object v0
.end method

.method public createHeader2()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "h2"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 98
    .local v0, "result":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "    "

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 99
    return-object v0
.end method

.method public createParagraph()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "p"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTable()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "table"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableBody()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "tbody"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableCell()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "td"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableRow()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "tr"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createText(Ljava/lang/String;)Lorg/w3c/dom/Text;
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    return-object v0
.end method

.method public createUnorderedList()Lorg/w3c/dom/Element;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "ul"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public getBody()Lorg/w3c/dom/Element;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->body:Lorg/w3c/dom/Element;

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method public getHead()Lorg/w3c/dom/Element;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    if-nez v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    invoke-interface {v0}, Lorg/w3c/dom/Text;->getTextContent()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "titleText"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->removeChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 165
    iput-object v2, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    .line 166
    iput-object v2, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    .line 169
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    if-nez v0, :cond_1

    .line 171
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "title"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    .line 172
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    .line 173
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 174
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->head:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->title:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 177
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->titleText:Lorg/w3c/dom/Text;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Text;->setData(Ljava/lang/String;)V

    .line 178
    return-void
.end method
