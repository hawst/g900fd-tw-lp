.class public final Lorg/apache/poi/hwpf/usermodel/HWPFList;
.super Ljava/lang/Object;
.source "HWPFList.java"


# static fields
.field private static log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _ignoreLogicalLeftIdentation:Z

.field private _lfo:Lorg/apache/poi/hwpf/model/LFO;

.field private _lfoData:Lorg/apache/poi/hwpf/model/LFOData;

.field private _listData:Lorg/apache/poi/hwpf/model/ListData;

.field private _styleSheet:Lorg/apache/poi/hwpf/model/StyleSheet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lorg/apache/poi/hwpf/usermodel/HWPFList;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->log:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/model/ListTables;I)V
    .locals 4
    .param p1, "styleSheet"    # Lorg/apache/poi/hwpf/model/StyleSheet;
    .param p2, "listTables"    # Lorg/apache/poi/hwpf/model/ListTables;
    .param p3, "ilfo"    # I

    .prologue
    const v3, 0xffff

    const/4 v2, 0x1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_ignoreLogicalLeftIdentation:Z

    .line 86
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_styleSheet:Lorg/apache/poi/hwpf/model/StyleSheet;

    .line 90
    if-eqz p3, :cond_0

    const v1, 0xf801

    if-ne p3, v1, :cond_1

    .line 92
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Paragraph not in list"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 94
    :cond_1
    if-gt v2, p3, :cond_2

    const/16 v1, 0x7fe

    if-gt p3, v1, :cond_2

    .line 96
    invoke-virtual {p2, p3}, Lorg/apache/poi/hwpf/model/ListTables;->getLfo(I)Lorg/apache/poi/hwpf/model/LFO;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    .line 97
    invoke-virtual {p2, p3}, Lorg/apache/poi/hwpf/model/ListTables;->getLfoData(I)Lorg/apache/poi/hwpf/model/LFOData;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    .line 111
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LFO;->getLsid()I

    move-result v1

    invoke-virtual {p2, v1}, Lorg/apache/poi/hwpf/model/ListTables;->getListData(I)Lorg/apache/poi/hwpf/model/ListData;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    .line 112
    return-void

    .line 99
    :cond_2
    const v1, 0xf802

    if-gt v1, p3, :cond_3

    if-gt p3, v3, :cond_3

    .line 101
    xor-int v0, p3, v3

    .line 102
    .local v0, "nilfo":I
    invoke-virtual {p2, v0}, Lorg/apache/poi/hwpf/model/ListTables;->getLfo(I)Lorg/apache/poi/hwpf/model/LFO;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    .line 103
    invoke-virtual {p2, v0}, Lorg/apache/poi/hwpf/model/ListTables;->getLfoData(I)Lorg/apache/poi/hwpf/model/LFOData;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    .line 104
    iput-boolean v2, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_ignoreLogicalLeftIdentation:Z

    goto :goto_0

    .line 108
    .end local v0    # "nilfo":I
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Incorrect ilfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>(ZLorg/apache/poi/hwpf/model/StyleSheet;)V
    .locals 6
    .param p1, "numbered"    # Z
    .param p2, "styleSheet"    # Lorg/apache/poi/hwpf/model/StyleSheet;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_ignoreLogicalLeftIdentation:Z

    .line 75
    new-instance v0, Lorg/apache/poi/hwpf/model/ListData;

    .line 76
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-direct {v0, v1, p1}, Lorg/apache/poi/hwpf/model/ListData;-><init>(IZ)V

    .line 75
    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    .line 77
    new-instance v0, Lorg/apache/poi/hwpf/model/LFO;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/LFO;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    .line 78
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListData;->getLsid()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/LFO;->setLsid(I)V

    .line 79
    new-instance v0, Lorg/apache/poi/hwpf/model/LFOData;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/LFOData;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    .line 80
    iput-object p2, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_styleSheet:Lorg/apache/poi/hwpf/model/StyleSheet;

    .line 81
    return-void
.end method


# virtual methods
.method public getLFO()Lorg/apache/poi/hwpf/model/LFO;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    return-object v0
.end method

.method public getLFOData()Lorg/apache/poi/hwpf/model/LFOData;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    return-object v0
.end method

.method getLVL(C)Lorg/apache/poi/hwpf/model/ListLevel;
    .locals 4
    .param p1, "level"    # C
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 140
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListData;->numLevels()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 142
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Required level "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 144
    const-string/jumbo v3, " is more than number of level for list ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 145
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/ListData;->numLevels()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 147
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListData;->getLevels()[Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v1

    aget-object v0, v1, p1

    .line 148
    .local v0, "lvl":Lorg/apache/poi/hwpf/model/ListLevel;
    return-object v0
.end method

.method public getListData()Lorg/apache/poi/hwpf/model/ListData;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    return-object v0
.end method

.method public getLsid()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfo:Lorg/apache/poi/hwpf/model/LFO;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFO;->getLsid()I

    move-result v0

    return v0
.end method

.method public getNumberFormat(C)I
    .locals 1
    .param p1, "level"    # C

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLVL(C)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListLevel;->getNumberFormat()I

    move-result v0

    return v0
.end method

.method public getNumberText(C)Ljava/lang/String;
    .locals 1
    .param p1, "level"    # C

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLVL(C)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListLevel;->getNumberText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartAt(C)I
    .locals 1
    .param p1, "level"    # C

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->isStartAtOverriden(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFOData;->getRgLfoLvl()[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->getIStartAt()I

    move-result v0

    .line 176
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLVL(C)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListLevel;->getStartAt()I

    move-result v0

    goto :goto_0
.end method

.method public getTypeOfCharFollowingTheNumber(C)B
    .locals 1
    .param p1, "level"    # C

    .prologue
    .line 184
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLVL(C)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListLevel;->getTypeOfCharFollowingTheNumber()B

    move-result v0

    return v0
.end method

.method public isIgnoreLogicalLeftIdentation()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_ignoreLogicalLeftIdentation:Z

    return v0
.end method

.method public isStartAtOverriden(C)Z
    .locals 2
    .param p1, "level"    # C

    .prologue
    .line 194
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LFOData;->getRgLfoLvl()[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    move-result-object v1

    array-length v1, v1

    if-le v1, p1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_lfoData:Lorg/apache/poi/hwpf/model/LFOData;

    .line 195
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LFOData;->getRgLfoLvl()[Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    move-result-object v1

    aget-object v0, v1, p1

    .line 197
    .local v0, "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->getIStartAt()I

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->isFormatting()Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 195
    .end local v0    # "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 197
    .restart local v0    # "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setIgnoreLogicalLeftIdentation(Z)V
    .locals 0
    .param p1, "ignoreLogicalLeftIdentation"    # Z

    .prologue
    .line 204
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_ignoreLogicalLeftIdentation:Z

    .line 205
    return-void
.end method

.method public setLevelNumberProperties(ILorg/apache/poi/hwpf/usermodel/CharacterProperties;)V
    .locals 5
    .param p1, "level"    # I
    .param p2, "chp"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .prologue
    .line 217
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hwpf/model/ListData;->getLevel(I)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v2

    .line 218
    .local v2, "listLevel":Lorg/apache/poi/hwpf/model/ListLevel;
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hwpf/model/ListData;->getLevelStyle(I)I

    move-result v3

    .line 219
    .local v3, "styleIndex":I
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_styleSheet:Lorg/apache/poi/hwpf/model/StyleSheet;

    invoke-virtual {v4, v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 221
    .local v0, "base":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-static {p2, v0}, Lorg/apache/poi/hwpf/sprm/CharacterSprmCompressor;->compressCharacterProperty(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)[B

    move-result-object v1

    .line 223
    .local v1, "grpprl":[B
    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/model/ListLevel;->setNumberProperties([B)V

    .line 224
    return-void
.end method

.method public setLevelParagraphProperties(ILorg/apache/poi/hwpf/usermodel/ParagraphProperties;)V
    .locals 5
    .param p1, "level"    # I
    .param p2, "pap"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .prologue
    .line 236
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hwpf/model/ListData;->getLevel(I)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v2

    .line 237
    .local v2, "listLevel":Lorg/apache/poi/hwpf/model/ListLevel;
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hwpf/model/ListData;->getLevelStyle(I)I

    move-result v3

    .line 238
    .local v3, "styleIndex":I
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_styleSheet:Lorg/apache/poi/hwpf/model/StyleSheet;

    invoke-virtual {v4, v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->getParagraphStyle(I)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v0

    .line 240
    .local v0, "base":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-static {p2, v0}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmCompressor;->compressParagraphProperty(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)[B

    move-result-object v1

    .line 242
    .local v1, "grpprl":[B
    invoke-virtual {v2, v1}, Lorg/apache/poi/hwpf/model/ListLevel;->setLevelProperties([B)V

    .line 243
    return-void
.end method

.method public setLevelStyle(II)V
    .locals 1
    .param p1, "level"    # I
    .param p2, "styleIndex"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HWPFList;->_listData:Lorg/apache/poi/hwpf/model/ListData;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hwpf/model/ListData;->setLevelStyle(II)V

    .line 248
    return-void
.end method
