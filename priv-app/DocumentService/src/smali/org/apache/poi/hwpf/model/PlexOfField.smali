.class public Lorg/apache/poi/hwpf/model/PlexOfField;
.super Ljava/lang/Object;
.source "PlexOfField.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private final fld:Lorg/apache/poi/hwpf/model/FieldDescriptor;

.field private final propertyNode:Lorg/apache/poi/hwpf/model/GenericPropertyNode;


# direct methods
.method public constructor <init>(II[B)V
    .locals 1
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "data"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->propertyNode:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 42
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldDescriptor;

    invoke-direct {v0, p3}, Lorg/apache/poi/hwpf/model/FieldDescriptor;-><init>([B)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/poi/hwpf/model/FieldDescriptor;

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V
    .locals 2
    .param p1, "propertyNode"    # Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->propertyNode:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 48
    new-instance v0, Lorg/apache/poi/hwpf/model/FieldDescriptor;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/FieldDescriptor;-><init>([B)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/poi/hwpf/model/FieldDescriptor;

    .line 49
    return-void
.end method


# virtual methods
.method public getFcEnd()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->propertyNode:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v0

    return v0
.end method

.method public getFcStart()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->propertyNode:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v0

    return v0
.end method

.method public getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/poi/hwpf/model/FieldDescriptor;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    const-string/jumbo v0, "[{0}, {1}) - FLD - 0x{2}; 0x{3}"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 69
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcEnd()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 70
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/poi/hwpf/model/FieldDescriptor;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 71
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/poi/hwpf/model/FieldDescriptor;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getFlt()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 68
    invoke-static {v0, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
