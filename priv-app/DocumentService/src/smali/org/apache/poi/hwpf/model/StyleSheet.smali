.class public final Lorg/apache/poi/hwpf/model/StyleSheet;
.super Ljava/lang/Object;
.source "StyleSheet.java"

# interfaces
.implements Lorg/apache/poi/hwpf/model/HDFType;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final NIL_CHP:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final NIL_CHPX:[B

.field private static final NIL_PAP:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final NIL_PAPX:[B

.field public static final NIL_STYLE:I = 0xfff


# instance fields
.field private _cbStshi:I

.field private _stshif:Lorg/apache/poi/hwpf/model/Stshif;

.field _styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    sput-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAP:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 53
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;-><init>()V

    sput-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHP:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .line 55
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHPX:[B

    .line 56
    const/4 v0, 0x2

    new-array v0, v0, [B

    sput-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAPX:[B

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 6
    .param p1, "tableStream"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    move v1, p2

    .line 80
    .local v1, "startOffset":I
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    iput v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    .line 81
    add-int/lit8 p2, p2, 0x2

    .line 91
    new-instance v4, Lorg/apache/poi/hwpf/model/Stshif;

    invoke-direct {v4, p1, p2}, Lorg/apache/poi/hwpf/model/Stshif;-><init>([BI)V

    iput-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    .line 92
    invoke-static {}, Lorg/apache/poi/hwpf/model/Stshif;->getSize()I

    move-result v4

    add-int/2addr p2, v4

    .line 96
    add-int/lit8 v4, v1, 0x2

    iget v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    add-int p2, v4, v5

    .line 97
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/Stshif;->getCstd()I

    move-result v4

    new-array v4, v4, [Lorg/apache/poi/hwpf/model/StyleDescription;

    iput-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    .line 98
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/Stshif;->getCstd()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 116
    const/4 v3, 0x0

    :goto_1
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v4, v4

    if-lt v3, v4, :cond_2

    .line 124
    return-void

    .line 100
    :cond_0
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    .line 102
    .local v2, "stdSize":I
    add-int/lit8 p2, p2, 0x2

    .line 103
    if-lez v2, :cond_1

    .line 107
    new-instance v0, Lorg/apache/poi/hwpf/model/StyleDescription;

    .line 108
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/Stshif;->getCbSTDBaseInFile()I

    move-result v4

    const/4 v5, 0x1

    .line 107
    invoke-direct {v0, p1, v4, p2, v5}, Lorg/apache/poi/hwpf/model/StyleDescription;-><init>([BIIZ)V

    .line 110
    .local v0, "aStyle":Lorg/apache/poi/hwpf/model/StyleDescription;
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aput-object v0, v4, v3

    .line 113
    .end local v0    # "aStyle":Lorg/apache/poi/hwpf/model/StyleDescription;
    :cond_1
    add-int/2addr p2, v2

    .line 98
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 118
    .end local v2    # "stdSize":I
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v4, v4, v3

    if-eqz v4, :cond_3

    .line 120
    invoke-direct {p0, v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->createPap(I)V

    .line 121
    invoke-direct {p0, v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->createChp(I)V

    .line 116
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private createChp(I)V
    .locals 6
    .param p1, "istd"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 264
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v4, v5, p1

    .line 265
    .local v4, "sd":Lorg/apache/poi/hwpf/model/StyleDescription;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHP()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v1

    .line 266
    .local v1, "chp":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHPX()[B

    move-result-object v2

    .line 267
    .local v2, "chpx":[B
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->getBaseStyle()I

    move-result v0

    .line 269
    .local v0, "baseIndex":I
    if-ne v0, p1, :cond_0

    .line 274
    const/16 v0, 0xfff

    .line 278
    :cond_0
    if-nez v1, :cond_2

    if-eqz v2, :cond_2

    .line 280
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;-><init>()V

    .line 281
    .local v3, "parentCHP":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    const/16 v5, 0xfff

    if-eq v0, v5, :cond_1

    .line 284
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHP()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v3

    .line 285
    if-nez v3, :cond_1

    .line 287
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/model/StyleSheet;->createChp(I)V

    .line 288
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHP()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v3

    .line 293
    :cond_1
    const/4 v5, 0x0

    invoke-static {v3, v2, v5}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->uncompressCHP(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BI)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v1

    .line 294
    invoke-virtual {v4, v1}, Lorg/apache/poi/hwpf/model/StyleDescription;->setCHP(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)V

    .line 296
    .end local v3    # "parentCHP":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    :cond_2
    return-void
.end method

.method private createPap(I)V
    .locals 8
    .param p1, "istd"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 220
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v4, v5, p1

    .line 221
    .local v4, "sd":Lorg/apache/poi/hwpf/model/StyleDescription;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAP()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v1

    .line 222
    .local v1, "pap":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAPX()[B

    move-result-object v2

    .line 223
    .local v2, "papx":[B
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->getBaseStyle()I

    move-result v0

    .line 224
    .local v0, "baseIndex":I
    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    .line 226
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 227
    .local v3, "parentPAP":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    const/16 v5, 0xfff

    if-eq v0, v5, :cond_1

    .line 230
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAP()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v3

    .line 231
    if-nez v3, :cond_1

    .line 232
    if-ne v0, p1, :cond_0

    .line 234
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Pap style "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " claimed to have itself as its parent, which isn\'t allowed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 237
    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/model/StyleSheet;->createPap(I)V

    .line 238
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAP()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v3

    .line 243
    :cond_1
    if-nez v3, :cond_2

    .line 244
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .end local v3    # "parentPAP":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-direct {v3}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 247
    .restart local v3    # "parentPAP":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :cond_2
    const/4 v5, 0x2

    invoke-static {v3, v2, v5}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v1

    .line 248
    invoke-virtual {v4, v1}, Lorg/apache/poi/hwpf/model/StyleDescription;->setPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)V

    .line 250
    .end local v3    # "parentPAP":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :cond_3
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 181
    if-eqz p1, :cond_0

    instance-of v3, p1, Lorg/apache/poi/hwpf/model/StyleSheet;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 182
    check-cast v0, Lorg/apache/poi/hwpf/model/StyleSheet;

    .line 184
    .local v0, "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Stshif;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    iget v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    if-ne v3, v4, :cond_0

    .line 186
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v3, v3

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 188
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 200
    const/4 v2, 0x1

    .line 205
    .end local v0    # "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    .end local v1    # "x":I
    :cond_0
    return v2

    .line 191
    .restart local v0    # "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    .restart local v1    # "x":I
    :cond_1
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v4, v4, v1

    if-eq v3, v4, :cond_2

    .line 194
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/StyleDescription;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCHPX(I)[B
    .locals 1
    .param p1, "styleIndex"    # I

    .prologue
    .line 362
    const/16 v0, 0xfff

    if-ne p1, v0, :cond_0

    .line 364
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHPX:[B

    .line 382
    :goto_0
    return-object v0

    .line 367
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 369
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHPX:[B

    goto :goto_0

    .line 372
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    if-nez v0, :cond_2

    .line 374
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHPX:[B

    goto :goto_0

    .line 377
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHPX()[B

    move-result-object v0

    if-nez v0, :cond_3

    .line 379
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHPX:[B

    goto :goto_0

    .line 382
    :cond_3
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHPX()[B

    move-result-object v0

    goto :goto_0
.end method

.method public getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 1
    .param p1, "styleIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 320
    const/16 v0, 0xfff

    if-ne p1, v0, :cond_0

    .line 322
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHP:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .line 330
    :goto_0
    return-object v0

    .line 325
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 327
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHP:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    goto :goto_0

    .line 330
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    .line 331
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getCHP()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_CHP:Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    goto :goto_0
.end method

.method public getPAPX(I)[B
    .locals 1
    .param p1, "styleIndex"    # I

    .prologue
    .line 387
    const/16 v0, 0xfff

    if-ne p1, v0, :cond_0

    .line 389
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAPX:[B

    .line 407
    :goto_0
    return-object v0

    .line 392
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 394
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAPX:[B

    goto :goto_0

    .line 397
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    if-nez v0, :cond_2

    .line 399
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAPX:[B

    goto :goto_0

    .line 402
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAPX()[B

    move-result-object v0

    if-nez v0, :cond_3

    .line 404
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAPX:[B

    goto :goto_0

    .line 407
    :cond_3
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAPX()[B

    move-result-object v0

    goto :goto_0
.end method

.method public getParagraphStyle(I)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .locals 1
    .param p1, "styleIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 337
    const/16 v0, 0xfff

    if-ne p1, v0, :cond_0

    .line 339
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAP:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .line 357
    :goto_0
    return-object v0

    .line 342
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 344
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAP:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    goto :goto_0

    .line 347
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    if-nez v0, :cond_2

    .line 349
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAP:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    goto :goto_0

    .line 352
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAP()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v0

    if-nez v0, :cond_3

    .line 354
    sget-object v0, Lorg/apache/poi/hwpf/model/StyleSheet;->NIL_PAP:Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    goto :goto_0

    .line 357
    :cond_3
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/StyleDescription;->getPAP()Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v0

    goto :goto_0
.end method

.method public getStyleDescription(I)Lorg/apache/poi/hwpf/model/StyleDescription;
    .locals 1
    .param p1, "styleIndex"    # I

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public numStyles()I
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v0, v0

    return v0
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 9
    .param p1, "out"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 130
    const/4 v1, 0x0

    .line 137
    .local v1, "offset":I
    const/16 v5, 0x12

    iput v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    .line 140
    iget v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    add-int/lit8 v5, v5, 0x2

    new-array v0, v5, [B

    .line 142
    .local v0, "buf":[B
    iget v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_cbStshi:I

    int-to-short v5, v5

    invoke-static {v0, v1, v5}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 143
    add-int/lit8 v1, v1, 0x2

    .line 145
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    iget-object v6, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v6, v6

    invoke-virtual {v5, v6}, Lorg/apache/poi/hwpf/model/Stshif;->setCstd(I)V

    .line 146
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_stshif:Lorg/apache/poi/hwpf/model/Stshif;

    invoke-virtual {v5, v0, v1}, Lorg/apache/poi/hwpf/model/Stshif;->serialize([BI)V

    .line 150
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 152
    const/4 v5, 0x2

    new-array v2, v5, [B

    .line 153
    .local v2, "sizeHolder":[B
    const/4 v4, 0x0

    .local v4, "x":I
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    array-length v5, v5

    if-lt v4, v5, :cond_0

    .line 177
    return-void

    .line 155
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v5, v5, v4

    if-eqz v5, :cond_2

    .line 157
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hwpf/model/StyleDescription;

    aget-object v5, v5, v4

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/StyleDescription;->toByteArray()[B

    move-result-object v3

    .line 160
    .local v3, "std":[B
    array-length v5, v3

    array-length v6, v3

    rem-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    int-to-short v5, v5

    invoke-static {v2, v5}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 161
    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 162
    invoke-virtual {p1, v3}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 165
    array-length v5, v3

    rem-int/lit8 v5, v5, 0x2

    if-ne v5, v8, :cond_1

    .line 167
    invoke-virtual {p1, v7}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write(I)V

    .line 153
    .end local v3    # "std":[B
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 172
    :cond_2
    aput-byte v7, v2, v7

    .line 173
    aput-byte v7, v2, v8

    .line 174
    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    goto :goto_1
.end method
