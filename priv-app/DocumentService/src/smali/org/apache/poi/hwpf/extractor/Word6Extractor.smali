.class public final Lorg/apache/poi/hwpf/extractor/Word6Extractor;
.super Lorg/apache/poi/POIOLE2TextExtractor;
.source "Word6Extractor.java"


# instance fields
.field private doc:Lorg/apache/poi/hwpf/HWPFOldDocument;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v0, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/extractor/Word6Extractor;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/HWPFOldDocument;)V
    .locals 0
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFOldDocument;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lorg/apache/poi/POIOLE2TextExtractor;-><init>(Lorg/apache/poi/POIDocument;)V

    .line 86
    iput-object p1, p0, Lorg/apache/poi/hwpf/extractor/Word6Extractor;->doc:Lorg/apache/poi/hwpf/HWPFOldDocument;

    .line 87
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/extractor/Word6Extractor;-><init>(Lorg/apache/poi/hwpf/HWPFOldDocument;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/extractor/Word6Extractor;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/extractor/Word6Extractor;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 62
    return-void
.end method


# virtual methods
.method public getParagraphText()[Ljava/lang/String;
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 99
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/hwpf/extractor/Word6Extractor;->doc:Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFOldDocument;->getRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v2

    .line 101
    .local v2, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-static {v2}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 115
    .end local v2    # "r":Lorg/apache/poi/hwpf/usermodel/Range;
    .local v3, "ret":[Ljava/lang/String;
    :cond_0
    return-object v3

    .line 102
    .end local v3    # "ret":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lorg/apache/poi/hwpf/extractor/Word6Extractor;->doc:Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFOldDocument;->getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/String;

    .line 106
    .restart local v3    # "ret":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 107
    iget-object v4, p0, Lorg/apache/poi/hwpf/extractor/Word6Extractor;->doc:Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFOldDocument;->getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 110
    aget-object v4, v3, v1

    const-string/jumbo v5, "\r"

    const-string/jumbo v6, "\ufffe"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 111
    aget-object v4, v3, v1

    const-string/jumbo v5, "\ufffe"

    const-string/jumbo v6, "\r\n"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 7

    .prologue
    .line 122
    :try_start_0
    new-instance v3, Lorg/apache/poi/hwpf/converter/WordToTextConverter;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;-><init>()V

    .line 123
    .local v3, "wordToTextConverter":Lorg/apache/poi/hwpf/converter/WordToTextConverter;
    iget-object v4, p0, Lorg/apache/poi/hwpf/extractor/Word6Extractor;->doc:Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 124
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 136
    .end local v3    # "wordToTextConverter":Lorg/apache/poi/hwpf/converter/WordToTextConverter;
    :goto_0
    return-object v4

    .line 126
    :catch_0
    move-exception v0

    .line 129
    .local v0, "exc":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 131
    .local v2, "text":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/extractor/Word6Extractor;->getParagraphText()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_1
    if-lt v4, v6, :cond_0

    .line 136
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 131
    :cond_0
    aget-object v1, v5, v4

    .line 133
    .local v1, "t":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
