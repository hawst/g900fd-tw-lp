.class public final Lorg/apache/poi/hwpf/model/ListData;
.super Ljava/lang/Object;
.source "ListData.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _levels:[Lorg/apache/poi/hwpf/model/ListLevel;

.field private _lstf:Lorg/apache/poi/hwpf/model/LSTF;


# direct methods
.method public constructor <init>(IZ)V
    .locals 4
    .param p1, "listID"    # I
    .param p2, "numbered"    # Z

    .prologue
    const/16 v3, 0x9

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v1, Lorg/apache/poi/hwpf/model/LSTF;

    invoke-direct {v1}, Lorg/apache/poi/hwpf/model/LSTF;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    .line 48
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hwpf/model/LSTF;->setLsid(I)V

    .line 49
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    new-array v2, v3, [S

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/model/LSTF;->setRgistdPara([S)V

    .line 50
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/LSTF;->getRgistdPara()[S

    move-result-object v1

    const/16 v2, 0xfff

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([SS)V

    .line 52
    new-array v1, v3, [Lorg/apache/poi/hwpf/model/ListLevel;

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    .line 53
    const/4 v0, 0x0

    .local v0, "x":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 57
    return-void

    .line 55
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    new-instance v2, Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-direct {v2, v0, p2}, Lorg/apache/poi/hwpf/model/ListLevel;-><init>(IZ)V

    aput-object v2, v1, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lorg/apache/poi/hwpf/model/LSTF;

    invoke-direct {v0, p1, p2}, Lorg/apache/poi/hwpf/model/LSTF;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    .line 35
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LSTF;->isFSimpleList()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/ListLevel;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    const/16 v0, 0x9

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/ListLevel;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    if-ne p0, p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 65
    goto :goto_0

    .line 66
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 67
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 68
    check-cast v0, Lorg/apache/poi/hwpf/model/ListData;

    .line 69
    .local v0, "other":Lorg/apache/poi/hwpf/model/ListData;
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 70
    goto :goto_0

    .line 71
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    if-nez v3, :cond_5

    .line 73
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    if-eqz v3, :cond_0

    move v1, v2

    .line 74
    goto :goto_0

    .line 76
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/LSTF;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 77
    goto :goto_0
.end method

.method public getLevel(I)Lorg/apache/poi/hwpf/model/ListLevel;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getLevelStyle(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LSTF;->getRgistdPara()[S

    move-result-object v0

    aget-short v0, v0, p1

    return v0
.end method

.method public getLevels()[Lorg/apache/poi/hwpf/model/ListLevel;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    return-object v0
.end method

.method public getLsid()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LSTF;->getLsid()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 111
    const/16 v0, 0x1f

    .line 112
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 113
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 114
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 115
    return v1

    .line 114
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/LSTF;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public numLevels()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    array-length v0, v0

    return v0
.end method

.method resetListID()I
    .locals 6

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/LSTF;->setLsid(I)V

    .line 126
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LSTF;->getLsid()I

    move-result v0

    return v0
.end method

.method public setLevel(ILorg/apache/poi/hwpf/model/ListLevel;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "level"    # Lorg/apache/poi/hwpf/model/ListLevel;

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_levels:[Lorg/apache/poi/hwpf/model/ListLevel;

    aput-object p2, v0, p1

    .line 132
    return-void
.end method

.method public setLevelStyle(II)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "styleIndex"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LSTF;->getRgistdPara()[S

    move-result-object v0

    int-to-short v1, p2

    aput-short v1, v0, p1

    .line 137
    return-void
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListData;->_lstf:Lorg/apache/poi/hwpf/model/LSTF;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LSTF;->serialize()[B

    move-result-object v0

    return-object v0
.end method
