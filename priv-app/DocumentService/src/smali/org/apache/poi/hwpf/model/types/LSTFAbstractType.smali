.class public abstract Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;
.super Ljava/lang/Object;
.source "LSTFAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final fAutoNum:Lorg/apache/poi/util/BitField;

.field private static final fHybrid:Lorg/apache/poi/util/BitField;

.field private static final fSimpleList:Lorg/apache/poi/util/BitField;

.field private static final reserved1:Lorg/apache/poi/util/BitField;

.field private static final unused1:Lorg/apache/poi/util/BitField;

.field private static final unused2:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_lsid:I

.field protected field_2_tplc:I

.field protected field_3_rgistdPara:[S

.field protected field_4_flags:B

.field protected field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fSimpleList:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    .line 52
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fAutoNum:Lorg/apache/poi/util/BitField;

    .line 53
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->unused2:Lorg/apache/poi/util/BitField;

    .line 54
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fHybrid:Lorg/apache/poi/util/BitField;

    .line 55
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xe0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->reserved1:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    new-array v0, v0, [S

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    .line 61
    new-instance v0, Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Grfhic;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 62
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 94
    const/16 v0, 0x1c

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    if-ne p0, p1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 103
    goto :goto_0

    .line 104
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 105
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 106
    check-cast v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;

    .line 107
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 108
    goto :goto_0

    .line 109
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 110
    goto :goto_0

    .line 111
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([S[S)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 112
    goto :goto_0

    .line 113
    :cond_6
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 114
    goto :goto_0

    .line 115
    :cond_7
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-nez v3, :cond_8

    .line 117
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-eqz v3, :cond_0

    move v1, v2

    .line 118
    goto :goto_0

    .line 120
    :cond_8
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Grfhic;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 121
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 66
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    .line 67
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    .line 68
    add-int/lit8 v0, p2, 0x8

    const/16 v1, 0x12

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShortArray([BII)[S

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    .line 69
    add-int/lit8 v0, p2, 0x1a

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 70
    new-instance v0, Lorg/apache/poi/hwpf/model/Grfhic;

    add-int/lit8 v1, p2, 0x1b

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Grfhic;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 71
    return-void
.end method

.method public getFlags()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 223
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    return v0
.end method

.method public getGrfhic()Lorg/apache/poi/hwpf/model/Grfhic;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    return-object v0
.end method

.method public getLsid()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 169
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    return v0
.end method

.method public getReserved1()B
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 376
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->reserved1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getRgistdPara()[S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    return-object v0
.end method

.method public getTplc()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 187
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 128
    const/16 v0, 0x1f

    .line 129
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 130
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    add-int/lit8 v1, v2, 0x1f

    .line 131
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    add-int v1, v2, v3

    .line 132
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([S)I

    move-result v3

    add-int v1, v2, v3

    .line 133
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    add-int v1, v2, v3

    .line 134
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/Grfhic;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 135
    return v1
.end method

.method public isFAutoNum()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 312
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fAutoNum:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHybrid()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 354
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fHybrid:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSimpleList()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 270
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fSimpleList:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused1()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 292
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused2()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 334
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->unused2:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 75
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 76
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 77
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShortArray([BI[S)V

    .line 78
    add-int/lit8 v0, p2, 0x1a

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    aput-byte v1, p1, v0

    .line 79
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    add-int/lit8 v1, p2, 0x1b

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Grfhic;->serialize([BI)V

    .line 80
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 85
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->serialize([BI)V

    .line 86
    return-object v0
.end method

.method public setFAutoNum(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 302
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fAutoNum:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 303
    return-void
.end method

.method public setFHybrid(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 344
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fHybrid:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 345
    return-void
.end method

.method public setFSimpleList(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 260
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->fSimpleList:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 261
    return-void
.end method

.method public setFlags(B)V
    .locals 0
    .param p1, "field_4_flags"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 232
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 233
    return-void
.end method

.method public setGrfhic(Lorg/apache/poi/hwpf/model/Grfhic;)V
    .locals 0
    .param p1, "field_5_grfhic"    # Lorg/apache/poi/hwpf/model/Grfhic;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 250
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_5_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 251
    return-void
.end method

.method public setLsid(I)V
    .locals 0
    .param p1, "field_1_lsid"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 178
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_1_lsid:I

    .line 179
    return-void
.end method

.method public setReserved1(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 364
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->reserved1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 365
    return-void
.end method

.method public setRgistdPara([S)V
    .locals 0
    .param p1, "field_3_rgistdPara"    # [S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 214
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_3_rgistdPara:[S

    .line 215
    return-void
.end method

.method public setTplc(I)V
    .locals 0
    .param p1, "field_2_tplc"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 196
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_2_tplc:I

    .line 197
    return-void
.end method

.method public setUnused1(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 280
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 281
    return-void
.end method

.method public setUnused2(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->unused2:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->field_4_flags:B

    .line 323
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 141
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[LSTF]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string/jumbo v1, "    .lsid                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getLsid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string/jumbo v1, "    .tplc                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getTplc()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v1, "    .rgistdPara           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getRgistdPara()[S

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string/jumbo v1, "    .flags                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getFlags()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, "         .fSimpleList              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->isFSimpleList()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 151
    const-string/jumbo v1, "         .unused1                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->isUnused1()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v1, "         .fAutoNum                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->isFAutoNum()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    const-string/jumbo v1, "         .unused2                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->isUnused2()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 154
    const-string/jumbo v1, "         .fHybrid                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->isFHybrid()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v1, "         .reserved1                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getReserved1()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v1, "    .grfhic               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LSTFAbstractType;->getGrfhic()Lorg/apache/poi/hwpf/model/Grfhic;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string/jumbo v1, "[/LSTF]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
