.class public final Lorg/apache/poi/hwpf/model/OldPAPBinTable;
.super Lorg/apache/poi/hwpf/model/PAPBinTable;
.source "OldPAPBinTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>([BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 10
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/PAPBinTable;-><init>()V

    .line 39
    new-instance v0, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v8, 0x2

    invoke-direct {v0, p1, p2, p3, v8}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 41
    .local v0, "binTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v1

    .line 42
    .local v1, "length":I
    const/4 v7, 0x0

    .local v7, "x":I
    :goto_0
    if-lt v7, v1, :cond_0

    .line 58
    return-void

    .line 44
    :cond_0
    invoke-virtual {v0, v7}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 46
    .local v2, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v8

    invoke-static {v8}, Lorg/apache/poi/util/LittleEndian;->getUShort([B)I

    move-result v3

    .line 47
    .local v3, "pageNum":I
    mul-int/lit16 v4, v3, 0x200

    .line 49
    .local v4, "pageOffset":I
    new-instance v6, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;

    invoke-direct {v6, p1, p1, v4, p5}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;-><init>([B[BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 52
    .local v6, "pfkp":Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PAPFormattedDiskPage;->getPAPXs()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 42
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 52
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/PAPX;

    .line 54
    .local v5, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    if-eqz v5, :cond_1

    .line 55
    iget-object v9, p0, Lorg/apache/poi/hwpf/model/OldPAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
