.class public final Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;
.super Lorg/apache/poi/hwpf/sprm/SprmUncompressor;
.source "CharacterSprmUncompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 37
    sput-object v0, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/poi/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 42
    return-void
.end method

.method private static applySprms(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BIZLorg/apache/poi/hwpf/usermodel/CharacterProperties;)V
    .locals 6
    .param p0, "parentProperties"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I
    .param p3, "warnAboutNonChpSprms"    # Z
    .param p4, "targetProperties"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .prologue
    .line 105
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-direct {v1, p1, p2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 107
    .local v1, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 123
    return-void

    .line 109
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v0

    .line 111
    .local v0, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    .line 113
    if-eqz p3, :cond_0

    .line 115
    sget-object v2, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    .line 116
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Non-CHP SPRM returned by SprmIterator: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 115
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_2
    invoke-static {p0, p4, v0}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->unCompressCHPOperation(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V

    goto :goto_0
.end method

.method private static getCHPFlag(BZ)Z
    .locals 4
    .param p0, "x"    # B
    .param p1, "oldVal"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 737
    if-nez p0, :cond_1

    .line 755
    :cond_0
    :goto_0
    return v0

    .line 741
    :cond_1
    if-ne p0, v1, :cond_2

    move v0, v1

    .line 743
    goto :goto_0

    .line 745
    :cond_2
    and-int/lit16 v2, p0, 0x81

    const/16 v3, 0x80

    if-ne v2, v3, :cond_3

    move v0, p1

    .line 747
    goto :goto_0

    .line 749
    :cond_3
    and-int/lit16 v2, p0, 0x81

    const/16 v3, 0x81

    if-ne v2, v3, :cond_0

    .line 751
    if-nez p1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static getIstd([BI)Ljava/lang/Integer;
    .locals 7
    .param p0, "grpprl"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 127
    const/4 v3, 0x0

    .line 130
    .local v3, "style":Ljava/lang/Integer;
    :try_start_0
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-direct {v2, p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 131
    .local v2, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 147
    .end local v2    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :goto_1
    return-object v3

    .line 133
    .restart local v2    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v1

    .line 135
    .local v1, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v4

    const/16 v5, 0x30

    if-ne v4, v5, :cond_0

    .line 138
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 142
    .end local v1    # "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    .end local v2    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "exc":Ljava/lang/Exception;
    sget-object v4, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x7

    .line 145
    const-string/jumbo v6, "Unable to extract istd from direct CHP SPRM: "

    .line 144
    invoke-virtual {v4, v5, v6, v0, v0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static unCompressCHPOperation(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    .locals 25
    .param p0, "oldCHP"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .param p1, "newCHP"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .param p2, "sprm"    # Lorg/apache/poi/hwpf/sprm/SprmOperation;

    .prologue
    .line 169
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v21

    packed-switch v21, :pswitch_data_0

    .line 715
    sget-object v21, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    const/16 v22, 0x1

    new-instance v23, Ljava/lang/StringBuilder;

    const-string/jumbo v24, "Unknown CHP sprm ignored: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 718
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 172
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFRMarkDel(Z)V

    goto :goto_0

    .line 175
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFRMark(Z)V

    goto :goto_0

    .line 178
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFFldVanish(Z)V

    goto :goto_0

    .line 191
    :pswitch_4
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFcPic(I)V

    .line 192
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSpec(Z)V

    goto :goto_0

    .line 195
    :pswitch_5
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIbstRMark(I)V

    goto :goto_0

    .line 198
    :pswitch_6
    new-instance v21, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v23

    invoke-direct/range {v21 .. v23}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>([BI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    goto :goto_0

    .line 201
    :pswitch_7
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFData(Z)V

    goto :goto_0

    .line 208
    :pswitch_8
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v18

    .line 209
    .local v18, "operand":I
    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-short v7, v0

    .line 210
    .local v7, "chsDiff":S
    invoke-static {v7}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFChsDiff(Z)V

    .line 211
    const v21, 0xffff00

    and-int v21, v21, v18

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setChse(S)V

    goto/16 :goto_0

    .line 214
    .end local v7    # "chsDiff":S
    .end local v18    # "operand":I
    :pswitch_9
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSpec(Z)V

    .line 215
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcSym(I)V

    .line 216
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v22

    add-int/lit8 v22, v22, 0x2

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setXchSym(I)V

    goto/16 :goto_0

    .line 219
    :pswitch_a
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFOle2(Z)V

    goto/16 :goto_0

    .line 226
    :pswitch_b
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIcoHighlight(B)V

    .line 227
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFHighlight(Z)V

    goto/16 :goto_0

    .line 234
    :pswitch_c
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFcObj(I)V

    goto/16 :goto_0

    .line 316
    :pswitch_d
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIstd(I)V

    goto/16 :goto_0

    .line 324
    :pswitch_e
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFBold(Z)V

    .line 325
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFItalic(Z)V

    .line 326
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFOutline(Z)V

    .line 327
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFStrike(Z)V

    .line 328
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFShadow(Z)V

    .line 329
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSmallCaps(Z)V

    .line 330
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFCaps(Z)V

    .line 331
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFVanish(Z)V

    .line 332
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setKul(B)V

    .line 333
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIco(B)V

    goto/16 :goto_0

    .line 337
    :pswitch_f
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSpec()Z

    move-result v9

    .line 338
    .local v9, "fSpec":Z
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object p1

    .line 339
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSpec(Z)V

    goto/16 :goto_0

    .line 345
    .end local v9    # "fSpec":Z
    :pswitch_10
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFBold()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFBold(Z)V

    goto/16 :goto_0

    .line 348
    :pswitch_11
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFItalic()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFItalic(Z)V

    goto/16 :goto_0

    .line 351
    :pswitch_12
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFStrike()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFStrike(Z)V

    goto/16 :goto_0

    .line 354
    :pswitch_13
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFOutline()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFOutline(Z)V

    goto/16 :goto_0

    .line 357
    :pswitch_14
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFShadow()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFShadow(Z)V

    goto/16 :goto_0

    .line 360
    :pswitch_15
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFSmallCaps()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSmallCaps(Z)V

    goto/16 :goto_0

    .line 363
    :pswitch_16
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFCaps()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFCaps(Z)V

    goto/16 :goto_0

    .line 366
    :pswitch_17
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFVanish()Z

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFVanish(Z)V

    goto/16 :goto_0

    .line 369
    :pswitch_18
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcAscii(I)V

    goto/16 :goto_0

    .line 372
    :pswitch_19
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setKul(B)V

    goto/16 :goto_0

    .line 375
    :pswitch_1a
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v18

    .line 376
    .restart local v18    # "operand":I
    move/from16 v0, v18

    and-int/lit16 v10, v0, 0xff

    .line 377
    .local v10, "hps":I
    if-eqz v10, :cond_1

    .line 379
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    .line 383
    :cond_1
    const v21, 0xff00

    and-int v21, v21, v18

    ushr-int/lit8 v21, v21, 0x8

    move/from16 v0, v21

    int-to-byte v6, v0

    .line 384
    .local v6, "cInc":B
    ushr-int/lit8 v21, v6, 0x1

    move/from16 v0, v21

    int-to-byte v6, v0

    .line 385
    if-eqz v6, :cond_2

    .line 387
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    mul-int/lit8 v22, v6, 0x2

    add-int v21, v21, v22

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    .line 391
    :cond_2
    const/high16 v21, 0xff0000

    and-int v21, v21, v18

    ushr-int/lit8 v21, v21, 0x10

    move/from16 v0, v21

    int-to-byte v12, v0

    .line 392
    .local v12, "hpsPos":B
    const/16 v21, 0x80

    move/from16 v0, v21

    if-eq v12, v0, :cond_3

    .line 394
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHpsPos(S)V

    .line 396
    :cond_3
    move/from16 v0, v18

    and-int/lit16 v0, v0, 0x100

    move/from16 v21, v0

    if-lez v21, :cond_5

    const/4 v8, 0x1

    .line 397
    .local v8, "fAdjust":Z
    :goto_1
    if-eqz v8, :cond_4

    const/16 v21, 0x80

    move/from16 v0, v21

    if-eq v12, v0, :cond_4

    if-eqz v12, :cond_4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v21

    if-nez v21, :cond_4

    .line 399
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    add-int/lit8 v21, v21, -0x2

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    .line 401
    :cond_4
    if-eqz v8, :cond_0

    if-nez v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v21

    if-eqz v21, :cond_0

    .line 403
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    add-int/lit8 v21, v21, 0x2

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 396
    .end local v8    # "fAdjust":Z
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 407
    .end local v6    # "cInc":B
    .end local v10    # "hps":I
    .end local v12    # "hpsPos":B
    .end local v18    # "operand":I
    :pswitch_1b
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDxaSpace(I)V

    goto/16 :goto_0

    .line 410
    :pswitch_1c
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setLidDefault(I)V

    goto/16 :goto_0

    .line 413
    :pswitch_1d
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIco(B)V

    goto/16 :goto_0

    .line 416
    :pswitch_1e
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 419
    :pswitch_1f
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v11, v0

    .line 420
    .local v11, "hpsLvl":B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    mul-int/lit8 v22, v11, 0x2

    add-int v21, v21, v22

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 423
    .end local v11    # "hpsLvl":B
    :pswitch_20
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHpsPos(S)V

    goto/16 :goto_0

    .line 426
    :pswitch_21
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    if-eqz v21, :cond_6

    .line 428
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v21

    if-nez v21, :cond_0

    .line 430
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    add-int/lit8 v21, v21, -0x2

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 435
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHpsPos()S

    move-result v21

    if-eqz v21, :cond_0

    .line 437
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    add-int/lit8 v21, v21, 0x2

    const/16 v22, 0x2

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 514
    :pswitch_22
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIss(B)V

    goto/16 :goto_0

    .line 517
    :pswitch_23
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 520
    :pswitch_24
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v14

    .line 521
    .local v14, "increment":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    add-int v21, v21, v14

    const/16 v22, 0x8

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(II)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 524
    .end local v14    # "increment":I
    :pswitch_25
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHpsKern(I)V

    goto/16 :goto_0

    .line 535
    :pswitch_26
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x42c80000    # 100.0f

    div-float v19, v21, v22

    .line 536
    .local v19, "percentage":F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v19

    move/from16 v0, v21

    float-to-int v4, v0

    .line 537
    .local v4, "add":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->getHps()I

    move-result v21

    add-int v21, v21, v4

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 541
    .end local v4    # "add":I
    .end local v19    # "percentage":F
    :pswitch_27
    new-instance v13, Lorg/apache/poi/hwpf/model/Hyphenation;

    .line 542
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    .line 541
    move/from16 v0, v21

    invoke-direct {v13, v0}, Lorg/apache/poi/hwpf/model/Hyphenation;-><init>(S)V

    .line 543
    .local v13, "hyphenation":Lorg/apache/poi/hwpf/model/Hyphenation;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setHresi(Lorg/apache/poi/hwpf/model/Hyphenation;)V

    goto/16 :goto_0

    .line 546
    .end local v13    # "hyphenation":Lorg/apache/poi/hwpf/model/Hyphenation;
    :pswitch_28
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcAscii(I)V

    goto/16 :goto_0

    .line 549
    :pswitch_29
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcFE(I)V

    goto/16 :goto_0

    .line 552
    :pswitch_2a
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFtcOther(I)V

    goto/16 :goto_0

    .line 558
    :pswitch_2b
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFDStrike(Z)V

    goto/16 :goto_0

    .line 561
    :pswitch_2c
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFImprint(Z)V

    goto/16 :goto_0

    .line 565
    :pswitch_2d
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFSpec(Z)V

    goto/16 :goto_0

    .line 568
    :pswitch_2e
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFObj(Z)V

    goto/16 :goto_0

    .line 583
    :pswitch_2f
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v5

    .line 584
    .local v5, "buf":[B
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v16

    .line 585
    .local v16, "offset":I
    aget-byte v21, v5, v16

    if-eqz v21, :cond_7

    const/16 v21, 0x1

    :goto_2
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFPropRMark(Z)V

    .line 586
    add-int/lit8 v21, v16, 0x1

    move/from16 v0, v21

    invoke-static {v5, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIbstPropRMark(I)V

    .line 587
    new-instance v21, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    add-int/lit8 v22, v16, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v5, v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>([BI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    goto/16 :goto_0

    .line 585
    :cond_7
    const/16 v21, 0x0

    goto :goto_2

    .line 590
    .end local v5    # "buf":[B
    .end local v16    # "offset":I
    :pswitch_30
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getFlag(I)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFEmboss(Z)V

    goto/16 :goto_0

    .line 593
    :pswitch_31
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setSfxtText(B)V

    goto/16 :goto_0

    .line 632
    :pswitch_32
    const/16 v21, 0x20

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 633
    .local v20, "xstDispFldRMark":[B
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v5

    .line 634
    .restart local v5    # "buf":[B
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v16

    .line 635
    .restart local v16    # "offset":I
    aget-byte v21, v5, v16

    if-eqz v21, :cond_8

    const/16 v21, 0x1

    :goto_3
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFDispFldRMark(Z)V

    .line 636
    add-int/lit8 v21, v16, 0x1

    move/from16 v0, v21

    invoke-static {v5, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIbstDispFldRMark(I)V

    .line 637
    new-instance v21, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    add-int/lit8 v22, v16, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v5, v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>([BI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmDispFldRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    .line 638
    add-int/lit8 v21, v16, 0x7

    const/16 v22, 0x0

    const/16 v23, 0x20

    move/from16 v0, v21

    move-object/from16 v1, v20

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-static {v5, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 639
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setXstDispFldRMark([B)V

    goto/16 :goto_0

    .line 635
    :cond_8
    const/16 v21, 0x0

    goto :goto_3

    .line 642
    .end local v5    # "buf":[B
    .end local v16    # "offset":I
    .end local v20    # "xstDispFldRMark":[B
    :pswitch_33
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIbstRMarkDel(I)V

    goto/16 :goto_0

    .line 645
    :pswitch_34
    new-instance v21, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v23

    invoke-direct/range {v21 .. v23}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>([BI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setDttmRMarkDel(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    goto/16 :goto_0

    .line 648
    :pswitch_35
    new-instance v21, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v23

    invoke-direct/range {v21 .. v23}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setBrc(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 658
    :pswitch_36
    new-instance v17, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;

    .line 659
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v22

    .line 658
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;-><init>([BI)V

    .line 661
    .local v17, "oldDescriptor":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->toShadingDescriptor()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v15

    .line 662
    .local v15, "newDescriptor":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    goto/16 :goto_0

    .line 679
    .end local v15    # "newDescriptor":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .end local v17    # "oldDescriptor":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;
    :pswitch_37
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setLidDefault(I)V

    goto/16 :goto_0

    .line 682
    :pswitch_38
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setLidFE(I)V

    goto/16 :goto_0

    .line 685
    :pswitch_39
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setIdctHint(B)V

    goto/16 :goto_0

    .line 689
    :pswitch_3a
    new-instance v21, Lorg/apache/poi/hwpf/model/Colorref;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v22

    invoke-direct/range {v21 .. v22}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setCv(Lorg/apache/poi/hwpf/model/Colorref;)V

    goto/16 :goto_0

    .line 711
    :pswitch_3b
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v21

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    .line 712
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->isFNoProof()Z

    move-result v22

    .line 711
    invoke-static/range {v21 .. v22}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->setFNoProof(Z)V

    goto/16 :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_0
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3b
    .end packed-switch
.end method

.method public static uncompressCHP(Lorg/apache/poi/hwpf/model/StyleSheet;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BI)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 9
    .param p0, "styleSheet"    # Lorg/apache/poi/hwpf/model/StyleSheet;
    .param p1, "parStyle"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .param p2, "grpprl"    # [B
    .param p3, "offset"    # I

    .prologue
    const/4 v1, 0x7

    .line 57
    if-nez p1, :cond_1

    .line 59
    new-instance p1, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    .end local p1    # "parStyle":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-direct {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;-><init>()V

    .line 60
    .restart local p1    # "parStyle":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    new-instance v7, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    invoke-direct {v7}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;-><init>()V

    .line 71
    .local v7, "newProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    :goto_0
    invoke-static {p2, p3}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->getIstd([BI)Ljava/lang/Integer;

    move-result-object v3

    .line 72
    .local v3, "style":Ljava/lang/Integer;
    if-eqz v3, :cond_0

    .line 76
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCHPX(I)[B

    move-result-object v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {p1, v0, v2, v4, v7}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->applySprms(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BIZLorg/apache/poi/hwpf/usermodel/CharacterProperties;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :cond_0
    :goto_1
    move-object v8, v7

    .line 87
    .local v8, "styleProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v7

    .line 91
    const/4 v0, 0x1

    :try_start_1
    invoke-static {v8, p2, p3, v0, v7}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->applySprms(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BIZLorg/apache/poi/hwpf/usermodel/CharacterProperties;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 98
    :goto_2
    return-object v7

    .line 64
    .end local v3    # "style":Ljava/lang/Integer;
    .end local v7    # "newProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .end local v8    # "styleProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v7

    .restart local v7    # "newProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    goto :goto_0

    .line 79
    .restart local v3    # "style":Ljava/lang/Integer;
    :catch_0
    move-exception v5

    .line 81
    .local v5, "exc":Ljava/lang/Exception;
    sget-object v0, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v2, "Unable to apply all style "

    .line 82
    const-string/jumbo v4, " CHP SPRMs to CHP: "

    move-object v6, v5

    .line 81
    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 93
    .end local v5    # "exc":Ljava/lang/Exception;
    .restart local v8    # "styleProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    :catch_1
    move-exception v5

    .line 95
    .restart local v5    # "exc":Ljava/lang/Exception;
    sget-object v0, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    .line 96
    const-string/jumbo v2, "Unable to process all direct CHP SPRMs: "

    .line 95
    invoke-virtual {v0, v1, v2, v5, v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static uncompressCHP(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BI)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .locals 2
    .param p0, "parent"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/CharacterProperties;->clone()Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 49
    .local v0, "newProperties":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v1, v0}, Lorg/apache/poi/hwpf/sprm/CharacterSprmUncompressor;->applySprms(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;[BIZLorg/apache/poi/hwpf/usermodel/CharacterProperties;)V

    .line 50
    return-object v0
.end method
