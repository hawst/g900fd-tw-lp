.class final Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;
.super Ljava/lang/Object;
.source "FieldsImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/usermodel/FieldsImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PlexOfFieldComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/poi/hwpf/model/PlexOfField;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;)V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/model/PlexOfField;

    check-cast p2, Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/FieldsImpl$PlexOfFieldComparator;->compare(Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;)I
    .locals 3
    .param p1, "o1"    # Lorg/apache/poi/hwpf/model/PlexOfField;
    .param p2, "o2"    # Lorg/apache/poi/hwpf/model/PlexOfField;

    .prologue
    .line 273
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v1

    .line 274
    .local v1, "thisVal":I
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v0

    .line 275
    .local v0, "anotherVal":I
    if-ge v1, v0, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    if-ne v1, v0, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
