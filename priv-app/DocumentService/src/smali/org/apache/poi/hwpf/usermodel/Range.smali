.class public Lorg/apache/poi/hwpf/usermodel/Range;
.super Ljava/lang/Object;
.source "Range.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final TYPE_CHARACTER:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_LISTENTRY:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_PARAGRAPH:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_SECTION:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_TABLE:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_TEXT:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_UNDEFINED:I = 0x6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field protected _charEnd:I

.field protected _charRangeFound:Z

.field protected _charStart:I

.field protected _characters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field

.field protected _doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

.field protected _end:I

.field protected _parEnd:I

.field protected _parRangeFound:Z

.field protected _parStart:I

.field protected _paragraphs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field

.field private _parent:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Range;",
            ">;"
        }
    .end annotation
.end field

.field protected _sectionEnd:I

.field _sectionRangeFound:Z

.field protected _sectionStart:I

.field protected _sections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation
.end field

.field protected _start:I

.field protected _text:Ljava/lang/StringBuilder;

.field private logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    .line 74
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(IIILorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 1
    .param p1, "startIdx"    # I
    .param p2, "endIdx"    # I
    .param p3, "idxType"    # I
    .param p4, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-class v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->logger:Lorg/apache/poi/util/POILogger;

    .line 195
    iget-object v0, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .line 196
    iget-object v0, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    .line 197
    iget-object v0, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 198
    iget-object v0, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    .line 199
    iget-object v0, p4, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    .line 200
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    .line 202
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->sanityCheckStartEnd()V

    .line 203
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "doc"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-class v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->logger:Lorg/apache/poi/util/POILogger;

    .line 144
    iput p1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    .line 145
    iput p2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 146
    iput-object p3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .line 147
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getSectionTable()Lorg/apache/poi/hwpf/model/SectionTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/SectionTable;->getSections()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    .line 148
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPBinTable;->getParagraphs()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 149
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPBinTable;->getTextRuns()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    .line 150
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getText()Ljava/lang/StringBuilder;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    .line 151
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    .line 153
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->sanityCheckStartEnd()V

    .line 154
    return-void
.end method

.method protected constructor <init>(IILorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-class v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->logger:Lorg/apache/poi/util/POILogger;

    .line 167
    iput p1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    .line 168
    iput p2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 169
    iget-object v0, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    .line 170
    iget-object v0, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    .line 171
    iget-object v0, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 172
    iget-object v0, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    .line 173
    iget-object v0, p3, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    .line 174
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    .line 176
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->sanityCheckStartEnd()V

    .line 177
    sget-boolean v0, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->sanityCheck()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 178
    :cond_0
    return-void
.end method

.method private adjustForInsert(I)V
    .locals 2
    .param p1, "length"    # I

    .prologue
    .line 1253
    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 1255
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->reset()V

    .line 1256
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/Range;

    .line 1257
    .local v0, "parent":Lorg/apache/poi/hwpf/usermodel/Range;
    if-eqz v0, :cond_0

    .line 1258
    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/usermodel/Range;->adjustForInsert(I)V

    .line 1260
    :cond_0
    return-void
.end method

.method private static binarySearchEnd(Ljava/util/List;II)I
    .locals 5
    .param p1, "foundStart"    # I
    .param p2, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/poi/hwpf/model/PropertyNode",
            "<*>;>;II)I"
        }
    .end annotation

    .prologue
    .line 1048
    .local p0, "rpl":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/hwpf/model/PropertyNode<*>;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v4

    if-gt v4, p2, :cond_1

    .line 1049
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 1075
    :cond_0
    :goto_0
    return v2

    .line 1051
    :cond_1
    move v1, p1

    .line 1052
    .local v1, "low":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 1054
    .local v0, "high":I
    :goto_1
    if-le v1, v0, :cond_3

    .line 1073
    sget-boolean v4, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    if-ltz v1, :cond_2

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_6

    :cond_2
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1056
    :cond_3
    add-int v4, v1, v0

    ushr-int/lit8 v2, v4, 0x1

    .line 1057
    .local v2, "mid":I
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/PropertyNode;

    .line 1059
    .local v3, "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v4

    if-ge v4, p2, :cond_4

    .line 1061
    add-int/lit8 v1, v2, 0x1

    .line 1062
    goto :goto_1

    .line 1063
    :cond_4
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v4

    if-le v4, p2, :cond_5

    .line 1065
    add-int/lit8 v0, v2, -0x1

    .line 1066
    goto :goto_1

    .line 1069
    :cond_5
    sget-boolean v4, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v4

    if-eq v4, p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .end local v2    # "mid":I
    .end local v3    # "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    :cond_6
    move v2, v1

    .line 1075
    goto :goto_0
.end method

.method private static binarySearchStart(Ljava/util/List;I)I
    .locals 5
    .param p1, "start"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/poi/hwpf/model/PropertyNode",
            "<*>;>;I)I"
        }
    .end annotation

    .prologue
    .local p0, "rpl":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/hwpf/model/PropertyNode<*>;>;"
    const/4 v2, 0x0

    .line 1016
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v4

    if-lt v4, p1, :cond_1

    .line 1042
    :cond_0
    :goto_0
    return v2

    .line 1019
    :cond_1
    const/4 v1, 0x0

    .line 1020
    .local v1, "low":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 1022
    .local v0, "high":I
    :goto_1
    if-le v1, v0, :cond_2

    .line 1041
    sget-boolean v4, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    if-nez v1, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1024
    :cond_2
    add-int v4, v1, v0

    ushr-int/lit8 v2, v4, 0x1

    .line 1025
    .local v2, "mid":I
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/PropertyNode;

    .line 1027
    .local v3, "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v4

    if-ge v4, p1, :cond_3

    .line 1029
    add-int/lit8 v1, v2, 0x1

    .line 1030
    goto :goto_1

    .line 1031
    :cond_3
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v4

    if-le v4, p1, :cond_4

    .line 1033
    add-int/lit8 v0, v2, -0x1

    .line 1034
    goto :goto_1

    .line 1037
    :cond_4
    sget-boolean v4, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v4

    if-eq v4, p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1042
    .end local v2    # "mid":I
    .end local v3    # "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    :cond_5
    add-int/lit8 v2, v1, -0x1

    goto :goto_0
.end method

.method private findRange(Ljava/util/List;II)[I
    .locals 5
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/poi/hwpf/model/PropertyNode",
            "<*>;>;II)[I"
        }
    .end annotation

    .prologue
    .line 1095
    .local p1, "rpl":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/hwpf/model/PropertyNode<*>;>;"
    invoke-static {p1, p2}, Lorg/apache/poi/hwpf/usermodel/Range;->binarySearchStart(Ljava/util/List;I)I

    move-result v1

    .line 1096
    .local v1, "startIndex":I
    :goto_0
    if-lez v1, :cond_0

    add-int/lit8 v2, v1, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v2

    if-ge v2, p2, :cond_3

    .line 1099
    :cond_0
    invoke-static {p1, v1, p3}, Lorg/apache/poi/hwpf/usermodel/Range;->binarySearchEnd(Ljava/util/List;II)I

    move-result v0

    .line 1100
    .local v0, "endIndex":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 1101
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v2

    .line 1100
    if-le v2, p3, :cond_4

    .line 1104
    :cond_1
    if-ltz v1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1105
    if-gt v1, v0, :cond_2

    if-ltz v0, :cond_2

    .line 1106
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_5

    .line 1107
    :cond_2
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1097
    .end local v0    # "endIndex":I
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1102
    .restart local v0    # "endIndex":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1109
    :cond_5
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    add-int/lit8 v4, v0, 0x1

    aput v4, v2, v3

    return-object v2
.end method

.method private findRange(Ljava/util/List;III)[I
    .locals 7
    .param p2, "min"    # I
    .param p3, "start"    # I
    .param p4, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/poi/hwpf/model/PropertyNode",
            "<*>;>;III)[I"
        }
    .end annotation

    .prologue
    .local p1, "rpl":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/poi/hwpf/model/PropertyNode<*>;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 1127
    move v1, p2

    .line 1129
    .local v1, "x":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, p2, :cond_0

    .line 1130
    new-array v3, v4, [I

    aput p2, v3, v5

    aput p2, v3, v6

    .line 1168
    :goto_0
    return-object v3

    .line 1132
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PropertyNode;

    .line 1134
    .local v0, "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    if-gt v3, p3, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_2

    .line 1144
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v3

    if-le v3, p4, :cond_4

    .line 1146
    new-array v3, v4, [I

    goto :goto_0

    .line 1135
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 1137
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_3

    .line 1138
    new-array v3, v4, [I

    goto :goto_0

    .line 1141
    :cond_3
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    check-cast v0, Lorg/apache/poi/hwpf/model/PropertyNode;

    .restart local v0    # "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    goto :goto_1

    .line 1149
    :cond_4
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    if-gt v3, p3, :cond_5

    .line 1151
    new-array v3, v4, [I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    aput v4, v3, v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    aput v4, v3, v6

    goto :goto_0

    .line 1154
    :cond_5
    move v2, v1

    .local v2, "y":I
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_6

    .line 1168
    new-array v3, v4, [I

    aput v1, v3, v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    aput v4, v3, v6

    goto :goto_0

    .line 1156
    :cond_6
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    check-cast v0, Lorg/apache/poi/hwpf/model/PropertyNode;

    .line 1157
    .restart local v0    # "node":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    if-nez v0, :cond_8

    .line 1154
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1160
    :cond_8
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v3

    if-ge v3, p4, :cond_9

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    if-le v3, p4, :cond_7

    .line 1163
    :cond_9
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v3

    if-ge v3, p4, :cond_a

    .line 1164
    new-array v3, v4, [I

    aput v1, v3, v5

    add-int/lit8 v4, v2, 0x1

    aput v4, v3, v6

    goto :goto_0

    .line 1166
    :cond_a
    new-array v3, v4, [I

    aput v1, v3, v5

    aput v2, v3, v6

    goto/16 :goto_0
.end method

.method private initCharacterRuns()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 993
    iget-boolean v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    if-nez v1, :cond_0

    .line 994
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;II)[I

    move-result-object v0

    .line 995
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    .line 996
    aget v1, v0, v4

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    .line 997
    iput-boolean v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    .line 999
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private initParagraphs()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 981
    iget-boolean v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    if-nez v1, :cond_0

    .line 982
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;II)[I

    move-result-object v0

    .line 983
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    .line 984
    aget v1, v0, v4

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    .line 985
    iput-boolean v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    .line 987
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private initSections()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1005
    iget-boolean v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionRangeFound:Z

    if-nez v1, :cond_0

    .line 1006
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3, v4}, Lorg/apache/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;III)[I

    move-result-object v0

    .line 1007
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    .line 1008
    aget v1, v0, v5

    iput v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionEnd:I

    .line 1009
    iput-boolean v5, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionRangeFound:Z

    .line 1011
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private sanityCheckStartEnd()V
    .locals 3

    .prologue
    .line 210
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    if-gez v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Range start must not be negative. Given "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    if-ge v0, v1, :cond_1

    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The end ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 215
    const-string/jumbo v2, ") must not be before the start ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_1
    return-void
.end method

.method public static stripFields(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x15

    const/4 v8, 0x0

    const/16 v7, 0x13

    const/4 v6, -0x1

    .line 251
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-ne v4, v6, :cond_4

    .line 289
    :cond_0
    :goto_0
    return-object p0

    .line 257
    :cond_1
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 258
    .local v0, "first13":I
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v7, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 259
    .local v3, "next13":I
    const/16 v4, 0x14

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 260
    .local v1, "first14":I
    invoke-virtual {p0, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 263
    .local v2, "last15":I
    if-lt v2, v0, :cond_0

    .line 268
    if-ne v3, v6, :cond_2

    if-ne v1, v6, :cond_2

    .line 269
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 270
    goto :goto_0

    .line 276
    :cond_2
    if-eq v1, v6, :cond_5

    if-lt v1, v3, :cond_3

    if-ne v3, v6, :cond_5

    .line 277
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 278
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 277
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 256
    .end local v0    # "first13":I
    .end local v1    # "first14":I
    .end local v2    # "last15":I
    .end local v3    # "next13":I
    :cond_4
    :goto_1
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-le v4, v6, :cond_0

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-gt v4, v6, :cond_1

    goto :goto_0

    .line 285
    .restart local v0    # "first13":I
    .restart local v1    # "first14":I
    .restart local v2    # "last15":I
    .restart local v3    # "next13":I
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method


# virtual methods
.method protected adjustFIB(I)V
    .locals 8
    .param p1, "adjustment"    # I

    .prologue
    .line 1191
    sget-boolean v4, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v4, v4, Lorg/apache/poi/hwpf/HWPFDocument;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1200
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-result-object v2

    .line 1228
    .local v2, "fib":Lorg/apache/poi/hwpf/model/FileInformationBlock;
    const/4 v0, 0x0

    .line 1229
    .local v0, "currentEnd":I
    sget-object v5, Lorg/apache/poi/hwpf/model/SubdocumentType;->ORDERED:[Lorg/apache/poi/hwpf/model/SubdocumentType;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_1

    .line 1243
    :goto_1
    return-void

    .line 1229
    :cond_1
    aget-object v3, v5, v4

    .line 1231
    .local v3, "type":Lorg/apache/poi/hwpf/model/SubdocumentType;
    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I

    move-result v1

    .line 1232
    .local v1, "currentLength":I
    add-int/2addr v0, v1

    .line 1235
    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    if-le v7, v0, :cond_2

    .line 1229
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1239
    :cond_2
    add-int v4, v1, p1

    .line 1238
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->setSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;I)V

    goto :goto_1
.end method

.method public delete()V
    .locals 12

    .prologue
    .line 549
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 551
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    .line 552
    .local v3, "numSections":I
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    .line 553
    .local v2, "numRuns":I
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    .line 555
    .local v1, "numParagraphs":I
    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    .local v7, "x":I
    :goto_0
    if-lt v7, v2, :cond_2

    .line 560
    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    :goto_1
    if-lt v7, v1, :cond_3

    .line 569
    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    :goto_2
    if-lt v7, v3, :cond_4

    .line 578
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v8, v8, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v8, :cond_0

    .line 580
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v8, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/HWPFDocument;->getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    .line 581
    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v10, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v11, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    sub-int/2addr v10, v11

    invoke-virtual {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->afterDelete(II)V

    .line 584
    :cond_0
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v10, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 585
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/usermodel/Range;

    .line 586
    .local v5, "parent":Lorg/apache/poi/hwpf/usermodel/Range;
    if-eqz v5, :cond_1

    .line 588
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    sub-int/2addr v8, v9

    neg-int v8, v8

    invoke-direct {v5, v8}, Lorg/apache/poi/hwpf/usermodel/Range;->adjustForInsert(I)V

    .line 592
    :cond_1
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    sub-int/2addr v8, v9

    neg-int v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/Range;->adjustFIB(I)V

    .line 593
    return-void

    .line 556
    .end local v5    # "parent":Lorg/apache/poi/hwpf/usermodel/Range;
    :cond_2
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 557
    .local v0, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v10, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    sub-int/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Lorg/apache/poi/hwpf/model/CHPX;->adjustForDelete(II)V

    .line 555
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 561
    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    :cond_3
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/PAPX;

    .line 564
    .local v4, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v10, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    sub-int/2addr v9, v10

    invoke-virtual {v4, v8, v9}, Lorg/apache/poi/hwpf/model/PAPX;->adjustForDelete(II)V

    .line 560
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 570
    .end local v4    # "papx":Lorg/apache/poi/hwpf/model/PAPX;
    :cond_4
    iget-object v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/hwpf/model/SEPX;

    .line 573
    .local v6, "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget v10, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    sub-int/2addr v9, v10

    invoke-virtual {v6, v8, v9}, Lorg/apache/poi/hwpf/model/SEPX;->adjustForDelete(II)V

    .line 569
    add-int/lit8 v7, v7, 0x1

    goto :goto_2
.end method

.method public getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 11
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 815
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initCharacterRuns()V

    .line 817
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    add-int/2addr v6, p1

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    if-lt v6, v7, :cond_0

    .line 818
    new-instance v6, Ljava/lang/IndexOutOfBoundsException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "CHPX #"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 819
    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    add-int/2addr v8, p1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ") not in range ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 820
    const-string/jumbo v8, "; "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 818
    invoke-direct {v6, v7}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 822
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    add-int/2addr v7, p1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/CHPX;

    .line 823
    .local v1, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    if-nez v1, :cond_2

    .line 854
    :cond_1
    :goto_0
    return-object v0

    .line 829
    :cond_2
    instance-of v6, p0, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    if-eqz v6, :cond_3

    move-object v6, p0

    .line 831
    check-cast v6, Lorg/apache/poi/hwpf/usermodel/Paragraph;

    iget-short v2, v6, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 851
    .local v2, "istd":S
    :goto_1
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v6

    invoke-direct {v0, v1, v6, v2, p0}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;-><init>(Lorg/apache/poi/hwpf/model/CHPX;Lorg/apache/poi/hwpf/model/StyleSheet;SLorg/apache/poi/hwpf/usermodel/Range;)V

    .line 854
    .local v0, "chp":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    goto :goto_0

    .line 835
    .end local v0    # "chp":Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .end local v2    # "istd":S
    :cond_3
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 836
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v7

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 837
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v8

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 835
    invoke-direct {p0, v6, v7, v8}, Lorg/apache/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;II)[I

    move-result-object v5

    .line 839
    .local v5, "point":[I
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 840
    aget v6, v5, v10

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 842
    .local v4, "parStart":I
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_1

    .line 847
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    aget v7, v5, v10

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/PAPX;

    .line 848
    .local v3, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v2

    .restart local v2    # "istd":S
    goto :goto_1
.end method

.method protected getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    return-object v0
.end method

.method public getEndOffset()I
    .locals 1

    .prologue
    .line 1273
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    return v0
.end method

.method public getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 880
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 882
    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v1, p1

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    if-lt v1, v2, :cond_0

    .line 883
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Paragraph #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 884
    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") not in range ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 885
    const-string/jumbo v3, "; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 883
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 887
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v2, p1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    .line 888
    .local v0, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-static {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->newParagraph(Lorg/apache/poi/hwpf/usermodel/Range;Lorg/apache/poi/hwpf/model/PAPX;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v1

    return-object v1
.end method

.method public getSection(I)Lorg/apache/poi/hwpf/usermodel/Section;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 865
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initSections()V

    .line 866
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    add-int/2addr v3, p1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/SEPX;

    .line 867
    .local v1, "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Section;

    invoke-direct {v0, v1, p0}, Lorg/apache/poi/hwpf/usermodel/Section;-><init>(Lorg/apache/poi/hwpf/model/SEPX;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 868
    .local v0, "sep":Lorg/apache/poi/hwpf/usermodel/Section;
    return-object v0
.end method

.method public getStartOffset()I
    .locals 1

    .prologue
    .line 1266
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    return v0
.end method

.method public getTable(Lorg/apache/poi/hwpf/usermodel/Paragraph;)Lorg/apache/poi/hwpf/usermodel/Table;
    .locals 15
    .param p1, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;

    .prologue
    .line 911
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 912
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "This paragraph doesn\'t belong to a table"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 915
    :cond_0
    move-object/from16 v11, p1

    .line 916
    .local v11, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    iget-object v0, v11, Lorg/apache/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 917
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "This paragraph is not a child of this range instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 920
    :cond_1
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 921
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v13

    .line 922
    .local v13, "tableLevel":I
    iget v12, v11, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    .line 924
    .local v12, "tableEndInclusive":I
    iget v0, v11, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    if-eqz v0, :cond_2

    .line 927
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v1, v11, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    .line 926
    invoke-static {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->newParagraph(Lorg/apache/poi/hwpf/usermodel/Range;Lorg/apache/poi/hwpf/model/PAPX;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v10

    .line 928
    .local v10, "previous":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v10}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 929
    invoke-virtual {v10}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v0

    if-ne v0, v13, :cond_2

    .line 930
    iget v0, v10, Lorg/apache/poi/hwpf/usermodel/Paragraph;->_sectionEnd:I

    iget v1, v11, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    if-lt v0, v1, :cond_2

    .line 932
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 933
    const-string/jumbo v1, "This paragraph is not the first one in the table"

    .line 932
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 937
    .end local v10    # "previous":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v9

    .line 938
    .local v9, "overallRange":Lorg/apache/poi/hwpf/usermodel/Range;
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 939
    .local v7, "limit":I
    :goto_0
    add-int/lit8 v0, v7, -0x1

    if-lt v12, v0, :cond_5

    .line 947
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 948
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    if-lt v12, v0, :cond_4

    .line 950
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    const-string/jumbo v2, "The table\'s bounds "

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 951
    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 952
    const-string/jumbo v4, " fall outside of this Range paragraphs numbers "

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v14, "["

    invoke-direct {v5, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 953
    iget v14, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v14, "; "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v14, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v14, ")"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 950
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 956
    :cond_4
    if-gez v12, :cond_6

    .line 958
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 959
    const-string/jumbo v1, "The table\'s end is negative, which isn\'t allowed!"

    .line 958
    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 942
    :cond_5
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    add-int/lit8 v1, v12, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    .line 941
    invoke-static {v9, v0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->newParagraph(Lorg/apache/poi/hwpf/usermodel/Range;Lorg/apache/poi/hwpf/model/PAPX;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v8

    .line 943
    .local v8, "next":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isInTable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v0

    if-lt v0, v13, :cond_3

    .line 939
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 962
    .end local v8    # "next":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    :cond_6
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/PAPX;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v6

    .line 964
    .local v6, "endOffsetExclusive":I
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Table;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getStartOffset()I

    move-result v1

    .line 965
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v2

    .line 964
    invoke-direct {v0, v1, v6, p0, v2}, Lorg/apache/poi/hwpf/usermodel/Table;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;I)V

    return-object v0
.end method

.method protected initAll()V
    .locals 0

    .prologue
    .line 972
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initCharacterRuns()V

    .line 973
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 974
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initSections()V

    .line 975
    return-void
.end method

.method public insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 364
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 366
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v0, v1, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/CHPBinTable;->adjustForInsert(II)V

    .line 369
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/PAPBinTable;->adjustForInsert(II)V

    .line 370
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getSectionTable()Lorg/apache/poi/hwpf/model/SectionTable;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionEnd:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/SectionTable;->adjustForInsert(II)V

    .line 371
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v0, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v0, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    .line 374
    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->afterInsert(II)V

    .line 376
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->adjustForInsert(I)V

    .line 378
    sget-boolean v0, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->sanityCheck()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 379
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->numCharacterRuns()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v0

    return-object v0
.end method

.method public insertAfter(Ljava/lang/String;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "props"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 427
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 428
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/PAPX;

    .line 429
    .local v4, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v3

    .line 431
    .local v3, "istd":S
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v5

    .line 432
    .local v5, "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    invoke-virtual {v5, v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 433
    .local v0, "baseStyle":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-static {p2, v0}, Lorg/apache/poi/hwpf/sprm/CharacterSprmCompressor;->compressCharacterProperty(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)[B

    move-result-object v2

    .line 434
    .local v2, "grpprl":[B
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v6, 0x0

    invoke-direct {v1, v2, v6}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 435
    .local v1, "buf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-result-object v6

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v6, v7, v8, v1}, Lorg/apache/poi/hwpf/model/CHPBinTable;->insert(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 436
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    .line 437
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/Range;->insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v6

    return-object v6
.end method

.method public insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;III)Lorg/apache/poi/hwpf/usermodel/ListEntry;
    .locals 4
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p2, "listID"    # I
    .param p3, "level"    # I
    .param p4, "styleIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 718
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getListTables()Lorg/apache/poi/hwpf/model/ListTables;

    move-result-object v1

    .line 719
    .local v1, "lt":Lorg/apache/poi/hwpf/model/ListTables;
    invoke-virtual {v1, p2, p3}, Lorg/apache/poi/hwpf/model/ListTables;->getLevel(II)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v2

    if-nez v2, :cond_0

    .line 720
    new-instance v2, Ljava/util/NoSuchElementException;

    const-string/jumbo v3, "The specified list and level do not exist"

    invoke-direct {v2, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 722
    :cond_0
    invoke-virtual {v1, p2}, Lorg/apache/poi/hwpf/model/ListTables;->getOverrideIndexFromListID(I)I

    move-result v0

    .line 723
    .local v0, "ilfo":I
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIlfo(I)V

    .line 724
    int-to-byte v2, p3

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIlvl(B)V

    .line 726
    invoke-virtual {p0, p1, p4}, Lorg/apache/poi/hwpf/usermodel/Range;->insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/ListEntry;

    return-object v2
.end method

.method public insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .locals 1
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p2, "styleIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 507
    const-string/jumbo v0, "\r"

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;ILjava/lang/String;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    return-object v0
.end method

.method protected insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;ILjava/lang/String;)Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .locals 9
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p2, "styleIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v8, 0x2

    .line 530
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 531
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v4

    .line 532
    .local v4, "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    invoke-virtual {v4, p2}, Lorg/apache/poi/hwpf/model/StyleSheet;->getParagraphStyle(I)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v1

    .line 533
    .local v1, "baseStyle":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {v4, p2}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 535
    .local v0, "baseChp":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-static {p1, v1}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmCompressor;->compressParagraphProperty(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)[B

    move-result-object v3

    .line 536
    .local v3, "grpprl":[B
    array-length v6, v3

    add-int/lit8 v6, v6, 0x2

    new-array v5, v6, [B

    .line 537
    .local v5, "withIndex":[B
    int-to-short v6, p2

    invoke-static {v5, v6}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 538
    const/4 v6, 0x0

    array-length v7, v3

    invoke-static {v3, v6, v5, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 539
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v2, v5, v8}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 541
    .local v2, "buf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v6

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v6, v7, v8, v2}, Lorg/apache/poi/hwpf/model/PAPBinTable;->insert(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 542
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    .line 543
    invoke-virtual {p0, p3, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->insertAfter(Ljava/lang/String;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 544
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v6

    return-object v6
.end method

.method public insertBefore(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 334
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 336
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v0, v1, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/CHPBinTable;->adjustForInsert(II)V

    .line 338
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/PAPBinTable;->adjustForInsert(II)V

    .line 339
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getSectionTable()Lorg/apache/poi/hwpf/model/SectionTable;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/model/SectionTable;->adjustForInsert(II)V

    .line 340
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    instance-of v0, v0, Lorg/apache/poi/hwpf/HWPFDocument;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    check-cast v0, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/HWPFDocument;->getBookmarks()Lorg/apache/poi/hwpf/usermodel/Bookmarks;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    .line 343
    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->afterInsert(II)V

    .line 345
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->adjustForInsert(I)V

    .line 348
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->adjustFIB(I)V

    .line 350
    sget-boolean v0, Lorg/apache/poi/hwpf/usermodel/Range;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->sanityCheck()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 352
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v0

    return-object v0
.end method

.method public insertBefore(Ljava/lang/String;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "props"    # Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 398
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 399
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/PAPX;

    .line 400
    .local v4, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PAPX;->getIstd()S

    move-result v3

    .line 402
    .local v3, "istd":S
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v5

    .line 403
    .local v5, "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    invoke-virtual {v5, v3}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 404
    .local v0, "baseStyle":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-static {p2, v0}, Lorg/apache/poi/hwpf/sprm/CharacterSprmCompressor;->compressCharacterProperty(Lorg/apache/poi/hwpf/usermodel/CharacterProperties;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)[B

    move-result-object v2

    .line 405
    .local v2, "grpprl":[B
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v6, 0x0

    invoke-direct {v1, v2, v6}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 406
    .local v1, "buf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-result-object v6

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v6, v7, v8, v1}, Lorg/apache/poi/hwpf/model/CHPBinTable;->insert(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 408
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v6

    return-object v6
.end method

.method public insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;III)Lorg/apache/poi/hwpf/usermodel/ListEntry;
    .locals 4
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p2, "listID"    # I
    .param p3, "level"    # I
    .param p4, "styleIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 689
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getListTables()Lorg/apache/poi/hwpf/model/ListTables;

    move-result-object v1

    .line 690
    .local v1, "lt":Lorg/apache/poi/hwpf/model/ListTables;
    invoke-virtual {v1, p2, p3}, Lorg/apache/poi/hwpf/model/ListTables;->getLevel(II)Lorg/apache/poi/hwpf/model/ListLevel;

    move-result-object v2

    if-nez v2, :cond_0

    .line 691
    new-instance v2, Ljava/util/NoSuchElementException;

    const-string/jumbo v3, "The specified list and level do not exist"

    invoke-direct {v2, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 694
    :cond_0
    invoke-virtual {v1, p2}, Lorg/apache/poi/hwpf/model/ListTables;->getOverrideIndexFromListID(I)I

    move-result v0

    .line 695
    .local v0, "ilfo":I
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIlfo(I)V

    .line 696
    int-to-byte v2, p3

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIlvl(B)V

    .line 698
    invoke-virtual {p0, p1, p4}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/usermodel/ListEntry;

    return-object v2
.end method

.method public insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .locals 1
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p2, "styleIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 454
    const-string/jumbo v0, "\r"

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;ILjava/lang/String;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    return-object v0
.end method

.method protected insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;ILjava/lang/String;)Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .locals 10
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p2, "styleIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x0

    .line 477
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initAll()V

    .line 478
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;

    move-result-object v4

    .line 479
    .local v4, "ss":Lorg/apache/poi/hwpf/model/StyleSheet;
    invoke-virtual {v4, p2}, Lorg/apache/poi/hwpf/model/StyleSheet;->getParagraphStyle(I)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    move-result-object v1

    .line 480
    .local v1, "baseStyle":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {v4, p2}, Lorg/apache/poi/hwpf/model/StyleSheet;->getCharacterStyle(I)Lorg/apache/poi/hwpf/usermodel/CharacterProperties;

    move-result-object v0

    .line 482
    .local v0, "baseChp":Lorg/apache/poi/hwpf/usermodel/CharacterProperties;
    invoke-static {p1, v1}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmCompressor;->compressParagraphProperty(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)[B

    move-result-object v3

    .line 483
    .local v3, "grpprl":[B
    array-length v6, v3

    add-int/lit8 v6, v6, 0x2

    new-array v5, v6, [B

    .line 484
    .local v5, "withIndex":[B
    int-to-short v6, p2

    invoke-static {v5, v6}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 485
    array-length v6, v3

    invoke-static {v3, v9, v5, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 486
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v2, v5, v7}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 488
    .local v2, "buf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-result-object v6

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v6, v7, v8, v2}, Lorg/apache/poi/hwpf/model/PAPBinTable;->insert(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 489
    invoke-virtual {p0, p3, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Ljava/lang/String;Lorg/apache/poi/hwpf/usermodel/CharacterProperties;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 490
    invoke-virtual {p0, v9}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v6

    return-object v6
.end method

.method public insertBefore(Lorg/apache/poi/hwpf/usermodel/TableProperties;I)Lorg/apache/poi/hwpf/usermodel/Table;
    .locals 12
    .param p1, "props"    # Lorg/apache/poi/hwpf/usermodel/TableProperties;
    .param p2, "rows"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v10, 0xfff

    const/4 v9, 0x7

    const/4 v11, 0x1

    .line 609
    new-instance v5, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-direct {v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 610
    .local v5, "parProps":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {v5, v11}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFInTable(Z)V

    .line 611
    invoke-virtual {v5, v11}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setItap(I)V

    .line 613
    iget v4, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 615
    .local v4, "oldEnd":I
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;->getItcMac()S

    move-result v1

    .line 616
    .local v1, "columns":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    if-lt v6, p2, :cond_0

    .line 630
    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 631
    .local v3, "newEnd":I
    sub-int v2, v3, v4

    .line 633
    .local v2, "diff":I
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/Table;

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v10, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    add-int/2addr v10, v2

    invoke-direct {v8, v9, v10, p0, v11}, Lorg/apache/poi/hwpf/usermodel/Table;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;I)V

    return-object v8

    .line 618
    .end local v2    # "diff":I
    .end local v3    # "newEnd":I
    :cond_0
    invoke-virtual {p0, v5, v10}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 619
    .local v0, "cell":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 620
    const/4 v7, 0x1

    .local v7, "y":I
    :goto_1
    if-lt v7, v1, :cond_1

    .line 626
    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    .line 625
    invoke-virtual {v0, v5, v10, v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;ILjava/lang/String;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 627
    invoke-virtual {v0, p1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->setTableRowEnd(Lorg/apache/poi/hwpf/usermodel/TableProperties;)V

    .line 616
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 622
    :cond_1
    invoke-virtual {v0, v5, v10}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 623
    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 620
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public insertTableBefore(SI)Lorg/apache/poi/hwpf/usermodel/Table;
    .locals 11
    .param p1, "columns"    # S
    .param p2, "rows"    # I

    .prologue
    const/16 v9, 0xfff

    const/4 v8, 0x7

    const/4 v10, 0x1

    .line 646
    new-instance v4, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    invoke-direct {v4}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 647
    .local v4, "parProps":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    invoke-virtual {v4, v10}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFInTable(Z)V

    .line 648
    invoke-virtual {v4, v10}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setItap(I)V

    .line 650
    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 652
    .local v3, "oldEnd":I
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    if-lt v5, p2, :cond_0

    .line 666
    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    .line 667
    .local v2, "newEnd":I
    sub-int v1, v2, v3

    .line 669
    .local v1, "diff":I
    new-instance v7, Lorg/apache/poi/hwpf/usermodel/Table;

    iget v8, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    add-int/2addr v9, v1

    invoke-direct {v7, v8, v9, p0, v10}, Lorg/apache/poi/hwpf/usermodel/Table;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;I)V

    return-object v7

    .line 654
    .end local v1    # "diff":I
    .end local v2    # "newEnd":I
    :cond_0
    invoke-virtual {p0, v4, v9}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 655
    .local v0, "cell":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 656
    const/4 v6, 0x1

    .local v6, "y":I
    :goto_1
    if-lt v6, p1, :cond_1

    .line 662
    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    .line 661
    invoke-virtual {v0, v4, v9, v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;ILjava/lang/String;)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 663
    new-instance v7, Lorg/apache/poi/hwpf/usermodel/TableProperties;

    invoke-direct {v7, p1}, Lorg/apache/poi/hwpf/usermodel/TableProperties;-><init>(S)V

    invoke-virtual {v0, v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->setTableRowEnd(Lorg/apache/poi/hwpf/usermodel/TableProperties;)V

    .line 652
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 658
    :cond_1
    invoke-virtual {v0, v4, v9}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 659
    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 656
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public numCharacterRuns()I
    .locals 2

    .prologue
    .line 321
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initCharacterRuns()V

    .line 322
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public numParagraphs()I
    .locals 2

    .prologue
    .line 311
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 312
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public numSections()I
    .locals 2

    .prologue
    .line 299
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->initSections()V

    .line 300
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionEnd:I

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public replaceText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pPlaceHolder"    # Ljava/lang/String;
    .param p2, "pValue"    # Ljava/lang/String;

    .prologue
    .line 794
    const/4 v0, 0x1

    .line 795
    .local v0, "keepLooking":Z
    :goto_0
    if-nez v0, :cond_0

    .line 804
    return-void

    .line 797
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->text()Ljava/lang/String;

    move-result-object v2

    .line 798
    .local v2, "text":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 799
    .local v1, "offset":I
    if-ltz v1, :cond_1

    .line 800
    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->replaceText(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 802
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public replaceText(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "pPlaceHolder"    # Ljava/lang/String;
    .param p2, "pValue"    # Ljava/lang/String;
    .param p3, "pOffset"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 771
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v2

    add-int v0, v2, p3

    .line 773
    .local v0, "absPlaceHolderIndex":I
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/Range;

    .line 774
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    .line 773
    invoke-direct {v1, v0, v2, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 775
    .local v1, "subRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v1, p2}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 778
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/Range;

    .end local v1    # "subRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    .line 779
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    .line 778
    invoke-direct {v1, v2, v3, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 782
    .restart local v1    # "subRange":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/Range;->delete()V

    .line 783
    return-void
.end method

.method public replaceText(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "newText"    # Ljava/lang/String;
    .param p2, "addAfter"    # Z

    .prologue
    .line 741
    if-eqz p2, :cond_0

    .line 743
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v0

    .line 744
    .local v0, "originalEnd":I
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/Range;->insertAfter(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 745
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v3

    invoke-direct {v2, v3, v0, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Range;->delete()V

    .line 756
    :goto_0
    return-void

    .line 749
    .end local v0    # "originalEnd":I
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v1

    .line 750
    .local v1, "originalStart":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v0

    .line 752
    .restart local v0    # "originalEnd":I
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/usermodel/Range;->insertBefore(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .line 753
    new-instance v2, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    .line 754
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v0

    .line 753
    invoke-direct {v2, v3, v4, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 754
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/Range;->delete()V

    goto :goto_0
.end method

.method protected reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1175
    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    .line 1176
    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    .line 1177
    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_sectionRangeFound:Z

    .line 1178
    return-void
.end method

.method public sanityCheck()Z
    .locals 8

    .prologue
    .line 1293
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    if-gez v6, :cond_0

    .line 1294
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1295
    :cond_0
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-le v6, v7, :cond_1

    .line 1296
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1297
    :cond_1
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    if-gez v6, :cond_2

    .line 1298
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1299
    :cond_2
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-le v6, v7, :cond_3

    .line 1300
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1301
    :cond_3
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v7, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    if-le v6, v7, :cond_4

    .line 1302
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1304
    :cond_4
    iget-boolean v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    if-eqz v6, :cond_5

    .line 1306
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charStart:I

    .local v0, "c":I
    :goto_0
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_charEnd:I

    if-lt v0, v6, :cond_7

    .line 1317
    .end local v0    # "c":I
    :cond_5
    iget-boolean v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    if-eqz v6, :cond_6

    .line 1319
    iget v3, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parStart:I

    .local v3, "p":I
    :goto_1
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_parEnd:I

    if-lt v3, v6, :cond_9

    .line 1331
    .end local v3    # "p":I
    :cond_6
    const/4 v6, 0x1

    return v6

    .line 1308
    .restart local v0    # "c":I
    :cond_7
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/CHPX;

    .line 1310
    .local v1, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1311
    .local v2, "left":I
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1313
    .local v5, "right":I
    if-lt v2, v5, :cond_8

    .line 1314
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1306
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1321
    .end local v0    # "c":I
    .end local v1    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v2    # "left":I
    .end local v5    # "right":I
    .restart local v3    # "p":I
    :cond_9
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/PAPX;

    .line 1323
    .local v4, "papx":Lorg/apache/poi/hwpf/model/PAPX;
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PAPX;->getStart()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1324
    .restart local v2    # "left":I
    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PAPX;->getEnd()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1326
    .restart local v5    # "right":I
    if-lt v2, v5, :cond_a

    .line 1327
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1319
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public text()Ljava/lang/String;
    .locals 3

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_text:Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    iget v2, p0, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1283
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Range from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1284
    const-string/jumbo v1, " (chars)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1283
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 899
    const/4 v0, 0x6

    return v0
.end method

.method public usesUnicode()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228
    const/4 v0, 0x1

    return v0
.end method
