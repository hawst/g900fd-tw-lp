.class public abstract Lorg/apache/poi/hwpf/model/types/SEPAbstractType;
.super Ljava/lang/Object;
.source "SEPAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final BKC_EVEN_PAGE:B = 0x3t

.field public static final BKC_NEW_COLUMN:B = 0x1t

.field public static final BKC_NEW_PAGE:B = 0x2t

.field public static final BKC_NO_BREAK:B = 0x0t

.field public static final BKC_ODD_PAGE:B = 0x4t

.field public static final DMORIENTPAGE_LANDSCAPE:Z = false

.field public static final DMORIENTPAGE_PORTRAIT:Z = true

.field public static final NFCPGN_ARABIC:B = 0x0t

.field public static final NFCPGN_LETTER_LOWER_CASE:B = 0x4t

.field public static final NFCPGN_LETTER_UPPER_CASE:B = 0x3t

.field public static final NFCPGN_ROMAN_LOWER_CASE:B = 0x2t

.field public static final NFCPGN_ROMAN_UPPER_CASE:B = 0x1t


# instance fields
.field protected field_10_grpfIhdt:B

.field protected field_11_nLnnMod:I

.field protected field_12_dxaLnn:I

.field protected field_13_dxaPgn:I

.field protected field_14_dyaPgn:I

.field protected field_15_fLBetween:Z

.field protected field_16_vjc:B

.field protected field_17_dmBinFirst:I

.field protected field_18_dmBinOther:I

.field protected field_19_dmPaperReq:I

.field protected field_1_bkc:B

.field protected field_20_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_21_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_22_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_23_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_24_fPropMark:Z

.field protected field_25_ibstPropRMark:I

.field protected field_26_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_27_dxtCharSpace:I

.field protected field_28_dyaLinePitch:I

.field protected field_29_clm:I

.field protected field_2_fTitlePage:Z

.field protected field_30_unused2:I

.field protected field_31_dmOrientPage:Z

.field protected field_32_iHeadingPgn:B

.field protected field_33_pgnStart:I

.field protected field_34_lnnMin:I

.field protected field_35_wTextFlow:I

.field protected field_36_unused3:S

.field protected field_37_pgbProp:I

.field protected field_38_unused4:S

.field protected field_39_xaPage:I

.field protected field_3_fAutoPgn:Z

.field protected field_40_yaPage:I

.field protected field_41_xaPageNUp:I

.field protected field_42_yaPageNUp:I

.field protected field_43_dxaLeft:I

.field protected field_44_dxaRight:I

.field protected field_45_dyaTop:I

.field protected field_46_dyaBottom:I

.field protected field_47_dzaGutter:I

.field protected field_48_dyaHdrTop:I

.field protected field_49_dyaHdrBottom:I

.field protected field_4_nfcPgn:B

.field protected field_50_ccolM1:I

.field protected field_51_fEvenlySpaced:Z

.field protected field_52_unused5:B

.field protected field_53_dxaColumns:I

.field protected field_54_rgdxaColumn:[I

.field protected field_55_dxaColumnWidth:I

.field protected field_56_dmOrientFirst:B

.field protected field_57_fLayout:B

.field protected field_58_unused6:S

.field protected field_59_olstAnm:[B

.field protected field_5_fUnlocked:Z

.field protected field_6_cnsPgn:B

.field protected field_7_fPgnRestart:Z

.field protected field_8_fEndNote:Z

.field protected field_9_lnc:B


# direct methods
.method protected constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x2fd0

    const/16 v4, 0x708

    const/16 v3, 0x5a0

    const/4 v2, 0x1

    const/16 v1, 0x2d0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v0, 0x2

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_1_bkc:B

    .line 121
    iput-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_8_fEndNote:Z

    .line 122
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_13_dxaPgn:I

    .line 123
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_14_dyaPgn:I

    .line 124
    iput-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_31_dmOrientPage:Z

    .line 125
    iput v2, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_33_pgnStart:I

    .line 126
    iput v5, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_39_xaPage:I

    .line 127
    const/16 v0, 0x3de0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_40_yaPage:I

    .line 128
    iput v5, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_41_xaPageNUp:I

    .line 129
    const/16 v0, 0x3de0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_42_yaPageNUp:I

    .line 130
    iput v4, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_43_dxaLeft:I

    .line 131
    iput v4, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_44_dxaRight:I

    .line 132
    iput v3, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_45_dyaTop:I

    .line 133
    iput v3, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_46_dyaBottom:I

    .line 134
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_48_dyaHdrTop:I

    .line 135
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_49_dyaHdrBottom:I

    .line 136
    iput-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_51_fEvenlySpaced:Z

    .line 137
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_53_dxaColumns:I

    .line 138
    return-void
.end method


# virtual methods
.method public getBkc()B
    .locals 1

    .prologue
    .line 280
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_1_bkc:B

    return v0
.end method

.method public getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_22_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_21_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_23_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_20_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getCcolM1()I
    .locals 1

    .prologue
    .line 1096
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_50_ccolM1:I

    return v0
.end method

.method public getClm()I
    .locals 1

    .prologue
    .line 751
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_29_clm:I

    return v0
.end method

.method public getCnsPgn()B
    .locals 1

    .prologue
    .line 383
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_6_cnsPgn:B

    return v0
.end method

.method public getDmBinFirst()I
    .locals 1

    .prologue
    .line 559
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_17_dmBinFirst:I

    return v0
.end method

.method public getDmBinOther()I
    .locals 1

    .prologue
    .line 575
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_18_dmBinOther:I

    return v0
.end method

.method public getDmOrientFirst()B
    .locals 1

    .prologue
    .line 1192
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_56_dmOrientFirst:B

    return v0
.end method

.method public getDmOrientPage()Z
    .locals 1

    .prologue
    .line 787
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_31_dmOrientPage:Z

    return v0
.end method

.method public getDmPaperReq()I
    .locals 1

    .prologue
    .line 591
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_19_dmPaperReq:I

    return v0
.end method

.method public getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_26_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDxaColumnWidth()I
    .locals 1

    .prologue
    .line 1176
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_55_dxaColumnWidth:I

    return v0
.end method

.method public getDxaColumns()I
    .locals 1

    .prologue
    .line 1144
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_53_dxaColumns:I

    return v0
.end method

.method public getDxaLeft()I
    .locals 1

    .prologue
    .line 984
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_43_dxaLeft:I

    return v0
.end method

.method public getDxaLnn()I
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_12_dxaLnn:I

    return v0
.end method

.method public getDxaPgn()I
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_13_dxaPgn:I

    return v0
.end method

.method public getDxaRight()I
    .locals 1

    .prologue
    .line 1000
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_44_dxaRight:I

    return v0
.end method

.method public getDxtCharSpace()I
    .locals 1

    .prologue
    .line 719
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_27_dxtCharSpace:I

    return v0
.end method

.method public getDyaBottom()I
    .locals 1

    .prologue
    .line 1032
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_46_dyaBottom:I

    return v0
.end method

.method public getDyaHdrBottom()I
    .locals 1

    .prologue
    .line 1080
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_49_dyaHdrBottom:I

    return v0
.end method

.method public getDyaHdrTop()I
    .locals 1

    .prologue
    .line 1064
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_48_dyaHdrTop:I

    return v0
.end method

.method public getDyaLinePitch()I
    .locals 1

    .prologue
    .line 735
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_28_dyaLinePitch:I

    return v0
.end method

.method public getDyaPgn()I
    .locals 1

    .prologue
    .line 511
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_14_dyaPgn:I

    return v0
.end method

.method public getDyaTop()I
    .locals 1

    .prologue
    .line 1016
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_45_dyaTop:I

    return v0
.end method

.method public getDzaGutter()I
    .locals 1

    .prologue
    .line 1048
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_47_dzaGutter:I

    return v0
.end method

.method public getFAutoPgn()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_3_fAutoPgn:Z

    return v0
.end method

.method public getFEndNote()Z
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_8_fEndNote:Z

    return v0
.end method

.method public getFEvenlySpaced()Z
    .locals 1

    .prologue
    .line 1112
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_51_fEvenlySpaced:Z

    return v0
.end method

.method public getFLBetween()Z
    .locals 1

    .prologue
    .line 527
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_15_fLBetween:Z

    return v0
.end method

.method public getFLayout()B
    .locals 1

    .prologue
    .line 1208
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_57_fLayout:B

    return v0
.end method

.method public getFPgnRestart()Z
    .locals 1

    .prologue
    .line 399
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_7_fPgnRestart:Z

    return v0
.end method

.method public getFPropMark()Z
    .locals 1

    .prologue
    .line 671
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_24_fPropMark:Z

    return v0
.end method

.method public getFTitlePage()Z
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_2_fTitlePage:Z

    return v0
.end method

.method public getFUnlocked()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_5_fUnlocked:Z

    return v0
.end method

.method public getGrpfIhdt()B
    .locals 1

    .prologue
    .line 447
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_10_grpfIhdt:B

    return v0
.end method

.method public getIHeadingPgn()B
    .locals 1

    .prologue
    .line 808
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_32_iHeadingPgn:B

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1

    .prologue
    .line 687
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_25_ibstPropRMark:I

    return v0
.end method

.method public getLnc()B
    .locals 1

    .prologue
    .line 431
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_9_lnc:B

    return v0
.end method

.method public getLnnMin()I
    .locals 1

    .prologue
    .line 840
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_34_lnnMin:I

    return v0
.end method

.method public getNLnnMod()I
    .locals 1

    .prologue
    .line 463
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_11_nLnnMod:I

    return v0
.end method

.method public getNfcPgn()B
    .locals 1

    .prologue
    .line 343
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_4_nfcPgn:B

    return v0
.end method

.method public getOlstAnm()[B
    .locals 1

    .prologue
    .line 1240
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_59_olstAnm:[B

    return-object v0
.end method

.method public getPgbProp()I
    .locals 1

    .prologue
    .line 888
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_37_pgbProp:I

    return v0
.end method

.method public getPgnStart()I
    .locals 1

    .prologue
    .line 824
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_33_pgnStart:I

    return v0
.end method

.method public getRgdxaColumn()[I
    .locals 1

    .prologue
    .line 1160
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_54_rgdxaColumn:[I

    return-object v0
.end method

.method public getUnused2()I
    .locals 1

    .prologue
    .line 767
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_30_unused2:I

    return v0
.end method

.method public getUnused3()S
    .locals 1

    .prologue
    .line 872
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_36_unused3:S

    return v0
.end method

.method public getUnused4()S
    .locals 1

    .prologue
    .line 904
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_38_unused4:S

    return v0
.end method

.method public getUnused5()B
    .locals 1

    .prologue
    .line 1128
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_52_unused5:B

    return v0
.end method

.method public getUnused6()S
    .locals 1

    .prologue
    .line 1224
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_58_unused6:S

    return v0
.end method

.method public getVjc()B
    .locals 1

    .prologue
    .line 543
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_16_vjc:B

    return v0
.end method

.method public getWTextFlow()I
    .locals 1

    .prologue
    .line 856
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_35_wTextFlow:I

    return v0
.end method

.method public getXaPage()I
    .locals 1

    .prologue
    .line 920
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_39_xaPage:I

    return v0
.end method

.method public getXaPageNUp()I
    .locals 1

    .prologue
    .line 952
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_41_xaPageNUp:I

    return v0
.end method

.method public getYaPage()I
    .locals 1

    .prologue
    .line 936
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_40_yaPage:I

    return v0
.end method

.method public getYaPageNUp()I
    .locals 1

    .prologue
    .line 968
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_42_yaPageNUp:I

    return v0
.end method

.method public setBkc(B)V
    .locals 0
    .param p1, "field_1_bkc"    # B

    .prologue
    .line 296
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_1_bkc:B

    .line 297
    return-void
.end method

.method public setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_22_brcBottom"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 647
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_22_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 648
    return-void
.end method

.method public setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_21_brcLeft"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 631
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_21_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 632
    return-void
.end method

.method public setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_23_brcRight"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 663
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_23_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 664
    return-void
.end method

.method public setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_20_brcTop"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 615
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_20_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 616
    return-void
.end method

.method public setCcolM1(I)V
    .locals 0
    .param p1, "field_50_ccolM1"    # I

    .prologue
    .line 1104
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_50_ccolM1:I

    .line 1105
    return-void
.end method

.method public setClm(I)V
    .locals 0
    .param p1, "field_29_clm"    # I

    .prologue
    .line 759
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_29_clm:I

    .line 760
    return-void
.end method

.method public setCnsPgn(B)V
    .locals 0
    .param p1, "field_6_cnsPgn"    # B

    .prologue
    .line 391
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_6_cnsPgn:B

    .line 392
    return-void
.end method

.method public setDmBinFirst(I)V
    .locals 0
    .param p1, "field_17_dmBinFirst"    # I

    .prologue
    .line 567
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_17_dmBinFirst:I

    .line 568
    return-void
.end method

.method public setDmBinOther(I)V
    .locals 0
    .param p1, "field_18_dmBinOther"    # I

    .prologue
    .line 583
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_18_dmBinOther:I

    .line 584
    return-void
.end method

.method public setDmOrientFirst(B)V
    .locals 0
    .param p1, "field_56_dmOrientFirst"    # B

    .prologue
    .line 1200
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_56_dmOrientFirst:B

    .line 1201
    return-void
.end method

.method public setDmOrientPage(Z)V
    .locals 0
    .param p1, "field_31_dmOrientPage"    # Z

    .prologue
    .line 800
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_31_dmOrientPage:Z

    .line 801
    return-void
.end method

.method public setDmPaperReq(I)V
    .locals 0
    .param p1, "field_19_dmPaperReq"    # I

    .prologue
    .line 599
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_19_dmPaperReq:I

    .line 600
    return-void
.end method

.method public setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_26_dttmPropRMark"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .prologue
    .line 711
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_26_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 712
    return-void
.end method

.method public setDxaColumnWidth(I)V
    .locals 0
    .param p1, "field_55_dxaColumnWidth"    # I

    .prologue
    .line 1184
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_55_dxaColumnWidth:I

    .line 1185
    return-void
.end method

.method public setDxaColumns(I)V
    .locals 0
    .param p1, "field_53_dxaColumns"    # I

    .prologue
    .line 1152
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_53_dxaColumns:I

    .line 1153
    return-void
.end method

.method public setDxaLeft(I)V
    .locals 0
    .param p1, "field_43_dxaLeft"    # I

    .prologue
    .line 992
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_43_dxaLeft:I

    .line 993
    return-void
.end method

.method public setDxaLnn(I)V
    .locals 0
    .param p1, "field_12_dxaLnn"    # I

    .prologue
    .line 487
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_12_dxaLnn:I

    .line 488
    return-void
.end method

.method public setDxaPgn(I)V
    .locals 0
    .param p1, "field_13_dxaPgn"    # I

    .prologue
    .line 503
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_13_dxaPgn:I

    .line 504
    return-void
.end method

.method public setDxaRight(I)V
    .locals 0
    .param p1, "field_44_dxaRight"    # I

    .prologue
    .line 1008
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_44_dxaRight:I

    .line 1009
    return-void
.end method

.method public setDxtCharSpace(I)V
    .locals 0
    .param p1, "field_27_dxtCharSpace"    # I

    .prologue
    .line 727
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_27_dxtCharSpace:I

    .line 728
    return-void
.end method

.method public setDyaBottom(I)V
    .locals 0
    .param p1, "field_46_dyaBottom"    # I

    .prologue
    .line 1040
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_46_dyaBottom:I

    .line 1041
    return-void
.end method

.method public setDyaHdrBottom(I)V
    .locals 0
    .param p1, "field_49_dyaHdrBottom"    # I

    .prologue
    .line 1088
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_49_dyaHdrBottom:I

    .line 1089
    return-void
.end method

.method public setDyaHdrTop(I)V
    .locals 0
    .param p1, "field_48_dyaHdrTop"    # I

    .prologue
    .line 1072
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_48_dyaHdrTop:I

    .line 1073
    return-void
.end method

.method public setDyaLinePitch(I)V
    .locals 0
    .param p1, "field_28_dyaLinePitch"    # I

    .prologue
    .line 743
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_28_dyaLinePitch:I

    .line 744
    return-void
.end method

.method public setDyaPgn(I)V
    .locals 0
    .param p1, "field_14_dyaPgn"    # I

    .prologue
    .line 519
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_14_dyaPgn:I

    .line 520
    return-void
.end method

.method public setDyaTop(I)V
    .locals 0
    .param p1, "field_45_dyaTop"    # I

    .prologue
    .line 1024
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_45_dyaTop:I

    .line 1025
    return-void
.end method

.method public setDzaGutter(I)V
    .locals 0
    .param p1, "field_47_dzaGutter"    # I

    .prologue
    .line 1056
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_47_dzaGutter:I

    .line 1057
    return-void
.end method

.method public setFAutoPgn(Z)V
    .locals 0
    .param p1, "field_3_fAutoPgn"    # Z

    .prologue
    .line 328
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_3_fAutoPgn:Z

    .line 329
    return-void
.end method

.method public setFEndNote(Z)V
    .locals 0
    .param p1, "field_8_fEndNote"    # Z

    .prologue
    .line 423
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_8_fEndNote:Z

    .line 424
    return-void
.end method

.method public setFEvenlySpaced(Z)V
    .locals 0
    .param p1, "field_51_fEvenlySpaced"    # Z

    .prologue
    .line 1120
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_51_fEvenlySpaced:Z

    .line 1121
    return-void
.end method

.method public setFLBetween(Z)V
    .locals 0
    .param p1, "field_15_fLBetween"    # Z

    .prologue
    .line 535
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_15_fLBetween:Z

    .line 536
    return-void
.end method

.method public setFLayout(B)V
    .locals 0
    .param p1, "field_57_fLayout"    # B

    .prologue
    .line 1216
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_57_fLayout:B

    .line 1217
    return-void
.end method

.method public setFPgnRestart(Z)V
    .locals 0
    .param p1, "field_7_fPgnRestart"    # Z

    .prologue
    .line 407
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_7_fPgnRestart:Z

    .line 408
    return-void
.end method

.method public setFPropMark(Z)V
    .locals 0
    .param p1, "field_24_fPropMark"    # Z

    .prologue
    .line 679
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_24_fPropMark:Z

    .line 680
    return-void
.end method

.method public setFTitlePage(Z)V
    .locals 0
    .param p1, "field_2_fTitlePage"    # Z

    .prologue
    .line 312
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_2_fTitlePage:Z

    .line 313
    return-void
.end method

.method public setFUnlocked(Z)V
    .locals 0
    .param p1, "field_5_fUnlocked"    # Z

    .prologue
    .line 375
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_5_fUnlocked:Z

    .line 376
    return-void
.end method

.method public setGrpfIhdt(B)V
    .locals 0
    .param p1, "field_10_grpfIhdt"    # B

    .prologue
    .line 455
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_10_grpfIhdt:B

    .line 456
    return-void
.end method

.method public setIHeadingPgn(B)V
    .locals 0
    .param p1, "field_32_iHeadingPgn"    # B

    .prologue
    .line 816
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_32_iHeadingPgn:B

    .line 817
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_25_ibstPropRMark"    # I

    .prologue
    .line 695
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_25_ibstPropRMark:I

    .line 696
    return-void
.end method

.method public setLnc(B)V
    .locals 0
    .param p1, "field_9_lnc"    # B

    .prologue
    .line 439
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_9_lnc:B

    .line 440
    return-void
.end method

.method public setLnnMin(I)V
    .locals 0
    .param p1, "field_34_lnnMin"    # I

    .prologue
    .line 848
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_34_lnnMin:I

    .line 849
    return-void
.end method

.method public setNLnnMod(I)V
    .locals 0
    .param p1, "field_11_nLnnMod"    # I

    .prologue
    .line 471
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_11_nLnnMod:I

    .line 472
    return-void
.end method

.method public setNfcPgn(B)V
    .locals 0
    .param p1, "field_4_nfcPgn"    # B

    .prologue
    .line 359
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_4_nfcPgn:B

    .line 360
    return-void
.end method

.method public setOlstAnm([B)V
    .locals 0
    .param p1, "field_59_olstAnm"    # [B

    .prologue
    .line 1248
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_59_olstAnm:[B

    .line 1249
    return-void
.end method

.method public setPgbProp(I)V
    .locals 0
    .param p1, "field_37_pgbProp"    # I

    .prologue
    .line 896
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_37_pgbProp:I

    .line 897
    return-void
.end method

.method public setPgnStart(I)V
    .locals 0
    .param p1, "field_33_pgnStart"    # I

    .prologue
    .line 832
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_33_pgnStart:I

    .line 833
    return-void
.end method

.method public setRgdxaColumn([I)V
    .locals 0
    .param p1, "field_54_rgdxaColumn"    # [I

    .prologue
    .line 1168
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_54_rgdxaColumn:[I

    .line 1169
    return-void
.end method

.method public setUnused2(I)V
    .locals 0
    .param p1, "field_30_unused2"    # I

    .prologue
    .line 775
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_30_unused2:I

    .line 776
    return-void
.end method

.method public setUnused3(S)V
    .locals 0
    .param p1, "field_36_unused3"    # S

    .prologue
    .line 880
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_36_unused3:S

    .line 881
    return-void
.end method

.method public setUnused4(S)V
    .locals 0
    .param p1, "field_38_unused4"    # S

    .prologue
    .line 912
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_38_unused4:S

    .line 913
    return-void
.end method

.method public setUnused5(B)V
    .locals 0
    .param p1, "field_52_unused5"    # B

    .prologue
    .line 1136
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_52_unused5:B

    .line 1137
    return-void
.end method

.method public setUnused6(S)V
    .locals 0
    .param p1, "field_58_unused6"    # S

    .prologue
    .line 1232
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_58_unused6:S

    .line 1233
    return-void
.end method

.method public setVjc(B)V
    .locals 0
    .param p1, "field_16_vjc"    # B

    .prologue
    .line 551
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_16_vjc:B

    .line 552
    return-void
.end method

.method public setWTextFlow(I)V
    .locals 0
    .param p1, "field_35_wTextFlow"    # I

    .prologue
    .line 864
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_35_wTextFlow:I

    .line 865
    return-void
.end method

.method public setXaPage(I)V
    .locals 0
    .param p1, "field_39_xaPage"    # I

    .prologue
    .line 928
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_39_xaPage:I

    .line 929
    return-void
.end method

.method public setXaPageNUp(I)V
    .locals 0
    .param p1, "field_41_xaPageNUp"    # I

    .prologue
    .line 960
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_41_xaPageNUp:I

    .line 961
    return-void
.end method

.method public setYaPage(I)V
    .locals 0
    .param p1, "field_40_yaPage"    # I

    .prologue
    .line 944
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_40_yaPage:I

    .line 945
    return-void
.end method

.method public setYaPageNUp(I)V
    .locals 0
    .param p1, "field_42_yaPageNUp"    # I

    .prologue
    .line 976
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->field_42_yaPageNUp:I

    .line 977
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[SEP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, "    .bkc                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getBkc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v1, "    .fTitlePage           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFTitlePage()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v1, "    .fAutoPgn             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFAutoPgn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string/jumbo v1, "    .nfcPgn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getNfcPgn()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string/jumbo v1, "    .fUnlocked            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFUnlocked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v1, "    .cnsPgn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getCnsPgn()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v1, "    .fPgnRestart          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFPgnRestart()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string/jumbo v1, "    .fEndNote             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFEndNote()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    const-string/jumbo v1, "    .lnc                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getLnc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const-string/jumbo v1, "    .grpfIhdt             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getGrpfIhdt()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    const-string/jumbo v1, "    .nLnnMod              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getNLnnMod()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const-string/jumbo v1, "    .dxaLnn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxaLnn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string/jumbo v1, "    .dxaPgn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxaPgn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string/jumbo v1, "    .dyaPgn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDyaPgn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string/jumbo v1, "    .fLBetween            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFLBetween()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v1, "    .vjc                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getVjc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v1, "    .dmBinFirst           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDmBinFirst()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v1, "    .dmBinOther           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDmBinOther()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string/jumbo v1, "    .dmPaperReq           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDmPaperReq()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v1, "    .brcTop               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v1, "    .brcLeft              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v1, "    .brcBottom            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v1, "    .brcRight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v1, "    .fPropMark            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFPropMark()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string/jumbo v1, "    .ibstPropRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getIbstPropRMark()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string/jumbo v1, "    .dttmPropRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v1, "    .dxtCharSpace         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxtCharSpace()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v1, "    .dyaLinePitch         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDyaLinePitch()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v1, "    .clm                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getClm()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v1, "    .unused2              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getUnused2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v1, "    .dmOrientPage         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDmOrientPage()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string/jumbo v1, "    .iHeadingPgn          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getIHeadingPgn()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string/jumbo v1, "    .pgnStart             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getPgnStart()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string/jumbo v1, "    .lnnMin               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getLnnMin()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string/jumbo v1, "    .wTextFlow            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getWTextFlow()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string/jumbo v1, "    .unused3              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getUnused3()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string/jumbo v1, "    .pgbProp              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getPgbProp()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string/jumbo v1, "    .unused4              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getUnused4()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    const-string/jumbo v1, "    .xaPage               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getXaPage()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const-string/jumbo v1, "    .yaPage               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getYaPage()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string/jumbo v1, "    .xaPageNUp            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getXaPageNUp()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    const-string/jumbo v1, "    .yaPageNUp            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getYaPageNUp()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string/jumbo v1, "    .dxaLeft              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxaLeft()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string/jumbo v1, "    .dxaRight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxaRight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    const-string/jumbo v1, "    .dyaTop               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDyaTop()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    const-string/jumbo v1, "    .dyaBottom            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDyaBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const-string/jumbo v1, "    .dzaGutter            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDzaGutter()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    const-string/jumbo v1, "    .dyaHdrTop            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDyaHdrTop()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const-string/jumbo v1, "    .dyaHdrBottom         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDyaHdrBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const-string/jumbo v1, "    .ccolM1               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getCcolM1()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string/jumbo v1, "    .fEvenlySpaced        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFEvenlySpaced()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string/jumbo v1, "    .unused5              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getUnused5()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string/jumbo v1, "    .dxaColumns           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxaColumns()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string/jumbo v1, "    .rgdxaColumn          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getRgdxaColumn()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    const-string/jumbo v1, "    .dxaColumnWidth       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDxaColumnWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string/jumbo v1, "    .dmOrientFirst        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getDmOrientFirst()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string/jumbo v1, "    .fLayout              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getFLayout()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string/jumbo v1, "    .unused6              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getUnused6()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string/jumbo v1, "    .olstAnm              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;->getOlstAnm()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    const-string/jumbo v1, "[/SEP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
