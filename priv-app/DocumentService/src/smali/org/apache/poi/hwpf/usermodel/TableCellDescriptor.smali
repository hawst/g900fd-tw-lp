.class public final Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
.super Lorg/apache/poi/hwpf/model/types/TCAbstractType;
.source "TableCellDescriptor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/TCAbstractType;-><init>()V

    .line 30
    return-void
.end method

.method public static convertBytesToTC([BI)Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .locals 1
    .param p0, "buf"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 66
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;-><init>()V

    .line 67
    .local v0, "tc":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    invoke-virtual {v0, p0, p1}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->fillFields([BI)V

    .line 68
    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    .line 56
    .local v0, "tc":Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    .line 57
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 58
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 59
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 60
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 61
    return-object v0
.end method

.method protected fillFields([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 34
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->field_1_rgf:S

    .line 35
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->field_2_wWidth:S

    .line 36
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    add-int/lit8 v1, p2, 0x4

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 37
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    add-int/lit8 v1, p2, 0x8

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 38
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    add-int/lit8 v1, p2, 0xc

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 39
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    add-int/lit8 v1, p2, 0x10

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    .line 40
    return-void
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 44
    add-int/lit8 v0, p2, 0x0

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->field_1_rgf:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 45
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->field_2_wWidth:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 46
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    add-int/lit8 v1, p2, 0x4

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 47
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    add-int/lit8 v1, p2, 0x8

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 48
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    add-int/lit8 v1, p2, 0xc

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 49
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    add-int/lit8 v1, p2, 0x10

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 50
    return-void
.end method
