.class public interface abstract Lorg/apache/poi/hwpf/usermodel/OfficeDrawings;
.super Ljava/lang/Object;
.source "OfficeDrawings.java"


# virtual methods
.method public abstract getEscherBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;
.end method

.method public abstract getOfficeDrawingAt(I)Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
.end method

.method public abstract getOfficeDrawings()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOfficeDrawingsArray()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation
.end method
