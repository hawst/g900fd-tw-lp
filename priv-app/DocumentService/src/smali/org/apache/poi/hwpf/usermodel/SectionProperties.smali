.class public final Lorg/apache/poi/hwpf/usermodel/SectionProperties;
.super Lorg/apache/poi/hwpf/model/types/SEPAbstractType;
.source "SectionProperties.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/SEPAbstractType;-><init>()V

    .line 29
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_20_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 30
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_21_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 31
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_22_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 32
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_23_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 33
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_26_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 34
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    .line 39
    .local v0, "copy":Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_20_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_20_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 40
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_21_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_21_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 41
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_22_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_22_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 42
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_23_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_23_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 43
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_26_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 44
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 43
    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->field_26_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 46
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 51
    const-class v7, Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v7

    .line 52
    invoke-virtual {v7}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 53
    .local v1, "fields":[Ljava/lang/reflect/Field;
    invoke-static {v1, v6}, Ljava/lang/reflect/AccessibleObject;->setAccessible([Ljava/lang/reflect/AccessibleObject;Z)V

    .line 56
    const/4 v4, 0x0

    .local v4, "x":I
    :goto_0
    :try_start_0
    array-length v7, v1

    if-lt v4, v7, :cond_0

    move v5, v6

    .line 74
    :goto_1
    return v5

    .line 58
    :cond_0
    aget-object v7, v1, v4

    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 59
    .local v2, "obj1":Ljava/lang/Object;
    aget-object v7, v1, v4

    invoke-virtual {v7, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 60
    .local v3, "obj2":Ljava/lang/Object;
    if-nez v2, :cond_2

    if-nez v3, :cond_2

    .line 56
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 65
    :cond_2
    if-eqz v2, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_1

    goto :goto_1

    .line 72
    .end local v2    # "obj1":Ljava/lang/Object;
    .end local v3    # "obj2":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_1
.end method
