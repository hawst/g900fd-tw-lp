.class public Lorg/apache/poi/hwpf/model/Grfhic;
.super Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;
.source "Grfhic.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;-><init>()V

    .line 43
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/GrfhicAbstractType;-><init>()V

    .line 47
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/Grfhic;->fillFields([BI)V

    .line 48
    return-void
.end method


# virtual methods
.method public toByteArray()[B
    .locals 2

    .prologue
    .line 52
    invoke-static {}, Lorg/apache/poi/hwpf/model/Grfhic;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 53
    .local v0, "buf":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/Grfhic;->serialize([BI)V

    .line 54
    return-object v0
.end method
