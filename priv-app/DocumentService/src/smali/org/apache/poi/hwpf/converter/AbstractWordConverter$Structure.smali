.class final Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;
.super Ljava/lang/Object;
.source "AbstractWordConverter.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/converter/AbstractWordConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Structure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;",
        ">;"
    }
.end annotation


# instance fields
.field final end:I

.field final start:I

.field final structure:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;II)V
    .locals 0
    .param p1, "deadFieldBoundaries"    # Lorg/apache/poi/hwpf/converter/AbstractWordConverter$DeadFieldBoundaries;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput p2, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    .line 92
    iput p3, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    .line 93
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    .line 94
    return-void
.end method

.method constructor <init>(Lorg/apache/poi/hwpf/usermodel/Bookmark;)V
    .locals 1
    .param p1, "bookmark"    # Lorg/apache/poi/hwpf/usermodel/Bookmark;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-interface {p1}, Lorg/apache/poi/hwpf/usermodel/Bookmark;->getStart()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    .line 85
    invoke-interface {p1}, Lorg/apache/poi/hwpf/usermodel/Bookmark;->getEnd()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    .line 86
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    .line 87
    return-void
.end method

.method constructor <init>(Lorg/apache/poi/hwpf/usermodel/Field;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/poi/hwpf/usermodel/Field;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-interface {p1}, Lorg/apache/poi/hwpf/usermodel/Field;->getFieldStartOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    .line 99
    invoke-interface {p1}, Lorg/apache/poi/hwpf/usermodel/Field;->getFieldEndOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    .line 100
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    .line 101
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->compareTo(Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;)I
    .locals 2
    .param p1, "o"    # Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v1, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    iget v1, p1, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Structure ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->start:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->end:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/AbstractWordConverter$Structure;->structure:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
