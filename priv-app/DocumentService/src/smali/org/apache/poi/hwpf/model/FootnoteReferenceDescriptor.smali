.class public final Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;
.super Lorg/apache/poi/hwpf/model/types/FRDAbstractType;
.source "FootnoteReferenceDescriptor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FRDAbstractType;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FRDAbstractType;-><init>()V

    .line 32
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->fillFields([BI)V

    .line 33
    return-void
.end method


# virtual methods
.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->clone()Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;

    move-result-object v0

    return-object v0
.end method

.method protected clone()Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;
    .locals 2

    .prologue
    .line 40
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p0, p1, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 54
    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 56
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 57
    check-cast v0, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;

    .line 58
    .local v0, "other":Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->field_1_nAuto:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->field_1_nAuto:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 59
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 66
    const/16 v0, 0x1f

    .line 67
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 68
    .local v1, "result":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->field_1_nAuto:S

    add-int/lit8 v1, v2, 0x1f

    .line 69
    return v1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 74
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->field_1_nAuto:S

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FootnoteReferenceDescriptor;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string/jumbo v0, "[FRD] EMPTY"

    .line 83
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/poi/hwpf/model/types/FRDAbstractType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
