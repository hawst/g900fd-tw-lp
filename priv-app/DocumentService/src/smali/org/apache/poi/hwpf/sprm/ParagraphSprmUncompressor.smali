.class public final Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;
.super Lorg/apache/poi/hwpf/sprm/SprmUncompressor;
.source "ParagraphSprmUncompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 43
    sput-object v0, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/poi/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 48
    return-void
.end method

.method private static handleTabs(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    .locals 15
    .param p0, "pap"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p1, "sprm"    # Lorg/apache/poi/hwpf/sprm/SprmOperation;

    .prologue
    .line 436
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    .line 437
    .local v2, "grpprl":[B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v5

    .line 438
    .local v5, "offset":I
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "offset":I
    .local v6, "offset":I
    aget-byte v1, v2, v5

    .line 439
    .local v1, "delSize":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgdxaTab()[I

    move-result-object v10

    .line 440
    .local v10, "tabPositions":[I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgtbd()[Lorg/apache/poi/hwpf/model/TabDescriptor;

    move-result-object v8

    .line 442
    .local v8, "tabDescriptors":[Lorg/apache/poi/hwpf/model/TabDescriptor;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 443
    .local v9, "tabMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lorg/apache/poi/hwpf/model/TabDescriptor;>;"
    const/4 v12, 0x0

    .local v12, "x":I
    :goto_0
    array-length v13, v10

    if-lt v12, v13, :cond_0

    .line 448
    const/4 v12, 0x0

    :goto_1
    if-lt v12, v1, :cond_1

    .line 454
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "offset":I
    .restart local v5    # "offset":I
    aget-byte v0, v2, v6

    .line 455
    .local v0, "addSize":I
    move v7, v5

    .line 456
    .local v7, "start":I
    const/4 v12, 0x0

    :goto_2
    if-lt v12, v0, :cond_2

    .line 464
    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v13

    new-array v10, v13, [I

    .line 465
    array-length v13, v10

    new-array v8, v13, [Lorg/apache/poi/hwpf/model/TabDescriptor;

    .line 467
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-direct {v4, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 468
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 470
    const/4 v12, 0x0

    :goto_3
    array-length v13, v10

    if-lt v12, v13, :cond_3

    .line 480
    invoke-virtual {p0, v10}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setRgdxaTab([I)V

    .line 481
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setRgtbd([Lorg/apache/poi/hwpf/model/TabDescriptor;)V

    .line 482
    return-void

    .line 445
    .end local v0    # "addSize":I
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5    # "offset":I
    .end local v7    # "start":I
    .restart local v6    # "offset":I
    :cond_0
    aget v13, v10, v12

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aget-object v14, v8, v12

    invoke-interface {v9, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 450
    :cond_1
    invoke-static {v2, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v9, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    add-int/lit8 v5, v6, 0x2

    .line 448
    .end local v6    # "offset":I
    .restart local v5    # "offset":I
    add-int/lit8 v12, v12, 0x1

    move v6, v5

    .end local v5    # "offset":I
    .restart local v6    # "offset":I
    goto :goto_1

    .line 458
    .end local v6    # "offset":I
    .restart local v0    # "addSize":I
    .restart local v5    # "offset":I
    .restart local v7    # "start":I
    :cond_2
    invoke-static {v2, v5}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 459
    .local v3, "key":Ljava/lang/Integer;
    new-instance v11, Lorg/apache/poi/hwpf/model/TabDescriptor;

    invoke-static {}, Lorg/apache/poi/hwpf/model/TabDescriptor;->getSize()I

    move-result v13

    mul-int/2addr v13, v0

    add-int/2addr v13, v12

    add-int/2addr v13, v7

    invoke-direct {v11, v2, v13}, Lorg/apache/poi/hwpf/model/TabDescriptor;-><init>([BI)V

    .line 460
    .local v11, "val":Lorg/apache/poi/hwpf/model/TabDescriptor;
    invoke-interface {v9, v3, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    add-int/lit8 v5, v5, 0x2

    .line 456
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 472
    .end local v3    # "key":Ljava/lang/Integer;
    .end local v11    # "val":Lorg/apache/poi/hwpf/model/TabDescriptor;
    .restart local v4    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_3
    invoke-interface {v4, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 473
    .restart local v3    # "key":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aput v13, v10, v12

    .line 474
    invoke-interface {v9, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 475
    invoke-interface {v9, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/TabDescriptor;

    aput-object v13, v8, v12

    .line 470
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 477
    :cond_4
    new-instance v13, Lorg/apache/poi/hwpf/model/TabDescriptor;

    invoke-direct {v13}, Lorg/apache/poi/hwpf/model/TabDescriptor;-><init>()V

    aput-object v13, v8, v12

    goto :goto_4
.end method

.method static unCompressPAPOperation(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    .locals 13
    .param p0, "newPAP"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p1, "sprm"    # Lorg/apache/poi/hwpf/sprm/SprmOperation;

    .prologue
    const/16 v12, 0x9

    const/4 v11, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 104
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 429
    :pswitch_0
    sget-object v9, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Unknown PAP sprm ignored: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 432
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 107
    :pswitch_2
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIstd(I)V

    goto :goto_0

    .line 120
    :pswitch_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v9

    if-le v9, v12, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v9

    if-lt v9, v8, :cond_0

    .line 122
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v9

    int-to-byte v3, v9

    .line 123
    .local v3, "paramTmp":B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {p0, v9}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIstd(I)V

    .line 124
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLvl()B

    move-result v9

    add-int/2addr v9, v3

    int-to-byte v9, v9

    invoke-virtual {p0, v9}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setLvl(B)V

    .line 126
    shr-int/lit8 v9, v3, 0x7

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_2

    .line 128
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v9

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIstd(I)V

    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v8

    invoke-static {v8, v12}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIstd(I)V

    goto :goto_0

    .line 139
    .end local v3    # "paramTmp":B
    :pswitch_4
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setJc(B)V

    goto :goto_0

    .line 142
    :pswitch_5
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_3

    :goto_1
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFSideBySide(Z)V

    goto :goto_0

    :cond_3
    move v8, v9

    goto :goto_1

    .line 145
    :pswitch_6
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_4

    :goto_2
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFKeep(Z)V

    goto :goto_0

    :cond_4
    move v8, v9

    goto :goto_2

    .line 148
    :pswitch_7
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_5

    :goto_3
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFKeepFollow(Z)V

    goto :goto_0

    :cond_5
    move v8, v9

    goto :goto_3

    .line 151
    :pswitch_8
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_6

    :goto_4
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFPageBreakBefore(Z)V

    goto :goto_0

    :cond_6
    move v8, v9

    goto :goto_4

    .line 154
    :pswitch_9
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcl(B)V

    goto/16 :goto_0

    .line 157
    :pswitch_a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcp(B)V

    goto/16 :goto_0

    .line 160
    :pswitch_b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIlvl(B)V

    goto/16 :goto_0

    .line 164
    :pswitch_c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperandShortSigned()S

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIlfo(I)V

    goto/16 :goto_0

    .line 167
    :pswitch_d
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_7

    :goto_5
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFNoLnn(Z)V

    goto/16 :goto_0

    :cond_7
    move v8, v9

    goto :goto_5

    .line 171
    :pswitch_e
    invoke-static {p0, p1}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->handleTabs(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V

    goto/16 :goto_0

    .line 174
    :pswitch_f
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaRight(I)V

    goto/16 :goto_0

    .line 177
    :pswitch_10
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 182
    :pswitch_11
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft()I

    move-result v8

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    add-int/2addr v8, v10

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft(I)V

    .line 183
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft()I

    move-result v8

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 186
    :pswitch_12
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft1(I)V

    goto/16 :goto_0

    .line 189
    :pswitch_13
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setLspd(Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;)V

    goto/16 :goto_0

    .line 192
    :pswitch_14
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaBefore(I)V

    goto/16 :goto_0

    .line 195
    :pswitch_15
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaAfter(I)V

    goto/16 :goto_0

    .line 203
    :pswitch_16
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_8

    :goto_6
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFInTable(Z)V

    goto/16 :goto_0

    :cond_8
    move v8, v9

    goto :goto_6

    .line 206
    :pswitch_17
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_9

    :goto_7
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFTtp(Z)V

    goto/16 :goto_0

    :cond_9
    move v8, v9

    goto :goto_7

    .line 209
    :pswitch_18
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaAbs(I)V

    goto/16 :goto_0

    .line 212
    :pswitch_19
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaAbs(I)V

    goto/16 :goto_0

    .line 215
    :pswitch_1a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaWidth(I)V

    goto/16 :goto_0

    .line 218
    :pswitch_1b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v2, v8

    .line 220
    .local v2, "param":B
    and-int/lit8 v8, v2, 0xc

    shr-int/lit8 v8, v8, 0x2

    int-to-byte v5, v8

    .line 221
    .local v5, "pcVert":B
    and-int/lit8 v8, v2, 0x3

    int-to-byte v4, v8

    .line 222
    .local v4, "pcHorz":B
    if-eq v5, v11, :cond_a

    .line 224
    invoke-virtual {p0, v5}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setPcVert(B)V

    .line 226
    :cond_a
    if-eq v4, v11, :cond_0

    .line 228
    invoke-virtual {p0, v4}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setPcHorz(B)V

    goto/16 :goto_0

    .line 253
    .end local v2    # "param":B
    .end local v4    # "pcHorz":B
    .end local v5    # "pcVert":B
    :pswitch_1c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaFromText(I)V

    goto/16 :goto_0

    .line 256
    :pswitch_1d
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setWr(B)V

    goto/16 :goto_0

    .line 259
    :pswitch_1e
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 262
    :pswitch_1f
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 265
    :pswitch_20
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 268
    :pswitch_21
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 271
    :pswitch_22
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBetween(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 274
    :pswitch_23
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setBrcBar(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 277
    :pswitch_24
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_b

    :goto_8
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFNoAutoHyph(Z)V

    goto/16 :goto_0

    :cond_b
    move v8, v9

    goto :goto_8

    .line 280
    :pswitch_25
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaHeight(I)V

    goto/16 :goto_0

    .line 283
    :pswitch_26
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v9

    int-to-short v9, v9

    invoke-direct {v8, v9}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;-><init>(S)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDcs(Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;)V

    goto/16 :goto_0

    .line 286
    :pswitch_27
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v9

    int-to-short v9, v9

    invoke-direct {v8, v9}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;-><init>(S)V

    .line 287
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor80;->toShadingDescriptor()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v8

    .line 286
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    goto/16 :goto_0

    .line 290
    :pswitch_28
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDyaFromText(I)V

    goto/16 :goto_0

    .line 293
    :pswitch_29
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaFromText(I)V

    goto/16 :goto_0

    .line 296
    :pswitch_2a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_c

    :goto_9
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFLocked(Z)V

    goto/16 :goto_0

    :cond_c
    move v8, v9

    goto :goto_9

    .line 299
    :pswitch_2b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_d

    :goto_a
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFWidowControl(Z)V

    goto/16 :goto_0

    :cond_d
    move v8, v9

    goto :goto_a

    .line 302
    :pswitch_2c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_e

    :goto_b
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFKinsoku(Z)V

    goto/16 :goto_0

    :cond_e
    move v8, v9

    goto :goto_b

    .line 305
    :pswitch_2d
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_f

    :goto_c
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFWordWrap(Z)V

    goto/16 :goto_0

    :cond_f
    move v8, v9

    goto :goto_c

    .line 308
    :pswitch_2e
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_10

    :goto_d
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFOverflowPunct(Z)V

    goto/16 :goto_0

    :cond_10
    move v8, v9

    goto :goto_d

    .line 311
    :pswitch_2f
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_11

    :goto_e
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFTopLinePunct(Z)V

    goto/16 :goto_0

    :cond_11
    move v8, v9

    goto :goto_e

    .line 314
    :pswitch_30
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_12

    :goto_f
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFAutoSpaceDE(Z)V

    goto/16 :goto_0

    :cond_12
    move v8, v9

    goto :goto_f

    .line 317
    :pswitch_31
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_13

    :goto_10
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFAutoSpaceDN(Z)V

    goto/16 :goto_0

    :cond_13
    move v8, v9

    goto :goto_10

    .line 320
    :pswitch_32
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setWAlignFont(I)V

    goto/16 :goto_0

    .line 323
    :pswitch_33
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-short v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFontAlign(S)V

    goto/16 :goto_0

    .line 330
    :pswitch_34
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x3

    new-array v0, v8, [B

    .line 331
    .local v0, "buf":[B
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v8

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    .line 332
    array-length v11, v0

    .line 331
    invoke-static {v0, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 333
    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setAnld([B)V

    goto/16 :goto_0

    .line 339
    .end local v0    # "buf":[B
    :pswitch_35
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v7

    .line 340
    .local v7, "varParam":[B
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v1

    .line 341
    .local v1, "offset":I
    aget-byte v10, v7, v1

    if-eqz v10, :cond_14

    :goto_11
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFPropRMark(Z)V

    .line 342
    add-int/lit8 v8, v1, 0x1

    invoke-static {v7, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setIbstPropRMark(I)V

    .line 343
    new-instance v8, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    add-int/lit8 v9, v1, 0x3

    invoke-direct {v8, v7, v9}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>([BI)V

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V

    goto/16 :goto_0

    :cond_14
    move v8, v9

    .line 341
    goto :goto_11

    .line 351
    .end local v1    # "offset":I
    .end local v7    # "varParam":[B
    :pswitch_36
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setLvl(B)V

    goto/16 :goto_0

    .line 356
    :pswitch_37
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_15

    :goto_12
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFBiDi(Z)V

    goto/16 :goto_0

    :cond_15
    move v8, v9

    goto :goto_12

    .line 361
    :pswitch_38
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_16

    :goto_13
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFNumRMIns(Z)V

    goto/16 :goto_0

    :cond_16
    move v8, v9

    goto :goto_13

    .line 368
    :pswitch_39
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v8

    const/4 v10, 0x6

    if-ne v8, v10, :cond_0

    .line 370
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x3

    new-array v0, v8, [B

    .line 371
    .restart local v0    # "buf":[B
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v8

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v10

    array-length v11, v0

    invoke-static {v0, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 372
    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setNumrm([B)V

    goto/16 :goto_0

    .line 381
    .end local v0    # "buf":[B
    :pswitch_3a
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_17

    :goto_14
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFUsePgsuSettings(Z)V

    goto/16 :goto_0

    :cond_17
    move v8, v9

    goto :goto_14

    .line 384
    :pswitch_3b
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_18

    :goto_15
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFAdjustRight(Z)V

    goto/16 :goto_0

    :cond_18
    move v8, v9

    goto :goto_15

    .line 388
    :pswitch_3c
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setItap(I)V

    goto/16 :goto_0

    .line 392
    :pswitch_3d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItap()I

    move-result v8

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v9

    add-int/2addr v8, v9

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setItap(I)V

    goto/16 :goto_0

    .line 396
    :pswitch_3e
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_19

    :goto_16
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFInnerTableCell(Z)V

    goto/16 :goto_0

    :cond_19
    move v8, v9

    goto :goto_16

    .line 400
    :pswitch_3f
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v10

    if-eqz v10, :cond_1a

    :goto_17
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setFTtpEmbedded(Z)V

    goto/16 :goto_0

    :cond_1a
    move v8, v9

    goto :goto_17

    .line 404
    :pswitch_40
    new-instance v6, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 405
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v8

    .line 404
    invoke-direct {v6, v8, v11}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>([BI)V

    .line 406
    .local v6, "shadingDescriptor":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    invoke-virtual {p0, v6}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setShading(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V

    goto/16 :goto_0

    .line 410
    .end local v6    # "shadingDescriptor":Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    :pswitch_41
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaRight(I)V

    goto/16 :goto_0

    .line 414
    :pswitch_42
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 418
    :pswitch_43
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setDxaLeft1(I)V

    goto/16 :goto_0

    .line 422
    :pswitch_44
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setJustificationLogical(B)V

    goto/16 :goto_0

    .line 426
    :pswitch_45
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {p0, v8, v9}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->setRsid(J)V

    goto/16 :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_1
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_0
        :pswitch_38
        :pswitch_1
        :pswitch_39
        :pswitch_0
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_41
        :pswitch_42
        :pswitch_0
        :pswitch_43
        :pswitch_44
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_45
    .end packed-switch
.end method

.method public static uncompressPAP(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;[BI)Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .locals 9
    .param p0, "parent"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 54
    const/4 v2, 0x0

    .line 57
    .local v2, "newProperties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->clone()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "newProperties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    check-cast v2, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .restart local v2    # "newProperties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    new-instance v4, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    invoke-direct {v4, p1, p2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 65
    .local v4, "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 87
    return-object v2

    .line 59
    .end local v2    # "newProperties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .end local v4    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :catch_0
    move-exception v0

    .line 61
    .local v0, "cnse":Ljava/lang/CloneNotSupportedException;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "There is no way this exception should happen!!"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 67
    .end local v0    # "cnse":Ljava/lang/CloneNotSupportedException;
    .restart local v2    # "newProperties":Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .restart local v4    # "sprmIt":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_1
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v3

    .line 71
    .local v3, "sprm":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 75
    :try_start_1
    invoke-static {v2, v3}, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->unCompressPAPOperation(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/sprm/SprmOperation;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 77
    :catch_1
    move-exception v1

    .line 79
    .local v1, "exc":Ljava/lang/Exception;
    sget-object v5, Lorg/apache/poi/hwpf/sprm/ParagraphSprmUncompressor;->logger:Lorg/apache/poi/util/POILogger;

    .line 80
    const/4 v6, 0x7

    .line 81
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Unable to apply SPRM operation \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\': "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 81
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 79
    invoke-virtual {v5, v6, v7, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
