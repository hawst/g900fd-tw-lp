.class Lorg/apache/poi/hwpf/model/SttbUtils;
.super Ljava/lang/Object;
.source "SttbUtils.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final CDATA_SIZE_STTBF_BKMK:I = 0x2

.field private static final CDATA_SIZE_STTBF_R_MARK:I = 0x2

.field private static final CDATA_SIZE_STTB_SAVED_BY:I = 0x2


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static readSttbSavedBy([BI)[Ljava/lang/String;
    .locals 2
    .param p0, "buffer"    # [B
    .param p1, "startOffset"    # I

    .prologue
    .line 52
    new-instance v0, Lorg/apache/poi/hwpf/model/Sttb;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0, p1}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[BI)V

    .line 53
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->getData()[Ljava/lang/String;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method static readSttbfBkmk([BI)[Ljava/lang/String;
    .locals 2
    .param p0, "buffer"    # [B
    .param p1, "startOffset"    # I

    .prologue
    .line 41
    new-instance v0, Lorg/apache/poi/hwpf/model/Sttb;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0, p1}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[BI)V

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->getData()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static readSttbfRMark([BI)[Ljava/lang/String;
    .locals 2
    .param p0, "buffer"    # [B
    .param p1, "startOffset"    # I

    .prologue
    .line 46
    new-instance v0, Lorg/apache/poi/hwpf/model/Sttb;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0, p1}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[BI)V

    .line 47
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->getData()[Ljava/lang/String;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method static writeSttbSavedBy([Ljava/lang/String;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .param p0, "data"    # [Ljava/lang/String;
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lorg/apache/poi/hwpf/model/Sttb;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->serialize()[B

    move-result-object v0

    .line 72
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 74
    return-void
.end method

.method static writeSttbfBkmk([Ljava/lang/String;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .param p0, "data"    # [Ljava/lang/String;
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lorg/apache/poi/hwpf/model/Sttb;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->serialize()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 60
    return-void
.end method

.method static writeSttbfRMark([Ljava/lang/String;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .param p0, "data"    # [Ljava/lang/String;
    .param p1, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Lorg/apache/poi/hwpf/model/Sttb;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/apache/poi/hwpf/model/Sttb;-><init>(I[Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Sttb;->serialize()[B

    move-result-object v0

    .line 65
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 67
    return-void
.end method
