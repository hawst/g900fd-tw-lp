.class public final Lorg/apache/poi/hwpf/usermodel/TableCell;
.super Lorg/apache/poi/hwpf/usermodel/Range;
.source "TableCell.java"


# instance fields
.field private _leftEdge:I

.field private _tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

.field private _width:I


# direct methods
.method public constructor <init>(IILorg/apache/poi/hwpf/usermodel/TableRow;ILorg/apache/poi/hwpf/usermodel/TableCellDescriptor;II)V
    .locals 0
    .param p1, "startIdxInclusive"    # I
    .param p2, "endIdxExclusive"    # I
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/TableRow;
    .param p4, "levelNum"    # I
    .param p5, "tcd"    # Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .param p6, "leftEdge"    # I
    .param p7, "width"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 33
    iput-object p5, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    .line 34
    iput p6, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_leftEdge:I

    .line 35
    iput p7, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_width:I

    .line 37
    return-void
.end method


# virtual methods
.method public getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptor()Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    return-object v0
.end method

.method public getLeftEdge()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_leftEdge:I

    return v0
.end method

.method public getVertAlign()B
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->getVertAlign()B

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_width:I

    return v0
.end method

.method public isBackward()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFBackward()Z

    move-result v0

    return v0
.end method

.method public isFirstMerged()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFFirstMerged()Z

    move-result v0

    return v0
.end method

.method public isFirstVerticallyMerged()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFVertRestart()Z

    move-result v0

    return v0
.end method

.method public isMerged()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFMerged()Z

    move-result v0

    return v0
.end method

.method public isRotateFont()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFRotateFont()Z

    move-result v0

    return v0
.end method

.method public isVertical()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFVertical()Z

    move-result v0

    return v0
.end method

.method public isVerticallyMerged()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/TableCell;->_tcd:Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;->isFVertMerge()Z

    move-result v0

    return v0
.end method
