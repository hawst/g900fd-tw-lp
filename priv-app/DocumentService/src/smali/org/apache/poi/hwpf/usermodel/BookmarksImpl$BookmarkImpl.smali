.class final Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;
.super Ljava/lang/Object;
.source "BookmarksImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/Bookmark;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BookmarkImpl"
.end annotation


# instance fields
.field private final first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

.field final synthetic this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;


# direct methods
.method private constructor <init>(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V
    .locals 0
    .param p2, "first"    # Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 45
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p2, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;Lorg/apache/poi/hwpf/model/GenericPropertyNode;Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;-><init>(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p0, p1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 55
    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 57
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 58
    check-cast v0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;

    .line 59
    .local v0, "other":Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    if-nez v3, :cond_4

    .line 61
    iget-object v3, v0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    if-eqz v3, :cond_0

    move v1, v2

    .line 62
    goto :goto_0

    .line 64
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    iget-object v4, v0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public getEnd()I
    .locals 5

    .prologue
    .line 71
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    # getter for: Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;
    invoke-static {v3}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorFirstIndex(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)I

    move-result v0

    .line 74
    .local v0, "currentIndex":I
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    # getter for: Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;
    invoke-static {v3}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;

    move-result-object v3

    .line 75
    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorLim(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v1

    .line 76
    .local v1, "descriptorLim":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 80
    .end local v1    # "descriptorLim":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    :goto_0
    return v3

    .line 78
    :catch_0
    move-exception v2

    .line 80
    .local v2, "exc":Ljava/lang/IndexOutOfBoundsException;
    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v3

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    # getter for: Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;
    invoke-static {v2}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorFirstIndex(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)I

    move-result v0

    .line 89
    .local v0, "currentIndex":I
    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    # getter for: Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;
    invoke-static {v2}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getName(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 93
    :goto_0
    return-object v2

    .line 91
    :catch_0
    move-exception v1

    .line 93
    .local v1, "exc":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v2, ""

    goto :goto_0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    # getter for: Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;
    invoke-static {v1}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->first:Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/model/BookmarksTables;->getDescriptorFirstIndex(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)I

    move-result v0

    .line 111
    .local v0, "currentIndex":I
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->this$0:Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;

    # getter for: Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->bookmarksTables:Lorg/apache/poi/hwpf/model/BookmarksTables;
    invoke-static {v1}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;->access$0(Lorg/apache/poi/hwpf/usermodel/BookmarksImpl;)Lorg/apache/poi/hwpf/model/BookmarksTables;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lorg/apache/poi/hwpf/model/BookmarksTables;->setName(ILjava/lang/String;)V

    .line 112
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Bookmark ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->getStart()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->getEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "): name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 118
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
