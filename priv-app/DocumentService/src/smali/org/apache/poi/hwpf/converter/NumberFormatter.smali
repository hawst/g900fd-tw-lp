.class public final Lorg/apache/poi/hwpf/converter/NumberFormatter;
.super Ljava/lang/Object;
.source "NumberFormatter.java"


# static fields
.field private static final ENGLISH_LETTERS:[Ljava/lang/String;

.field private static final ROMAN_LETTERS:[Ljava/lang/String;

.field private static final ROMAN_VALUES:[I

.field private static final T_ARABIC:I = 0x0

.field private static final T_LOWER_LETTER:I = 0x4

.field private static final T_LOWER_ROMAN:I = 0x2

.field private static final T_ORDINAL:I = 0x5

.field private static final T_UPPER_LETTER:I = 0x3

.field private static final T_UPPER_ROMAN:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xd

    .line 33
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "a"

    aput-object v1, v0, v4

    const-string/jumbo v1, "b"

    aput-object v1, v0, v5

    .line 34
    const-string/jumbo v1, "c"

    aput-object v1, v0, v6

    const-string/jumbo v1, "d"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "m"

    aput-object v2, v0, v1

    const-string/jumbo v1, "n"

    aput-object v1, v0, v3

    const/16 v1, 0xe

    const-string/jumbo v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 35
    const-string/jumbo v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "q"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "r"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "t"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "u"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "w"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "z"

    aput-object v2, v0, v1

    .line 33
    sput-object v0, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ENGLISH_LETTERS:[Ljava/lang/String;

    .line 37
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "m"

    aput-object v1, v0, v4

    const-string/jumbo v1, "cm"

    aput-object v1, v0, v5

    const-string/jumbo v1, "d"

    aput-object v1, v0, v6

    const-string/jumbo v1, "cd"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 38
    const-string/jumbo v2, "xc"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "l"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "xl"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "ix"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "iv"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "i"

    aput-object v2, v0, v1

    .line 37
    sput-object v0, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ROMAN_LETTERS:[Ljava/lang/String;

    .line 40
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ROMAN_VALUES:[I

    .line 48
    return-void

    .line 40
    nop

    :array_0
    .array-data 4
        0x3e8
        0x384
        0x1f4
        0x190
        0x64
        0x5a
        0x32
        0x28
        0xa
        0x9
        0x5
        0x4
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNumber(II)Ljava/lang/String;
    .locals 1
    .param p0, "num"    # I
    .param p1, "style"    # I

    .prologue
    .line 52
    packed-switch p1, :pswitch_data_0

    .line 65
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 55
    :pswitch_0
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/NumberFormatter;->toRoman(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 57
    :pswitch_1
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/NumberFormatter;->toRoman(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 59
    :pswitch_2
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/NumberFormatter;->toLetters(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :pswitch_3
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/NumberFormatter;->toLetters(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static toLetters(I)Ljava/lang/String;
    .locals 18
    .param p0, "number"    # I

    .prologue
    .line 71
    const/16 v2, 0x1a

    .line 73
    .local v2, "base":I
    if-gtz p0, :cond_0

    .line 74
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "Unsupported number: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 76
    :cond_0
    const/16 v14, 0x1b

    move/from16 v0, p0

    if-ge v0, v14, :cond_1

    .line 77
    sget-object v14, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ENGLISH_LETTERS:[Ljava/lang/String;

    add-int/lit8 v15, p0, -0x1

    aget-object v14, v14, v15

    .line 116
    :goto_0
    return-object v14

    .line 79
    :cond_1
    move/from16 v0, p0

    int-to-long v12, v0

    .line 81
    .local v12, "toProcess":J
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .local v9, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 84
    .local v7, "maxPower":I
    const/4 v4, 0x0

    .line 85
    .local v4, "boundary":I
    :cond_2
    int-to-long v14, v4

    cmp-long v14, v12, v14

    if-gtz v14, :cond_3

    .line 95
    add-int/lit8 v7, v7, -0x1

    .line 97
    move v8, v7

    .end local v4    # "boundary":I
    .local v8, "p":I
    :goto_1
    if-gtz v8, :cond_4

    .line 115
    sget-object v14, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ENGLISH_LETTERS:[Ljava/lang/String;

    long-to-int v15, v12

    add-int/lit8 v15, v15, -0x1

    aget-object v14, v14, v15

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto :goto_0

    .line 87
    .end local v8    # "p":I
    .restart local v4    # "boundary":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    .line 88
    mul-int/lit8 v14, v4, 0x1a

    add-int/lit8 v4, v14, 0x1a

    .line 90
    const v14, 0x7fffffff

    if-le v4, v14, :cond_2

    .line 91
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "Unsupported number: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v15, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 91
    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 99
    .end local v4    # "boundary":I
    .restart local v8    # "p":I
    :cond_4
    const-wide/16 v4, 0x0

    .line 100
    .local v4, "boundary":J
    const-wide/16 v10, 0x1

    .line 101
    .local v10, "shift":J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    if-lt v6, v8, :cond_5

    .line 107
    const/4 v3, 0x0

    .line 108
    .local v3, "count":I
    :goto_3
    cmp-long v14, v12, v4

    if-gtz v14, :cond_6

    .line 113
    sget-object v14, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ENGLISH_LETTERS:[Ljava/lang/String;

    add-int/lit8 v15, v3, -0x1

    aget-object v14, v14, v15

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    add-int/lit8 v8, v8, -0x1

    goto :goto_1

    .line 103
    .end local v3    # "count":I
    :cond_5
    const-wide/16 v14, 0x1a

    mul-long/2addr v10, v14

    .line 104
    const-wide/16 v14, 0x1a

    mul-long/2addr v14, v4

    const-wide/16 v16, 0x1a

    add-long v4, v14, v16

    .line 101
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 110
    .restart local v3    # "count":I
    :cond_6
    add-int/lit8 v3, v3, 0x1

    .line 111
    sub-long/2addr v12, v10

    goto :goto_3
.end method

.method private static toRoman(I)Ljava/lang/String;
    .locals 7
    .param p0, "number"    # I

    .prologue
    .line 121
    if-gtz p0, :cond_0

    .line 122
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Unsupported number: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 124
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v2, "result":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ROMAN_LETTERS:[Ljava/lang/String;

    array-length v4, v4

    if-lt v0, v4, :cond_1

    .line 136
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 128
    :cond_1
    sget-object v4, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ROMAN_LETTERS:[Ljava/lang/String;

    aget-object v1, v4, v0

    .line 129
    .local v1, "letter":Ljava/lang/String;
    sget-object v4, Lorg/apache/poi/hwpf/converter/NumberFormatter;->ROMAN_VALUES:[I

    aget v3, v4, v0

    .line 130
    .local v3, "value":I
    :goto_1
    if-ge p0, v3, :cond_2

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_2
    sub-int/2addr p0, v3

    .line 133
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
