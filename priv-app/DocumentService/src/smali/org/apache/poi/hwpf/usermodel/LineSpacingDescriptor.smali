.class public final Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
.super Ljava/lang/Object;
.source "LineSpacingDescriptor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field _dyaLine:S

.field _fMultiLinespace:S


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/16 v0, 0xf0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    .line 37
    const/4 v0, 0x1

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    .line 38
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    .line 43
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    .line 44
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 77
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 78
    check-cast v0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    .line 80
    .local v0, "lspd":Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    iget-short v3, v0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 82
    .end local v0    # "lspd":Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;
    :cond_0
    return v1
.end method

.method public get_dyaLine()S
    .locals 1

    .prologue
    .line 101
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    return v0
.end method

.method public get_fMultiLinespace()S
    .locals 1

    .prologue
    .line 105
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 87
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    if-nez v0, :cond_0

    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 66
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    invoke-static {p1, p2, v0}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 67
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 68
    return-void
.end method

.method public setDyaLine(S)V
    .locals 0
    .param p1, "dyaLine"    # S

    .prologue
    .line 72
    iput-short p1, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    .line 73
    return-void
.end method

.method public setMultiLinespace(S)V
    .locals 0
    .param p1, "fMultiLinespace"    # S

    .prologue
    .line 54
    iput-short p1, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    .line 55
    return-void
.end method

.method public toInt()I
    .locals 2

    .prologue
    .line 59
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 60
    .local v0, "intHolder":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->serialize([BI)V

    .line 61
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string/jumbo v0, "[LSPD] EMPTY"

    .line 96
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[LSPD] (dyaLine: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_dyaLine:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; fMultLinespace: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 97
    iget-short v1, p0, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->_fMultiLinespace:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
