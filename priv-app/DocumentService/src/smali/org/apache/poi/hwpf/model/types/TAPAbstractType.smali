.class public abstract Lorg/apache/poi/hwpf/model/types/TAPAbstractType;
.super Ljava/lang/Object;
.source "TAPAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static fAdjusted:Lorg/apache/poi/util/BitField;

.field private static fAutofit:Lorg/apache/poi/util/BitField;

.field private static fCellSpacing:Lorg/apache/poi/util/BitField;

.field private static fFirstRow:Lorg/apache/poi/util/BitField;

.field private static fInvalAutofit:Lorg/apache/poi/util/BitField;

.field private static fKeepFollow:Lorg/apache/poi/util/BitField;

.field private static fLastRow:Lorg/apache/poi/util/BitField;

.field private static fNeverBeenAutofit:Lorg/apache/poi/util/BitField;

.field private static fNotPageView:Lorg/apache/poi/util/BitField;

.field private static fOrigWordTableRules:Lorg/apache/poi/util/BitField;

.field private static fOutline:Lorg/apache/poi/util/BitField;

.field private static fVert:Lorg/apache/poi/util/BitField;

.field private static fWebView:Lorg/apache/poi/util/BitField;

.field private static fWrapToWwd:Lorg/apache/poi/util/BitField;

.field private static ftsWidth:Lorg/apache/poi/util/BitField;

.field private static ftsWidthAfter:Lorg/apache/poi/util/BitField;

.field private static ftsWidthBefore:Lorg/apache/poi/util/BitField;

.field private static ftsWidthIndent:Lorg/apache/poi/util/BitField;

.field private static grpfTap_unused:Lorg/apache/poi/util/BitField;

.field private static pcHorz:Lorg/apache/poi/util/BitField;

.field private static pcVert:Lorg/apache/poi/util/BitField;

.field private static viewFlags_unused1:Lorg/apache/poi/util/BitField;

.field private static viewFlags_unused2:Lorg/apache/poi/util/BitField;

.field private static widthAndFitsFlags_empty1:Lorg/apache/poi/util/BitField;

.field private static widthAndFitsFlags_empty2:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_10_wWidthIndent:S

.field protected field_11_wWidthBefore:S

.field protected field_12_wWidthAfter:S

.field protected field_13_widthAndFitsFlags:I

.field protected field_14_dxaAbs:I

.field protected field_15_dyaAbs:I

.field protected field_16_dxaFromText:I

.field protected field_17_dyaFromText:I

.field protected field_18_dxaFromTextRight:I

.field protected field_19_dyaFromTextBottom:I

.field protected field_1_istd:S

.field protected field_20_fBiDi:B

.field protected field_21_fRTL:B

.field protected field_22_fNoAllowOverlap:B

.field protected field_23_fSpare:B

.field protected field_24_grpfTap:I

.field protected field_25_internalFlags:I

.field protected field_26_itcMac:S

.field protected field_27_dxaAdjust:I

.field protected field_28_dxaWebView:I

.field protected field_29_dxaRTEWrapWidth:I

.field protected field_2_jc:S

.field protected field_30_dxaColWidthWwd:I

.field protected field_31_pctWwd:S

.field protected field_32_viewFlags:I

.field protected field_33_rgdxaCenter:[S

.field protected field_34_rgdxaCenterPrint:[S

.field protected field_35_shdTable:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

.field protected field_36_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_37_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_38_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_39_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_3_dxaGapHalf:I

.field protected field_40_brcVertical:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_41_brcHorizontal:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_42_wCellPaddingDefaultTop:S

.field protected field_43_wCellPaddingDefaultLeft:S

.field protected field_44_wCellPaddingDefaultBottom:S

.field protected field_45_wCellPaddingDefaultRight:S

.field protected field_46_ftsCellPaddingDefaultTop:B

.field protected field_47_ftsCellPaddingDefaultLeft:B

.field protected field_48_ftsCellPaddingDefaultBottom:B

.field protected field_49_ftsCellPaddingDefaultRight:B

.field protected field_4_dyaRowHeight:I

.field protected field_50_wCellSpacingDefaultTop:S

.field protected field_51_wCellSpacingDefaultLeft:S

.field protected field_52_wCellSpacingDefaultBottom:S

.field protected field_53_wCellSpacingDefaultRight:S

.field protected field_54_ftsCellSpacingDefaultTop:B

.field protected field_55_ftsCellSpacingDefaultLeft:B

.field protected field_56_ftsCellSpacingDefaultBottom:B

.field protected field_57_ftsCellSpacingDefaultRight:B

.field protected field_58_wCellPaddingOuterTop:S

.field protected field_59_wCellPaddingOuterLeft:S

.field protected field_5_fCantSplit:Z

.field protected field_60_wCellPaddingOuterBottom:S

.field protected field_61_wCellPaddingOuterRight:S

.field protected field_62_ftsCellPaddingOuterTop:B

.field protected field_63_ftsCellPaddingOuterLeft:B

.field protected field_64_ftsCellPaddingOuterBottom:B

.field protected field_65_ftsCellPaddingOuterRight:B

.field protected field_66_wCellSpacingOuterTop:S

.field protected field_67_wCellSpacingOuterLeft:S

.field protected field_68_wCellSpacingOuterBottom:S

.field protected field_69_wCellSpacingOuterRight:S

.field protected field_6_fCantSplit90:Z

.field protected field_70_ftsCellSpacingOuterTop:B

.field protected field_71_ftsCellSpacingOuterLeft:B

.field protected field_72_ftsCellSpacingOuterBottom:B

.field protected field_73_ftsCellSpacingOuterRight:B

.field protected field_74_rgtc:[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

.field protected field_75_rgshd:[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

.field protected field_76_fPropRMark:B

.field protected field_77_fHasOldProps:B

.field protected field_78_cHorzBands:S

.field protected field_79_cVertBands:S

.field protected field_7_fTableHeader:Z

.field protected field_80_rgbrcInsideDefault_0:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_81_rgbrcInsideDefault_1:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_8_tlp:Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

.field protected field_9_wWidth:S


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 54
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fAutofit:Lorg/apache/poi/util/BitField;

    .line 55
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fKeepFollow:Lorg/apache/poi/util/BitField;

    .line 56
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1c

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidth:Lorg/apache/poi/util/BitField;

    .line 57
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xe0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthIndent:Lorg/apache/poi/util/BitField;

    .line 58
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x700

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthBefore:Lorg/apache/poi/util/BitField;

    .line 59
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x3800

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthAfter:Lorg/apache/poi/util/BitField;

    .line 60
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x4000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fNeverBeenAutofit:Lorg/apache/poi/util/BitField;

    .line 61
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0x8000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fInvalAutofit:Lorg/apache/poi/util/BitField;

    .line 62
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x70000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->widthAndFitsFlags_empty1:Lorg/apache/poi/util/BitField;

    .line 63
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x80000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fVert:Lorg/apache/poi/util/BitField;

    .line 64
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x300000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->pcVert:Lorg/apache/poi/util/BitField;

    .line 65
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0xc00000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->pcHorz:Lorg/apache/poi/util/BitField;

    .line 66
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->widthAndFitsFlags_empty2:Lorg/apache/poi/util/BitField;

    .line 79
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fFirstRow:Lorg/apache/poi/util/BitField;

    .line 80
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fLastRow:Lorg/apache/poi/util/BitField;

    .line 81
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v4}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    .line 82
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v5}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fOrigWordTableRules:Lorg/apache/poi/util/BitField;

    .line 83
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v6}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fCellSpacing:Lorg/apache/poi/util/BitField;

    .line 84
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0xffe0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->grpfTap_unused:Lorg/apache/poi/util/BitField;

    .line 92
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fWrapToWwd:Lorg/apache/poi/util/BitField;

    .line 93
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fNotPageView:Lorg/apache/poi/util/BitField;

    .line 94
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v4}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->viewFlags_unused1:Lorg/apache/poi/util/BitField;

    .line 95
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v5}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fWebView:Lorg/apache/poi/util/BitField;

    .line 96
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v6}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fAdjusted:Lorg/apache/poi/util/BitField;

    .line 97
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0xffe0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->viewFlags_unused2:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_8_tlp:Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    .line 151
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_33_rgdxaCenter:[S

    .line 152
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_34_rgdxaCenterPrint:[S

    .line 153
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_35_shdTable:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 154
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_36_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 155
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_37_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 156
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_38_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 157
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_39_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 158
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_40_brcVertical:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 159
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_41_brcHorizontal:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 160
    new-array v0, v1, [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_74_rgtc:[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    .line 161
    new-array v0, v1, [Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_75_rgshd:[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 162
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_80_rgbrcInsideDefault_0:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 163
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_81_rgbrcInsideDefault_1:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 164
    return-void
.end method


# virtual methods
.method public getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 999
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_36_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcHorizontal()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1089
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_41_brcHorizontal:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1035
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_38_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1053
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_39_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1017
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_37_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcVertical()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1071
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_40_brcVertical:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getCHorzBands()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1755
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_78_cHorzBands:S

    return v0
.end method

.method public getCVertBands()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1773
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_79_cVertBands:S

    return v0
.end method

.method public getDxaAbs()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 603
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_14_dxaAbs:I

    return v0
.end method

.method public getDxaAdjust()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 837
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_27_dxaAdjust:I

    return v0
.end method

.method public getDxaColWidthWwd()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 891
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_30_dxaColWidthWwd:I

    return v0
.end method

.method public getDxaFromText()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 639
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_16_dxaFromText:I

    return v0
.end method

.method public getDxaFromTextRight()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 675
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_18_dxaFromTextRight:I

    return v0
.end method

.method public getDxaGapHalf()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 405
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_3_dxaGapHalf:I

    return v0
.end method

.method public getDxaRTEWrapWidth()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 873
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_29_dxaRTEWrapWidth:I

    return v0
.end method

.method public getDxaWebView()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 855
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_28_dxaWebView:I

    return v0
.end method

.method public getDyaAbs()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 621
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_15_dyaAbs:I

    return v0
.end method

.method public getDyaFromText()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 657
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_17_dyaFromText:I

    return v0
.end method

.method public getDyaFromTextBottom()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 693
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_19_dyaFromTextBottom:I

    return v0
.end method

.method public getDyaRowHeight()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 423
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_4_dyaRowHeight:I

    return v0
.end method

.method public getFBiDi()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 711
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_20_fBiDi:B

    return v0
.end method

.method public getFCantSplit()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 441
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_5_fCantSplit:Z

    return v0
.end method

.method public getFCantSplit90()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 459
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_6_fCantSplit90:Z

    return v0
.end method

.method public getFHasOldProps()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1737
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_77_fHasOldProps:B

    return v0
.end method

.method public getFNoAllowOverlap()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 747
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_22_fNoAllowOverlap:B

    return v0
.end method

.method public getFPropRMark()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1719
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_76_fPropRMark:B

    return v0
.end method

.method public getFRTL()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 729
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_21_fRTL:B

    return v0
.end method

.method public getFSpare()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 765
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_23_fSpare:B

    return v0
.end method

.method public getFTableHeader()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 477
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_7_fTableHeader:Z

    return v0
.end method

.method public getFtsCellPaddingDefaultBottom()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1215
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_48_ftsCellPaddingDefaultBottom:B

    return v0
.end method

.method public getFtsCellPaddingDefaultLeft()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1197
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_47_ftsCellPaddingDefaultLeft:B

    return v0
.end method

.method public getFtsCellPaddingDefaultRight()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1233
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_49_ftsCellPaddingDefaultRight:B

    return v0
.end method

.method public getFtsCellPaddingDefaultTop()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1179
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_46_ftsCellPaddingDefaultTop:B

    return v0
.end method

.method public getFtsCellPaddingOuterBottom()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1503
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_64_ftsCellPaddingOuterBottom:B

    return v0
.end method

.method public getFtsCellPaddingOuterLeft()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1485
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_63_ftsCellPaddingOuterLeft:B

    return v0
.end method

.method public getFtsCellPaddingOuterRight()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1521
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_65_ftsCellPaddingOuterRight:B

    return v0
.end method

.method public getFtsCellPaddingOuterTop()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1467
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_62_ftsCellPaddingOuterTop:B

    return v0
.end method

.method public getFtsCellSpacingDefaultBottom()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1359
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_56_ftsCellSpacingDefaultBottom:B

    return v0
.end method

.method public getFtsCellSpacingDefaultLeft()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1341
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_55_ftsCellSpacingDefaultLeft:B

    return v0
.end method

.method public getFtsCellSpacingDefaultRight()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1377
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_57_ftsCellSpacingDefaultRight:B

    return v0
.end method

.method public getFtsCellSpacingDefaultTop()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1323
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_54_ftsCellSpacingDefaultTop:B

    return v0
.end method

.method public getFtsCellSpacingOuterBottom()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1647
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_72_ftsCellSpacingOuterBottom:B

    return v0
.end method

.method public getFtsCellSpacingOuterLeft()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1629
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_71_ftsCellSpacingOuterLeft:B

    return v0
.end method

.method public getFtsCellSpacingOuterRight()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1665
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_73_ftsCellSpacingOuterRight:B

    return v0
.end method

.method public getFtsCellSpacingOuterTop()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1611
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_70_ftsCellSpacingOuterTop:B

    return v0
.end method

.method public getFtsWidth()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1878
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidth:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getFtsWidthAfter()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1938
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthAfter:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getFtsWidthBefore()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1918
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthBefore:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getFtsWidthIndent()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1898
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthIndent:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getGrpfTap()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 783
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_24_grpfTap:I

    return v0
.end method

.method public getGrpfTap_unused()S
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2198
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->grpfTap_unused:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getInternalFlags()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 801
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    return v0
.end method

.method public getIstd()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 369
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_1_istd:S

    return v0
.end method

.method public getItcMac()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 819
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_26_itcMac:S

    return v0
.end method

.method public getJc()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 387
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_2_jc:S

    return v0
.end method

.method public getPcHorz()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2058
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->pcHorz:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getPcVert()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2038
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->pcVert:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getPctWwd()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 909
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_31_pctWwd:S

    return v0
.end method

.method public getRgbrcInsideDefault_0()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1791
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_80_rgbrcInsideDefault_0:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getRgbrcInsideDefault_1()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1809
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_81_rgbrcInsideDefault_1:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getRgdxaCenter()[S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 945
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_33_rgdxaCenter:[S

    return-object v0
.end method

.method public getRgdxaCenterPrint()[S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 963
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_34_rgdxaCenterPrint:[S

    return-object v0
.end method

.method public getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1701
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_75_rgshd:[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    return-object v0
.end method

.method public getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1683
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_74_rgtc:[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    return-object v0
.end method

.method public getShdTable()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 981
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_35_shdTable:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    return-object v0
.end method

.method public getTlp()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 495
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_8_tlp:Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    return-object v0
.end method

.method public getViewFlags()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 927
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    return v0
.end method

.method public getViewFlags_unused2()S
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2318
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->viewFlags_unused2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getWCellPaddingDefaultBottom()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1143
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_44_wCellPaddingDefaultBottom:S

    return v0
.end method

.method public getWCellPaddingDefaultLeft()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1125
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_43_wCellPaddingDefaultLeft:S

    return v0
.end method

.method public getWCellPaddingDefaultRight()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1161
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_45_wCellPaddingDefaultRight:S

    return v0
.end method

.method public getWCellPaddingDefaultTop()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1107
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_42_wCellPaddingDefaultTop:S

    return v0
.end method

.method public getWCellPaddingOuterBottom()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1431
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_60_wCellPaddingOuterBottom:S

    return v0
.end method

.method public getWCellPaddingOuterLeft()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1413
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_59_wCellPaddingOuterLeft:S

    return v0
.end method

.method public getWCellPaddingOuterRight()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1449
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_61_wCellPaddingOuterRight:S

    return v0
.end method

.method public getWCellPaddingOuterTop()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1395
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_58_wCellPaddingOuterTop:S

    return v0
.end method

.method public getWCellSpacingDefaultBottom()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1287
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_52_wCellSpacingDefaultBottom:S

    return v0
.end method

.method public getWCellSpacingDefaultLeft()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1269
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_51_wCellSpacingDefaultLeft:S

    return v0
.end method

.method public getWCellSpacingDefaultRight()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1305
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_53_wCellSpacingDefaultRight:S

    return v0
.end method

.method public getWCellSpacingDefaultTop()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1251
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_50_wCellSpacingDefaultTop:S

    return v0
.end method

.method public getWCellSpacingOuterBottom()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1575
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_68_wCellSpacingOuterBottom:S

    return v0
.end method

.method public getWCellSpacingOuterLeft()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1557
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_67_wCellSpacingOuterLeft:S

    return v0
.end method

.method public getWCellSpacingOuterRight()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1593
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_69_wCellSpacingOuterRight:S

    return v0
.end method

.method public getWCellSpacingOuterTop()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1539
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_66_wCellSpacingOuterTop:S

    return v0
.end method

.method public getWWidth()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 513
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_9_wWidth:S

    return v0
.end method

.method public getWWidthAfter()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 567
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_12_wWidthAfter:S

    return v0
.end method

.method public getWWidthBefore()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 549
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_11_wWidthBefore:S

    return v0
.end method

.method public getWWidthIndent()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 531
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_10_wWidthIndent:S

    return v0
.end method

.method public getWidthAndFitsFlags()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 585
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    return v0
.end method

.method public getWidthAndFitsFlags_empty1()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1998
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->widthAndFitsFlags_empty1:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getWidthAndFitsFlags_empty2()S
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2078
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->widthAndFitsFlags_empty2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public isFAdjusted()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2298
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fAdjusted:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFAutofit()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1838
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fAutofit:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFCellSpacing()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2178
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fCellSpacing:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFirstRow()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2098
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fFirstRow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFInvalAutofit()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1978
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fInvalAutofit:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFKeepFollow()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1858
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fKeepFollow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLastRow()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2118
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fLastRow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNeverBeenAutofit()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1958
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fNeverBeenAutofit:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNotPageView()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2238
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fNotPageView:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOrigWordTableRules()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2158
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fOrigWordTableRules:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOutline()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2138
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVert()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2018
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fVert:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWebView()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2278
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fWebView:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWrapToWwd()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2218
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fWrapToWwd:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isViewFlags_unused1()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2258
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->viewFlags_unused1:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public setBrcBottom(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_36_brcBottom"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1008
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_36_brcBottom:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1009
    return-void
.end method

.method public setBrcHorizontal(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_41_brcHorizontal"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1098
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_41_brcHorizontal:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1099
    return-void
.end method

.method public setBrcLeft(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_38_brcLeft"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1044
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_38_brcLeft:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1045
    return-void
.end method

.method public setBrcRight(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_39_brcRight"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1062
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_39_brcRight:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1063
    return-void
.end method

.method public setBrcTop(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_37_brcTop"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1026
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_37_brcTop:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1027
    return-void
.end method

.method public setBrcVertical(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_40_brcVertical"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1080
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_40_brcVertical:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1081
    return-void
.end method

.method public setCHorzBands(S)V
    .locals 0
    .param p1, "field_78_cHorzBands"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1764
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_78_cHorzBands:S

    .line 1765
    return-void
.end method

.method public setCVertBands(S)V
    .locals 0
    .param p1, "field_79_cVertBands"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1782
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_79_cVertBands:S

    .line 1783
    return-void
.end method

.method public setDxaAbs(I)V
    .locals 0
    .param p1, "field_14_dxaAbs"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 612
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_14_dxaAbs:I

    .line 613
    return-void
.end method

.method public setDxaAdjust(I)V
    .locals 0
    .param p1, "field_27_dxaAdjust"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 846
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_27_dxaAdjust:I

    .line 847
    return-void
.end method

.method public setDxaColWidthWwd(I)V
    .locals 0
    .param p1, "field_30_dxaColWidthWwd"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 900
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_30_dxaColWidthWwd:I

    .line 901
    return-void
.end method

.method public setDxaFromText(I)V
    .locals 0
    .param p1, "field_16_dxaFromText"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 648
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_16_dxaFromText:I

    .line 649
    return-void
.end method

.method public setDxaFromTextRight(I)V
    .locals 0
    .param p1, "field_18_dxaFromTextRight"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 684
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_18_dxaFromTextRight:I

    .line 685
    return-void
.end method

.method public setDxaGapHalf(I)V
    .locals 0
    .param p1, "field_3_dxaGapHalf"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 414
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_3_dxaGapHalf:I

    .line 415
    return-void
.end method

.method public setDxaRTEWrapWidth(I)V
    .locals 0
    .param p1, "field_29_dxaRTEWrapWidth"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 882
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_29_dxaRTEWrapWidth:I

    .line 883
    return-void
.end method

.method public setDxaWebView(I)V
    .locals 0
    .param p1, "field_28_dxaWebView"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 864
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_28_dxaWebView:I

    .line 865
    return-void
.end method

.method public setDyaAbs(I)V
    .locals 0
    .param p1, "field_15_dyaAbs"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 630
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_15_dyaAbs:I

    .line 631
    return-void
.end method

.method public setDyaFromText(I)V
    .locals 0
    .param p1, "field_17_dyaFromText"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 666
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_17_dyaFromText:I

    .line 667
    return-void
.end method

.method public setDyaFromTextBottom(I)V
    .locals 0
    .param p1, "field_19_dyaFromTextBottom"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 702
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_19_dyaFromTextBottom:I

    .line 703
    return-void
.end method

.method public setDyaRowHeight(I)V
    .locals 0
    .param p1, "field_4_dyaRowHeight"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 432
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_4_dyaRowHeight:I

    .line 433
    return-void
.end method

.method public setFAdjusted(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2288
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fAdjusted:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 2289
    return-void
.end method

.method public setFAutofit(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1828
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fAutofit:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1829
    return-void
.end method

.method public setFBiDi(B)V
    .locals 0
    .param p1, "field_20_fBiDi"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 720
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_20_fBiDi:B

    .line 721
    return-void
.end method

.method public setFCantSplit(Z)V
    .locals 0
    .param p1, "field_5_fCantSplit"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 450
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_5_fCantSplit:Z

    .line 451
    return-void
.end method

.method public setFCantSplit90(Z)V
    .locals 0
    .param p1, "field_6_fCantSplit90"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 468
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_6_fCantSplit90:Z

    .line 469
    return-void
.end method

.method public setFCellSpacing(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2168
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fCellSpacing:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 2169
    return-void
.end method

.method public setFFirstRow(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2088
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fFirstRow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 2089
    return-void
.end method

.method public setFHasOldProps(B)V
    .locals 0
    .param p1, "field_77_fHasOldProps"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1746
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_77_fHasOldProps:B

    .line 1747
    return-void
.end method

.method public setFInvalAutofit(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1968
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fInvalAutofit:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1969
    return-void
.end method

.method public setFKeepFollow(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1848
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fKeepFollow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1849
    return-void
.end method

.method public setFLastRow(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2108
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fLastRow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 2109
    return-void
.end method

.method public setFNeverBeenAutofit(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1948
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fNeverBeenAutofit:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1949
    return-void
.end method

.method public setFNoAllowOverlap(B)V
    .locals 0
    .param p1, "field_22_fNoAllowOverlap"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 756
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_22_fNoAllowOverlap:B

    .line 757
    return-void
.end method

.method public setFNotPageView(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2228
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fNotPageView:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 2229
    return-void
.end method

.method public setFOrigWordTableRules(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2148
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fOrigWordTableRules:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 2149
    return-void
.end method

.method public setFOutline(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2128
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 2129
    return-void
.end method

.method public setFPropRMark(B)V
    .locals 0
    .param p1, "field_76_fPropRMark"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1728
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_76_fPropRMark:B

    .line 1729
    return-void
.end method

.method public setFRTL(B)V
    .locals 0
    .param p1, "field_21_fRTL"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 738
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_21_fRTL:B

    .line 739
    return-void
.end method

.method public setFSpare(B)V
    .locals 0
    .param p1, "field_23_fSpare"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 774
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_23_fSpare:B

    .line 775
    return-void
.end method

.method public setFTableHeader(Z)V
    .locals 0
    .param p1, "field_7_fTableHeader"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 486
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_7_fTableHeader:Z

    .line 487
    return-void
.end method

.method public setFVert(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2008
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fVert:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 2009
    return-void
.end method

.method public setFWebView(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2268
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fWebView:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 2269
    return-void
.end method

.method public setFWrapToWwd(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2208
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->fWrapToWwd:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 2209
    return-void
.end method

.method public setFtsCellPaddingDefaultBottom(B)V
    .locals 0
    .param p1, "field_48_ftsCellPaddingDefaultBottom"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1224
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_48_ftsCellPaddingDefaultBottom:B

    .line 1225
    return-void
.end method

.method public setFtsCellPaddingDefaultLeft(B)V
    .locals 0
    .param p1, "field_47_ftsCellPaddingDefaultLeft"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1206
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_47_ftsCellPaddingDefaultLeft:B

    .line 1207
    return-void
.end method

.method public setFtsCellPaddingDefaultRight(B)V
    .locals 0
    .param p1, "field_49_ftsCellPaddingDefaultRight"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1242
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_49_ftsCellPaddingDefaultRight:B

    .line 1243
    return-void
.end method

.method public setFtsCellPaddingDefaultTop(B)V
    .locals 0
    .param p1, "field_46_ftsCellPaddingDefaultTop"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1188
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_46_ftsCellPaddingDefaultTop:B

    .line 1189
    return-void
.end method

.method public setFtsCellPaddingOuterBottom(B)V
    .locals 0
    .param p1, "field_64_ftsCellPaddingOuterBottom"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1512
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_64_ftsCellPaddingOuterBottom:B

    .line 1513
    return-void
.end method

.method public setFtsCellPaddingOuterLeft(B)V
    .locals 0
    .param p1, "field_63_ftsCellPaddingOuterLeft"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1494
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_63_ftsCellPaddingOuterLeft:B

    .line 1495
    return-void
.end method

.method public setFtsCellPaddingOuterRight(B)V
    .locals 0
    .param p1, "field_65_ftsCellPaddingOuterRight"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1530
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_65_ftsCellPaddingOuterRight:B

    .line 1531
    return-void
.end method

.method public setFtsCellPaddingOuterTop(B)V
    .locals 0
    .param p1, "field_62_ftsCellPaddingOuterTop"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1476
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_62_ftsCellPaddingOuterTop:B

    .line 1477
    return-void
.end method

.method public setFtsCellSpacingDefaultBottom(B)V
    .locals 0
    .param p1, "field_56_ftsCellSpacingDefaultBottom"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1368
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_56_ftsCellSpacingDefaultBottom:B

    .line 1369
    return-void
.end method

.method public setFtsCellSpacingDefaultLeft(B)V
    .locals 0
    .param p1, "field_55_ftsCellSpacingDefaultLeft"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1350
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_55_ftsCellSpacingDefaultLeft:B

    .line 1351
    return-void
.end method

.method public setFtsCellSpacingDefaultRight(B)V
    .locals 0
    .param p1, "field_57_ftsCellSpacingDefaultRight"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1386
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_57_ftsCellSpacingDefaultRight:B

    .line 1387
    return-void
.end method

.method public setFtsCellSpacingDefaultTop(B)V
    .locals 0
    .param p1, "field_54_ftsCellSpacingDefaultTop"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1332
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_54_ftsCellSpacingDefaultTop:B

    .line 1333
    return-void
.end method

.method public setFtsCellSpacingOuterBottom(B)V
    .locals 0
    .param p1, "field_72_ftsCellSpacingOuterBottom"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1656
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_72_ftsCellSpacingOuterBottom:B

    .line 1657
    return-void
.end method

.method public setFtsCellSpacingOuterLeft(B)V
    .locals 0
    .param p1, "field_71_ftsCellSpacingOuterLeft"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1638
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_71_ftsCellSpacingOuterLeft:B

    .line 1639
    return-void
.end method

.method public setFtsCellSpacingOuterRight(B)V
    .locals 0
    .param p1, "field_73_ftsCellSpacingOuterRight"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1674
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_73_ftsCellSpacingOuterRight:B

    .line 1675
    return-void
.end method

.method public setFtsCellSpacingOuterTop(B)V
    .locals 0
    .param p1, "field_70_ftsCellSpacingOuterTop"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1620
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_70_ftsCellSpacingOuterTop:B

    .line 1621
    return-void
.end method

.method public setFtsWidth(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1868
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidth:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1869
    return-void
.end method

.method public setFtsWidthAfter(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1928
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthAfter:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1929
    return-void
.end method

.method public setFtsWidthBefore(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1908
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthBefore:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1909
    return-void
.end method

.method public setFtsWidthIndent(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1888
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->ftsWidthIndent:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1889
    return-void
.end method

.method public setGrpfTap(I)V
    .locals 0
    .param p1, "field_24_grpfTap"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 792
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_24_grpfTap:I

    .line 793
    return-void
.end method

.method public setGrpfTap_unused(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2188
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->grpfTap_unused:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 2189
    return-void
.end method

.method public setInternalFlags(I)V
    .locals 0
    .param p1, "field_25_internalFlags"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 810
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_25_internalFlags:I

    .line 811
    return-void
.end method

.method public setIstd(S)V
    .locals 0
    .param p1, "field_1_istd"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 378
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_1_istd:S

    .line 379
    return-void
.end method

.method public setItcMac(S)V
    .locals 0
    .param p1, "field_26_itcMac"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 828
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_26_itcMac:S

    .line 829
    return-void
.end method

.method public setJc(S)V
    .locals 0
    .param p1, "field_2_jc"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 396
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_2_jc:S

    .line 397
    return-void
.end method

.method public setPcHorz(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2048
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->pcHorz:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 2049
    return-void
.end method

.method public setPcVert(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2028
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->pcVert:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 2029
    return-void
.end method

.method public setPctWwd(S)V
    .locals 0
    .param p1, "field_31_pctWwd"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 918
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_31_pctWwd:S

    .line 919
    return-void
.end method

.method public setRgbrcInsideDefault_0(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_80_rgbrcInsideDefault_0"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1800
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_80_rgbrcInsideDefault_0:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1801
    return-void
.end method

.method public setRgbrcInsideDefault_1(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_81_rgbrcInsideDefault_1"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1818
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_81_rgbrcInsideDefault_1:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1819
    return-void
.end method

.method public setRgdxaCenter([S)V
    .locals 0
    .param p1, "field_33_rgdxaCenter"    # [S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 954
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_33_rgdxaCenter:[S

    .line 955
    return-void
.end method

.method public setRgdxaCenterPrint([S)V
    .locals 0
    .param p1, "field_34_rgdxaCenterPrint"    # [S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 972
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_34_rgdxaCenterPrint:[S

    .line 973
    return-void
.end method

.method public setRgshd([Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 0
    .param p1, "field_75_rgshd"    # [Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1710
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_75_rgshd:[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 1711
    return-void
.end method

.method public setRgtc([Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;)V
    .locals 0
    .param p1, "field_74_rgtc"    # [Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1692
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_74_rgtc:[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    .line 1693
    return-void
.end method

.method public setShdTable(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 0
    .param p1, "field_35_shdTable"    # Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 990
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_35_shdTable:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 991
    return-void
.end method

.method public setTlp(Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;)V
    .locals 0
    .param p1, "field_8_tlp"    # Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 504
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_8_tlp:Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    .line 505
    return-void
.end method

.method public setViewFlags(I)V
    .locals 0
    .param p1, "field_32_viewFlags"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 936
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 937
    return-void
.end method

.method public setViewFlags_unused1(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2248
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->viewFlags_unused1:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 2249
    return-void
.end method

.method public setViewFlags_unused2(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2308
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->viewFlags_unused2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_32_viewFlags:I

    .line 2309
    return-void
.end method

.method public setWCellPaddingDefaultBottom(S)V
    .locals 0
    .param p1, "field_44_wCellPaddingDefaultBottom"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1152
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_44_wCellPaddingDefaultBottom:S

    .line 1153
    return-void
.end method

.method public setWCellPaddingDefaultLeft(S)V
    .locals 0
    .param p1, "field_43_wCellPaddingDefaultLeft"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1134
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_43_wCellPaddingDefaultLeft:S

    .line 1135
    return-void
.end method

.method public setWCellPaddingDefaultRight(S)V
    .locals 0
    .param p1, "field_45_wCellPaddingDefaultRight"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1170
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_45_wCellPaddingDefaultRight:S

    .line 1171
    return-void
.end method

.method public setWCellPaddingDefaultTop(S)V
    .locals 0
    .param p1, "field_42_wCellPaddingDefaultTop"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1116
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_42_wCellPaddingDefaultTop:S

    .line 1117
    return-void
.end method

.method public setWCellPaddingOuterBottom(S)V
    .locals 0
    .param p1, "field_60_wCellPaddingOuterBottom"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1440
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_60_wCellPaddingOuterBottom:S

    .line 1441
    return-void
.end method

.method public setWCellPaddingOuterLeft(S)V
    .locals 0
    .param p1, "field_59_wCellPaddingOuterLeft"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1422
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_59_wCellPaddingOuterLeft:S

    .line 1423
    return-void
.end method

.method public setWCellPaddingOuterRight(S)V
    .locals 0
    .param p1, "field_61_wCellPaddingOuterRight"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1458
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_61_wCellPaddingOuterRight:S

    .line 1459
    return-void
.end method

.method public setWCellPaddingOuterTop(S)V
    .locals 0
    .param p1, "field_58_wCellPaddingOuterTop"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1404
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_58_wCellPaddingOuterTop:S

    .line 1405
    return-void
.end method

.method public setWCellSpacingDefaultBottom(S)V
    .locals 0
    .param p1, "field_52_wCellSpacingDefaultBottom"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1296
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_52_wCellSpacingDefaultBottom:S

    .line 1297
    return-void
.end method

.method public setWCellSpacingDefaultLeft(S)V
    .locals 0
    .param p1, "field_51_wCellSpacingDefaultLeft"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1278
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_51_wCellSpacingDefaultLeft:S

    .line 1279
    return-void
.end method

.method public setWCellSpacingDefaultRight(S)V
    .locals 0
    .param p1, "field_53_wCellSpacingDefaultRight"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1314
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_53_wCellSpacingDefaultRight:S

    .line 1315
    return-void
.end method

.method public setWCellSpacingDefaultTop(S)V
    .locals 0
    .param p1, "field_50_wCellSpacingDefaultTop"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1260
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_50_wCellSpacingDefaultTop:S

    .line 1261
    return-void
.end method

.method public setWCellSpacingOuterBottom(S)V
    .locals 0
    .param p1, "field_68_wCellSpacingOuterBottom"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1584
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_68_wCellSpacingOuterBottom:S

    .line 1585
    return-void
.end method

.method public setWCellSpacingOuterLeft(S)V
    .locals 0
    .param p1, "field_67_wCellSpacingOuterLeft"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1566
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_67_wCellSpacingOuterLeft:S

    .line 1567
    return-void
.end method

.method public setWCellSpacingOuterRight(S)V
    .locals 0
    .param p1, "field_69_wCellSpacingOuterRight"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1602
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_69_wCellSpacingOuterRight:S

    .line 1603
    return-void
.end method

.method public setWCellSpacingOuterTop(S)V
    .locals 0
    .param p1, "field_66_wCellSpacingOuterTop"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1548
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_66_wCellSpacingOuterTop:S

    .line 1549
    return-void
.end method

.method public setWWidth(S)V
    .locals 0
    .param p1, "field_9_wWidth"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 522
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_9_wWidth:S

    .line 523
    return-void
.end method

.method public setWWidthAfter(S)V
    .locals 0
    .param p1, "field_12_wWidthAfter"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 576
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_12_wWidthAfter:S

    .line 577
    return-void
.end method

.method public setWWidthBefore(S)V
    .locals 0
    .param p1, "field_11_wWidthBefore"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 558
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_11_wWidthBefore:S

    .line 559
    return-void
.end method

.method public setWWidthIndent(S)V
    .locals 0
    .param p1, "field_10_wWidthIndent"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 540
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_10_wWidthIndent:S

    .line 541
    return-void
.end method

.method public setWidthAndFitsFlags(I)V
    .locals 0
    .param p1, "field_13_widthAndFitsFlags"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 594
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 595
    return-void
.end method

.method public setWidthAndFitsFlags_empty1(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1988
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->widthAndFitsFlags_empty1:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 1989
    return-void
.end method

.method public setWidthAndFitsFlags_empty2(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2068
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->widthAndFitsFlags_empty2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->field_13_widthAndFitsFlags:I

    .line 2069
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[TAP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string/jumbo v1, "    .istd                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getIstd()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string/jumbo v1, "    .jc                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getJc()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v1, "    .dxaGapHalf           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaGapHalf()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v1, "    .dyaRowHeight         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDyaRowHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v1, "    .fCantSplit           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFCantSplit()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string/jumbo v1, "    .fCantSplit90         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFCantSplit90()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v1, "    .fTableHeader         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFTableHeader()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v1, "    .tlp                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getTlp()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v1, "    .wWidth               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWWidth()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v1, "    .wWidthIndent         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWWidthIndent()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v1, "    .wWidthBefore         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWWidthBefore()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string/jumbo v1, "    .wWidthAfter          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWWidthAfter()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string/jumbo v1, "    .widthAndFitsFlags    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWidthAndFitsFlags()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v1, "         .fAutofit                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFAutofit()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v1, "         .fKeepFollow              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFKeepFollow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v1, "         .ftsWidth                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsWidth()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 200
    const-string/jumbo v1, "         .ftsWidthIndent           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsWidthIndent()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v1, "         .ftsWidthBefore           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsWidthBefore()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v1, "         .ftsWidthAfter            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsWidthAfter()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v1, "         .fNeverBeenAutofit        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFNeverBeenAutofit()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v1, "         .fInvalAutofit            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFInvalAutofit()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v1, "         .widthAndFitsFlags_empty1     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWidthAndFitsFlags_empty1()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v1, "         .fVert                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFVert()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 207
    const-string/jumbo v1, "         .pcVert                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getPcVert()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 208
    const-string/jumbo v1, "         .pcHorz                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getPcHorz()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 209
    const-string/jumbo v1, "         .widthAndFitsFlags_empty2     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWidthAndFitsFlags_empty2()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    const-string/jumbo v1, "    .dxaAbs               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaAbs()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string/jumbo v1, "    .dyaAbs               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDyaAbs()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string/jumbo v1, "    .dxaFromText          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaFromText()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string/jumbo v1, "    .dyaFromText          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDyaFromText()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    const-string/jumbo v1, "    .dxaFromTextRight     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaFromTextRight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string/jumbo v1, "    .dyaFromTextBottom    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDyaFromTextBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    const-string/jumbo v1, "    .fBiDi                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFBiDi()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string/jumbo v1, "    .fRTL                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFRTL()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    const-string/jumbo v1, "    .fNoAllowOverlap      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFNoAllowOverlap()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    const-string/jumbo v1, "    .fSpare               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFSpare()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string/jumbo v1, "    .grpfTap              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getGrpfTap()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string/jumbo v1, "    .internalFlags        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getInternalFlags()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    const-string/jumbo v1, "         .fFirstRow                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFFirstRow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 235
    const-string/jumbo v1, "         .fLastRow                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFLastRow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 236
    const-string/jumbo v1, "         .fOutline                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFOutline()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 237
    const-string/jumbo v1, "         .fOrigWordTableRules      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFOrigWordTableRules()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 238
    const-string/jumbo v1, "         .fCellSpacing             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFCellSpacing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 239
    const-string/jumbo v1, "         .grpfTap_unused           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getGrpfTap_unused()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 240
    const-string/jumbo v1, "    .itcMac               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getItcMac()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const-string/jumbo v1, "    .dxaAdjust            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaAdjust()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string/jumbo v1, "    .dxaWebView           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaWebView()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string/jumbo v1, "    .dxaRTEWrapWidth      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaRTEWrapWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string/jumbo v1, "    .dxaColWidthWwd       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getDxaColWidthWwd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string/jumbo v1, "    .pctWwd               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getPctWwd()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string/jumbo v1, "    .viewFlags            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getViewFlags()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string/jumbo v1, "         .fWrapToWwd               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFWrapToWwd()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 255
    const-string/jumbo v1, "         .fNotPageView             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFNotPageView()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 256
    const-string/jumbo v1, "         .viewFlags_unused1        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isViewFlags_unused1()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 257
    const-string/jumbo v1, "         .fWebView                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFWebView()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 258
    const-string/jumbo v1, "         .fAdjusted                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->isFAdjusted()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 259
    const-string/jumbo v1, "         .viewFlags_unused2        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getViewFlags_unused2()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 260
    const-string/jumbo v1, "    .rgdxaCenter          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getRgdxaCenter()[S

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string/jumbo v1, "    .rgdxaCenterPrint     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getRgdxaCenterPrint()[S

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    const-string/jumbo v1, "    .shdTable             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getShdTable()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string/jumbo v1, "    .brcBottom            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string/jumbo v1, "    .brcTop               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string/jumbo v1, "    .brcLeft              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string/jumbo v1, "    .brcRight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string/jumbo v1, "    .brcVertical          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getBrcVertical()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string/jumbo v1, "    .brcHorizontal        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getBrcHorizontal()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    const-string/jumbo v1, "    .wCellPaddingDefaultTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingDefaultTop()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    const-string/jumbo v1, "    .wCellPaddingDefaultLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingDefaultLeft()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const-string/jumbo v1, "    .wCellPaddingDefaultBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingDefaultBottom()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string/jumbo v1, "    .wCellPaddingDefaultRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingDefaultRight()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const-string/jumbo v1, "    .ftsCellPaddingDefaultTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingDefaultTop()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    const-string/jumbo v1, "    .ftsCellPaddingDefaultLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingDefaultLeft()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    const-string/jumbo v1, "    .ftsCellPaddingDefaultBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingDefaultBottom()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    const-string/jumbo v1, "    .ftsCellPaddingDefaultRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingDefaultRight()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    const-string/jumbo v1, "    .wCellSpacingDefaultTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingDefaultTop()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    const-string/jumbo v1, "    .wCellSpacingDefaultLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingDefaultLeft()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string/jumbo v1, "    .wCellSpacingDefaultBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingDefaultBottom()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    const-string/jumbo v1, "    .wCellSpacingDefaultRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingDefaultRight()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    const-string/jumbo v1, "    .ftsCellSpacingDefaultTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingDefaultTop()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    const-string/jumbo v1, "    .ftsCellSpacingDefaultLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingDefaultLeft()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    const-string/jumbo v1, "    .ftsCellSpacingDefaultBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingDefaultBottom()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    const-string/jumbo v1, "    .ftsCellSpacingDefaultRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingDefaultRight()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string/jumbo v1, "    .wCellPaddingOuterTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingOuterTop()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string/jumbo v1, "    .wCellPaddingOuterLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingOuterLeft()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const-string/jumbo v1, "    .wCellPaddingOuterBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingOuterBottom()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string/jumbo v1, "    .wCellPaddingOuterRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellPaddingOuterRight()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    const-string/jumbo v1, "    .ftsCellPaddingOuterTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingOuterTop()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string/jumbo v1, "    .ftsCellPaddingOuterLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingOuterLeft()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    const-string/jumbo v1, "    .ftsCellPaddingOuterBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingOuterBottom()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string/jumbo v1, "    .ftsCellPaddingOuterRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellPaddingOuterRight()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    const-string/jumbo v1, "    .wCellSpacingOuterTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingOuterTop()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    const-string/jumbo v1, "    .wCellSpacingOuterLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingOuterLeft()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string/jumbo v1, "    .wCellSpacingOuterBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingOuterBottom()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const-string/jumbo v1, "    .wCellSpacingOuterRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getWCellSpacingOuterRight()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    const-string/jumbo v1, "    .ftsCellSpacingOuterTop = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingOuterTop()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    const-string/jumbo v1, "    .ftsCellSpacingOuterLeft = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingOuterLeft()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    const-string/jumbo v1, "    .ftsCellSpacingOuterBottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingOuterBottom()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    const-string/jumbo v1, "    .ftsCellSpacingOuterRight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFtsCellSpacingOuterRight()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    const-string/jumbo v1, "    .rgtc                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getRgtc()[Lorg/apache/poi/hwpf/usermodel/TableCellDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    const-string/jumbo v1, "    .rgshd                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getRgshd()[Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    const-string/jumbo v1, "    .fPropRMark           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFPropRMark()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    const-string/jumbo v1, "    .fHasOldProps         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getFHasOldProps()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    const-string/jumbo v1, "    .cHorzBands           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getCHorzBands()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    const-string/jumbo v1, "    .cVertBands           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getCVertBands()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    const-string/jumbo v1, "    .rgbrcInsideDefault_0 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getRgbrcInsideDefault_0()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    const-string/jumbo v1, "    .rgbrcInsideDefault_1 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TAPAbstractType;->getRgbrcInsideDefault_1()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    const-string/jumbo v1, "[/TAP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
