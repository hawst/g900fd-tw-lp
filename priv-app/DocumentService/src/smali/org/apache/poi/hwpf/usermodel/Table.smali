.class public final Lorg/apache/poi/hwpf/usermodel/Table;
.super Lorg/apache/poi/hwpf/usermodel/Range;
.source "Table.java"


# instance fields
.field private _rows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/TableRow;",
            ">;"
        }
    .end annotation
.end field

.field private _rowsFound:Z

.field private _tableLevel:I


# direct methods
.method constructor <init>(IILorg/apache/poi/hwpf/usermodel/Range;I)V
    .locals 1
    .param p1, "startIdxInclusive"    # I
    .param p2, "endIdxExclusive"    # I
    .param p3, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "levelNum"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rowsFound:Z

    .line 34
    iput p4, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_tableLevel:I

    .line 35
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Table;->initRows()V

    .line 36
    return-void
.end method

.method private initRows()V
    .locals 10

    .prologue
    .line 51
    iget-boolean v5, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rowsFound:Z

    if-eqz v5, :cond_0

    .line 73
    :goto_0
    return-void

    .line 54
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rows:Ljava/util/ArrayList;

    .line 55
    const/4 v3, 0x0

    .line 56
    .local v3, "rowStart":I
    const/4 v2, 0x0

    .line 58
    .local v2, "rowEnd":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Table;->numParagraphs()I

    move-result v1

    .line 59
    .local v1, "numParagraphs":I
    :cond_1
    :goto_1
    if-lt v2, v1, :cond_2

    .line 72
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rowsFound:Z

    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p0, v3}, Lorg/apache/poi/hwpf/usermodel/Table;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v4

    .line 62
    .local v4, "startRowP":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {p0, v2}, Lorg/apache/poi/hwpf/usermodel/Table;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v0

    .line 63
    .local v0, "endRowP":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    add-int/lit8 v2, v2, 0x1

    .line 64
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->isTableRowEnd()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 65
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getTableLevel()I

    move-result v5

    iget v6, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_tableLevel:I

    if-ne v5, v6, :cond_1

    .line 67
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rows:Ljava/util/ArrayList;

    new-instance v6, Lorg/apache/poi/hwpf/usermodel/TableRow;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getStartOffset()I

    move-result v7

    .line 68
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->getEndOffset()I

    move-result v8

    iget v9, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_tableLevel:I

    invoke-direct {v6, v7, v8, p0, v9}, Lorg/apache/poi/hwpf/usermodel/TableRow;-><init>(IILorg/apache/poi/hwpf/usermodel/Table;I)V

    .line 67
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    move v3, v2

    goto :goto_1
.end method


# virtual methods
.method public getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Table;->initRows()V

    .line 41
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/TableRow;

    return-object v0
.end method

.method public getTableLevel()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_tableLevel:I

    return v0
.end method

.method public numRows()I
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/poi/hwpf/usermodel/Table;->initRows()V

    .line 78
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/Table;->_rowsFound:Z

    .line 85
    return-void
.end method

.method public type()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x5

    return v0
.end method
