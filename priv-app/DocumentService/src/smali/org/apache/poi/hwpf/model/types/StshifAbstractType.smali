.class public abstract Lorg/apache/poi/hwpf/model/types/StshifAbstractType;
.super Ljava/lang/Object;
.source "StshifAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final fHasOriginalStyle:Lorg/apache/poi/util/BitField;

.field private static final fReserved:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_cstd:I

.field protected field_2_cbSTDBaseInFile:I

.field protected field_3_info3:I

.field protected field_4_stiMaxWhenSaved:I

.field protected field_5_istdMaxFixedWhenSaved:I

.field protected field_6_nVerBuiltInNamesWhenSaved:I

.field protected field_7_ftcAsci:S

.field protected field_8_ftcFE:S

.field protected field_9_ftcOther:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->fHasOriginalStyle:Lorg/apache/poi/util/BitField;

    .line 48
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0xfffe

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->fReserved:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 91
    const/16 v0, 0x12

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 62
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_1_cstd:I

    .line 63
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_2_cbSTDBaseInFile:I

    .line 64
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    .line 65
    add-int/lit8 v0, p2, 0x6

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_4_stiMaxWhenSaved:I

    .line 66
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_5_istdMaxFixedWhenSaved:I

    .line 67
    add-int/lit8 v0, p2, 0xa

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_6_nVerBuiltInNamesWhenSaved:I

    .line 68
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_7_ftcAsci:S

    .line 69
    add-int/lit8 v0, p2, 0xe

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_8_ftcFE:S

    .line 70
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_9_ftcOther:S

    .line 71
    return-void
.end method

.method public getCbSTDBaseInFile()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 147
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_2_cbSTDBaseInFile:I

    return v0
.end method

.method public getCstd()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 129
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_1_cstd:I

    return v0
.end method

.method public getFReserved()S
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->fReserved:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getFtcAsci()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 237
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_7_ftcAsci:S

    return v0
.end method

.method public getFtcFE()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 255
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_8_ftcFE:S

    return v0
.end method

.method public getFtcOther()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 273
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_9_ftcOther:S

    return v0
.end method

.method public getInfo3()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 165
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    return v0
.end method

.method public getIstdMaxFixedWhenSaved()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 201
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_5_istdMaxFixedWhenSaved:I

    return v0
.end method

.method public getNVerBuiltInNamesWhenSaved()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 219
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_6_nVerBuiltInNamesWhenSaved:I

    return v0
.end method

.method public getStiMaxWhenSaved()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 183
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_4_stiMaxWhenSaved:I

    return v0
.end method

.method public isFHasOriginalStyle()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 302
    sget-object v0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->fHasOriginalStyle:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 75
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_1_cstd:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 76
    add-int/lit8 v0, p2, 0x2

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_2_cbSTDBaseInFile:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 77
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 78
    add-int/lit8 v0, p2, 0x6

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_4_stiMaxWhenSaved:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 79
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_5_istdMaxFixedWhenSaved:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 80
    add-int/lit8 v0, p2, 0xa

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_6_nVerBuiltInNamesWhenSaved:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 81
    add-int/lit8 v0, p2, 0xc

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_7_ftcAsci:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 82
    add-int/lit8 v0, p2, 0xe

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_8_ftcFE:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    add-int/lit8 v0, p2, 0x10

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_9_ftcOther:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 84
    return-void
.end method

.method public setCbSTDBaseInFile(I)V
    .locals 0
    .param p1, "field_2_cbSTDBaseInFile"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 156
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_2_cbSTDBaseInFile:I

    .line 157
    return-void
.end method

.method public setCstd(I)V
    .locals 0
    .param p1, "field_1_cstd"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 138
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_1_cstd:I

    .line 139
    return-void
.end method

.method public setFHasOriginalStyle(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 292
    sget-object v0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->fHasOriginalStyle:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    .line 293
    return-void
.end method

.method public setFReserved(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 312
    sget-object v0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->fReserved:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    .line 313
    return-void
.end method

.method public setFtcAsci(S)V
    .locals 0
    .param p1, "field_7_ftcAsci"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 246
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_7_ftcAsci:S

    .line 247
    return-void
.end method

.method public setFtcFE(S)V
    .locals 0
    .param p1, "field_8_ftcFE"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 264
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_8_ftcFE:S

    .line 265
    return-void
.end method

.method public setFtcOther(S)V
    .locals 0
    .param p1, "field_9_ftcOther"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 282
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_9_ftcOther:S

    .line 283
    return-void
.end method

.method public setInfo3(I)V
    .locals 0
    .param p1, "field_3_info3"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 174
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_3_info3:I

    .line 175
    return-void
.end method

.method public setIstdMaxFixedWhenSaved(I)V
    .locals 0
    .param p1, "field_5_istdMaxFixedWhenSaved"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 210
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_5_istdMaxFixedWhenSaved:I

    .line 211
    return-void
.end method

.method public setNVerBuiltInNamesWhenSaved(I)V
    .locals 0
    .param p1, "field_6_nVerBuiltInNamesWhenSaved"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 228
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_6_nVerBuiltInNamesWhenSaved:I

    .line 229
    return-void
.end method

.method public setStiMaxWhenSaved(I)V
    .locals 0
    .param p1, "field_4_stiMaxWhenSaved"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 192
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->field_4_stiMaxWhenSaved:I

    .line 193
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[Stshif]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string/jumbo v1, "    .cstd                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getCstd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    const-string/jumbo v1, "    .cbSTDBaseInFile      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getCbSTDBaseInFile()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    const-string/jumbo v1, "    .info3                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getInfo3()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string/jumbo v1, "         .fHasOriginalStyle        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->isFHasOriginalStyle()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105
    const-string/jumbo v1, "         .fReserved                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getFReserved()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 106
    const-string/jumbo v1, "    .stiMaxWhenSaved      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getStiMaxWhenSaved()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string/jumbo v1, "    .istdMaxFixedWhenSaved = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getIstdMaxFixedWhenSaved()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string/jumbo v1, "    .nVerBuiltInNamesWhenSaved = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getNVerBuiltInNamesWhenSaved()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v1, "    .ftcAsci              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getFtcAsci()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v1, "    .ftcFE                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getFtcFE()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string/jumbo v1, "    .ftcOther             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/StshifAbstractType;->getFtcOther()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string/jumbo v1, "[/Stshif]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
