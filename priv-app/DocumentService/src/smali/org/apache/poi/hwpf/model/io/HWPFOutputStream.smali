.class public final Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
.super Ljava/io/ByteArrayOutputStream;
.source "HWPFOutputStream.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field _offset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public getOffset()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->_offset:I

    return v0
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->_offset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write(I)V
    .locals 1
    .param p1, "b"    # I

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 55
    iget v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->_offset:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->_offset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 49
    iget v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->_offset:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->_offset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    monitor-exit p0

    return-void

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
