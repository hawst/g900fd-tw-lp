.class public final Lorg/apache/poi/hwpf/model/SEPX;
.super Lorg/apache/poi/hwpf/model/PropertyNode;
.source "SEPX.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/poi/hwpf/model/PropertyNode",
        "<",
        "Lorg/apache/poi/hwpf/model/SEPX;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field _sed:Lorg/apache/poi/hwpf/model/SectionDescriptor;

.field sectionProperties:Lorg/apache/poi/hwpf/usermodel/SectionProperties;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/SectionDescriptor;II[B)V
    .locals 2
    .param p1, "sed"    # Lorg/apache/poi/hwpf/model/SectionDescriptor;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "grpprl"    # [B

    .prologue
    .line 36
    new-instance v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v1, 0x0

    invoke-direct {v0, p4, v1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p2, p3, v0}, Lorg/apache/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 37
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/SEPX;->_sed:Lorg/apache/poi/hwpf/model/SectionDescriptor;

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 70
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hwpf/model/SEPX;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 71
    check-cast v0, Lorg/apache/poi/hwpf/model/SEPX;

    .line 72
    .local v0, "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    invoke-super {p0, p1}, Lorg/apache/poi/hwpf/model/PropertyNode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    iget-object v1, v0, Lorg/apache/poi/hwpf/model/SEPX;->_sed:Lorg/apache/poi/hwpf/model/SectionDescriptor;

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/SEPX;->_sed:Lorg/apache/poi/hwpf/model/SectionDescriptor;

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/model/SectionDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 78
    .end local v0    # "sepx":Lorg/apache/poi/hwpf/model/SEPX;
    :cond_0
    return v1
.end method

.method public getGrpprl()[B
    .locals 3

    .prologue
    .line 42
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/SEPX;->sectionProperties:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/SEPX;->sectionProperties:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-static {v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmCompressor;->compressSectionProperty(Lorg/apache/poi/hwpf/usermodel/SectionProperties;)[B

    move-result-object v0

    .line 46
    .local v0, "grpprl":[B
    new-instance v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hwpf/model/SEPX;->_buf:Ljava/lang/Object;

    .line 49
    .end local v0    # "grpprl":[B
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/SEPX;->_buf:Ljava/lang/Object;

    check-cast v1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getSectionDescriptor()Lorg/apache/poi/hwpf/model/SectionDescriptor;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/SEPX;->_sed:Lorg/apache/poi/hwpf/model/SectionDescriptor;

    return-object v0
.end method

.method public getSectionProperties()Lorg/apache/poi/hwpf/usermodel/SectionProperties;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/SEPX;->sectionProperties:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/SEPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    const/4 v1, 0x0

    .line 61
    invoke-static {v0, v1}, Lorg/apache/poi/hwpf/sprm/SectionSprmUncompressor;->uncompressSEP([BI)Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/SEPX;->sectionProperties:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    .line 64
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/SEPX;->sectionProperties:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "SEPX from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/SEPX;->getStart()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
