.class public Lorg/apache/poi/hwpf/model/CHPBinTable;
.super Ljava/lang/Object;
.source "CHPBinTable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected _textRuns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/poi/hwpf/model/CHPBinTable;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 52
    sput-object v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    .line 53
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 60
    return-void
.end method

.method public constructor <init>([B[BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V
    .locals 6
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "size"    # I
    .param p5, "fcMin"    # I
    .param p6, "tpt"    # Lorg/apache/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 72
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hwpf/model/CHPBinTable;-><init>([B[BIILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 73
    return-void
.end method

.method public constructor <init>([B[BIILorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 21
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "size"    # I
    .param p5, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;

    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 90
    .local v18, "start":J
    new-instance v11, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v4, 0x4

    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v11, v0, v1, v2, v4}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 92
    .local v11, "bte":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v14

    .line 93
    .local v14, "length":I
    const/16 v20, 0x0

    .local v20, "x":I
    :goto_0
    move/from16 v0, v20

    if-lt v0, v14, :cond_1

    .line 109
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "CHPX FKPs loaded in "

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v18

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms ("

    .line 111
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string/jumbo v10, " elements)"

    .line 109
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 113
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 115
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x5

    const-string/jumbo v6, "CHPX FKPs are empty"

    invoke-virtual {v4, v5, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 116
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    new-instance v5, Lorg/apache/poi/hwpf/model/CHPX;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    invoke-direct {v5, v6, v7, v8}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_0
    return-void

    .line 95
    :cond_1
    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v15

    .line 97
    .local v15, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v15}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v16

    .line 98
    .local v16, "pageNum":I
    move/from16 v0, v16

    mul-int/lit16 v0, v0, 0x200

    move/from16 v17, v0

    .line 100
    .local v17, "pageOffset":I
    new-instance v12, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;

    move-object/from16 v0, p1

    move/from16 v1, v17

    move-object/from16 v2, p5

    invoke-direct {v12, v0, v1, v2}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;-><init>([BILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 103
    .local v12, "cfkp":Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;
    invoke-virtual {v12}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->getCHPXs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 93
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 103
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/CHPX;

    .line 105
    .local v13, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    if-eqz v13, :cond_2

    .line 106
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static binarySearch(Ljava/util/List;I)I
    .locals 6
    .param p1, "startPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 340
    .local p0, "chpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    const/4 v1, 0x0

    .line 341
    .local v1, "low":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 343
    .local v0, "high":I
    :goto_0
    if-le v1, v0, :cond_1

    .line 356
    add-int/lit8 v5, v1, 0x1

    neg-int v2, v5

    :cond_0
    return v2

    .line 345
    :cond_1
    add-int v5, v1, v0

    ushr-int/lit8 v2, v5, 0x1

    .line 346
    .local v2, "mid":I
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/CHPX;

    .line 347
    .local v3, "midVal":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v4

    .line 349
    .local v4, "midValue":I
    if-ge v4, p1, :cond_2

    .line 350
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 351
    :cond_2
    if-le v4, p1, :cond_0

    .line 352
    add-int/lit8 v0, v2, -0x1

    goto :goto_0
.end method


# virtual methods
.method public adjustForDelete(III)V
    .locals 6
    .param p1, "listIndex"    # I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 361
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 362
    .local v3, "size":I
    add-int v2, p2, p3

    .line 363
    .local v2, "endMark":I
    move v1, p1

    .line 365
    .local v1, "endIndex":I
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 366
    .local v0, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    :goto_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    if-lt v5, v2, :cond_0

    .line 370
    if-ne p1, v1, :cond_1

    .line 372
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 373
    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v2

    add-int/2addr v5, p2

    invoke-virtual {v0, v5}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 389
    :goto_1
    add-int/lit8 v4, v1, 0x1

    .local v4, "x":I
    :goto_2
    if-lt v4, v3, :cond_3

    .line 395
    return-void

    .line 368
    .end local v4    # "x":I
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    goto :goto_0

    .line 377
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 378
    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 379
    add-int/lit8 v4, p1, 0x1

    .restart local v4    # "x":I
    :goto_3
    if-lt v4, v1, :cond_2

    .line 385
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 386
    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v2

    add-int/2addr v5, p2

    invoke-virtual {v0, v5}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    goto :goto_1

    .line 381
    :cond_2
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 382
    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setStart(I)V

    .line 383
    invoke-virtual {v0, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 379
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 391
    :cond_3
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 392
    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v0, v5}, Lorg/apache/poi/hwpf/model/CHPX;->setStart(I)V

    .line 393
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v0, v5}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 389
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public adjustForInsert(II)V
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 439
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 440
    .local v1, "size":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 441
    .local v0, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 443
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 449
    return-void

    .line 445
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 446
    .restart local v0    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/CHPX;->setStart(I)V

    .line 447
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 443
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getTextRuns()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    return-object v0
.end method

.method public insert(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V
    .locals 5
    .param p1, "listIndex"    # I
    .param p2, "cpStart"    # I
    .param p3, "buf"    # Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .prologue
    const/4 v4, 0x0

    .line 400
    new-instance v2, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-direct {v2, v4, v4, p3}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 403
    .local v2, "insertChpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v2, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setStart(I)V

    .line 404
    invoke-virtual {v2, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 406
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne p1, v3, :cond_0

    .line 408
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    :goto_0
    return-void

    .line 412
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/CHPX;

    .line 413
    .local v0, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v3

    if-ge v3, p2, :cond_1

    .line 420
    new-instance v1, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getSprmBuf()Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v3

    invoke-direct {v1, v4, v4, v3}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 422
    .local v1, "clone":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v1, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setStart(I)V

    .line 423
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 425
    invoke-virtual {v0, p2}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 427
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 428
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x2

    invoke-virtual {v3, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 432
    .end local v1    # "clone":Lorg/apache/poi/hwpf/model/CHPX;
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, p1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public rebuild(Lorg/apache/poi/hwpf/model/ComplexFileTable;)V
    .locals 48
    .param p1, "complexFileTable"    # Lorg/apache/poi/hwpf/model/ComplexFileTable;

    .prologue
    .line 122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 124
    .local v40, "start":J
    if-eqz p1, :cond_1

    .line 126
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getGrpprls()[Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    move-result-object v38

    .line 129
    .local v38, "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v4

    .line 130
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 129
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 175
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    .line 176
    const-string/jumbo v6, "Merged with CHPX from complex file table in "

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v40

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 178
    const-string/jumbo v8, " ms ("

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 179
    const-string/jumbo v10, " elements in total)"

    .line 175
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 183
    .end local v38    # "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_1
    new-instance v33, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 185
    .local v33, "oldChpxSortedByStartPos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    sget-object v4, Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$StartComparator;

    .line 184
    move-object/from16 v0, v33

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 187
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "CHPX sorted by start position in "

    .line 188
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v40

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms"

    .line 187
    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 191
    new-instance v15, Ljava/util/IdentityHashMap;

    invoke-direct {v15}, Ljava/util/IdentityHashMap;-><init>()V

    .line 193
    .local v15, "chpxToFileOrder":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/hwpf/model/CHPX;Ljava/lang/Integer;>;"
    const/16 v17, 0x0

    .line 194
    .local v17, "counter":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_7

    .line 199
    new-instance v14, Lorg/apache/poi/hwpf/model/CHPBinTable$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lorg/apache/poi/hwpf/model/CHPBinTable$1;-><init>(Lorg/apache/poi/hwpf/model/CHPBinTable;Ljava/util/Map;)V

    .line 209
    .local v14, "chpxFileOrderComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/poi/hwpf/model/CHPX;>;"
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "CHPX\'s order map created in "

    .line 210
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v40

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms"

    .line 209
    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 211
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 215
    new-instance v46, Ljava/util/HashSet;

    invoke-direct/range {v46 .. v46}, Ljava/util/HashSet;-><init>()V

    .line 216
    .local v46, "textRunsBoundariesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_8

    .line 221
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v46

    invoke-interface {v0, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 222
    new-instance v45, Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v46}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 224
    .local v45, "textRunsBoundariesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static/range {v45 .. v45}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 227
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "Texts CHPX boundaries collected in "

    .line 228
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v40

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms"

    .line 227
    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 229
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 231
    new-instance v30, Ljava/util/LinkedList;

    invoke-direct/range {v30 .. v30}, Ljava/util/LinkedList;-><init>()V

    .line 232
    .local v30, "newChpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    const/16 v27, 0x0

    .line 233
    .local v27, "lastTextRunStart":I
    invoke-interface/range {v45 .. v45}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v47

    :goto_3
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_9

    .line 303
    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, v30

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 305
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "CHPX rebuilded in "

    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v40

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms ("

    .line 307
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string/jumbo v10, " elements)"

    .line 305
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 310
    const/16 v34, 0x0

    .line 311
    .local v34, "previous":Lorg/apache/poi/hwpf/model/CHPX;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .line 312
    .local v26, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hwpf/model/CHPX;>;"
    :goto_4
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_13

    .line 333
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x1

    const-string/jumbo v6, "CHPX compacted in "

    .line 334
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v40

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string/jumbo v8, " ms ("

    .line 335
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string/jumbo v10, " elements)"

    .line 333
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 336
    return-void

    .line 130
    .end local v14    # "chpxFileOrderComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .end local v15    # "chpxToFileOrder":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/hwpf/model/CHPX;Ljava/lang/Integer;>;"
    .end local v17    # "counter":I
    .end local v26    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .end local v27    # "lastTextRunStart":I
    .end local v30    # "newChpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .end local v33    # "oldChpxSortedByStartPos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .end local v34    # "previous":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v45    # "textRunsBoundariesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v46    # "textRunsBoundariesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v38    # "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 132
    .local v44, "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual/range {v44 .. v44}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getPrm()Lorg/apache/poi/hwpf/model/PropertyModifier;

    move-result-object v35

    .line 133
    .local v35, "prm":Lorg/apache/poi/hwpf/model/PropertyModifier;
    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 135
    invoke-virtual/range {v35 .. v35}, Lorg/apache/poi/hwpf/model/PropertyModifier;->getIgrpprl()S

    move-result v24

    .line 137
    .local v24, "igrpprl":I
    if-ltz v24, :cond_3

    move-object/from16 v0, v38

    array-length v5, v0

    move/from16 v0, v24

    if-lt v0, v5, :cond_4

    .line 139
    :cond_3
    sget-object v5, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v6, 0x5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v44

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 140
    const-string/jumbo v8, "\'s PRM references to unknown grpprl"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 139
    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 144
    :cond_4
    const/16 v23, 0x0

    .line 145
    .local v23, "hasChp":Z
    aget-object v37, v38, v24

    .line 146
    .local v37, "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->iterator()Lorg/apache/poi/hwpf/sprm/SprmIterator;

    move-result-object v25

    .line 147
    .local v25, "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_5
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_6

    .line 157
    :goto_5
    if-eqz v23, :cond_0

    .line 162
    :try_start_0
    invoke-virtual/range {v37 .. v37}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .local v31, "newSprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    new-instance v13, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-virtual/range {v44 .. v44}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v5

    .line 171
    invoke-virtual/range {v44 .. v44}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v6

    .line 170
    move-object/from16 v0, v31

    invoke-direct {v13, v5, v6, v0}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 172
    .local v13, "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 149
    .end local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v31    # "newSprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :cond_6
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v39

    .line 150
    .local v39, "sprmOperation":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual/range {v39 .. v39}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 152
    const/16 v23, 0x1

    .line 153
    goto :goto_5

    .line 164
    .end local v39    # "sprmOperation":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    :catch_0
    move-exception v20

    .line 167
    .local v20, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v4, Ljava/lang/Error;

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 194
    .end local v20    # "e":Ljava/lang/CloneNotSupportedException;
    .end local v23    # "hasChp":Z
    .end local v24    # "igrpprl":I
    .end local v25    # "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .end local v35    # "prm":Lorg/apache/poi/hwpf/model/PropertyModifier;
    .end local v37    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v38    # "sprmBuffers":[Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v44    # "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    .restart local v15    # "chpxToFileOrder":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/hwpf/model/CHPX;Ljava/lang/Integer;>;"
    .restart local v17    # "counter":I
    .restart local v33    # "oldChpxSortedByStartPos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/CHPX;

    .line 196
    .restart local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    add-int/lit8 v18, v17, 0x1

    .end local v17    # "counter":I
    .local v18, "counter":I
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v15, v13, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v17, v18

    .end local v18    # "counter":I
    .restart local v17    # "counter":I
    goto/16 :goto_1

    .line 216
    .end local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .restart local v14    # "chpxFileOrderComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .restart local v46    # "textRunsBoundariesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/CHPX;

    .line 218
    .restart local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v46

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v46

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 233
    .end local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .restart local v27    # "lastTextRunStart":I
    .restart local v30    # "newChpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .restart local v45    # "textRunsBoundariesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_9
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/Integer;

    .line 235
    .local v32, "objBoundary":Ljava/lang/Integer;
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 237
    .local v11, "boundary":I
    move/from16 v42, v27

    .line 238
    .local v42, "startInclusive":I
    move/from16 v21, v11

    .line 239
    .local v21, "endExclusive":I
    move/from16 v27, v21

    .line 241
    move-object/from16 v0, v33

    invoke-static {v0, v11}, Lorg/apache/poi/hwpf/model/CHPBinTable;->binarySearch(Ljava/util/List;I)I

    move-result v43

    .line 242
    .local v43, "startPosition":I
    invoke-static/range {v43 .. v43}, Ljava/lang/Math;->abs(I)I

    move-result v43

    .line 243
    :goto_6
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v43

    if-ge v0, v4, :cond_c

    .line 245
    :goto_7
    if-lez v43, :cond_a

    .line 246
    move-object/from16 v0, v33

    move/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v4

    .line 245
    if-ge v4, v11, :cond_d

    .line 249
    :cond_a
    new-instance v16, Ljava/util/LinkedList;

    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedList;-><init>()V

    .line 250
    .local v16, "chpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    move/from16 v12, v43

    .local v12, "c":I
    :goto_8
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v4

    if-lt v12, v4, :cond_e

    .line 266
    :cond_b
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_10

    .line 268
    sget-object v4, Lorg/apache/poi/hwpf/model/CHPBinTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x5

    const-string/jumbo v6, "Text piece ["

    .line 269
    invoke-static/range {v42 .. v42}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string/jumbo v8, "; "

    .line 270
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 271
    const-string/jumbo v10, ") has no CHPX. Creating new one."

    .line 268
    invoke-virtual/range {v4 .. v10}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 273
    new-instance v13, Lorg/apache/poi/hwpf/model/CHPX;

    .line 274
    new-instance v4, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 273
    move/from16 v0, v42

    move/from16 v1, v21

    invoke-direct {v13, v0, v1, v4}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 275
    .restart local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    move-object/from16 v0, v30

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 244
    .end local v12    # "c":I
    .end local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v16    # "chpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    :cond_c
    add-int/lit8 v43, v43, -0x1

    goto :goto_6

    .line 247
    :cond_d
    add-int/lit8 v43, v43, -0x1

    goto :goto_7

    .line 252
    .restart local v12    # "c":I
    .restart local v16    # "chpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    :cond_e
    move-object/from16 v0, v33

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/CHPX;

    .line 254
    .restart local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v4

    if-lt v11, v4, :cond_b

    .line 257
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v4

    move/from16 v0, v42

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v28

    .line 258
    .local v28, "left":I
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v4

    move/from16 v0, v21

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v36

    .line 260
    .local v36, "right":I
    move/from16 v0, v28

    move/from16 v1, v36

    if-ge v0, v1, :cond_f

    .line 262
    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    :cond_f
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    .line 279
    .end local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v28    # "left":I
    .end local v36    # "right":I
    :cond_10
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_11

    .line 282
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/poi/hwpf/model/CHPX;

    .line 283
    .local v22, "existing":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v4

    move/from16 v0, v42

    if-ne v4, v0, :cond_11

    .line 284
    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v4

    move/from16 v0, v21

    if-ne v4, v0, :cond_11

    .line 286
    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 291
    .end local v22    # "existing":Lorg/apache/poi/hwpf/model/CHPX;
    :cond_11
    move-object/from16 v0, v16

    invoke-static {v0, v14}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 293
    new-instance v37, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-direct {v0, v4}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 294
    .restart local v37    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_12

    .line 298
    new-instance v29, Lorg/apache/poi/hwpf/model/CHPX;

    move-object/from16 v0, v29

    move/from16 v1, v42

    move/from16 v2, v21

    move-object/from16 v3, v37

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/CHPX;-><init>(IILorg/apache/poi/hwpf/sprm/SprmBuffer;)V

    .line 299
    .local v29, "newChpx":Lorg/apache/poi/hwpf/model/CHPX;
    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 294
    .end local v29    # "newChpx":Lorg/apache/poi/hwpf/model/CHPX;
    :cond_12
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hwpf/model/CHPX;

    .line 296
    .restart local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v13}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v6}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->append([BI)V

    goto :goto_9

    .line 314
    .end local v11    # "boundary":I
    .end local v12    # "c":I
    .end local v13    # "chpx":Lorg/apache/poi/hwpf/model/CHPX;
    .end local v16    # "chpxs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .end local v21    # "endExclusive":I
    .end local v32    # "objBoundary":Ljava/lang/Integer;
    .end local v37    # "sprmBuffer":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    .end local v42    # "startInclusive":I
    .end local v43    # "startPosition":I
    .restart local v26    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hwpf/model/CHPX;>;"
    .restart local v34    # "previous":Lorg/apache/poi/hwpf/model/CHPX;
    :cond_13
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/poi/hwpf/model/CHPX;

    .line 315
    .local v19, "current":Lorg/apache/poi/hwpf/model/CHPX;
    if-nez v34, :cond_14

    .line 317
    move-object/from16 v34, v19

    .line 318
    goto/16 :goto_4

    .line 321
    :cond_14
    invoke-virtual/range {v34 .. v34}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v5

    if-ne v4, v5, :cond_15

    .line 323
    invoke-virtual/range {v34 .. v34}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v5

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 325
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lorg/apache/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 326
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_4

    .line 330
    :cond_15
    move-object/from16 v34, v19

    goto/16 :goto_4
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;ILorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 3
    .param p1, "sys"    # Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;
    .param p2, "fcMin"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 460
    const-string/jumbo v2, "WordDocument"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    .line 461
    .local v0, "docStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    const-string/jumbo v2, "1Table"

    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;

    move-result-object v1

    .line 463
    .local v1, "tableStream":Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/apache/poi/hwpf/model/CHPBinTable;->writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;ILorg/apache/poi/hwpf/model/CharIndexTranslator;)V

    .line 464
    return-void
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;ILorg/apache/poi/hwpf/model/CharIndexTranslator;)V
    .locals 17
    .param p1, "wordDocumentStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p2, "tableStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .param p3, "fcMin"    # I
    .param p4, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 479
    new-instance v1, Lorg/apache/poi/hwpf/model/PlexOfCps;

    const/4 v15, 0x4

    invoke-direct {v1, v15}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    .line 482
    .local v1, "bte":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v4

    .line 483
    .local v4, "docOffset":I
    rem-int/lit16 v8, v4, 0x200

    .line 484
    .local v8, "mod":I
    if-eqz v8, :cond_0

    .line 486
    rsub-int v15, v8, 0x200

    new-array v10, v15, [B

    .line 487
    .local v10, "padding":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 491
    .end local v10    # "padding":[B
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v4

    .line 492
    div-int/lit16 v11, v4, 0x200

    .line 498
    .local v11, "pageNum":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 499
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    .line 498
    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/hwpf/model/CHPX;

    .line 499
    invoke-virtual {v15}, Lorg/apache/poi/hwpf/model/CHPX;->getEnd()I

    move-result v15

    .line 498
    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v6

    .line 501
    .local v6, "endingFc":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 504
    .local v9, "overflow":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hwpf/model/CHPX;>;"
    :goto_0
    const/4 v15, 0x0

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/poi/hwpf/model/CHPX;

    .line 506
    .local v14, "startingProp":Lorg/apache/poi/hwpf/model/CHPX;
    invoke-virtual {v14}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v15

    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v13

    .line 508
    .local v13, "start":I
    new-instance v3, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;-><init>()V

    .line 509
    .local v3, "cfkp":Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;
    invoke-virtual {v3, v9}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->fill(Ljava/util/List;)V

    .line 511
    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->toByteArray(Lorg/apache/poi/hwpf/model/CharIndexTranslator;)[B

    move-result-object v2

    .line 512
    .local v2, "bufFkp":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 513
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/CHPFormattedDiskPage;->getOverflow()Ljava/util/ArrayList;

    move-result-object v9

    .line 515
    move v5, v6

    .line 516
    .local v5, "end":I
    if-eqz v9, :cond_1

    .line 519
    const/4 v15, 0x0

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/hwpf/model/CHPX;

    invoke-virtual {v15}, Lorg/apache/poi/hwpf/model/CHPX;->getStart()I

    move-result v15

    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v5

    .line 522
    :cond_1
    const/4 v15, 0x4

    new-array v7, v15, [B

    .line 523
    .local v7, "intHolder":[B
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "pageNum":I
    .local v12, "pageNum":I
    invoke-static {v7, v11}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 524
    new-instance v15, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    invoke-direct {v15, v13, v5, v7}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    invoke-virtual {v1, v15}, Lorg/apache/poi/hwpf/model/PlexOfCps;->addProperty(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 527
    if-nez v9, :cond_2

    .line 528
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 529
    return-void

    :cond_2
    move v11, v12

    .end local v12    # "pageNum":I
    .restart local v11    # "pageNum":I
    goto :goto_0
.end method
