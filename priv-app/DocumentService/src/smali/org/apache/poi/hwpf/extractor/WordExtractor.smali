.class public final Lorg/apache/poi/hwpf/extractor/WordExtractor;
.super Lorg/apache/poi/POIOLE2TextExtractor;
.source "WordExtractor.java"


# instance fields
.field private doc:Lorg/apache/poi/hwpf/HWPFDocument;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/HWPFDocument;)V
    .locals 0
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lorg/apache/poi/POIOLE2TextExtractor;-><init>(Lorg/apache/poi/POIDocument;)V

    .line 91
    iput-object p1, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 92
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;)V

    .line 64
    return-void
.end method

.method private appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "out"    # Ljava/lang/StringBuffer;

    .prologue
    const/16 v1, 0xa

    .line 192
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 196
    const-string/jumbo v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 199
    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 202
    :cond_2
    const-string/jumbo v0, "\n\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 207
    :cond_3
    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method protected static getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;
    .locals 5
    .param p0, "r"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 172
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->numParagraphs()I

    move-result v3

    new-array v2, v3, [Ljava/lang/String;

    .line 173
    .local v2, "ret":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_0

    .line 184
    return-object v2

    .line 175
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hwpf/usermodel/Range;->getParagraph(I)Lorg/apache/poi/hwpf/usermodel/Paragraph;

    move-result-object v1

    .line 176
    .local v1, "p":Lorg/apache/poi/hwpf/usermodel/Paragraph;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->text()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 179
    aget-object v3, v2, v0

    const-string/jumbo v4, "\r"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 173
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static stripFields(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 331
    invoke-static {p0}, Lorg/apache/poi/hwpf/usermodel/Range;->stripFields(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCommentsText()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    iget-object v1, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocument;->getCommentsRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    .line 166
    .local v0, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-static {v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getEndnoteText()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    iget-object v1, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocument;->getEndnoteRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    .line 159
    .local v0, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-static {v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getFooterText()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 242
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    iget-object v2, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v0, v2}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;)V

    .line 244
    .local v0, "hs":Lorg/apache/poi/hwpf/usermodel/HeaderStories;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 245
    .local v1, "ret":Ljava/lang/StringBuffer;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooter()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 247
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooter()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 249
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooter()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 251
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooter()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 253
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddFooter()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 255
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddFooter()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 258
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getFootnoteText()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    iget-object v1, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocument;->getFootnoteRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    .line 145
    .local v0, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-static {v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getHeaderText()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 217
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    iget-object v2, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v0, v2}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;)V

    .line 219
    .local v0, "hs":Lorg/apache/poi/hwpf/usermodel/HeaderStories;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 220
    .local v1, "ret":Ljava/lang/StringBuffer;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeader()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 222
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeader()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 224
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeader()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 226
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeader()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 228
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddHeader()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 230
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddHeader()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->appendHeaderFooter(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 233
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getMainTextboxText()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    iget-object v1, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocument;->getMainTextboxRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    .line 152
    .local v0, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-static {v0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getParagraphText()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 126
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/HWPFDocument;->getRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v1

    .line 128
    .local v1, "r":Lorg/apache/poi/hwpf/usermodel/Range;
    invoke-static {v1}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getParagraphText(Lorg/apache/poi/hwpf/usermodel/Range;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 138
    .end local v1    # "r":Lorg/apache/poi/hwpf/usermodel/Range;
    .local v2, "ret":[Ljava/lang/String;
    :goto_0
    return-object v2

    .line 130
    .end local v2    # "ret":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    .line 135
    .restart local v2    # "ret":[Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/extractor/WordExtractor;->getTextFromPieces()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 290
    :try_start_0
    new-instance v2, Lorg/apache/poi/hwpf/converter/WordToTextConverter;

    invoke-direct {v2}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;-><init>()V

    .line 292
    .local v2, "wordToTextConverter":Lorg/apache/poi/hwpf/converter/WordToTextConverter;
    new-instance v1, Lorg/apache/poi/hwpf/usermodel/HeaderStories;

    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v1, v3}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;-><init>(Lorg/apache/poi/hwpf/HWPFDocument;)V

    .line 294
    .local v1, "hs":Lorg/apache/poi/hwpf/usermodel/HeaderStories;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 295
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 296
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 295
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 297
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 298
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 299
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 298
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 300
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 301
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 302
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 301
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 304
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 305
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 306
    iget-object v4, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/HWPFDocument;->getMainTextboxRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 305
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 308
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 309
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 310
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 309
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 311
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 312
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 313
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 312
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 314
    :cond_4
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 315
    iget-object v3, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    .line 316
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v4

    .line 315
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 318
    :cond_5
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 320
    .end local v1    # "hs":Lorg/apache/poi/hwpf/usermodel/HeaderStories;
    .end local v2    # "wordToTextConverter":Lorg/apache/poi/hwpf/converter/WordToTextConverter;
    :catch_0
    move-exception v0

    .line 322
    .local v0, "exc":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getTextFromPieces()Ljava/lang/String;
    .locals 3

    .prologue
    .line 268
    iget-object v1, p0, Lorg/apache/poi/hwpf/extractor/WordExtractor;->doc:Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/HWPFDocument;->getDocumentText()Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "text":Ljava/lang/String;
    const-string/jumbo v1, "\r\r\r"

    const-string/jumbo v2, "\r\n\r\n\r\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 272
    const-string/jumbo v1, "\r\r"

    const-string/jumbo v2, "\r\n\r\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 274
    const-string/jumbo v1, "\r"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    :cond_0
    return-object v0
.end method
