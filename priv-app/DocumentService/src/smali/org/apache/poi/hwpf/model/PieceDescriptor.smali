.class public final Lorg/apache/poi/hwpf/model/PieceDescriptor;
.super Ljava/lang/Object;
.source "PieceDescriptor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field descriptor:S

.field fc:I

.field prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

.field unicode:Z


# direct methods
.method public constructor <init>([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->descriptor:S

    .line 41
    add-int/lit8 p2, p2, 0x2

    .line 42
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 43
    add-int/lit8 p2, p2, 0x4

    .line 44
    new-instance v0, Lorg/apache/poi/hwpf/model/PropertyModifier;

    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PropertyModifier;-><init>(S)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    .line 47
    iget v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    .line 58
    :goto_0
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    .line 54
    iget v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    const v1, -0x40000001    # -1.9999999f

    and-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 55
    iget v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    goto :goto_0
.end method

.method public static getSizeInBytes()I
    .locals 1

    .prologue
    .line 104
    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    if-ne p0, p1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v1

    .line 123
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 124
    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 126
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 127
    check-cast v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;

    .line 128
    .local v0, "other":Lorg/apache/poi/hwpf/model/PieceDescriptor;
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->descriptor:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->descriptor:S

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 129
    goto :goto_0

    .line 130
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    if-nez v3, :cond_5

    .line 132
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    if-eqz v3, :cond_6

    move v1, v2

    .line 133
    goto :goto_0

    .line 135
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/PropertyModifier;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 136
    goto :goto_0

    .line 137
    :cond_6
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 138
    goto :goto_0
.end method

.method public getFilePosition()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    return v0
.end method

.method public getPrm()Lorg/apache/poi/hwpf/model/PropertyModifier;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 110
    const/16 v0, 0x1f

    .line 111
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 112
    .local v1, "result":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->descriptor:S

    add-int/lit8 v1, v2, 0x1f

    .line 113
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 114
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    :goto_1
    add-int v1, v3, v2

    .line 115
    return v1

    .line 113
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/PropertyModifier;->hashCode()I

    move-result v2

    goto :goto_0

    .line 114
    :cond_1
    const/16 v2, 0x4d5

    goto :goto_1
.end method

.method public isUnicode()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    return v0
.end method

.method public setFilePosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 67
    iput p1, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 68
    return-void
.end method

.method protected toByteArray()[B
    .locals 4

    .prologue
    .line 83
    iget v2, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 84
    .local v2, "tempFc":I
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->unicode:Z

    if-nez v3, :cond_0

    .line 86
    mul-int/lit8 v2, v2, 0x2

    .line 87
    const/high16 v3, 0x40000000    # 2.0f

    or-int/2addr v2, v3

    .line 90
    :cond_0
    const/4 v1, 0x0

    .line 91
    .local v1, "offset":I
    const/16 v3, 0x8

    new-array v0, v3, [B

    .line 92
    .local v0, "buf":[B
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->descriptor:S

    invoke-static {v0, v1, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 93
    add-int/lit8 v1, v1, 0x2

    .line 94
    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 95
    add-int/lit8 v1, v1, 0x4

    .line 96
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PieceDescriptor;->prm:Lorg/apache/poi/hwpf/model/PropertyModifier;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PropertyModifier;->getValue()S

    move-result v3

    invoke-static {v0, v1, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 98
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "PieceDescriptor (pos: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 146
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "unicode"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; prm: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 147
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getPrm()Lorg/apache/poi/hwpf/model/PropertyModifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 146
    :cond_0
    const-string/jumbo v0, "non-unicode"

    goto :goto_0
.end method
