.class public Lorg/apache/poi/hwpf/converter/WordToFoConverter;
.super Lorg/apache/poi/hwpf/converter/AbstractWordConverter;
.source "WordToFoConverter.java"


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private endnotes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/w3c/dom/Element;",
            ">;"
        }
    .end annotation
.end field

.field protected final foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

.field private internalLinkCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private outputCharactersLanguage:Z

.field private usedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 69
    sput-object v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/converter/FoDocumentFacade;)V
    .locals 2
    .param p1, "foDocumentFacade"    # Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    .line 137
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->internalLinkCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 139
    iput-boolean v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->outputCharactersLanguage:Z

    .line 141
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->usedIds:Ljava/util/Set;

    .line 158
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 159
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    const/4 v1, 0x0

    .line 151
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    .line 137
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->internalLinkCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 139
    iput-boolean v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->outputCharactersLanguage:Z

    .line 141
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->usedIds:Ljava/util/Set;

    .line 153
    new-instance v0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;-><init>(Lorg/w3c/dom/Document;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 154
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 12
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 83
    array-length v8, p0

    const/4 v9, 0x2

    if-ge v8, v9, :cond_1

    .line 85
    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 86
    const-string/jumbo v9, "Usage: WordToFoConverter <inputFile.doc> <saveTo.fo>"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    const/4 v3, 0x0

    .line 96
    .local v3, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v8, Ljava/io/File;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->process(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 98
    .local v0, "doc":Lorg/w3c/dom/Document;
    new-instance v4, Ljava/io/FileWriter;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v4, v8}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    .end local v3    # "out":Ljava/io/FileWriter;
    .local v4, "out":Ljava/io/FileWriter;
    :try_start_1
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v0}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 100
    .local v1, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v6, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v6, v4}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 101
    .local v6, "streamResult":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v7

    .line 102
    .local v7, "tf":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v7}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v5

    .line 104
    .local v5, "serializer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v8, "encoding"

    const-string/jumbo v9, "UTF-8"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string/jumbo v8, "indent"

    const-string/jumbo v9, "yes"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-virtual {v5, v1, v6}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 114
    if-eqz v4, :cond_3

    .line 116
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 117
    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 109
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_0
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 114
    if-eqz v3, :cond_0

    .line 116
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 117
    :catch_1
    move-exception v2

    .line 118
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 113
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 114
    :goto_2
    if-eqz v3, :cond_2

    .line 116
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 120
    :cond_2
    :goto_3
    throw v8

    .line 117
    :catch_2
    move-exception v2

    .line 118
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 117
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .restart local v4    # "out":Ljava/io/FileWriter;
    .restart local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .restart local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .restart local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_3
    move-exception v2

    .line 118
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_0

    .line 113
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v3    # "out":Ljava/io/FileWriter;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .line 109
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_1
.end method

.method static process(Ljava/io/File;)Lorg/w3c/dom/Document;
    .locals 3
    .param p0, "docFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    .line 126
    .local v0, "hwpfDocument":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    new-instance v1, Lorg/apache/poi/hwpf/converter/WordToFoConverter;

    .line 127
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    .line 126
    invoke-direct {v1, v2}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;-><init>(Lorg/w3c/dom/Document;)V

    .line 129
    .local v1, "wordToFoConverter":Lorg/apache/poi/hwpf/converter/WordToFoConverter;
    invoke-virtual {v1, v0}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 130
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected createNoteInline(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 3
    .param p1, "noteIndexText"    # Ljava/lang/String;

    .prologue
    .line 163
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 164
    .local v0, "inline":Lorg/w3c/dom/Element;
    invoke-interface {v0, p1}, Lorg/w3c/dom/Element;->setTextContent(Ljava/lang/String;)V

    .line 165
    const-string/jumbo v1, "baseline-shift"

    const-string/jumbo v2, "super"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string/jumbo v1, "font-size"

    const-string/jumbo v2, "smaller"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    return-object v0
.end method

.method protected createPageMaster(Lorg/apache/poi/hwpf/usermodel/Section;Ljava/lang/String;I)Ljava/lang/String;
    .locals 14
    .param p1, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "sectionIndex"    # I

    .prologue
    .line 173
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getPageHeight()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x44b40000    # 1440.0f

    div-float v3, v11, v12

    .line 174
    .local v3, "height":F
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getPageWidth()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x44b40000    # 1440.0f

    div-float v10, v11, v12

    .line 175
    .local v10, "width":F
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginLeft()I

    move-result v11

    int-to-float v11, v11

    .line 176
    const/high16 v12, 0x44b40000    # 1440.0f

    .line 175
    div-float v4, v11, v12

    .line 177
    .local v4, "leftMargin":F
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginRight()I

    move-result v11

    int-to-float v11, v11

    .line 178
    const/high16 v12, 0x44b40000    # 1440.0f

    .line 177
    div-float v8, v11, v12

    .line 179
    .local v8, "rightMargin":F
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginTop()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x44b40000    # 1440.0f

    div-float v9, v11, v12

    .line 180
    .local v9, "topMargin":F
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getMarginBottom()I

    move-result v11

    int-to-float v11, v11

    .line 181
    const/high16 v12, 0x44b40000    # 1440.0f

    .line 180
    div-float v1, v11, v12

    .line 184
    .local v1, "bottomMargin":F
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v12, "-page"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 186
    .local v6, "pageMasterName":Ljava/lang/String;
    iget-object v11, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 187
    invoke-virtual {v11, v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addSimplePageMaster(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 188
    .local v5, "pageMaster":Lorg/w3c/dom/Element;
    const-string/jumbo v11, "page-height"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v13, "in"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v5, v11, v12}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string/jumbo v11, "page-width"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v13, "in"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v5, v11, v12}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v11, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v11, v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addRegionBody(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v7

    .line 192
    .local v7, "regionBody":Lorg/w3c/dom/Element;
    const-string/jumbo v11, "margin"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v13, "in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 193
    const-string/jumbo v13, "in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "in"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 192
    invoke-interface {v7, v11, v12}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getNumColumns()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_0

    .line 207
    const-string/jumbo v11, "column-count"

    .line 208
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getNumColumns()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 207
    invoke-interface {v7, v11, v12}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->isColumnsEvenlySpaced()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 211
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/Section;->getDistanceBetweenColumns()I

    move-result v11

    int-to-float v11, v11

    .line 212
    const/high16 v12, 0x44b40000    # 1440.0f

    .line 211
    div-float v2, v11, v12

    .line 213
    .local v2, "distance":F
    const-string/jumbo v11, "column-gap"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v13, "in"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v7, v11, v12}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .end local v2    # "distance":F
    :cond_0
    :goto_0
    return-object v6

    .line 217
    :cond_1
    const-string/jumbo v11, "column-gap"

    const-string/jumbo v12, "0.25in"

    invoke-interface {v7, v11, v12}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method public isOutputCharactersLanguage()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->outputCharactersLanguage:Z

    return v0
.end method

.method protected outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V
    .locals 4
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 240
    .local v0, "inline":Lorg/w3c/dom/Element;
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->getCharacterRunTriplet(Lorg/apache/poi/hwpf/usermodel/CharacterRun;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    move-result-object v2

    .line 242
    .local v2, "triplet":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    iget-object v3, v2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, v2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-static {v0, v3}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setFontFamily(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    .line 244
    :cond_0
    iget-boolean v3, v2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    invoke-static {v0, v3}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setBold(Lorg/w3c/dom/Element;Z)V

    .line 245
    iget-boolean v3, v2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    invoke-static {v0, v3}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setItalic(Lorg/w3c/dom/Element;Z)V

    .line 246
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/CharacterRun;->getFontSize()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-static {v0, v3}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setFontSize(Lorg/w3c/dom/Element;I)V

    .line 247
    invoke-static {p2, v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setCharactersProperties(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V

    .line 249
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->isOutputCharactersLanguage()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 250
    invoke-static {p2, v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setLanguage(Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/w3c/dom/Element;)V

    .line 252
    :cond_1
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 254
    iget-object v3, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v3, p3}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    .line 255
    .local v1, "textNode":Lorg/w3c/dom/Text;
    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 256
    return-void
.end method

.method protected processBookmarks(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/util/List;)V
    .locals 7
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/HWPFDocumentCore;",
            "Lorg/w3c/dom/Element;",
            "Lorg/apache/poi/hwpf/usermodel/Range;",
            "I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p5, "rangeBookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    move-object v3, p2

    .line 264
    .local v3, "parent":Lorg/w3c/dom/Element;
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 280
    if-eqz p3, :cond_1

    .line 281
    invoke-virtual {p0, p1, p4, p3, v3}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 282
    :cond_1
    return-void

    .line 264
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/Bookmark;

    .line 266
    .local v0, "bookmark":Lorg/apache/poi/hwpf/usermodel/Bookmark;
    iget-object v5, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 267
    .local v1, "bookmarkElement":Lorg/w3c/dom/Element;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "bookmark_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/poi/hwpf/usermodel/Bookmark;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "idName":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->setId(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 275
    invoke-interface {v3, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 276
    move-object v3, v1

    goto :goto_0
.end method

.method protected processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
    .locals 2
    .param p1, "summaryInformation"    # Lorg/apache/poi/hpsf/SummaryInformation;

    .prologue
    .line 288
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setTitle(Ljava/lang/String;)V

    .line 291
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setCreator(Ljava/lang/String;)V

    .line 294
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setKeywords(Ljava/lang/String;)V

    .line 297
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 298
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setDescription(Ljava/lang/String;)V

    .line 299
    :cond_3
    return-void
.end method

.method protected processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;Ljava/lang/String;Lorg/w3c/dom/Element;)V
    .locals 2
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "officeDrawing"    # Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 306
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 307
    invoke-virtual {v1, p4}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createExternalGraphic(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 308
    .local v0, "externalGraphic":Lorg/w3c/dom/Element;
    invoke-interface {p5, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 309
    return-void
.end method

.method protected processEndnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 8
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "noteIndex"    # I
    .param p3, "block"    # Lorg/w3c/dom/Element;
    .param p4, "endnoteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 315
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->internalLinkCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 316
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    .line 315
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 317
    .local v5, "textIndex":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "endnote_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 318
    .local v4, "forwardLinkName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "endnote_back_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 320
    .local v1, "backwardLinkName":Ljava/lang/String;
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 321
    invoke-virtual {v6, v4}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBasicLinkInternal(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 322
    .local v3, "forwardLink":Lorg/w3c/dom/Element;
    invoke-virtual {p0, v5}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->createNoteInline(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v6

    invoke-interface {v3, v6}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 323
    invoke-virtual {p0, v3, v1}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->setId(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    .line 324
    invoke-interface {p3, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 326
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 327
    .local v2, "endnote":Lorg/w3c/dom/Element;
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 328
    invoke-virtual {v6, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBasicLinkInternal(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 329
    .local v0, "backwardLink":Lorg/w3c/dom/Element;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->createNoteInline(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v6

    invoke-interface {v0, v6}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 330
    invoke-virtual {p0, v0, v4}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->setId(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    .line 331
    invoke-interface {v2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 333
    const/high16 v6, -0x80000000

    invoke-virtual {p0, p1, v6, p4, v2}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 336
    invoke-static {v2}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->compactInlines(Lorg/w3c/dom/Element;)V

    .line 337
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    return-void
.end method

.method protected processFootnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 11
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "noteIndex"    # I
    .param p3, "block"    # Lorg/w3c/dom/Element;
    .param p4, "footnoteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 344
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->internalLinkCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 345
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v9

    .line 344
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 346
    .local v8, "textIndex":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "footnote_"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 347
    .local v6, "forwardLinkName":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "footnote_back_"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "backwardLinkName":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createFootnote()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 350
    .local v2, "footNote":Lorg/w3c/dom/Element;
    invoke-interface {p3, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 352
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v7

    .line 353
    .local v7, "inline":Lorg/w3c/dom/Element;
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 354
    invoke-virtual {v9, v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBasicLinkInternal(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 355
    .local v5, "forwardLink":Lorg/w3c/dom/Element;
    invoke-virtual {p0, v8}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->createNoteInline(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v9

    invoke-interface {v5, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 356
    invoke-virtual {p0, v5, v1}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->setId(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    .line 357
    invoke-interface {v7, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 358
    invoke-interface {v2, v7}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 360
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createFootnoteBody()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 361
    .local v4, "footnoteBody":Lorg/w3c/dom/Element;
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 362
    .local v3, "footnoteBlock":Lorg/w3c/dom/Element;
    iget-object v9, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 363
    invoke-virtual {v9, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBasicLinkInternal(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 364
    .local v0, "backwardLink":Lorg/w3c/dom/Element;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->createNoteInline(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v9

    invoke-interface {v0, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 365
    invoke-virtual {p0, v0, v6}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->setId(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    .line 366
    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 367
    invoke-interface {v4, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 368
    invoke-interface {v2, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 370
    const/high16 v9, -0x80000000

    invoke-virtual {p0, p1, v9, p4, v3}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 373
    invoke-static {v3}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->compactInlines(Lorg/w3c/dom/Element;)V

    .line 374
    return-void
.end method

.method protected processHyperlink(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
    .locals 2
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "textRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 380
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 381
    invoke-virtual {v1, p5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBasicLinkExternal(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 382
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 384
    if-eqz p3, :cond_0

    .line 385
    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 387
    :cond_0
    return-void
.end method

.method protected processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;Ljava/lang/String;)V
    .locals 2
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;
    .param p4, "url"    # Ljava/lang/String;

    .prologue
    .line 392
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 393
    invoke-virtual {v1, p4}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createExternalGraphic(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 394
    .local v0, "externalGraphic":Lorg/w3c/dom/Element;
    invoke-static {p3, v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setPictureProperties(Lorg/apache/poi/hwpf/usermodel/Picture;Lorg/w3c/dom/Element;)V

    .line 395
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 396
    return-void
.end method

.method protected processImageWithoutPicturesManager(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V
    .locals 3
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;

    .prologue
    .line 403
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    iget-object v0, v0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    .line 404
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Image link to \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 405
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Picture;->suggestFullFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' can be here"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 404
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createComment(Ljava/lang/String;)Lorg/w3c/dom/Comment;

    move-result-object v0

    .line 403
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 406
    return-void
.end method

.method protected processLineBreak(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;)V
    .locals 1
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 412
    return-void
.end method

.method protected processPageBreak(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;)V
    .locals 6
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;

    .prologue
    .line 417
    const/4 v0, 0x0

    .line 418
    .local v0, "block":Lorg/w3c/dom/Element;
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 419
    .local v1, "childNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    if-lez v4, :cond_0

    .line 421
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v1, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 422
    .local v2, "lastChild":Lorg/w3c/dom/Node;
    instance-of v4, v2, Lorg/w3c/dom/Element;

    if-eqz v4, :cond_0

    move-object v3, v2

    .line 424
    check-cast v3, Lorg/w3c/dom/Element;

    .line 425
    .local v3, "lastElement":Lorg/w3c/dom/Element;
    const-string/jumbo v4, "break-after"

    invoke-interface {v3, v4}, Lorg/w3c/dom/Element;->hasAttribute(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 427
    move-object v0, v3

    .line 432
    .end local v2    # "lastChild":Lorg/w3c/dom/Node;
    .end local v3    # "lastElement":Lorg/w3c/dom/Element;
    :cond_0
    if-nez v0, :cond_1

    .line 434
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 435
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 437
    :cond_1
    const-string/jumbo v4, "break-after"

    const-string/jumbo v5, "page"

    invoke-interface {v0, v4, v5}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-void
.end method

.method protected processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
    .locals 4
    .param p1, "hwpfDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "textRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "pageref"    # Ljava/lang/String;

    .prologue
    .line 444
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 445
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "bookmark_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBasicLinkInternal(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 446
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 448
    if-eqz p3, :cond_0

    .line 449
    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 451
    :cond_0
    return-void
.end method

.method protected processParagraph(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;ILorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/String;)V
    .locals 7
    .param p1, "hwpfDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "parentFopElement"    # Lorg/w3c/dom/Element;
    .param p3, "currentTableLevel"    # I
    .param p4, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p5, "bulletText"    # Ljava/lang/String;

    .prologue
    .line 457
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 458
    .local v0, "block":Lorg/w3c/dom/Element;
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 460
    invoke-static {p4, v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setParagraphProperties(Lorg/apache/poi/hwpf/usermodel/Paragraph;Lorg/w3c/dom/Element;)V

    .line 462
    invoke-virtual {p4}, Lorg/apache/poi/hwpf/usermodel/Paragraph;->numCharacterRuns()I

    move-result v1

    .line 464
    .local v1, "charRuns":I
    if-nez v1, :cond_0

    .line 492
    :goto_0
    return-void

    .line 469
    :cond_0
    const/4 v2, 0x0

    .line 471
    .local v2, "haveAnyText":Z
    invoke-static {p5}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 473
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 474
    .local v3, "inline":Lorg/w3c/dom/Element;
    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 476
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v6, p5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v5

    .line 477
    .local v5, "textNode":Lorg/w3c/dom/Text;
    invoke-interface {v3, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 479
    invoke-virtual {p5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    or-int/2addr v2, v6

    .line 482
    .end local v3    # "inline":Lorg/w3c/dom/Element;
    .end local v5    # "textNode":Lorg/w3c/dom/Text;
    :cond_1
    invoke-virtual {p0, p1, p3, p4, v0}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    move-result v2

    .line 485
    if-nez v2, :cond_2

    .line 487
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createLeader()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 488
    .local v4, "leader":Lorg/w3c/dom/Element;
    invoke-interface {v0, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 491
    .end local v4    # "leader":Lorg/w3c/dom/Element;
    :cond_2
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->compactInlines(Lorg/w3c/dom/Element;)V

    goto :goto_0

    .line 479
    .restart local v3    # "inline":Lorg/w3c/dom/Element;
    .restart local v5    # "textNode":Lorg/w3c/dom/Text;
    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method protected processSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;I)V
    .locals 6
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;
    .param p3, "sectionCounter"    # I

    .prologue
    .line 498
    const-string/jumbo v4, "page"

    invoke-virtual {p0, p2, v4, p3}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->createPageMaster(Lorg/apache/poi/hwpf/usermodel/Section;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 500
    .local v3, "regularPage":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v4, v3}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addPageSequence(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 501
    .local v2, "pageSequence":Lorg/w3c/dom/Element;
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 502
    const-string/jumbo v5, "xsl-region-body"

    .line 501
    invoke-virtual {v4, v2, v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addFlowToPageSequence(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 504
    .local v1, "flow":Lorg/w3c/dom/Element;
    const/high16 v4, -0x80000000

    invoke-virtual {p0, p1, v1, p2, v4}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 506
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 508
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 510
    iget-object v4, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->endnotes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 512
    :cond_0
    return-void

    .line 508
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 509
    .local v0, "endnote":Lorg/w3c/dom/Element;
    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0
.end method

.method protected processTable(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Table;)V
    .locals 23
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;
    .param p3, "table"    # Lorg/apache/poi/hwpf/usermodel/Table;

    .prologue
    .line 517
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableHeader()Lorg/w3c/dom/Element;

    move-result-object v20

    .line 518
    .local v20, "tableHeader":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableBody()Lorg/w3c/dom/Element;

    move-result-object v17

    .line 521
    .local v17, "tableBody":Lorg/w3c/dom/Element;
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hwpf/converter/WordToHtmlUtils;->buildTableCellEdgesArray(Lorg/apache/poi/hwpf/usermodel/Table;)[I

    move-result-object v18

    .line 522
    .local v18, "tableCellEdges":[I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->numRows()I

    move-result v22

    .line 524
    .local v22, "tableRows":I
    const/high16 v13, -0x80000000

    .line 525
    .local v13, "maxColumns":I
    const/4 v14, 0x0

    .local v14, "r":I
    :goto_0
    move/from16 v0, v22

    if-lt v14, v0, :cond_1

    .line 530
    const/4 v14, 0x0

    :goto_1
    move/from16 v0, v22

    if-lt v14, v0, :cond_2

    .line 599
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTable()Lorg/w3c/dom/Element;

    move-result-object v19

    .line 600
    .local v19, "tableElement":Lorg/w3c/dom/Element;
    const-string/jumbo v5, "table-layout"

    const-string/jumbo v6, "fixed"

    move-object/from16 v0, v19

    invoke-interface {v0, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 603
    invoke-interface/range {v19 .. v20}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 605
    :cond_0
    invoke-interface/range {v17 .. v17}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 607
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 608
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 618
    :goto_2
    return-void

    .line 527
    .end local v19    # "tableElement":Lorg/w3c/dom/Element;
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v5

    invoke-static {v13, v5}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 525
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 532
    :cond_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v2

    .line 534
    .local v2, "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v21

    .line 535
    .local v21, "tableRowElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, v21

    invoke-static {v2, v0}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setTableRowProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Lorg/w3c/dom/Element;)V

    .line 538
    const/4 v12, 0x0

    .line 539
    .local v12, "currentEdgeIndex":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v15

    .line 540
    .local v15, "rowCells":I
    const/4 v9, 0x0

    .local v9, "c":I
    :goto_3
    if-lt v9, v15, :cond_4

    .line 586
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 588
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/TableRow;->isTableHeader()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 590
    invoke-interface/range {v20 .. v21}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 530
    :cond_3
    :goto_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 542
    :cond_4
    invoke-virtual {v2, v9}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v3

    .line 544
    .local v3, "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isVerticallyMerged()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 545
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/usermodel/TableCell;->isFirstVerticallyMerged()Z

    move-result v5

    if-nez v5, :cond_6

    .line 548
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v12, v3}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v5

    add-int/2addr v12, v5

    .line 540
    :cond_5
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 552
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 554
    .local v4, "tableCellElement":Lorg/w3c/dom/Element;
    if-nez v14, :cond_a

    const/4 v5, 0x1

    :goto_6
    add-int/lit8 v6, v22, -0x1

    if-ne v14, v6, :cond_b

    const/4 v6, 0x1

    :goto_7
    if-nez v9, :cond_c

    const/4 v7, 0x1

    .line 555
    :goto_8
    add-int/lit8 v8, v15, -0x1

    if-ne v9, v8, :cond_d

    const/4 v8, 0x1

    .line 553
    :goto_9
    invoke-static/range {v2 .. v8}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->setTableCellProperties(Lorg/apache/poi/hwpf/usermodel/TableRow;Lorg/apache/poi/hwpf/usermodel/TableCell;Lorg/w3c/dom/Element;ZZZZ)V

    .line 557
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v12, v3}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->getNumberColumnsSpanned([IILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v11

    .line 559
    .local v11, "colSpan":I
    add-int/2addr v12, v11

    .line 561
    if-eqz v11, :cond_5

    .line 564
    const/4 v5, 0x1

    if-eq v11, v5, :cond_7

    .line 565
    const-string/jumbo v5, "number-columns-spanned"

    .line 566
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 565
    invoke-interface {v4, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v7, v18

    move v8, v14

    move-object v10, v3

    .line 568
    invoke-virtual/range {v5 .. v10}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->getNumberRowsSpanned(Lorg/apache/poi/hwpf/usermodel/Table;[IIILorg/apache/poi/hwpf/usermodel/TableCell;)I

    move-result v16

    .line 570
    .local v16, "rowSpan":I
    const/4 v5, 0x1

    move/from16 v0, v16

    if-le v0, v5, :cond_8

    .line 571
    const-string/jumbo v5, "number-rows-spanned"

    .line 572
    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 571
    invoke-interface {v4, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :cond_8
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getTableLevel()I

    move-result v5

    .line 574
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v3, v5}, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 577
    invoke-interface {v4}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v5

    if-nez v5, :cond_9

    .line 579
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 580
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v5

    .line 579
    invoke-interface {v4, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 583
    :cond_9
    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_5

    .line 554
    .end local v11    # "colSpan":I
    .end local v16    # "rowSpan":I
    :cond_a
    const/4 v5, 0x0

    goto :goto_6

    :cond_b
    const/4 v6, 0x0

    goto :goto_7

    :cond_c
    const/4 v7, 0x0

    goto :goto_8

    .line 555
    :cond_d
    const/4 v8, 0x0

    goto :goto_9

    .line 594
    .end local v3    # "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    .end local v4    # "tableCellElement":Lorg/w3c/dom/Element;
    :cond_e
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto/16 :goto_4

    .line 612
    .end local v2    # "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    .end local v9    # "c":I
    .end local v12    # "currentEdgeIndex":I
    .end local v15    # "rowCells":I
    .end local v21    # "tableRowElement":Lorg/w3c/dom/Element;
    .restart local v19    # "tableElement":Lorg/w3c/dom/Element;
    :cond_f
    sget-object v5, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 613
    const/4 v6, 0x5

    .line 614
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Table without body starting on offset "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 615
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getStartOffset()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " -- "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 616
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getEndOffset()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 614
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 612
    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto/16 :goto_2
.end method

.method protected setId(Lorg/w3c/dom/Element;Ljava/lang/String;)Z
    .locals 4
    .param p1, "element"    # Lorg/w3c/dom/Element;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 623
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->usedIds:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 625
    sget-object v0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    .line 626
    const-string/jumbo v2, "Tried to create element with same ID \'"

    const-string/jumbo v3, "\'. Skipped"

    .line 625
    invoke-virtual {v0, v1, v2, p2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 627
    const/4 v0, 0x0

    .line 632
    :goto_0
    return v0

    .line 630
    :cond_0
    const-string/jumbo v0, "id"

    invoke-interface {p1, v0, p2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->usedIds:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 632
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setOutputCharactersLanguage(Z)V
    .locals 0
    .param p1, "outputCharactersLanguage"    # Z

    .prologue
    .line 637
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/converter/WordToFoConverter;->outputCharactersLanguage:Z

    .line 638
    return-void
.end method
