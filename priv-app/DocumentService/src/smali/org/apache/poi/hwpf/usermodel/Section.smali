.class public final Lorg/apache/poi/hwpf/usermodel/Section;
.super Lorg/apache/poi/hwpf/usermodel/Range;
.source "Section.java"


# instance fields
.field private _props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/SEPX;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "sepx"    # Lorg/apache/poi/hwpf/model/SEPX;
    .param p2, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 29
    iget v0, p2, Lorg/apache/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/SEPX;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 30
    iget v1, p2, Lorg/apache/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/SEPX;->getEnd()I

    move-result v2

    .line 29
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 30
    invoke-direct {p0, v0, v1, p2}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 33
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/usermodel/Range;->getDocument()Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    .line 37
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/SEPX;->getSectionProperties()Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/Section;

    .line 42
    .local v0, "s":Lorg/apache/poi/hwpf/usermodel/Section;
    iget-object v1, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    iput-object v1, v0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    .line 43
    return-object v0
.end method

.method public getDistanceBetweenColumns()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v0

    return v0
.end method

.method public getMarginBottom()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v0

    return v0
.end method

.method public getMarginLeft()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v0

    return v0
.end method

.method public getMarginRight()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v0

    return v0
.end method

.method public getMarginTop()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v0

    return v0
.end method

.method public getNumColumns()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getPageHeight()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v0

    return v0
.end method

.method public getPageWidth()I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v0

    return v0
.end method

.method public isColumnsEvenlySpaced()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/Section;->_props:Lorg/apache/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Section ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getStartOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Section;->getEndOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x2

    return v0
.end method
