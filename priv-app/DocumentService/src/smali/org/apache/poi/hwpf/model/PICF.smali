.class public Lorg/apache/poi/hwpf/model/PICF;
.super Lorg/apache/poi/hwpf/model/types/PICFAbstractType;
.source "PICF.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/PICFAbstractType;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/PICFAbstractType;-><init>()V

    .line 43
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/PICF;->fillFields([BI)V

    .line 44
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p0, p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 54
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 55
    check-cast v0, Lorg/apache/poi/hwpf/model/PICF;

    .line 56
    .local v0, "other":Lorg/apache/poi/hwpf/model/PICF;
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_10_padding2:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_10_padding2:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 57
    goto :goto_0

    .line 58
    :cond_4
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_11_dxaGoal:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_11_dxaGoal:S

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 59
    goto :goto_0

    .line 60
    :cond_5
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_12_dyaGoal:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_12_dyaGoal:S

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 61
    goto :goto_0

    .line 62
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_13_mx:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_13_mx:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 63
    goto :goto_0

    .line 64
    :cond_7
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_14_my:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_14_my:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 65
    goto :goto_0

    .line 66
    :cond_8
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_15_dxaReserved1:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_15_dxaReserved1:S

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 67
    goto :goto_0

    .line 68
    :cond_9
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_16_dyaReserved1:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_16_dyaReserved1:S

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 69
    goto :goto_0

    .line 70
    :cond_a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_17_dxaReserved2:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_17_dxaReserved2:S

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 71
    goto :goto_0

    .line 72
    :cond_b
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_18_dyaReserved2:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_18_dyaReserved2:S

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 73
    goto :goto_0

    .line 74
    :cond_c
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_19_fReserved:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_19_fReserved:B

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_d
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_1_lcb:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_1_lcb:I

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 77
    goto :goto_0

    .line 78
    :cond_e
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_20_bpp:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_20_bpp:B

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 79
    goto :goto_0

    .line 80
    :cond_f
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_21_brcTop80:[B

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_21_brcTop80:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 81
    goto :goto_0

    .line 82
    :cond_10
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_22_brcLeft80:[B

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_22_brcLeft80:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 83
    goto/16 :goto_0

    .line 84
    :cond_11
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_23_brcBottom80:[B

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_23_brcBottom80:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 85
    goto/16 :goto_0

    .line 86
    :cond_12
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_24_brcRight80:[B

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_24_brcRight80:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 87
    goto/16 :goto_0

    .line 88
    :cond_13
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_25_dxaReserved3:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_25_dxaReserved3:S

    if-eq v3, v4, :cond_14

    move v1, v2

    .line 89
    goto/16 :goto_0

    .line 90
    :cond_14
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_26_dyaReserved3:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_26_dyaReserved3:S

    if-eq v3, v4, :cond_15

    move v1, v2

    .line 91
    goto/16 :goto_0

    .line 92
    :cond_15
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_27_cProps:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_27_cProps:S

    if-eq v3, v4, :cond_16

    move v1, v2

    .line 93
    goto/16 :goto_0

    .line 94
    :cond_16
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_2_cbHeader:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_2_cbHeader:I

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 95
    goto/16 :goto_0

    .line 96
    :cond_17
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_3_mm:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_3_mm:S

    if-eq v3, v4, :cond_18

    move v1, v2

    .line 97
    goto/16 :goto_0

    .line 98
    :cond_18
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_4_xExt:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_4_xExt:S

    if-eq v3, v4, :cond_19

    move v1, v2

    .line 99
    goto/16 :goto_0

    .line 100
    :cond_19
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_5_yExt:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_5_yExt:S

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 101
    goto/16 :goto_0

    .line 102
    :cond_1a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_6_swHMF:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_6_swHMF:S

    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 103
    goto/16 :goto_0

    .line 104
    :cond_1b
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_7_grf:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_7_grf:I

    if-eq v3, v4, :cond_1c

    move v1, v2

    .line 105
    goto/16 :goto_0

    .line 106
    :cond_1c
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_8_padding:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_8_padding:I

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 107
    goto/16 :goto_0

    .line 108
    :cond_1d
    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_9_mmPM:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/PICF;->field_9_mmPM:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 109
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 116
    const/16 v0, 0x1f

    .line 117
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 118
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/PICF;->field_10_padding2:I

    add-int/lit8 v1, v2, 0x1f

    .line 119
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_11_dxaGoal:S

    add-int v1, v2, v3

    .line 120
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_12_dyaGoal:S

    add-int v1, v2, v3

    .line 121
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_13_mx:I

    add-int v1, v2, v3

    .line 122
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_14_my:I

    add-int v1, v2, v3

    .line 123
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_15_dxaReserved1:S

    add-int v1, v2, v3

    .line 124
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_16_dyaReserved1:S

    add-int v1, v2, v3

    .line 125
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_17_dxaReserved2:S

    add-int v1, v2, v3

    .line 126
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_18_dyaReserved2:S

    add-int v1, v2, v3

    .line 127
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_19_fReserved:B

    add-int v1, v2, v3

    .line 128
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_1_lcb:I

    add-int v1, v2, v3

    .line 129
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_20_bpp:B

    add-int v1, v2, v3

    .line 130
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_21_brcTop80:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v1, v2, v3

    .line 131
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_22_brcLeft80:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v1, v2, v3

    .line 132
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_23_brcBottom80:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v1, v2, v3

    .line 133
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_24_brcRight80:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v1, v2, v3

    .line 134
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_25_dxaReserved3:S

    add-int v1, v2, v3

    .line 135
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_26_dyaReserved3:S

    add-int v1, v2, v3

    .line 136
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_27_cProps:S

    add-int v1, v2, v3

    .line 137
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_2_cbHeader:I

    add-int v1, v2, v3

    .line 138
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_3_mm:S

    add-int v1, v2, v3

    .line 139
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_4_xExt:S

    add-int v1, v2, v3

    .line 140
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_5_yExt:S

    add-int v1, v2, v3

    .line 141
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_6_swHMF:S

    add-int v1, v2, v3

    .line 142
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_7_grf:I

    add-int v1, v2, v3

    .line 143
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_8_padding:I

    add-int v1, v2, v3

    .line 144
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/PICF;->field_9_mmPM:I

    add-int v1, v2, v3

    .line 145
    return v1
.end method
