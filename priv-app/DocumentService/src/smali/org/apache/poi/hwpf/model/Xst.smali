.class public Lorg/apache/poi/hwpf/model/Xst;
.super Ljava/lang/Object;
.source "Xst.java"


# instance fields
.field private _cch:I

.field private _rgtchar:[C


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    .line 51
    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    .line 74
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    move v0, p2

    .line 58
    .local v0, "offset":I
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    .line 59
    add-int/lit8 v0, v0, 0x2

    .line 61
    iget v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    new-array v2, v2, [C

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    .line 62
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    iget v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    if-lt v1, v2, :cond_0

    .line 68
    return-void

    .line 64
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    int-to-char v3, v3

    aput-char v3, v2, v1

    .line 65
    add-int/lit8 v0, v0, 0x2

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    if-ne p0, p1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v1

    .line 81
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 82
    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 84
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 85
    check-cast v0, Lorg/apache/poi/hwpf/model/Xst;

    .line 86
    .local v0, "other":Lorg/apache/poi/hwpf/model/Xst;
    iget v3, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 87
    goto :goto_0

    .line 88
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 89
    goto :goto_0
.end method

.method public getAsJavaString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public getCch()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    return v0
.end method

.method public getRgtchar()[C
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 123
    const/16 v0, 0x1f

    .line 124
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 125
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    add-int/lit8 v1, v2, 0x1f

    .line 126
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([C)I

    move-result v3

    add-int v1, v2, v3

    .line 127
    return v1
.end method

.method public serialize([BI)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 132
    move v1, p2

    .line 134
    .local v1, "offset":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 135
    add-int/lit8 v1, v1, 0x2

    .line 137
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 142
    return-void

    .line 137
    :cond_0
    aget-char v0, v3, v2

    .line 139
    .local v0, "c":C
    int-to-short v5, v0

    invoke-static {p1, v1, v5}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 140
    add-int/lit8 v1, v1, 0x2

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Xst ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_cch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Xst;->_rgtchar:[C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
