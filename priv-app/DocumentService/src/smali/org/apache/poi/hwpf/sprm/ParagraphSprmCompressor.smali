.class public final Lorg/apache/poi/hwpf/sprm/ParagraphSprmCompressor;
.super Ljava/lang/Object;
.source "ParagraphSprmCompressor.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static compressParagraphProperty(Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;)[B
    .locals 13
    .param p0, "newPAP"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;
    .param p1, "oldPAP"    # Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;

    .prologue
    const/4 v12, 0x4

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 40
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v3, "sprmList":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v2, 0x0

    .line 44
    .local v2, "size":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v9

    if-eq v6, v9, :cond_0

    .line 47
    const/16 v6, 0x4600

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIstd()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 49
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v9

    if-eq v6, v9, :cond_1

    .line 52
    const/16 v6, 0x2403

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getJc()B

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 54
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFSideBySide()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFSideBySide()Z

    move-result v9

    if-eq v6, v9, :cond_2

    .line 57
    const/16 v6, 0x2404

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFSideBySide()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 59
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeep()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeep()Z

    move-result v9

    if-eq v6, v9, :cond_3

    .line 61
    const/16 v6, 0x2405

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeep()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 63
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeepFollow()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeepFollow()Z

    move-result v9

    if-eq v6, v9, :cond_4

    .line 65
    const/16 v6, 0x2406

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKeepFollow()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 67
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPageBreakBefore()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPageBreakBefore()Z

    move-result v9

    if-eq v6, v9, :cond_5

    .line 69
    const/16 v6, 0x2407

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPageBreakBefore()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 71
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcl()B

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcl()B

    move-result v9

    if-eq v6, v9, :cond_6

    .line 73
    const/16 v6, 0x2408

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcl()B

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 75
    :cond_6
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcp()B

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcp()B

    move-result v9

    if-eq v6, v9, :cond_7

    .line 77
    const/16 v6, 0x2409

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcp()B

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 79
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlvl()B

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlvl()B

    move-result v9

    if-eq v6, v9, :cond_8

    .line 81
    const/16 v6, 0x260a

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlvl()B

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 83
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v9

    if-eq v6, v9, :cond_9

    .line 85
    const/16 v6, 0x460b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIlfo()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 87
    :cond_9
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoLnn()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoLnn()Z

    move-result v9

    if-eq v6, v9, :cond_a

    .line 89
    const/16 v6, 0x240c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoLnn()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 91
    :cond_a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItbdMac()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItbdMac()I

    move-result v9

    if-ne v6, v9, :cond_b

    .line 92
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgdxaTab()[I

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgdxaTab()[I

    move-result-object v9

    invoke-static {v6, v9}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 93
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgtbd()[Lorg/apache/poi/hwpf/model/TabDescriptor;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRgtbd()[Lorg/apache/poi/hwpf/model/TabDescriptor;

    move-result-object v9

    invoke-static {v6, v9}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    .line 113
    :cond_b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft()I

    move-result v9

    if-eq v6, v9, :cond_c

    .line 116
    const/16 v6, -0x7bf1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 120
    :cond_c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft1()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft1()I

    move-result v9

    if-eq v6, v9, :cond_d

    .line 123
    const/16 v6, -0x7bef

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaLeft1()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 125
    :cond_d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaRight()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaRight()I

    move-result v9

    if-eq v6, v9, :cond_e

    .line 128
    const/16 v6, -0x7bf2

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaRight()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 130
    :cond_e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcLeft()S

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcLeft()S

    move-result v9

    if-eq v6, v9, :cond_f

    .line 133
    const/16 v6, 0x4456

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcLeft()S

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 135
    :cond_f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcLeft1()S

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcLeft1()S

    move-result v9

    if-eq v6, v9, :cond_10

    .line 138
    const/16 v6, 0x4457

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcLeft1()S

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 140
    :cond_10
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcRight()S

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcRight()S

    move-result v9

    if-eq v6, v9, :cond_11

    .line 143
    const/16 v6, 0x4455

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxcRight()S

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 145
    :cond_11
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    .line 148
    new-array v1, v12, [B

    .line 149
    .local v1, "buf":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLspd()Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v6

    invoke-virtual {v6, v1, v8}, Lorg/apache/poi/hwpf/usermodel/LineSpacingDescriptor;->serialize([BI)V

    .line 151
    const/16 v6, 0x6412

    invoke-static {v1}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 153
    .end local v1    # "buf":[B
    :cond_12
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaBefore()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaBefore()I

    move-result v9

    if-eq v6, v9, :cond_13

    .line 156
    const/16 v6, -0x5bed

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaBefore()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 158
    :cond_13
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAfter()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAfter()I

    move-result v9

    if-eq v6, v9, :cond_14

    .line 161
    const/16 v6, -0x5bec

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAfter()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 163
    :cond_14
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFDyaBeforeAuto()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFDyaBeforeAuto()Z

    move-result v9

    if-eq v6, v9, :cond_15

    .line 166
    const/16 v6, 0x245b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFDyaBeforeAuto()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 168
    :cond_15
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFDyaAfterAuto()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFDyaAfterAuto()Z

    move-result v9

    if-eq v6, v9, :cond_16

    .line 171
    const/16 v6, 0x245c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFDyaAfterAuto()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 173
    :cond_16
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInTable()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInTable()Z

    move-result v9

    if-eq v6, v9, :cond_17

    .line 176
    const/16 v6, 0x2416

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInTable()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 178
    :cond_17
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtp()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtp()Z

    move-result v9

    if-eq v6, v9, :cond_18

    .line 181
    const/16 v6, 0x2417

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtp()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 183
    :cond_18
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaAbs()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaAbs()I

    move-result v9

    if-eq v6, v9, :cond_19

    .line 186
    const/16 v6, -0x7be8

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaAbs()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 188
    :cond_19
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAbs()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAbs()I

    move-result v9

    if-eq v6, v9, :cond_1a

    .line 191
    const/16 v6, -0x7be7

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaAbs()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 193
    :cond_1a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaWidth()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaWidth()I

    move-result v9

    if-eq v6, v9, :cond_1b

    .line 196
    const/16 v6, -0x7be6

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaWidth()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 200
    :cond_1b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWr()B

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWr()B

    move-result v9

    if-eq v6, v9, :cond_1c

    .line 202
    const/16 v6, 0x2423

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWr()B

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 205
    :cond_1c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 208
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBar()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v0

    .line 209
    .local v0, "brc":I
    const/16 v6, 0x6428

    invoke-static {v6, v0, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 211
    .end local v0    # "brc":I
    :cond_1d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1e

    .line 214
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcBottom()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v0

    .line 215
    .restart local v0    # "brc":I
    const/16 v6, 0x6426

    invoke-static {v6, v0, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 217
    .end local v0    # "brc":I
    :cond_1e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1f

    .line 220
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcLeft()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v0

    .line 221
    .restart local v0    # "brc":I
    const/16 v6, 0x6425

    invoke-static {v6, v0, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 225
    .end local v0    # "brc":I
    :cond_1f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_20

    .line 228
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcRight()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v0

    .line 229
    .restart local v0    # "brc":I
    const/16 v6, 0x6427

    invoke-static {v6, v0, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 231
    .end local v0    # "brc":I
    :cond_20
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_21

    .line 234
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getBrcTop()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v0

    .line 235
    .restart local v0    # "brc":I
    const/16 v6, 0x6424

    invoke-static {v6, v0, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 237
    .end local v0    # "brc":I
    :cond_21
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoAutoHyph()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoAutoHyph()Z

    move-result v9

    if-eq v6, v9, :cond_22

    .line 239
    const/16 v6, 0x242a

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNoAutoHyph()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 241
    :cond_22
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaHeight()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaHeight()I

    move-result v9

    if-ne v6, v9, :cond_23

    .line 242
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFMinHeight()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFMinHeight()Z

    move-result v9

    if-eq v6, v9, :cond_25

    .line 245
    :cond_23
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaHeight()I

    move-result v6

    int-to-short v4, v6

    .line 246
    .local v4, "val":S
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFMinHeight()Z

    move-result v6

    if-eqz v6, :cond_24

    .line 248
    const v6, 0x8000

    or-int/2addr v6, v4

    int-to-short v4, v6

    .line 250
    :cond_24
    const/16 v6, 0x442b

    invoke-static {v6, v4, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 252
    .end local v4    # "val":S
    :cond_25
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v6

    if-eqz v6, :cond_26

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_26

    .line 255
    const/16 v6, 0x442c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDcs()Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/hwpf/usermodel/DropCapSpecifier;->toShort()S

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 257
    :cond_26
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaFromText()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaFromText()I

    move-result v9

    if-eq v6, v9, :cond_27

    .line 260
    const/16 v6, -0x7bd2

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaFromText()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 262
    :cond_27
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaFromText()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaFromText()I

    move-result v9

    if-eq v6, v9, :cond_28

    .line 265
    const/16 v6, -0x7bd1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDxaFromText()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 267
    :cond_28
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFLocked()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFLocked()Z

    move-result v9

    if-eq v6, v9, :cond_29

    .line 270
    const/16 v6, 0x2430

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFLocked()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 272
    :cond_29
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWidowControl()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWidowControl()Z

    move-result v9

    if-eq v6, v9, :cond_2a

    .line 275
    const/16 v6, 0x2431

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWidowControl()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 277
    :cond_2a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKinsoku()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFKinsoku()Z

    move-result v9

    if-eq v6, v9, :cond_2b

    .line 279
    const/16 v6, 0x2433

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDyaBefore()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 281
    :cond_2b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWordWrap()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWordWrap()Z

    move-result v9

    if-eq v6, v9, :cond_2c

    .line 283
    const/16 v6, 0x2434

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFWordWrap()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 285
    :cond_2c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFOverflowPunct()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFOverflowPunct()Z

    move-result v9

    if-eq v6, v9, :cond_2d

    .line 287
    const/16 v6, 0x2435

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFOverflowPunct()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 289
    :cond_2d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTopLinePunct()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTopLinePunct()Z

    move-result v9

    if-eq v6, v9, :cond_2e

    .line 291
    const/16 v6, 0x2436

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTopLinePunct()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 293
    :cond_2e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFAutoSpaceDE()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFAutoSpaceDE()Z

    move-result v9

    if-eq v6, v9, :cond_2f

    .line 295
    const/16 v6, 0x2437

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFAutoSpaceDE()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 297
    :cond_2f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFAutoSpaceDN()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFAutoSpaceDN()Z

    move-result v9

    if-eq v6, v9, :cond_30

    .line 299
    const/16 v6, 0x2438

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFAutoSpaceDN()Z

    move-result v9

    invoke-static {v6, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 301
    :cond_30
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWAlignFont()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWAlignFont()I

    move-result v9

    if-eq v6, v9, :cond_31

    .line 303
    const/16 v6, 0x4439

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getWAlignFont()I

    move-result v9

    invoke-static {v6, v9, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 307
    :cond_31
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFBackward()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFBackward()Z

    move-result v9

    if-ne v6, v9, :cond_32

    .line 308
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFVertical()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFVertical()Z

    move-result v9

    if-ne v6, v9, :cond_32

    .line 309
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFRotateFont()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFRotateFont()Z

    move-result v9

    if-eq v6, v9, :cond_36

    .line 311
    :cond_32
    const/4 v4, 0x0

    .line 312
    .local v4, "val":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFBackward()Z

    move-result v6

    if-eqz v6, :cond_33

    .line 314
    or-int/lit8 v4, v4, 0x2

    .line 316
    :cond_33
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFVertical()Z

    move-result v6

    if-eqz v6, :cond_34

    .line 318
    or-int/lit8 v4, v4, 0x1

    .line 320
    :cond_34
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->isFRotateFont()Z

    move-result v6

    if-eqz v6, :cond_35

    .line 322
    or-int/lit8 v4, v4, 0x4

    .line 324
    :cond_35
    const/16 v6, 0x443a

    invoke-static {v6, v4, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 326
    .end local v4    # "val":I
    :cond_36
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getAnld()[B

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getAnld()[B

    move-result-object v9

    invoke-static {v6, v9}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-nez v6, :cond_37

    .line 329
    const/16 v6, -0x39c2

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getAnld()[B

    move-result-object v9

    invoke-static {v6, v8, v9, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 331
    :cond_37
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPropRMark()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPropRMark()Z

    move-result v9

    if-ne v6, v9, :cond_38

    .line 332
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIbstPropRMark()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIbstPropRMark()I

    move-result v9

    if-ne v6, v9, :cond_38

    .line 333
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_39

    .line 336
    :cond_38
    const/4 v6, 0x7

    new-array v1, v6, [B

    .line 337
    .restart local v1    # "buf":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFPropRMark()Z

    move-result v6

    if-eqz v6, :cond_43

    move v6, v7

    :goto_0
    int-to-byte v6, v6

    aput-byte v6, v1, v8

    .line 338
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getIbstPropRMark()I

    move-result v6

    int-to-short v6, v6

    invoke-static {v1, v7, v6}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 339
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v1, v7}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->serialize([BI)V

    .line 340
    const/16 v6, -0x39c1

    invoke-static {v6, v8, v1, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 342
    .end local v1    # "buf":[B
    :cond_39
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLvl()B

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLvl()B

    move-result v7

    if-eq v6, v7, :cond_3a

    .line 345
    const/16 v6, 0x2640

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getLvl()B

    move-result v7

    invoke-static {v6, v7, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 347
    :cond_3a
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFBiDi()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFBiDi()Z

    move-result v7

    if-eq v6, v7, :cond_3b

    .line 350
    const/16 v6, 0x2441

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFBiDi()Z

    move-result v7

    invoke-static {v6, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 352
    :cond_3b
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNumRMIns()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNumRMIns()Z

    move-result v7

    if-eq v6, v7, :cond_3c

    .line 355
    const/16 v6, 0x2443

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFNumRMIns()Z

    move-result v7

    invoke-static {v6, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 357
    :cond_3c
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getNumrm()[B

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getNumrm()[B

    move-result-object v7

    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-nez v6, :cond_3d

    .line 360
    const/16 v6, -0x39bb

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getNumrm()[B

    move-result-object v7

    invoke-static {v6, v8, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 362
    :cond_3d
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInnerTableCell()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInnerTableCell()Z

    move-result v7

    if-eq v6, v7, :cond_3e

    .line 365
    const/16 v6, 0x244b

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFInnerTableCell()Z

    move-result v7

    invoke-static {v6, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 367
    :cond_3e
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtpEmbedded()Z

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtpEmbedded()Z

    move-result v7

    if-eq v6, v7, :cond_3f

    .line 370
    const/16 v6, 0x244c

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getFTtpEmbedded()Z

    move-result v7

    invoke-static {v6, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 373
    :cond_3f
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v6

    if-eqz v6, :cond_40

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_40

    .line 377
    const/16 v6, -0x39b3

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->serialize()[B

    move-result-object v7

    invoke-static {v6, v8, v7, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 381
    :cond_40
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItap()I

    move-result v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItap()I

    move-result v7

    if-eq v6, v7, :cond_41

    .line 384
    const/16 v6, 0x6649

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getItap()I

    move-result v7

    invoke-static {v6, v7, v10, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 387
    :cond_41
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRsid()J

    move-result-wide v6

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRsid()J

    move-result-wide v10

    cmp-long v6, v6, v10

    if-eqz v6, :cond_42

    .line 390
    new-array v5, v12, [B

    .line 391
    .local v5, "value":[B
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ParagraphProperties;->getRsid()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lorg/apache/poi/util/LittleEndian;->putUInt([BJ)V

    .line 392
    const/16 v6, 0x6467

    invoke-static {v6, v8, v5, v3}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v6

    add-int/2addr v2, v6

    .line 395
    .end local v5    # "value":[B
    :cond_42
    invoke-static {v3, v2}, Lorg/apache/poi/hwpf/sprm/SprmUtils;->getGrpprl(Ljava/util/List;I)[B

    move-result-object v6

    return-object v6

    .restart local v1    # "buf":[B
    :cond_43
    move v6, v8

    .line 337
    goto/16 :goto_0
.end method
