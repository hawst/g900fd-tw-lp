.class Lorg/apache/poi/hwpf/model/StdfBase;
.super Lorg/apache/poi/hwpf/model/types/StdfBaseAbstractType;
.source "StdfBase.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/StdfBaseAbstractType;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/StdfBaseAbstractType;-><init>()V

    .line 35
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/StdfBase;->fillFields([BI)V

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p0, p1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 44
    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 46
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 47
    check-cast v0, Lorg/apache/poi/hwpf/model/StdfBase;

    .line 48
    .local v0, "other":Lorg/apache/poi/hwpf/model/StdfBase;
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_1_info1:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/StdfBase;->field_1_info1:S

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 49
    goto :goto_0

    .line 50
    :cond_4
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_2_info2:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/StdfBase;->field_2_info2:S

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 51
    goto :goto_0

    .line 52
    :cond_5
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_3_info3:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/StdfBase;->field_3_info3:S

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_4_bchUpe:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/StdfBase;->field_4_bchUpe:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 55
    goto :goto_0

    .line 56
    :cond_7
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_5_grfstd:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/StdfBase;->field_5_grfstd:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 57
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 64
    const/16 v0, 0x1f

    .line 65
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 66
    .local v1, "result":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_1_info1:S

    add-int/lit8 v1, v2, 0x1f

    .line 67
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_2_info2:S

    add-int v1, v2, v3

    .line 68
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_3_info3:S

    add-int v1, v2, v3

    .line 69
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_4_bchUpe:I

    add-int v1, v2, v3

    .line 70
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/StdfBase;->field_5_grfstd:S

    add-int v1, v2, v3

    .line 71
    return v1
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 76
    invoke-static {}, Lorg/apache/poi/hwpf/model/StdfBase;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 77
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/StdfBase;->serialize([BI)V

    .line 78
    return-object v0
.end method
