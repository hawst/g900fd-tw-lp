.class public abstract Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;
.super Ljava/lang/Object;
.source "LVLFAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final fConverted:Lorg/apache/poi/util/BitField;

.field private static final fIndentSav:Lorg/apache/poi/util/BitField;

.field private static final fLegal:Lorg/apache/poi/util/BitField;

.field private static final fNoRestart:Lorg/apache/poi/util/BitField;

.field private static final fTentative:Lorg/apache/poi/util/BitField;

.field private static final jc:Lorg/apache/poi/util/BitField;

.field private static final unused1:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_10_ilvlRestartLim:S

.field protected field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

.field protected field_1_iStartAt:I

.field protected field_2_nfc:B

.field protected field_3_info:B

.field protected field_4_rgbxchNums:[B

.field protected field_5_ixchFollow:B

.field protected field_6_dxaIndentSav:I

.field protected field_7_unused2:I

.field protected field_8_cbGrpprlChpx:S

.field protected field_9_cbGrpprlPapx:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->jc:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fLegal:Lorg/apache/poi/util/BitField;

    .line 52
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fNoRestart:Lorg/apache/poi/util/BitField;

    .line 53
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fIndentSav:Lorg/apache/poi/util/BitField;

    .line 54
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fConverted:Lorg/apache/poi/util/BitField;

    .line 55
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    .line 56
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fTentative:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/16 v0, 0x9

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    .line 69
    new-instance v0, Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Grfhic;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 70
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 114
    const/16 v0, 0x1c

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    if-ne p0, p1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v1

    .line 122
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 123
    goto :goto_0

    .line 124
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 125
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 126
    check-cast v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;

    .line 127
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 128
    goto :goto_0

    .line 129
    :cond_4
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 130
    goto :goto_0

    .line 131
    :cond_5
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 132
    goto :goto_0

    .line 133
    :cond_6
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 134
    goto :goto_0

    .line 135
    :cond_7
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 136
    goto :goto_0

    .line 137
    :cond_8
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 138
    goto :goto_0

    .line 139
    :cond_9
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 140
    goto :goto_0

    .line 141
    :cond_a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 142
    goto :goto_0

    .line 143
    :cond_b
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 144
    goto :goto_0

    .line 145
    :cond_c
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 146
    goto :goto_0

    .line 147
    :cond_d
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-nez v3, :cond_e

    .line 149
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    if-eqz v3, :cond_0

    move v1, v2

    .line 150
    goto :goto_0

    .line 152
    :cond_e
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Grfhic;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 153
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 74
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    .line 75
    add-int/lit8 v0, p2, 0x4

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    .line 76
    add-int/lit8 v0, p2, 0x5

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 77
    add-int/lit8 v0, p2, 0x6

    const/16 v1, 0x9

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    .line 78
    add-int/lit8 v0, p2, 0xf

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    .line 79
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    .line 80
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    .line 81
    add-int/lit8 v0, p2, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUByte([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    .line 82
    add-int/lit8 v0, p2, 0x19

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUByte([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    .line 83
    add-int/lit8 v0, p2, 0x1a

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUByte([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    .line 84
    new-instance v0, Lorg/apache/poi/hwpf/model/Grfhic;

    add-int/lit8 v1, p2, 0x1b

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Grfhic;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 85
    return-void
.end method

.method public getCbGrpprlChpx()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 346
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    return v0
.end method

.method public getCbGrpprlPapx()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 364
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    return v0
.end method

.method public getDxaIndentSav()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 310
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    return v0
.end method

.method public getGrfhic()Lorg/apache/poi/hwpf/model/Grfhic;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 400
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    return-object v0
.end method

.method public getIStartAt()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 220
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    return v0
.end method

.method public getIlvlRestartLim()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 382
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    return v0
.end method

.method public getInfo()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 256
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    return v0
.end method

.method public getIxchFollow()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 292
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    return v0
.end method

.method public getJc()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 429
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->jc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getNfc()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 238
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    return v0
.end method

.method public getRgbxchNums()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    return-object v0
.end method

.method public getUnused2()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 328
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 160
    const/16 v0, 0x1f

    .line 161
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 162
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    add-int/lit8 v1, v2, 0x1f

    .line 163
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    add-int v1, v2, v3

    .line 164
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    add-int v1, v2, v3

    .line 165
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v1, v2, v3

    .line 166
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    add-int v1, v2, v3

    .line 167
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    add-int v1, v2, v3

    .line 168
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    add-int v1, v2, v3

    .line 169
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    add-int v1, v2, v3

    .line 170
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    add-int v1, v2, v3

    .line 171
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    add-int v1, v2, v3

    .line 172
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/Grfhic;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 173
    return v1
.end method

.method public isFConverted()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 509
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fConverted:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFIndentSav()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 489
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fIndentSav:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLegal()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 449
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fLegal:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNoRestart()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 469
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fNoRestart:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFTentative()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 551
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fTentative:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused1()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 531
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 89
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 90
    add-int/lit8 v0, p2, 0x4

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    aput-byte v1, p1, v0

    .line 91
    add-int/lit8 v0, p2, 0x5

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    aput-byte v1, p1, v0

    .line 92
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    const/4 v1, 0x0

    add-int/lit8 v2, p2, 0x6

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    array-length v3, v3

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    add-int/lit8 v0, p2, 0xf

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    aput-byte v1, p1, v0

    .line 94
    add-int/lit8 v0, p2, 0x10

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 95
    add-int/lit8 v0, p2, 0x14

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 96
    add-int/lit8 v0, p2, 0x18

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUByte([BIS)V

    .line 97
    add-int/lit8 v0, p2, 0x19

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUByte([BIS)V

    .line 98
    add-int/lit8 v0, p2, 0x1a

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUByte([BIS)V

    .line 99
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    add-int/lit8 v1, p2, 0x1b

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hwpf/model/Grfhic;->serialize([BI)V

    .line 100
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 104
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 105
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->serialize([BI)V

    .line 106
    return-object v0
.end method

.method public setCbGrpprlChpx(S)V
    .locals 0
    .param p1, "field_8_cbGrpprlChpx"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 355
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_8_cbGrpprlChpx:S

    .line 356
    return-void
.end method

.method public setCbGrpprlPapx(S)V
    .locals 0
    .param p1, "field_9_cbGrpprlPapx"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 373
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_9_cbGrpprlPapx:S

    .line 374
    return-void
.end method

.method public setDxaIndentSav(I)V
    .locals 0
    .param p1, "field_6_dxaIndentSav"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 319
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_6_dxaIndentSav:I

    .line 320
    return-void
.end method

.method public setFConverted(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 499
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fConverted:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 500
    return-void
.end method

.method public setFIndentSav(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 479
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fIndentSav:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 480
    return-void
.end method

.method public setFLegal(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 439
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fLegal:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 440
    return-void
.end method

.method public setFNoRestart(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 459
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fNoRestart:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 460
    return-void
.end method

.method public setFTentative(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 541
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->fTentative:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 542
    return-void
.end method

.method public setGrfhic(Lorg/apache/poi/hwpf/model/Grfhic;)V
    .locals 0
    .param p1, "field_11_grfhic"    # Lorg/apache/poi/hwpf/model/Grfhic;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 409
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_11_grfhic:Lorg/apache/poi/hwpf/model/Grfhic;

    .line 410
    return-void
.end method

.method public setIStartAt(I)V
    .locals 0
    .param p1, "field_1_iStartAt"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 229
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_1_iStartAt:I

    .line 230
    return-void
.end method

.method public setIlvlRestartLim(S)V
    .locals 0
    .param p1, "field_10_ilvlRestartLim"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 391
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_10_ilvlRestartLim:S

    .line 392
    return-void
.end method

.method public setInfo(B)V
    .locals 0
    .param p1, "field_3_info"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 265
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 266
    return-void
.end method

.method public setIxchFollow(B)V
    .locals 0
    .param p1, "field_5_ixchFollow"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 301
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_5_ixchFollow:B

    .line 302
    return-void
.end method

.method public setJc(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 419
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->jc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 420
    return-void
.end method

.method public setNfc(B)V
    .locals 0
    .param p1, "field_2_nfc"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 247
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_2_nfc:B

    .line 248
    return-void
.end method

.method public setRgbxchNums([B)V
    .locals 0
    .param p1, "field_4_rgbxchNums"    # [B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 283
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_4_rgbxchNums:[B

    .line 284
    return-void
.end method

.method public setUnused1(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 519
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_3_info:B

    .line 520
    return-void
.end method

.method public setUnused2(I)V
    .locals 0
    .param p1, "field_7_unused2"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 337
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->field_7_unused2:I

    .line 338
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[LVLF]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v1, "    .iStartAt             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getIStartAt()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v1, "    .nfc                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getNfc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v1, "    .info                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getInfo()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v1, "         .jc                       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getJc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v1, "         .fLegal                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->isFLegal()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v1, "         .fNoRestart               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->isFNoRestart()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v1, "         .fIndentSav               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->isFIndentSav()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v1, "         .fConverted               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->isFConverted()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v1, "         .unused1                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->isUnused1()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 192
    const-string/jumbo v1, "         .fTentative               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->isFTentative()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    const-string/jumbo v1, "    .rgbxchNums           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getRgbxchNums()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string/jumbo v1, "    .ixchFollow           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getIxchFollow()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v1, "    .dxaIndentSav         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getDxaIndentSav()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v1, "    .unused2              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getUnused2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v1, "    .cbGrpprlChpx         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getCbGrpprlChpx()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v1, "    .cbGrpprlPapx         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getCbGrpprlPapx()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v1, "    .ilvlRestartLim       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getIlvlRestartLim()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string/jumbo v1, "    .grfhic               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LVLFAbstractType;->getGrfhic()Lorg/apache/poi/hwpf/model/Grfhic;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string/jumbo v1, "[/LVLF]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
