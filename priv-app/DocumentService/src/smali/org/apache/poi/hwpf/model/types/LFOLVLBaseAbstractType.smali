.class public abstract Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;
.super Ljava/lang/Object;
.source "LFOLVLBaseAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final fFormatting:Lorg/apache/poi/util/BitField;

.field private static final fStartAt:Lorg/apache/poi/util/BitField;

.field private static final grfhic:Lorg/apache/poi/util/BitField;

.field private static final iLvl:Lorg/apache/poi/util/BitField;

.field private static final unused1:Lorg/apache/poi/util/BitField;

.field private static final unused2:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_iStartAt:I

.field protected field_2_flags:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->iLvl:Lorg/apache/poi/util/BitField;

    .line 47
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->fStartAt:Lorg/apache/poi/util/BitField;

    .line 48
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->fFormatting:Lorg/apache/poi/util/BitField;

    .line 49
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x3fc0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->grfhic:Lorg/apache/poi/util/BitField;

    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0x1fffc000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, -0x20000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->unused2:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 81
    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    if-ne p0, p1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 90
    goto :goto_0

    .line 91
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 92
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 93
    check-cast v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;

    .line 94
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 95
    goto :goto_0

    .line 96
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 97
    goto :goto_0
.end method

.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 59
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    .line 60
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 61
    return-void
.end method

.method public getFlags()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 154
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    return v0
.end method

.method public getGrfhic()S
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 243
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->grfhic:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getILvl()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->iLvl:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getIStartAt()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 136
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    return v0
.end method

.method public getUnused1()S
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 265
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getUnused2()B
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 287
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->unused2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 104
    const/16 v0, 0x1f

    .line 105
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 106
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    add-int/lit8 v1, v2, 0x1f

    .line 107
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    add-int v1, v2, v3

    .line 108
    return v1
.end method

.method public isFFormatting()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 223
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->fFormatting:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFStartAt()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 203
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->fStartAt:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 65
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 66
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 67
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 72
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->serialize([BI)V

    .line 73
    return-object v0
.end method

.method public setFFormatting(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 213
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->fFormatting:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 214
    return-void
.end method

.method public setFStartAt(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 193
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->fStartAt:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 194
    return-void
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "field_2_flags"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 163
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 164
    return-void
.end method

.method public setGrfhic(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 233
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->grfhic:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 234
    return-void
.end method

.method public setILvl(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 173
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->iLvl:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 174
    return-void
.end method

.method public setIStartAt(I)V
    .locals 0
    .param p1, "field_1_iStartAt"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 145
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_1_iStartAt:I

    .line 146
    return-void
.end method

.method public setUnused1(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 253
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 254
    return-void
.end method

.method public setUnused2(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 275
    sget-object v0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->unused2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->field_2_flags:I

    .line 276
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[LFOLVLBase]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v1, "    .iStartAt             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getIStartAt()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v1, "    .flags                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getFlags()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string/jumbo v1, "         .iLvl                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getILvl()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 120
    const-string/jumbo v1, "         .fStartAt                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->isFStartAt()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 121
    const-string/jumbo v1, "         .fFormatting              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->isFFormatting()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v1, "         .grfhic                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getGrfhic()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 123
    const-string/jumbo v1, "         .unused1                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getUnused1()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 124
    const-string/jumbo v1, "         .unused2                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/LFOLVLBaseAbstractType;->getUnused2()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 126
    const-string/jumbo v1, "[/LFOLVLBase]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
