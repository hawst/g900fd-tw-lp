.class public Lorg/apache/poi/hwpf/model/Colorref;
.super Ljava/lang/Object;
.source "Colorref.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private value:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    .line 77
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput p1, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    .line 87
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    .line 82
    return-void
.end method

.method public static valueOfIco(I)Lorg/apache/poi/hwpf/model/Colorref;
    .locals 2
    .param p0, "ico"    # I

    .prologue
    const/4 v1, 0x0

    .line 33
    packed-switch p0, :pswitch_data_0

    .line 68
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    :goto_0
    return-object v0

    .line 36
    :pswitch_0
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 38
    :pswitch_1
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const/high16 v1, 0xff0000

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 40
    :pswitch_2
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xffff00

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 42
    :pswitch_3
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xff00

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 44
    :pswitch_4
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xff00ff

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 46
    :pswitch_5
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 48
    :pswitch_6
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xffff

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 50
    :pswitch_7
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xffffff

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 52
    :pswitch_8
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const/high16 v1, 0x8b0000

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 54
    :pswitch_9
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0x8b8b00

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 56
    :pswitch_a
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const/16 v1, 0x6400

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 58
    :pswitch_b
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0x8b008b

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 60
    :pswitch_c
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const/16 v1, 0x8b

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 62
    :pswitch_d
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xccff

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 64
    :pswitch_e
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xa9a9a9

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto :goto_0

    .line 66
    :pswitch_f
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    const v1, 0xc0c0c0

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    goto/16 :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Colorref;->clone()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/model/Colorref;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    iget v1, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    if-ne p0, p1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v1

    .line 100
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 101
    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 103
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 104
    check-cast v0, Lorg/apache/poi/hwpf/model/Colorref;

    .line 105
    .local v0, "other":Lorg/apache/poi/hwpf/model/Colorref;
    iget v3, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 106
    goto :goto_0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 123
    iget v0, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    invoke-static {p1, p2, v0}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 129
    return-void
.end method

.method public setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 133
    iput p1, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    .line 134
    return-void
.end method

.method public toByteArray()[B
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Colorref;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 140
    const-string/jumbo v2, "Structure state (EMPTY) is not good for serialization"

    .line 139
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :cond_0
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 143
    .local v0, "bs":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/Colorref;->serialize([BI)V

    .line 144
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Colorref;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string/jumbo v0, "[COLORREF] EMPTY"

    .line 153
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[COLORREF] 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/poi/hwpf/model/Colorref;->value:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
