.class public Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;
.super Ljava/lang/Object;
.source "ObjectPoolImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/ObjectsPool;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _objectPool:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V
    .locals 0
    .param p1, "_objectPool"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .line 36
    return-void
.end method


# virtual methods
.method public getObjectById(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;
    .locals 3
    .param p1, "objId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    if-nez v2, :cond_0

    .line 49
    :goto_0
    return-object v1

    .line 45
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    invoke-interface {v2, p1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    .line 49
    .local v0, "exc":Ljava/io/FileNotFoundException;
    goto :goto_0
.end method

.method public writeTo(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V
    .locals 1
    .param p1, "directoryEntry"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    invoke-static {v0, p1}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodeRecursively(Lorg/apache/poi/poifs/filesystem/Entry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    .line 58
    :cond_0
    return-void
.end method
