.class public abstract Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;
.super Ljava/lang/Object;
.source "FibBaseAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final cQuickSaves:Lorg/apache/poi/util/BitField;

.field private static final fComplex:Lorg/apache/poi/util/BitField;

.field private static final fDot:Lorg/apache/poi/util/BitField;

.field private static final fEmptySpecial:Lorg/apache/poi/util/BitField;

.field private static final fEncrypted:Lorg/apache/poi/util/BitField;

.field private static final fExtChar:Lorg/apache/poi/util/BitField;

.field private static final fFarEast:Lorg/apache/poi/util/BitField;

.field private static final fGlsy:Lorg/apache/poi/util/BitField;

.field private static final fHasPic:Lorg/apache/poi/util/BitField;

.field private static final fLoadOverride:Lorg/apache/poi/util/BitField;

.field private static final fLoadOverridePage:Lorg/apache/poi/util/BitField;

.field private static final fMac:Lorg/apache/poi/util/BitField;

.field private static final fObfuscated:Lorg/apache/poi/util/BitField;

.field private static final fReadOnlyRecommended:Lorg/apache/poi/util/BitField;

.field private static final fSpare0:Lorg/apache/poi/util/BitField;

.field private static final fWhichTblStm:Lorg/apache/poi/util/BitField;

.field private static final fWriteReservation:Lorg/apache/poi/util/BitField;

.field private static final reserved1:Lorg/apache/poi/util/BitField;

.field private static final reserved2:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_10_flags2:B

.field protected field_11_Chs:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_12_chsTables:S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_13_fcMin:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_14_fcMac:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_1_wIdent:I

.field protected field_2_nFib:I

.field protected field_3_unused:I

.field protected field_4_lid:I

.field protected field_5_pnNext:I

.field protected field_6_flags1:S

.field protected field_7_nFibBack:I

.field protected field_8_lKey:I

.field protected field_9_envr:B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fDot:Lorg/apache/poi/util/BitField;

    .line 52
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fGlsy:Lorg/apache/poi/util/BitField;

    .line 53
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v4}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fComplex:Lorg/apache/poi/util/BitField;

    .line 54
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v5}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fHasPic:Lorg/apache/poi/util/BitField;

    .line 55
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xf0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->cQuickSaves:Lorg/apache/poi/util/BitField;

    .line 56
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fEncrypted:Lorg/apache/poi/util/BitField;

    .line 57
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fWhichTblStm:Lorg/apache/poi/util/BitField;

    .line 58
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fReadOnlyRecommended:Lorg/apache/poi/util/BitField;

    .line 59
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x800

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fWriteReservation:Lorg/apache/poi/util/BitField;

    .line 60
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fExtChar:Lorg/apache/poi/util/BitField;

    .line 61
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fLoadOverride:Lorg/apache/poi/util/BitField;

    .line 62
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x4000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fFarEast:Lorg/apache/poi/util/BitField;

    .line 63
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0x8000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fObfuscated:Lorg/apache/poi/util/BitField;

    .line 69
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fMac:Lorg/apache/poi/util/BitField;

    .line 70
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fEmptySpecial:Lorg/apache/poi/util/BitField;

    .line 71
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v4}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fLoadOverridePage:Lorg/apache/poi/util/BitField;

    .line 72
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v5}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->reserved1:Lorg/apache/poi/util/BitField;

    .line 73
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->reserved2:Lorg/apache/poi/util/BitField;

    .line 74
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xfe

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fSpare0:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 136
    const/16 v0, 0x20

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 90
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_1_wIdent:I

    .line 91
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_2_nFib:I

    .line 92
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_3_unused:I

    .line 93
    add-int/lit8 v0, p2, 0x6

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_4_lid:I

    .line 94
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_5_pnNext:I

    .line 95
    add-int/lit8 v0, p2, 0xa

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 96
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_7_nFibBack:I

    .line 97
    add-int/lit8 v0, p2, 0xe

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_8_lKey:I

    .line 98
    add-int/lit8 v0, p2, 0x12

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_9_envr:B

    .line 99
    add-int/lit8 v0, p2, 0x13

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 100
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_11_Chs:S

    .line 101
    add-int/lit8 v0, p2, 0x16

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_12_chsTables:S

    .line 102
    add-int/lit8 v0, p2, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_13_fcMin:I

    .line 103
    add-int/lit8 v0, p2, 0x1c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_14_fcMac:I

    .line 104
    return-void
.end method

.method public getCQuickSaves()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 544
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->cQuickSaves:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getChs()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 381
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_11_Chs:S

    return v0
.end method

.method public getChsTables()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 399
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_12_chsTables:S

    return v0
.end method

.method public getEnvr()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 345
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_9_envr:B

    return v0
.end method

.method public getFSpare0()B
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 834
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fSpare0:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getFcMac()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 435
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_14_fcMac:I

    return v0
.end method

.method public getFcMin()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 417
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_13_fcMin:I

    return v0
.end method

.method public getFlags1()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 291
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    return v0
.end method

.method public getFlags2()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 363
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    return v0
.end method

.method public getLKey()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 327
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_8_lKey:I

    return v0
.end method

.method public getLid()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 255
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_4_lid:I

    return v0
.end method

.method public getNFib()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 219
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_2_nFib:I

    return v0
.end method

.method public getNFibBack()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 309
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_7_nFibBack:I

    return v0
.end method

.method public getPnNext()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 273
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_5_pnNext:I

    return v0
.end method

.method public getUnused()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 237
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_3_unused:I

    return v0
.end method

.method public getWIdent()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 201
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_1_wIdent:I

    return v0
.end method

.method public isFComplex()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 504
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fComplex:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFDot()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 464
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fDot:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEmptySpecial()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 748
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fEmptySpecial:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEncrypted()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 564
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fEncrypted:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFExtChar()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 644
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fExtChar:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFarEast()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 684
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fFarEast:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFGlsy()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 484
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fGlsy:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHasPic()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 524
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fHasPic:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLoadOverride()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 664
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fLoadOverride:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLoadOverridePage()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 768
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fLoadOverridePage:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMac()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 726
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fMac:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFObfuscated()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 704
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fObfuscated:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFReadOnlyRecommended()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 604
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fReadOnlyRecommended:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWhichTblStm()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 584
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fWhichTblStm:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWriteReservation()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 624
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fWriteReservation:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isReserved1()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 790
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->reserved1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isReserved2()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 812
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->reserved2:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 108
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_1_wIdent:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 109
    add-int/lit8 v0, p2, 0x2

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_2_nFib:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 110
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_3_unused:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 111
    add-int/lit8 v0, p2, 0x6

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_4_lid:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 112
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_5_pnNext:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 113
    add-int/lit8 v0, p2, 0xa

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 114
    add-int/lit8 v0, p2, 0xc

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_7_nFibBack:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 115
    add-int/lit8 v0, p2, 0xe

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_8_lKey:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 116
    add-int/lit8 v0, p2, 0x12

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_9_envr:B

    aput-byte v1, p1, v0

    .line 117
    add-int/lit8 v0, p2, 0x13

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    aput-byte v1, p1, v0

    .line 118
    add-int/lit8 v0, p2, 0x14

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_11_Chs:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 119
    add-int/lit8 v0, p2, 0x16

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_12_chsTables:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 120
    add-int/lit8 v0, p2, 0x18

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_13_fcMin:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 121
    add-int/lit8 v0, p2, 0x1c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_14_fcMac:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 122
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 127
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->serialize([BI)V

    .line 128
    return-object v0
.end method

.method public setCQuickSaves(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 534
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->cQuickSaves:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 535
    return-void
.end method

.method public setChs(S)V
    .locals 0
    .param p1, "field_11_Chs"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 390
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_11_Chs:S

    .line 391
    return-void
.end method

.method public setChsTables(S)V
    .locals 0
    .param p1, "field_12_chsTables"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 408
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_12_chsTables:S

    .line 409
    return-void
.end method

.method public setEnvr(B)V
    .locals 0
    .param p1, "field_9_envr"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 354
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_9_envr:B

    .line 355
    return-void
.end method

.method public setFComplex(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 494
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fComplex:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 495
    return-void
.end method

.method public setFDot(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 454
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fDot:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 455
    return-void
.end method

.method public setFEmptySpecial(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 736
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fEmptySpecial:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 737
    return-void
.end method

.method public setFEncrypted(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 554
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fEncrypted:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 555
    return-void
.end method

.method public setFExtChar(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 634
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fExtChar:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 635
    return-void
.end method

.method public setFFarEast(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 674
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fFarEast:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 675
    return-void
.end method

.method public setFGlsy(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 474
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fGlsy:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 475
    return-void
.end method

.method public setFHasPic(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 514
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fHasPic:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 515
    return-void
.end method

.method public setFLoadOverride(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 654
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fLoadOverride:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 655
    return-void
.end method

.method public setFLoadOverridePage(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 758
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fLoadOverridePage:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 759
    return-void
.end method

.method public setFMac(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 714
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fMac:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 715
    return-void
.end method

.method public setFObfuscated(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 694
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fObfuscated:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 695
    return-void
.end method

.method public setFReadOnlyRecommended(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 594
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fReadOnlyRecommended:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 595
    return-void
.end method

.method public setFSpare0(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 822
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fSpare0:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 823
    return-void
.end method

.method public setFWhichTblStm(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 574
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fWhichTblStm:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 575
    return-void
.end method

.method public setFWriteReservation(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 614
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->fWriteReservation:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 615
    return-void
.end method

.method public setFcMac(I)V
    .locals 0
    .param p1, "field_14_fcMac"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 444
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_14_fcMac:I

    .line 445
    return-void
.end method

.method public setFcMin(I)V
    .locals 0
    .param p1, "field_13_fcMin"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 426
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_13_fcMin:I

    .line 427
    return-void
.end method

.method public setFlags1(S)V
    .locals 0
    .param p1, "field_6_flags1"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 300
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_6_flags1:S

    .line 301
    return-void
.end method

.method public setFlags2(B)V
    .locals 0
    .param p1, "field_10_flags2"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 372
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 373
    return-void
.end method

.method public setLKey(I)V
    .locals 0
    .param p1, "field_8_lKey"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 336
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_8_lKey:I

    .line 337
    return-void
.end method

.method public setLid(I)V
    .locals 0
    .param p1, "field_4_lid"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 264
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_4_lid:I

    .line 265
    return-void
.end method

.method public setNFib(I)V
    .locals 0
    .param p1, "field_2_nFib"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 228
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_2_nFib:I

    .line 229
    return-void
.end method

.method public setNFibBack(I)V
    .locals 0
    .param p1, "field_7_nFibBack"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 318
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_7_nFibBack:I

    .line 319
    return-void
.end method

.method public setPnNext(I)V
    .locals 0
    .param p1, "field_5_pnNext"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 282
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_5_pnNext:I

    .line 283
    return-void
.end method

.method public setReserved1(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 778
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->reserved1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 779
    return-void
.end method

.method public setReserved2(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 800
    sget-object v0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->reserved2:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_10_flags2:B

    .line 801
    return-void
.end method

.method public setUnused(I)V
    .locals 0
    .param p1, "field_3_unused"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 246
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_3_unused:I

    .line 247
    return-void
.end method

.method public setWIdent(I)V
    .locals 0
    .param p1, "field_1_wIdent"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 210
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->field_1_wIdent:I

    .line 211
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[FibBase]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string/jumbo v1, "    .wIdent               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getWIdent()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, "    .nFib                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getNFib()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v1, "    .unused               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getUnused()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v1, "    .lid                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getLid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string/jumbo v1, "    .pnNext               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getPnNext()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string/jumbo v1, "    .flags1               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getFlags1()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string/jumbo v1, "         .fDot                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFDot()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 156
    const-string/jumbo v1, "         .fGlsy                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFGlsy()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 157
    const-string/jumbo v1, "         .fComplex                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFComplex()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    const-string/jumbo v1, "         .fHasPic                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFHasPic()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    const-string/jumbo v1, "         .cQuickSaves              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getCQuickSaves()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 160
    const-string/jumbo v1, "         .fEncrypted               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFEncrypted()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    const-string/jumbo v1, "         .fWhichTblStm             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFWhichTblStm()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    const-string/jumbo v1, "         .fReadOnlyRecommended     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFReadOnlyRecommended()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 163
    const-string/jumbo v1, "         .fWriteReservation        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFWriteReservation()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    const-string/jumbo v1, "         .fExtChar                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFExtChar()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 165
    const-string/jumbo v1, "         .fLoadOverride            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFLoadOverride()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    const-string/jumbo v1, "         .fFarEast                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFFarEast()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    const-string/jumbo v1, "         .fObfuscated              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFObfuscated()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    const-string/jumbo v1, "    .nFibBack             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getNFibBack()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string/jumbo v1, "    .lKey                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getLKey()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string/jumbo v1, "    .envr                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getEnvr()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v1, "    .flags2               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getFlags2()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v1, "         .fMac                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFMac()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v1, "         .fEmptySpecial            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFEmptySpecial()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 178
    const-string/jumbo v1, "         .fLoadOverridePage        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isFLoadOverridePage()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v1, "         .reserved1                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isReserved1()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v1, "         .reserved2                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->isReserved2()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 181
    const-string/jumbo v1, "         .fSpare0                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getFSpare0()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v1, "    .Chs                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getChs()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v1, "    .chsTables            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getChsTables()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v1, "    .fcMin                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getFcMin()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v1, "    .fcMac                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibBaseAbstractType;->getFcMac()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v1, "[/FibBase]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
