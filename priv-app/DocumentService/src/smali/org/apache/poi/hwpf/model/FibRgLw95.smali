.class Lorg/apache/poi/hwpf/model/FibRgLw95;
.super Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;
.source "FibRgLw95.java"

# interfaces
.implements Lorg/apache/poi/hwpf/model/FibRgLw;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$poi$hwpf$model$SubdocumentType:[I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$poi$hwpf$model$SubdocumentType()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lorg/apache/poi/hwpf/model/FibRgLw95;->$SWITCH_TABLE$org$apache$poi$hwpf$model$SubdocumentType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/hwpf/model/SubdocumentType;->values()[Lorg/apache/poi/hwpf/model/SubdocumentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->ANNOTATION:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENDNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->MACRO:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lorg/apache/poi/hwpf/model/FibRgLw95;->$SWITCH_TABLE$org$apache$poi$hwpf$model$SubdocumentType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;-><init>()V

    .line 43
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->fillFields([BI)V

    .line 44
    return-void
.end method


# virtual methods
.method public getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I
    .locals 3
    .param p1, "subdocumentType"    # Lorg/apache/poi/hwpf/model/SubdocumentType;

    .prologue
    .line 49
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgLw95;->$SWITCH_TABLE$org$apache$poi$hwpf$model$SubdocumentType()[I

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unsupported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpText()I

    move-result v0

    .line 66
    :goto_0
    return v0

    .line 54
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpFtn()I

    move-result v0

    goto :goto_0

    .line 56
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpHdd()I

    move-result v0

    goto :goto_0

    .line 58
    :pswitch_3
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpMcr()I

    move-result v0

    goto :goto_0

    .line 60
    :pswitch_4
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpAtn()I

    move-result v0

    goto :goto_0

    .line 62
    :pswitch_5
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpEdn()I

    move-result v0

    goto :goto_0

    .line 64
    :pswitch_6
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpTxbx()I

    move-result v0

    goto :goto_0

    .line 66
    :pswitch_7
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/FibRgLw95;->getCcpHdrTxbx()I

    move-result v0

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;I)V
    .locals 3
    .param p1, "subdocumentType"    # Lorg/apache/poi/hwpf/model/SubdocumentType;
    .param p2, "newLength"    # I

    .prologue
    .line 76
    invoke-static {}, Lorg/apache/poi/hwpf/model/FibRgLw95;->$SWITCH_TABLE$org$apache$poi$hwpf$model$SubdocumentType()[I

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/SubdocumentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 103
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unsupported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :pswitch_0
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpText(I)V

    .line 101
    :goto_0
    return-void

    .line 82
    :pswitch_1
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpFtn(I)V

    goto :goto_0

    .line 85
    :pswitch_2
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpHdd(I)V

    goto :goto_0

    .line 88
    :pswitch_3
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCbMac(I)V

    goto :goto_0

    .line 91
    :pswitch_4
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpAtn(I)V

    goto :goto_0

    .line 94
    :pswitch_5
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpEdn(I)V

    goto :goto_0

    .line 97
    :pswitch_6
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpTxbx(I)V

    goto :goto_0

    .line 100
    :pswitch_7
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/model/FibRgLw95;->setCcpHdrTxbx(I)V

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
