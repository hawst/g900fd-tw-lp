.class public Lorg/apache/poi/hwpf/converter/AbstractWordUtils;
.super Ljava/lang/Object;
.source "AbstractWordUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;
    }
.end annotation


# static fields
.field static final EMPTY:Ljava/lang/String; = ""

.field public static final TWIPS_PER_INCH:F = 1440.0f

.field public static final TWIPS_PER_PT:I = 0x14

.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 54
    sput-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->logger:Lorg/apache/poi/util/POILogger;

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static buildTableCellEdgesArray(Lorg/apache/poi/hwpf/usermodel/Table;)[I
    .locals 10
    .param p0, "table"    # Lorg/apache/poi/hwpf/usermodel/Table;

    .prologue
    .line 71
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 73
    .local v1, "edges":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "r":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/Table;->numRows()I

    move-result v8

    if-lt v3, v8, :cond_0

    .line 86
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/Integer;

    invoke-interface {v1, v8}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Integer;

    .line 87
    .local v5, "sorted":[Ljava/lang/Integer;
    array-length v8, v5

    new-array v4, v8, [I

    .line 88
    .local v4, "result":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v5

    if-lt v2, v8, :cond_2

    .line 93
    return-object v4

    .line 75
    .end local v2    # "i":I
    .end local v4    # "result":[I
    .end local v5    # "sorted":[Ljava/lang/Integer;
    :cond_0
    invoke-virtual {p0, v3}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v7

    .line 76
    .local v7, "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_2
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v8

    if-lt v0, v8, :cond_1

    .line 73
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v7, v0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v6

    .line 80
    .local v6, "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getLeftEdge()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getLeftEdge()I

    move-result v8

    .line 82
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/TableCell;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    .line 81
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 90
    .end local v0    # "c":I
    .end local v6    # "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    .end local v7    # "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    .restart local v2    # "i":I
    .restart local v4    # "result":[I
    .restart local v5    # "sorted":[Ljava/lang/Integer;
    :cond_2
    aget-object v8, v5, v2

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v4, v2

    .line 88
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static canBeMerged(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Ljava/lang/String;)Z
    .locals 11
    .param p0, "node1"    # Lorg/w3c/dom/Node;
    .param p1, "node2"    # Lorg/w3c/dom/Node;
    .param p2, "requiredTagName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 98
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v9

    if-ne v9, v8, :cond_0

    .line 99
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v9

    if-eq v9, v8, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v7

    :cond_1
    move-object v4, p0

    .line 102
    check-cast v4, Lorg/w3c/dom/Element;

    .local v4, "element1":Lorg/w3c/dom/Element;
    move-object v5, p1

    .line 103
    check-cast v5, Lorg/w3c/dom/Element;

    .line 105
    .local v5, "element2":Lorg/w3c/dom/Element;
    invoke-interface {v4}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v9

    invoke-static {p2, v9}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 106
    invoke-interface {v5}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v9

    invoke-static {p2, v9}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 109
    invoke-interface {v4}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 110
    .local v2, "attributes1":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v5}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v3

    .line 112
    .local v3, "attributes2":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v9

    invoke-interface {v3}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v10

    if-ne v9, v10, :cond_0

    .line 115
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v9

    if-lt v6, v9, :cond_2

    move v7, v8

    .line 130
    goto :goto_0

    .line 117
    :cond_2
    invoke-interface {v2, v6}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Attr;

    .line 119
    .local v0, "attr1":Lorg/w3c/dom/Attr;
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 121
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getLocalName()Ljava/lang/String;

    move-result-object v10

    .line 120
    invoke-interface {v3, v9, v10}, Lorg/w3c/dom/NamedNodeMap;->getNamedItemNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Attr;

    .line 125
    .local v1, "attr2":Lorg/w3c/dom/Attr;
    :goto_2
    if-eqz v1, :cond_0

    .line 126
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getTextContent()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, Lorg/w3c/dom/Attr;->getTextContent()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 115
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 123
    .end local v1    # "attr2":Lorg/w3c/dom/Attr;
    :cond_3
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Attr;

    .restart local v1    # "attr2":Lorg/w3c/dom/Attr;
    goto :goto_2
.end method

.method static compactChildNodesR(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .locals 6
    .param p0, "parentElement"    # Lorg/w3c/dom/Element;
    .param p1, "childTagName"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 136
    .local v3, "childNodes":Lorg/w3c/dom/NodeList;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    .line 150
    invoke-interface {p0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 151
    const/4 v4, 0x0

    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_3

    .line 159
    return-void

    .line 138
    :cond_0
    invoke-interface {v3, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 139
    .local v1, "child1":Lorg/w3c/dom/Node;
    add-int/lit8 v5, v4, 0x1

    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 140
    .local v2, "child2":Lorg/w3c/dom/Node;
    invoke-static {v1, v2, p1}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->canBeMerged(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 136
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 145
    :cond_1
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v1, v5}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 144
    :cond_2
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-gtz v5, :cond_1

    .line 146
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5, v2}, Lorg/w3c/dom/Node;->removeChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 147
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 153
    .end local v1    # "child1":Lorg/w3c/dom/Node;
    .end local v2    # "child2":Lorg/w3c/dom/Node;
    :cond_3
    invoke-interface {v3, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 154
    .local v0, "child":Lorg/w3c/dom/Node;
    instance-of v5, v0, Lorg/w3c/dom/Element;

    if-eqz v5, :cond_4

    .line 156
    check-cast v0, Lorg/w3c/dom/Element;

    .end local v0    # "child":Lorg/w3c/dom/Node;
    invoke-static {v0, p1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->compactChildNodesR(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    .line 151
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method static equals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "str1"    # Ljava/lang/String;
    .param p1, "str2"    # Ljava/lang/String;

    .prologue
    .line 163
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static getBorderType(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;
    .locals 2
    .param p0, "borderCode"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 168
    if-nez p0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "borderCode is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getBorderType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 211
    :pswitch_0
    const-string/jumbo v0, "solid"

    :goto_0
    return-object v0

    .line 175
    :pswitch_1
    const-string/jumbo v0, "solid"

    goto :goto_0

    .line 177
    :pswitch_2
    const-string/jumbo v0, "double"

    goto :goto_0

    .line 179
    :pswitch_3
    const-string/jumbo v0, "solid"

    goto :goto_0

    .line 181
    :pswitch_4
    const-string/jumbo v0, "dotted"

    goto :goto_0

    .line 184
    :pswitch_5
    const-string/jumbo v0, "dashed"

    goto :goto_0

    .line 186
    :pswitch_6
    const-string/jumbo v0, "dotted"

    goto :goto_0

    .line 197
    :pswitch_7
    const-string/jumbo v0, "double"

    goto :goto_0

    .line 199
    :pswitch_8
    const-string/jumbo v0, "solid"

    goto :goto_0

    .line 201
    :pswitch_9
    const-string/jumbo v0, "double"

    goto :goto_0

    .line 203
    :pswitch_a
    const-string/jumbo v0, "dashed"

    goto :goto_0

    .line 205
    :pswitch_b
    const-string/jumbo v0, "dashed"

    goto :goto_0

    .line 207
    :pswitch_c
    const-string/jumbo v0, "ridge"

    goto :goto_0

    .line 209
    :pswitch_d
    const-string/jumbo v0, "grooved"

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static getBorderWidth(Lorg/apache/poi/hwpf/usermodel/BorderCode;)Ljava/lang/String;
    .locals 5
    .param p0, "borderCode"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 217
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->getLineWidth()I

    move-result v0

    .line 218
    .local v0, "lineWidth":I
    div-int/lit8 v1, v0, 0x8

    .line 219
    .local v1, "pt":I
    mul-int/lit8 v4, v1, 0x8

    sub-int v2, v0, v4

    .line 221
    .local v2, "pte":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 223
    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    mul-int/lit8 v4, v2, 0x7d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 225
    const-string/jumbo v4, "pt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static getBulletText(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;Lorg/apache/poi/hwpf/usermodel/HWPFList;C)Ljava/lang/String;
    .locals 13
    .param p0, "numberingState"    # Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;
    .param p1, "list"    # Lorg/apache/poi/hwpf/usermodel/HWPFList;
    .param p2, "level"    # C

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 240
    .local v0, "bulletBuffer":Ljava/lang/StringBuffer;
    invoke-virtual {p1, p2}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getNumberText(C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .line 241
    .local v8, "xst":[C
    array-length v11, v8

    const/4 v9, 0x0

    move v10, v9

    :goto_0
    if-lt v10, v11, :cond_0

    .line 284
    invoke-virtual {p1, p2}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getTypeOfCharFollowingTheNumber(C)B

    move-result v3

    .line 285
    .local v3, "follow":B
    packed-switch v3, :pswitch_data_0

    .line 297
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 241
    .end local v3    # "follow":B
    :cond_0
    aget-char v2, v8, v10

    .line 243
    .local v2, "element":C
    const/16 v9, 0x9

    if-ge v2, v9, :cond_5

    .line 245
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getLsid()I

    move-result v6

    .line 246
    .local v6, "lsid":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v12, "#"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 249
    .local v5, "key":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->isStartAtOverriden(C)Z

    move-result v9

    if-nez v9, :cond_3

    .line 250
    # getter for: Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->access$0(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 252
    # getter for: Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->access$0(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 253
    .local v7, "num":I
    if-ne p2, v2, :cond_1

    .line 255
    add-int/lit8 v7, v7, 0x1

    .line 256
    # getter for: Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->access$0(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;)Ljava/util/Map;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v9, v5, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :cond_1
    :goto_2
    if-ne p2, v2, :cond_2

    .line 268
    add-int/lit8 v4, v2, 0x1

    .local v4, "i":I
    :goto_3
    const/16 v9, 0x9

    if-lt v4, v9, :cond_4

    .line 276
    .end local v4    # "i":I
    :cond_2
    invoke-virtual {p1, p2}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getNumberFormat(C)I

    move-result v9

    .line 275
    invoke-static {v7, v9}, Lorg/apache/poi/hwpf/converter/NumberFormatter;->getNumber(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 241
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "lsid":I
    .end local v7    # "num":I
    :goto_4
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_0

    .line 261
    .restart local v5    # "key":Ljava/lang/String;
    .restart local v6    # "lsid":I
    :cond_3
    invoke-virtual {p1, v2}, Lorg/apache/poi/hwpf/usermodel/HWPFList;->getStartAt(C)I

    move-result v7

    .line 262
    .restart local v7    # "num":I
    # getter for: Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->access$0(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;)Ljava/util/Map;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v9, v5, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 270
    .restart local v4    # "i":I
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v12, "#"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "childKey":Ljava/lang/String;
    # getter for: Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->levels:Ljava/util/Map;
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;->access$0(Lorg/apache/poi/hwpf/converter/AbstractWordUtils$NumberingState;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 280
    .end local v1    # "childKey":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "lsid":I
    .end local v7    # "num":I
    :cond_5
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 288
    .end local v2    # "element":C
    .restart local v3    # "follow":B
    :pswitch_0
    const-string/jumbo v9, "\t"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 291
    :pswitch_1
    const-string/jumbo v9, " "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getColor(I)Ljava/lang/String;
    .locals 1
    .param p0, "ico"    # I

    .prologue
    .line 302
    packed-switch p0, :pswitch_data_0

    .line 337
    const-string/jumbo v0, "black"

    :goto_0
    return-object v0

    .line 305
    :pswitch_0
    const-string/jumbo v0, "black"

    goto :goto_0

    .line 307
    :pswitch_1
    const-string/jumbo v0, "blue"

    goto :goto_0

    .line 309
    :pswitch_2
    const-string/jumbo v0, "cyan"

    goto :goto_0

    .line 311
    :pswitch_3
    const-string/jumbo v0, "green"

    goto :goto_0

    .line 313
    :pswitch_4
    const-string/jumbo v0, "magenta"

    goto :goto_0

    .line 315
    :pswitch_5
    const-string/jumbo v0, "red"

    goto :goto_0

    .line 317
    :pswitch_6
    const-string/jumbo v0, "yellow"

    goto :goto_0

    .line 319
    :pswitch_7
    const-string/jumbo v0, "white"

    goto :goto_0

    .line 321
    :pswitch_8
    const-string/jumbo v0, "darkblue"

    goto :goto_0

    .line 323
    :pswitch_9
    const-string/jumbo v0, "darkcyan"

    goto :goto_0

    .line 325
    :pswitch_a
    const-string/jumbo v0, "darkgreen"

    goto :goto_0

    .line 327
    :pswitch_b
    const-string/jumbo v0, "darkmagenta"

    goto :goto_0

    .line 329
    :pswitch_c
    const-string/jumbo v0, "darkred"

    goto :goto_0

    .line 331
    :pswitch_d
    const-string/jumbo v0, "darkyellow"

    goto :goto_0

    .line 333
    :pswitch_e
    const-string/jumbo v0, "darkgray"

    goto :goto_0

    .line 335
    :pswitch_f
    const-string/jumbo v0, "lightgray"

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static getColor24(I)Ljava/lang/String;
    .locals 7
    .param p0, "argbValue"    # I

    .prologue
    .line 352
    const/4 v5, -0x1

    if-ne p0, v5, :cond_0

    .line 353
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "This colorref is empty"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 355
    :cond_0
    const v5, 0xffffff

    and-int v0, p0, v5

    .line 356
    .local v0, "bgrValue":I
    and-int/lit16 v5, v0, 0xff

    shl-int/lit8 v5, v5, 0x10

    const v6, 0xff00

    and-int/2addr v6, v0

    or-int/2addr v5, v6

    .line 357
    const/high16 v6, 0xff0000

    and-int/2addr v6, v0

    shr-int/lit8 v6, v6, 0x10

    .line 356
    or-int v4, v5, v6

    .line 360
    .local v4, "rgbValue":I
    sparse-switch v4, :sswitch_data_0

    .line 396
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "#"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 397
    .local v3, "result":Ljava/lang/StringBuilder;
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "hex":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .local v2, "i":I
    :goto_0
    const/4 v5, 0x6

    if-lt v2, v5, :cond_1

    .line 402
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .end local v1    # "hex":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "result":Ljava/lang/StringBuilder;
    :goto_1
    return-object v5

    .line 363
    :sswitch_0
    const-string/jumbo v5, "white"

    goto :goto_1

    .line 365
    :sswitch_1
    const-string/jumbo v5, "silver"

    goto :goto_1

    .line 367
    :sswitch_2
    const-string/jumbo v5, "gray"

    goto :goto_1

    .line 369
    :sswitch_3
    const-string/jumbo v5, "black"

    goto :goto_1

    .line 371
    :sswitch_4
    const-string/jumbo v5, "red"

    goto :goto_1

    .line 373
    :sswitch_5
    const-string/jumbo v5, "maroon"

    goto :goto_1

    .line 375
    :sswitch_6
    const-string/jumbo v5, "yellow"

    goto :goto_1

    .line 377
    :sswitch_7
    const-string/jumbo v5, "olive"

    goto :goto_1

    .line 379
    :sswitch_8
    const-string/jumbo v5, "lime"

    goto :goto_1

    .line 381
    :sswitch_9
    const-string/jumbo v5, "green"

    goto :goto_1

    .line 383
    :sswitch_a
    const-string/jumbo v5, "aqua"

    goto :goto_1

    .line 385
    :sswitch_b
    const-string/jumbo v5, "teal"

    goto :goto_1

    .line 387
    :sswitch_c
    const-string/jumbo v5, "blue"

    goto :goto_1

    .line 389
    :sswitch_d
    const-string/jumbo v5, "navy"

    goto :goto_1

    .line 391
    :sswitch_e
    const-string/jumbo v5, "fuchsia"

    goto :goto_1

    .line 393
    :sswitch_f
    const-string/jumbo v5, "purple"

    goto :goto_1

    .line 400
    .restart local v1    # "hex":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "result":Ljava/lang/StringBuilder;
    :cond_1
    const/16 v5, 0x30

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 398
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 360
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x80 -> :sswitch_d
        0xff -> :sswitch_c
        0x8000 -> :sswitch_9
        0x8080 -> :sswitch_b
        0xff00 -> :sswitch_8
        0xffff -> :sswitch_a
        0x800000 -> :sswitch_5
        0x800080 -> :sswitch_f
        0x808000 -> :sswitch_7
        0x808080 -> :sswitch_2
        0xc0c0c0 -> :sswitch_1
        0xff0000 -> :sswitch_4
        0xff00ff -> :sswitch_e
        0xffff00 -> :sswitch_6
        0xffffff -> :sswitch_0
    .end sparse-switch
.end method

.method public static getJustification(I)Ljava/lang/String;
    .locals 1
    .param p0, "js"    # I

    .prologue
    .line 408
    packed-switch p0, :pswitch_data_0

    .line 430
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 411
    :pswitch_0
    const-string/jumbo v0, "start"

    goto :goto_0

    .line 413
    :pswitch_1
    const-string/jumbo v0, "center"

    goto :goto_0

    .line 415
    :pswitch_2
    const-string/jumbo v0, "end"

    goto :goto_0

    .line 418
    :pswitch_3
    const-string/jumbo v0, "justify"

    goto :goto_0

    .line 420
    :pswitch_4
    const-string/jumbo v0, "center"

    goto :goto_0

    .line 422
    :pswitch_5
    const-string/jumbo v0, "left"

    goto :goto_0

    .line 424
    :pswitch_6
    const-string/jumbo v0, "start"

    goto :goto_0

    .line 426
    :pswitch_7
    const-string/jumbo v0, "end"

    goto :goto_0

    .line 428
    :pswitch_8
    const-string/jumbo v0, "justify"

    goto :goto_0

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getLanguage(I)Ljava/lang/String;
    .locals 4
    .param p0, "languageCode"    # I

    .prologue
    .line 435
    sparse-switch p0, :sswitch_data_0

    .line 446
    sget-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    const-string/jumbo v2, "Uknown or unmapped language code: "

    .line 447
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 446
    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 448
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 438
    :sswitch_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 440
    :sswitch_1
    const-string/jumbo v0, "en-us"

    goto :goto_0

    .line 442
    :sswitch_2
    const-string/jumbo v0, "ru-ru"

    goto :goto_0

    .line 444
    :sswitch_3
    const-string/jumbo v0, "en-uk"

    goto :goto_0

    .line 435
    :sswitch_data_0
    .sparse-switch
        0x400 -> :sswitch_0
        0x409 -> :sswitch_1
        0x419 -> :sswitch_2
        0x809 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getListItemNumberLabel(II)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # I
    .param p1, "format"    # I

    .prologue
    .line 455
    if-eqz p1, :cond_0

    .line 456
    sget-object v0, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "NYI: toListItemNumberLabel(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 458
    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOpacity(I)Ljava/lang/String;
    .locals 6
    .param p0, "argbValue"    # I

    .prologue
    .line 343
    int-to-long v2, p0

    const-wide v4, 0xff000000L

    and-long/2addr v2, v4

    const/16 v1, 0x18

    ushr-long/2addr v2, v1

    long-to-int v0, v2

    .line 344
    .local v0, "opacity":I
    if-eqz v0, :cond_0

    const/16 v1, 0xff

    if-ne v0, v1, :cond_1

    .line 345
    :cond_0
    const-string/jumbo v1, ".0"

    .line 347
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    int-to-float v2, v0

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 463
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static isNotEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 468
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 2
    .param p0, "docFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 486
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 489
    .local v0, "istream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->loadDoc(Ljava/io/InputStream;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 493
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 489
    return-object v1

    .line 492
    :catchall_0
    move-exception v1

    .line 493
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 494
    throw v1
.end method

.method public static loadDoc(Ljava/io/InputStream;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 1
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 500
    invoke-static {p0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->loadDoc(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    return-object v0
.end method

.method public static loadDoc(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 2
    .param p0, "root"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 476
    :try_start_0
    new-instance v1, Lorg/apache/poi/hwpf/HWPFDocument;

    invoke-direct {v1, p0}, Lorg/apache/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    :try_end_0
    .catch Lorg/apache/poi/hwpf/OldWordFileFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    :goto_0
    return-object v1

    .line 478
    :catch_0
    move-exception v0

    .line 480
    .local v0, "exc":Lorg/apache/poi/hwpf/OldWordFileFormatException;
    new-instance v1, Lorg/apache/poi/hwpf/HWPFOldDocument;

    invoke-direct {v1, p0}, Lorg/apache/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    goto :goto_0
.end method

.method public static loadDoc(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .locals 1
    .param p0, "poifsFileSystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->loadDoc(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    return-object v0
.end method

.method static substringBeforeLast(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 511
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 520
    .end local p0    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 515
    .restart local p0    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 516
    .local v0, "pos":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 520
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
