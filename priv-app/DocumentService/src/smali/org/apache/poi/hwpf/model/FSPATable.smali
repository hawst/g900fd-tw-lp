.class public final Lorg/apache/poi/hwpf/model/FSPATable;
.super Ljava/lang/Object;
.source "FSPATable.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private final _byStart:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hwpf/model/GenericPropertyNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([BIILjava/util/List;)V
    .locals 5
    .param p1, "tableStream"    # [B
    .param p2, "fcPlcspa"    # I
    .param p3, "lcbPlcspa"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/TextPiece;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54
    .local p4, "tpt":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/TextPiece;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    .line 58
    if-nez p2, :cond_1

    .line 68
    :cond_0
    return-void

    .line 61
    :cond_1
    new-instance v1, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 62
    sget v3, Lorg/apache/poi/hwpf/model/FSPA;->FSPA_SIZE:I

    .line 61
    invoke-direct {v1, p1, p2, p3, v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 63
    .local v1, "plex":Lorg/apache/poi/hwpf/model/PlexOfCps;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 65
    invoke-virtual {v1, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 66
    .local v2, "property":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([BLorg/apache/poi/hwpf/model/FileInformationBlock;Lorg/apache/poi/hwpf/model/FSPADocumentPart;)V
    .locals 7
    .param p1, "tableStream"    # [B
    .param p2, "fib"    # Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .param p3, "part"    # Lorg/apache/poi/hwpf/model/FSPADocumentPart;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    .line 41
    invoke-virtual {p2, p3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFSPAPlcfOffset(Lorg/apache/poi/hwpf/model/FSPADocumentPart;)I

    move-result v2

    .line 42
    .local v2, "offset":I
    invoke-virtual {p2, p3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFSPAPlcfLength(Lorg/apache/poi/hwpf/model/FSPADocumentPart;)I

    move-result v1

    .line 44
    .local v1, "length":I
    new-instance v3, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 45
    invoke-static {}, Lorg/apache/poi/hwpf/model/FSPA;->getSize()I

    move-result v5

    .line 44
    invoke-direct {v3, p1, v2, v1, v5}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 46
    .local v3, "plex":Lorg/apache/poi/hwpf/model/PlexOfCps;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v5

    if-lt v0, v5, :cond_0

    .line 51
    return-void

    .line 48
    :cond_0
    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v4

    .line 49
    .local v4, "property":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getFspaFromCp(I)Lorg/apache/poi/hwpf/model/FSPA;
    .locals 4
    .param p1, "cp"    # I

    .prologue
    .line 72
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 73
    .local v0, "propertyNode":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    if-nez v0, :cond_0

    .line 75
    const/4 v1, 0x0

    .line 77
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/hwpf/model/FSPA;-><init>([BI)V

    goto :goto_0
.end method

.method public getShapes()[Lorg/apache/poi/hwpf/model/FSPA;
    .locals 6

    .prologue
    .line 82
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/model/FSPA;>;"
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 87
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hwpf/model/FSPA;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hwpf/model/FSPA;

    return-object v2

    .line 83
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 85
    .local v0, "propertyNode":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    new-instance v3, Lorg/apache/poi/hwpf/model/FSPA;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/hwpf/model/FSPA;-><init>([BI)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 93
    .local v0, "buf":Ljava/lang/StringBuffer;
    const-string/jumbo v5, "[FPSA PLC size="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 94
    const-string/jumbo v6, "]\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 96
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/FSPATable;->_byStart:Ljava/util/Map;

    .line 97
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 96
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 115
    const-string/jumbo v5, "[/FSPA PLC]"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 97
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 99
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/poi/hwpf/model/GenericPropertyNode;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 100
    .local v4, "i":Ljava/lang/Integer;
    const-string/jumbo v6, "  "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, " => \t"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p0, v6}, Lorg/apache/poi/hwpf/model/FSPATable;->getFspaFromCp(I)Lorg/apache/poi/hwpf/model/FSPA;

    move-result-object v3

    .line 106
    .local v3, "fspa":Lorg/apache/poi/hwpf/model/FSPA;
    if-eqz v3, :cond_1

    .line 107
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FSPA;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v3    # "fspa":Lorg/apache/poi/hwpf/model/FSPA;
    :cond_1
    :goto_1
    const-string/jumbo v6, "\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 109
    :catch_0
    move-exception v2

    .line 111
    .local v2, "exc":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
