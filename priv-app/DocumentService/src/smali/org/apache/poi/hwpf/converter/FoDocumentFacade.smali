.class public Lorg/apache/poi/hwpf/converter/FoDocumentFacade;
.super Ljava/lang/Object;
.source "FoDocumentFacade.java"


# static fields
.field private static final NS_RDF:Ljava/lang/String; = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

.field private static final NS_XSLFO:Ljava/lang/String; = "http://www.w3.org/1999/XSL/Format"


# instance fields
.field protected final declarations:Lorg/w3c/dom/Element;

.field protected final document:Lorg/w3c/dom/Document;

.field protected final layoutMasterSet:Lorg/w3c/dom/Element;

.field protected propertiesRoot:Lorg/w3c/dom/Element;

.field protected final root:Lorg/w3c/dom/Element;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    .line 43
    const-string/jumbo v0, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v1, "fo:root"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->root:Lorg/w3c/dom/Element;

    .line 44
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->root:Lorg/w3c/dom/Element;

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 46
    const-string/jumbo v0, "http://www.w3.org/1999/XSL/Format"

    .line 47
    const-string/jumbo v1, "fo:layout-master-set"

    .line 46
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->layoutMasterSet:Lorg/w3c/dom/Element;

    .line 48
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->root:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->layoutMasterSet:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 50
    const-string/jumbo v0, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v1, "fo:declarations"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->declarations:Lorg/w3c/dom/Element;

    .line 51
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->root:Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->declarations:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 52
    return-void
.end method


# virtual methods
.method public addFlowToPageSequence(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "pageSequence"    # Lorg/w3c/dom/Element;
    .param p2, "flowName"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v3, "fo:flow"

    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 58
    .local v0, "flow":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "flow-name"

    invoke-interface {v0, v1, p2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 61
    return-object v0
.end method

.method public addListItem(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;
    .locals 1
    .param p1, "listBlock"    # Lorg/w3c/dom/Element;

    .prologue
    .line 66
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createListItem()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 67
    .local v0, "result":Lorg/w3c/dom/Element;
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 68
    return-object v0
.end method

.method public addListItemBody(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;
    .locals 1
    .param p1, "listItem"    # Lorg/w3c/dom/Element;

    .prologue
    .line 73
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createListItemBody()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 74
    .local v0, "result":Lorg/w3c/dom/Element;
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 75
    return-object v0
.end method

.method public addListItemLabel(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 1
    .param p1, "listItem"    # Lorg/w3c/dom/Element;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-virtual {p0, p2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createListItemLabel(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 81
    .local v0, "result":Lorg/w3c/dom/Element;
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 82
    return-object v0
.end method

.method public addPageSequence(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "pageMaster"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createPageSequence(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 93
    .local v0, "pageSequence":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->root:Lorg/w3c/dom/Element;

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 94
    return-object v0
.end method

.method public addPageSequence(Lorg/w3c/dom/Element;)V
    .locals 1
    .param p1, "pageSequence"    # Lorg/w3c/dom/Element;

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->root:Lorg/w3c/dom/Element;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 88
    return-void
.end method

.method public addRegionBody(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "pageMaster"    # Lorg/w3c/dom/Element;

    .prologue
    .line 99
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    .line 100
    const-string/jumbo v3, "fo:region-body"

    .line 99
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 101
    .local v0, "regionBody":Lorg/w3c/dom/Element;
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 103
    return-object v0
.end method

.method public addSimplePageMaster(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "masterName"    # Ljava/lang/String;

    .prologue
    .line 108
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    .line 109
    const-string/jumbo v3, "fo:simple-page-master"

    .line 108
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 110
    .local v0, "simplePageMaster":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "master-name"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->layoutMasterSet:Lorg/w3c/dom/Element;

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 113
    return-object v0
.end method

.method public createBasicLinkExternal(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "externalDestination"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    .line 119
    const-string/jumbo v3, "fo:basic-link"

    .line 118
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 120
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "external-destination"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-object v0
.end method

.method public createBasicLinkInternal(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "internalDestination"    # Ljava/lang/String;

    .prologue
    .line 126
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    .line 127
    const-string/jumbo v3, "fo:basic-link"

    .line 126
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 128
    .local v0, "basicLink":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "internal-destination"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    return-object v0
.end method

.method public createBlock()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:block"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createExternalGraphic(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 139
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    .line 140
    const-string/jumbo v3, "fo:external-graphic"

    .line 139
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 141
    .local v0, "result":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "src"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "url(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\')"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-object v0
.end method

.method public createFootnote()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:footnote"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createFootnoteBody()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:footnote-body"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createInline()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:inline"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createLeader()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:leader"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createListBlock()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:list-block"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createListItem()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:list-item"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createListItemBody()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:list-item-body"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createListItemLabel(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 182
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v3, "http://www.w3.org/1999/XSL/Format"

    .line 183
    const-string/jumbo v4, "fo:list-item-label"

    .line 182
    invoke-interface {v2, v3, v4}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 184
    .local v1, "result":Lorg/w3c/dom/Element;
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 185
    .local v0, "block":Lorg/w3c/dom/Element;
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v2, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 186
    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 187
    return-object v1
.end method

.method public createPageSequence(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 4
    .param p1, "pageMaster"    # Ljava/lang/String;

    .prologue
    .line 192
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v2, "http://www.w3.org/1999/XSL/Format"

    .line 193
    const-string/jumbo v3, "fo:page-sequence"

    .line 192
    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 194
    .local v0, "pageSequence":Lorg/w3c/dom/Element;
    const-string/jumbo v1, "master-reference"

    invoke-interface {v0, v1, p1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-object v0
.end method

.method public createTable()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:table"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableBody()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:table-body"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableCell()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:table-cell"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableColumn()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:table-column"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableHeader()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:table-header"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createTableRow()Lorg/w3c/dom/Element;
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v1, "http://www.w3.org/1999/XSL/Format"

    const-string/jumbo v2, "fo:table-row"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public createText(Ljava/lang/String;)Lorg/w3c/dom/Text;
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method protected getOrCreatePropertiesRoot()Lorg/w3c/dom/Element;
    .locals 6

    .prologue
    .line 240
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->propertiesRoot:Lorg/w3c/dom/Element;

    if-eqz v2, :cond_0

    .line 241
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->propertiesRoot:Lorg/w3c/dom/Element;

    .line 256
    :goto_0
    return-object v2

    .line 245
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v3, "adobe:ns:meta/"

    .line 246
    const-string/jumbo v4, "x:xmpmeta"

    .line 245
    invoke-interface {v2, v3, v4}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    .line 247
    .local v1, "xmpmeta":Lorg/w3c/dom/Element;
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->declarations:Lorg/w3c/dom/Element;

    invoke-interface {v2, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 249
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v3, "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

    const-string/jumbo v4, "rdf:RDF"

    invoke-interface {v2, v3, v4}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 250
    .local v0, "rdf":Lorg/w3c/dom/Element;
    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 252
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    const-string/jumbo v3, "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

    const-string/jumbo v4, "rdf:Description"

    invoke-interface {v2, v3, v4}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->propertiesRoot:Lorg/w3c/dom/Element;

    .line 253
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->propertiesRoot:Lorg/w3c/dom/Element;

    const-string/jumbo v3, "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

    const-string/jumbo v4, "rdf:about"

    const-string/jumbo v5, ""

    invoke-interface {v2, v3, v4, v5}, Lorg/w3c/dom/Element;->setAttributeNS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->propertiesRoot:Lorg/w3c/dom/Element;

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 256
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->propertiesRoot:Lorg/w3c/dom/Element;

    goto :goto_0
.end method

.method public setCreator(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 261
    const-string/jumbo v0, "creator"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setDublinCoreProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    .line 262
    return-void
.end method

.method public setCreatorTool(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 266
    const-string/jumbo v0, "CreatorTool"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setXmpProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    .line 267
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 271
    const-string/jumbo v1, "description"

    invoke-virtual {p0, v1, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setDublinCoreProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 273
    .local v0, "element":Lorg/w3c/dom/Element;
    if-eqz v0, :cond_0

    .line 275
    const-string/jumbo v1, "http://www.w3.org/XML/1998/namespace"

    .line 276
    const-string/jumbo v2, "xml:lang"

    const-string/jumbo v3, "x-default"

    .line 275
    invoke-interface {v0, v1, v2, v3}, Lorg/w3c/dom/Element;->setAttributeNS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_0
    return-void
.end method

.method public setDublinCoreProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 282
    const-string/jumbo v0, "http://purl.org/dc/elements/1.1/"

    const-string/jumbo v1, "dc"

    invoke-virtual {p0, v0, v1, p1, p2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public setKeywords(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 288
    const-string/jumbo v0, "Keywords"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setPdfProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    .line 289
    return-void
.end method

.method public setPdfProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 293
    const-string/jumbo v0, "http://ns.adobe.com/pdf/1.3/"

    const-string/jumbo v1, "pdf"

    invoke-virtual {p0, v0, v1, p1, p2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public setProducer(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 298
    const-string/jumbo v0, "Producer"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setPdfProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    .line 299
    return-void
.end method

.method protected setProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 9
    .param p1, "namespace"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->getOrCreatePropertiesRoot()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 305
    .local v4, "propertiesRoot":Lorg/w3c/dom/Element;
    invoke-interface {v4}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 306
    .local v2, "existingChildren":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-lt v3, v6, :cond_0

    .line 324
    :goto_1
    invoke-static {p4}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 326
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 327
    const-string/jumbo v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 326
    invoke-interface {v6, p1, v7}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 328
    .local v5, "property":Lorg/w3c/dom/Element;
    iget-object v6, p0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->document:Lorg/w3c/dom/Document;

    invoke-interface {v6, p4}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 329
    invoke-interface {v4, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 333
    .end local v5    # "property":Lorg/w3c/dom/Element;
    :goto_2
    return-object v5

    .line 308
    :cond_0
    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 309
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    move-object v1, v0

    .line 311
    check-cast v1, Lorg/w3c/dom/Element;

    .line 312
    .local v1, "childElement":Lorg/w3c/dom/Element;
    invoke-interface {v1}, Lorg/w3c/dom/Element;->getNamespaceURI()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 314
    invoke-interface {v1}, Lorg/w3c/dom/Element;->getLocalName()Ljava/lang/String;

    move-result-object v6

    .line 313
    invoke-static {v6}, Lorg/apache/poi/hwpf/converter/WordToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v6

    .line 314
    if-eqz v6, :cond_1

    .line 315
    invoke-interface {v1}, Lorg/w3c/dom/Element;->getNamespaceURI()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 316
    invoke-interface {v1}, Lorg/w3c/dom/Element;->getLocalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 318
    invoke-interface {v4, v1}, Lorg/w3c/dom/Element;->removeChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_1

    .line 306
    .end local v1    # "childElement":Lorg/w3c/dom/Element;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 333
    .end local v0    # "child":Lorg/w3c/dom/Node;
    :cond_2
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 338
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setDublinCoreProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    .line 339
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 343
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setDublinCoreProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    .line 344
    return-void
.end method

.method public setXmpProperty(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 348
    const-string/jumbo v0, "http://ns.adobe.com/xap/1.0/"

    const-string/jumbo v1, "xmp"

    invoke-virtual {p0, v0, v1, p1, p2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method
