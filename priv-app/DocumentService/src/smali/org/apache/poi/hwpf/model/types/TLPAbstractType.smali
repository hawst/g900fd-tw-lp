.class public abstract Lorg/apache/poi/hwpf/model/types/TLPAbstractType;
.super Ljava/lang/Object;
.source "TLPAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static fBestFit:Lorg/apache/poi/util/BitField;

.field private static fBorders:Lorg/apache/poi/util/BitField;

.field private static fColor:Lorg/apache/poi/util/BitField;

.field private static fFont:Lorg/apache/poi/util/BitField;

.field private static fHdrRows:Lorg/apache/poi/util/BitField;

.field private static fLastRow:Lorg/apache/poi/util/BitField;

.field private static fShading:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_itl:S

.field protected field_2_tlp_flags:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fBorders:Lorg/apache/poi/util/BitField;

    .line 44
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fShading:Lorg/apache/poi/util/BitField;

    .line 45
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fFont:Lorg/apache/poi/util/BitField;

    .line 46
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fColor:Lorg/apache/poi/util/BitField;

    .line 47
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fBestFit:Lorg/apache/poi/util/BitField;

    .line 48
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fHdrRows:Lorg/apache/poi/util/BitField;

    .line 49
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fLastRow:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 58
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_1_itl:S

    .line 59
    add-int/lit8 v0, p2, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 60
    return-void
.end method

.method public getItl()S
    .locals 1

    .prologue
    .line 111
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_1_itl:S

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x7

    return v0
.end method

.method public getTlp_flags()B
    .locals 1

    .prologue
    .line 127
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    return v0
.end method

.method public isFBestFit()Z
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fBestFit:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBorders()Z
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fBorders:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFColor()Z
    .locals 2

    .prologue
    .line 220
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fColor:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFont()Z
    .locals 2

    .prologue
    .line 199
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fFont:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHdrRows()Z
    .locals 2

    .prologue
    .line 265
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fHdrRows:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLastRow()Z
    .locals 2

    .prologue
    .line 288
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fLastRow:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFShading()Z
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fShading:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 64
    add-int/lit8 v0, p2, 0x0

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_1_itl:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 65
    add-int/lit8 v0, p2, 0x2

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    aput-byte v1, p1, v0

    .line 66
    return-void
.end method

.method public setFBestFit(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 230
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fBestFit:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 233
    return-void
.end method

.method public setFBorders(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 144
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fBorders:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 147
    return-void
.end method

.method public setFColor(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 209
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fColor:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 211
    return-void
.end method

.method public setFFont(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 188
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fFont:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 190
    return-void
.end method

.method public setFHdrRows(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 252
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fHdrRows:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 255
    return-void
.end method

.method public setFLastRow(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 275
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fLastRow:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 278
    return-void
.end method

.method public setFShading(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 166
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->fShading:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 169
    return-void
.end method

.method public setItl(S)V
    .locals 0
    .param p1, "field_1_itl"    # S

    .prologue
    .line 119
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_1_itl:S

    .line 120
    return-void
.end method

.method public setTlp_flags(B)V
    .locals 0
    .param p1, "field_2_tlp_flags"    # B

    .prologue
    .line 135
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->field_2_tlp_flags:B

    .line 136
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 70
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 72
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[TLP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    const-string/jumbo v1, "    .itl                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->getItl()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    const-string/jumbo v1, "    .tlp_flags            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->getTlp_flags()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    const-string/jumbo v1, "         .fBorders                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFBorders()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 81
    const-string/jumbo v1, "         .fShading                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 82
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFShading()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 83
    const-string/jumbo v1, "         .fFont                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 84
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFFont()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 85
    const-string/jumbo v1, "         .fColor                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 86
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFColor()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 87
    const-string/jumbo v1, "         .fBestFit                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 88
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFBestFit()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 89
    const-string/jumbo v1, "         .fHdrRows                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 90
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFHdrRows()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 91
    const-string/jumbo v1, "         .fLastRow                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 92
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;->isFLastRow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 94
    const-string/jumbo v1, "[/TLP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
