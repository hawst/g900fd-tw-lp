.class public abstract Lorg/apache/poi/hwpf/model/types/CHPAbstractType;
.super Ljava/lang/Object;
.source "CHPAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field protected static final ISS_NONE:B = 0x0t

.field protected static final ISS_SUBSCRIPTED:B = 0x2t

.field protected static final ISS_SUPERSCRIPTED:B = 0x1t

.field protected static final KCD_CIRCLE:B = 0x3t

.field protected static final KCD_COMMA:B = 0x2t

.field protected static final KCD_DOT:B = 0x1t

.field protected static final KCD_NON:B = 0x0t

.field protected static final KCD_UNDER_DOT:B = 0x4t

.field protected static final KUL_BY_WORD:B = 0x2t

.field protected static final KUL_DASH:B = 0x7t

.field protected static final KUL_DASHED_HEAVY:B = 0x17t

.field protected static final KUL_DASH_LONG:B = 0x27t

.field protected static final KUL_DASH_LONG_HEAVY:B = 0x37t

.field protected static final KUL_DOT:B = 0x8t

.field protected static final KUL_DOTTED:B = 0x4t

.field protected static final KUL_DOTTED_HEAVY:B = 0x14t

.field protected static final KUL_DOT_DASH:B = 0x9t

.field protected static final KUL_DOT_DASH_HEAVY:B = 0x19t

.field protected static final KUL_DOT_DOT_DASH:B = 0xat

.field protected static final KUL_DOT_DOT_DASH_HEAVY:B = 0x1at

.field protected static final KUL_DOUBLE:B = 0x3t

.field protected static final KUL_HIDDEN:B = 0x5t

.field protected static final KUL_NONE:B = 0x0t

.field protected static final KUL_SINGLE:B = 0x1t

.field protected static final KUL_THICK:B = 0x6t

.field protected static final KUL_WAVE:B = 0xbt

.field protected static final KUL_WAVE_DOUBLE:B = 0x2bt

.field protected static final KUL_WAVE_HEAVY:B = 0x1bt

.field protected static final LBRCRJ_BOTH:B = 0x3t

.field protected static final LBRCRJ_LEFT:B = 0x1t

.field protected static final LBRCRJ_NONE:B = 0x0t

.field protected static final LBRCRJ_RIGHT:B = 0x2t

.field protected static final SFXTTEXT_BACKGROUND_BLINK:B = 0x2t

.field protected static final SFXTTEXT_LAS_VEGAS_LIGHTS:B = 0x1t

.field protected static final SFXTTEXT_MARCHING_ANTS:B = 0x4t

.field protected static final SFXTTEXT_MARCHING_RED_ANTS:B = 0x5t

.field protected static final SFXTTEXT_NO:B = 0x0t

.field protected static final SFXTTEXT_SHIMMER:B = 0x6t

.field protected static final SFXTTEXT_SPARKLE_TEXT:B = 0x3t

.field private static final fBiDi:Lorg/apache/poi/util/BitField;

.field private static final fBold:Lorg/apache/poi/util/BitField;

.field private static final fBoldBi:Lorg/apache/poi/util/BitField;

.field private static final fBoldOther:Lorg/apache/poi/util/BitField;

.field private static final fCalc:Lorg/apache/poi/util/BitField;

.field private static final fCaps:Lorg/apache/poi/util/BitField;

.field private static final fCellFitText:Lorg/apache/poi/util/BitField;

.field private static final fChsDiff:Lorg/apache/poi/util/BitField;

.field private static final fComplexScripts:Lorg/apache/poi/util/BitField;

.field private static final fDStrike:Lorg/apache/poi/util/BitField;

.field private static final fData:Lorg/apache/poi/util/BitField;

.field private static final fEmboss:Lorg/apache/poi/util/BitField;

.field private static final fFitText:Lorg/apache/poi/util/BitField;

.field private static final fFldVanish:Lorg/apache/poi/util/BitField;

.field private static final fFmtLineProp:Lorg/apache/poi/util/BitField;

.field private static final fHighlight:Lorg/apache/poi/util/BitField;

.field private static final fIcoBi:Lorg/apache/poi/util/BitField;

.field private static final fImprint:Lorg/apache/poi/util/BitField;

.field private static final fItalic:Lorg/apache/poi/util/BitField;

.field private static final fItalicBi:Lorg/apache/poi/util/BitField;

.field private static final fItalicOther:Lorg/apache/poi/util/BitField;

.field private static final fKumimoji:Lorg/apache/poi/util/BitField;

.field private static final fLSFitText:Lorg/apache/poi/util/BitField;

.field private static final fLowerCase:Lorg/apache/poi/util/BitField;

.field private static final fMacChs:Lorg/apache/poi/util/BitField;

.field private static final fNoProof:Lorg/apache/poi/util/BitField;

.field private static final fNonGlyph:Lorg/apache/poi/util/BitField;

.field private static final fObj:Lorg/apache/poi/util/BitField;

.field private static final fOle2:Lorg/apache/poi/util/BitField;

.field private static final fOutline:Lorg/apache/poi/util/BitField;

.field private static final fRMark:Lorg/apache/poi/util/BitField;

.field private static final fRMarkDel:Lorg/apache/poi/util/BitField;

.field private static final fRuby:Lorg/apache/poi/util/BitField;

.field private static final fShadow:Lorg/apache/poi/util/BitField;

.field private static final fSmallCaps:Lorg/apache/poi/util/BitField;

.field private static final fSpec:Lorg/apache/poi/util/BitField;

.field private static final fStrike:Lorg/apache/poi/util/BitField;

.field private static final fTNY:Lorg/apache/poi/util/BitField;

.field private static final fTNYCompress:Lorg/apache/poi/util/BitField;

.field private static final fTNYFetchTxm:Lorg/apache/poi/util/BitField;

.field private static final fUsePgsuSettings:Lorg/apache/poi/util/BitField;

.field private static final fVanish:Lorg/apache/poi/util/BitField;

.field private static final fWarichu:Lorg/apache/poi/util/BitField;

.field private static final fWarichuNoOpenBracket:Lorg/apache/poi/util/BitField;

.field private static final fWebHidden:Lorg/apache/poi/util/BitField;

.field private static final iWarichuBracket:Lorg/apache/poi/util/BitField;

.field private static final icoHighlight:Lorg/apache/poi/util/BitField;

.field private static final itypFELayout:Lorg/apache/poi/util/BitField;

.field private static final spare:Lorg/apache/poi/util/BitField;

.field private static final unused:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_10_pctCharWidth:I

.field protected field_11_lidDefault:I

.field protected field_12_lidFE:I

.field protected field_13_kcd:B

.field protected field_14_fUndetermine:Z

.field protected field_15_iss:B

.field protected field_16_fSpecSymbol:Z

.field protected field_17_idct:B

.field protected field_18_idctHint:B

.field protected field_19_kul:B

.field protected field_1_grpfChp:I

.field protected field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

.field protected field_21_hpsKern:I

.field protected field_22_hpsPos:S

.field protected field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

.field protected field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

.field protected field_25_ibstRMark:I

.field protected field_26_sfxtText:B

.field protected field_27_fDblBdr:Z

.field protected field_28_fBorderWS:Z

.field protected field_29_ufel:S

.field protected field_2_hps:I

.field protected field_30_copt:B

.field protected field_31_hpsAsci:I

.field protected field_32_hpsFE:I

.field protected field_33_hpsBi:I

.field protected field_34_ftcSym:I

.field protected field_35_xchSym:I

.field protected field_36_fcPic:I

.field protected field_37_fcObj:I

.field protected field_38_lTagObj:I

.field protected field_39_fcData:I

.field protected field_3_ftcAscii:I

.field protected field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

.field protected field_41_ibstRMarkDel:I

.field protected field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_44_istd:I

.field protected field_45_idslRMReason:I

.field protected field_46_idslReasonDel:I

.field protected field_47_cpg:I

.field protected field_48_Highlight:S

.field protected field_49_CharsetFlags:S

.field protected field_4_ftcFE:I

.field protected field_50_chse:S

.field protected field_51_fPropRMark:Z

.field protected field_52_ibstPropRMark:I

.field protected field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_54_fConflictOrig:Z

.field protected field_55_fConflictOtherDel:Z

.field protected field_56_wConflict:I

.field protected field_57_IbstConflict:I

.field protected field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_59_fDispFldRMark:Z

.field protected field_5_ftcOther:I

.field protected field_60_ibstDispFldRMark:I

.field protected field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

.field protected field_62_xstDispFldRMark:[B

.field protected field_63_fcObjp:I

.field protected field_64_lbrCRJ:B

.field protected field_65_fSpecVanish:Z

.field protected field_66_fHasOldProps:Z

.field protected field_67_fSdtVanish:Z

.field protected field_68_wCharScale:I

.field protected field_6_ftcBi:I

.field protected field_7_dxaSpace:I

.field protected field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

.field protected field_9_ico:B


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x40

    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/16 v2, 0x20

    .line 47
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBold:Lorg/apache/poi/util/BitField;

    .line 48
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalic:Lorg/apache/poi/util/BitField;

    .line 49
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRMarkDel:Lorg/apache/poi/util/BitField;

    .line 50
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v4}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    .line 51
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v5}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFldVanish:Lorg/apache/poi/util/BitField;

    .line 52
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fSmallCaps:Lorg/apache/poi/util/BitField;

    .line 53
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v6}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCaps:Lorg/apache/poi/util/BitField;

    .line 54
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fVanish:Lorg/apache/poi/util/BitField;

    .line 55
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRMark:Lorg/apache/poi/util/BitField;

    .line 56
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fSpec:Lorg/apache/poi/util/BitField;

    .line 57
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fStrike:Lorg/apache/poi/util/BitField;

    .line 58
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x800

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fObj:Lorg/apache/poi/util/BitField;

    .line 59
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fShadow:Lorg/apache/poi/util/BitField;

    .line 60
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fLowerCase:Lorg/apache/poi/util/BitField;

    .line 61
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x4000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fData:Lorg/apache/poi/util/BitField;

    .line 62
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0x8000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fOle2:Lorg/apache/poi/util/BitField;

    .line 63
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fEmboss:Lorg/apache/poi/util/BitField;

    .line 64
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x20000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fImprint:Lorg/apache/poi/util/BitField;

    .line 65
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x40000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fDStrike:Lorg/apache/poi/util/BitField;

    .line 66
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x80000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fUsePgsuSettings:Lorg/apache/poi/util/BitField;

    .line 67
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x100000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBoldBi:Lorg/apache/poi/util/BitField;

    .line 68
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x100000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fComplexScripts:Lorg/apache/poi/util/BitField;

    .line 69
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalicBi:Lorg/apache/poi/util/BitField;

    .line 70
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x400000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBiDi:Lorg/apache/poi/util/BitField;

    .line 71
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x800000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fIcoBi:Lorg/apache/poi/util/BitField;

    .line 72
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x1000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fNonGlyph:Lorg/apache/poi/util/BitField;

    .line 73
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x2000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBoldOther:Lorg/apache/poi/util/BitField;

    .line 74
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x4000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalicOther:Lorg/apache/poi/util/BitField;

    .line 75
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x8000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fNoProof:Lorg/apache/poi/util/BitField;

    .line 76
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x10000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWebHidden:Lorg/apache/poi/util/BitField;

    .line 77
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x20000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFitText:Lorg/apache/poi/util/BitField;

    .line 78
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCalc:Lorg/apache/poi/util/BitField;

    .line 79
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/high16 v1, -0x80000000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFmtLineProp:Lorg/apache/poi/util/BitField;

    .line 143
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->itypFELayout:Lorg/apache/poi/util/BitField;

    .line 144
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNY:Lorg/apache/poi/util/BitField;

    .line 145
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWarichu:Lorg/apache/poi/util/BitField;

    .line 146
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fKumimoji:Lorg/apache/poi/util/BitField;

    .line 147
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x800

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRuby:Lorg/apache/poi/util/BitField;

    .line 148
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fLSFitText:Lorg/apache/poi/util/BitField;

    .line 149
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0xe000

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->spare:Lorg/apache/poi/util/BitField;

    .line 151
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->iWarichuBracket:Lorg/apache/poi/util/BitField;

    .line 152
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v4}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWarichuNoOpenBracket:Lorg/apache/poi/util/BitField;

    .line 153
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v5}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNYCompress:Lorg/apache/poi/util/BitField;

    .line 154
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNYFetchTxm:Lorg/apache/poi/util/BitField;

    .line 155
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v6}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCellFitText:Lorg/apache/poi/util/BitField;

    .line 156
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->unused:Lorg/apache/poi/util/BitField;

    .line 175
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x1f

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->icoHighlight:Lorg/apache/poi/util/BitField;

    .line 176
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fHighlight:Lorg/apache/poi/util/BitField;

    .line 178
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v3}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fChsDiff:Lorg/apache/poi/util/BitField;

    .line 179
    new-instance v0, Lorg/apache/poi/util/BitField;

    invoke-direct {v0, v2}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fMacChs:Lorg/apache/poi/util/BitField;

    .line 198
    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x400

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    const/16 v0, 0x14

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    .line 207
    new-instance v0, Lorg/apache/poi/hwpf/model/Colorref;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Colorref;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    .line 208
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    .line 209
    iput v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    .line 210
    new-instance v0, Lorg/apache/poi/hwpf/model/Hyphenation;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Hyphenation;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    .line 211
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 212
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/BorderCode;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 213
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    .line 214
    new-instance v0, Lorg/apache/poi/hwpf/model/Hyphenation;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Hyphenation;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    .line 215
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 216
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 217
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    .line 218
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 219
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 220
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 221
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    .line 222
    const/16 v0, 0x64

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    .line 223
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    if-ne p0, p1, :cond_1

    .line 422
    :cond_0
    :goto_0
    return v1

    .line 231
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 232
    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 234
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 235
    check-cast v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;

    .line 236
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/CHPAbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 237
    goto :goto_0

    .line 238
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 239
    goto :goto_0

    .line 240
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 241
    goto :goto_0

    .line 242
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 243
    goto :goto_0

    .line 244
    :cond_7
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 245
    goto :goto_0

    .line 246
    :cond_8
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 247
    goto :goto_0

    .line 248
    :cond_9
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 249
    goto :goto_0

    .line 250
    :cond_a
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    if-nez v3, :cond_b

    .line 252
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    if-eqz v3, :cond_c

    move v1, v2

    .line 253
    goto :goto_0

    .line 255
    :cond_b
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Colorref;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 256
    goto :goto_0

    .line 257
    :cond_c
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 258
    goto :goto_0

    .line 259
    :cond_d
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 260
    goto :goto_0

    .line 261
    :cond_e
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 262
    goto :goto_0

    .line 263
    :cond_f
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 264
    goto/16 :goto_0

    .line 265
    :cond_10
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    if-eq v3, v4, :cond_11

    move v1, v2

    .line 266
    goto/16 :goto_0

    .line 267
    :cond_11
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    if-eq v3, v4, :cond_12

    move v1, v2

    .line 268
    goto/16 :goto_0

    .line 269
    :cond_12
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    if-eq v3, v4, :cond_13

    move v1, v2

    .line 270
    goto/16 :goto_0

    .line 271
    :cond_13
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    if-eq v3, v4, :cond_14

    move v1, v2

    .line 272
    goto/16 :goto_0

    .line 273
    :cond_14
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    if-eq v3, v4, :cond_15

    move v1, v2

    .line 274
    goto/16 :goto_0

    .line 275
    :cond_15
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    if-eq v3, v4, :cond_16

    move v1, v2

    .line 276
    goto/16 :goto_0

    .line 277
    :cond_16
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 278
    goto/16 :goto_0

    .line 279
    :cond_17
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    if-nez v3, :cond_18

    .line 281
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    if-eqz v3, :cond_19

    move v1, v2

    .line 282
    goto/16 :goto_0

    .line 284
    :cond_18
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Hyphenation;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    move v1, v2

    .line 285
    goto/16 :goto_0

    .line 286
    :cond_19
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 287
    goto/16 :goto_0

    .line 288
    :cond_1a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    if-eq v3, v4, :cond_1b

    move v1, v2

    .line 289
    goto/16 :goto_0

    .line 290
    :cond_1b
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    if-nez v3, :cond_1c

    .line 292
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    if-eqz v3, :cond_1d

    move v1, v2

    .line 293
    goto/16 :goto_0

    .line 295
    :cond_1c
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    move v1, v2

    .line 296
    goto/16 :goto_0

    .line 297
    :cond_1d
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    if-nez v3, :cond_1e

    .line 299
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    if-eqz v3, :cond_1f

    move v1, v2

    .line 300
    goto/16 :goto_0

    .line 302
    :cond_1e
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1f

    move v1, v2

    .line 303
    goto/16 :goto_0

    .line 304
    :cond_1f
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    if-eq v3, v4, :cond_20

    move v1, v2

    .line 305
    goto/16 :goto_0

    .line 306
    :cond_20
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    if-eq v3, v4, :cond_21

    move v1, v2

    .line 307
    goto/16 :goto_0

    .line 308
    :cond_21
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    if-eq v3, v4, :cond_22

    move v1, v2

    .line 309
    goto/16 :goto_0

    .line 310
    :cond_22
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    if-eq v3, v4, :cond_23

    move v1, v2

    .line 311
    goto/16 :goto_0

    .line 312
    :cond_23
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    if-eq v3, v4, :cond_24

    move v1, v2

    .line 313
    goto/16 :goto_0

    .line 314
    :cond_24
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    if-eq v3, v4, :cond_25

    move v1, v2

    .line 315
    goto/16 :goto_0

    .line 316
    :cond_25
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    if-eq v3, v4, :cond_26

    move v1, v2

    .line 317
    goto/16 :goto_0

    .line 318
    :cond_26
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    if-eq v3, v4, :cond_27

    move v1, v2

    .line 319
    goto/16 :goto_0

    .line 320
    :cond_27
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    if-eq v3, v4, :cond_28

    move v1, v2

    .line 321
    goto/16 :goto_0

    .line 322
    :cond_28
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    if-eq v3, v4, :cond_29

    move v1, v2

    .line 323
    goto/16 :goto_0

    .line 324
    :cond_29
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    if-eq v3, v4, :cond_2a

    move v1, v2

    .line 325
    goto/16 :goto_0

    .line 326
    :cond_2a
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    if-eq v3, v4, :cond_2b

    move v1, v2

    .line 327
    goto/16 :goto_0

    .line 328
    :cond_2b
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    if-eq v3, v4, :cond_2c

    move v1, v2

    .line 329
    goto/16 :goto_0

    .line 330
    :cond_2c
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    if-eq v3, v4, :cond_2d

    move v1, v2

    .line 331
    goto/16 :goto_0

    .line 332
    :cond_2d
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    if-eq v3, v4, :cond_2e

    move v1, v2

    .line 333
    goto/16 :goto_0

    .line 334
    :cond_2e
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    if-nez v3, :cond_2f

    .line 336
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    if-eqz v3, :cond_30

    move v1, v2

    .line 337
    goto/16 :goto_0

    .line 339
    :cond_2f
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/model/Hyphenation;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_30

    move v1, v2

    .line 340
    goto/16 :goto_0

    .line 341
    :cond_30
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    if-eq v3, v4, :cond_31

    move v1, v2

    .line 342
    goto/16 :goto_0

    .line 343
    :cond_31
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v3, :cond_32

    .line 345
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-eqz v3, :cond_33

    move v1, v2

    .line 346
    goto/16 :goto_0

    .line 348
    :cond_32
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_33

    move v1, v2

    .line 349
    goto/16 :goto_0

    .line 350
    :cond_33
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v3, :cond_34

    .line 352
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-eqz v3, :cond_35

    move v1, v2

    .line 353
    goto/16 :goto_0

    .line 355
    :cond_34
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_35

    move v1, v2

    .line 356
    goto/16 :goto_0

    .line 357
    :cond_35
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    if-eq v3, v4, :cond_36

    move v1, v2

    .line 358
    goto/16 :goto_0

    .line 359
    :cond_36
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    if-eq v3, v4, :cond_37

    move v1, v2

    .line 360
    goto/16 :goto_0

    .line 361
    :cond_37
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    if-eq v3, v4, :cond_38

    move v1, v2

    .line 362
    goto/16 :goto_0

    .line 363
    :cond_38
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    if-eq v3, v4, :cond_39

    move v1, v2

    .line 364
    goto/16 :goto_0

    .line 365
    :cond_39
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    if-eq v3, v4, :cond_3a

    move v1, v2

    .line 366
    goto/16 :goto_0

    .line 367
    :cond_3a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    if-eq v3, v4, :cond_3b

    move v1, v2

    .line 368
    goto/16 :goto_0

    .line 369
    :cond_3b
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    if-eq v3, v4, :cond_3c

    move v1, v2

    .line 370
    goto/16 :goto_0

    .line 371
    :cond_3c
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    if-eq v3, v4, :cond_3d

    move v1, v2

    .line 372
    goto/16 :goto_0

    .line 373
    :cond_3d
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    if-eq v3, v4, :cond_3e

    move v1, v2

    .line 374
    goto/16 :goto_0

    .line 375
    :cond_3e
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v3, :cond_3f

    .line 377
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-eqz v3, :cond_40

    move v1, v2

    .line 378
    goto/16 :goto_0

    .line 380
    :cond_3f
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_40

    move v1, v2

    .line 381
    goto/16 :goto_0

    .line 382
    :cond_40
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    if-eq v3, v4, :cond_41

    move v1, v2

    .line 383
    goto/16 :goto_0

    .line 384
    :cond_41
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    if-eq v3, v4, :cond_42

    move v1, v2

    .line 385
    goto/16 :goto_0

    .line 386
    :cond_42
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    if-eq v3, v4, :cond_43

    move v1, v2

    .line 387
    goto/16 :goto_0

    .line 388
    :cond_43
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    if-eq v3, v4, :cond_44

    move v1, v2

    .line 389
    goto/16 :goto_0

    .line 390
    :cond_44
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v3, :cond_45

    .line 392
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-eqz v3, :cond_46

    move v1, v2

    .line 393
    goto/16 :goto_0

    .line 395
    :cond_45
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_46

    move v1, v2

    .line 396
    goto/16 :goto_0

    .line 397
    :cond_46
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    if-eq v3, v4, :cond_47

    move v1, v2

    .line 398
    goto/16 :goto_0

    .line 399
    :cond_47
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    if-eq v3, v4, :cond_48

    move v1, v2

    .line 400
    goto/16 :goto_0

    .line 401
    :cond_48
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v3, :cond_49

    .line 403
    iget-object v3, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-eqz v3, :cond_4a

    move v1, v2

    .line 404
    goto/16 :goto_0

    .line 406
    :cond_49
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4a

    move v1, v2

    .line 407
    goto/16 :goto_0

    .line 408
    :cond_4a
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_4b

    move v1, v2

    .line 409
    goto/16 :goto_0

    .line 410
    :cond_4b
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    if-eq v3, v4, :cond_4c

    move v1, v2

    .line 411
    goto/16 :goto_0

    .line 412
    :cond_4c
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    if-eq v3, v4, :cond_4d

    move v1, v2

    .line 413
    goto/16 :goto_0

    .line 414
    :cond_4d
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    if-eq v3, v4, :cond_4e

    move v1, v2

    .line 415
    goto/16 :goto_0

    .line 416
    :cond_4e
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    if-eq v3, v4, :cond_4f

    move v1, v2

    .line 417
    goto/16 :goto_0

    .line 418
    :cond_4f
    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    iget-boolean v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    if-eq v3, v4, :cond_50

    move v1, v2

    .line 419
    goto/16 :goto_0

    .line 420
    :cond_50
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 421
    goto/16 :goto_0
.end method

.method public getBrc()Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1197
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getCharsetFlags()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1666
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    return v0
.end method

.method public getChse()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1684
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    return v0
.end method

.method public getCopt()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1324
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    return v0
.end method

.method public getCpg()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1630
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    return v0
.end method

.method public getCv()Lorg/apache/poi/hwpf/model/Colorref;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 838
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    return-object v0
.end method

.method public getDttmConflict()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1828
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDttmDispFldRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1882
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1738
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1540
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDttmRMarkDel()Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1558
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDxaSpace()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 820
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    return v0
.end method

.method public getFBorderWS()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1288
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    return v0
.end method

.method public getFConflictOrig()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1756
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    return v0
.end method

.method public getFConflictOtherDel()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1774
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    return v0
.end method

.method public getFDblBdr()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1270
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    return v0
.end method

.method public getFDispFldRMark()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1846
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    return v0
.end method

.method public getFHasOldProps()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1985
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    return v0
.end method

.method public getFPropRMark()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1702
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    return v0
.end method

.method public getFSdtVanish()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2003
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    return v0
.end method

.method public getFSpecSymbol()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1008
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    return v0
.end method

.method public getFSpecVanish()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1967
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    return v0
.end method

.method public getFUndetermine()Z
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 961
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    return v0
.end method

.method public getFcData()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1486
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    return v0
.end method

.method public getFcObj()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1450
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    return v0
.end method

.method public getFcObjp()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1918
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    return v0
.end method

.method public getFcPic()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1432
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    return v0
.end method

.method public getFtcAscii()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 748
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    return v0
.end method

.method public getFtcBi()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 802
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    return v0
.end method

.method public getFtcFE()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 766
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    return v0
.end method

.method public getFtcOther()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 784
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    return v0
.end method

.method public getFtcSym()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1396
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    return v0
.end method

.method public getGrpfChp()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 712
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    return v0
.end method

.method public getHighlight()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1648
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    return v0
.end method

.method public getHps()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 730
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    return v0
.end method

.method public getHpsAsci()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1342
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    return v0
.end method

.method public getHpsBi()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1378
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    return v0
.end method

.method public getHpsFE()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1360
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    return v0
.end method

.method public getHpsKern()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1143
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    return v0
.end method

.method public getHpsPos()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1161
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    return v0
.end method

.method public getHresi()Lorg/apache/poi/hwpf/model/Hyphenation;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1125
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    return-object v0
.end method

.method public getHresiOld()Lorg/apache/poi/hwpf/model/Hyphenation;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1504
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    return-object v0
.end method

.method public getIWarichuBracket()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2850
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->iWarichuBracket:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getIbstConflict()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1810
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    return v0
.end method

.method public getIbstDispFldRMark()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1864
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1720
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    return v0
.end method

.method public getIbstRMark()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1215
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    return v0
.end method

.method public getIbstRMarkDel()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1522
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    return v0
.end method

.method public getIco()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 856
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    return v0
.end method

.method public getIcoHighlight()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2970
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->icoHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getIdct()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1026
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    return v0
.end method

.method public getIdctHint()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1044
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    return v0
.end method

.method public getIdslRMReason()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1594
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    return v0
.end method

.method public getIdslReasonDel()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1612
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    return v0
.end method

.method public getIss()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 984
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    return v0
.end method

.method public getIstd()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1576
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    return v0
.end method

.method public getItypFELayout()S
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2710
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->itypFELayout:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getKcd()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 935
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    return v0
.end method

.method public getKul()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1084
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    return v0
.end method

.method public getLTagObj()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1468
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    return v0
.end method

.method public getLbrCRJ()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1942
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    return v0
.end method

.method public getLidDefault()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 892
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    return v0
.end method

.method public getLidFE()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 910
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    return v0
.end method

.method public getPctCharWidth()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 874
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    return v0
.end method

.method public getSfxtText()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1242
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    return v0
.end method

.method public getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1179
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    return-object v0
.end method

.method public getSpare()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2830
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->spare:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getUfel()S
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1306
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    return v0
.end method

.method public getWCharScale()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2021
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    return v0
.end method

.method public getWConflict()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1792
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    return v0
.end method

.method public getXchSym()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1414
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    return v0
.end method

.method public getXstDispFldRMark()[B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1900
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    .line 428
    const/16 v0, 0x1f

    .line 429
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 430
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    add-int/lit8 v1, v2, 0x1f

    .line 431
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    add-int v1, v2, v6

    .line 432
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    add-int v1, v2, v6

    .line 433
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    add-int v1, v2, v6

    .line 434
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    add-int v1, v2, v6

    .line 435
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    add-int v1, v2, v6

    .line 436
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    add-int v1, v2, v6

    .line 437
    mul-int/lit8 v6, v1, 0x1f

    .line 438
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    if-nez v2, :cond_0

    move v2, v3

    .line 437
    :goto_0
    add-int v1, v6, v2

    .line 439
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    add-int v1, v2, v6

    .line 440
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    add-int v1, v2, v6

    .line 441
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    add-int v1, v2, v6

    .line 442
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    add-int v1, v2, v6

    .line 443
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    add-int v1, v2, v6

    .line 444
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    if-eqz v2, :cond_1

    move v2, v4

    :goto_1
    add-int v1, v6, v2

    .line 445
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    add-int v1, v2, v6

    .line 446
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    if-eqz v2, :cond_2

    move v2, v4

    :goto_2
    add-int v1, v6, v2

    .line 447
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    add-int v1, v2, v6

    .line 448
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    add-int v1, v2, v6

    .line 449
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    add-int v1, v2, v6

    .line 450
    mul-int/lit8 v6, v1, 0x1f

    .line 451
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    if-nez v2, :cond_3

    move v2, v3

    .line 450
    :goto_3
    add-int v1, v6, v2

    .line 452
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    add-int v1, v2, v6

    .line 453
    mul-int/lit8 v2, v1, 0x1f

    iget-short v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    add-int v1, v2, v6

    .line 454
    mul-int/lit8 v6, v1, 0x1f

    .line 455
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    if-nez v2, :cond_4

    move v2, v3

    .line 454
    :goto_4
    add-int v1, v6, v2

    .line 456
    mul-int/lit8 v6, v1, 0x1f

    .line 457
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    if-nez v2, :cond_5

    move v2, v3

    .line 456
    :goto_5
    add-int v1, v6, v2

    .line 458
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    add-int v1, v2, v6

    .line 459
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    add-int v1, v2, v6

    .line 460
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    if-eqz v2, :cond_6

    move v2, v4

    :goto_6
    add-int v1, v6, v2

    .line 461
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    if-eqz v2, :cond_7

    move v2, v4

    :goto_7
    add-int v1, v6, v2

    .line 462
    mul-int/lit8 v2, v1, 0x1f

    iget-short v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    add-int v1, v2, v6

    .line 463
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    add-int v1, v2, v6

    .line 464
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    add-int v1, v2, v6

    .line 465
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    add-int v1, v2, v6

    .line 466
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    add-int v1, v2, v6

    .line 467
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    add-int v1, v2, v6

    .line 468
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    add-int v1, v2, v6

    .line 469
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    add-int v1, v2, v6

    .line 470
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    add-int v1, v2, v6

    .line 471
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    add-int v1, v2, v6

    .line 472
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    add-int v1, v2, v6

    .line 473
    mul-int/lit8 v6, v1, 0x1f

    .line 474
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    if-nez v2, :cond_8

    move v2, v3

    .line 473
    :goto_8
    add-int v1, v6, v2

    .line 475
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    add-int v1, v2, v6

    .line 476
    mul-int/lit8 v6, v1, 0x1f

    .line 477
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v2, :cond_9

    move v2, v3

    .line 476
    :goto_9
    add-int v1, v6, v2

    .line 478
    mul-int/lit8 v6, v1, 0x1f

    .line 479
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v2, :cond_a

    move v2, v3

    .line 478
    :goto_a
    add-int v1, v6, v2

    .line 480
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    add-int v1, v2, v6

    .line 481
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    add-int v1, v2, v6

    .line 482
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    add-int v1, v2, v6

    .line 483
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    add-int v1, v2, v6

    .line 484
    mul-int/lit8 v2, v1, 0x1f

    iget-short v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    add-int v1, v2, v6

    .line 485
    mul-int/lit8 v2, v1, 0x1f

    iget-short v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    add-int v1, v2, v6

    .line 486
    mul-int/lit8 v2, v1, 0x1f

    iget-short v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    add-int v1, v2, v6

    .line 487
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    if-eqz v2, :cond_b

    move v2, v4

    :goto_b
    add-int v1, v6, v2

    .line 488
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    add-int v1, v2, v6

    .line 489
    mul-int/lit8 v6, v1, 0x1f

    .line 490
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v2, :cond_c

    move v2, v3

    .line 489
    :goto_c
    add-int v1, v6, v2

    .line 491
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    if-eqz v2, :cond_d

    move v2, v4

    :goto_d
    add-int v1, v6, v2

    .line 492
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    if-eqz v2, :cond_e

    move v2, v4

    :goto_e
    add-int v1, v6, v2

    .line 493
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    add-int v1, v2, v6

    .line 494
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    add-int v1, v2, v6

    .line 495
    mul-int/lit8 v6, v1, 0x1f

    .line 496
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v2, :cond_f

    move v2, v3

    .line 495
    :goto_f
    add-int v1, v6, v2

    .line 497
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    if-eqz v2, :cond_10

    move v2, v4

    :goto_10
    add-int v1, v6, v2

    .line 498
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    add-int v1, v2, v6

    .line 499
    mul-int/lit8 v2, v1, 0x1f

    .line 500
    iget-object v6, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    if-nez v6, :cond_11

    .line 499
    :goto_11
    add-int v1, v2, v3

    .line 501
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    move-result v3

    add-int v1, v2, v3

    .line 502
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    add-int v1, v2, v3

    .line 503
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    add-int v1, v2, v3

    .line 504
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    if-eqz v2, :cond_12

    move v2, v4

    :goto_12
    add-int v1, v3, v2

    .line 505
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    if-eqz v2, :cond_13

    move v2, v4

    :goto_13
    add-int v1, v3, v2

    .line 506
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    if-eqz v3, :cond_14

    :goto_14
    add-int v1, v2, v4

    .line 507
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    add-int v1, v2, v3

    .line 508
    return v1

    .line 438
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Colorref;->hashCode()I

    move-result v2

    goto/16 :goto_0

    :cond_1
    move v2, v5

    .line 444
    goto/16 :goto_1

    :cond_2
    move v2, v5

    .line 446
    goto/16 :goto_2

    .line 451
    :cond_3
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Hyphenation;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 455
    :cond_4
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 457
    :cond_5
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_5

    :cond_6
    move v2, v5

    .line 460
    goto/16 :goto_6

    :cond_7
    move v2, v5

    .line 461
    goto/16 :goto_7

    .line 474
    :cond_8
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Hyphenation;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 477
    :cond_9
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 479
    :cond_a
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_a

    :cond_b
    move v2, v5

    .line 487
    goto/16 :goto_b

    .line 490
    :cond_c
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_c

    :cond_d
    move v2, v5

    .line 491
    goto/16 :goto_d

    :cond_e
    move v2, v5

    .line 492
    goto/16 :goto_e

    .line 496
    :cond_f
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_f

    :cond_10
    move v2, v5

    .line 497
    goto/16 :goto_10

    .line 500
    :cond_11
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto/16 :goto_11

    :cond_12
    move v2, v5

    .line 504
    goto/16 :goto_12

    :cond_13
    move v2, v5

    .line 505
    goto :goto_13

    :cond_14
    move v4, v5

    .line 506
    goto :goto_14
.end method

.method public isFBiDi()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2510
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBiDi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBold()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2050
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBold:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBoldBi()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2450
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBoldBi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBoldOther()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2570
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBoldOther:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFCalc()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2670
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCalc:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFCaps()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2170
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFCellFitText()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2930
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCellFitText:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFChsDiff()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 3010
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fChsDiff:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFComplexScripts()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2470
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fComplexScripts:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFDStrike()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2410
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fDStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFData()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2330
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fData:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEmboss()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2370
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fEmboss:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFitText()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2650
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFitText:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFldVanish()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2130
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFldVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFmtLineProp()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2690
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFmtLineProp:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHighlight()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2990
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFIcoBi()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2530
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fIcoBi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFImprint()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2390
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fImprint:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFItalic()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2070
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalic:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFItalicBi()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2490
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalicBi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFItalicOther()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2590
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalicOther:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFKumimoji()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2770
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fKumimoji:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLSFitText()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2810
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fLSFitText:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLowerCase()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2310
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fLowerCase:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMacChs()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 3030
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fMacChs:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNoProof()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2610
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fNoProof:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNonGlyph()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2550
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fNonGlyph:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFObj()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2270
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fObj:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOle2()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2350
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fOle2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOutline()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2110
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRMark()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2210
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRMark:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRMarkDel()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2090
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRMarkDel:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRuby()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2790
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRuby:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFShadow()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2290
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fShadow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSmallCaps()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2150
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fSmallCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSpec()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2230
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fSpec:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFStrike()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2250
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFTNY()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2730
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNY:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFTNYCompress()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2890
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNYCompress:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFTNYFetchTxm()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2910
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNYFetchTxm:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFUsePgsuSettings()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2430
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fUsePgsuSettings:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVanish()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2190
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWarichu()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2750
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWarichu:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWarichuNoOpenBracket()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2870
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWarichuNoOpenBracket:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWebHidden()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2630
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWebHidden:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused()Z
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2950
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->unused:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public setBrc(Lorg/apache/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_24_brc"    # Lorg/apache/poi/hwpf/usermodel/BorderCode;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1206
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_24_brc:Lorg/apache/poi/hwpf/usermodel/BorderCode;

    .line 1207
    return-void
.end method

.method public setCharsetFlags(S)V
    .locals 0
    .param p1, "field_49_CharsetFlags"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1675
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 1676
    return-void
.end method

.method public setChse(S)V
    .locals 0
    .param p1, "field_50_chse"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1693
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    .line 1694
    return-void
.end method

.method public setCopt(B)V
    .locals 0
    .param p1, "field_30_copt"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1333
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 1334
    return-void
.end method

.method public setCpg(I)V
    .locals 0
    .param p1, "field_47_cpg"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1639
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    .line 1640
    return-void
.end method

.method public setCv(Lorg/apache/poi/hwpf/model/Colorref;)V
    .locals 0
    .param p1, "field_8_cv"    # Lorg/apache/poi/hwpf/model/Colorref;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 847
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_8_cv:Lorg/apache/poi/hwpf/model/Colorref;

    .line 848
    return-void
.end method

.method public setDttmConflict(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_58_dttmConflict"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1837
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 1838
    return-void
.end method

.method public setDttmDispFldRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_61_dttmDispFldRMark"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1891
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 1892
    return-void
.end method

.method public setDttmPropRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_53_dttmPropRMark"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1747
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 1748
    return-void
.end method

.method public setDttmRMark(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_42_dttmRMark"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1549
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 1550
    return-void
.end method

.method public setDttmRMarkDel(Lorg/apache/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_43_dttmRMarkDel"    # Lorg/apache/poi/hwpf/usermodel/DateAndTime;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1567
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    .line 1568
    return-void
.end method

.method public setDxaSpace(I)V
    .locals 0
    .param p1, "field_7_dxaSpace"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 829
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    .line 830
    return-void
.end method

.method public setFBiDi(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2500
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBiDi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2501
    return-void
.end method

.method public setFBold(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2040
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBold:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2041
    return-void
.end method

.method public setFBoldBi(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2440
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBoldBi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2441
    return-void
.end method

.method public setFBoldOther(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2560
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fBoldOther:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2561
    return-void
.end method

.method public setFBorderWS(Z)V
    .locals 0
    .param p1, "field_28_fBorderWS"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1297
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    .line 1298
    return-void
.end method

.method public setFCalc(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2660
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCalc:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2661
    return-void
.end method

.method public setFCaps(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2160
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2161
    return-void
.end method

.method public setFCellFitText(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2920
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fCellFitText:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2921
    return-void
.end method

.method public setFChsDiff(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 3000
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fChsDiff:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 3001
    return-void
.end method

.method public setFComplexScripts(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2460
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fComplexScripts:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2461
    return-void
.end method

.method public setFConflictOrig(Z)V
    .locals 0
    .param p1, "field_54_fConflictOrig"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1765
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    .line 1766
    return-void
.end method

.method public setFConflictOtherDel(Z)V
    .locals 0
    .param p1, "field_55_fConflictOtherDel"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1783
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    .line 1784
    return-void
.end method

.method public setFDStrike(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2400
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fDStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2401
    return-void
.end method

.method public setFData(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2320
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fData:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2321
    return-void
.end method

.method public setFDblBdr(Z)V
    .locals 0
    .param p1, "field_27_fDblBdr"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1279
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    .line 1280
    return-void
.end method

.method public setFDispFldRMark(Z)V
    .locals 0
    .param p1, "field_59_fDispFldRMark"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1855
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    .line 1856
    return-void
.end method

.method public setFEmboss(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2360
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fEmboss:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2361
    return-void
.end method

.method public setFFitText(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2640
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFitText:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2641
    return-void
.end method

.method public setFFldVanish(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2120
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFldVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2121
    return-void
.end method

.method public setFFmtLineProp(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2680
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fFmtLineProp:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2681
    return-void
.end method

.method public setFHasOldProps(Z)V
    .locals 0
    .param p1, "field_66_fHasOldProps"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1994
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    .line 1995
    return-void
.end method

.method public setFHighlight(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2980
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 2981
    return-void
.end method

.method public setFIcoBi(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2520
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fIcoBi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2521
    return-void
.end method

.method public setFImprint(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2380
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fImprint:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2381
    return-void
.end method

.method public setFItalic(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2060
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalic:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2061
    return-void
.end method

.method public setFItalicBi(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2480
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalicBi:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2481
    return-void
.end method

.method public setFItalicOther(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2580
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fItalicOther:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2581
    return-void
.end method

.method public setFKumimoji(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2760
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fKumimoji:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2761
    return-void
.end method

.method public setFLSFitText(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2800
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fLSFitText:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2801
    return-void
.end method

.method public setFLowerCase(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2300
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fLowerCase:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2301
    return-void
.end method

.method public setFMacChs(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 3020
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fMacChs:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 3021
    return-void
.end method

.method public setFNoProof(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2600
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fNoProof:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2601
    return-void
.end method

.method public setFNonGlyph(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2540
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fNonGlyph:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2541
    return-void
.end method

.method public setFObj(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2260
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fObj:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2261
    return-void
.end method

.method public setFOle2(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2340
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fOle2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2341
    return-void
.end method

.method public setFOutline(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2100
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2101
    return-void
.end method

.method public setFPropRMark(Z)V
    .locals 0
    .param p1, "field_51_fPropRMark"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1711
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    .line 1712
    return-void
.end method

.method public setFRMark(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2200
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRMark:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2201
    return-void
.end method

.method public setFRMarkDel(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2080
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRMarkDel:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2081
    return-void
.end method

.method public setFRuby(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2780
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fRuby:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2781
    return-void
.end method

.method public setFSdtVanish(Z)V
    .locals 0
    .param p1, "field_67_fSdtVanish"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2012
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    .line 2013
    return-void
.end method

.method public setFShadow(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2280
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fShadow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2281
    return-void
.end method

.method public setFSmallCaps(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2140
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fSmallCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2141
    return-void
.end method

.method public setFSpec(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2220
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fSpec:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2221
    return-void
.end method

.method public setFSpecSymbol(Z)V
    .locals 0
    .param p1, "field_16_fSpecSymbol"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1017
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    .line 1018
    return-void
.end method

.method public setFSpecVanish(Z)V
    .locals 0
    .param p1, "field_65_fSpecVanish"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1976
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    .line 1977
    return-void
.end method

.method public setFStrike(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2240
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2241
    return-void
.end method

.method public setFTNY(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2720
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNY:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2721
    return-void
.end method

.method public setFTNYCompress(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2880
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNYCompress:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2881
    return-void
.end method

.method public setFTNYFetchTxm(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2900
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fTNYFetchTxm:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2901
    return-void
.end method

.method public setFUndetermine(Z)V
    .locals 0
    .param p1, "field_14_fUndetermine"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 970
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    .line 971
    return-void
.end method

.method public setFUsePgsuSettings(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2420
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fUsePgsuSettings:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2421
    return-void
.end method

.method public setFVanish(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2180
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2181
    return-void
.end method

.method public setFWarichu(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2740
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWarichu:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2741
    return-void
.end method

.method public setFWarichuNoOpenBracket(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2860
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWarichuNoOpenBracket:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2861
    return-void
.end method

.method public setFWebHidden(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2620
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->fWebHidden:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2621
    return-void
.end method

.method public setFcData(I)V
    .locals 0
    .param p1, "field_39_fcData"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1495
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    .line 1496
    return-void
.end method

.method public setFcObj(I)V
    .locals 0
    .param p1, "field_37_fcObj"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1459
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    .line 1460
    return-void
.end method

.method public setFcObjp(I)V
    .locals 0
    .param p1, "field_63_fcObjp"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1927
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    .line 1928
    return-void
.end method

.method public setFcPic(I)V
    .locals 0
    .param p1, "field_36_fcPic"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1441
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    .line 1442
    return-void
.end method

.method public setFtcAscii(I)V
    .locals 0
    .param p1, "field_3_ftcAscii"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 757
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    .line 758
    return-void
.end method

.method public setFtcBi(I)V
    .locals 0
    .param p1, "field_6_ftcBi"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 811
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    .line 812
    return-void
.end method

.method public setFtcFE(I)V
    .locals 0
    .param p1, "field_4_ftcFE"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 775
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    .line 776
    return-void
.end method

.method public setFtcOther(I)V
    .locals 0
    .param p1, "field_5_ftcOther"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 793
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    .line 794
    return-void
.end method

.method public setFtcSym(I)V
    .locals 0
    .param p1, "field_34_ftcSym"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1405
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    .line 1406
    return-void
.end method

.method public setGrpfChp(I)V
    .locals 0
    .param p1, "field_1_grpfChp"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 721
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 722
    return-void
.end method

.method public setHighlight(S)V
    .locals 0
    .param p1, "field_48_Highlight"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1657
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 1658
    return-void
.end method

.method public setHps(I)V
    .locals 0
    .param p1, "field_2_hps"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 739
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    .line 740
    return-void
.end method

.method public setHpsAsci(I)V
    .locals 0
    .param p1, "field_31_hpsAsci"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1351
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    .line 1352
    return-void
.end method

.method public setHpsBi(I)V
    .locals 0
    .param p1, "field_33_hpsBi"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1387
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    .line 1388
    return-void
.end method

.method public setHpsFE(I)V
    .locals 0
    .param p1, "field_32_hpsFE"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1369
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    .line 1370
    return-void
.end method

.method public setHpsKern(I)V
    .locals 0
    .param p1, "field_21_hpsKern"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1152
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    .line 1153
    return-void
.end method

.method public setHpsPos(S)V
    .locals 0
    .param p1, "field_22_hpsPos"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1170
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    .line 1171
    return-void
.end method

.method public setHresi(Lorg/apache/poi/hwpf/model/Hyphenation;)V
    .locals 0
    .param p1, "field_20_hresi"    # Lorg/apache/poi/hwpf/model/Hyphenation;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1134
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lorg/apache/poi/hwpf/model/Hyphenation;

    .line 1135
    return-void
.end method

.method public setHresiOld(Lorg/apache/poi/hwpf/model/Hyphenation;)V
    .locals 0
    .param p1, "field_40_hresiOld"    # Lorg/apache/poi/hwpf/model/Hyphenation;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1513
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lorg/apache/poi/hwpf/model/Hyphenation;

    .line 1514
    return-void
.end method

.method public setIWarichuBracket(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2840
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->iWarichuBracket:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2841
    return-void
.end method

.method public setIbstConflict(I)V
    .locals 0
    .param p1, "field_57_IbstConflict"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1819
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    .line 1820
    return-void
.end method

.method public setIbstDispFldRMark(I)V
    .locals 0
    .param p1, "field_60_ibstDispFldRMark"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1873
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    .line 1874
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_52_ibstPropRMark"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1729
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    .line 1730
    return-void
.end method

.method public setIbstRMark(I)V
    .locals 0
    .param p1, "field_25_ibstRMark"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1224
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    .line 1225
    return-void
.end method

.method public setIbstRMarkDel(I)V
    .locals 0
    .param p1, "field_41_ibstRMarkDel"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1531
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    .line 1532
    return-void
.end method

.method public setIco(B)V
    .locals 0
    .param p1, "field_9_ico"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 865
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    .line 866
    return-void
.end method

.method public setIcoHighlight(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2960
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->icoHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 2961
    return-void
.end method

.method public setIdct(B)V
    .locals 0
    .param p1, "field_17_idct"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1035
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    .line 1036
    return-void
.end method

.method public setIdctHint(B)V
    .locals 0
    .param p1, "field_18_idctHint"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1053
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    .line 1054
    return-void
.end method

.method public setIdslRMReason(I)V
    .locals 0
    .param p1, "field_45_idslRMReason"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1603
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    .line 1604
    return-void
.end method

.method public setIdslReasonDel(I)V
    .locals 0
    .param p1, "field_46_idslReasonDel"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1621
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    .line 1622
    return-void
.end method

.method public setIss(B)V
    .locals 0
    .param p1, "field_15_iss"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 999
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    .line 1000
    return-void
.end method

.method public setIstd(I)V
    .locals 0
    .param p1, "field_44_istd"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1585
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    .line 1586
    return-void
.end method

.method public setItypFELayout(S)V
    .locals 2
    .param p1, "value"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2700
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->itypFELayout:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2701
    return-void
.end method

.method public setKcd(B)V
    .locals 0
    .param p1, "field_13_kcd"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 952
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    .line 953
    return-void
.end method

.method public setKul(B)V
    .locals 0
    .param p1, "field_19_kul"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1116
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    .line 1117
    return-void
.end method

.method public setLTagObj(I)V
    .locals 0
    .param p1, "field_38_lTagObj"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1477
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    .line 1478
    return-void
.end method

.method public setLbrCRJ(B)V
    .locals 0
    .param p1, "field_64_lbrCRJ"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1958
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    .line 1959
    return-void
.end method

.method public setLidDefault(I)V
    .locals 0
    .param p1, "field_11_lidDefault"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 901
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    .line 902
    return-void
.end method

.method public setLidFE(I)V
    .locals 0
    .param p1, "field_12_lidFE"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 919
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    .line 920
    return-void
.end method

.method public setPctCharWidth(I)V
    .locals 0
    .param p1, "field_10_pctCharWidth"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 883
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    .line 884
    return-void
.end method

.method public setSfxtText(B)V
    .locals 0
    .param p1, "field_26_sfxtText"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1261
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    .line 1262
    return-void
.end method

.method public setShd(Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;)V
    .locals 0
    .param p1, "field_23_shd"    # Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1188
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_23_shd:Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    .line 1189
    return-void
.end method

.method public setSpare(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2820
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->spare:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2821
    return-void
.end method

.method public setUfel(S)V
    .locals 0
    .param p1, "field_29_ufel"    # S
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1315
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 1316
    return-void
.end method

.method public setUnused(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2940
    sget-object v0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->unused:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2941
    return-void
.end method

.method public setWCharScale(I)V
    .locals 0
    .param p1, "field_68_wCharScale"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 2030
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    .line 2031
    return-void
.end method

.method public setWConflict(I)V
    .locals 0
    .param p1, "field_56_wConflict"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1801
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    .line 1802
    return-void
.end method

.method public setXchSym(I)V
    .locals 0
    .param p1, "field_35_xchSym"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1423
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    .line 1424
    return-void
.end method

.method public setXstDispFldRMark([B)V
    .locals 0
    .param p1, "field_62_xstDispFldRMark"    # [B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 1909
    iput-object p1, p0, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    .line 1910
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 513
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 514
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[CHP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    const-string/jumbo v1, "    .grpfChp              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getGrpfChp()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    const-string/jumbo v1, "         .fBold                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFBold()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 518
    const-string/jumbo v1, "         .fItalic                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFItalic()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 519
    const-string/jumbo v1, "         .fRMarkDel                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFRMarkDel()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 520
    const-string/jumbo v1, "         .fOutline                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFOutline()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 521
    const-string/jumbo v1, "         .fFldVanish               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFFldVanish()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 522
    const-string/jumbo v1, "         .fSmallCaps               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFSmallCaps()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 523
    const-string/jumbo v1, "         .fCaps                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFCaps()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 524
    const-string/jumbo v1, "         .fVanish                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFVanish()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 525
    const-string/jumbo v1, "         .fRMark                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFRMark()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 526
    const-string/jumbo v1, "         .fSpec                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFSpec()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 527
    const-string/jumbo v1, "         .fStrike                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFStrike()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 528
    const-string/jumbo v1, "         .fObj                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFObj()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 529
    const-string/jumbo v1, "         .fShadow                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFShadow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 530
    const-string/jumbo v1, "         .fLowerCase               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFLowerCase()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 531
    const-string/jumbo v1, "         .fData                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFData()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 532
    const-string/jumbo v1, "         .fOle2                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFOle2()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 533
    const-string/jumbo v1, "         .fEmboss                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFEmboss()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 534
    const-string/jumbo v1, "         .fImprint                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFImprint()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 535
    const-string/jumbo v1, "         .fDStrike                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFDStrike()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 536
    const-string/jumbo v1, "         .fUsePgsuSettings         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFUsePgsuSettings()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 537
    const-string/jumbo v1, "         .fBoldBi                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFBoldBi()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 538
    const-string/jumbo v1, "         .fComplexScripts          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFComplexScripts()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 539
    const-string/jumbo v1, "         .fItalicBi                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFItalicBi()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 540
    const-string/jumbo v1, "         .fBiDi                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFBiDi()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 541
    const-string/jumbo v1, "         .fIcoBi                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFIcoBi()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 542
    const-string/jumbo v1, "         .fNonGlyph                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFNonGlyph()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 543
    const-string/jumbo v1, "         .fBoldOther               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFBoldOther()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 544
    const-string/jumbo v1, "         .fItalicOther             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFItalicOther()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 545
    const-string/jumbo v1, "         .fNoProof                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFNoProof()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 546
    const-string/jumbo v1, "         .fWebHidden               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFWebHidden()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 547
    const-string/jumbo v1, "         .fFitText                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFFitText()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 548
    const-string/jumbo v1, "         .fCalc                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFCalc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 549
    const-string/jumbo v1, "         .fFmtLineProp             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFFmtLineProp()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 550
    const-string/jumbo v1, "    .hps                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHps()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const-string/jumbo v1, "    .ftcAscii             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFtcAscii()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    const-string/jumbo v1, "    .ftcFE                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFtcFE()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const-string/jumbo v1, "    .ftcOther             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFtcOther()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    const-string/jumbo v1, "    .ftcBi                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFtcBi()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    const-string/jumbo v1, "    .dxaSpace             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getDxaSpace()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string/jumbo v1, "    .cv                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getCv()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const-string/jumbo v1, "    .ico                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIco()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    const-string/jumbo v1, "    .pctCharWidth         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getPctCharWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    const-string/jumbo v1, "    .lidDefault           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getLidDefault()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    const-string/jumbo v1, "    .lidFE                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getLidFE()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    const-string/jumbo v1, "    .kcd                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 573
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getKcd()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    const-string/jumbo v1, "    .fUndetermine         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFUndetermine()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    const-string/jumbo v1, "    .iss                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIss()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    const-string/jumbo v1, "    .fSpecSymbol          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFSpecSymbol()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    const-string/jumbo v1, "    .idct                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIdct()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 582
    const-string/jumbo v1, "    .idctHint             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIdctHint()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    const-string/jumbo v1, "    .kul                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getKul()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    const-string/jumbo v1, "    .hresi                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHresi()Lorg/apache/poi/hwpf/model/Hyphenation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    const-string/jumbo v1, "    .hpsKern              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsKern()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    const-string/jumbo v1, "    .hpsPos               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 592
    const-string/jumbo v1, "    .shd                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getShd()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    const-string/jumbo v1, "    .brc                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getBrc()Lorg/apache/poi/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    const-string/jumbo v1, "    .ibstRMark            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIbstRMark()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    const-string/jumbo v1, "    .sfxtText             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getSfxtText()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    const-string/jumbo v1, "    .fDblBdr              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFDblBdr()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    const-string/jumbo v1, "    .fBorderWS            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFBorderWS()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    const-string/jumbo v1, "    .ufel                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getUfel()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    const-string/jumbo v1, "         .itypFELayout             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getItypFELayout()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 607
    const-string/jumbo v1, "         .fTNY                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFTNY()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 608
    const-string/jumbo v1, "         .fWarichu                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFWarichu()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 609
    const-string/jumbo v1, "         .fKumimoji                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFKumimoji()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 610
    const-string/jumbo v1, "         .fRuby                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFRuby()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 611
    const-string/jumbo v1, "         .fLSFitText               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFLSFitText()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 612
    const-string/jumbo v1, "         .spare                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getSpare()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 613
    const-string/jumbo v1, "    .copt                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getCopt()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    const-string/jumbo v1, "         .iWarichuBracket          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIWarichuBracket()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 616
    const-string/jumbo v1, "         .fWarichuNoOpenBracket     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFWarichuNoOpenBracket()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 617
    const-string/jumbo v1, "         .fTNYCompress             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFTNYCompress()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 618
    const-string/jumbo v1, "         .fTNYFetchTxm             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFTNYFetchTxm()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 619
    const-string/jumbo v1, "         .fCellFitText             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFCellFitText()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 620
    const-string/jumbo v1, "         .unused                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isUnused()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 621
    const-string/jumbo v1, "    .hpsAsci              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsAsci()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    const-string/jumbo v1, "    .hpsFE                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsFE()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    const-string/jumbo v1, "    .hpsBi                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHpsBi()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    const-string/jumbo v1, "    .ftcSym               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFtcSym()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 629
    const-string/jumbo v1, "    .xchSym               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getXchSym()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 631
    const-string/jumbo v1, "    .fcPic                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 632
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFcPic()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    const-string/jumbo v1, "    .fcObj                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFcObj()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635
    const-string/jumbo v1, "    .lTagObj              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 636
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getLTagObj()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 637
    const-string/jumbo v1, "    .fcData               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFcData()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    const-string/jumbo v1, "    .hresiOld             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHresiOld()Lorg/apache/poi/hwpf/model/Hyphenation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 641
    const-string/jumbo v1, "    .ibstRMarkDel         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 642
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIbstRMarkDel()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 643
    const-string/jumbo v1, "    .dttmRMark            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getDttmRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    const-string/jumbo v1, "    .dttmRMarkDel         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getDttmRMarkDel()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 647
    const-string/jumbo v1, "    .istd                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 648
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIstd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    const-string/jumbo v1, "    .idslRMReason         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIdslRMReason()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    const-string/jumbo v1, "    .idslReasonDel        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIdslReasonDel()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    const-string/jumbo v1, "    .cpg                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 654
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getCpg()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    const-string/jumbo v1, "    .Highlight            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getHighlight()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    const-string/jumbo v1, "         .icoHighlight             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIcoHighlight()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 658
    const-string/jumbo v1, "         .fHighlight               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFHighlight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 659
    const-string/jumbo v1, "    .CharsetFlags         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getCharsetFlags()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    const-string/jumbo v1, "         .fChsDiff                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFChsDiff()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 662
    const-string/jumbo v1, "         .fMacChs                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->isFMacChs()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 663
    const-string/jumbo v1, "    .chse                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getChse()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    const-string/jumbo v1, "    .fPropRMark           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 666
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFPropRMark()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 667
    const-string/jumbo v1, "    .ibstPropRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 668
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIbstPropRMark()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669
    const-string/jumbo v1, "    .dttmPropRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getDttmPropRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    const-string/jumbo v1, "    .fConflictOrig        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 672
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFConflictOrig()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673
    const-string/jumbo v1, "    .fConflictOtherDel    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFConflictOtherDel()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 675
    const-string/jumbo v1, "    .wConflict            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 676
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getWConflict()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 677
    const-string/jumbo v1, "    .IbstConflict         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIbstConflict()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    const-string/jumbo v1, "    .dttmConflict         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getDttmConflict()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    const-string/jumbo v1, "    .fDispFldRMark        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFDispFldRMark()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    const-string/jumbo v1, "    .ibstDispFldRMark     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getIbstDispFldRMark()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 685
    const-string/jumbo v1, "    .dttmDispFldRMark     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getDttmDispFldRMark()Lorg/apache/poi/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    const-string/jumbo v1, "    .xstDispFldRMark      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getXstDispFldRMark()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    const-string/jumbo v1, "    .fcObjp               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFcObjp()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    const-string/jumbo v1, "    .lbrCRJ               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getLbrCRJ()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 693
    const-string/jumbo v1, "    .fSpecVanish          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFSpecVanish()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    const-string/jumbo v1, "    .fHasOldProps         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFHasOldProps()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    const-string/jumbo v1, "    .fSdtVanish           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getFSdtVanish()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    const-string/jumbo v1, "    .wCharScale           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/CHPAbstractType;->getWCharScale()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    const-string/jumbo v1, "[/CHP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
