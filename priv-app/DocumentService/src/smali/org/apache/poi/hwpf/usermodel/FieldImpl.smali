.class Lorg/apache/poi/hwpf/usermodel/FieldImpl;
.super Ljava/lang/Object;
.source "FieldImpl.java"

# interfaces
.implements Lorg/apache/poi/hwpf/usermodel/Field;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

.field private separatorPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

.field private startPlex:Lorg/apache/poi/hwpf/model/PlexOfField;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;Lorg/apache/poi/hwpf/model/PlexOfField;)V
    .locals 3
    .param p1, "startPlex"    # Lorg/apache/poi/hwpf/model/PlexOfField;
    .param p2, "separatorPlex"    # Lorg/apache/poi/hwpf/model/PlexOfField;
    .param p3, "endPlex"    # Lorg/apache/poi/hwpf/model/PlexOfField;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    if-nez p1, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "startPlex == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    if-nez p3, :cond_1

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "endPlex == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v0

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "startPlex ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 45
    const-string/jumbo v2, ") is not type of FIELD_BEGIN"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    if-eqz p2, :cond_3

    .line 48
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v0

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "separatorPlex"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 50
    const-string/jumbo v2, ") is not type of FIELD_SEPARATOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_3
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getBoundaryType()I

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_4

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "endPlex ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 54
    const-string/jumbo v2, ") is not type of FIELD_END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_4
    iput-object p1, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->startPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    .line 57
    iput-object p2, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->separatorPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    .line 58
    iput-object p3, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    .line 59
    return-void
.end method


# virtual methods
.method public firstSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->hasSeparator()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkStartOffset()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkSeparatorOffset()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/FieldImpl$1;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkStartOffset()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 69
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkSeparatorOffset()I

    move-result v2

    .line 68
    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/poi/hwpf/usermodel/FieldImpl$1;-><init>(Lorg/apache/poi/hwpf/usermodel/FieldImpl;IILorg/apache/poi/hwpf/usermodel/Range;)V

    goto :goto_0

    .line 79
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkStartOffset()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkEndOffset()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 82
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/FieldImpl$2;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkStartOffset()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkEndOffset()I

    move-result v2

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/poi/hwpf/usermodel/FieldImpl$2;-><init>(Lorg/apache/poi/hwpf/usermodel/FieldImpl;IILorg/apache/poi/hwpf/usermodel/Range;)V

    goto :goto_0
.end method

.method public getFieldEndOffset()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getFieldStartOffset()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->startPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v0

    return v0
.end method

.method public getMarkEndCharacterRun(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 117
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkEndOffset()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkEndOffset()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v1, v2, p1}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 118
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v0

    .line 117
    return-object v0
.end method

.method public getMarkEndOffset()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v0

    return v0
.end method

.method public getMarkSeparatorCharacterRun(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 131
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->hasSeparator()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkSeparatorOffset()I

    move-result v1

    .line 135
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkSeparatorOffset()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 134
    invoke-direct {v0, v1, v2, p1}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 135
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v0

    goto :goto_0
.end method

.method public getMarkSeparatorOffset()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->separatorPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v0

    return v0
.end method

.method public getMarkStartCharacterRun(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 149
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkStartOffset()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkStartOffset()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v1, v2, p1}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    .line 150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/usermodel/Range;->getCharacterRun(I)Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    move-result-object v0

    .line 149
    return-object v0
.end method

.method public getMarkStartOffset()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->startPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFcStart()I

    move-result v0

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->startPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->getFieldType()I

    move-result v0

    return v0
.end method

.method public hasSeparator()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->separatorPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHasSep()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFHasSep()Z

    move-result v0

    return v0
.end method

.method public isLocked()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFLocked()Z

    move-result v0

    return v0
.end method

.method public isNested()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFNested()Z

    move-result v0

    return v0
.end method

.method public isPrivateResult()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFPrivateResult()Z

    move-result v0

    return v0
.end method

.method public isResultDirty()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFResultDirty()Z

    move-result v0

    return v0
.end method

.method public isResultEdited()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFResultEdited()Z

    move-result v0

    return v0
.end method

.method public isZombieEmbed()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->endPlex:Lorg/apache/poi/hwpf/model/PlexOfField;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/PlexOfField;->getFld()Lorg/apache/poi/hwpf/model/FieldDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FieldDescriptor;->isFZombieEmbed()Z

    move-result v0

    return v0
.end method

.method public secondSubrange(Lorg/apache/poi/hwpf/usermodel/Range;)Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 208
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->hasSeparator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkSeparatorOffset()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkEndOffset()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 210
    :cond_0
    const/4 v0, 0x0

    .line 212
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/FieldImpl$3;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkSeparatorOffset()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getMarkEndOffset()I

    move-result v2

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/poi/hwpf/usermodel/FieldImpl$3;-><init>(Lorg/apache/poi/hwpf/usermodel/FieldImpl;IILorg/apache/poi/hwpf/usermodel/Range;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Field ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getFieldStartOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getFieldEndOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 227
    const-string/jumbo v1, "] (type: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 228
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/FieldImpl;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
