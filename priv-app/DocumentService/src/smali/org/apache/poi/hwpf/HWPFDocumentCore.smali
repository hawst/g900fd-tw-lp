.class public abstract Lorg/apache/poi/hwpf/HWPFDocumentCore;
.super Lorg/apache/poi/POIDocument;
.source "HWPFDocumentCore.java"


# static fields
.field protected static final STREAM_OBJECT_POOL:Ljava/lang/String; = "ObjectPool"

.field protected static final STREAM_WORD_DOCUMENT:Ljava/lang/String; = "WordDocument"


# instance fields
.field protected _cbt:Lorg/apache/poi/hwpf/model/CHPBinTable;

.field protected _fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

.field protected _ft:Lorg/apache/poi/hwpf/model/FontTable;

.field protected _lt:Lorg/apache/poi/hwpf/model/ListTables;

.field protected _mainStream:[B

.field protected _mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

.field protected _objectPool:Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;

.field protected _pbt:Lorg/apache/poi/hwpf/model/PAPBinTable;

.field protected _ss:Lorg/apache/poi/hwpf/model/StyleSheet;

.field protected _st:Lorg/apache/poi/hwpf/model/SectionTable;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0, v0}, Lorg/apache/poi/POIDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 56
    iput-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 88
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "istream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {p1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 127
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 6
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lorg/apache/poi/POIDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 56
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 156
    const-string/jumbo v4, "WordDocument"

    invoke-virtual {p1, v4}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v0

    .line 155
    check-cast v0, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 157
    .local v0, "documentProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v4

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 159
    const-string/jumbo v4, "WordDocument"

    invoke-virtual {p1, v4}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 161
    iget-object v4, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    iget-object v5, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    invoke-virtual {v4, v5}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    move-result v3

    .line 162
    .local v3, "read":I
    if-gez v3, :cond_0

    .line 163
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 167
    :cond_0
    new-instance v4, Lorg/apache/poi/hwpf/model/FileInformationBlock;

    iget-object v5, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    invoke-direct {v4, v5}, Lorg/apache/poi/hwpf/model/FileInformationBlock;-><init>([B)V

    iput-object v4, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    .line 172
    :try_start_0
    const-string/jumbo v4, "ObjectPool"

    invoke-virtual {p1, v4}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v2

    .line 171
    check-cast v2, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    .local v2, "objectPoolEntry":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    :goto_0
    new-instance v4, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;

    invoke-direct {v4, v2}, Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    iput-object v4, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_objectPool:Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;

    .line 177
    return-void

    .line 173
    .end local v2    # "objectPoolEntry":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    :catch_0
    move-exception v1

    .line 174
    .local v1, "exc":Ljava/io/FileNotFoundException;
    const/4 v2, 0x0

    .restart local v2    # "objectPoolEntry":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "pfilesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 139
    return-void
.end method

.method public static verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .locals 5
    .param p0, "istream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x6

    .line 96
    new-instance v2, Ljava/io/PushbackInputStream;

    invoke-direct {v2, p0, v3}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 97
    .local v2, "pis":Ljava/io/PushbackInputStream;
    new-array v0, v3, [B

    .line 99
    .local v0, "first6":[B
    invoke-virtual {v2, v0}, Ljava/io/PushbackInputStream;->read([B)I

    move-result v1

    .line 100
    .local v1, "len":I
    if-gez v1, :cond_0

    .line 101
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3

    .line 105
    :cond_0
    const/4 v3, 0x0

    aget-byte v3, v0, v3

    const/16 v4, 0x7b

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    aget-byte v3, v0, v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_1

    const/4 v3, 0x2

    aget-byte v3, v0, v3

    const/16 v4, 0x72

    if-ne v3, v4, :cond_1

    .line 106
    const/4 v3, 0x3

    aget-byte v3, v0, v3

    const/16 v4, 0x74

    if-ne v3, v4, :cond_1

    const/4 v3, 0x4

    aget-byte v3, v0, v3

    const/16 v4, 0x66

    if-ne v3, v4, :cond_1

    .line 107
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "The document is really a RTF file"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 112
    :cond_1
    invoke-virtual {v2, v0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 113
    new-instance v3, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v3, v2}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    return-object v3
.end method


# virtual methods
.method public getCharacterTable()Lorg/apache/poi/hwpf/model/CHPBinTable;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_cbt:Lorg/apache/poi/hwpf/model/CHPBinTable;

    return-object v0
.end method

.method public getDocumentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/HWPFDocumentCore;->getText()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    return-object v0
.end method

.method public getFontTable()Lorg/apache/poi/hwpf/model/FontTable;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_ft:Lorg/apache/poi/hwpf/model/FontTable;

    return-object v0
.end method

.method public getListTables()Lorg/apache/poi/hwpf/model/ListTables;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_lt:Lorg/apache/poi/hwpf/model/ListTables;

    return-object v0
.end method

.method public getObjectsPool()Lorg/apache/poi/hwpf/usermodel/ObjectsPool;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_objectPool:Lorg/apache/poi/hwpf/usermodel/ObjectPoolImpl;

    return-object v0
.end method

.method public abstract getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;
.end method

.method public getParagraphTable()Lorg/apache/poi/hwpf/model/PAPBinTable;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_pbt:Lorg/apache/poi/hwpf/model/PAPBinTable;

    return-object v0
.end method

.method public abstract getRange()Lorg/apache/poi/hwpf/usermodel/Range;
.end method

.method public getSectionTable()Lorg/apache/poi/hwpf/model/SectionTable;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_st:Lorg/apache/poi/hwpf/model/SectionTable;

    return-object v0
.end method

.method public getStyleSheet()Lorg/apache/poi/hwpf/model/StyleSheet;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFDocumentCore;->_ss:Lorg/apache/poi/hwpf/model/StyleSheet;

    return-object v0
.end method

.method public abstract getText()Ljava/lang/StringBuilder;
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation
.end method

.method public abstract getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;
.end method
