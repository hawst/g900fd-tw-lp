.class public final enum Lorg/apache/poi/hwpf/usermodel/PictureType;
.super Ljava/lang/Enum;
.source "PictureType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hwpf/usermodel/PictureType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum GIF:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

.field public static final enum WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;


# instance fields
.field private _extension:Ljava/lang/String;

.field private _mime:Ljava/lang/String;

.field private _signatures:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v1, "BMP"

    const-string/jumbo v3, "image/bmp"

    const-string/jumbo v4, "bmp"

    new-array v5, v9, [[B

    new-array v6, v10, [B

    fill-array-data v6, :array_0

    aput-object v6, v5, v2

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 28
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "EMF"

    const-string/jumbo v6, "image/x-emf"

    const-string/jumbo v7, "emf"

    new-array v8, v9, [[B

    new-array v0, v12, [B

    aput-byte v9, v0, v2

    aput-object v0, v8, v2

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 30
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "GIF"

    const-string/jumbo v6, "image/gif"

    const-string/jumbo v7, "gif"

    new-array v8, v9, [[B

    new-array v0, v11, [B

    fill-array-data v0, :array_1

    aput-object v0, v8, v2

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->GIF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 32
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "JPEG"

    const-string/jumbo v6, "image/jpeg"

    const-string/jumbo v7, "jpg"

    new-array v8, v9, [[B

    new-array v0, v10, [B

    fill-array-data v0, :array_2

    aput-object v0, v8, v2

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 34
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "PICT"

    const-string/jumbo v6, "image/pict"

    const-string/jumbo v7, ".pict"

    new-array v8, v2, [[B

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 36
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "PNG"

    const/4 v5, 0x5

    const-string/jumbo v6, "image/png"

    const-string/jumbo v7, "png"

    new-array v8, v9, [[B

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_3

    .line 37
    aput-object v0, v8, v2

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    .line 36
    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 39
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "TIFF"

    const/4 v5, 0x6

    const-string/jumbo v6, "image/tiff"

    const-string/jumbo v7, "tiff"

    new-array v8, v10, [[B

    new-array v0, v12, [B

    const/16 v1, 0x49

    aput-byte v1, v0, v2

    const/16 v1, 0x49

    aput-byte v1, v0, v9

    const/16 v1, 0x2a

    aput-byte v1, v0, v10

    aput-object v0, v8, v2

    .line 40
    new-array v0, v12, [B

    const/16 v1, 0x4d

    aput-byte v1, v0, v2

    const/16 v1, 0x4d

    aput-byte v1, v0, v9

    const/16 v1, 0x2a

    aput-byte v1, v0, v11

    aput-object v0, v8, v9

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    .line 39
    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 42
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "UNKNOWN"

    const/4 v5, 0x7

    const-string/jumbo v6, "image/unknown"

    const-string/jumbo v7, ""

    new-array v8, v2, [[B

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 44
    new-instance v3, Lorg/apache/poi/hwpf/usermodel/PictureType;

    const-string/jumbo v4, "WMF"

    const/16 v5, 0x8

    const-string/jumbo v6, "image/x-wmf"

    const-string/jumbo v7, "wmf"

    new-array v8, v10, [[B

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [B

    const/16 v1, -0x29

    aput-byte v1, v0, v2

    const/16 v1, -0x33

    aput-byte v1, v0, v9

    const/16 v1, -0x3a

    aput-byte v1, v0, v10

    const/16 v1, -0x66

    aput-byte v1, v0, v11

    aput-object v0, v8, v2

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [B

    aput-byte v9, v0, v2

    const/16 v1, 0x9

    aput-byte v1, v0, v10

    const/4 v1, 0x5

    aput-byte v11, v0, v1

    aput-object v0, v8, v9

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/usermodel/PictureType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V

    .line 44
    sput-object v3, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    .line 24
    const/16 v0, 0x9

    new-array v0, v0, [Lorg/apache/poi/hwpf/usermodel/PictureType;

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->BMP:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->EMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v1, v0, v9

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->GIF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v1, v0, v10

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->JPEG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v1, v0, v11

    sget-object v1, Lorg/apache/poi/hwpf/usermodel/PictureType;->PICT:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->PNG:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->TIFF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/poi/hwpf/usermodel/PictureType;->WMF:Lorg/apache/poi/hwpf/usermodel/PictureType;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/hwpf/usermodel/PictureType;->ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/PictureType;

    return-void

    .line 26
    nop

    :array_0
    .array-data 1
        0x42t
        0x4dt
    .end array-data

    .line 30
    nop

    :array_1
    .array-data 1
        0x47t
        0x49t
        0x46t
    .end array-data

    .line 32
    :array_2
    .array-data 1
        -0x1t
        -0x28t
    .end array-data

    .line 36
    nop

    :array_3
    .array-data 1
        -0x77t
        0x50t
        0x4et
        0x47t
        0xdt
        0xat
        0x1at
        0xat
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[[B)V
    .locals 0
    .param p3, "mime"    # Ljava/lang/String;
    .param p4, "extension"    # Ljava/lang/String;
    .param p5, "signatures"    # [[B

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput-object p3, p0, Lorg/apache/poi/hwpf/usermodel/PictureType;->_mime:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Lorg/apache/poi/hwpf/usermodel/PictureType;->_extension:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lorg/apache/poi/hwpf/usermodel/PictureType;->_signatures:[[B

    .line 82
    return-void
.end method

.method public static findMatchingType([B)Lorg/apache/poi/hwpf/usermodel/PictureType;
    .locals 10
    .param p0, "pictureContent"    # [B

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-static {}, Lorg/apache/poi/hwpf/usermodel/PictureType;->values()[Lorg/apache/poi/hwpf/usermodel/PictureType;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_0
    if-lt v4, v6, :cond_1

    .line 56
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/PictureType;->UNKNOWN:Lorg/apache/poi/hwpf/usermodel/PictureType;

    :cond_0
    return-object v0

    .line 50
    :cond_1
    aget-object v0, v5, v4

    .line 51
    .local v0, "pictureType":Lorg/apache/poi/hwpf/usermodel/PictureType;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/usermodel/PictureType;->getSignatures()[[B

    move-result-object v7

    array-length v8, v7

    move v2, v3

    :goto_1
    if-lt v2, v8, :cond_2

    .line 50
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 51
    :cond_2
    aget-object v1, v7, v2

    .line 52
    .local v1, "signature":[B
    invoke-static {p0, v1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->matchSignature([B[B)Z

    move-result v9

    if-nez v9, :cond_0

    .line 51
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private static matchSignature([B[B)Z
    .locals 4
    .param p0, "pictureData"    # [B
    .param p1, "signature"    # [B

    .prologue
    const/4 v1, 0x0

    .line 61
    array-length v2, p0

    array-length v3, p1

    if-ge v2, v3, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p1

    if-lt v0, v2, :cond_2

    .line 68
    const/4 v1, 0x1

    goto :goto_0

    .line 65
    :cond_2
    aget-byte v2, p0, v0

    aget-byte v3, p1, v0

    if-ne v2, v3, :cond_0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hwpf/usermodel/PictureType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/PictureType;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/hwpf/usermodel/PictureType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hwpf/usermodel/PictureType;->ENUM$VALUES:[Lorg/apache/poi/hwpf/usermodel/PictureType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hwpf/usermodel/PictureType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/PictureType;->_extension:Ljava/lang/String;

    return-object v0
.end method

.method public getMime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/PictureType;->_mime:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatures()[[B
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/PictureType;->_signatures:[[B

    return-object v0
.end method

.method public matchSignature([B)Z
    .locals 6
    .param p1, "pictureData"    # [B

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/PictureType;->getSignatures()[[B

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 104
    :goto_1
    return v1

    .line 101
    :cond_0
    aget-object v0, v3, v2

    .line 102
    .local v0, "signature":[B
    invoke-static {v0, p1}, Lorg/apache/poi/hwpf/usermodel/PictureType;->matchSignature([B[B)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 103
    const/4 v1, 0x1

    goto :goto_1

    .line 101
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
