.class public Lorg/apache/poi/hwpf/model/SinglentonTextPiece;
.super Lorg/apache/poi/hwpf/model/TextPiece;
.source "SinglentonTextPiece.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/StringBuilder;)V
    .locals 5
    .param p1, "buffer"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 29
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "UTF-16LE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 30
    new-instance v2, Lorg/apache/poi/hwpf/model/PieceDescriptor;

    const/16 v3, 0x8

    new-array v3, v3, [B

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hwpf/model/PieceDescriptor;-><init>([BI)V

    invoke-direct {p0, v4, v0, v1, v2}, Lorg/apache/poi/hwpf/model/TextPiece;-><init>(II[BLorg/apache/poi/hwpf/model/PieceDescriptor;)V

    .line 31
    return-void
.end method


# virtual methods
.method public bytesLength()I
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/SinglentonTextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public characterLength()I
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/SinglentonTextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    return v0
.end method

.method public getCP()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/SinglentonTextPiece;->characterLength()I

    move-result v0

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "SinglentonTextPiece ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/SinglentonTextPiece;->characterLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " chars)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
