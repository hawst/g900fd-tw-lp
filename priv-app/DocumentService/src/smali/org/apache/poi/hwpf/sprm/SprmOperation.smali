.class public final Lorg/apache/poi/hwpf/sprm/SprmOperation;
.super Ljava/lang/Object;
.source "SprmOperation.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final BITFIELD_OP:Lorg/apache/poi/util/BitField;

.field private static final BITFIELD_SIZECODE:Lorg/apache/poi/util/BitField;

.field private static final BITFIELD_TYPE:Lorg/apache/poi/util/BitField;

.field public static final PAP_TYPE:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final SPRM_LONG_PARAGRAPH:S = -0x39ebs

.field private static final SPRM_LONG_TABLE:S = -0x29f8s

.field public static final TAP_TYPE:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_CHP:I = 0x2

.field public static final TYPE_PAP:I = 0x1

.field public static final TYPE_PIC:I = 0x3

.field public static final TYPE_SEP:I = 0x4

.field public static final TYPE_TAP:I = 0x5


# instance fields
.field private _gOffset:I

.field private _grpprl:[B

.field private _offset:I

.field private _size:I

.field private _value:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x1ff

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    .line 35
    sput-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_OP:Lorg/apache/poi/util/BitField;

    .line 38
    const v0, 0xe000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    .line 37
    sput-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_SIZECODE:Lorg/apache/poi/util/BitField;

    .line 42
    const/16 v0, 0x1c00

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    .line 41
    sput-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_TYPE:Lorg/apache/poi/util/BitField;

    .line 56
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    .line 77
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_value:S

    .line 78
    iput p2, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_offset:I

    .line 79
    add-int/lit8 v0, p2, 0x2

    iput v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    .line 80
    iget-short v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_value:S

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->initSize(S)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_size:I

    .line 81
    return-void
.end method

.method public static getOperationFromOpcode(S)I
    .locals 1
    .param p0, "opcode"    # S

    .prologue
    .line 60
    sget-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_OP:Lorg/apache/poi/util/BitField;

    invoke-virtual {v0, p0}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method public static getTypeFromOpcode(S)I
    .locals 1
    .param p0, "opcode"    # S

    .prologue
    .line 65
    sget-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_TYPE:Lorg/apache/poi/util/BitField;

    invoke-virtual {v0, p0}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method private initSize(S)I
    .locals 5
    .param p1, "sprm"    # S

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 187
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 188
    const-string/jumbo v3, "SPRM contains an invalid size code"

    .line 187
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 167
    :pswitch_0
    const/4 v1, 0x3

    .line 185
    :goto_0
    return v1

    .line 171
    :pswitch_1
    const/4 v1, 0x4

    goto :goto_0

    .line 173
    :pswitch_2
    const/4 v1, 0x6

    goto :goto_0

    .line 175
    :pswitch_3
    iget v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    .line 176
    .local v0, "offset":I
    const/16 v2, -0x29f8

    if-eq p1, v2, :cond_0

    const/16 v2, -0x39eb

    if-ne p1, v2, :cond_1

    .line 178
    :cond_0
    const v2, 0xffff

    iget-object v3, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    invoke-static {v3, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    and-int/2addr v2, v3

    add-int/lit8 v1, v2, 0x3

    .line 180
    .local v1, "retVal":I
    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    goto :goto_0

    .line 183
    .end local v1    # "retVal":I
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v3, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v1, v2, 0x3

    goto :goto_0

    .line 185
    .end local v0    # "offset":I
    :pswitch_4
    const/4 v1, 0x5

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    return-object v0
.end method

.method public getGrpprlOffset()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    return v0
.end method

.method public getOperand()I
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 102
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 132
    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 133
    const-string/jumbo v5, "SPRM contains an invalid size code"

    .line 132
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 106
    :pswitch_0
    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    aget-byte v4, v4, v5

    .line 130
    :goto_0
    return v4

    .line 110
    :pswitch_1
    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    goto :goto_0

    .line 112
    :pswitch_2
    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    goto :goto_0

    .line 115
    :pswitch_3
    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v5, v5, 0x1

    aget-byte v2, v4, v5

    .line 118
    .local v2, "operandLength":B
    new-array v0, v6, [B

    .line 119
    .local v0, "codeBytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_0

    .line 123
    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    goto :goto_0

    .line 120
    :cond_0
    iget v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/2addr v4, v1

    iget-object v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    array-length v5, v5

    if-ge v4, v5, :cond_1

    .line 121
    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v1

    aget-byte v4, v4, v5

    aput-byte v4, v0, v1

    .line 119
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    .end local v0    # "codeBytes":[B
    .end local v1    # "i":I
    .end local v2    # "operandLength":B
    :pswitch_4
    new-array v3, v6, [B

    .line 126
    .local v3, "threeByteInt":[B
    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    aget-byte v4, v4, v5

    aput-byte v4, v3, v7

    .line 127
    const/4 v4, 0x1

    iget-object v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v6, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v6, v6, 0x1

    aget-byte v5, v5, v6

    aput-byte v5, v3, v4

    .line 128
    const/4 v4, 0x2

    iget-object v5, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v6, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v6, v6, 0x2

    aget-byte v5, v5, v6

    aput-byte v5, v3, v4

    .line 129
    const/4 v4, 0x3

    aput-byte v7, v3, v4

    .line 130
    invoke-static {v3, v7}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getOperandShortSigned()S
    .locals 3

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Current SPRM doesn\'t have signed short operand: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method public getOperation()I
    .locals 2

    .prologue
    .line 148
    sget-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_OP:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method public getSizeCode()I
    .locals 2

    .prologue
    .line 153
    sget-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_SIZECODE:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method public getType()I
    .locals 2

    .prologue
    .line 158
    sget-object v0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->BITFIELD_TYPE:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_size:I

    return v0
.end method

.method public toByteArray()[B
    .locals 5

    .prologue
    .line 85
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v1

    new-array v0, v1, [B

    .line 86
    .local v0, "result":[B
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_offset:I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v4

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 201
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v2, "[SPRM] (0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    iget-short v2, p0, Lorg/apache/poi/hwpf/sprm/SprmOperation;->_value:S

    const v3, 0xffff

    and-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 208
    :catch_0
    move-exception v0

    .line 210
    .local v0, "exc":Ljava/lang/Exception;
    const-string/jumbo v2, "(error)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
