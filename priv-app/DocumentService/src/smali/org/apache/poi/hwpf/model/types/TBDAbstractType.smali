.class public abstract Lorg/apache/poi/hwpf/model/types/TBDAbstractType;
.super Ljava/lang/Object;
.source "TBDAbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static jc:Lorg/apache/poi/util/BitField;

.field private static reserved:Lorg/apache/poi/util/BitField;

.field private static tlc:Lorg/apache/poi/util/BitField;


# instance fields
.field protected field_1_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->jc:Lorg/apache/poi/util/BitField;

    .line 45
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0x38

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->tlc:Lorg/apache/poi/util/BitField;

    .line 46
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xc0

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->reserved:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 54
    add-int/lit8 v0, p2, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    .line 55
    return-void
.end method

.method public getJc()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->jc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getReserved()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->reserved:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getTlc()B
    .locals 2
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->tlc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getValue()B
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 90
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 59
    add-int/lit8 v0, p2, 0x0

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    aput-byte v1, p1, v0

    .line 60
    return-void
.end method

.method public setJc(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->jc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    .line 110
    return-void
.end method

.method public setReserved(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 149
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->reserved:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    .line 150
    return-void
.end method

.method public setTlc(B)V
    .locals 2
    .param p1, "value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->tlc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    .line 130
    return-void
.end method

.method public setValue(B)V
    .locals 0
    .param p1, "field_1_value"    # B
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 99
    iput-byte p1, p0, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->field_1_value:B

    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[TBD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string/jumbo v1, "    .value                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->getValue()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v1, "         .jc                       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->getJc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    const-string/jumbo v1, "         .tlc                      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->getTlc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v1, "         .reserved                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/TBDAbstractType;->getReserved()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    const-string/jumbo v1, "[/TBD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
