.class public Lorg/apache/poi/hwpf/converter/WordToTextConverter;
.super Lorg/apache/poi/hwpf/converter/AbstractWordConverter;
.source "WordToTextConverter.java"


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private noteCounters:Ljava/util/concurrent/atomic/AtomicInteger;

.field private notes:Lorg/w3c/dom/Element;

.field private outputSummaryInformation:Z

.field private final textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 64
    sput-object v0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 154
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->noteCounters:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->outputSummaryInformation:Z

    .line 171
    new-instance v0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 172
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;-><init>(Lorg/w3c/dom/Document;)V

    .line 171
    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 174
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/converter/TextDocumentFacade;)V
    .locals 2
    .param p1, "textDocumentFacade"    # Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .prologue
    .line 188
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 154
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->noteCounters:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->outputSummaryInformation:Z

    .line 190
    iput-object p1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 191
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 183
    invoke-direct {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;-><init>()V

    .line 154
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->noteCounters:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->outputSummaryInformation:Z

    .line 185
    new-instance v0, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;-><init>(Lorg/w3c/dom/Document;)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 186
    return-void
.end method

.method public static getText(Ljava/io/File;)Ljava/lang/String;
    .locals 2
    .param p0, "docFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    .line 77
    .local v0, "wordDocument":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getText(Lorg/apache/poi/hwpf/HWPFDocumentCore;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getText(Lorg/apache/poi/hwpf/HWPFDocumentCore;)Ljava/lang/String;
    .locals 2
    .param p0, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;

    .line 84
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v1

    .line 83
    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;-><init>(Lorg/w3c/dom/Document;)V

    .line 86
    .local v0, "wordToTextConverter":Lorg/apache/poi/hwpf/converter/WordToTextConverter;
    invoke-virtual {v0, p0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 87
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getText()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getText(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/lang/String;
    .locals 2
    .param p0, "root"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->loadDoc(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    .line 70
    .local v0, "wordDocument":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getText(Lorg/apache/poi/hwpf/HWPFDocumentCore;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static main([Ljava/lang/String;)V
    .locals 12
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 101
    array-length v8, p0

    const/4 v9, 0x2

    if-ge v8, v9, :cond_1

    .line 103
    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 104
    const-string/jumbo v9, "Usage: WordToTextConverter <inputFile.doc> <saveTo.txt>"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const/4 v3, 0x0

    .line 114
    .local v3, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v8, Ljava/io/File;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->process(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 116
    .local v0, "doc":Lorg/w3c/dom/Document;
    new-instance v4, Ljava/io/FileWriter;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v4, v8}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    .end local v3    # "out":Ljava/io/FileWriter;
    .local v4, "out":Ljava/io/FileWriter;
    :try_start_1
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v0}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 118
    .local v1, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v6, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v6, v4}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 120
    .local v6, "streamResult":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v7

    .line 121
    .local v7, "tf":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v7}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v5

    .line 123
    .local v5, "serializer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v8, "encoding"

    const-string/jumbo v9, "UTF-8"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string/jumbo v8, "indent"

    const-string/jumbo v9, "no"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string/jumbo v8, "method"

    const-string/jumbo v9, "text"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-virtual {v5, v1, v6}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 134
    if-eqz v4, :cond_3

    .line 136
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 137
    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 129
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 134
    if-eqz v3, :cond_0

    .line 136
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 137
    :catch_1
    move-exception v2

    .line 138
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 133
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 134
    :goto_2
    if-eqz v3, :cond_2

    .line 136
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 140
    :cond_2
    :goto_3
    throw v8

    .line 137
    :catch_2
    move-exception v2

    .line 138
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 137
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .restart local v4    # "out":Ljava/io/FileWriter;
    .restart local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .restart local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .restart local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_3
    move-exception v2

    .line 138
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_0

    .line 133
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v3    # "out":Ljava/io/FileWriter;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .line 129
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_1
.end method

.method static process(Ljava/io/File;)Lorg/w3c/dom/Document;
    .locals 3
    .param p0, "docFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 146
    invoke-static {p0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->loadDoc(Ljava/io/File;)Lorg/apache/poi/hwpf/HWPFDocumentCore;

    move-result-object v0

    .line 147
    .local v0, "wordDocument":Lorg/apache/poi/hwpf/HWPFDocumentCore;
    new-instance v1, Lorg/apache/poi/hwpf/converter/WordToTextConverter;

    .line 148
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    .line 147
    invoke-direct {v1, v2}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;-><init>(Lorg/w3c/dom/Document;)V

    .line 150
    .local v1, "wordToTextConverter":Lorg/apache/poi/hwpf/converter/WordToTextConverter;
    invoke-virtual {v1, v0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processDocument(Lorg/apache/poi/hwpf/HWPFDocumentCore;)V

    .line 151
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected afterProcess()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->getBody()Lorg/w3c/dom/Element;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 198
    :cond_0
    return-void
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 207
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 208
    .local v3, "stringWriter":Ljava/io/StringWriter;
    new-instance v0, Ljavax/xml/transform/dom/DOMSource;

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v5

    invoke-direct {v0, v5}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 209
    .local v0, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v2, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v2, v3}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 211
    .local v2, "streamResult":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v4

    .line 212
    .local v4, "tf":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v4}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v1

    .line 214
    .local v1, "serializer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v5, "encoding"

    const-string/jumbo v6, "UTF-8"

    invoke-virtual {v1, v5, v6}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string/jumbo v5, "indent"

    const-string/jumbo v6, "no"

    invoke-virtual {v1, v5, v6}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string/jumbo v5, "method"

    const-string/jumbo v6, "text"

    invoke-virtual {v1, v5, v6}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-virtual {v1, v0, v2}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    .line 219
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public isOutputSummaryInformation()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->outputSummaryInformation:Z

    return v0
.end method

.method protected outputCharacters(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Ljava/lang/String;)V
    .locals 1
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 231
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v0, p3}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 232
    return-void
.end method

.method protected processBookmarks(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/util/List;)V
    .locals 0
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/HWPFDocumentCore;",
            "Lorg/w3c/dom/Element;",
            "Lorg/apache/poi/hwpf/usermodel/Range;",
            "I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/usermodel/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p5, "rangeBookmarks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hwpf/usermodel/Bookmark;>;"
    invoke-virtual {p0, p1, p4, p3, p2}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 240
    return-void
.end method

.method protected processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
    .locals 2
    .param p1, "summaryInformation"    # Lorg/apache/poi/hpsf/SummaryInformation;

    .prologue
    .line 246
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->isOutputSummaryInformation()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 248
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->setTitle(Ljava/lang/String;)V

    .line 251
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->addAuthor(Ljava/lang/String;)V

    .line 255
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 257
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->addDescription(Ljava/lang/String;)V

    .line 260
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hwpf/converter/AbstractWordUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 262
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v1

    .line 261
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->addKeywords(Ljava/lang/String;)V

    .line 264
    :cond_3
    return-void
.end method

.method public processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 0
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "range"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 269
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hwpf/converter/AbstractWordConverter;->processDocumentPart(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 270
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->afterProcess()V

    .line 271
    return-void
.end method

.method protected processDrawnObject(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/apache/poi/hwpf/usermodel/CharacterRun;Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;Ljava/lang/String;Lorg/w3c/dom/Element;)V
    .locals 0
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;
    .param p3, "officeDrawing"    # Lorg/apache/poi/hwpf/usermodel/OfficeDrawing;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "block"    # Lorg/w3c/dom/Element;

    .prologue
    .line 279
    return-void
.end method

.method protected processEndnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 0
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "noteIndex"    # I
    .param p3, "block"    # Lorg/w3c/dom/Element;
    .param p4, "endnoteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 285
    invoke-virtual {p0, p1, p3, p4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processNote(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 286
    return-void
.end method

.method protected processFootnoteAutonumbered(Lorg/apache/poi/hwpf/HWPFDocument;ILorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 0
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "noteIndex"    # I
    .param p3, "block"    # Lorg/w3c/dom/Element;
    .param p4, "footnoteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 292
    invoke-virtual {p0, p1, p3, p4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processNote(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V

    .line 293
    return-void
.end method

.method protected processHyperlink(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
    .locals 4
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "textRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 300
    invoke-virtual {p0, p1, p4, p3, p2}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 303
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " (\u200b"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 305
    const-string/jumbo v2, "\\/"

    const-string/jumbo v3, "\u200b\\/\u200b"

    invoke-virtual {p5, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 307
    const/16 v2, 0x200b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 303
    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 308
    return-void
.end method

.method protected processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V
    .locals 0
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;

    .prologue
    .line 315
    return-void
.end method

.method protected processImage(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;Ljava/lang/String;)V
    .locals 0
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;
    .param p4, "url"    # Ljava/lang/String;

    .prologue
    .line 322
    return-void
.end method

.method protected processImageWithoutPicturesManager(Lorg/w3c/dom/Element;ZLorg/apache/poi/hwpf/usermodel/Picture;)V
    .locals 0
    .param p1, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p2, "inlined"    # Z
    .param p3, "picture"    # Lorg/apache/poi/hwpf/usermodel/Picture;

    .prologue
    .line 329
    return-void
.end method

.method protected processLineBreak(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/CharacterRun;)V
    .locals 2
    .param p1, "block"    # Lorg/w3c/dom/Element;
    .param p2, "characterRun"    # Lorg/apache/poi/hwpf/usermodel/CharacterRun;

    .prologue
    .line 334
    iget-object v0, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 335
    return-void
.end method

.method protected processNote(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;)V
    .locals 5
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "block"    # Lorg/w3c/dom/Element;
    .param p3, "noteTextRange"    # Lorg/apache/poi/hwpf/usermodel/Range;

    .prologue
    .line 340
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->noteCounters:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    .line 341
    .local v1, "noteIndex":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 342
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "\u200b["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 343
    const-string/jumbo v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x200b

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 342
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v2

    .line 341
    invoke-interface {p2, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 345
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    if-nez v2, :cond_0

    .line 346
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    .line 348
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 349
    .local v0, "note":Lorg/w3c/dom/Element;
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->notes:Lorg/w3c/dom/Element;

    invoke-interface {v2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 351
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "^"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 352
    const-string/jumbo v4, "\t "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 351
    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 353
    const/high16 v2, -0x80000000

    invoke-virtual {p0, p1, v2, p3, v0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 354
    iget-object v2, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 355
    return-void
.end method

.method protected processOle2(Lorg/apache/poi/hwpf/HWPFDocument;Lorg/w3c/dom/Element;Lorg/apache/poi/poifs/filesystem/Entry;)Z
    .locals 15
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocument;
    .param p2, "block"    # Lorg/w3c/dom/Element;
    .param p3, "entry"    # Lorg/apache/poi/poifs/filesystem/Entry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 361
    move-object/from16 v0, p3

    instance-of v1, v0, Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    if-nez v1, :cond_0

    .line 362
    const/4 v1, 0x0

    .line 412
    .end local p3    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    :goto_0
    return v1

    .restart local p3    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_0
    move-object/from16 v10, p3

    .line 363
    check-cast v10, Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .line 369
    .local v10, "directoryNode":Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    const-string/jumbo v1, "WordDocument"

    invoke-virtual {v10, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->hasEntry(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    check-cast p3, Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .end local p3    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->getText(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/lang/String;

    move-result-object v14

    .line 372
    .local v14, "text":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 373
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x200b

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 374
    const/16 v3, 0x200b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 373
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    .line 372
    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 375
    const/4 v1, 0x1

    goto :goto_0

    .line 382
    .end local v14    # "text":Ljava/lang/String;
    .restart local p3    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_1
    :try_start_0
    const-string/jumbo v1, "org.apache.poi.extractor.ExtractorFactory"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .line 383
    .local v8, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v1, "createExtractor"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    .line 384
    const-class v4, Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    aput-object v4, v2, v3

    .line 383
    invoke-virtual {v8, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 385
    .local v9, "createExtractor":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    invoke-virtual {v9, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 399
    .local v12, "extractor":Ljava/lang/Object;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string/jumbo v2, "getText"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 400
    .local v13, "getText":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v13, v12, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 402
    .restart local v14    # "text":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x200b

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 404
    const/16 v3, 0x200b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 403
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    .line 402
    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 405
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 387
    .end local v8    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v9    # "createExtractor":Ljava/lang/reflect/Method;
    .end local v12    # "extractor":Ljava/lang/Object;
    .end local v13    # "getText":Ljava/lang/reflect/Method;
    .end local v14    # "text":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 390
    .local v11, "exc":Ljava/lang/Error;
    sget-object v1, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x5

    const-string/jumbo v3, "There is an OLE object entry \'"

    .line 391
    invoke-interface/range {p3 .. p3}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v4

    .line 392
    const-string/jumbo v5, "\', but there is no text extractor for this object type "

    .line 393
    const-string/jumbo v6, "or text extractor factory is not available: "

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 390
    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 394
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 407
    .end local v11    # "exc":Ljava/lang/Error;
    .restart local v8    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v9    # "createExtractor":Ljava/lang/reflect/Method;
    .restart local v12    # "extractor":Ljava/lang/Object;
    :catch_1
    move-exception v6

    .line 409
    .local v6, "exc":Ljava/lang/Exception;
    sget-object v1, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x7

    .line 410
    const-string/jumbo v3, "Unable to extract text from OLE entry \'"

    invoke-interface/range {p3 .. p3}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v4

    .line 411
    const-string/jumbo v5, "\': "

    move-object v7, v6

    .line 409
    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 412
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method protected processPageBreak(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;)V
    .locals 3
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;

    .prologue
    .line 419
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 420
    .local v0, "block":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 421
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 422
    return-void
.end method

.method protected processPageref(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;ILjava/lang/String;)V
    .locals 0
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "currentBlock"    # Lorg/w3c/dom/Element;
    .param p3, "textRange"    # Lorg/apache/poi/hwpf/usermodel/Range;
    .param p4, "currentTableLevel"    # I
    .param p5, "pageref"    # Ljava/lang/String;

    .prologue
    .line 429
    invoke-virtual {p0, p1, p4, p3, p2}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 431
    return-void
.end method

.method protected processParagraph(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;ILorg/apache/poi/hwpf/usermodel/Paragraph;Ljava/lang/String;)V
    .locals 3
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "parentElement"    # Lorg/w3c/dom/Element;
    .param p3, "currentTableLevel"    # I
    .param p4, "paragraph"    # Lorg/apache/poi/hwpf/usermodel/Paragraph;
    .param p5, "bulletText"    # Ljava/lang/String;

    .prologue
    .line 438
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createParagraph()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 439
    .local v0, "pElement":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v1, p5}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 440
    invoke-virtual {p0, p1, p3, p4, v0}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 441
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 442
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 443
    return-void
.end method

.method protected processSection(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/apache/poi/hwpf/usermodel/Section;I)V
    .locals 3
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "section"    # Lorg/apache/poi/hwpf/usermodel/Section;
    .param p3, "s"    # I

    .prologue
    .line 449
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 451
    .local v0, "sectionElement":Lorg/w3c/dom/Element;
    const/high16 v1, -0x80000000

    .line 450
    invoke-virtual {p0, p1, v0, p2, v1}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processParagraphes(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Range;I)V

    .line 452
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 453
    iget-object v1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    iget-object v1, v1, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->body:Lorg/w3c/dom/Element;

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 454
    return-void
.end method

.method protected processTable(Lorg/apache/poi/hwpf/HWPFDocumentCore;Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/usermodel/Table;)V
    .locals 10
    .param p1, "wordDocument"    # Lorg/apache/poi/hwpf/HWPFDocumentCore;
    .param p2, "flow"    # Lorg/w3c/dom/Element;
    .param p3, "table"    # Lorg/apache/poi/hwpf/usermodel/Table;

    .prologue
    .line 459
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Table;->numRows()I

    move-result v7

    .line 460
    .local v7, "tableRows":I
    const/4 v1, 0x0

    .local v1, "r":I
    :goto_0
    if-lt v1, v7, :cond_0

    .line 485
    return-void

    .line 462
    :cond_0
    invoke-virtual {p3, v1}, Lorg/apache/poi/hwpf/usermodel/Table;->getRow(I)Lorg/apache/poi/hwpf/usermodel/TableRow;

    move-result-object v5

    .line 464
    .local v5, "tableRow":Lorg/apache/poi/hwpf/usermodel/TableRow;
    iget-object v8, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v6

    .line 466
    .local v6, "tableRowElement":Lorg/w3c/dom/Element;
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/TableRow;->numCells()I

    move-result v2

    .line 467
    .local v2, "rowCells":I
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_1
    if-lt v0, v2, :cond_1

    .line 482
    iget-object v8, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v8

    invoke-interface {v6, v8}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 483
    invoke-interface {p2, v6}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 460
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 469
    :cond_1
    invoke-virtual {v5, v0}, Lorg/apache/poi/hwpf/usermodel/TableRow;->getCell(I)Lorg/apache/poi/hwpf/usermodel/TableCell;

    move-result-object v3

    .line 471
    .local v3, "tableCell":Lorg/apache/poi/hwpf/usermodel/TableCell;
    iget-object v8, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 473
    .local v4, "tableCellElement":Lorg/w3c/dom/Element;
    if-eqz v0, :cond_2

    .line 474
    iget-object v8, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->textDocumentFacade:Lorg/apache/poi/hwpf/converter/TextDocumentFacade;

    .line 475
    const-string/jumbo v9, "\t"

    invoke-virtual {v8, v9}, Lorg/apache/poi/hwpf/converter/TextDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v8

    .line 474
    invoke-interface {v4, v8}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 477
    :cond_2
    invoke-virtual {p3}, Lorg/apache/poi/hwpf/usermodel/Table;->getTableLevel()I

    move-result v8

    invoke-virtual {p0, p1, v8, v3, v4}, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->processCharacters(Lorg/apache/poi/hwpf/HWPFDocumentCore;ILorg/apache/poi/hwpf/usermodel/Range;Lorg/w3c/dom/Element;)Z

    .line 479
    invoke-interface {v6, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setOutputSummaryInformation(Z)V
    .locals 0
    .param p1, "outputDocumentInformation"    # Z

    .prologue
    .line 489
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/converter/WordToTextConverter;->outputSummaryInformation:Z

    .line 490
    return-void
.end method
