.class public final Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
.super Lorg/apache/poi/hwpf/model/types/SHDAbstractType;
.source "ShadingDescriptor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;-><init>()V

    .line 33
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/SHDAbstractType;-><init>()V

    .line 38
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->fillFields([BI)V

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->field_3_ipat:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 54
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->serialize([BI)V

    .line 55
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string/jumbo v0, "[SHD] EMPTY"

    .line 64
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[SHD] (cvFore: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getCvFore()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; cvBack: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getCvBack()Lorg/apache/poi/hwpf/model/Colorref;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 65
    const-string/jumbo v1, "; iPat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/ShadingDescriptor;->getIpat()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
