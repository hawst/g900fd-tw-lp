.class public final Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hwpf/model/PropertyNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EndComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/poi/hwpf/model/PropertyNode",
        "<*>;>;"
    }
.end annotation


# static fields
.field public static instance:Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;-><init>()V

    sput-object v0, Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;->instance:Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hwpf/model/PropertyNode;

    check-cast p2, Lorg/apache/poi/hwpf/model/PropertyNode;

    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/PropertyNode$EndComparator;->compare(Lorg/apache/poi/hwpf/model/PropertyNode;Lorg/apache/poi/hwpf/model/PropertyNode;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/poi/hwpf/model/PropertyNode;Lorg/apache/poi/hwpf/model/PropertyNode;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hwpf/model/PropertyNode",
            "<*>;",
            "Lorg/apache/poi/hwpf/model/PropertyNode",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "o1":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    .local p2, "o2":Lorg/apache/poi/hwpf/model/PropertyNode;, "Lorg/apache/poi/hwpf/model/PropertyNode<*>;"
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v1

    .line 48
    .local v1, "thisVal":I
    invoke-virtual {p2}, Lorg/apache/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    .line 49
    .local v0, "anotherVal":I
    if-ge v1, v0, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    if-ne v1, v0, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    .line 50
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
