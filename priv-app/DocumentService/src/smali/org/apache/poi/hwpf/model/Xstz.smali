.class public Lorg/apache/poi/hwpf/model/Xstz;
.super Ljava/lang/Object;
.source "Xstz.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final _chTerm:S

.field private _xst:Lorg/apache/poi/hwpf/model/Xst;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/poi/hwpf/model/Xstz;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hwpf/model/Xstz;->log:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/Xstz;->_chTerm:S

    .line 34
    new-instance v0, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/model/Xst;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 35
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-short v0, p0, Lorg/apache/poi/hwpf/model/Xstz;->_chTerm:S

    .line 39
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/Xstz;->fillFields([BI)V

    .line 40
    return-void
.end method


# virtual methods
.method public fillFields([BI)V
    .locals 7
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 44
    move v0, p2

    .line 46
    .local v0, "offset":I
    new-instance v2, Lorg/apache/poi/hwpf/model/Xst;

    invoke-direct {v2, p1, v0}, Lorg/apache/poi/hwpf/model/Xst;-><init>([BI)V

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    .line 47
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/Xst;->getSize()I

    move-result v2

    add-int/2addr v0, v2

    .line 49
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 50
    .local v1, "term":S
    if-eqz v1, :cond_0

    .line 52
    sget-object v2, Lorg/apache/poi/hwpf/model/Xstz;->log:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    const-string/jumbo v4, "chTerm at the end of Xstz at offset "

    .line 53
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string/jumbo v6, " is not 0"

    .line 52
    invoke-virtual {v2, v3, v4, v5, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 55
    :cond_0
    return-void
.end method

.method public getAsJavaString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Xst;->getAsJavaString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/Xst;->getSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public serialize([BI)I
    .locals 2
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 69
    move v0, p2

    .line 71
    .local v0, "offset":I
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v1, p1, v0}, Lorg/apache/poi/hwpf/model/Xst;->serialize([BI)V

    .line 72
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xst;->getSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 75
    add-int/lit8 v0, v0, 0x2

    .line 77
    sub-int v1, v0, p2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "[Xstz]"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/poi/hwpf/model/Xstz;->_xst:Lorg/apache/poi/hwpf/model/Xst;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Xst;->getAsJavaString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "[/Xstz]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
