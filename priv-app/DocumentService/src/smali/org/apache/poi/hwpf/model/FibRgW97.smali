.class Lorg/apache/poi/hwpf/model/FibRgW97;
.super Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;
.source "FibRgW97.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;-><init>()V

    .line 38
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "std"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/FibRgW97AbstractType;-><init>()V

    .line 42
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/model/FibRgW97;->fillFields([BI)V

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p0, p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 54
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 55
    check-cast v0, Lorg/apache/poi/hwpf/model/FibRgW97;

    .line 56
    .local v0, "other":Lorg/apache/poi/hwpf/model/FibRgW97;
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_10_reserved10:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_10_reserved10:S

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 57
    goto :goto_0

    .line 58
    :cond_4
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_11_reserved11:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_11_reserved11:S

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 59
    goto :goto_0

    .line 60
    :cond_5
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_12_reserved12:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_12_reserved12:S

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 61
    goto :goto_0

    .line 62
    :cond_6
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_13_reserved13:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_13_reserved13:S

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 63
    goto :goto_0

    .line 64
    :cond_7
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_14_lidFE:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_14_lidFE:S

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 65
    goto :goto_0

    .line 66
    :cond_8
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_1_reserved1:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_1_reserved1:S

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 67
    goto :goto_0

    .line 68
    :cond_9
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_2_reserved2:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_2_reserved2:S

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 69
    goto :goto_0

    .line 70
    :cond_a
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_3_reserved3:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_3_reserved3:S

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 71
    goto :goto_0

    .line 72
    :cond_b
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_4_reserved4:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_4_reserved4:S

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 73
    goto :goto_0

    .line 74
    :cond_c
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_5_reserved5:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_5_reserved5:S

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_d
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_6_reserved6:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_6_reserved6:S

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 77
    goto :goto_0

    .line 78
    :cond_e
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_7_reserved7:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_7_reserved7:S

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 79
    goto :goto_0

    .line 80
    :cond_f
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_8_reserved8:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_8_reserved8:S

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 81
    goto :goto_0

    .line 82
    :cond_10
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_9_reserved9:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_9_reserved9:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 83
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 91
    const/16 v0, 0x1f

    .line 92
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 93
    .local v1, "result":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_10_reserved10:S

    add-int/lit8 v1, v2, 0x1f

    .line 94
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_11_reserved11:S

    add-int v1, v2, v3

    .line 95
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_12_reserved12:S

    add-int v1, v2, v3

    .line 96
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_13_reserved13:S

    add-int v1, v2, v3

    .line 97
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_14_lidFE:S

    add-int v1, v2, v3

    .line 98
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_1_reserved1:S

    add-int v1, v2, v3

    .line 99
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_2_reserved2:S

    add-int v1, v2, v3

    .line 100
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_3_reserved3:S

    add-int v1, v2, v3

    .line 101
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_4_reserved4:S

    add-int v1, v2, v3

    .line 102
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_5_reserved5:S

    add-int v1, v2, v3

    .line 103
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_6_reserved6:S

    add-int v1, v2, v3

    .line 104
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_7_reserved7:S

    add-int v1, v2, v3

    .line 105
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_8_reserved8:S

    add-int v1, v2, v3

    .line 106
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hwpf/model/FibRgW97;->field_9_reserved9:S

    add-int v1, v2, v3

    .line 107
    return v1
.end method
