.class public abstract Lorg/apache/poi/hwpf/model/BytePropertyNode;
.super Lorg/apache/poi/hwpf/model/PropertyNode;
.source "BytePropertyNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/poi/hwpf/model/BytePropertyNode",
        "<TT;>;>",
        "Lorg/apache/poi/hwpf/model/PropertyNode",
        "<TT;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final endBytes:I

.field private final startBytes:I


# direct methods
.method public constructor <init>(IILjava/lang/Object;)V
    .locals 3
    .param p1, "charStart"    # I
    .param p2, "charEnd"    # I
    .param p3, "buf"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/poi/hwpf/model/BytePropertyNode;, "Lorg/apache/poi/hwpf/model/BytePropertyNode<TT;>;"
    const/4 v0, -0x1

    .line 58
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 60
    if-le p1, p2, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "charStart ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 62
    const-string/jumbo v2, ") > charEnd ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    iput v0, p0, Lorg/apache/poi/hwpf/model/BytePropertyNode;->startBytes:I

    .line 65
    iput v0, p0, Lorg/apache/poi/hwpf/model/BytePropertyNode;->endBytes:I

    .line 66
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V
    .locals 3
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/poi/hwpf/model/CharIndexTranslator;
    .param p4, "buf"    # Ljava/lang/Object;

    .prologue
    .line 42
    .line 43
    .local p0, "this":Lorg/apache/poi/hwpf/model/BytePropertyNode;, "Lorg/apache/poi/hwpf/model/BytePropertyNode<TT;>;"
    invoke-interface {p3, p1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v0

    .line 44
    invoke-interface {p3, p1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v1

    invoke-interface {p3, p2, v1}, Lorg/apache/poi/hwpf/model/CharIndexTranslator;->getCharIndex(II)I

    move-result v1

    .line 45
    invoke-direct {p0, v0, v1, p4}, Lorg/apache/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 48
    if-le p1, p2, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "fcStart ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 50
    const-string/jumbo v2, ") > fcEnd ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    iput p1, p0, Lorg/apache/poi/hwpf/model/BytePropertyNode;->startBytes:I

    .line 53
    iput p2, p0, Lorg/apache/poi/hwpf/model/BytePropertyNode;->endBytes:I

    .line 54
    return-void
.end method


# virtual methods
.method public getEndBytes()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lorg/apache/poi/hwpf/model/BytePropertyNode;, "Lorg/apache/poi/hwpf/model/BytePropertyNode<TT;>;"
    iget v0, p0, Lorg/apache/poi/hwpf/model/BytePropertyNode;->endBytes:I

    return v0
.end method

.method public getStartBytes()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lorg/apache/poi/hwpf/model/BytePropertyNode;, "Lorg/apache/poi/hwpf/model/BytePropertyNode<TT;>;"
    iget v0, p0, Lorg/apache/poi/hwpf/model/BytePropertyNode;->startBytes:I

    return v0
.end method
