.class public final Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
.super Ljava/lang/Object;
.source "ListFormatOverrideLevel.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

.field private _lvl:Lorg/apache/poi/hwpf/model/ListLevel;


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-direct {v0, p1, p2}, Lorg/apache/poi/hwpf/model/LFOLVLBase;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    .line 37
    invoke-static {}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->getSize()I

    move-result v0

    add-int/2addr p2, v0

    .line 39
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->isFFormatting()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    new-instance v0, Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-direct {v0, p1, p2}, Lorg/apache/poi/hwpf/model/ListLevel;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    .line 43
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 48
    if-eqz p1, :cond_0

    instance-of v4, p1, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    if-eqz v4, :cond_0

    move-object v0, p1

    .line 50
    check-cast v0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;

    .line 51
    .local v0, "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    const/4 v1, 0x0

    .line 52
    .local v1, "lvlEquality":Z
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    if-eqz v4, :cond_1

    .line 54
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    iget-object v5, v0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/model/ListLevel;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 61
    :goto_0
    if-eqz v1, :cond_0

    iget-object v4, v0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    iget-object v5, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v4, v5}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    .line 63
    .end local v0    # "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    .end local v1    # "lvlEquality":Z
    :cond_0
    return v3

    .line 58
    .restart local v0    # "lfolvl":Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;
    .restart local v1    # "lvlEquality":Z
    :cond_1
    iget-object v4, v0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    if-nez v4, :cond_2

    move v1, v2

    :goto_1
    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public getIStartAt()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->getIStartAt()I

    move-result v0

    return v0
.end method

.method public getLevel()Lorg/apache/poi/hwpf/model/ListLevel;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    return-object v0
.end method

.method public getLevelNum()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->getILvl()B

    move-result v0

    return v0
.end method

.method public getSizeInBytes()I
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->getSize()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->getSize()I

    move-result v0

    .line 84
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/ListLevel;->getSizeInBytes()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 90
    const/16 v0, 0x1f

    .line 91
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 92
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->hashCode()I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 93
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_0
    add-int v1, v3, v2

    .line 94
    return v1

    .line 93
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isFormatting()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->isFFormatting()Z

    move-result v0

    return v0
.end method

.method public isStartAt()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->isFStartAt()Z

    move-result v0

    return v0
.end method

.method public toByteArray()[B
    .locals 5

    .prologue
    .line 109
    const/4 v2, 0x0

    .line 111
    .local v2, "offset":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->getSizeInBytes()I

    move-result v3

    new-array v0, v3, [B

    .line 112
    .local v0, "buf":[B
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_base:Lorg/apache/poi/hwpf/model/LFOLVLBase;

    invoke-virtual {v3, v0, v2}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->serialize([BI)V

    .line 113
    invoke-static {}, Lorg/apache/poi/hwpf/model/LFOLVLBase;->getSize()I

    move-result v3

    add-int/2addr v2, v3

    .line 115
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    if-eqz v3, :cond_0

    .line 117
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/ListFormatOverrideLevel;->_lvl:Lorg/apache/poi/hwpf/model/ListLevel;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/ListLevel;->toByteArray()[B

    move-result-object v1

    .line 118
    .local v1, "levelBuf":[B
    const/4 v3, 0x0

    array-length v4, v1

    invoke-static {v1, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    .end local v1    # "levelBuf":[B
    :cond_0
    return-object v0
.end method
