.class public final Lorg/apache/poi/hwpf/usermodel/HeaderStories;
.super Ljava/lang/Object;
.source "HeaderStories.java"


# instance fields
.field private headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

.field private plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

.field private stripFields:Z


# direct methods
.method public constructor <init>(Lorg/apache/poi/hwpf/HWPFDocument;)V
    .locals 6
    .param p1, "doc"    # Lorg/apache/poi/hwpf/HWPFDocument;

    .prologue
    const/4 v5, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-boolean v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->stripFields:Z

    .line 43
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getHeaderStoryRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 44
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getFileInformationBlock()Lorg/apache/poi/hwpf/model/FileInformationBlock;

    move-result-object v0

    .line 51
    .local v0, "fib":Lorg/apache/poi/hwpf/model/FileInformationBlock;
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getSubdocumentTextStreamLength(Lorg/apache/poi/hwpf/model/SubdocumentType;)I

    move-result v1

    if-nez v1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getPlcfHddSize()I

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    new-instance v1, Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {p1}, Lorg/apache/poi/hwpf/HWPFDocument;->getTableStream()[B

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getPlcfHddOffset()I

    move-result v3

    .line 82
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getPlcfHddSize()I

    move-result v4

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 81
    iput-object v1, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    goto :goto_0
.end method

.method private getAt(I)Ljava/lang/String;
    .locals 7
    .param p1, "plcfHddIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 296
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-nez v5, :cond_1

    const/4 v4, 0x0

    .line 327
    :cond_0
    :goto_0
    return-object v4

    .line 298
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v5, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v1

    .line 299
    .local v1, "prop":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v5

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 301
    const-string/jumbo v4, ""

    goto :goto_0

    .line 303
    :cond_2
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v5

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 305
    const-string/jumbo v4, ""

    goto :goto_0

    .line 309
    :cond_3
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/Range;->text()Ljava/lang/String;

    move-result-object v2

    .line 310
    .local v2, "rawText":Ljava/lang/String;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 311
    .local v3, "start":I
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 314
    .local v0, "end":I
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "text":Ljava/lang/String;
    iget-boolean v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->stripFields:Z

    if-eqz v5, :cond_4

    .line 318
    invoke-static {v4}, Lorg/apache/poi/hwpf/usermodel/Range;->stripFields(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 323
    :cond_4
    const-string/jumbo v5, "\r\r"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 324
    const-string/jumbo v4, ""

    goto :goto_0
.end method

.method private getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 8
    .param p1, "plcfHddIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 332
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    if-nez v5, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-object v4

    .line 335
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-virtual {v5, p1}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 336
    .local v2, "prop":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v5

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v6

    if-eq v5, v6, :cond_0

    .line 341
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v5

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v6

    if-lt v5, v6, :cond_0

    .line 347
    iget-object v4, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/usermodel/Range;->getEndOffset()I

    move-result v4

    .line 348
    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v5

    .line 347
    sub-int v1, v4, v5

    .line 349
    .local v1, "headersLength":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 350
    .local v3, "start":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 352
    .local v0, "end":I
    new-instance v4, Lorg/apache/poi/hwpf/usermodel/Range;

    iget-object v5, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v5

    add-int/2addr v5, v3

    .line 353
    iget-object v6, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/usermodel/Range;->getStartOffset()I

    move-result v6

    add-int/2addr v6, v0

    iget-object v7, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    .line 352
    invoke-direct {v4, v5, v6, v7}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/usermodel/Range;)V

    goto :goto_0
.end method


# virtual methods
.method public areFieldsStripped()Z
    .locals 1

    .prologue
    .line 369
    iget-boolean v0, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->stripFields:Z

    return v0
.end method

.method public getEndnoteContNote()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 118
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndnoteContNoteSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getEndnoteContSeparator()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndnoteContSeparatorSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getEndnoteSeparator()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndnoteSeparatorSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getEvenFooter()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 202
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEvenFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 219
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getEvenHeader()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 153
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEvenHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getFirstFooter()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 214
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirstFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 229
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getFirstHeader()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 161
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirstHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 172
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getFooter(I)Ljava/lang/String;
    .locals 1
    .param p1, "pageNumber"    # I

    .prologue
    .line 240
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooter()Ljava/lang/String;

    move-result-object v0

    .line 253
    :goto_0
    return-object v0

    .line 247
    :cond_0
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 248
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 249
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooter()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 253
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddFooter()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFooterPageType(I)I
    .locals 1
    .param p1, "pageNumber"    # I

    .prologue
    .line 275
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstFooter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 277
    const/16 v0, 0xb

    .line 286
    :goto_0
    return v0

    .line 281
    :cond_0
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 282
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenFooter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 283
    const/16 v0, 0x8

    goto :goto_0

    .line 286
    :cond_1
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public getFootnoteContNote()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFootnoteContNoteSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getFootnoteContSeparator()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 94
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFootnoteContSeparatorSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getFootnoteSeparator()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFootnoteSeparatorSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getHeader(I)Ljava/lang/String;
    .locals 1
    .param p1, "pageNumber"    # I

    .prologue
    .line 183
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 184
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeader()Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    .line 190
    :cond_0
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 191
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 192
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeader()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getOddHeader()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderPageType(I)I
    .locals 1
    .param p1, "pageNumber"    # I

    .prologue
    .line 259
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 260
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getFirstHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 261
    const/16 v0, 0xa

    .line 270
    :goto_0
    return v0

    .line 265
    :cond_0
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 266
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getEvenHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 267
    const/4 v0, 0x6

    goto :goto_0

    .line 270
    :cond_1
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public getOddFooter()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 208
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOddFooterSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 224
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getOddHeader()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 157
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOddHeaderSubrange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->getSubrangeAt(I)Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method protected getPlcfHdd()Lorg/apache/poi/hwpf/model/PlexOfCps;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->plcfHdd:Lorg/apache/poi/hwpf/model/PlexOfCps;

    return-object v0
.end method

.method public getRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->headerStories:Lorg/apache/poi/hwpf/usermodel/Range;

    return-object v0
.end method

.method public setAreFieldsStripped(Z)V
    .locals 0
    .param p1, "stripFields"    # Z

    .prologue
    .line 378
    iput-boolean p1, p0, Lorg/apache/poi/hwpf/usermodel/HeaderStories;->stripFields:Z

    .line 379
    return-void
.end method
