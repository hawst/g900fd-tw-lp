.class public final Lorg/apache/poi/hwpf/model/Ffn;
.super Ljava/lang/Object;
.source "Ffn.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field private _cbFfnM1:I

.field private _chs:B

.field private _fontSig:[B

.field private _info:B

.field private _ixchSzAlt:B

.field private _panose:[B

.field private _wWeight:S

.field private _xszFfn:[C

.field private _xszFfnLength:I


# direct methods
.method public constructor <init>([BI)V
    .locals 5
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    const/4 v4, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/16 v2, 0xa

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    .line 49
    const/16 v2, 0x18

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    .line 60
    move v1, p2

    .line 62
    .local v1, "offsetTmp":I
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_cbFfnM1:I

    .line 63
    add-int/lit8 p2, p2, 0x1

    .line 64
    aget-byte v2, p1, p2

    iput-byte v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_info:B

    .line 65
    add-int/lit8 p2, p2, 0x1

    .line 66
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    iput-short v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_wWeight:S

    .line 67
    add-int/lit8 p2, p2, 0x2

    .line 68
    aget-byte v2, p1, p2

    iput-byte v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_chs:B

    .line 69
    add-int/lit8 p2, p2, 0x1

    .line 70
    aget-byte v2, p1, p2

    iput-byte v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    .line 71
    add-int/lit8 p2, p2, 0x1

    .line 74
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    array-length v3, v3

    invoke-static {p1, p2, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    array-length v2, v2

    add-int/2addr p2, v2

    .line 76
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v3, v3

    invoke-static {p1, p2, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v2, v2

    add-int/2addr p2, v2

    .line 79
    sub-int v1, p2, v1

    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Ffn;->getSize()I

    move-result v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfnLength:I

    .line 81
    iget v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfnLength:I

    new-array v2, v2, [C

    iput-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    .line 83
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfnLength:I

    if-lt v0, v2, :cond_0

    .line 90
    return-void

    .line 85
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    int-to-char v3, v3

    aput-char v3, v2, v0

    .line 86
    add-int/lit8 p2, p2, 0x2

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 189
    if-eqz p1, :cond_8

    instance-of v1, p1, Lorg/apache/poi/hwpf/model/Ffn;

    if-eqz v1, :cond_8

    .line 190
    const/4 v0, 0x1

    .local v0, "retVal":Z
    move-object v1, p1

    .line 192
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/Ffn;->get_cbFfnM1()I

    move-result v1

    iget v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_cbFfnM1:I

    if-ne v1, v2, :cond_7

    move-object v1, p1

    .line 194
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    iget-byte v1, v1, Lorg/apache/poi/hwpf/model/Ffn;->_info:B

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_info:B

    if-ne v1, v2, :cond_6

    move-object v1, p1

    .line 196
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    iget-short v1, v1, Lorg/apache/poi/hwpf/model/Ffn;->_wWeight:S

    iget-short v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_wWeight:S

    if-ne v1, v2, :cond_5

    move-object v1, p1

    .line 198
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    iget-byte v1, v1, Lorg/apache/poi/hwpf/model/Ffn;->_chs:B

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_chs:B

    if-ne v1, v2, :cond_4

    move-object v1, p1

    .line 200
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    iget-byte v1, v1, Lorg/apache/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    iget-byte v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    if-ne v1, v2, :cond_3

    move-object v1, p1

    .line 202
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    iget-object v1, v1, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 204
    check-cast v1, Lorg/apache/poi/hwpf/model/Ffn;

    iget-object v1, v1, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    check-cast p1, Lorg/apache/poi/hwpf/model/Ffn;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    const/4 v0, 0x0

    .line 232
    .end local v0    # "retVal":Z
    :cond_0
    :goto_0
    return v0

    .line 210
    .restart local v0    # "retVal":Z
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    .line 211
    goto :goto_0

    .line 213
    :cond_2
    const/4 v0, 0x0

    .line 214
    goto :goto_0

    .line 216
    :cond_3
    const/4 v0, 0x0

    .line 217
    goto :goto_0

    .line 219
    :cond_4
    const/4 v0, 0x0

    .line 220
    goto :goto_0

    .line 222
    :cond_5
    const/4 v0, 0x0

    .line 223
    goto :goto_0

    .line 225
    :cond_6
    const/4 v0, 0x0

    .line 226
    goto :goto_0

    .line 228
    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 232
    .end local v0    # "retVal":Z
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAltFontName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 137
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    .line 138
    .local v0, "index":I
    :goto_0
    iget v1, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfnLength:I

    if-lt v0, v1, :cond_1

    .line 145
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    return-object v1

    .line 140
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    aget-char v1, v1, v0

    if-eqz v1, :cond_0

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getChs()B
    .locals 1

    .prologue
    .line 104
    iget-byte v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_chs:B

    return v0
.end method

.method public getFontSig()[B
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    return-object v0
.end method

.method public getMainFontName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 125
    .local v0, "index":I
    :goto_0
    iget v1, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfnLength:I

    if-lt v0, v1, :cond_1

    .line 132
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    return-object v1

    .line 127
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    aget-char v1, v1, v0

    if-eqz v1, :cond_0

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPanose()[B
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_cbFfnM1:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getWeight()S
    .locals 1

    .prologue
    .line 99
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_wWeight:S

    return v0
.end method

.method public get_cbFfnM1()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/poi/hwpf/model/Ffn;->_cbFfnM1:I

    return v0
.end method

.method public set_cbFfnM1(I)V
    .locals 0
    .param p1, "_cbFfnM1"    # I

    .prologue
    .line 151
    iput p1, p0, Lorg/apache/poi/hwpf/model/Ffn;->_cbFfnM1:I

    .line 152
    return-void
.end method

.method public toByteArray()[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 157
    const/4 v2, 0x0

    .line 158
    .local v2, "offset":I
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/Ffn;->getSize()I

    move-result v3

    new-array v0, v3, [B

    .line 160
    .local v0, "buf":[B
    iget v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_cbFfnM1:I

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 161
    add-int/lit8 v2, v2, 0x1

    .line 162
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_info:B

    aput-byte v3, v0, v2

    .line 163
    add-int/lit8 v2, v2, 0x1

    .line 164
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_wWeight:S

    invoke-static {v0, v2, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 165
    add-int/lit8 v2, v2, 0x2

    .line 166
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_chs:B

    aput-byte v3, v0, v2

    .line 167
    add-int/lit8 v2, v2, 0x1

    .line 168
    iget-byte v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    aput-byte v3, v0, v2

    .line 169
    add-int/lit8 v2, v2, 0x1

    .line 171
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    array-length v4, v4

    invoke-static {v3, v5, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_panose:[B

    array-length v3, v3

    add-int/lit8 v2, v3, 0x6

    .line 173
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    iget-object v4, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v4, v4

    invoke-static {v3, v5, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v3, v3

    add-int/2addr v2, v3

    .line 176
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 182
    return-object v0

    .line 178
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/Ffn;->_xszFfn:[C

    aget-char v3, v3, v1

    int-to-short v3, v3

    invoke-static {v0, v2, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 179
    add-int/lit8 v2, v2, 0x2

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
