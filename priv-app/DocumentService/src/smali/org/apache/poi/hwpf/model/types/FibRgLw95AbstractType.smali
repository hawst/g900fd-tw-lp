.class public abstract Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;
.super Ljava/lang/Object;
.source "FibRgLw95AbstractType.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field protected field_10_ccpAtn:I

.field protected field_11_ccpEdn:I

.field protected field_12_ccpTxbx:I

.field protected field_13_ccpHdrTxbx:I

.field protected field_14_reserved5:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_1_cbMac:I

.field protected field_2_reserved1:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_3_reserved2:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_4_reserved3:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_5_reserved4:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected field_6_ccpText:I

.field protected field_7_ccpFtn:I

.field protected field_8_ccpHdd:I

.field protected field_9_ccpMcr:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 113
    const/16 v0, 0x38

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    if-ne p0, p1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 122
    goto :goto_0

    .line 123
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 124
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 125
    check-cast v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;

    .line 126
    .local v0, "other":Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 127
    goto :goto_0

    .line 128
    :cond_4
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 129
    goto :goto_0

    .line 130
    :cond_5
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 131
    goto :goto_0

    .line 132
    :cond_6
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 133
    goto :goto_0

    .line 134
    :cond_7
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 135
    goto :goto_0

    .line 136
    :cond_8
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 137
    goto :goto_0

    .line 138
    :cond_9
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 139
    goto :goto_0

    .line 140
    :cond_a
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 141
    goto :goto_0

    .line 142
    :cond_b
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 143
    goto :goto_0

    .line 144
    :cond_c
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 145
    goto :goto_0

    .line 146
    :cond_d
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 147
    goto :goto_0

    .line 148
    :cond_e
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 149
    goto :goto_0

    .line 150
    :cond_f
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 151
    goto :goto_0

    .line 152
    :cond_10
    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    iget v4, v0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 153
    goto/16 :goto_0
.end method

.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 67
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    .line 68
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    .line 69
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    .line 70
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    .line 71
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    .line 72
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    .line 73
    add-int/lit8 v0, p2, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    .line 74
    add-int/lit8 v0, p2, 0x1c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    .line 75
    add-int/lit8 v0, p2, 0x20

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    .line 76
    add-int/lit8 v0, p2, 0x24

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    .line 77
    add-int/lit8 v0, p2, 0x28

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    .line 78
    add-int/lit8 v0, p2, 0x2c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    .line 79
    add-int/lit8 v0, p2, 0x30

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    .line 80
    add-int/lit8 v0, p2, 0x34

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    .line 81
    return-void
.end method

.method public getCbMac()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 222
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    return v0
.end method

.method public getCcpAtn()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 384
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    return v0
.end method

.method public getCcpEdn()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 402
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    return v0
.end method

.method public getCcpFtn()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 330
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    return v0
.end method

.method public getCcpHdd()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 348
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    return v0
.end method

.method public getCcpHdrTxbx()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 438
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    return v0
.end method

.method public getCcpMcr()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 366
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    return v0
.end method

.method public getCcpText()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 312
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    return v0
.end method

.method public getCcpTxbx()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 420
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    return v0
.end method

.method public getReserved1()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 240
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    return v0
.end method

.method public getReserved2()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 258
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    return v0
.end method

.method public getReserved3()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 276
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    return v0
.end method

.method public getReserved4()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 294
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    return v0
.end method

.method public getReserved5()I
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 456
    iget v0, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 160
    const/16 v0, 0x1f

    .line 161
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 162
    .local v1, "result":I
    iget v2, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    add-int/lit8 v1, v2, 0x1f

    .line 163
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    add-int v1, v2, v3

    .line 164
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    add-int v1, v2, v3

    .line 165
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    add-int v1, v2, v3

    .line 166
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    add-int v1, v2, v3

    .line 167
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    add-int v1, v2, v3

    .line 168
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    add-int v1, v2, v3

    .line 169
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    add-int v1, v2, v3

    .line 170
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    add-int v1, v2, v3

    .line 171
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    add-int v1, v2, v3

    .line 172
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    add-int v1, v2, v3

    .line 173
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    add-int v1, v2, v3

    .line 174
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    add-int v1, v2, v3

    .line 175
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    add-int v1, v2, v3

    .line 176
    return v1
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 85
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 86
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 87
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 88
    add-int/lit8 v0, p2, 0xc

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 89
    add-int/lit8 v0, p2, 0x10

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 90
    add-int/lit8 v0, p2, 0x14

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 91
    add-int/lit8 v0, p2, 0x18

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 92
    add-int/lit8 v0, p2, 0x1c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 93
    add-int/lit8 v0, p2, 0x20

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 94
    add-int/lit8 v0, p2, 0x24

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 95
    add-int/lit8 v0, p2, 0x28

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 96
    add-int/lit8 v0, p2, 0x2c

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 97
    add-int/lit8 v0, p2, 0x30

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 98
    add-int/lit8 v0, p2, 0x34

    iget v1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 99
    return-void
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 103
    invoke-static {}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 104
    .local v0, "result":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->serialize([BI)V

    .line 105
    return-object v0
.end method

.method public setCbMac(I)V
    .locals 0
    .param p1, "field_1_cbMac"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 231
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_1_cbMac:I

    .line 232
    return-void
.end method

.method public setCcpAtn(I)V
    .locals 0
    .param p1, "field_10_ccpAtn"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 393
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_10_ccpAtn:I

    .line 394
    return-void
.end method

.method public setCcpEdn(I)V
    .locals 0
    .param p1, "field_11_ccpEdn"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 411
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_11_ccpEdn:I

    .line 412
    return-void
.end method

.method public setCcpFtn(I)V
    .locals 0
    .param p1, "field_7_ccpFtn"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 339
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_7_ccpFtn:I

    .line 340
    return-void
.end method

.method public setCcpHdd(I)V
    .locals 0
    .param p1, "field_8_ccpHdd"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 357
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_8_ccpHdd:I

    .line 358
    return-void
.end method

.method public setCcpHdrTxbx(I)V
    .locals 0
    .param p1, "field_13_ccpHdrTxbx"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 447
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_13_ccpHdrTxbx:I

    .line 448
    return-void
.end method

.method public setCcpMcr(I)V
    .locals 0
    .param p1, "field_9_ccpMcr"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 375
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_9_ccpMcr:I

    .line 376
    return-void
.end method

.method public setCcpText(I)V
    .locals 0
    .param p1, "field_6_ccpText"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 321
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_6_ccpText:I

    .line 322
    return-void
.end method

.method public setCcpTxbx(I)V
    .locals 0
    .param p1, "field_12_ccpTxbx"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 429
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_12_ccpTxbx:I

    .line 430
    return-void
.end method

.method public setReserved1(I)V
    .locals 0
    .param p1, "field_2_reserved1"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 249
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_2_reserved1:I

    .line 250
    return-void
.end method

.method public setReserved2(I)V
    .locals 0
    .param p1, "field_3_reserved2"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 267
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_3_reserved2:I

    .line 268
    return-void
.end method

.method public setReserved3(I)V
    .locals 0
    .param p1, "field_4_reserved3"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 285
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_4_reserved3:I

    .line 286
    return-void
.end method

.method public setReserved4(I)V
    .locals 0
    .param p1, "field_5_reserved4"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 303
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_5_reserved4:I

    .line 304
    return-void
.end method

.method public setReserved5(I)V
    .locals 0
    .param p1, "field_14_reserved5"    # I
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 465
    iput p1, p0, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->field_14_reserved5:I

    .line 466
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[FibRgLw95]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string/jumbo v1, "    .cbMac                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCbMac()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v1, "    .reserved1            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getReserved1()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string/jumbo v1, "    .reserved2            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getReserved2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v1, "    .reserved3            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getReserved3()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v1, "    .reserved4            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getReserved4()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string/jumbo v1, "    .ccpText              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpText()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string/jumbo v1, "    .ccpFtn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpFtn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v1, "    .ccpHdd               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpHdd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v1, "    .ccpMcr               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpMcr()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v1, "    .ccpAtn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpAtn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v1, "    .ccpEdn               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v1, "    .ccpTxbx              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpTxbx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string/jumbo v1, "    .ccpHdrTxbx           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getCcpHdrTxbx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string/jumbo v1, "    .reserved5            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/types/FibRgLw95AbstractType;->getReserved5()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string/jumbo v1, "[/FibRgLw95]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
