.class public Lorg/apache/poi/hwpf/HWPFOldDocument;
.super Lorg/apache/poi/hwpf/HWPFDocumentCore;
.source "HWPFOldDocument.java"


# instance fields
.field private _text:Ljava/lang/StringBuilder;

.field private tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 24
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct/range {p0 .. p1}, Lorg/apache/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 57
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0x88

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v13

    .line 58
    .local v13, "sedTableOffset":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0x8c

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v14

    .line 59
    .local v14, "sedTableSize":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0xb8

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v5

    .line 60
    .local v5, "chpTableOffset":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0xbc

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v6

    .line 61
    .local v6, "chpTableSize":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0xc0

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v9

    .line 62
    .local v9, "papTableOffset":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0xc4

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v10

    .line 65
    .local v10, "papTableSize":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v4, 0x160

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v18

    .line 69
    .local v18, "complexTableOffset":I
    const/16 v17, 0x0

    .line 70
    .local v17, "cft":Lorg/apache/poi/hwpf/model/ComplexFileTable;
    new-instance v21, Ljava/lang/StringBuffer;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuffer;-><init>()V

    .line 71
    .local v21, "text":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FibBase;->isFComplex()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    new-instance v17, Lorg/apache/poi/hwpf/model/ComplexFileTable;

    .line 73
    .end local v17    # "cft":Lorg/apache/poi/hwpf/model/ComplexFileTable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 74
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v7

    .line 72
    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v3, v4, v1, v7}, Lorg/apache/poi/hwpf/model/ComplexFileTable;-><init>([B[BII)V

    .line 76
    .restart local v17    # "cft":Lorg/apache/poi/hwpf/model/ComplexFileTable;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    .line 78
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 102
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getText()Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_text:Ljava/lang/StringBuilder;

    .line 105
    new-instance v3, Lorg/apache/poi/hwpf/model/OldCHPBinTable;

    .line 106
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 107
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hwpf/model/OldCHPBinTable;-><init>([BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V

    .line 105
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_cbt:Lorg/apache/poi/hwpf/model/CHPBinTable;

    .line 109
    new-instance v7, Lorg/apache/poi/hwpf/model/OldPAPBinTable;

    .line 110
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 111
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-direct/range {v7 .. v12}, Lorg/apache/poi/hwpf/model/OldPAPBinTable;-><init>([BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V

    .line 109
    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_pbt:Lorg/apache/poi/hwpf/model/PAPBinTable;

    .line 113
    new-instance v11, Lorg/apache/poi/hwpf/model/OldSectionTable;

    .line 114
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 115
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-object/from16 v16, v0

    invoke-direct/range {v11 .. v16}, Lorg/apache/poi/hwpf/model/OldSectionTable;-><init>([BIIILorg/apache/poi/hwpf/model/TextPieceTable;)V

    .line 113
    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_st:Lorg/apache/poi/hwpf/model/SectionTable;

    .line 122
    const/16 v20, 0x0

    .line 126
    .local v20, "preserveBinTables":Z
    :try_start_0
    const-string/jumbo v3, "org.apache.poi.hwpf.preserveBinTables"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 125
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v20

    .line 133
    :goto_2
    if-nez v20, :cond_0

    .line 135
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_cbt:Lorg/apache/poi/hwpf/model/CHPBinTable;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/model/CHPBinTable;->rebuild(Lorg/apache/poi/hwpf/model/ComplexFileTable;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_pbt:Lorg/apache/poi/hwpf/model/PAPBinTable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_text:Ljava/lang/StringBuilder;

    move-object/from16 v0, v17

    invoke-virtual {v3, v4, v0}, Lorg/apache/poi/hwpf/model/PAPBinTable;->rebuild(Ljava/lang/StringBuilder;Lorg/apache/poi/hwpf/model/ComplexFileTable;)V

    .line 138
    :cond_0
    return-void

    .line 78
    .end local v20    # "preserveBinTables":Z
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 79
    .local v23, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 86
    .end local v23    # "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    :cond_2
    new-instance v19, Lorg/apache/poi/hwpf/model/PieceDescriptor;

    const/16 v3, 0x8

    new-array v3, v3, [B

    const/4 v4, 0x5

    const/16 v7, 0x7f

    aput-byte v7, v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v4}, Lorg/apache/poi/hwpf/model/PieceDescriptor;-><init>([BI)V

    .line 87
    .local v19, "pd":Lorg/apache/poi/hwpf/model/PieceDescriptor;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->setFilePosition(I)V

    .line 91
    new-instance v3, Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-direct {v3}, Lorg/apache/poi/hwpf/model/TextPieceTable;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    .line 92
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMac()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v4

    sub-int/2addr v3, v4

    new-array v0, v3, [B

    move-object/from16 v22, v0

    .line 93
    .local v22, "textData":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v4

    const/4 v7, 0x0

    move-object/from16 v0, v22

    array-length v8, v0

    move-object/from16 v0, v22

    invoke-static {v3, v4, v0, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    new-instance v23, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 95
    const/4 v3, 0x0

    move-object/from16 v0, v22

    array-length v4, v0

    .line 94
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-direct {v0, v3, v4, v1, v2}, Lorg/apache/poi/hwpf/model/TextPiece;-><init>(II[BLorg/apache/poi/hwpf/model/PieceDescriptor;)V

    .line 97
    .restart local v23    # "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lorg/apache/poi/hwpf/model/TextPieceTable;->add(Lorg/apache/poi/hwpf/model/TextPiece;)V

    .line 99
    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 128
    .end local v19    # "pd":Lorg/apache/poi/hwpf/model/PieceDescriptor;
    .end local v22    # "textData":[B
    .end local v23    # "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    .restart local v20    # "preserveBinTables":Z
    :catch_0
    move-exception v3

    goto/16 :goto_2
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 45
    return-void
.end method


# virtual methods
.method public getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 4

    .prologue
    .line 143
    new-instance v0, Lorg/apache/poi/hwpf/usermodel/Range;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMac()I

    move-result v2

    iget-object v3, p0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FileInformationBlock;->getFibBase()Lorg/apache/poi/hwpf/model/FibBase;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/FibBase;->getFcMin()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2, p0}, Lorg/apache/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getRange()Lorg/apache/poi/hwpf/usermodel/Range;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/HWPFOldDocument;->getOverallRange()Lorg/apache/poi/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFOldDocument;->_text:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getTextTable()Lorg/apache/poi/hwpf/model/TextPieceTable;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/poi/hwpf/model/TextPieceTable;

    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Writing is not available for the older file formats"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
