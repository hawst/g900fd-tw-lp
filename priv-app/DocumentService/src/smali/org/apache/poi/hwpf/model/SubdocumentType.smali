.class public final enum Lorg/apache/poi/hwpf/model/SubdocumentType;
.super Ljava/lang/Enum;
.source "SubdocumentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hwpf/model/SubdocumentType;",
        ">;"
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field public static final enum ANNOTATION:Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final enum ENDNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final enum FOOTNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final enum HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final enum HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final enum MACRO:Lorg/apache/poi/hwpf/model/SubdocumentType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final ORDERED:[Lorg/apache/poi/hwpf/model/SubdocumentType;

.field public static final enum TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "MAIN"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 30
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "FOOTNOTE"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 32
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "HEADER"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 34
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "MACRO"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    .line 35
    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->MACRO:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 37
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "ANNOTATION"

    invoke-direct {v0, v1, v7}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ANNOTATION:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 39
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "ENDNOTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENDNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 41
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "TEXTBOX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 43
    new-instance v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    const-string/jumbo v1, "HEADER_TEXTBOX"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/SubdocumentType;

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->MACRO:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->ANNOTATION:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENDNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/poi/hwpf/model/SubdocumentType;->TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENUM$VALUES:[Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 49
    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 50
    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->MAIN:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->FOOTNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->MACRO:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/hwpf/model/SubdocumentType;->ANNOTATION:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENDNOTE:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/poi/hwpf/model/SubdocumentType;->TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 51
    sget-object v2, Lorg/apache/poi/hwpf/model/SubdocumentType;->HEADER_TEXTBOX:Lorg/apache/poi/hwpf/model/SubdocumentType;

    aput-object v2, v0, v1

    .line 49
    sput-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ORDERED:[Lorg/apache/poi/hwpf/model/SubdocumentType;

    .line 51
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hwpf/model/SubdocumentType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/model/SubdocumentType;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/hwpf/model/SubdocumentType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hwpf/model/SubdocumentType;->ENUM$VALUES:[Lorg/apache/poi/hwpf/model/SubdocumentType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hwpf/model/SubdocumentType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
