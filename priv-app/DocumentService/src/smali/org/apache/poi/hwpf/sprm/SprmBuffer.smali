.class public final Lorg/apache/poi/hwpf/sprm/SprmBuffer;
.super Ljava/lang/Object;
.source "SprmBuffer.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# instance fields
.field _buf:[B

.field _istd:Z

.field _offset:I

.field private final _sprmsStartOffset:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 41
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "sprmsStartOffset"    # I

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    add-int/lit8 v0, p1, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 77
    iput p1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 78
    iput p1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_sprmsStartOffset:I

    .line 79
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "buf"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 50
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "_sprmsStartOffset"    # I

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BZI)V

    .line 72
    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "istd"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;-><init>([BZI)V

    .line 59
    return-void
.end method

.method public constructor <init>([BZI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "istd"    # Z
    .param p3, "sprmsStartOffset"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    array-length v0, p1

    iput v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 64
    iput-object p1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 65
    iput-boolean p2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_istd:Z

    .line 66
    iput p3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_sprmsStartOffset:I

    .line 67
    return-void
.end method

.method private ensureCapacity(I)V
    .locals 4
    .param p1, "addition"    # I

    .prologue
    const/4 v3, 0x0

    .line 141
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 148
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/2addr v1, p1

    new-array v0, v1, [B

    .line 149
    .local v0, "newBuf":[B
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    iput-object v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 152
    .end local v0    # "newBuf":[B
    :cond_0
    return-void
.end method

.method private findSprmOffset(S)I
    .locals 2
    .param p1, "opcode"    # S

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->findSprm(S)Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v0

    .line 181
    .local v0, "sprmOperation":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    if-nez v0, :cond_0

    .line 182
    const/4 v1, -0x1

    .line 184
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public addSprm(SB)V
    .locals 4
    .param p1, "opcode"    # S
    .param p2, "operand"    # B

    .prologue
    .line 83
    const/4 v0, 0x3

    .line 84
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 85
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 86
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 87
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    aput-byte p2, v1, v2

    .line 88
    return-void
.end method

.method public addSprm(SI)V
    .locals 3
    .param p1, "opcode"    # S
    .param p2, "operand"    # I

    .prologue
    .line 102
    const/4 v0, 0x6

    .line 103
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 104
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 105
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 106
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 107
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 108
    return-void
.end method

.method public addSprm(SS)V
    .locals 3
    .param p1, "opcode"    # S
    .param p2, "operand"    # S

    .prologue
    .line 112
    const/4 v0, 0x4

    .line 113
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 114
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 115
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 116
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 117
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 118
    return-void
.end method

.method public addSprm(S[B)V
    .locals 5
    .param p1, "opcode"    # S
    .param p2, "operand"    # [B

    .prologue
    .line 92
    array-length v1, p2

    add-int/lit8 v0, v1, 0x3

    .line 93
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 94
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 95
    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 96
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v3, p2

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 97
    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v4, p2

    invoke-static {p2, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 98
    return-void
.end method

.method public append([B)V
    .locals 1
    .param p1, "grpprl"    # [B

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->append([BI)V

    .line 123
    return-void
.end method

.method public append([BI)V
    .locals 3
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 127
    array-length v0, p1

    sub-int/2addr v0, p2

    invoke-direct {p0, v0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 128
    iget-object v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v2, p1

    sub-int/2addr v2, p2

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    iget v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v1, p1

    sub-int/2addr v1, p2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 130
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 134
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 135
    .local v0, "retVal":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v1, v1

    new-array v1, v1, [B

    iput-object v1, v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 136
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v2, v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 137
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 156
    if-eqz p1, :cond_0

    instance-of v1, p1, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 157
    check-cast v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;

    .line 158
    .local v0, "sprmBuf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v2, v0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    .line 160
    .end local v0    # "sprmBuf":Lorg/apache/poi/hwpf/sprm/SprmBuffer;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public findSprm(S)Lorg/apache/poi/hwpf/sprm/SprmOperation;
    .locals 6
    .param p1, "opcode"    # S

    .prologue
    .line 165
    invoke-static {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperationFromOpcode(S)I

    move-result v1

    .line 166
    .local v1, "operation":I
    invoke-static {p1}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getTypeFromOpcode(S)I

    move-result v3

    .line 168
    .local v3, "type":I
    new-instance v2, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    iget-object v4, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    const/4 v5, 0x2

    invoke-direct {v2, v4, v5}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 169
    .local v2, "si":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :cond_0
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 175
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 171
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v0

    .line 172
    .local v0, "i":Lorg/apache/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v4

    if-ne v4, v1, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v4

    if-ne v4, v3, :cond_0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/poi/hwpf/sprm/SprmIterator;
    .locals 3

    .prologue
    .line 194
    new-instance v0, Lorg/apache/poi/hwpf/sprm/SprmIterator;

    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_sprmsStartOffset:I

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    return-object v0
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 244
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    .local v2, "stringBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "Sprms ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    iget-object v3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 247
    const-string/jumbo v3, " byte(s)): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->iterator()Lorg/apache/poi/hwpf/sprm/SprmIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/poi/hwpf/sprm/SprmIterator;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 260
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 252
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/poi/hwpf/sprm/SprmOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_1
    const-string/jumbo v3, "; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 254
    :catch_0
    move-exception v0

    .line 256
    .local v0, "exc":Ljava/lang/Exception;
    const-string/jumbo v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public updateSprm(SB)V
    .locals 2
    .param p1, "opcode"    # S
    .param p2, "operand"    # B

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    .line 200
    .local v0, "grpprlOffset":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 202
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    aput-byte p2, v1, v0

    .line 206
    :goto_0
    return-void

    .line 205
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->addSprm(SB)V

    goto :goto_0
.end method

.method public updateSprm(SI)V
    .locals 2
    .param p1, "opcode"    # S
    .param p2, "operand"    # I

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    .line 222
    .local v0, "grpprlOffset":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 224
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v1, v0, p2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 228
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->addSprm(SI)V

    goto :goto_0
.end method

.method public updateSprm(SS)V
    .locals 2
    .param p1, "opcode"    # S
    .param p2, "operand"    # S

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    .line 233
    .local v0, "grpprlOffset":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 235
    iget-object v1, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v1, v0, p2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->addSprm(SS)V

    goto :goto_0
.end method

.method public updateSprm(SZ)V
    .locals 4
    .param p1, "opcode"    # S
    .param p2, "operand"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 210
    invoke-direct {p0, p1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    .line 211
    .local v0, "grpprlOffset":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 213
    iget-object v3, p0, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    if-eqz p2, :cond_0

    :goto_0
    int-to-byte v1, v1

    aput-byte v1, v3, v0

    .line 217
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 213
    goto :goto_0

    .line 216
    :cond_1
    if-eqz p2, :cond_2

    :goto_2
    invoke-virtual {p0, p1, v1}, Lorg/apache/poi/hwpf/sprm/SprmBuffer;->addSprm(SI)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
