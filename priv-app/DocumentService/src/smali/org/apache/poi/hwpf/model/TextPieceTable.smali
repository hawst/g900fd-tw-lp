.class public Lorg/apache/poi/hwpf/model/TextPieceTable;
.super Ljava/lang/Object;
.source "TextPieceTable.java"

# interfaces
.implements Lorg/apache/poi/hwpf/model/CharIndexTranslator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;
    }
.end annotation

.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field _cpMin:I

.field protected _textPieces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field

.field protected _textPiecesFCOrder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->$assertionsDisabled:Z

    .line 45
    const-class v0, Lorg/apache/poi/hwpf/model/TextPieceTable;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 44
    sput-object v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->logger:Lorg/apache/poi/util/POILogger;

    .line 45
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 54
    return-void
.end method

.method public constructor <init>([B[BIII)V
    .locals 21
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "size"    # I
    .param p5, "fcMin"    # I

    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 50
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 60
    new-instance v11, Lorg/apache/poi/hwpf/model/PlexOfCps;

    .line 61
    invoke-static {}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getSizeInBytes()I

    move-result v18

    .line 60
    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, v18

    invoke-direct {v11, v0, v1, v2, v3}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 63
    .local v11, "pieceTable":Lorg/apache/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/PlexOfCps;->length()I

    move-result v5

    .line 64
    .local v5, "length":I
    new-array v12, v5, [Lorg/apache/poi/hwpf/model/PieceDescriptor;

    .line 68
    .local v12, "pieces":[Lorg/apache/poi/hwpf/model/PieceDescriptor;
    const/16 v17, 0x0

    .local v17, "x":I
    :goto_0
    move/from16 v0, v17

    if-lt v0, v5, :cond_0

    .line 76
    const/16 v18, 0x0

    aget-object v18, v12, v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v18

    sub-int v18, v18, p5

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/hwpf/model/TextPieceTable;->_cpMin:I

    .line 77
    const/16 v17, 0x0

    :goto_1
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_1

    .line 87
    const/16 v17, 0x0

    :goto_2
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 130
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    new-instance v19, Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;

    const/16 v20, 0x0

    invoke-direct/range {v19 .. v20}, Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;-><init>(Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;)V

    invoke-static/range {v18 .. v19}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 132
    return-void

    .line 70
    :cond_0
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v8

    .line 71
    .local v8, "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    new-instance v18, Lorg/apache/poi/hwpf/model/PieceDescriptor;

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v19

    const/16 v20, 0x0

    invoke-direct/range {v18 .. v20}, Lorg/apache/poi/hwpf/model/PieceDescriptor;-><init>([BI)V

    aput-object v18, v12, v17

    .line 68
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 79
    .end local v8    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    :cond_1
    aget-object v18, v12, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v18

    sub-int v13, v18, p5

    .line 80
    .local v13, "start":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_cpMin:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_2

    .line 82
    move-object/from16 v0, p0

    iput v13, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_cpMin:I

    .line 77
    :cond_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 89
    .end local v13    # "start":I
    :cond_3
    aget-object v18, v12, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v13

    .line 90
    .restart local v13    # "start":I
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/apache/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    move-result-object v8

    .line 93
    .restart local v8    # "node":Lorg/apache/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v10

    .line 94
    .local v10, "nodeStartChars":I
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v9

    .line 97
    .local v9, "nodeEndChars":I
    aget-object v18, v12, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v16

    .line 98
    .local v16, "unicode":Z
    const/4 v6, 0x1

    .line 99
    .local v6, "multiple":I
    if-eqz v16, :cond_4

    .line 101
    const/4 v6, 0x2

    .line 105
    :cond_4
    sub-int v15, v9, v10

    .line 106
    .local v15, "textSizeChars":I
    mul-int v14, v15, v6

    .line 109
    .local v14, "textSizeBytes":I
    const/high16 v18, 0x80000

    move/from16 v0, v18

    if-le v14, v0, :cond_5

    .line 110
    const/high16 v14, 0x80000

    .line 111
    div-int v15, v14, v6

    .line 112
    add-int v9, v15, v10

    .line 117
    :cond_5
    new-array v4, v14, [B

    .line 118
    .local v4, "buf":[B
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v13, v4, v1, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    new-instance v7, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 122
    aget-object v18, v12, v17

    .line 121
    move-object/from16 v0, v18

    invoke-direct {v7, v10, v9, v4, v0}, Lorg/apache/poi/hwpf/model/TextPiece;-><init>(II[BLorg/apache/poi/hwpf/model/PieceDescriptor;)V

    .line 124
    .local v7, "newTextPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public add(Lorg/apache/poi/hwpf/model/TextPiece;)V
    .locals 3
    .param p1, "piece"    # Lorg/apache/poi/hwpf/model/TextPiece;

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 139
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    new-instance v1, Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;-><init>(Lorg/apache/poi/hwpf/model/TextPieceTable$FCComparator;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 140
    return-void
.end method

.method public adjustForInsert(II)I
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 152
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 154
    .local v0, "size":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 157
    .local v1, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v1, v3}, Lorg/apache/poi/hwpf/model/TextPiece;->setEnd(I)V

    .line 160
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 168
    return p2

    .line 162
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    check-cast v1, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 163
    .restart local v1    # "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v1, v3}, Lorg/apache/poi/hwpf/model/TextPiece;->setStart(I)V

    .line 164
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v1, v3}, Lorg/apache/poi/hwpf/model/TextPiece;->setEnd(I)V

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 174
    if-eqz p1, :cond_3

    instance-of v3, p1, Lorg/apache/poi/hwpf/model/TextPieceTable;

    if-eqz v3, :cond_3

    move-object v1, p1

    .line 175
    check-cast v1, Lorg/apache/poi/hwpf/model/TextPieceTable;

    .line 177
    .local v1, "tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    iget-object v3, v1, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 178
    .local v0, "size":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v0, v3, :cond_2

    .line 180
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 187
    const/4 v3, 0x1

    .line 191
    .end local v0    # "size":I
    .end local v1    # "tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    .end local v2    # "x":I
    :goto_1
    return v3

    .line 182
    .restart local v0    # "size":I
    .restart local v1    # "tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    .restart local v2    # "x":I
    :cond_0
    iget-object v3, v1, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/TextPiece;

    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/poi/hwpf/model/TextPiece;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    .line 184
    goto :goto_1

    .line 180
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v2    # "x":I
    :cond_2
    move v3, v4

    .line 189
    goto :goto_1

    .end local v0    # "size":I
    .end local v1    # "tpt":Lorg/apache/poi/hwpf/model/TextPieceTable;
    :cond_3
    move v3, v4

    .line 191
    goto :goto_1
.end method

.method public getByteIndex(I)I
    .locals 9
    .param p1, "charPos"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    .line 196
    const/4 v0, 0x0

    .line 197
    .local v0, "byteCount":I
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 218
    :goto_0
    return v0

    .line 197
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 199
    .local v2, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    if-lt p1, v3, :cond_3

    .line 201
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v7

    .line 202
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v8

    sub-int v8, v3, v8

    .line 203
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 202
    :goto_1
    mul-int/2addr v3, v8

    .line 201
    add-int v0, v7, v3

    .line 205
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    if-ne p1, v3, :cond_0

    goto :goto_0

    :cond_2
    move v3, v5

    .line 203
    goto :goto_1

    .line 210
    :cond_3
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 212
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v3

    sub-int v1, p1, v3

    .line 213
    .local v1, "left":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v3

    .line 214
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v6

    if-eqz v6, :cond_4

    :goto_2
    mul-int/2addr v4, v1

    .line 213
    add-int v0, v3, v4

    .line 215
    goto :goto_0

    :cond_4
    move v4, v5

    .line 214
    goto :goto_2
.end method

.method public getCharIndex(I)I
    .locals 1
    .param p1, "bytePos"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 224
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hwpf/model/TextPieceTable;->getCharIndex(II)I

    move-result v0

    return v0
.end method

.method public getCharIndex(II)I
    .locals 9
    .param p1, "startBytePos"    # I
    .param p2, "startCP"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 230
    const/4 v2, 0x0

    .line 232
    .local v2, "charCount":I
    invoke-virtual {p0, p1}, Lorg/apache/poi/hwpf/model/TextPieceTable;->lookIndexForward(I)I

    move-result v0

    .line 234
    .local v0, "bytePos":I
    iget-object v7, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 272
    :goto_0
    return v2

    .line 234
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 236
    .local v6, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v4

    .line 238
    .local v4, "pieceStart":I
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v1

    .line 239
    .local v1, "bytesLength":I
    add-int v3, v4, v1

    .line 243
    .local v3, "pieceEnd":I
    if-lt v0, v4, :cond_2

    if-le v0, v3, :cond_3

    .line 245
    :cond_2
    move v5, v1

    .line 256
    .local v5, "toAdd":I
    :goto_1
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 258
    div-int/lit8 v8, v5, 0x2

    add-int/2addr v2, v8

    .line 265
    :goto_2
    if-lt v0, v4, :cond_0

    if-gt v0, v3, :cond_0

    .line 266
    if-lt v2, p2, :cond_0

    goto :goto_0

    .line 247
    .end local v5    # "toAdd":I
    :cond_3
    if-le v0, v4, :cond_4

    if-ge v0, v3, :cond_4

    .line 249
    sub-int v5, v0, v4

    .line 250
    .restart local v5    # "toAdd":I
    goto :goto_1

    .line 253
    .end local v5    # "toAdd":I
    :cond_4
    sub-int v8, v3, v0

    sub-int v5, v1, v8

    .restart local v5    # "toAdd":I
    goto :goto_1

    .line 262
    :cond_5
    add-int/2addr v2, v5

    goto :goto_2
.end method

.method public getCharIndexRanges(II)[[I
    .locals 13
    .param p1, "startBytePosInclusive"    # I
    .param p2, "endBytePosExclusive"    # I

    .prologue
    .line 278
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 279
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<[I>;"
    iget-object v10, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    .line 308
    :cond_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [[I

    invoke-interface {v6, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [[I

    return-object v10

    .line 279
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 281
    .local v7, "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v11

    .line 282
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v9

    .line 283
    .local v9, "tpStart":I
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v11

    .line 284
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v12

    .line 283
    add-int v8, v11, v12

    .line 285
    .local v8, "tpEnd":I
    if-gt p1, v8, :cond_0

    .line 287
    if-lt p2, v9, :cond_1

    .line 290
    invoke-static {v9, p1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 292
    .local v4, "rangeStartBytes":I
    invoke-static {v8, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 293
    .local v1, "rangeEndBytes":I
    sub-int v3, v1, v4

    .line 295
    .local v3, "rangeLengthBytes":I
    if-gt v4, v1, :cond_0

    .line 298
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v0, 0x2

    .line 300
    .local v0, "encodingMultiplier":I
    :goto_1
    invoke-virtual {v7}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v11

    .line 301
    sub-int v12, v4, v9

    div-int/2addr v12, v0

    .line 300
    add-int v5, v11, v12

    .line 303
    .local v5, "rangeStartCp":I
    div-int v11, v3, v0

    .line 302
    add-int v2, v5, v11

    .line 305
    .local v2, "rangeEndCp":I
    const/4 v11, 0x2

    new-array v11, v11, [I

    const/4 v12, 0x0

    aput v5, v11, v12

    const/4 v12, 0x1

    aput v2, v11, v12

    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    .end local v0    # "encodingMultiplier":I
    .end local v2    # "rangeEndCp":I
    .end local v5    # "rangeStartCp":I
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public getCpMin()I
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_cpMin:I

    return v0
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 18

    .prologue
    .line 318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 321
    .local v12, "start":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 322
    .local v10, "docText":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 351
    :cond_0
    sget-object v2, Lorg/apache/poi/hwpf/model/TextPieceTable;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x1

    const-string/jumbo v4, "Document text were rebuilded in "

    .line 352
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v12

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string/jumbo v6, " ms ("

    .line 353
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string/jumbo v8, " chars)"

    .line 351
    invoke-virtual/range {v2 .. v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 355
    return-object v10

    .line 322
    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 324
    .local v11, "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 325
    .local v14, "toAppend":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v15

    .line 328
    .local v15, "toAppendLength":I
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v2

    const/high16 v3, 0x80000

    if-gt v2, v3, :cond_0

    .line 329
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v2

    const/high16 v3, 0x80000

    if-gt v2, v3, :cond_0

    .line 334
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v2

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v3

    sub-int/2addr v2, v3

    if-eq v15, v2, :cond_2

    .line 336
    sget-object v2, Lorg/apache/poi/hwpf/model/TextPieceTable;->logger:Lorg/apache/poi/util/POILogger;

    .line 337
    const/4 v3, 0x5

    .line 338
    const-string/jumbo v4, "Text piece has boundaries ["

    .line 339
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 340
    const-string/jumbo v6, "; "

    .line 341
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 342
    const-string/jumbo v8, ") but length "

    .line 343
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v9

    .line 344
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v17

    .line 343
    sub-int v9, v9, v17

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 336
    invoke-virtual/range {v2 .. v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 347
    :cond_2
    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v2

    invoke-virtual {v11}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v3

    .line 348
    add-int/2addr v3, v15

    .line 347
    invoke-virtual {v10, v2, v3, v14}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method public getTextPieces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isIndexInTable(I)Z
    .locals 5
    .param p1, "bytePos"    # I

    .prologue
    const/4 v2, 0x0

    .line 371
    iget-object v3, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 388
    :cond_1
    :goto_0
    return v2

    .line 371
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 373
    .local v1, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v0

    .line 375
    .local v0, "pieceStart":I
    invoke-virtual {v1}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v4

    add-int/2addr v4, v0

    if-gt p1, v4, :cond_0

    .line 380
    if-gt v0, p1, :cond_1

    .line 385
    const/4 v2, 0x1

    goto :goto_0
.end method

.method isIndexInTable(II)Z
    .locals 7
    .param p1, "startBytePos"    # I
    .param p2, "endBytePos"    # I

    .prologue
    const/4 v4, 0x0

    .line 393
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 411
    :cond_1
    :goto_0
    return v4

    .line 393
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 395
    .local v3, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v1

    .line 397
    .local v1, "pieceStart":I
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v6

    add-int/2addr v6, v1

    if-ge p1, v6, :cond_0

    .line 402
    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 403
    .local v0, "left":I
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v5

    add-int/2addr v5, v1

    invoke-static {p2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 405
    .local v2, "right":I
    if-ge v0, v2, :cond_1

    .line 408
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public lookIndexBackward(I)I
    .locals 6
    .param p1, "startBytePos"    # I

    .prologue
    .line 416
    move v0, p1

    .line 417
    .local v0, "bytePos":I
    const/4 v1, 0x0

    .line 419
    .local v1, "lastEnd":I
    iget-object v4, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 437
    :cond_0
    :goto_1
    return v0

    .line 419
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 421
    .local v3, "tp":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v2

    .line 423
    .local v2, "pieceStart":I
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v5

    add-int/2addr v5, v2

    if-le v0, v5, :cond_2

    .line 425
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v5

    add-int v1, v2, v5

    .line 426
    goto :goto_0

    .line 429
    :cond_2
    if-le v2, v0, :cond_0

    .line 431
    move v0, v1

    .line 434
    goto :goto_1
.end method

.method public lookIndexForward(I)I
    .locals 7
    .param p1, "startBytePos"    # I

    .prologue
    const/4 v6, 0x0

    .line 442
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 443
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string/jumbo v6, "Text pieces table is empty"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 445
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v5

    if-le v5, p1, :cond_2

    .line 446
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result p1

    .line 477
    .end local p1    # "startBytePos":I
    :cond_1
    :goto_0
    return p1

    .line 448
    .restart local p1    # "startBytePos":I
    :cond_2
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    iget-object v6, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 449
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v5

    if-le v5, p1, :cond_1

    .line 452
    const/4 v1, 0x0

    .line 453
    .local v1, "low":I
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 455
    .local v0, "high":I
    :goto_1
    if-le v1, v0, :cond_3

    .line 469
    sget-boolean v5, Lorg/apache/poi/hwpf/model/TextPieceTable;->$assertionsDisabled:Z

    if-nez v5, :cond_6

    if-eq v1, v0, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 457
    :cond_3
    add-int v5, v1, v0

    ushr-int/lit8 v2, v5, 0x1

    .line 458
    .local v2, "mid":I
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 459
    .local v4, "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v3

    .line 461
    .local v3, "midVal":I
    if-ge v3, p1, :cond_4

    .line 462
    add-int/lit8 v1, v2, 0x1

    goto :goto_1

    .line 463
    :cond_4
    if-le v3, p1, :cond_5

    .line 464
    add-int/lit8 v0, v2, -0x1

    goto :goto_1

    .line 467
    :cond_5
    invoke-virtual {v4}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result p1

    goto :goto_0

    .line 470
    .end local v2    # "mid":I
    .end local v3    # "midVal":I
    .end local v4    # "textPiece":Lorg/apache/poi/hwpf/model/TextPiece;
    :cond_6
    sget-boolean v5, Lorg/apache/poi/hwpf/model/TextPieceTable;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    .line 471
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v5

    if-lt v5, p1, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 473
    :cond_7
    sget-boolean v5, Lorg/apache/poi/hwpf/model/TextPieceTable;->$assertionsDisabled:Z

    if-nez v5, :cond_8

    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    .line 474
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v5

    if-gt v5, p1, :cond_8

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 477
    :cond_8
    iget-object v5, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hwpf/model/TextPiece;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result p1

    goto/16 :goto_0
.end method

.method public writeTo(Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;)[B
    .locals 12
    .param p1, "docStream"    # Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 482
    new-instance v8, Lorg/apache/poi/hwpf/model/PlexOfCps;

    invoke-static {}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->getSizeInBytes()I

    move-result v10

    invoke-direct {v8, v10}, Lorg/apache/poi/hwpf/model/PlexOfCps;-><init>(I)V

    .line 485
    .local v8, "textPlex":Lorg/apache/poi/hwpf/model/PlexOfCps;
    iget-object v10, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 486
    .local v7, "size":I
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    if-lt v9, v7, :cond_0

    .line 516
    invoke-virtual {v8}, Lorg/apache/poi/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object v10

    return-object v10

    .line 488
    :cond_0
    iget-object v10, p0, Lorg/apache/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hwpf/model/TextPiece;

    .line 489
    .local v2, "next":Lorg/apache/poi/hwpf/model/TextPiece;
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/poi/hwpf/model/PieceDescriptor;

    move-result-object v6

    .line 491
    .local v6, "pd":Lorg/apache/poi/hwpf/model/PieceDescriptor;
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v5

    .line 492
    .local v5, "offset":I
    rem-int/lit16 v1, v5, 0x200

    .line 493
    .local v1, "mod":I
    if-eqz v1, :cond_1

    .line 495
    rsub-int v1, v1, 0x200

    .line 496
    new-array v0, v1, [B

    .line 497
    .local v0, "buf":[B
    invoke-virtual {p1, v0}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 501
    .end local v0    # "buf":[B
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v10

    invoke-virtual {v6, v10}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->setFilePosition(I)V

    .line 506
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getRawBytes()[B

    move-result-object v10

    invoke-virtual {p1, v10}, Lorg/apache/poi/hwpf/model/io/HWPFOutputStream;->write([B)V

    .line 510
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getStart()I

    move-result v4

    .line 511
    .local v4, "nodeStart":I
    invoke-virtual {v2}, Lorg/apache/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    .line 512
    .local v3, "nodeEnd":I
    new-instance v10, Lorg/apache/poi/hwpf/model/GenericPropertyNode;

    .line 513
    invoke-virtual {v6}, Lorg/apache/poi/hwpf/model/PieceDescriptor;->toByteArray()[B

    move-result-object v11

    invoke-direct {v10, v4, v3, v11}, Lorg/apache/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    .line 512
    invoke-virtual {v8, v10}, Lorg/apache/poi/hwpf/model/PlexOfCps;->addProperty(Lorg/apache/poi/hwpf/model/GenericPropertyNode;)V

    .line 486
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method
