.class public final Lorg/apache/poi/hwpf/model/PropertyModifier;
.super Ljava/lang/Object;
.source "PropertyModifier.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static _fComplex:Lorg/apache/poi/util/BitField;

.field private static _figrpprl:Lorg/apache/poi/util/BitField;

.field private static _fisprm:Lorg/apache/poi/util/BitField;

.field private static _fval:Lorg/apache/poi/util/BitField;


# instance fields
.field private value:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_fComplex:Lorg/apache/poi/util/BitField;

    .line 33
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0xfffe

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_figrpprl:Lorg/apache/poi/util/BitField;

    .line 38
    new-instance v0, Lorg/apache/poi/util/BitField;

    const/16 v1, 0xfe

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_fisprm:Lorg/apache/poi/util/BitField;

    .line 43
    new-instance v0, Lorg/apache/poi/util/BitField;

    const v1, 0xff00

    invoke-direct {v0, v1}, Lorg/apache/poi/util/BitField;-><init>(I)V

    sput-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_fval:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>(S)V
    .locals 0
    .param p1, "value"    # S

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-short p1, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    .line 50
    return-void
.end method


# virtual methods
.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->clone()Lorg/apache/poi/hwpf/model/PropertyModifier;

    move-result-object v0

    return-object v0
.end method

.method protected clone()Lorg/apache/poi/hwpf/model/PropertyModifier;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lorg/apache/poi/hwpf/model/PropertyModifier;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    invoke-direct {v0, v1}, Lorg/apache/poi/hwpf/model/PropertyModifier;-><init>(S)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    if-ne p0, p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 64
    goto :goto_0

    .line 65
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 66
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 67
    check-cast v0, Lorg/apache/poi/hwpf/model/PropertyModifier;

    .line 68
    .local v0, "other":Lorg/apache/poi/hwpf/model/PropertyModifier;
    iget-short v3, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 69
    goto :goto_0
.end method

.method public getIgrpprl()S
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not complex"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    sget-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_figrpprl:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getIsprm()S
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not simple"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    sget-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_fisprm:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getVal()S
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not simple"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    sget-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_fval:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getValue()S
    .locals 1

    .prologue
    .line 102
    iget-short v0, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 108
    const/16 v0, 0x1f

    .line 109
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 110
    .local v1, "result":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    add-int/lit8 v1, v2, 0x1f

    .line 111
    return v1
.end method

.method public isComplex()Z
    .locals 2

    .prologue
    .line 116
    sget-object v0, Lorg/apache/poi/hwpf/model/PropertyModifier;->_fComplex:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hwpf/model/PropertyModifier;->value:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[PRM] (complex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 125
    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->isComplex()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    const-string/jumbo v1, "igrpprl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->getIgrpprl()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 130
    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :goto_0
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 134
    :cond_0
    const-string/jumbo v1, "isprm: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->getIsprm()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 136
    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string/jumbo v1, "val: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/model/PropertyModifier;->getVal()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 139
    const-string/jumbo v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
