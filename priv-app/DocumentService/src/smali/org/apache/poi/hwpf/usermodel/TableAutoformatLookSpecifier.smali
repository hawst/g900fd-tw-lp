.class public Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;
.super Lorg/apache/poi/hwpf/model/types/TLPAbstractType;
.source "TableAutoformatLookSpecifier.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/poi/hwpf/model/types/TLPAbstractType;-><init>()V

    .line 34
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->fillFields([BI)V

    .line 35
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->clone()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;
    .locals 3

    .prologue
    .line 42
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 44
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p0, p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 56
    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 58
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 59
    check-cast v0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;

    .line 60
    .local v0, "other":Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;
    iget-short v3, p0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_1_itl:S

    iget-short v4, v0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_1_itl:S

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 61
    goto :goto_0

    .line 62
    :cond_4
    iget-byte v3, p0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_2_tlp_flags:B

    iget-byte v4, v0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_2_tlp_flags:B

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 63
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 70
    const/16 v0, 0x1f

    .line 71
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 72
    .local v1, "result":I
    iget-short v2, p0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_1_itl:S

    add-int/lit8 v1, v2, 0x1f

    .line 73
    mul-int/lit8 v2, v1, 0x1f

    iget-byte v3, p0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_2_tlp_flags:B

    add-int v1, v2, v3

    .line 74
    return v1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 79
    iget-short v0, p0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_1_itl:S

    if-nez v0, :cond_0

    iget-byte v0, p0, Lorg/apache/poi/hwpf/usermodel/TableAutoformatLookSpecifier;->field_2_tlp_flags:B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
