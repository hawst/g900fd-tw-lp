.class public Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
.super Ljava/lang/Object;
.source "ZipInputStreamZipEntrySource.java"

# interfaces
.implements Lorg/apache/poi/openxml4j/util/ZipEntrySource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$EntryEnumerator;,
        Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "ZipInputStreamZipEntrySource"


# instance fields
.field private imgEntry:Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

.field private zipEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/zip/ZipInputStream;)V
    .locals 8
    .param p1, "imagePath"    # Ljava/lang/String;
    .param p2, "inp"    # Ljava/util/zip/ZipInputStream;

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    const/4 v2, 0x1

    .line 189
    .local v2, "going":Z
    :goto_0
    if-nez v2, :cond_1

    .line 208
    :goto_1
    if-eqz p2, :cond_0

    .line 210
    :try_start_0
    invoke-virtual {p2}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 216
    :cond_0
    :goto_2
    return-void

    .line 190
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v3

    .line 191
    .local v3, "zipEntry":Ljava/util/zip/ZipEntry;
    if-nez v3, :cond_2

    .line 192
    const/4 v2, 0x0

    .line 193
    goto :goto_0

    .line 195
    :cond_2
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 196
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 197
    new-instance v1, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

    invoke-direct {v1, v3, p2}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;-><init>(Ljava/util/zip/ZipEntry;Ljava/util/zip/ZipInputStream;)V

    .line 198
    .local v1, "entry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    invoke-virtual {p2}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 199
    iput-object v1, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->imgEntry:Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 205
    .end local v1    # "entry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v4, "ZipInputStreamZipEntrySource"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "IOException: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 208
    if-eqz p2, :cond_0

    .line 210
    :try_start_3
    invoke-virtual {p2}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 211
    :catch_1
    move-exception v0

    .line 212
    const-string/jumbo v4, "ZipInputStreamZipEntrySource"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "IOException: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 202
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_3
    :try_start_4
    invoke-virtual {p2}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 207
    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v4

    .line 208
    if-eqz p2, :cond_4

    .line 210
    :try_start_5
    invoke-virtual {p2}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 215
    :cond_4
    :goto_3
    throw v4

    .line 211
    :catch_2
    move-exception v0

    .line 212
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "ZipInputStreamZipEntrySource"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "IOException: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 211
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 212
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "ZipInputStreamZipEntrySource"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "IOException: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public constructor <init>(Ljava/util/zip/ZipInputStream;)V
    .locals 12
    .param p1, "inp"    # Ljava/util/zip/ZipInputStream;

    .prologue
    const/4 v10, 0x1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->zipEntries:Ljava/util/ArrayList;

    .line 55
    const/4 v3, 0x1

    .line 58
    .local v3, "going":Z
    :goto_0
    if-nez v3, :cond_1

    .line 175
    if-eqz p1, :cond_0

    .line 177
    :try_start_0
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 183
    :cond_0
    :goto_1
    return-void

    .line 59
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    .line 60
    .local v7, "zipEntry":Ljava/util/zip/ZipEntry;
    if-nez v7, :cond_2

    .line 61
    const/4 v3, 0x0

    .line 62
    goto :goto_0

    .line 64
    :cond_2
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    .line 65
    .local v4, "name":Ljava/lang/String;
    if-eqz v4, :cond_b

    const-string/jumbo v8, "xl/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 66
    const-string/jumbo v8, "xl/worksheets/sheet"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 67
    const-string/jumbo v8, "/sheet"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 68
    .local v5, "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 69
    .local v1, "end":I
    add-int/lit8 v8, v5, 0x6

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 70
    .local v6, "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 71
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 170
    .end local v1    # "end":I
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    .end local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/NumberFormatException;
    :try_start_2
    const-string/jumbo v8, "ZipInputStreamZipEntrySource"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "NFEException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    if-eqz p1, :cond_0

    .line 177
    :try_start_3
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 178
    :catch_1
    move-exception v0

    .line 179
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v8, "ZipInputStreamZipEntrySource"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "IOException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 74
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "name":Ljava/lang/String;
    .restart local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_3
    :try_start_4
    const-string/jumbo v8, "xl/worksheets/_rels/sheet"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 75
    const-string/jumbo v8, "/sheet"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 76
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 77
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0x6

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 78
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 79
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 172
    .end local v1    # "end":I
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    .end local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_2
    move-exception v0

    .line 173
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_5
    const-string/jumbo v8, "ZipInputStreamZipEntrySource"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "IOException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 175
    if-eqz p1, :cond_0

    .line 177
    :try_start_6
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_1

    .line 178
    :catch_3
    move-exception v0

    .line 179
    const-string/jumbo v8, "ZipInputStreamZipEntrySource"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "IOException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 82
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "name":Ljava/lang/String;
    .restart local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_4
    :try_start_7
    const-string/jumbo v8, "xl/drawings/drawing"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 84
    const-string/jumbo v8, "/drawing"

    const/4 v9, 0x2

    .line 83
    invoke-static {v4, v8, v9}, Lcom/samsung/thumbnail/office/util/StringUtils;->ordinalIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    .line 85
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 86
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0x8

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 87
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 88
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 174
    .end local v1    # "end":I
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    .end local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v8

    .line 175
    if-eqz p1, :cond_5

    .line 177
    :try_start_8
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 182
    :cond_5
    :goto_2
    throw v8

    .line 91
    .restart local v4    # "name":Ljava/lang/String;
    .restart local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_6
    :try_start_9
    const-string/jumbo v8, "xl/drawings/_rels/drawing"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 93
    const-string/jumbo v8, "/drawing"

    const/4 v9, 0x2

    .line 92
    invoke-static {v4, v8, v9}, Lcom/samsung/thumbnail/office/util/StringUtils;->ordinalIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    .line 94
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 95
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0x8

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 96
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 97
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 100
    .end local v1    # "end":I
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    :cond_7
    const-string/jumbo v8, "xl/drawings/vmlDrawing"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 101
    const-string/jumbo v8, "/vmlDrawing"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 102
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 103
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0xb

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 104
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 105
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 109
    .end local v1    # "end":I
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    :cond_8
    const-string/jumbo v8, "xl/drawings/_rels/vmlDrawing"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 110
    const-string/jumbo v8, "/vmlDrawing"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 111
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 112
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0xb

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 113
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 114
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 117
    .end local v1    # "end":I
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    :cond_9
    const-string/jumbo v8, "xl/tables"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 118
    const-string/jumbo v8, "xl/embeddings"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 119
    const-string/jumbo v8, "xl/printerSettings"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 120
    const-string/jumbo v8, "xl/activeX"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 121
    :cond_a
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 124
    :cond_b
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, ".bin"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 125
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "word/media/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 126
    :cond_c
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 130
    :cond_d
    if-eqz v4, :cond_11

    const-string/jumbo v8, "ppt/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 132
    const-string/jumbo v8, "ppt/slides/slide"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 133
    const-string/jumbo v8, "/slides/slide"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 134
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 135
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0xd

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 136
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 137
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 142
    .end local v1    # "end":I
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    :cond_e
    const-string/jumbo v8, "ppt/slides/_rels/slide"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 143
    const-string/jumbo v8, "/_rels/slide"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 144
    .restart local v5    # "start":I
    const-string/jumbo v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 145
    .restart local v1    # "end":I
    add-int/lit8 v8, v5, 0xc

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 146
    .restart local v6    # "target":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v10, :cond_11

    .line 147
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 152
    .end local v1    # "end":I
    .end local v5    # "start":I
    .end local v6    # "target":Ljava/lang/String;
    :cond_f
    const-string/jumbo v8, "ppt/notesSlides"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 153
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 157
    :cond_10
    const-string/jumbo v8, "ppt/embeddings"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 158
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 164
    :cond_11
    new-instance v2, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

    invoke-direct {v2, v7, p1}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;-><init>(Ljava/util/zip/ZipEntry;Ljava/util/zip/ZipInputStream;)V

    .line 165
    .local v2, "entry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    invoke-virtual {p1}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 167
    iget-object v8, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->zipEntries:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 178
    .end local v2    # "entry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    .end local v4    # "name":Ljava/lang/String;
    .end local v7    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_4
    move-exception v0

    .line 179
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "ZipInputStreamZipEntrySource"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "IOException: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 178
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 179
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "ZipInputStreamZipEntrySource"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "IOException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->zipEntries:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static checkExcel(Ljava/util/zip/ZipInputStream;)Z
    .locals 15
    .param p0, "inp"    # Ljava/util/zip/ZipInputStream;

    .prologue
    const/4 v14, 0x1

    .line 219
    const/4 v0, 0x1

    .line 222
    .local v0, "chck":Z
    const/4 v3, 0x1

    .line 223
    .local v3, "going":Z
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 310
    :goto_1
    if-eqz p0, :cond_1

    .line 312
    :try_start_0
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 319
    :cond_1
    :goto_2
    return v0

    .line 224
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v9

    .line 225
    .local v9, "zipEntry":Ljava/util/zip/ZipEntry;
    if-nez v9, :cond_3

    .line 226
    const/4 v3, 0x0

    .line 227
    goto :goto_0

    .line 229
    :cond_3
    invoke-virtual {v9}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    .line 230
    .local v6, "name":Ljava/lang/String;
    if-eqz v6, :cond_c

    const-string/jumbo v10, "xl/"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 231
    const-string/jumbo v10, "xl/worksheets/sheet"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 232
    const-string/jumbo v10, "/sheet"

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 233
    .local v7, "start":I
    const-string/jumbo v10, "."

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 234
    .local v2, "end":I
    add-int/lit8 v10, v7, 0x6

    invoke-virtual {v6, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 235
    .local v8, "target":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v10, v14, :cond_d

    .line 236
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 307
    .end local v2    # "end":I
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "start":I
    .end local v8    # "target":Ljava/lang/String;
    .end local v9    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v1

    .line 308
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v10, "ZipInputStreamZipEntrySource"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "IOException: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 310
    if-eqz p0, :cond_1

    .line 312
    :try_start_3
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 313
    :catch_1
    move-exception v1

    .line 314
    const-string/jumbo v10, "ZipInputStreamZipEntrySource"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "IOException: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 239
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v9    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_4
    :try_start_4
    const-string/jumbo v10, "xl/worksheets/_rels/sheet"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 240
    const-string/jumbo v10, "/sheet"

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 241
    .restart local v7    # "start":I
    const-string/jumbo v10, "."

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 242
    .restart local v2    # "end":I
    add-int/lit8 v10, v7, 0x6

    invoke-virtual {v6, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 243
    .restart local v8    # "target":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v10, v14, :cond_d

    .line 244
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 309
    .end local v2    # "end":I
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "start":I
    .end local v8    # "target":Ljava/lang/String;
    .end local v9    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v10

    .line 310
    if-eqz p0, :cond_5

    .line 312
    :try_start_5
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 317
    :cond_5
    :goto_3
    throw v10

    .line 247
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v9    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_6
    :try_start_6
    const-string/jumbo v10, "xl/drawings/drawing"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 249
    const-string/jumbo v10, "/drawing"

    const/4 v11, 0x2

    .line 248
    invoke-static {v6, v10, v11}, Lcom/samsung/thumbnail/office/util/StringUtils;->ordinalIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    .line 250
    .restart local v7    # "start":I
    const-string/jumbo v10, "."

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 251
    .restart local v2    # "end":I
    add-int/lit8 v10, v7, 0x8

    invoke-virtual {v6, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 252
    .restart local v8    # "target":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v10, v14, :cond_d

    .line 253
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 256
    .end local v2    # "end":I
    .end local v7    # "start":I
    .end local v8    # "target":Ljava/lang/String;
    :cond_7
    const-string/jumbo v10, "xl/drawings/_rels/drawing"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 258
    const-string/jumbo v10, "/drawing"

    const/4 v11, 0x2

    .line 257
    invoke-static {v6, v10, v11}, Lcom/samsung/thumbnail/office/util/StringUtils;->ordinalIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    .line 259
    .restart local v7    # "start":I
    const-string/jumbo v10, "."

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 260
    .restart local v2    # "end":I
    add-int/lit8 v10, v7, 0x8

    invoke-virtual {v6, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 261
    .restart local v8    # "target":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v10, v14, :cond_d

    .line 262
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 265
    .end local v2    # "end":I
    .end local v7    # "start":I
    .end local v8    # "target":Ljava/lang/String;
    :cond_8
    const-string/jumbo v10, "xl/drawings/vmlDrawing"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 266
    const-string/jumbo v10, "/vmlDrawing"

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 267
    .restart local v7    # "start":I
    const-string/jumbo v10, "."

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 268
    .restart local v2    # "end":I
    add-int/lit8 v10, v7, 0xb

    invoke-virtual {v6, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 269
    .restart local v8    # "target":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v10, v14, :cond_d

    .line 270
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 274
    .end local v2    # "end":I
    .end local v7    # "start":I
    .end local v8    # "target":Ljava/lang/String;
    :cond_9
    const-string/jumbo v10, "xl/drawings/_rels/vmlDrawing"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 275
    const-string/jumbo v10, "/vmlDrawing"

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 276
    .restart local v7    # "start":I
    const-string/jumbo v10, "."

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 277
    .restart local v2    # "end":I
    add-int/lit8 v10, v7, 0xb

    invoke-virtual {v6, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 278
    .restart local v8    # "target":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-le v10, v14, :cond_d

    .line 279
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 282
    .end local v2    # "end":I
    .end local v7    # "start":I
    .end local v8    # "target":Ljava/lang/String;
    :cond_a
    const-string/jumbo v10, "xl/tables"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 283
    const-string/jumbo v10, "xl/embeddings"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 284
    const-string/jumbo v10, "xl/printerSettings"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 285
    const-string/jumbo v10, "xl/activeX"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 286
    :cond_b
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 289
    :cond_c
    invoke-virtual {v9}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, ".bin"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 290
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    goto/16 :goto_0

    .line 294
    :cond_d
    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {v9}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v4

    .line 296
    .local v4, "entrySize":J
    const-wide/32 v10, 0x100000

    div-long v10, v4, v10

    const-wide/16 v12, 0x2d

    cmp-long v10, v10, v12

    if-ltz v10, :cond_e

    .line 297
    const/4 v0, 0x0

    .line 298
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 299
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->close()V

    .line 300
    const/4 v3, 0x0

    .line 301
    goto/16 :goto_1

    .line 303
    :cond_e
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 313
    .end local v4    # "entrySize":J
    .end local v6    # "name":Ljava/lang/String;
    .end local v9    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_2
    move-exception v1

    .line 314
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v11, "ZipInputStreamZipEntrySource"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "IOException: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 313
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 314
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "ZipInputStreamZipEntrySource"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "IOException: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->zipEntries:Ljava/util/ArrayList;

    .line 338
    return-void
.end method

.method public getEntries()Ljava/util/Enumeration;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<+",
            "Ljava/util/zip/ZipEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    new-instance v0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$EntryEnumerator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$EntryEnumerator;-><init>(Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$EntryEnumerator;)V

    return-object v0
.end method

.method public getFakeZipEntry()Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->imgEntry:Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

    return-object v0
.end method

.method public getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    .locals 2
    .param p1, "zipEntry"    # Ljava/util/zip/ZipEntry;

    .prologue
    .line 331
    move-object v0, p1

    check-cast v0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

    .line 332
    .local v0, "entry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    return-object v1
.end method
