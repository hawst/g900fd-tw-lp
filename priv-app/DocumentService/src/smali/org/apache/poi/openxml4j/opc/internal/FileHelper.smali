.class public final Lorg/apache/poi/openxml4j/opc/internal/FileHelper;
.super Ljava/lang/Object;
.source "FileHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .param p0, "in"    # Ljava/io/File;
    .param p1, "out"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 68
    .local v1, "sourceChannel":Ljava/nio/channels/FileChannel;
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 72
    .local v6, "destinationChannel":Ljava/nio/channels/FileChannel;
    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    if-eqz v1, :cond_0

    .line 77
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 78
    :cond_0
    if-eqz v6, :cond_1

    .line 79
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 81
    :cond_1
    return-void

    .line 75
    :catchall_0
    move-exception v0

    .line 76
    if-eqz v1, :cond_2

    .line 77
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 78
    :cond_2
    if-eqz v6, :cond_3

    .line 79
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 80
    :cond_3
    throw v0
.end method

.method public static getDirectory(Ljava/io/File;)Ljava/io/File;
    .locals 6
    .param p0, "f"    # Ljava/io/File;

    .prologue
    .line 42
    if-eqz p0, :cond_1

    .line 43
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "path":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 45
    .local v1, "len":I
    move v2, v1

    .line 46
    .local v2, "num2":I
    :cond_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_2

    .line 53
    .end local v1    # "len":I
    .end local v2    # "num2":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    :goto_0
    return-object v4

    .line 47
    .restart local v1    # "len":I
    .restart local v2    # "num2":I
    .restart local v3    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 48
    .local v0, "ch1":C
    sget-char v4, Ljava/io/File;->separatorChar:C

    if-ne v0, v4, :cond_0

    .line 49
    new-instance v4, Ljava/io/File;

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getFilename(Ljava/io/File;)Ljava/lang/String;
    .locals 5
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 87
    if-eqz p0, :cond_1

    .line 88
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 89
    .local v3, "path":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 90
    .local v1, "len":I
    move v2, v1

    .line 91
    .local v2, "num2":I
    :cond_0
    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_2

    .line 97
    .end local v1    # "len":I
    .end local v2    # "num2":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    const-string/jumbo v4, ""

    :goto_0
    return-object v4

    .line 92
    .restart local v1    # "len":I
    .restart local v2    # "num2":I
    .restart local v3    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 93
    .local v0, "ch1":C
    sget-char v4, Ljava/io/File;->separatorChar:C

    if-ne v0, v4, :cond_0

    .line 94
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
