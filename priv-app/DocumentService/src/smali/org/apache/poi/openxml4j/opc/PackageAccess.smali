.class public final enum Lorg/apache/poi/openxml4j/opc/PackageAccess;
.super Ljava/lang/Enum;
.source "PackageAccess.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/openxml4j/opc/PackageAccess;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/PackageAccess;

.field public static final enum READ:Lorg/apache/poi/openxml4j/opc/PackageAccess;

.field public static final enum READ_WRITE:Lorg/apache/poi/openxml4j/opc/PackageAccess;

.field public static final enum WRITE:Lorg/apache/poi/openxml4j/opc/PackageAccess;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;

    const-string/jumbo v1, "READ"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/openxml4j/opc/PackageAccess;-><init>(Ljava/lang/String;I)V

    .line 28
    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;->READ:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    .line 29
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;

    const-string/jumbo v1, "WRITE"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/openxml4j/opc/PackageAccess;-><init>(Ljava/lang/String;I)V

    .line 30
    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;->WRITE:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    .line 31
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;

    const-string/jumbo v1, "READ_WRITE"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/openxml4j/opc/PackageAccess;-><init>(Ljava/lang/String;I)V

    .line 32
    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;->READ_WRITE:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/poi/openxml4j/opc/PackageAccess;

    sget-object v1, Lorg/apache/poi/openxml4j/opc/PackageAccess;->READ:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/openxml4j/opc/PackageAccess;->WRITE:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/openxml4j/opc/PackageAccess;->READ_WRITE:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/PackageAccess;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageAccess;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/openxml4j/opc/PackageAccess;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/openxml4j/opc/PackageAccess;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/PackageAccess;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/openxml4j/opc/PackageAccess;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
