.class public final Lorg/apache/poi/openxml4j/opc/PackagePartName;
.super Ljava/lang/Object;
.source "PackagePartName.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/poi/openxml4j/opc/PackagePartName;",
        ">;"
    }
.end annotation


# static fields
.field private static RFC3986_PCHAR_AUTHORIZED_SUP:[Ljava/lang/String;

.field private static RFC3986_PCHAR_SUB_DELIMS:[Ljava/lang/String;

.field private static RFC3986_PCHAR_UNRESERVED_SUP:[Ljava/lang/String;


# instance fields
.field private isRelationship:Z

.field private partNameURI:Ljava/net/URI;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "!"

    aput-object v1, v0, v3

    const-string/jumbo v1, "$"

    aput-object v1, v0, v4

    const-string/jumbo v1, "&"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\'"

    aput-object v1, v0, v6

    .line 48
    const-string/jumbo v1, "("

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, ")"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, ";"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "="

    aput-object v2, v0, v1

    .line 47
    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_SUB_DELIMS:[Ljava/lang/String;

    .line 53
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "-"

    aput-object v1, v0, v3

    const-string/jumbo v1, "."

    aput-object v1, v0, v4

    const-string/jumbo v1, "_"

    aput-object v1, v0, v5

    const-string/jumbo v1, "~"

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_UNRESERVED_SUP:[Ljava/lang/String;

    .line 58
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, ":"

    aput-object v1, v0, v3

    const-string/jumbo v1, "@"

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_AUTHORIZED_SUP:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "partName"    # Ljava/lang/String;
    .param p2, "checkConformance"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    .local v1, "partURI":Ljava/net/URI;
    if-eqz p2, :cond_1

    .line 119
    invoke-static {v1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfInvalidPartUri(Ljava/net/URI;)V

    .line 126
    :cond_0
    iput-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    .line 127
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-direct {p0, v2}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationshipPartURI(Ljava/net/URI;)Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationship:Z

    .line 128
    return-void

    .line 113
    .end local v1    # "partURI":Ljava/net/URI;
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/net/URISyntaxException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 115
    const-string/jumbo v3, "partName argmument is not a valid OPC part name !"

    .line 114
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 121
    .end local v0    # "e":Ljava/net/URISyntaxException;
    .restart local v1    # "partURI":Ljava/net/URI;
    :cond_1
    sget-object v2, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->PACKAGE_ROOT_URI:Ljava/net/URI;

    invoke-virtual {v2, v1}, Ljava/net/URI;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    new-instance v2, Lorg/apache/poi/openxml4j/exceptions/OpenXML4JRuntimeException;

    .line 123
    const-string/jumbo v3, "OCP conformance must be check for ALL part name except special cases : [\'/\']"

    .line 122
    invoke-direct {v2, v3}, Lorg/apache/poi/openxml4j/exceptions/OpenXML4JRuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method constructor <init>(Ljava/net/URI;Z)V
    .locals 2
    .param p1, "uri"    # Ljava/net/URI;
    .param p2, "checkConformance"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    if-eqz p2, :cond_1

    .line 83
    invoke-static {p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfInvalidPartUri(Ljava/net/URI;)V

    .line 90
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    .line 91
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationshipPartURI(Ljava/net/URI;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationship:Z

    .line 92
    return-void

    .line 85
    :cond_1
    sget-object v0, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->PACKAGE_ROOT_URI:Ljava/net/URI;

    invoke-virtual {v0, p1}, Ljava/net/URI;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/OpenXML4JRuntimeException;

    .line 87
    const-string/jumbo v1, "OCP conformance must be check for ALL part name except special cases : [\'/\']"

    .line 86
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/OpenXML4JRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static checkPCharCompliance(Ljava/lang/String;)V
    .locals 12
    .param p0, "segment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x5a

    const/16 v10, 0x41

    const/16 v9, 0x39

    const/16 v8, 0x30

    const/4 v7, 0x0

    .line 290
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v3, v5, :cond_0

    .line 373
    return-void

    .line 291
    :cond_0
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 292
    .local v0, "c":C
    const/4 v2, 0x1

    .line 297
    .local v2, "errorFlag":Z
    if-lt v0, v10, :cond_1

    if-le v0, v11, :cond_3

    :cond_1
    const/16 v5, 0x61

    if-lt v0, v5, :cond_2

    const/16 v5, 0x7a

    if-le v0, v5, :cond_3

    .line 298
    :cond_2
    if-lt v0, v8, :cond_5

    if-gt v0, v9, :cond_5

    .line 299
    :cond_3
    const/4 v2, 0x0

    .line 326
    :cond_4
    if-eqz v2, :cond_16

    const/16 v5, 0x25

    if-ne v0, v5, :cond_16

    .line 329
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v3

    const/4 v6, 0x2

    if-ge v5, v6, :cond_c

    .line 330
    new-instance v5, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "The segment "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 331
    const-string/jumbo v7, " contain invalid encoded character !"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 330
    invoke-direct {v5, v6}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 302
    :cond_5
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_UNRESERVED_SUP:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_8

    .line 310
    :goto_2
    const/4 v4, 0x0

    :goto_3
    if-eqz v2, :cond_6

    .line 311
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_AUTHORIZED_SUP:[Ljava/lang/String;

    array-length v5, v5

    .line 310
    if-lt v4, v5, :cond_a

    .line 318
    :cond_6
    const/4 v4, 0x0

    :goto_4
    if-eqz v2, :cond_4

    .line 319
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_SUB_DELIMS:[Ljava/lang/String;

    array-length v5, v5

    .line 318
    if-ge v4, v5, :cond_4

    .line 320
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_SUB_DELIMS:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v0, v5, :cond_7

    .line 321
    const/4 v2, 0x0

    .line 319
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 303
    :cond_8
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_UNRESERVED_SUP:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v0, v5, :cond_9

    .line 304
    const/4 v2, 0x0

    .line 305
    goto :goto_2

    .line 302
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 312
    :cond_a
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_AUTHORIZED_SUP:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v0, v5, :cond_b

    .line 313
    const/4 v2, 0x0

    .line 311
    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 336
    .end local v4    # "j":I
    :cond_c
    const/4 v2, 0x0

    .line 340
    add-int/lit8 v5, v3, 0x1

    add-int/lit8 v6, v3, 0x3

    .line 339
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 340
    const/16 v6, 0x10

    .line 339
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-char v1, v5

    .line 341
    .local v1, "decodedChar":C
    add-int/lit8 v3, v3, 0x2

    .line 344
    const/16 v5, 0x2f

    if-eq v1, v5, :cond_d

    const/16 v5, 0x5c

    if-ne v1, v5, :cond_e

    .line 345
    :cond_d
    new-instance v5, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 346
    const-string/jumbo v6, "A segment shall not contain percent-encoded forward slash (\'/\'), or backward slash (\'\') characters. [M1.7]"

    .line 345
    invoke-direct {v5, v6}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 351
    :cond_e
    if-lt v1, v10, :cond_f

    if-le v1, v11, :cond_11

    .line 352
    :cond_f
    const/16 v5, 0x61

    if-lt v1, v5, :cond_10

    const/16 v5, 0x7a

    if-le v1, v5, :cond_11

    .line 353
    :cond_10
    if-lt v1, v8, :cond_12

    if-gt v1, v9, :cond_12

    .line 354
    :cond_11
    const/4 v2, 0x1

    .line 357
    :cond_12
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_5
    if-nez v2, :cond_13

    .line 358
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_UNRESERVED_SUP:[Ljava/lang/String;

    array-length v5, v5

    .line 357
    if-lt v4, v5, :cond_14

    .line 364
    :cond_13
    :goto_6
    if-eqz v2, :cond_16

    .line 365
    new-instance v5, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 366
    const-string/jumbo v6, "A segment shall not contain percent-encoded unreserved characters. [M1.8]"

    .line 365
    invoke-direct {v5, v6}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 359
    :cond_14
    sget-object v5, Lorg/apache/poi/openxml4j/opc/PackagePartName;->RFC3986_PCHAR_UNRESERVED_SUP:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v0, v5, :cond_15

    .line 360
    const/4 v2, 0x1

    .line 361
    goto :goto_6

    .line 358
    :cond_15
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 369
    .end local v1    # "decodedChar":C
    .end local v4    # "j":I
    :cond_16
    if-eqz v2, :cond_17

    .line 370
    new-instance v5, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 371
    const-string/jumbo v6, "A segment shall not hold any characters other than pchar characters. [M1.6]"

    .line 370
    invoke-direct {v5, v6}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 290
    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method private isRelationshipPartURI(Ljava/net/URI;)Z
    .locals 3
    .param p1, "partUri"    # Ljava/net/URI;

    .prologue
    .line 139
    if-nez p1, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "partUri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "^.*/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->RELATIONSHIP_PART_SEGMENT_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/.*\\"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 144
    sget-object v2, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->RELATIONSHIP_PART_EXTENSION_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 145
    const-string/jumbo v2, "$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 143
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 142
    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static throwExceptionIfAbsoluteUri(Ljava/net/URI;)V
    .locals 3
    .param p0, "partUri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 425
    invoke-virtual {p0}, Ljava/net/URI;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Absolute URI forbidden: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 426
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_0
    return-void
.end method

.method private static throwExceptionIfEmptyURI(Ljava/net/URI;)V
    .locals 4
    .param p0, "partURI"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 198
    if-nez p0, :cond_0

    .line 199
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "partURI"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :cond_0
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "uriPath":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sget-char v2, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->FORWARD_SLASH_CHAR:C

    if-ne v1, v2, :cond_2

    .line 204
    :cond_1
    new-instance v1, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 205
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "A part name shall not be empty [M1.1]: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 205
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-direct {v1, v2}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 207
    :cond_2
    return-void
.end method

.method private static throwExceptionIfInvalidPartUri(Ljava/net/URI;)V
    .locals 2
    .param p0, "partUri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 169
    if-nez p0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "partUri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    invoke-static {p0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfEmptyURI(Ljava/net/URI;)V

    .line 175
    invoke-static {p0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfAbsoluteUri(Ljava/net/URI;)V

    .line 178
    invoke-static {p0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfPartNameNotStartsWithForwardSlashChar(Ljava/net/URI;)V

    .line 181
    invoke-static {p0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfPartNameEndsWithForwardSlashChar(Ljava/net/URI;)V

    .line 185
    invoke-static {p0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->throwExceptionIfPartNameHaveInvalidSegments(Ljava/net/URI;)V

    .line 186
    return-void
.end method

.method private static throwExceptionIfPartNameEndsWithForwardSlashChar(Ljava/net/URI;)V
    .locals 4
    .param p0, "partUri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 408
    .local v0, "uriPath":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 409
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sget-char v2, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->FORWARD_SLASH_CHAR:C

    if-ne v1, v2, :cond_0

    .line 410
    new-instance v1, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 411
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "A part name shall not have a forward slash as the last character [M1.5]: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 412
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 411
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 410
    invoke-direct {v1, v2}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 413
    :cond_0
    return-void
.end method

.method private static throwExceptionIfPartNameHaveInvalidSegments(Ljava/net/URI;)V
    .locals 6
    .param p0, "partUri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 236
    if-nez p0, :cond_0

    .line 237
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "partUri"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 241
    :cond_0
    invoke-virtual {p0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "segments":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 243
    :cond_1
    new-instance v3, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 244
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "A part name shall not have empty segments [M1.3]: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 244
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 243
    invoke-direct {v3, v4}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 247
    :cond_2
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_3

    .line 272
    return-void

    .line 248
    :cond_3
    aget-object v1, v2, v0

    .line 249
    .local v1, "seg":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    .line 250
    :cond_4
    new-instance v3, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 251
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "A part name shall not have empty segments [M1.3]: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 251
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 250
    invoke-direct {v3, v4}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 255
    :cond_5
    const-string/jumbo v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 256
    new-instance v3, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 257
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "A segment shall not end with a dot (\'.\') character [M1.9]: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 258
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 257
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 256
    invoke-direct {v3, v4}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 261
    :cond_6
    const-string/jumbo v3, "\\\\."

    const-string/jumbo v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    .line 264
    new-instance v3, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 265
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "A segment shall include at least one non-dot character. [M1.10]: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 265
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 264
    invoke-direct {v3, v4}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 270
    :cond_7
    invoke-static {v1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->checkPCharCompliance(Ljava/lang/String;)V

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static throwExceptionIfPartNameNotStartsWithForwardSlashChar(Ljava/net/URI;)V
    .locals 4
    .param p0, "partUri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "uriPath":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 389
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sget-char v2, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->FORWARD_SLASH_CHAR:C

    if-eq v1, v2, :cond_0

    .line 390
    new-instance v1, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 391
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "A part name shall start with a forward slash (\'/\') character [M1.4]: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 391
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 390
    invoke-direct {v1, v2}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 393
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/openxml4j/opc/PackagePartName;

    invoke-virtual {p0, p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->compareTo(Lorg/apache/poi/openxml4j/opc/PackagePartName;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/openxml4j/opc/PackagePartName;)I
    .locals 2
    .param p1, "otherPartName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .prologue
    .line 439
    if-nez p1, :cond_0

    .line 440
    const/4 v0, -0x1

    .line 441
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 442
    iget-object v1, p1, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 441
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "otherPartName"    # Ljava/lang/Object;

    .prologue
    .line 478
    if-eqz p1, :cond_0

    .line 479
    instance-of v0, p1, Lorg/apache/poi/openxml4j/opc/PackagePartName;

    if-nez v0, :cond_1

    .line 480
    :cond_0
    const/4 v0, 0x0

    .line 481
    .end local p1    # "otherPartName":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "otherPartName":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 482
    check-cast p1, Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .end local p1    # "otherPartName":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v1

    .line 483
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 481
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 3

    .prologue
    .line 452
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v2}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "fragment":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 454
    const-string/jumbo v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 455
    .local v1, "i":I
    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 456
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 458
    .end local v1    # "i":I
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, ""

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getURI()Ljava/net/URI;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->partNameURI:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isRelationshipPartURI()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationship:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 493
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
