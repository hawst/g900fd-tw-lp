.class public Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
.super Ljava/util/zip/ZipEntry;
.source "ZipInputStreamZipEntrySource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FakeZipEntry"
.end annotation


# static fields
.field public static size:J


# instance fields
.field private data:[B

.field private entryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 367
    const-wide/16 v0, 0x0

    sput-wide v0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->size:J

    return-void
.end method

.method public constructor <init>(Ljava/util/zip/ZipEntry;Ljava/util/zip/ZipInputStream;)V
    .locals 10
    .param p1, "entry"    # Ljava/util/zip/ZipEntry;
    .param p2, "inp"    # Ljava/util/zip/ZipInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 371
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 365
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->entryName:Ljava/lang/String;

    .line 375
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v2

    .line 376
    .local v2, "entrySize":J
    sput-wide v2, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->size:J

    .line 378
    const-wide/32 v6, 0x100000

    div-long v6, v2, v6

    const-wide/16 v8, 0x10

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 379
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "xl/media/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 380
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "ppt/media/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 381
    :cond_0
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->entryName:Ljava/lang/String;

    .line 401
    :goto_0
    return-void

    .line 385
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-eqz v5, :cond_3

    .line 386
    const-wide/32 v6, 0x7fffffff

    cmp-long v5, v2, v6

    if-ltz v5, :cond_2

    .line 387
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "ZIP entry size is too large"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 390
    :cond_2
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    long-to-int v5, v2

    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 395
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :goto_1
    const/16 v5, 0x1000

    new-array v1, v5, [B

    .line 396
    .local v1, "buffer":[B
    const/4 v4, 0x0

    .line 397
    .local v4, "read":I
    :goto_2
    invoke-virtual {p2, v1}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    .line 400
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iput-object v5, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->data:[B

    goto :goto_0

    .line 392
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "buffer":[B
    .end local v4    # "read":I
    :cond_3
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_1

    .line 398
    .restart local v1    # "buffer":[B
    .restart local v4    # "read":I
    :cond_4
    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2
.end method


# virtual methods
.method public getEntrySize()J
    .locals 2

    .prologue
    .line 412
    sget-wide v0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->size:J

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->entryName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;->data:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 407
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
