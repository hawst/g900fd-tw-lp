.class public final enum Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;
.super Ljava/lang/Enum;
.source "CertificateEmbeddingOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

.field public static final enum IN_CERTIFICATE_PART:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

.field public static final enum IN_SIGNATURE_PART:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

.field public static final enum NOT_EMBEDDED:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    const-string/jumbo v1, "IN_CERTIFICATE_PART"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;-><init>(Ljava/lang/String;I)V

    .line 27
    sput-object v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->IN_CERTIFICATE_PART:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    .line 28
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    const-string/jumbo v1, "IN_SIGNATURE_PART"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;-><init>(Ljava/lang/String;I)V

    .line 29
    sput-object v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->IN_SIGNATURE_PART:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    .line 30
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    const-string/jumbo v1, "NOT_EMBEDDED"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;-><init>(Ljava/lang/String;I)V

    .line 31
    sput-object v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->NOT_EMBEDDED:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->IN_CERTIFICATE_PART:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->IN_SIGNATURE_PART:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->NOT_EMBEDDED:Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/openxml4j/opc/CertificateEmbeddingOption;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
