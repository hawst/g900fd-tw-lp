.class public final Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;
.super Lorg/apache/poi/openxml4j/opc/PackagePart;
.source "PackagePropertiesPart.java"

# interfaces
.implements Lorg/apache/poi/openxml4j/opc/PackageProperties;


# static fields
.field public static final NAMESPACE_CP_URI:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties"

.field public static final NAMESPACE_DCTERMS_URI:Ljava/lang/String; = "http://purl.org/dc/terms/"

.field public static final NAMESPACE_DC_URI:Ljava/lang/String; = "http://purl.org/dc/elements/1.1/"

.field public static final NAMESPACE_XSI_URI:Ljava/lang/String; = "http://www.w3.org/2001/XMLSchema-instance"


# instance fields
.field protected category:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected contentStatus:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected contentType:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected created:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field protected creator:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected description:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected identifier:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected keywords:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected language:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected lastModifiedBy:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected lastPrinted:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field protected modified:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field protected revision:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected subject:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected title:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected version:Lorg/apache/poi/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;)V
    .locals 1
    .param p1, "pack"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .param p2, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 69
    const-string/jumbo v0, "application/vnd.openxmlformats-package.core-properties+xml"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 80
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->category:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 88
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->contentStatus:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 98
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->contentType:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 103
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->created:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 108
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->creator:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 117
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->description:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 122
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->identifier:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 129
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->keywords:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 137
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->language:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 146
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastModifiedBy:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 151
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastPrinted:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 156
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->modified:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 164
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->revision:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 169
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->subject:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 174
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->title:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 179
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->version:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 70
    return-void
.end method

.method private getDateValue(Lorg/apache/poi/openxml4j/util/Nullable;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 593
    .local p1, "d":Lorg/apache/poi/openxml4j/util/Nullable;, "Lorg/apache/poi/openxml4j/util/Nullable<Ljava/util/Date;>;"
    if-nez p1, :cond_0

    .line 594
    const-string/jumbo v2, ""

    .line 604
    :goto_0
    return-object v2

    .line 596
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 597
    .local v0, "date":Ljava/util/Date;
    if-nez v0, :cond_1

    .line 598
    const-string/jumbo v2, ""

    goto :goto_0

    .line 601
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 602
    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    .line 601
    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 603
    .local v1, "df":Ljava/text/SimpleDateFormat;
    const-string/jumbo v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 604
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private setDateValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 571
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 572
    :cond_0
    new-instance v2, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v2}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    .line 581
    :goto_0
    return-object v2

    .line 574
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 575
    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    .line 574
    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 576
    .local v1, "df":Ljava/text/SimpleDateFormat;
    const-string/jumbo v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 577
    new-instance v2, Ljava/text/ParsePosition;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/text/ParsePosition;-><init>(I)V

    invoke-virtual {v1, p1, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v0

    .line 578
    .local v0, "d":Ljava/util/Date;
    if-nez v0, :cond_2

    .line 579
    new-instance v2, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    const-string/jumbo v3, "Date not well formated"

    invoke-direct {v2, v3}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 581
    :cond_2
    new-instance v2, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v2, v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 559
    :cond_0
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>()V

    .line 561
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {v0, p1}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 636
    return-void
.end method

.method public flush()V
    .locals 0

    .prologue
    .line 641
    return-void
.end method

.method public getCategoryProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->category:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getContentStatusProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->contentStatus:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getContentTypeProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->contentType:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getCreatedProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->created:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getCreatedPropertyString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->created:Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->getDateValue(Lorg/apache/poi/openxml4j/util/Nullable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCreatorProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->creator:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getDescriptionProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->description:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method protected getEntryNameImpl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 614
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIdentifierProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->identifier:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method protected getInputStreamImpl()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 609
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    const-string/jumbo v1, "Operation not authorized. This part may only be manipulated using the getters and setters on PackagePropertiesPart"

    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getKeywordsProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->keywords:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getLanguageProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->language:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getLastModifiedByProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastModifiedBy:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getLastPrintedProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastPrinted:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getLastPrintedPropertyString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastPrinted:Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->getDateValue(Lorg/apache/poi/openxml4j/util/Nullable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->modified:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getModifiedPropertyString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->modified:Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/util/Nullable;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->modified:Lorg/apache/poi/openxml4j/util/Nullable;

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->getDateValue(Lorg/apache/poi/openxml4j/util/Nullable;)Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/poi/openxml4j/util/Nullable;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/util/Nullable;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->getDateValue(Lorg/apache/poi/openxml4j/util/Nullable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getOutputStreamImpl()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 619
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 620
    const-string/jumbo v1, "Can\'t use output stream to set properties !"

    .line 619
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getRevisionProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->revision:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getSubjectProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->subject:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getTitleProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->title:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public getVersionProperty()Lorg/apache/poi/openxml4j/util/Nullable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->version:Lorg/apache/poi/openxml4j/util/Nullable;

    return-object v0
.end method

.method public load(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "ios"    # Ljava/io/InputStream;

    .prologue
    .line 630
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    const-string/jumbo v1, "Operation not authorized. This part may only be manipulated using the getters and setters on PackagePropertiesPart"

    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public save(Ljava/io/OutputStream;)Z
    .locals 2
    .param p1, "zos"    # Ljava/io/OutputStream;

    .prologue
    .line 625
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    const-string/jumbo v1, "Operation not authorized. This part may only be manipulated using the getters and setters on PackagePropertiesPart"

    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setCategoryProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 365
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->category:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 366
    return-void
.end method

.method public setContentStatusProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentStatus"    # Ljava/lang/String;

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->contentStatus:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 375
    return-void
.end method

.method public setContentTypeProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 383
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->contentType:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 384
    return-void
.end method

.method public setCreatedProperty(Ljava/lang/String;)V
    .locals 4
    .param p1, "created"    # Ljava/lang/String;

    .prologue
    .line 393
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setDateValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->created:Lorg/apache/poi/openxml4j/util/Nullable;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :goto_0
    return-void

    .line 394
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCreatedProperty(Lorg/apache/poi/openxml4j/util/Nullable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408
    .local p1, "created":Lorg/apache/poi/openxml4j/util/Nullable;, "Lorg/apache/poi/openxml4j/util/Nullable<Ljava/util/Date;>;"
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/util/Nullable;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->created:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 410
    :cond_0
    return-void
.end method

.method public setCreatorProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "creator"    # Ljava/lang/String;

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->creator:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 419
    return-void
.end method

.method public setDescriptionProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->description:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 428
    return-void
.end method

.method public setIdentifierProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 436
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->identifier:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 437
    return-void
.end method

.method public setKeywordsProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "keywords"    # Ljava/lang/String;

    .prologue
    .line 445
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->keywords:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 446
    return-void
.end method

.method public setLanguageProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->language:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 455
    return-void
.end method

.method public setLastModifiedByProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "lastModifiedBy"    # Ljava/lang/String;

    .prologue
    .line 463
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastModifiedBy:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 464
    return-void
.end method

.method public setLastPrintedProperty(Ljava/lang/String;)V
    .locals 4
    .param p1, "lastPrinted"    # Ljava/lang/String;

    .prologue
    .line 473
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setDateValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastPrinted:Lorg/apache/poi/openxml4j/util/Nullable;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    :goto_0
    return-void

    .line 474
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLastPrintedProperty(Lorg/apache/poi/openxml4j/util/Nullable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 488
    .local p1, "lastPrinted":Lorg/apache/poi/openxml4j/util/Nullable;, "Lorg/apache/poi/openxml4j/util/Nullable<Ljava/util/Date;>;"
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/util/Nullable;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->lastPrinted:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 490
    :cond_0
    return-void
.end method

.method public setModifiedProperty(Ljava/lang/String;)V
    .locals 4
    .param p1, "modified"    # Ljava/lang/String;

    .prologue
    .line 499
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setDateValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->modified:Lorg/apache/poi/openxml4j/util/Nullable;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 506
    :goto_0
    return-void

    .line 500
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setModifiedProperty(Lorg/apache/poi/openxml4j/util/Nullable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/openxml4j/util/Nullable",
            "<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 514
    .local p1, "modified":Lorg/apache/poi/openxml4j/util/Nullable;, "Lorg/apache/poi/openxml4j/util/Nullable<Ljava/util/Date;>;"
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/util/Nullable;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->modified:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 516
    :cond_0
    return-void
.end method

.method public setRevisionProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "revision"    # Ljava/lang/String;

    .prologue
    .line 524
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->revision:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 525
    return-void
.end method

.method public setSubjectProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 533
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->subject:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 534
    return-void
.end method

.method public setTitleProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 542
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->title:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 543
    return-void
.end method

.method public setVersionProperty(Ljava/lang/String;)V
    .locals 1
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 551
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->setStringValue(Ljava/lang/String;)Lorg/apache/poi/openxml4j/util/Nullable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/PackagePropertiesPart;->version:Lorg/apache/poi/openxml4j/util/Nullable;

    .line 552
    return-void
.end method
