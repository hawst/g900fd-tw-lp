.class public final Lorg/apache/poi/openxml4j/opc/signature/PackageDigitalSignature;
.super Lorg/apache/poi/openxml4j/opc/PackagePart;
.source "PackageDigitalSignature.java"


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 32
    new-instance v0, Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/opc/internal/ContentType;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2, v2, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/internal/ContentType;)V

    .line 33
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public flush()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method protected getEntryNameImpl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getInputStreamImpl()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getOutputStreamImpl()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public load(Ljava/io/InputStream;)Z
    .locals 1
    .param p1, "ios"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public save(Ljava/io/OutputStream;)Z
    .locals 1
    .param p1, "zos"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method
