.class public final enum Lorg/apache/poi/openxml4j/opc/CompressionOption;
.super Ljava/lang/Enum;
.source "CompressionOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/openxml4j/opc/CompressionOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/CompressionOption;

.field public static final enum FAST:Lorg/apache/poi/openxml4j/opc/CompressionOption;

.field public static final enum MAXIMUM:Lorg/apache/poi/openxml4j/opc/CompressionOption;

.field public static final enum NORMAL:Lorg/apache/poi/openxml4j/opc/CompressionOption;

.field public static final enum NOT_COMPRESSED:Lorg/apache/poi/openxml4j/opc/CompressionOption;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;

    const-string/jumbo v1, "FAST"

    .line 30
    invoke-direct {v0, v1, v3, v4}, Lorg/apache/poi/openxml4j/opc/CompressionOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->FAST:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    .line 31
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;

    const-string/jumbo v1, "MAXIMUM"

    .line 32
    const/16 v2, 0x9

    invoke-direct {v0, v1, v4, v2}, Lorg/apache/poi/openxml4j/opc/CompressionOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->MAXIMUM:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    .line 33
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;

    const-string/jumbo v1, "NORMAL"

    .line 34
    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/poi/openxml4j/opc/CompressionOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->NORMAL:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    .line 35
    new-instance v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;

    const-string/jumbo v1, "NOT_COMPRESSED"

    .line 36
    invoke-direct {v0, v1, v6, v3}, Lorg/apache/poi/openxml4j/opc/CompressionOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->NOT_COMPRESSED:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/poi/openxml4j/opc/CompressionOption;

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CompressionOption;->FAST:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CompressionOption;->MAXIMUM:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CompressionOption;->NORMAL:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/openxml4j/opc/CompressionOption;->NOT_COMPRESSED:Lorg/apache/poi/openxml4j/opc/CompressionOption;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/CompressionOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->value:I

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/CompressionOption;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/openxml4j/opc/CompressionOption;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/CompressionOption;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/openxml4j/opc/CompressionOption;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public value()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lorg/apache/poi/openxml4j/opc/CompressionOption;->value:I

    return v0
.end method
