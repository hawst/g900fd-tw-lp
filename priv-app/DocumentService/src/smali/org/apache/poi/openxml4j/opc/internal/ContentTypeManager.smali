.class public abstract Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;
.super Ljava/lang/Object;
.source "ContentTypeManager.java"


# static fields
.field public static final CONTENT_TYPES_PART_NAME:Ljava/lang/String; = "[Content_Types].xml"

.field public static final TYPES_NAMESPACE_URI:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/content-types"


# instance fields
.field protected container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

.field private defaultContentType:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private overrideContentType:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lorg/apache/poi/openxml4j/opc/PackagePartName;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/poi/openxml4j/opc/OPCPackage;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "pkg"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p2, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    .line 102
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    .line 103
    if-eqz p1, :cond_0

    .line 105
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->parseContentTypesFile(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v1, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 108
    const-string/jumbo v2, "Can\'t read content types part !"

    .line 107
    invoke-direct {v1, v2}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private addDefaultContentType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "extension"    # Ljava/lang/String;
    .param p2, "contentType"    # Ljava/lang/String;

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    return-void
.end method

.method private addOverrideContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V
    .locals 1
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p2, "contentType"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    .line 173
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    return-void
.end method

.method private parseContentTypesFile(Ljava/io/InputStream;)V
    .locals 17
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 379
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v11

    .line 380
    .local v11, "pullMaker":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v11}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v9

    .line 382
    .local v9, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-interface {v9, v0, v14}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 383
    const/4 v12, 0x0

    .line 385
    .local v12, "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 386
    .local v4, "eventType":I
    :goto_0
    const/4 v14, 0x1

    if-ne v4, v14, :cond_1

    .line 420
    if-nez v12, :cond_5

    .line 452
    .end local v4    # "eventType":I
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v11    # "pullMaker":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v12    # "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    :cond_0
    :goto_1
    return-void

    .line 387
    .restart local v4    # "eventType":I
    .restart local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11    # "pullMaker":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v12    # "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    :cond_1
    packed-switch v4, :pswitch_data_0

    .line 414
    :cond_2
    :goto_2
    :pswitch_0
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_0

    .line 391
    :pswitch_1
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "Types"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 392
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;

    .end local v12    # "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    invoke-direct {v12}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;-><init>()V

    .line 394
    .restart local v12    # "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    goto :goto_2

    :cond_3
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "Default"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 395
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;-><init>()V

    .line 396
    .local v2, "def":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;
    const/4 v14, 0x0

    const-string/jumbo v15, "ContentType"

    invoke-interface {v9, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;->setContentType(Ljava/lang/String;)V

    .line 397
    const/4 v14, 0x0

    const-string/jumbo v15, "Extension"

    invoke-interface {v9, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;->setExtension(Ljava/lang/String;)V

    .line 399
    if-eqz v12, :cond_2

    .line 400
    invoke-virtual {v12, v2}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->addDefault(Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 448
    .end local v2    # "def":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;
    .end local v4    # "eventType":I
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v11    # "pullMaker":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v12    # "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    :catch_0
    move-exception v5

    .line 449
    .local v5, "ex":Ljava/lang/Exception;
    const-string/jumbo v14, "DocumentService"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "Exception: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 402
    .end local v5    # "ex":Ljava/lang/Exception;
    .restart local v4    # "eventType":I
    .restart local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11    # "pullMaker":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v12    # "types":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;
    :cond_4
    :try_start_1
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "Override"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 403
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;-><init>()V

    .line 404
    .local v7, "override":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;
    const/4 v14, 0x0

    const-string/jumbo v15, "ContentType"

    invoke-interface {v9, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->setContentType(Ljava/lang/String;)V

    .line 405
    const/4 v14, 0x0

    const-string/jumbo v15, "PartName"

    invoke-interface {v9, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->setPartName(Ljava/lang/String;)V

    .line 407
    if-eqz v12, :cond_2

    .line 408
    invoke-virtual {v12, v7}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->addOverride(Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;)V

    goto/16 :goto_2

    .line 426
    .end local v7    # "override":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;
    :cond_5
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->getCTDefault()Ljava/util/List;

    move-result-object v3

    .line 427
    .local v3, "defList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;>;"
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTTypes;->getCTOverride()Ljava/util/List;

    move-result-object v8

    .line 429
    .local v8, "overrideList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;>;"
    if-eqz v3, :cond_6

    .line 430
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v14

    if-lt v6, v14, :cond_7

    .line 436
    .end local v6    # "i":I
    :cond_6
    if-eqz v8, :cond_0

    .line 437
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v14

    if-ge v6, v14, :cond_0

    .line 438
    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;

    .line 440
    .restart local v7    # "override":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;
    new-instance v13, Ljava/net/URI;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->getPartName()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 442
    .local v13, "uri":Ljava/net/URI;
    invoke-static {v13}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->createPartName(Ljava/net/URI;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v10

    .line 443
    .local v10, "partName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;->getContentType()Ljava/lang/String;

    move-result-object v1

    .line 444
    .local v1, "contentType":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v1}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->addOverrideContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 437
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 431
    .end local v1    # "contentType":Ljava/lang/String;
    .end local v7    # "override":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTOverride;
    .end local v10    # "partName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .end local v13    # "uri":Ljava/net/URI;
    :cond_7
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;

    .line 432
    .restart local v2    # "def":Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;->getExtension()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/contenttype/CTDefault;->getContentType()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->addDefaultContentType(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 430
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 387
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V
    .locals 4
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p2, "contentType"    # Ljava/lang/String;

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "defaultCTExists":Z
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getExtension()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "extension":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    .line 155
    invoke-virtual {v2, p2}, Ljava/util/TreeMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->addOverrideContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 159
    :cond_1
    :goto_0
    return-void

    .line 157
    :cond_2
    if-nez v0, :cond_1

    .line 158
    invoke-direct {p0, v1, p2}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->addDefaultContentType(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public clearAll()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 353
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 355
    :cond_0
    return-void
.end method

.method public clearOverrideContentTypes()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 364
    :cond_0
    return-void
.end method

.method public getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;
    .locals 3
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .prologue
    .line 323
    if-nez p1, :cond_0

    .line 324
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "partName"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 326
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    if-eqz v1, :cond_1

    .line 327
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 345
    :goto_0
    return-object v1

    .line 330
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getExtension()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 331
    .local v0, "extension":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 332
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    .line 341
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v1, p1}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getPart(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 342
    new-instance v1, Lorg/apache/poi/openxml4j/exceptions/OpenXML4JRuntimeException;

    .line 343
    const-string/jumbo v2, "Rule M2.4 exception : this error should NEVER happen, if so please send a mail to the developers team, thanks !"

    .line 342
    invoke-direct {v1, v2}, Lorg/apache/poi/openxml4j/exceptions/OpenXML4JRuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 345
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isContentTypeRegister(Ljava/lang/String;)Z
    .locals 2
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 276
    if-nez p1, :cond_0

    .line 277
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "contentType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    .line 280
    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)V
    .locals 7
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;
        }
    .end annotation

    .prologue
    .line 211
    if-nez p1, :cond_0

    .line 212
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "partName"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 215
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    if-eqz v4, :cond_2

    .line 216
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v4, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 218
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->overrideContentType:Ljava/util/TreeMap;

    invoke-virtual {v4, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :cond_1
    return-void

    .line 223
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getExtension()Ljava/lang/String;

    move-result-object v2

    .line 224
    .local v2, "extensionToDelete":Ljava/lang/String;
    const/4 v0, 0x1

    .line 225
    .local v0, "deleteDefaultContentTypeFlag":Z
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    if-eqz v4, :cond_4

    .line 227
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getParts()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-nez v5, :cond_7

    .line 241
    :cond_4
    :goto_0
    if-eqz v0, :cond_5

    .line 242
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->defaultContentType:Ljava/util/TreeMap;

    invoke-virtual {v4, v2}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    :cond_5
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    if-eqz v4, :cond_1

    .line 254
    :try_start_1
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getParts()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 255
    .local v3, "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v5

    invoke-virtual {v5, p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 256
    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_6

    .line 257
    new-instance v4, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 258
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Rule M2.4 is not respected: Nor a default element or override element is associated with the part: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 258
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 257
    invoke-direct {v4, v5}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 261
    .end local v3    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    :catch_0
    move-exception v1

    .line 262
    .local v1, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v4, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 227
    .end local v1    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    :cond_7
    :try_start_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 228
    .restart local v3    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v5

    invoke-virtual {v5, p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 229
    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getExtension()Ljava/lang/String;

    move-result-object v5

    .line 230
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v5

    if-eqz v5, :cond_3

    .line 231
    const/4 v0, 0x0

    .line 232
    goto :goto_0

    .line 235
    .end local v3    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    :catch_1
    move-exception v1

    .line 236
    .restart local v1    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    new-instance v4, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
