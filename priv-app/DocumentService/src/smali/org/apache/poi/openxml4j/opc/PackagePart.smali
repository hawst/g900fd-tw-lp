.class public abstract Lorg/apache/poi/openxml4j/opc/PackagePart;
.super Ljava/lang/Object;
.source "PackagePart.java"

# interfaces
.implements Lorg/apache/poi/openxml4j/opc/RelationshipSource;


# instance fields
.field protected _container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

.field protected _contentType:Lorg/apache/poi/openxml4j/opc/internal/ContentType;

.field private _isDeleted:Z

.field private _isRelationshipPart:Z

.field protected _partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

.field private _relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

.field private relArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V
    .locals 1
    .param p1, "pack"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .param p2, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p3, "contentType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    invoke-direct {v0, p3}, Lorg/apache/poi/openxml4j/opc/internal/ContentType;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/internal/ContentType;)V

    .line 135
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/internal/ContentType;)V
    .locals 1
    .param p1, "pack"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .param p2, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p3, "contentType"    # Lorg/apache/poi/openxml4j/opc/internal/ContentType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/internal/ContentType;Z)V

    .line 89
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/internal/ContentType;Z)V
    .locals 1
    .param p1, "pack"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .param p2, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p3, "contentType"    # Lorg/apache/poi/openxml4j/opc/internal/ContentType;
    .param p4, "loadRelationships"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .line 109
    iput-object p3, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_contentType:Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    .line 110
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    .line 113
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationshipPartURI()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    .line 117
    :cond_0
    if-eqz p4, :cond_1

    .line 118
    invoke-direct {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->loadRelationships()V

    .line 119
    :cond_1
    return-void
.end method

.method private getRelationshipsCore(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;
    .locals 2
    .param p1, "filter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->throwExceptionIfWriteOnly()V

    .line 425
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_0

    .line 426
    invoke-direct {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->throwExceptionIfRelationship()V

    .line 427
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0, p0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;)V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 429
    :cond_0
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0, v1, p1}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;-><init>(Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;Ljava/lang/String;)V

    return-object v0
.end method

.method private loadRelationships()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 576
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-nez v0, :cond_0

    .line 577
    invoke-direct {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->throwExceptionIfRelationship()V

    .line 578
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0, p0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;)V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 579
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->getRelArray()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->relArray:Ljava/util/List;

    .line 581
    :cond_0
    return-void
.end method

.method private throwExceptionIfRelationship()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;
        }
    .end annotation

    .prologue
    .line 564
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-eqz v0, :cond_0

    .line 565
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 566
    const-string/jumbo v1, "Can do this operation on a relationship part !"

    .line 565
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 567
    :cond_0
    return-void
.end method


# virtual methods
.method public addExternalRelationship(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 1
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "relationshipType"    # Ljava/lang/String;

    .prologue
    .line 154
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->addExternalRelationship(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v0

    return-object v0
.end method

.method public addExternalRelationship(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 5
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "relationshipType"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 177
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "target"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 179
    :cond_0
    if-nez p2, :cond_1

    .line 180
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "relationshipType"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 183
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-nez v2, :cond_2

    .line 184
    new-instance v2, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v2}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 189
    :cond_2
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .local v1, "targetURI":Ljava/net/URI;
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    sget-object v3, Lorg/apache/poi/openxml4j/opc/TargetMode;->EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    invoke-virtual {v2, v1, v3, p2, p3}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v2

    return-object v2

    .line 190
    .end local v1    # "targetURI":Ljava/net/URI;
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/net/URISyntaxException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Invalid target - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public addRelationship(Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 1
    .param p1, "targetURI"    # Ljava/net/URI;
    .param p2, "targetMode"    # Lorg/apache/poi/openxml4j/opc/TargetMode;
    .param p3, "relationshipType"    # Ljava/lang/String;

    .prologue
    .line 285
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->addRelationship(Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v0

    return-object v0
.end method

.method public addRelationship(Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 2
    .param p1, "targetURI"    # Ljava/net/URI;
    .param p2, "targetMode"    # Lorg/apache/poi/openxml4j/opc/TargetMode;
    .param p3, "relationshipType"    # Ljava/lang/String;
    .param p4, "id"    # Ljava/lang/String;

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->throwExceptionIfReadOnly()V

    .line 316
    if-nez p1, :cond_0

    .line 317
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "targetPartName"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_0
    if-nez p2, :cond_1

    .line 320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "targetMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_1
    if-nez p3, :cond_2

    .line 323
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "relationshipType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_2
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-nez v0, :cond_3

    .line 329
    invoke-static {p1}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->isRelationshipPartURI(Ljava/net/URI;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 330
    :cond_3
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 331
    const-string/jumbo v1, "Rule M1.25: The Relationships part shall not have relationships to any other part."

    .line 330
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 334
    :cond_4
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_5

    .line 335
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 338
    :cond_5
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v0

    return-object v0
.end method

.method public addRelationship(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 1
    .param p1, "targetPartName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p2, "targetMode"    # Lorg/apache/poi/openxml4j/opc/TargetMode;
    .param p3, "relationshipType"    # Ljava/lang/String;

    .prologue
    .line 214
    .line 215
    const/4 v0, 0x0

    .line 214
    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->addRelationship(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v0

    return-object v0
.end method

.method public addRelationship(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 2
    .param p1, "targetPartName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p2, "targetMode"    # Lorg/apache/poi/openxml4j/opc/TargetMode;
    .param p3, "relationshipType"    # Ljava/lang/String;
    .param p4, "id"    # Ljava/lang/String;

    .prologue
    .line 244
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->throwExceptionIfReadOnly()V

    .line 246
    if-nez p1, :cond_0

    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "targetPartName"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    if-nez p2, :cond_1

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "targetMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_1
    if-nez p3, :cond_2

    .line 253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "relationshipType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_2
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->isRelationshipPartURI()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 257
    :cond_3
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 258
    const-string/jumbo v1, "Rule M1.25: The Relationships part shall not have relationships to any other part."

    .line 257
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_4
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_5

    .line 262
    new-instance v0, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 265
    :cond_5
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v0

    return-object v0
.end method

.method public clearRelationships()V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->clear()V

    .line 349
    :cond_0
    return-void
.end method

.method public abstract close()V
.end method

.method public abstract flush()V
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_contentType:Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/internal/ContentType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 521
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryNameImpl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getEntryNameImpl()Ljava/lang/String;
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 504
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStreamImpl()Ljava/io/InputStream;

    move-result-object v0

    .line 511
    .local v0, "inStream":Ljava/io/InputStream;
    return-object v0
.end method

.method protected abstract getInputStreamImpl()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 6

    .prologue
    .line 536
    instance-of v2, p0, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;

    if-eqz v2, :cond_1

    .line 538
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    iget-object v3, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    invoke-virtual {v2, v3}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->removePart(Lorg/apache/poi/openxml4j/opc/PackagePartName;)V

    .line 541
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    iget-object v3, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .line 542
    iget-object v4, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_contentType:Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/internal/ContentType;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 541
    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->createPart(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;Z)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    .line 544
    .local v1, "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    if-nez v1, :cond_0

    .line 545
    new-instance v2, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 546
    const-string/jumbo v3, "Can\'t create a temporary part !"

    .line 545
    invoke-direct {v2, v3}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 548
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    iput-object v2, v1, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 549
    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getOutputStreamImpl()Ljava/io/OutputStream;

    move-result-object v0

    .line 553
    .end local v1    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    .local v0, "outStream":Ljava/io/OutputStream;
    :goto_0
    return-object v0

    .line 551
    .end local v0    # "outStream":Ljava/io/OutputStream;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getOutputStreamImpl()Ljava/io/OutputStream;

    move-result-object v0

    .restart local v0    # "outStream":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method protected abstract getOutputStreamImpl()Ljava/io/OutputStream;
.end method

.method public getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    return-object v0
.end method

.method public getPartName()Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    return-object v0
.end method

.method public getRelArray()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 584
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->relArray:Ljava/util/List;

    return-object v0
.end method

.method public getRelatedPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 9
    .param p1, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 473
    invoke-virtual {p0, p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->isRelationshipExists(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 474
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Relationship "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " doesn\'t start with this part "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 478
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    move-result-object v4

    .line 479
    .local v4, "target":Ljava/net/URI;
    invoke-virtual {v4}, Ljava/net/URI;->getFragment()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 480
    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    .line 482
    .local v3, "t":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/net/URI;

    const/4 v6, 0x0

    const/16 v7, 0x23

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v4    # "target":Ljava/net/URI;
    .local v5, "target":Ljava/net/URI;
    move-object v4, v5

    .line 489
    .end local v3    # "t":Ljava/lang/String;
    .end local v5    # "target":Ljava/net/URI;
    .restart local v4    # "target":Ljava/net/URI;
    :cond_1
    invoke-static {v4}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->createPartName(Ljava/net/URI;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v2

    .line 490
    .local v2, "relName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v6, v2}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getPart(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    .line 491
    .local v1, "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    if-nez v1, :cond_2

    .line 492
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "No part found for relationship "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 483
    .end local v1    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    .end local v2    # "relName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .restart local v3    # "t":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Ljava/net/URISyntaxException;
    new-instance v6, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Invalid target URI: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 494
    .end local v0    # "e":Ljava/net/URISyntaxException;
    .end local v3    # "t":Ljava/lang/String;
    .restart local v1    # "part":Lorg/apache/poi/openxml4j/opc/PackagePart;
    .restart local v2    # "relName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    :cond_2
    return-object v1
.end method

.method public getRelationship(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 385
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {v0, p1}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->getRelationshipByID(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v0

    return-object v0
.end method

.method public getRelationships()Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 373
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsCore(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v0

    return-object v0
.end method

.method public getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;
    .locals 1
    .param p1, "relationshipType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 403
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->throwExceptionIfWriteOnly()V

    .line 405
    invoke-direct {p0, p1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsCore(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v0

    return-object v0
.end method

.method public hasRelationships()Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    .line 441
    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->size()I

    move-result v0

    .line 440
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 641
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isDeleted:Z

    return v0
.end method

.method public isRelationshipExists(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Z
    .locals 3
    .param p1, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 455
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationships()Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 462
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 455
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 456
    .local v0, "r":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    if-ne v0, p1, :cond_0

    .line 457
    const/4 v1, 0x1

    goto :goto_1

    .line 459
    .end local v0    # "r":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public isRelationshipPart()Z
    .locals 1

    .prologue
    .line 634
    iget-boolean v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    return v0
.end method

.method public abstract load(Ljava/io/InputStream;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation
.end method

.method public removeRelationship(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 359
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->throwExceptionIfReadOnly()V

    .line 360
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_relationships:Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {v0, p1}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->removeRelationship(Ljava/lang/String;)V

    .line 362
    :cond_0
    return-void
.end method

.method public abstract save(Ljava/io/OutputStream;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    if-nez v0, :cond_0

    .line 620
    new-instance v0, Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    invoke-direct {v0, p1}, Lorg/apache/poi/openxml4j/opc/internal/ContentType;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_contentType:Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    .line 624
    return-void

    .line 622
    :cond_0
    new-instance v0, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 623
    const-string/jumbo v1, "You can\'t change the content type of a part."

    .line 622
    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 649
    iput-boolean p1, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_isDeleted:Z

    .line 650
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 654
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " - Content Type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 655
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_contentType:Lorg/apache/poi/openxml4j/opc/internal/ContentType;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/internal/ContentType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 654
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
