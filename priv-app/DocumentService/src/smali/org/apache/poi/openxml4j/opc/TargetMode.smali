.class public final enum Lorg/apache/poi/openxml4j/opc/TargetMode;
.super Ljava/lang/Enum;
.source "TargetMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/openxml4j/opc/TargetMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/TargetMode;

.field public static final enum EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

.field public static final enum INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lorg/apache/poi/openxml4j/opc/TargetMode;

    const-string/jumbo v1, "INTERNAL"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/openxml4j/opc/TargetMode;-><init>(Ljava/lang/String;I)V

    .line 29
    sput-object v0, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 30
    new-instance v0, Lorg/apache/poi/openxml4j/opc/TargetMode;

    const-string/jumbo v1, "EXTERNAL"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/openxml4j/opc/TargetMode;-><init>(Ljava/lang/String;I)V

    .line 31
    sput-object v0, Lorg/apache/poi/openxml4j/opc/TargetMode;->EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/poi/openxml4j/opc/TargetMode;

    sget-object v1, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/openxml4j/opc/TargetMode;->EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/poi/openxml4j/opc/TargetMode;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/TargetMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/TargetMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/openxml4j/opc/TargetMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/TargetMode;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/openxml4j/opc/TargetMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/openxml4j/opc/TargetMode;->ENUM$VALUES:[Lorg/apache/poi/openxml4j/opc/TargetMode;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/openxml4j/opc/TargetMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
