.class public final Lorg/apache/poi/openxml4j/opc/PackageRelationship;
.super Ljava/lang/Object;
.source "PackageRelationship.java"


# static fields
.field public static final ID_ATTRIBUTE_NAME:Ljava/lang/String; = "Id"

.field public static final RELATIONSHIPS_TAG_NAME:Ljava/lang/String; = "Relationships"

.field public static final RELATIONSHIP_TAG_NAME:Ljava/lang/String; = "Relationship"

.field public static final TARGET_ATTRIBUTE_NAME:Ljava/lang/String; = "Target"

.field public static final TARGET_MODE_ATTRIBUTE_NAME:Ljava/lang/String; = "TargetMode"

.field public static final TYPE_ATTRIBUTE_NAME:Ljava/lang/String; = "Type"

.field private static containerRelationshipPart:Ljava/net/URI;


# instance fields
.field private container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

.field private id:Ljava/lang/String;

.field private relationshipType:Ljava/lang/String;

.field private source:Lorg/apache/poi/openxml4j/opc/PackagePart;

.field private targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

.field private targetUri:Ljava/net/URI;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    :try_start_0
    new-instance v0, Ljava/net/URI;

    const-string/jumbo v1, "/_rels/.rels"

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->containerRelationshipPart:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePart;Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "pkg"    # Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .param p2, "sourcePart"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p3, "targetUri"    # Ljava/net/URI;
    .param p4, "targetMode"    # Lorg/apache/poi/openxml4j/opc/TargetMode;
    .param p5, "relationshipType"    # Ljava/lang/String;
    .param p6, "id"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    if-nez p1, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "pkg"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    if-nez p3, :cond_1

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "targetUri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_1
    if-nez p5, :cond_2

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "relationshipType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_2
    if-nez p6, :cond_3

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_3
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    .line 110
    iput-object p2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 111
    iput-object p3, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    .line 112
    iput-object p4, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 113
    iput-object p5, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    .line 114
    iput-object p6, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public static getContainerPartRelationship()Ljava/net/URI;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->containerRelationshipPart:Ljava/net/URI;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 119
    instance-of v2, p1, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    if-nez v2, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 122
    check-cast v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .line 123
    .local v0, "rel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    iget-object v2, v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    iget-object v3, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    :cond_2
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    iget-object v3, v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    .line 127
    iget-object v3, v0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    invoke-virtual {v2, v3}, Ljava/net/URI;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    return-object v0
.end method

.method public getRelationshipType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    return-object v0
.end method

.method public getSourceURI()Ljava/net/URI;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    if-nez v0, :cond_0

    .line 179
    sget-object v0, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->PACKAGE_ROOT_URI:Ljava/net/URI;

    .line 181
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    iget-object v0, v0, Lorg/apache/poi/openxml4j/opc/PackagePart;->_partName:Lorg/apache/poi/openxml4j/opc/PackagePartName;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getURI()Ljava/net/URI;

    move-result-object v0

    goto :goto_0
.end method

.method public getTargetMode()Lorg/apache/poi/openxml4j/opc/TargetMode;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    return-object v0
.end method

.method public getTargetURI()Ljava/net/URI;
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    sget-object v1, Lorg/apache/poi/openxml4j/opc/TargetMode;->EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    if-ne v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    .line 210
    :goto_0
    return-object v0

    .line 206
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getSourceURI()Ljava/net/URI;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    invoke-static {v0, v1}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->resolvePartUri(Ljava/net/URI;Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 133
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 132
    add-int/2addr v1, v0

    .line 134
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 132
    :goto_0
    add-int/2addr v0, v1

    .line 135
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/opc/TargetMode;->hashCode()I

    move-result v1

    .line 132
    add-int/2addr v0, v1

    .line 136
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->hashCode()I

    move-result v1

    .line 132
    add-int/2addr v0, v1

    return v0

    .line 134
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string/jumbo v1, "id=null"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    if-nez v1, :cond_1

    const-string/jumbo v1, " - container=null"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string/jumbo v1, " - relationshipType=null"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->source:Lorg/apache/poi/openxml4j/opc/PackagePart;

    if-nez v1, :cond_3

    const-string/jumbo v1, " - source=null"

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetUri:Ljava/net/URI;

    if-nez v1, :cond_4

    const-string/jumbo v1, " - target=null"

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    iget-object v1, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    if-nez v1, :cond_5

    const-string/jumbo v1, ",targetMode=null"

    :goto_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 216
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 217
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " - container="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 218
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->container:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 220
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " - relationshipType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->relationshipType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 221
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " - source="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 222
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getSourceURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 223
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " - target="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 225
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, ",targetMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->targetMode:Lorg/apache/poi/openxml4j/opc/TargetMode;

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/TargetMode;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5
.end method
