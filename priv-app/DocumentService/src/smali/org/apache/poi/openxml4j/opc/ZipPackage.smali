.class public final Lorg/apache/poi/openxml4j/opc/ZipPackage;
.super Lorg/apache/poi/openxml4j/opc/Package;
.source "ZipPackage.java"


# static fields
.field private static logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private relArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field

.field private final zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/poi/openxml4j/opc/ZipPackage;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->defaultPackageAccess:Lorg/apache/poi/openxml4j/opc/PackageAccess;

    invoke-direct {p0, v0}, Lorg/apache/poi/openxml4j/opc/Package;-><init>(Lorg/apache/poi/openxml4j/opc/PackageAccess;)V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    .line 69
    return-void
.end method

.method constructor <init>(Ljava/io/InputStream;Lorg/apache/poi/openxml4j/opc/PackageAccess;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "access"    # Lorg/apache/poi/openxml4j/opc/PackageAccess;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p2}, Lorg/apache/poi/openxml4j/opc/Package;-><init>(Lorg/apache/poi/openxml4j/opc/PackageAccess;)V

    .line 83
    new-instance v0, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;

    .line 84
    new-instance v1, Ljava/util/zip/ZipInputStream;

    invoke-direct {v1, p1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;-><init>(Ljava/util/zip/ZipInputStream;)V

    .line 83
    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    .line 86
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lorg/apache/poi/openxml4j/opc/PackageAccess;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "access"    # Lorg/apache/poi/openxml4j/opc/PackageAccess;

    .prologue
    .line 99
    invoke-direct {p0, p2}, Lorg/apache/poi/openxml4j/opc/Package;-><init>(Lorg/apache/poi/openxml4j/opc/PackageAccess;)V

    .line 101
    invoke-static {p1}, Lorg/apache/poi/openxml4j/opc/internal/ZipHelper;->openZipFile(Ljava/lang/String;)Ljava/util/zip/ZipFile;

    move-result-object v0

    .line 102
    .local v0, "zipFile":Ljava/util/zip/ZipFile;
    if-nez v0, :cond_0

    .line 103
    new-instance v1, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Can\'t open the specified file: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-direct {v1, v2}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 105
    :cond_0
    new-instance v1, Lorg/apache/poi/openxml4j/util/ZipFileZipEntrySource;

    invoke-direct {v1, v0}, Lorg/apache/poi/openxml4j/util/ZipFileZipEntrySource;-><init>(Ljava/util/zip/ZipFile;)V

    iput-object v1, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    .line 106
    return-void
.end method

.method public static buildPartName(Ljava/util/zip/ZipEntry;)Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .locals 6
    .param p0, "entry"    # Ljava/util/zip/ZipEntry;

    .prologue
    const/4 v1, 0x0

    .line 220
    :try_start_0
    invoke-virtual {p0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 221
    const-string/jumbo v3, "[Content_Types].xml"

    .line 220
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 221
    if-eqz v2, :cond_0

    .line 231
    :goto_0
    return-object v1

    .line 225
    :cond_0
    invoke-virtual {p0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/poi/openxml4j/opc/internal/ZipHelper;->getOPCNameFromZipItemName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 224
    invoke-static {v2}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->createPartName(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackagePartName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lorg/apache/poi/openxml4j/opc/ZipPackage;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Entry "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 230
    const-string/jumbo v5, " is not valid, so this part won\'t be add to the package."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 228
    invoke-virtual {v2, v3, v4, v0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized generateTempFileName(Ljava/io/File;)Ljava/lang/String;
    .locals 4
    .param p1, "directory"    # Ljava/io/File;

    .prologue
    .line 338
    monitor-enter p0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 339
    const-string/jumbo v2, "OpenXML4J"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 338
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 340
    .local v0, "tmpFilename":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 341
    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/openxml4j/opc/internal/FileHelper;->getFilename(Ljava/io/File;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 338
    .end local v0    # "tmpFilename":Ljava/io/File;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method protected closeImpl()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x5

    .line 291
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->flush()V

    .line 294
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->originalPackagePath:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 295
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->originalPackagePath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->originalPackagePath:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    .local v0, "targetFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 302
    invoke-static {v0}, Lorg/apache/poi/openxml4j/opc/internal/FileHelper;->getDirectory(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 301
    invoke-direct {p0, v2}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->generateTempFileName(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    .line 302
    const-string/jumbo v3, ".tmp"

    .line 300
    invoke-static {v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 306
    .local v1, "tempFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {p0, v1}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->save(Ljava/io/File;)V

    .line 310
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    invoke-interface {v2}, Lorg/apache/poi/openxml4j/util/ZipEntrySource;->close()V

    .line 312
    invoke-static {v1, v0}, Lorg/apache/poi/openxml4j/opc/internal/FileHelper;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 317
    sget-object v2, Lorg/apache/poi/openxml4j/opc/ZipPackage;->logger:Lorg/apache/poi/util/POILogger;

    .line 318
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "The temporary file: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 319
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 320
    const-string/jumbo v4, "\' cannot be deleted ! Make sure that no other application use it."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 318
    invoke-virtual {v2, v6, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 328
    .end local v0    # "targetFile":Ljava/io/File;
    .end local v1    # "tempFile":Ljava/io/File;
    :cond_0
    return-void

    .line 313
    .restart local v0    # "targetFile":Ljava/io/File;
    .restart local v1    # "tempFile":Ljava/io/File;
    :catchall_0
    move-exception v2

    .line 316
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 317
    sget-object v3, Lorg/apache/poi/openxml4j/opc/ZipPackage;->logger:Lorg/apache/poi/util/POILogger;

    .line 318
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "The temporary file: \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 319
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 320
    const-string/jumbo v5, "\' cannot be deleted ! Make sure that no other application use it."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 318
    invoke-virtual {v3, v6, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 322
    :cond_1
    throw v2

    .line 324
    .end local v1    # "tempFile":Ljava/io/File;
    :cond_2
    new-instance v2, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;

    .line 325
    const-string/jumbo v3, "Can\'t close a package not previously open with the open() method !"

    .line 324
    invoke-direct {v2, v3}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected createPartImpl(Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;Z)Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 3
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .param p2, "contentType"    # Ljava/lang/String;
    .param p3, "loadRelationships"    # Z

    .prologue
    .line 248
    if-nez p2, :cond_0

    .line 249
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "contentType"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 251
    :cond_0
    if-nez p1, :cond_1

    .line 252
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "partName"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 255
    :cond_1
    :try_start_0
    new-instance v1, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    invoke-direct {v1, p0, p1, p2, p3}, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    :goto_0
    return-object v1

    .line 257
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    sget-object v1, Lorg/apache/poi/openxml4j/opc/ZipPackage;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Throwable;)V

    .line 259
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected flushImpl()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method protected getPartImpl(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 1
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .prologue
    .line 367
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v0, p1}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v0, p1}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 370
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getPartsImpl()[Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    if-nez v6, :cond_0

    .line 123
    new-instance v6, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-direct {v6}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;-><init>()V

    iput-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    .line 126
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    if-nez v6, :cond_1

    .line 127
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->values()Ljava/util/Collection;

    move-result-object v6

    .line 128
    iget-object v7, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v7}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->size()I

    move-result v7

    new-array v7, v7, [Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 127
    invoke-interface {v6, v7}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 205
    :goto_0
    return-object v6

    .line 131
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    invoke-interface {v6}, Lorg/apache/poi/openxml4j/util/ZipEntrySource;->getEntries()Ljava/util/Enumeration;

    move-result-object v2

    .line 132
    .local v2, "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_3

    .line 147
    :goto_1
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    if-nez v6, :cond_4

    .line 148
    new-instance v6, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 149
    const-string/jumbo v7, "Package should contain a content type part [M1.13]"

    .line 148
    invoke-direct {v6, v7}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 133
    :cond_3
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/zip/ZipEntry;

    .line 134
    .local v3, "entry":Ljava/util/zip/ZipEntry;
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    .line 135
    const-string/jumbo v7, "[Content_Types].xml"

    .line 134
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    .line 135
    if-eqz v6, :cond_2

    .line 137
    :try_start_0
    new-instance v6, Lorg/apache/poi/openxml4j/opc/internal/ZipContentTypeManager;

    .line 138
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->getZipArchive()Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    move-result-object v7

    invoke-interface {v7, v3}, Lorg/apache/poi/openxml4j/util/ZipEntrySource;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7, p0}, Lorg/apache/poi/openxml4j/opc/internal/ZipContentTypeManager;-><init>(Ljava/io/InputStream;Lorg/apache/poi/openxml4j/opc/OPCPackage;)V

    .line 137
    iput-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 139
    :catch_0
    move-exception v1

    .line 140
    .local v1, "e":Ljava/io/IOException;
    new-instance v6, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 156
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "entry":Ljava/util/zip/ZipEntry;
    :cond_4
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    invoke-interface {v6}, Lorg/apache/poi/openxml4j/util/ZipEntrySource;->getEntries()Ljava/util/Enumeration;

    move-result-object v2

    .line 157
    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_7

    .line 175
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    invoke-interface {v6}, Lorg/apache/poi/openxml4j/util/ZipEntrySource;->getEntries()Ljava/util/Enumeration;

    move-result-object v2

    .line 176
    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_8

    .line 205
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v6}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->values()Ljava/util/Collection;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v7}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->size()I

    move-result v7

    new-array v7, v7, [Lorg/apache/poi/openxml4j/opc/ZipPackagePart;

    invoke-interface {v6, v7}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/poi/openxml4j/opc/PackagePart;

    goto :goto_0

    .line 158
    :cond_7
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/zip/ZipEntry;

    .line 159
    .restart local v3    # "entry":Ljava/util/zip/ZipEntry;
    invoke-static {v3}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->buildPartName(Ljava/util/zip/ZipEntry;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v4

    .line 160
    .local v4, "partName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    if-eqz v4, :cond_5

    .line 163
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    invoke-virtual {v6, v4}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "contentType":Ljava/lang/String;
    if-eqz v0, :cond_5

    const-string/jumbo v6, "application/vnd.openxmlformats-package.relationships+xml"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 166
    :try_start_1
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    new-instance v7, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;

    .line 167
    invoke-direct {v7, p0, v3, v4, v0}, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Ljava/util/zip/ZipEntry;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 166
    invoke-virtual {v6, v4, v7}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->put(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/PackagePart;)Lorg/apache/poi/openxml4j/opc/PackagePart;
    :try_end_1
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 168
    :catch_1
    move-exception v1

    .line 169
    .local v1, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;
    new-instance v6, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 177
    .end local v0    # "contentType":Ljava/lang/String;
    .end local v1    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;
    .end local v3    # "entry":Ljava/util/zip/ZipEntry;
    .end local v4    # "partName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    :cond_8
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/zip/ZipEntry;

    .line 178
    .restart local v3    # "entry":Ljava/util/zip/ZipEntry;
    invoke-static {v3}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->buildPartName(Ljava/util/zip/ZipEntry;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v4

    .line 179
    .restart local v4    # "partName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    if-eqz v4, :cond_6

    .line 181
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    .line 182
    invoke-virtual {v6, v4}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v0

    .line 183
    .restart local v0    # "contentType":Ljava/lang/String;
    if-eqz v0, :cond_9

    const-string/jumbo v6, "application/vnd.openxmlformats-package.relationships+xml"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 186
    :cond_9
    if-eqz v0, :cond_a

    .line 188
    :try_start_2
    new-instance v5, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;

    invoke-direct {v5, p0, v3, v4, v0}, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Ljava/util/zip/ZipEntry;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 190
    .local v5, "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    iget-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->partList:Lorg/apache/poi/openxml4j/opc/PackagePartCollection;

    invoke-virtual {v6, v4, v5}, Lorg/apache/poi/openxml4j/opc/PackagePartCollection;->put(Lorg/apache/poi/openxml4j/opc/PackagePartName;Lorg/apache/poi/openxml4j/opc/PackagePart;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    .line 191
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "/word/document.xml"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 192
    invoke-virtual {v5}, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;->getRelArray()Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->relArray:Ljava/util/List;
    :try_end_2
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_3

    .line 194
    .end local v5    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    :catch_2
    move-exception v1

    .line 195
    .restart local v1    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;
    new-instance v6, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    invoke-virtual {v1}, Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 198
    .end local v1    # "e":Lorg/apache/poi/openxml4j/exceptions/InvalidOperationException;
    :cond_a
    new-instance v6, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;

    .line 199
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "The part "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/PackagePartName;->getURI()Ljava/net/URI;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 201
    const-string/jumbo v8, " does not have any content type ! Rule: Package require content types when retrieving a part from a package. [M.1.14]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 199
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 198
    invoke-direct {v6, v7}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public getRelArray()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->relArray:Ljava/util/List;

    return-object v0
.end method

.method public getZipArchive()Lorg/apache/poi/openxml4j/util/ZipEntrySource;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    return-object v0
.end method

.method protected removePartImpl(Lorg/apache/poi/openxml4j/opc/PackagePartName;)V
    .locals 2
    .param p1, "partName"    # Lorg/apache/poi/openxml4j/opc/PackagePartName;

    .prologue
    .line 271
    if-nez p1, :cond_0

    .line 272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "partUri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273
    :cond_0
    return-void
.end method

.method protected revertImpl()V
    .locals 1

    .prologue
    .line 351
    :try_start_0
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/ZipPackage;->zipArchive:Lorg/apache/poi/openxml4j/util/ZipEntrySource;

    invoke-interface {v0}, Lorg/apache/poi/openxml4j/util/ZipEntrySource;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 353
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected saveImpl(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    return-void
.end method
