.class public final Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;
.super Ljava/io/OutputStream;
.source "MemoryPackagePartOutputStream.java"


# instance fields
.field private _buff:Ljava/io/ByteArrayOutputStream;

.field private _part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;


# direct methods
.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    .line 37
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    .line 38
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->flush()V

    .line 52
    return-void
.end method

.method public flush()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 61
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 62
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iget-object v2, v2, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    if-eqz v2, :cond_0

    .line 63
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iget-object v2, v2, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    array-length v2, v2

    iget-object v3, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    add-int/2addr v2, v3

    new-array v1, v2, [B

    .line 65
    .local v1, "newArray":[B
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iget-object v2, v2, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    iget-object v3, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iget-object v3, v3, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 69
    .local v0, "buffArr":[B
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iget-object v2, v2, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    array-length v2, v2

    .line 70
    array-length v3, v0

    .line 69
    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iput-object v1, v2, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    .line 83
    .end local v0    # "buffArr":[B
    .end local v1    # "newArray":[B
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 84
    return-void

    .line 76
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_part:Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;

    iget-object v3, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, v2, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePart;->data:[B

    goto :goto_0
.end method

.method public write(I)V
    .locals 1
    .param p1, "b"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 43
    return-void
.end method

.method public write([B)V
    .locals 1
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 94
    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/poi/openxml4j/opc/internal/MemoryPackagePartOutputStream;->_buff:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 89
    return-void
.end method
