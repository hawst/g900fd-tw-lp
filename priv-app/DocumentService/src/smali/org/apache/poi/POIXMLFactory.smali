.class public abstract Lorg/apache/poi/POIXMLFactory;
.super Ljava/lang/Object;
.source "POIXMLFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copyDocumentPart(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/POIXMLDocumentPart;
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract createDocumentPart(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;Lorg/apache/poi/openxml4j/opc/PackagePart;)Lorg/apache/poi/POIXMLDocumentPart;
.end method

.method public abstract newDocumentPart(Lorg/apache/poi/POIXMLRelation;)Lorg/apache/poi/POIXMLDocumentPart;
.end method
