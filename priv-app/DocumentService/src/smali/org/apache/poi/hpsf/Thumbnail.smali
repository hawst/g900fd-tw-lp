.class public final Lorg/apache/poi/hpsf/Thumbnail;
.super Ljava/lang/Object;
.source "Thumbnail.java"


# static fields
.field public static CFTAG_FMTID:I

.field public static CFTAG_MACINTOSH:I

.field public static CFTAG_NODATA:I

.field public static CFTAG_WINDOWS:I

.field public static CF_BITMAP:I

.field public static CF_DIB:I

.field public static CF_ENHMETAFILE:I

.field public static CF_METAFILEPICT:I

.field public static OFFSET_CF:I

.field public static OFFSET_CFTAG:I

.field public static OFFSET_WMFDATA:I


# instance fields
.field private _thumbnailData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 35
    const/4 v0, 0x4

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_CFTAG:I

    .line 45
    sput v1, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_CF:I

    .line 63
    const/16 v0, 0x14

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_WMFDATA:I

    .line 71
    const/4 v0, -0x1

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CFTAG_WINDOWS:I

    .line 79
    const/4 v0, -0x2

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CFTAG_MACINTOSH:I

    .line 87
    const/4 v0, -0x3

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CFTAG_FMTID:I

    .line 95
    const/4 v0, 0x0

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CFTAG_NODATA:I

    .line 105
    const/4 v0, 0x3

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CF_METAFILEPICT:I

    .line 110
    sput v1, Lorg/apache/poi/hpsf/Thumbnail;->CF_DIB:I

    .line 115
    const/16 v0, 0xe

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CF_ENHMETAFILE:I

    .line 124
    const/4 v0, 0x2

    sput v0, Lorg/apache/poi/hpsf/Thumbnail;->CF_BITMAP:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hpsf/Thumbnail;->_thumbnailData:[B

    .line 144
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "thumbnailData"    # [B

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hpsf/Thumbnail;->_thumbnailData:[B

    .line 156
    iput-object p1, p0, Lorg/apache/poi/hpsf/Thumbnail;->_thumbnailData:[B

    .line 157
    return-void
.end method


# virtual methods
.method public getClipboardFormat()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/HPSFException;
        }
    .end annotation

    .prologue
    .line 233
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Thumbnail;->getClipboardFormatTag()J

    move-result-wide v0

    sget v2, Lorg/apache/poi/hpsf/Thumbnail;->CFTAG_WINDOWS:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 234
    new-instance v0, Lorg/apache/poi/hpsf/HPSFException;

    const-string/jumbo v1, "Clipboard Format Tag of Thumbnail must be CFTAG_WINDOWS."

    invoke-direct {v0, v1}, Lorg/apache/poi/hpsf/HPSFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Thumbnail;->getThumbnail()[B

    move-result-object v0

    sget v1, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_CF:I

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method public getClipboardFormatTag()J
    .locals 4

    .prologue
    .line 205
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Thumbnail;->getThumbnail()[B

    move-result-object v2

    .line 206
    sget v3, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_CFTAG:I

    .line 205
    invoke-static {v2, v3}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    .line 207
    .local v0, "clipboardFormatTag":J
    return-wide v0
.end method

.method public getThumbnail()[B
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/poi/hpsf/Thumbnail;->_thumbnailData:[B

    return-object v0
.end method

.method public getThumbnailAsWMF()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/HPSFException;
        }
    .end annotation

    .prologue
    .line 270
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Thumbnail;->getThumbnail()[B

    move-result-object v0

    .line 271
    .local v0, "thumbnail":[B
    if-nez v0, :cond_0

    .line 272
    const/4 v1, 0x0

    .line 281
    :goto_0
    return-object v1

    .line 274
    :cond_0
    array-length v3, v0

    sget v4, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_WMFDATA:I

    sub-int v2, v3, v4

    .line 275
    .local v2, "wmfImageLength":I
    new-array v1, v2, [B

    .line 277
    .local v1, "wmfImage":[B
    sget v3, Lorg/apache/poi/hpsf/Thumbnail;->OFFSET_WMFDATA:I

    .line 279
    const/4 v4, 0x0

    .line 276
    invoke-static {v0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public setThumbnail([B)V
    .locals 0
    .param p1, "thumbnail"    # [B

    .prologue
    .line 184
    iput-object p1, p0, Lorg/apache/poi/hpsf/Thumbnail;->_thumbnailData:[B

    .line 185
    return-void
.end method
