.class public Lorg/apache/poi/hpsf/ClassID;
.super Ljava/lang/Object;
.source "ClassID.java"


# static fields
.field public static final LENGTH:I = 0x10


# instance fields
.field protected bytes:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 64
    return-void

    .line 63
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x0

    aput-byte v2, v1, v0

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "src"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hpsf/ClassID;->read([BI)[B

    .line 52
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 197
    if-eqz p1, :cond_0

    instance-of v3, p1, Lorg/apache/poi/hpsf/ClassID;

    if-nez v3, :cond_1

    .line 205
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v0, p1

    .line 199
    check-cast v0, Lorg/apache/poi/hpsf/ClassID;

    .line 200
    .local v0, "cid":Lorg/apache/poi/hpsf/ClassID;
    iget-object v3, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    array-length v3, v3

    iget-object v4, v0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 202
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    array-length v3, v3

    if-lt v1, v3, :cond_2

    .line 205
    const/4 v2, 0x1

    goto :goto_0

    .line 203
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    aget-byte v3, v3, v1

    iget-object v4, v0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    aget-byte v4, v4, v1

    if-ne v3, v4, :cond_0

    .line 202
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getBytes()[B
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 215
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 78
    const/16 v0, 0x10

    return v0
.end method

.method public read([BI)[B
    .locals 5
    .param p1, "src"    # [B
    .param p2, "offset"    # I

    .prologue
    const/16 v4, 0x10

    .line 122
    new-array v1, v4, [B

    iput-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    .line 125
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x0

    add-int/lit8 v3, p2, 0x3

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 126
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x1

    add-int/lit8 v3, p2, 0x2

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 127
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x2

    add-int/lit8 v3, p2, 0x1

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 128
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x3

    add-int/lit8 v3, p2, 0x0

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 131
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x4

    add-int/lit8 v3, p2, 0x5

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 132
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x5

    add-int/lit8 v3, p2, 0x4

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 135
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x6

    add-int/lit8 v3, p2, 0x7

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 136
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v2, 0x7

    add-int/lit8 v3, p2, 0x6

    aget-byte v3, p1, v3

    aput-byte v3, v1, v2

    .line 139
    const/16 v0, 0x8

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 142
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    return-object v1

    .line 140
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    add-int v2, v0, p2

    aget-byte v2, p1, v2

    aput-byte v2, v1, v0

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setBytes([B)V
    .locals 3
    .param p1, "bytes"    # [B

    .prologue
    .line 104
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 106
    return-void

    .line 105
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    aget-byte v2, p1, v0

    aput-byte v2, v1, v0

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 228
    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 229
    .local v1, "sbClassId":Ljava/lang/StringBuffer;
    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 230
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    .line 236
    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 237
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 232
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    aget-byte v2, v2, v0

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->toHex(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 233
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    const/4 v2, 0x7

    if-eq v0, v2, :cond_1

    const/16 v2, 0x9

    if-ne v0, v2, :cond_2

    .line 234
    :cond_1
    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 230
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public write([BI)V
    .locals 5
    .param p1, "dst"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayStoreException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x10

    .line 162
    array-length v1, p1

    if-ge v1, v4, :cond_0

    .line 163
    new-instance v1, Ljava/lang/ArrayStoreException;

    .line 164
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Destination byte[] must have room for at least 16 bytes, but has a length of only "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 165
    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 164
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-direct {v1, v2}, Ljava/lang/ArrayStoreException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 167
    :cond_0
    add-int/lit8 v1, p2, 0x0

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 168
    add-int/lit8 v1, p2, 0x1

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 169
    add-int/lit8 v1, p2, 0x2

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 170
    add-int/lit8 v1, p2, 0x3

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 173
    add-int/lit8 v1, p2, 0x4

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 174
    add-int/lit8 v1, p2, 0x5

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 177
    add-int/lit8 v1, p2, 0x6

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 178
    add-int/lit8 v1, p2, 0x7

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 181
    const/16 v0, 0x8

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_1

    .line 183
    return-void

    .line 182
    :cond_1
    add-int v1, v0, p2

    iget-object v2, p0, Lorg/apache/poi/hpsf/ClassID;->bytes:[B

    aget-byte v2, v2, v0

    aput-byte v2, p1, v1

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
