.class Lorg/apache/poi/hpsf/UnicodeString;
.super Ljava/lang/Object;
.source "UnicodeString.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _value:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/poi/hpsf/UnicodeString;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 29
    sput-object v0, Lorg/apache/poi/hpsf/UnicodeString;->logger:Lorg/apache/poi/util/POILogger;

    .line 30
    return-void
.end method

.method constructor <init>([BI)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 38
    .local v0, "length":I
    if-nez v0, :cond_1

    .line 40
    const/4 v1, 0x0

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    .line 51
    :cond_0
    return-void

    .line 45
    :cond_1
    add-int/lit8 v1, p2, 0x4

    mul-int/lit8 v2, v0, 0x2

    .line 44
    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    .line 47
    iget-object v1, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    mul-int/lit8 v2, v0, 0x2

    add-int/lit8 v2, v2, -0x1

    aget-byte v1, v1, v2

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    mul-int/lit8 v2, v0, 0x2

    add-int/lit8 v2, v2, -0x2

    aget-byte v1, v1, v2

    if-eqz v1, :cond_0

    .line 48
    :cond_2
    new-instance v1, Lorg/apache/poi/hpsf/IllegalPropertySetDataException;

    .line 49
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "UnicodeString started at offset #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 50
    const-string/jumbo v3, " is not NULL-terminated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 48
    invoke-direct {v1, v2}, Lorg/apache/poi/hpsf/IllegalPropertySetDataException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method getSize()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method getValue()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    return-object v0
.end method

.method toJavaString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 65
    iget-object v2, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    array-length v2, v2

    if-nez v2, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    .line 68
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    .line 69
    iget-object v3, p0, Lorg/apache/poi/hpsf/UnicodeString;->_value:[B

    array-length v3, v3

    shr-int/lit8 v3, v3, 0x1

    .line 68
    invoke-static {v2, v4, v3}, Lorg/apache/poi/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "result":Ljava/lang/String;
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 72
    .local v1, "terminator":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 74
    sget-object v2, Lorg/apache/poi/hpsf/UnicodeString;->logger:Lorg/apache/poi/util/POILogger;

    .line 76
    const-string/jumbo v3, "String terminator (\\0) for UnicodeString property value not found.Continue without trimming and hope for the best."

    .line 74
    invoke-virtual {v2, v5, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_2

    .line 82
    sget-object v2, Lorg/apache/poi/hpsf/UnicodeString;->logger:Lorg/apache/poi/util/POILogger;

    .line 84
    const-string/jumbo v3, "String terminator (\\0) for UnicodeString property value occured before the end of string. Trimming and hope for the best."

    .line 82
    invoke-virtual {v2, v5, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 87
    :cond_2
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
