.class public Lorg/apache/poi/hpsf/VariantSupport;
.super Lorg/apache/poi/hpsf/Variant;
.source "VariantSupport.java"


# static fields
.field public static final SUPPORTED_TYPES:[I

.field private static logUnsupportedTypes:Z

.field private static logger:Lorg/apache/poi/util/POILogger;

.field protected static unsupportedMessage:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 51
    const-class v0, Lorg/apache/poi/hpsf/VariantSupport;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hpsf/VariantSupport;->logger:Lorg/apache/poi/util/POILogger;

    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/poi/hpsf/VariantSupport;->logUnsupportedTypes:Z

    .line 113
    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v1, 0x1

    .line 114
    aput v2, v0, v1

    aput v3, v0, v2

    const/16 v1, 0x14

    aput v1, v0, v3

    const/4 v1, 0x4

    aput v4, v0, v1

    .line 115
    const/16 v1, 0x40

    aput v1, v0, v4

    const/4 v1, 0x6

    const/16 v2, 0x1e

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x1f

    aput v2, v0, v1

    const/16 v1, 0x8

    .line 116
    const/16 v2, 0x47

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xb

    aput v2, v0, v1

    .line 113
    sput-object v0, Lorg/apache/poi/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    .line 116
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/poi/hpsf/Variant;-><init>()V

    return-void
.end method

.method public static codepageToEncoding(I)Ljava/lang/String;
    .locals 3
    .param p0, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 278
    if-gtz p0, :cond_0

    .line 279
    new-instance v0, Ljava/io/UnsupportedEncodingException;

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Codepage number may not be "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 279
    invoke-direct {v0, v1}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "cp"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 284
    :sswitch_0
    const-string/jumbo v0, "UTF-16"

    goto :goto_0

    .line 286
    :sswitch_1
    const-string/jumbo v0, "UTF-16BE"

    goto :goto_0

    .line 288
    :sswitch_2
    const-string/jumbo v0, "UTF-8"

    goto :goto_0

    .line 290
    :sswitch_3
    const-string/jumbo v0, "cp037"

    goto :goto_0

    .line 292
    :sswitch_4
    const-string/jumbo v0, "GBK"

    goto :goto_0

    .line 294
    :sswitch_5
    const-string/jumbo v0, "ms949"

    goto :goto_0

    .line 296
    :sswitch_6
    const-string/jumbo v0, "windows-1250"

    goto :goto_0

    .line 298
    :sswitch_7
    const-string/jumbo v0, "windows-1251"

    goto :goto_0

    .line 300
    :sswitch_8
    const-string/jumbo v0, "windows-1252"

    goto :goto_0

    .line 302
    :sswitch_9
    const-string/jumbo v0, "windows-1253"

    goto :goto_0

    .line 304
    :sswitch_a
    const-string/jumbo v0, "windows-1254"

    goto :goto_0

    .line 306
    :sswitch_b
    const-string/jumbo v0, "windows-1255"

    goto :goto_0

    .line 308
    :sswitch_c
    const-string/jumbo v0, "windows-1256"

    goto :goto_0

    .line 310
    :sswitch_d
    const-string/jumbo v0, "windows-1257"

    goto :goto_0

    .line 312
    :sswitch_e
    const-string/jumbo v0, "windows-1258"

    goto :goto_0

    .line 314
    :sswitch_f
    const-string/jumbo v0, "johab"

    goto :goto_0

    .line 316
    :sswitch_10
    const-string/jumbo v0, "MacRoman"

    goto :goto_0

    .line 318
    :sswitch_11
    const-string/jumbo v0, "SJIS"

    goto :goto_0

    .line 320
    :sswitch_12
    const-string/jumbo v0, "Big5"

    goto :goto_0

    .line 322
    :sswitch_13
    const-string/jumbo v0, "EUC-KR"

    goto :goto_0

    .line 324
    :sswitch_14
    const-string/jumbo v0, "MacArabic"

    goto :goto_0

    .line 326
    :sswitch_15
    const-string/jumbo v0, "MacHebrew"

    goto :goto_0

    .line 328
    :sswitch_16
    const-string/jumbo v0, "MacGreek"

    goto :goto_0

    .line 330
    :sswitch_17
    const-string/jumbo v0, "MacCyrillic"

    goto :goto_0

    .line 332
    :sswitch_18
    const-string/jumbo v0, "EUC_CN"

    goto :goto_0

    .line 334
    :sswitch_19
    const-string/jumbo v0, "MacRomania"

    goto :goto_0

    .line 336
    :sswitch_1a
    const-string/jumbo v0, "MacUkraine"

    goto :goto_0

    .line 338
    :sswitch_1b
    const-string/jumbo v0, "MacThai"

    goto :goto_0

    .line 340
    :sswitch_1c
    const-string/jumbo v0, "MacCentralEurope"

    goto :goto_0

    .line 342
    :sswitch_1d
    const-string/jumbo v0, "MacIceland"

    goto :goto_0

    .line 344
    :sswitch_1e
    const-string/jumbo v0, "MacTurkish"

    goto :goto_0

    .line 346
    :sswitch_1f
    const-string/jumbo v0, "MacCroatian"

    goto :goto_0

    .line 349
    :sswitch_20
    const-string/jumbo v0, "US-ASCII"

    goto/16 :goto_0

    .line 351
    :sswitch_21
    const-string/jumbo v0, "KOI8-R"

    goto/16 :goto_0

    .line 353
    :sswitch_22
    const-string/jumbo v0, "ISO-8859-1"

    goto/16 :goto_0

    .line 355
    :sswitch_23
    const-string/jumbo v0, "ISO-8859-2"

    goto/16 :goto_0

    .line 357
    :sswitch_24
    const-string/jumbo v0, "ISO-8859-3"

    goto/16 :goto_0

    .line 359
    :sswitch_25
    const-string/jumbo v0, "ISO-8859-4"

    goto/16 :goto_0

    .line 361
    :sswitch_26
    const-string/jumbo v0, "ISO-8859-5"

    goto/16 :goto_0

    .line 363
    :sswitch_27
    const-string/jumbo v0, "ISO-8859-6"

    goto/16 :goto_0

    .line 365
    :sswitch_28
    const-string/jumbo v0, "ISO-8859-7"

    goto/16 :goto_0

    .line 367
    :sswitch_29
    const-string/jumbo v0, "ISO-8859-8"

    goto/16 :goto_0

    .line 369
    :sswitch_2a
    const-string/jumbo v0, "ISO-8859-9"

    goto/16 :goto_0

    .line 373
    :sswitch_2b
    const-string/jumbo v0, "ISO-2022-JP"

    goto/16 :goto_0

    .line 375
    :sswitch_2c
    const-string/jumbo v0, "ISO-2022-KR"

    goto/16 :goto_0

    .line 377
    :sswitch_2d
    const-string/jumbo v0, "EUC-JP"

    goto/16 :goto_0

    .line 379
    :sswitch_2e
    const-string/jumbo v0, "EUC-KR"

    goto/16 :goto_0

    .line 381
    :sswitch_2f
    const-string/jumbo v0, "GB2312"

    goto/16 :goto_0

    .line 383
    :sswitch_30
    const-string/jumbo v0, "GB18030"

    goto/16 :goto_0

    .line 385
    :sswitch_31
    const-string/jumbo v0, "SJIS"

    goto/16 :goto_0

    .line 281
    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_3
        0x3a4 -> :sswitch_31
        0x3a8 -> :sswitch_4
        0x3b5 -> :sswitch_5
        0x4b0 -> :sswitch_0
        0x4b1 -> :sswitch_1
        0x4e2 -> :sswitch_6
        0x4e3 -> :sswitch_7
        0x4e4 -> :sswitch_8
        0x4e5 -> :sswitch_9
        0x4e6 -> :sswitch_a
        0x4e7 -> :sswitch_b
        0x4e8 -> :sswitch_c
        0x4e9 -> :sswitch_d
        0x4ea -> :sswitch_e
        0x551 -> :sswitch_f
        0x2710 -> :sswitch_10
        0x2711 -> :sswitch_11
        0x2712 -> :sswitch_12
        0x2713 -> :sswitch_13
        0x2714 -> :sswitch_14
        0x2715 -> :sswitch_15
        0x2716 -> :sswitch_16
        0x2717 -> :sswitch_17
        0x2718 -> :sswitch_18
        0x271a -> :sswitch_19
        0x2721 -> :sswitch_1a
        0x2725 -> :sswitch_1b
        0x272d -> :sswitch_1c
        0x275f -> :sswitch_1d
        0x2761 -> :sswitch_1e
        0x2762 -> :sswitch_1f
        0x4e9f -> :sswitch_20
        0x5182 -> :sswitch_21
        0x6faf -> :sswitch_22
        0x6fb0 -> :sswitch_23
        0x6fb1 -> :sswitch_24
        0x6fb2 -> :sswitch_25
        0x6fb3 -> :sswitch_26
        0x6fb4 -> :sswitch_27
        0x6fb5 -> :sswitch_28
        0x6fb6 -> :sswitch_29
        0x6fb7 -> :sswitch_2a
        0xc42c -> :sswitch_2b
        0xc42d -> :sswitch_2b
        0xc42e -> :sswitch_2b
        0xc431 -> :sswitch_2c
        0xcadc -> :sswitch_2d
        0xcaed -> :sswitch_2e
        0xcec8 -> :sswitch_2f
        0xd698 -> :sswitch_30
        0xfde8 -> :sswitch_20
        0xfde9 -> :sswitch_2
    .end sparse-switch
.end method

.method public static isLogUnsupportedTypes()Z
    .locals 1

    .prologue
    .line 75
    sget-boolean v0, Lorg/apache/poi/hpsf/VariantSupport;->logUnsupportedTypes:Z

    return v0
.end method

.method public static read([BIIJI)Ljava/lang/Object;
    .locals 15
    .param p0, "src"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "type"    # J
    .param p5, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/ReadingNotSupportedException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v8, Lorg/apache/poi/hpsf/TypedPropertyValue;

    .line 162
    move-wide/from16 v0, p3

    long-to-int v11, v0

    const/4 v12, 0x0

    .line 161
    invoke-direct {v8, v11, v12}, Lorg/apache/poi/hpsf/TypedPropertyValue;-><init>(ILjava/lang/Object;)V

    .line 166
    .local v8, "typedPropertyValue":Lorg/apache/poi/hpsf/TypedPropertyValue;
    :try_start_0
    move/from16 v0, p1

    invoke-virtual {v8, p0, v0}, Lorg/apache/poi/hpsf/TypedPropertyValue;->readValue([BI)I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 176
    .local v9, "unpadded":I
    move-wide/from16 v0, p3

    long-to-int v11, v0

    sparse-switch v11, :sswitch_data_0

    .line 254
    new-array v10, v9, [B

    .line 255
    .local v10, "v":[B
    const/4 v11, 0x0

    move/from16 v0, p1

    invoke-static {p0, v0, v10, v11, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 256
    new-instance v11, Lorg/apache/poi/hpsf/ReadingNotSupportedException;

    move-wide/from16 v0, p3

    invoke-direct {v11, v0, v1, v10}, Lorg/apache/poi/hpsf/ReadingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v11

    .line 168
    .end local v9    # "unpadded":I
    .end local v10    # "v":[B
    :catch_0
    move-exception v4

    .line 170
    .local v4, "exc":Ljava/lang/UnsupportedOperationException;
    array-length v11, p0

    sub-int v11, v11, p1

    move/from16 v0, p2

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 171
    .local v6, "propLength":I
    new-array v10, v6, [B

    .line 172
    .restart local v10    # "v":[B
    const/4 v11, 0x0

    move/from16 v0, p1

    invoke-static {p0, v0, v10, v11, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    new-instance v11, Lorg/apache/poi/hpsf/ReadingNotSupportedException;

    move-wide/from16 v0, p3

    invoke-direct {v11, v0, v1, v10}, Lorg/apache/poi/hpsf/ReadingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v11

    .line 188
    .end local v4    # "exc":Ljava/lang/UnsupportedOperationException;
    .end local v6    # "propLength":I
    .end local v10    # "v":[B
    .restart local v9    # "unpadded":I
    :sswitch_0
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v11

    .line 245
    :goto_0
    return-object v11

    .line 196
    :sswitch_1
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Short;

    .line 197
    invoke-virtual {v11}, Ljava/lang/Short;->intValue()I

    move-result v11

    .line 196
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    goto :goto_0

    .line 201
    :sswitch_2
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hpsf/Filetime;

    .line 202
    .local v5, "filetime":Lorg/apache/poi/hpsf/Filetime;
    invoke-virtual {v5}, Lorg/apache/poi/hpsf/Filetime;->getHigh()J

    move-result-wide v12

    long-to-int v11, v12

    .line 203
    invoke-virtual {v5}, Lorg/apache/poi/hpsf/Filetime;->getLow()J

    move-result-wide v12

    long-to-int v12, v12

    .line 202
    invoke-static {v11, v12}, Lorg/apache/poi/hpsf/Util;->filetimeToDate(II)Ljava/util/Date;

    move-result-object v11

    goto :goto_0

    .line 208
    .end local v5    # "filetime":Lorg/apache/poi/hpsf/Filetime;
    :sswitch_3
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v7

    .line 207
    check-cast v7, Lorg/apache/poi/hpsf/CodePageString;

    .line 209
    .local v7, "string":Lorg/apache/poi/hpsf/CodePageString;
    move/from16 v0, p5

    invoke-virtual {v7, v0}, Lorg/apache/poi/hpsf/CodePageString;->getJavaValue(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 214
    .end local v7    # "string":Lorg/apache/poi/hpsf/CodePageString;
    :sswitch_4
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v7

    .line 213
    check-cast v7, Lorg/apache/poi/hpsf/UnicodeString;

    .line 215
    .local v7, "string":Lorg/apache/poi/hpsf/UnicodeString;
    invoke-virtual {v7}, Lorg/apache/poi/hpsf/UnicodeString;->toJavaString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 238
    .end local v7    # "string":Lorg/apache/poi/hpsf/UnicodeString;
    :sswitch_5
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 237
    check-cast v3, Lorg/apache/poi/hpsf/ClipboardData;

    .line 239
    .local v3, "clipboardData":Lorg/apache/poi/hpsf/ClipboardData;
    invoke-virtual {v3}, Lorg/apache/poi/hpsf/ClipboardData;->toByteArray()[B

    move-result-object v11

    goto :goto_0

    .line 244
    .end local v3    # "clipboardData":Lorg/apache/poi/hpsf/ClipboardData;
    :sswitch_6
    invoke-virtual {v8}, Lorg/apache/poi/hpsf/TypedPropertyValue;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hpsf/VariantBool;

    .line 245
    .local v2, "bool":Lorg/apache/poi/hpsf/VariantBool;
    invoke-virtual {v2}, Lorg/apache/poi/hpsf/VariantBool;->getValue()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    goto :goto_0

    .line 176
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x5 -> :sswitch_0
        0xb -> :sswitch_6
        0x14 -> :sswitch_0
        0x1e -> :sswitch_3
        0x1f -> :sswitch_4
        0x40 -> :sswitch_2
        0x47 -> :sswitch_5
    .end sparse-switch
.end method

.method public static setLogUnsupportedTypes(Z)V
    .locals 0
    .param p0, "logUnsupportedTypes"    # Z

    .prologue
    .line 63
    sput-boolean p0, Lorg/apache/poi/hpsf/VariantSupport;->logUnsupportedTypes:Z

    .line 64
    return-void
.end method

.method public static write(Ljava/io/OutputStream;JLjava/lang/Object;I)I
    .locals 23
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "type"    # J
    .param p3, "value"    # Ljava/lang/Object;
    .param p4, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 415
    const/4 v12, 0x0

    .line 416
    .local v12, "length":I
    move-wide/from16 v0, p1

    long-to-int v0, v0

    move/from16 v17, v0

    sparse-switch v17, :sswitch_data_0

    .line 516
    move-object/from16 v0, p3

    instance-of v0, v0, [B

    move/from16 v17, v0

    if-eqz v17, :cond_3

    move-object/from16 v4, p3

    .line 518
    check-cast v4, [B

    .line 519
    .local v4, "b":[B
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    .line 520
    array-length v12, v4

    .line 522
    new-instance v17, Lorg/apache/poi/hpsf/WritingNotSupportedException;

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    .line 521
    invoke-static/range {v17 .. v17}, Lorg/apache/poi/hpsf/VariantSupport;->writeUnsupportedTypeMessage(Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;)V

    .line 531
    .end local v4    # "b":[B
    .end local p3    # "value":Ljava/lang/Object;
    :goto_0
    and-int/lit8 v17, v12, 0x3

    if-nez v17, :cond_4

    .line 537
    return v12

    .line 420
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p3, Ljava/lang/Boolean;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 422
    const/16 v17, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 423
    const/16 v17, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 430
    :goto_1
    add-int/lit8 v12, v12, 0x2

    .line 431
    goto :goto_0

    .line 427
    :cond_0
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 428
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    .line 435
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_1
    new-instance v5, Lorg/apache/poi/hpsf/CodePageString;

    check-cast p3, Ljava/lang/String;

    .end local p3    # "value":Ljava/lang/Object;
    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {v5, v0, v1}, Lorg/apache/poi/hpsf/CodePageString;-><init>(Ljava/lang/String;I)V

    .line 437
    .local v5, "codePageString":Lorg/apache/poi/hpsf/CodePageString;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lorg/apache/poi/hpsf/CodePageString;->write(Ljava/io/OutputStream;)I

    move-result v17

    add-int v12, v12, v17

    .line 438
    goto :goto_0

    .end local v5    # "codePageString":Lorg/apache/poi/hpsf/CodePageString;
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_2
    move-object/from16 v17, p3

    .line 442
    check-cast v17, Ljava/lang/String;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v15, v17, 0x1

    .line 443
    .local v15, "nrOfChars":I
    int-to-long v0, v15

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v17

    add-int v12, v12, v17

    .line 444
    check-cast p3, Ljava/lang/String;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v16

    .line 445
    .local v16, "s":[C
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v11, v0, :cond_1

    .line 456
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 457
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 458
    add-int/lit8 v12, v12, 0x2

    .line 459
    goto/16 :goto_0

    .line 447
    :cond_1
    aget-char v17, v16, v11

    const v18, 0xff00

    and-int v17, v17, v18

    shr-int/lit8 v9, v17, 0x8

    .line 448
    .local v9, "high":I
    aget-char v17, v16, v11

    move/from16 v0, v17

    and-int/lit16 v13, v0, 0xff

    .line 449
    .local v13, "low":I
    int-to-byte v10, v9

    .line 450
    .local v10, "highb":B
    int-to-byte v14, v13

    .line 451
    .local v14, "lowb":B
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Ljava/io/OutputStream;->write(I)V

    .line 452
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Ljava/io/OutputStream;->write(I)V

    .line 453
    add-int/lit8 v12, v12, 0x2

    .line 445
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .end local v9    # "high":I
    .end local v10    # "highb":B
    .end local v11    # "i":I
    .end local v13    # "low":I
    .end local v14    # "lowb":B
    .end local v15    # "nrOfChars":I
    .end local v16    # "s":[C
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_3
    move-object/from16 v4, p3

    .line 463
    check-cast v4, [B

    .line 464
    .restart local v4    # "b":[B
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    .line 465
    array-length v12, v4

    .line 466
    goto/16 :goto_0

    .line 470
    .end local v4    # "b":[B
    :sswitch_4
    const-wide/16 v18, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v17

    add-int v12, v12, v17

    .line 471
    goto/16 :goto_0

    .line 476
    :sswitch_5
    check-cast p3, Ljava/lang/Integer;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->shortValue()S

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    move-result v17

    add-int v12, v12, v17

    .line 477
    goto/16 :goto_0

    .line 481
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_6
    move-object/from16 v0, p3

    instance-of v0, v0, Ljava/lang/Integer;

    move/from16 v17, v0

    if-nez v17, :cond_2

    .line 483
    new-instance v17, Ljava/lang/ClassCastException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string/jumbo v19, "Could not cast an object to "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 484
    const-class v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ": "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 485
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 486
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 483
    invoke-direct/range {v17 .. v18}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 489
    :cond_2
    check-cast p3, Ljava/lang/Integer;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    move-result v17

    add-int v12, v12, v17

    .line 490
    goto/16 :goto_0

    .line 494
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_7
    check-cast p3, Ljava/lang/Long;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;J)I

    move-result v17

    add-int v12, v12, v17

    .line 495
    goto/16 :goto_0

    .line 500
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_8
    check-cast p3, Ljava/lang/Double;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;D)I

    move-result v17

    add-int v12, v12, v17

    .line 501
    goto/16 :goto_0

    .line 505
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_9
    check-cast p3, Ljava/util/Date;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hpsf/Util;->dateToFileTime(Ljava/util/Date;)J

    move-result-wide v6

    .line 506
    .local v6, "filetime":J
    const/16 v17, 0x20

    shr-long v18, v6, v17

    const-wide v20, 0xffffffffL

    and-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v9, v0

    .line 507
    .restart local v9    # "high":I
    const-wide v18, 0xffffffffL

    and-long v18, v18, v6

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 508
    .restart local v13    # "low":I
    new-instance v8, Lorg/apache/poi/hpsf/Filetime;

    invoke-direct {v8, v13, v9}, Lorg/apache/poi/hpsf/Filetime;-><init>(II)V

    .line 509
    .local v8, "filetimeValue":Lorg/apache/poi/hpsf/Filetime;
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Lorg/apache/poi/hpsf/Filetime;->write(Ljava/io/OutputStream;)I

    move-result v17

    add-int v12, v12, v17

    .line 510
    goto/16 :goto_0

    .line 525
    .end local v6    # "filetime":J
    .end local v8    # "filetimeValue":Lorg/apache/poi/hpsf/Filetime;
    .end local v9    # "high":I
    .end local v13    # "low":I
    .restart local p3    # "value":Ljava/lang/Object;
    :cond_3
    new-instance v17, Lorg/apache/poi/hpsf/WritingNotSupportedException;

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v17

    .line 533
    .end local p3    # "value":Ljava/lang/Object;
    :cond_4
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 534
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 416
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x2 -> :sswitch_5
        0x3 -> :sswitch_6
        0x5 -> :sswitch_8
        0xb -> :sswitch_0
        0x14 -> :sswitch_7
        0x1e -> :sswitch_1
        0x1f -> :sswitch_2
        0x40 -> :sswitch_9
        0x47 -> :sswitch_3
    .end sparse-switch
.end method

.method protected static writeUnsupportedTypeMessage(Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;)V
    .locals 4
    .param p0, "ex"    # Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;

    .prologue
    .line 96
    invoke-static {}, Lorg/apache/poi/hpsf/VariantSupport;->isLogUnsupportedTypes()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    sget-object v1, Lorg/apache/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    if-nez v1, :cond_0

    .line 99
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    sput-object v1, Lorg/apache/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    .line 100
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;->getVariantType()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 101
    .local v0, "vt":Ljava/lang/Long;
    sget-object v1, Lorg/apache/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    sget-object v1, Lorg/apache/poi/hpsf/VariantSupport;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x7

    invoke-virtual {p0}, Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 104
    sget-object v1, Lorg/apache/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    .end local v0    # "vt":Ljava/lang/Long;
    :cond_1
    return-void
.end method


# virtual methods
.method public isSupportedType(I)Z
    .locals 2
    .param p1, "variantType"    # I

    .prologue
    .line 132
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lorg/apache/poi/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 135
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 133
    :cond_0
    sget-object v1, Lorg/apache/poi/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_1

    .line 134
    const/4 v1, 0x1

    goto :goto_1

    .line 132
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
