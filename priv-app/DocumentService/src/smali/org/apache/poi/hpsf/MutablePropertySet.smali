.class public Lorg/apache/poi/hpsf/MutablePropertySet;
.super Lorg/apache/poi/hpsf/PropertySet;
.source "MutablePropertySet.java"


# instance fields
.field private final OFFSET_HEADER:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hpsf/PropertySet;-><init>()V

    .line 106
    sget-object v0, Lorg/apache/poi/hpsf/MutablePropertySet;->BYTE_ORDER_ASSERTION:[B

    array-length v0, v0

    .line 107
    sget-object v1, Lorg/apache/poi/hpsf/MutablePropertySet;->FORMAT_ASSERTION:[B

    array-length v1, v1

    .line 106
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->OFFSET_HEADER:I

    .line 56
    sget-object v0, Lorg/apache/poi/hpsf/MutablePropertySet;->BYTE_ORDER_ASSERTION:[B

    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getUShort([B)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->byteOrder:I

    .line 59
    sget-object v0, Lorg/apache/poi/hpsf/MutablePropertySet;->FORMAT_ASSERTION:[B

    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getUShort([B)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->format:I

    .line 63
    const v0, 0x20a04

    iput v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->osVersion:I

    .line 66
    new-instance v0, Lorg/apache/poi/hpsf/ClassID;

    invoke-direct {v0}, Lorg/apache/poi/hpsf/ClassID;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->classID:Lorg/apache/poi/hpsf/ClassID;

    .line 70
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 71
    iget-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    new-instance v1, Lorg/apache/poi/hpsf/MutableSection;

    invoke-direct {v1}, Lorg/apache/poi/hpsf/MutableSection;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hpsf/PropertySet;)V
    .locals 4
    .param p1, "ps"    # Lorg/apache/poi/hpsf/PropertySet;

    .prologue
    .line 84
    invoke-direct {p0}, Lorg/apache/poi/hpsf/PropertySet;-><init>()V

    .line 106
    sget-object v2, Lorg/apache/poi/hpsf/MutablePropertySet;->BYTE_ORDER_ASSERTION:[B

    array-length v2, v2

    .line 107
    sget-object v3, Lorg/apache/poi/hpsf/MutablePropertySet;->FORMAT_ASSERTION:[B

    array-length v3, v3

    .line 106
    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x10

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->OFFSET_HEADER:I

    .line 86
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/PropertySet;->getByteOrder()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->byteOrder:I

    .line 87
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/PropertySet;->getFormat()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->format:I

    .line 88
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/PropertySet;->getOSVersion()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->osVersion:I

    .line 89
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/PropertySet;->getClassID()Lorg/apache/poi/hpsf/ClassID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/poi/hpsf/MutablePropertySet;->setClassID(Lorg/apache/poi/hpsf/ClassID;)V

    .line 90
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/MutablePropertySet;->clearSections()V

    .line 91
    iget-object v2, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    if-nez v2, :cond_0

    .line 92
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 93
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/PropertySet;->getSections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 98
    return-void

    .line 95
    :cond_1
    new-instance v1, Lorg/apache/poi/hpsf/MutableSection;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hpsf/Section;

    invoke-direct {v1, v2}, Lorg/apache/poi/hpsf/MutableSection;-><init>(Lorg/apache/poi/hpsf/Section;)V

    .line 96
    .local v1, "s":Lorg/apache/poi/hpsf/MutableSection;
    invoke-virtual {p0, v1}, Lorg/apache/poi/hpsf/MutablePropertySet;->addSection(Lorg/apache/poi/hpsf/Section;)V

    goto :goto_0
.end method


# virtual methods
.method public addSection(Lorg/apache/poi/hpsf/Section;)V
    .locals 1
    .param p1, "section"    # Lorg/apache/poi/hpsf/Section;

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 186
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public clearSections()V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 171
    return-void
.end method

.method public setByteOrder(I)V
    .locals 0
    .param p1, "byteOrder"    # I

    .prologue
    .line 121
    iput p1, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->byteOrder:I

    .line 122
    return-void
.end method

.method public setClassID(Lorg/apache/poi/hpsf/ClassID;)V
    .locals 0
    .param p1, "classID"    # Lorg/apache/poi/hpsf/ClassID;

    .prologue
    .line 160
    iput-object p1, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->classID:Lorg/apache/poi/hpsf/ClassID;

    .line 161
    return-void
.end method

.method public setFormat(I)V
    .locals 0
    .param p1, "format"    # I

    .prologue
    .line 133
    iput p1, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->format:I

    .line 134
    return-void
.end method

.method public setOSVersion(I)V
    .locals 0
    .param p1, "osVersion"    # I

    .prologue
    .line 145
    iput p1, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->osVersion:I

    .line 146
    return-void
.end method

.method public toInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 270
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 271
    .local v0, "psStream":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/MutablePropertySet;->write(Ljava/io/OutputStream;)V

    .line 272
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 273
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 274
    .local v1, "streamData":[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v2
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 12
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v9, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    .line 205
    .local v5, "nrSections":I
    const/4 v4, 0x0

    .line 208
    .local v4, "length":I
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/MutablePropertySet;->getByteOrder()I

    move-result v9

    int-to-short v9, v9

    invoke-static {p1, v9}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    move-result v9

    add-int/2addr v4, v9

    .line 209
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/MutablePropertySet;->getFormat()I

    move-result v9

    int-to-short v9, v9

    invoke-static {p1, v9}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    move-result v9

    add-int/2addr v4, v9

    .line 210
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/MutablePropertySet;->getOSVersion()I

    move-result v9

    invoke-static {p1, v9}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    move-result v9

    add-int/2addr v4, v9

    .line 211
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/MutablePropertySet;->getClassID()Lorg/apache/poi/hpsf/ClassID;

    move-result-object v9

    invoke-static {p1, v9}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;Lorg/apache/poi/hpsf/ClassID;)I

    move-result v9

    add-int/2addr v4, v9

    .line 212
    invoke-static {p1, v5}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    move-result v9

    add-int/2addr v4, v9

    .line 213
    iget v6, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->OFFSET_HEADER:I

    .line 218
    .local v6, "offset":I
    mul-int/lit8 v9, v5, 0x14

    add-int/2addr v6, v9

    .line 219
    move v8, v6

    .line 220
    .local v8, "sectionsBegin":I
    iget-object v9, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    .local v3, "i":Ljava/util/ListIterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 243
    move v6, v8

    .line 244
    iget-object v9, p0, Lorg/apache/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 249
    return-void

    .line 222
    :cond_0
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hpsf/MutableSection;

    .line 223
    .local v7, "s":Lorg/apache/poi/hpsf/MutableSection;
    invoke-virtual {v7}, Lorg/apache/poi/hpsf/MutableSection;->getFormatID()Lorg/apache/poi/hpsf/ClassID;

    move-result-object v2

    .line 224
    .local v2, "formatID":Lorg/apache/poi/hpsf/ClassID;
    if-nez v2, :cond_1

    .line 225
    new-instance v9, Lorg/apache/poi/hpsf/NoFormatIDException;

    invoke-direct {v9}, Lorg/apache/poi/hpsf/NoFormatIDException;-><init>()V

    throw v9

    .line 226
    :cond_1
    invoke-virtual {v7}, Lorg/apache/poi/hpsf/MutableSection;->getFormatID()Lorg/apache/poi/hpsf/ClassID;

    move-result-object v9

    invoke-static {p1, v9}, Lorg/apache/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;Lorg/apache/poi/hpsf/ClassID;)I

    move-result v9

    add-int/2addr v4, v9

    .line 227
    int-to-long v10, v6

    invoke-static {p1, v10, v11}, Lorg/apache/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v9

    add-int/2addr v4, v9

    .line 230
    :try_start_0
    invoke-virtual {v7}, Lorg/apache/poi/hpsf/MutableSection;->getSize()I
    :try_end_0
    .catch Lorg/apache/poi/hpsf/HPSFRuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    add-int/2addr v6, v9

    goto :goto_0

    .line 232
    :catch_0
    move-exception v1

    .line 234
    .local v1, "ex":Lorg/apache/poi/hpsf/HPSFRuntimeException;
    invoke-virtual {v1}, Lorg/apache/poi/hpsf/HPSFRuntimeException;->getReason()Ljava/lang/Throwable;

    move-result-object v0

    .line 235
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v9, v0, Ljava/io/UnsupportedEncodingException;

    if-eqz v9, :cond_2

    .line 236
    new-instance v9, Lorg/apache/poi/hpsf/IllegalPropertySetDataException;

    invoke-direct {v9, v0}, Lorg/apache/poi/hpsf/IllegalPropertySetDataException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 238
    :cond_2
    throw v1

    .line 246
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "ex":Lorg/apache/poi/hpsf/HPSFRuntimeException;
    .end local v2    # "formatID":Lorg/apache/poi/hpsf/ClassID;
    .end local v7    # "s":Lorg/apache/poi/hpsf/MutableSection;
    :cond_3
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hpsf/MutableSection;

    .line 247
    .restart local v7    # "s":Lorg/apache/poi/hpsf/MutableSection;
    invoke-virtual {v7, p1}, Lorg/apache/poi/hpsf/MutableSection;->write(Ljava/io/OutputStream;)I

    move-result v9

    add-int/2addr v6, v9

    goto :goto_1
.end method

.method public write(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/lang/String;)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    :try_start_0
    invoke-interface {p1, p2}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v0

    .line 294
    .local v0, "e":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/Entry;->delete()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v0    # "e":Lorg/apache/poi/poifs/filesystem/Entry;
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/MutablePropertySet;->toInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 302
    return-void

    .line 296
    :catch_0
    move-exception v1

    goto :goto_0
.end method
