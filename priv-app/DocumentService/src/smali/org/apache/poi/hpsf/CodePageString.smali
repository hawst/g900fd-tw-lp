.class Lorg/apache/poi/hpsf/CodePageString;
.super Ljava/lang/Object;
.source "CodePageString.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _value:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/poi/hpsf/CodePageString;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 32
    sput-object v0, Lorg/apache/poi/hpsf/CodePageString;->logger:Lorg/apache/poi/util/POILogger;

    .line 33
    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hpsf/CodePageString;->setJavaValue(Ljava/lang/String;I)V

    .line 177
    return-void
.end method

.method constructor <init>([BI)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    move v0, p2

    .line 157
    .local v0, "offset":I
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 158
    .local v1, "size":I
    add-int/lit8 v0, v0, 0x4

    .line 160
    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    .line 161
    iget-object v2, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    add-int/lit8 v3, v1, -0x1

    aget-byte v2, v2, v3

    if-eqz v2, :cond_0

    .line 165
    sget-object v2, Lorg/apache/poi/hpsf/CodePageString;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "CodePageString started at offset #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 166
    const-string/jumbo v5, " is not NULL-terminated"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 165
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 171
    :cond_0
    return-void
.end method

.method private static codepageToEncoding(I)Ljava/lang/String;
    .locals 3
    .param p0, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 38
    if-gtz p0, :cond_0

    .line 39
    new-instance v0, Ljava/io/UnsupportedEncodingException;

    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Codepage number may not be "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-direct {v0, v1}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "cp"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 44
    :sswitch_0
    const-string/jumbo v0, "UTF-16"

    goto :goto_0

    .line 46
    :sswitch_1
    const-string/jumbo v0, "UTF-16BE"

    goto :goto_0

    .line 48
    :sswitch_2
    const-string/jumbo v0, "UTF-8"

    goto :goto_0

    .line 50
    :sswitch_3
    const-string/jumbo v0, "cp037"

    goto :goto_0

    .line 52
    :sswitch_4
    const-string/jumbo v0, "GBK"

    goto :goto_0

    .line 54
    :sswitch_5
    const-string/jumbo v0, "ms949"

    goto :goto_0

    .line 56
    :sswitch_6
    const-string/jumbo v0, "windows-1250"

    goto :goto_0

    .line 58
    :sswitch_7
    const-string/jumbo v0, "windows-1251"

    goto :goto_0

    .line 60
    :sswitch_8
    const-string/jumbo v0, "windows-1252"

    goto :goto_0

    .line 62
    :sswitch_9
    const-string/jumbo v0, "windows-1253"

    goto :goto_0

    .line 64
    :sswitch_a
    const-string/jumbo v0, "windows-1254"

    goto :goto_0

    .line 66
    :sswitch_b
    const-string/jumbo v0, "windows-1255"

    goto :goto_0

    .line 68
    :sswitch_c
    const-string/jumbo v0, "windows-1256"

    goto :goto_0

    .line 70
    :sswitch_d
    const-string/jumbo v0, "windows-1257"

    goto :goto_0

    .line 72
    :sswitch_e
    const-string/jumbo v0, "windows-1258"

    goto :goto_0

    .line 74
    :sswitch_f
    const-string/jumbo v0, "johab"

    goto :goto_0

    .line 76
    :sswitch_10
    const-string/jumbo v0, "MacRoman"

    goto :goto_0

    .line 78
    :sswitch_11
    const-string/jumbo v0, "SJIS"

    goto :goto_0

    .line 80
    :sswitch_12
    const-string/jumbo v0, "Big5"

    goto :goto_0

    .line 82
    :sswitch_13
    const-string/jumbo v0, "EUC-KR"

    goto :goto_0

    .line 84
    :sswitch_14
    const-string/jumbo v0, "MacArabic"

    goto :goto_0

    .line 86
    :sswitch_15
    const-string/jumbo v0, "MacHebrew"

    goto :goto_0

    .line 88
    :sswitch_16
    const-string/jumbo v0, "MacGreek"

    goto :goto_0

    .line 90
    :sswitch_17
    const-string/jumbo v0, "MacCyrillic"

    goto :goto_0

    .line 92
    :sswitch_18
    const-string/jumbo v0, "EUC_CN"

    goto :goto_0

    .line 94
    :sswitch_19
    const-string/jumbo v0, "MacRomania"

    goto :goto_0

    .line 96
    :sswitch_1a
    const-string/jumbo v0, "MacUkraine"

    goto :goto_0

    .line 98
    :sswitch_1b
    const-string/jumbo v0, "MacThai"

    goto :goto_0

    .line 100
    :sswitch_1c
    const-string/jumbo v0, "MacCentralEurope"

    goto :goto_0

    .line 102
    :sswitch_1d
    const-string/jumbo v0, "MacIceland"

    goto :goto_0

    .line 104
    :sswitch_1e
    const-string/jumbo v0, "MacTurkish"

    goto :goto_0

    .line 106
    :sswitch_1f
    const-string/jumbo v0, "MacCroatian"

    goto :goto_0

    .line 109
    :sswitch_20
    const-string/jumbo v0, "US-ASCII"

    goto/16 :goto_0

    .line 111
    :sswitch_21
    const-string/jumbo v0, "KOI8-R"

    goto/16 :goto_0

    .line 113
    :sswitch_22
    const-string/jumbo v0, "ISO-8859-1"

    goto/16 :goto_0

    .line 115
    :sswitch_23
    const-string/jumbo v0, "ISO-8859-2"

    goto/16 :goto_0

    .line 117
    :sswitch_24
    const-string/jumbo v0, "ISO-8859-3"

    goto/16 :goto_0

    .line 119
    :sswitch_25
    const-string/jumbo v0, "ISO-8859-4"

    goto/16 :goto_0

    .line 121
    :sswitch_26
    const-string/jumbo v0, "ISO-8859-5"

    goto/16 :goto_0

    .line 123
    :sswitch_27
    const-string/jumbo v0, "ISO-8859-6"

    goto/16 :goto_0

    .line 125
    :sswitch_28
    const-string/jumbo v0, "ISO-8859-7"

    goto/16 :goto_0

    .line 127
    :sswitch_29
    const-string/jumbo v0, "ISO-8859-8"

    goto/16 :goto_0

    .line 129
    :sswitch_2a
    const-string/jumbo v0, "ISO-8859-9"

    goto/16 :goto_0

    .line 133
    :sswitch_2b
    const-string/jumbo v0, "ISO-2022-JP"

    goto/16 :goto_0

    .line 135
    :sswitch_2c
    const-string/jumbo v0, "ISO-2022-KR"

    goto/16 :goto_0

    .line 137
    :sswitch_2d
    const-string/jumbo v0, "EUC-JP"

    goto/16 :goto_0

    .line 139
    :sswitch_2e
    const-string/jumbo v0, "EUC-KR"

    goto/16 :goto_0

    .line 141
    :sswitch_2f
    const-string/jumbo v0, "GB2312"

    goto/16 :goto_0

    .line 143
    :sswitch_30
    const-string/jumbo v0, "GB18030"

    goto/16 :goto_0

    .line 145
    :sswitch_31
    const-string/jumbo v0, "SJIS"

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_3
        0x3a4 -> :sswitch_31
        0x3a8 -> :sswitch_4
        0x3b5 -> :sswitch_5
        0x4b0 -> :sswitch_0
        0x4b1 -> :sswitch_1
        0x4e2 -> :sswitch_6
        0x4e3 -> :sswitch_7
        0x4e4 -> :sswitch_8
        0x4e5 -> :sswitch_9
        0x4e6 -> :sswitch_a
        0x4e7 -> :sswitch_b
        0x4e8 -> :sswitch_c
        0x4e9 -> :sswitch_d
        0x4ea -> :sswitch_e
        0x551 -> :sswitch_f
        0x2710 -> :sswitch_10
        0x2711 -> :sswitch_11
        0x2712 -> :sswitch_12
        0x2713 -> :sswitch_13
        0x2714 -> :sswitch_14
        0x2715 -> :sswitch_15
        0x2716 -> :sswitch_16
        0x2717 -> :sswitch_17
        0x2718 -> :sswitch_18
        0x271a -> :sswitch_19
        0x2721 -> :sswitch_1a
        0x2725 -> :sswitch_1b
        0x272d -> :sswitch_1c
        0x275f -> :sswitch_1d
        0x2761 -> :sswitch_1e
        0x2762 -> :sswitch_1f
        0x4e9f -> :sswitch_20
        0x5182 -> :sswitch_21
        0x6faf -> :sswitch_22
        0x6fb0 -> :sswitch_23
        0x6fb1 -> :sswitch_24
        0x6fb2 -> :sswitch_25
        0x6fb3 -> :sswitch_26
        0x6fb4 -> :sswitch_27
        0x6fb5 -> :sswitch_28
        0x6fb6 -> :sswitch_29
        0x6fb7 -> :sswitch_2a
        0xc42c -> :sswitch_2b
        0xc42d -> :sswitch_2b
        0xc42e -> :sswitch_2b
        0xc431 -> :sswitch_2c
        0xcadc -> :sswitch_2d
        0xcaed -> :sswitch_2e
        0xcec8 -> :sswitch_2f
        0xd698 -> :sswitch_30
        0xfde8 -> :sswitch_20
        0xfde9 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method getJavaValue(I)Ljava/lang/String;
    .locals 7
    .param p1, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 182
    if-ne p1, v4, :cond_0

    .line 183
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .line 186
    .local v0, "result":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 187
    .local v1, "terminator":I
    if-ne v1, v4, :cond_1

    .line 189
    sget-object v2, Lorg/apache/poi/hpsf/CodePageString;->logger:Lorg/apache/poi/util/POILogger;

    .line 191
    const-string/jumbo v3, "String terminator (\\0) for CodePageString property value not found.Continue without trimming and hope for the best."

    .line 189
    invoke-virtual {v2, v6, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 202
    .end local v0    # "result":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 185
    .end local v1    # "terminator":I
    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    invoke-static {p1}, Lorg/apache/poi/hpsf/CodePageString;->codepageToEncoding(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_0

    .line 195
    .restart local v1    # "terminator":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_2

    .line 197
    sget-object v2, Lorg/apache/poi/hpsf/CodePageString;->logger:Lorg/apache/poi/util/POILogger;

    .line 199
    const-string/jumbo v3, "String terminator (\\0) for CodePageString property value occured before the end of string. Trimming and hope for the best."

    .line 197
    invoke-virtual {v2, v6, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 202
    :cond_2
    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method getSize()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method setJavaValue(Ljava/lang/String;I)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 213
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "\u0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "\u0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-static {p2}, Lorg/apache/poi/hpsf/CodePageString;->codepageToEncoding(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 216
    iput-object v0, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    goto :goto_0
.end method

.method write(Ljava/io/OutputStream;)I
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    array-length v0, v0

    invoke-static {v0, p1}, Lorg/apache/poi/util/LittleEndian;->putInt(ILjava/io/OutputStream;)V

    .line 223
    iget-object v0, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 224
    iget-object v0, p0, Lorg/apache/poi/hpsf/CodePageString;->_value:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method
