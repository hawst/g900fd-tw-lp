.class Lorg/apache/poi/hpsf/ClipboardData;
.super Ljava/lang/Object;
.source "ClipboardData.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _format:I

.field private _value:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/poi/hpsf/ClipboardData;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 30
    sput-object v0, Lorg/apache/poi/hpsf/ClipboardData;->logger:Lorg/apache/poi/util/POILogger;

    .line 31
    return-void
.end method

.method constructor <init>([BI)V
    .locals 7
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    const/4 v6, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1, p2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 40
    .local v0, "size":I
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 42
    sget-object v1, Lorg/apache/poi/hpsf/ClipboardData;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x5

    const-string/jumbo v3, "ClipboardData at offset "

    .line 43
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string/jumbo v5, " size less than 4 bytes (doesn\'t even have format field!). Setting to format == 0 and hope for the best"

    .line 42
    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 46
    iput v6, p0, Lorg/apache/poi/hpsf/ClipboardData;->_format:I

    .line 47
    new-array v1, v6, [B

    iput-object v1, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    .line 54
    :goto_0
    return-void

    .line 51
    :cond_0
    add-int/lit8 v1, p2, 0x4

    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hpsf/ClipboardData;->_format:I

    .line 53
    add-int/lit8 v1, p2, 0x8

    add-int/lit8 v2, v0, -0x4

    .line 52
    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    goto :goto_0
.end method


# virtual methods
.method getSize()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method getValue()[B
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    return-object v0
.end method

.method toByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/ClipboardData;->getSize()I

    move-result v1

    new-array v0, v1, [B

    .line 70
    .local v0, "result":[B
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    .line 69
    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 71
    const/4 v1, 0x4

    iget v2, p0, Lorg/apache/poi/hpsf/ClipboardData;->_format:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 72
    iget-object v1, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    const/16 v2, 0x8

    .line 73
    iget-object v3, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    array-length v3, v3

    .line 72
    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    return-object v0
.end method

.method write(Ljava/io/OutputStream;)I
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    invoke-static {v0, p1}, Lorg/apache/poi/util/LittleEndian;->putInt(ILjava/io/OutputStream;)V

    .line 80
    iget v0, p0, Lorg/apache/poi/hpsf/ClipboardData;->_format:I

    invoke-static {v0, p1}, Lorg/apache/poi/util/LittleEndian;->putInt(ILjava/io/OutputStream;)V

    .line 81
    iget-object v0, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 82
    iget-object v0, p0, Lorg/apache/poi/hpsf/ClipboardData;->_value:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method
