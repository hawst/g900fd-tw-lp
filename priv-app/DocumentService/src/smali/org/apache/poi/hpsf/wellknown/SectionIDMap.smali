.class public Lorg/apache/poi/hpsf/wellknown/SectionIDMap;
.super Ljava/util/HashMap;
.source "SectionIDMap.java"


# static fields
.field public static final DOCUMENT_SUMMARY_INFORMATION_ID:[[B

.field public static final SUMMARY_INFORMATION_ID:[B

.field public static final UNDEFINED:Ljava/lang/String; = "[undefined]"

.field private static defaultMap:Lorg/apache/poi/hpsf/wellknown/SectionIDMap;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/16 v8, -0x2b

    const/4 v7, 0x2

    const/16 v6, 0x10

    const/16 v5, 0x8

    .line 45
    new-array v0, v6, [B

    .line 46
    const/16 v1, -0xe

    aput-byte v1, v0, v4

    const/4 v1, 0x1

    const/16 v2, -0x61

    aput-byte v2, v0, v1

    const/16 v1, -0x7b

    aput-byte v1, v0, v7

    const/4 v1, 0x3

    const/16 v2, -0x20

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    .line 47
    const/16 v2, 0x4f

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, -0x7

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    aput-byte v6, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x68

    aput-byte v2, v0, v1

    .line 48
    const/16 v1, -0x55

    aput-byte v1, v0, v5

    const/16 v1, 0x9

    const/16 v2, -0x6f

    aput-byte v2, v0, v1

    const/16 v1, 0xa

    aput-byte v5, v0, v1

    const/16 v1, 0xc

    .line 49
    const/16 v2, 0x2b

    aput-byte v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x27

    aput-byte v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, -0x4d

    aput-byte v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, -0x27

    aput-byte v2, v0, v1

    .line 44
    sput-object v0, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->SUMMARY_INFORMATION_ID:[B

    .line 57
    new-array v0, v7, [[B

    .line 58
    new-array v1, v6, [B

    .line 59
    aput-byte v8, v1, v4

    const/4 v2, 0x1

    const/16 v3, -0x33

    aput-byte v3, v1, v2

    aput-byte v8, v1, v7

    const/4 v2, 0x3

    aput-byte v7, v1, v2

    const/4 v2, 0x4

    .line 60
    const/16 v3, 0x2e

    aput-byte v3, v1, v2

    const/4 v2, 0x5

    const/16 v3, -0x64

    aput-byte v3, v1, v2

    const/4 v2, 0x6

    aput-byte v6, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    .line 61
    const/16 v2, -0x6d

    aput-byte v2, v1, v5

    const/16 v2, 0x9

    const/16 v3, -0x69

    aput-byte v3, v1, v2

    const/16 v2, 0xa

    aput-byte v5, v1, v2

    const/16 v2, 0xc

    .line 62
    const/16 v3, 0x2b

    aput-byte v3, v1, v2

    const/16 v2, 0xd

    const/16 v3, 0x2c

    aput-byte v3, v1, v2

    const/16 v2, 0xe

    const/4 v3, -0x7

    aput-byte v3, v1, v2

    const/16 v2, 0xf

    const/16 v3, -0x52

    aput-byte v3, v1, v2

    aput-object v1, v0, v4

    const/4 v1, 0x1

    .line 64
    new-array v2, v6, [B

    .line 65
    aput-byte v8, v2, v4

    const/4 v3, 0x1

    const/16 v4, -0x33

    aput-byte v4, v2, v3

    aput-byte v8, v2, v7

    const/4 v3, 0x3

    const/4 v4, 0x5

    aput-byte v4, v2, v3

    const/4 v3, 0x4

    .line 66
    const/16 v4, 0x2e

    aput-byte v4, v2, v3

    const/4 v3, 0x5

    const/16 v4, -0x64

    aput-byte v4, v2, v3

    const/4 v3, 0x6

    aput-byte v6, v2, v3

    const/4 v3, 0x7

    const/16 v4, 0x1b

    aput-byte v4, v2, v3

    .line 67
    const/16 v3, -0x6d

    aput-byte v3, v2, v5

    const/16 v3, 0x9

    const/16 v4, -0x69

    aput-byte v4, v2, v3

    const/16 v3, 0xa

    aput-byte v5, v2, v3

    const/16 v3, 0xc

    .line 68
    const/16 v4, 0x2b

    aput-byte v4, v2, v3

    const/16 v3, 0xd

    const/16 v4, 0x2c

    aput-byte v4, v2, v3

    const/16 v3, 0xe

    const/4 v4, -0x7

    aput-byte v4, v2, v3

    const/16 v3, 0xf

    const/16 v4, -0x52

    aput-byte v4, v2, v3

    aput-object v2, v0, v1

    .line 56
    sput-object v0, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/apache/poi/hpsf/wellknown/SectionIDMap;
    .locals 3

    .prologue
    .line 93
    sget-object v1, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->defaultMap:Lorg/apache/poi/hpsf/wellknown/SectionIDMap;

    if-nez v1, :cond_0

    .line 95
    new-instance v0, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;

    invoke-direct {v0}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;-><init>()V

    .line 96
    .local v0, "m":Lorg/apache/poi/hpsf/wellknown/SectionIDMap;
    sget-object v1, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->SUMMARY_INFORMATION_ID:[B

    .line 97
    invoke-static {}, Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;->getSummaryInformationProperties()Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    move-result-object v2

    .line 96
    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->put([BLorg/apache/poi/hpsf/wellknown/PropertyIDMap;)Ljava/lang/Object;

    .line 98
    sget-object v1, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 99
    invoke-static {}, Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;->getDocumentSummaryInformationProperties()Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    move-result-object v2

    .line 98
    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->put([BLorg/apache/poi/hpsf/wellknown/PropertyIDMap;)Ljava/lang/Object;

    .line 100
    sput-object v0, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->defaultMap:Lorg/apache/poi/hpsf/wellknown/SectionIDMap;

    .line 102
    :cond_0
    sget-object v1, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->defaultMap:Lorg/apache/poi/hpsf/wellknown/SectionIDMap;

    return-object v1
.end method

.method public static getPIDString([BJ)Ljava/lang/String;
    .locals 3
    .param p0, "sectionFormatID"    # [B
    .param p1, "pid"    # J

    .prologue
    .line 123
    invoke-static {}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->getInstance()Lorg/apache/poi/hpsf/wellknown/SectionIDMap;

    move-result-object v2

    invoke-virtual {v2, p0}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->get([B)Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    move-result-object v0

    .line 124
    .local v0, "m":Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;
    if-nez v0, :cond_1

    .line 125
    const-string/jumbo v1, "[undefined]"

    .line 130
    :cond_0
    :goto_0
    return-object v1

    .line 127
    :cond_1
    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 128
    .local v1, "s":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 129
    const-string/jumbo v1, "[undefined]"

    goto :goto_0
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "sectionFormatID"    # Ljava/lang/Object;

    .prologue
    .line 159
    check-cast p1, [B

    .end local p1    # "sectionFormatID":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->get([B)Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    move-result-object v0

    return-object v0
.end method

.method public get([B)Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;
    .locals 1
    .param p1, "sectionFormatID"    # [B

    .prologue
    .line 144
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-super {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 194
    check-cast p1, [B

    .end local p1    # "key":Ljava/lang/Object;
    check-cast p2, Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->put([BLorg/apache/poi/hpsf/wellknown/PropertyIDMap;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put([BLorg/apache/poi/hpsf/wellknown/PropertyIDMap;)Ljava/lang/Object;
    .locals 1
    .param p1, "sectionFormatID"    # [B
    .param p2, "propertyIDMap"    # Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    .prologue
    .line 175
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-super {p0, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
