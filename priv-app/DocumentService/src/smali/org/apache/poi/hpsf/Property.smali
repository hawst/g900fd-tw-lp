.class public Lorg/apache/poi/hpsf/Property;
.super Ljava/lang/Object;
.source "Property.java"


# instance fields
.field protected id:J

.field protected type:J

.field protected value:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    return-void
.end method

.method public constructor <init>(JJLjava/lang/Object;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "type"    # J
    .param p5, "value"    # Ljava/lang/Object;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-wide p1, p0, Lorg/apache/poi/hpsf/Property;->id:J

    .line 122
    iput-wide p3, p0, Lorg/apache/poi/hpsf/Property;->type:J

    .line 123
    iput-object p5, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    .line 124
    return-void
.end method

.method public constructor <init>(J[BJII)V
    .locals 8
    .param p1, "id"    # J
    .param p3, "src"    # [B
    .param p4, "offset"    # J
    .param p6, "length"    # I
    .param p7, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-wide p1, p0, Lorg/apache/poi/hpsf/Property;->id:J

    .line 152
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    move-object v0, p0

    move-object v1, p3

    move-wide v2, p4

    move v4, p6

    move v5, p7

    .line 154
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hpsf/Property;->readDictionary([BJII)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    .line 171
    :goto_0
    return-void

    .line 158
    :cond_0
    long-to-int v2, p4

    .line 159
    .local v2, "o":I
    invoke-static {p3, v2}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/poi/hpsf/Property;->type:J

    .line 160
    add-int/lit8 v2, v2, 0x4

    .line 164
    :try_start_0
    iget-wide v0, p0, Lorg/apache/poi/hpsf/Property;->type:J

    long-to-int v0, v0

    int-to-long v4, v0

    move-object v1, p3

    move v3, p6

    move v6, p7

    invoke-static/range {v1 .. v6}, Lorg/apache/poi/hpsf/VariantSupport;->read([BIIJI)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/poi/hpsf/UnsupportedVariantTypeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v7

    .line 168
    .local v7, "ex":Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;
    invoke-static {v7}, Lorg/apache/poi/hpsf/VariantSupport;->writeUnsupportedTypeMessage(Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;)V

    .line 169
    invoke-virtual {v7}, Lorg/apache/poi/hpsf/UnsupportedVariantTypeException;->getValue()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    goto :goto_0
.end method

.method private typesAreEqual(JJ)Z
    .locals 7
    .param p1, "t1"    # J
    .param p3, "t2"    # J

    .prologue
    const-wide/16 v4, 0x1f

    const-wide/16 v2, 0x1e

    .line 376
    cmp-long v0, p1, p3

    if-eqz v0, :cond_1

    .line 377
    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    cmp-long v0, p3, v4

    if-eqz v0, :cond_1

    .line 378
    :cond_0
    cmp-long v0, p3, v2

    if-nez v0, :cond_2

    cmp-long v0, p1, v4

    if-nez v0, :cond_2

    .line 379
    :cond_1
    const/4 v0, 0x1

    .line 381
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 346
    instance-of v7, p1, Lorg/apache/poi/hpsf/Property;

    if-nez v7, :cond_1

    .line 369
    :cond_0
    :goto_0
    return v6

    :cond_1
    move-object v0, p1

    .line 349
    check-cast v0, Lorg/apache/poi/hpsf/Property;

    .line 350
    .local v0, "p":Lorg/apache/poi/hpsf/Property;
    invoke-virtual {v0}, Lorg/apache/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 351
    .local v1, "pValue":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/apache/poi/hpsf/Property;->getID()J

    move-result-wide v2

    .line 352
    .local v2, "pId":J
    iget-wide v8, p0, Lorg/apache/poi/hpsf/Property;->id:J

    cmp-long v7, v8, v2

    if-nez v7, :cond_0

    iget-wide v8, p0, Lorg/apache/poi/hpsf/Property;->id:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    iget-wide v8, p0, Lorg/apache/poi/hpsf/Property;->type:J

    invoke-virtual {v0}, Lorg/apache/poi/hpsf/Property;->getType()J

    move-result-wide v10

    invoke-direct {p0, v8, v9, v10, v11}, Lorg/apache/poi/hpsf/Property;->typesAreEqual(JJ)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 354
    :cond_2
    iget-object v7, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    if-nez v7, :cond_3

    if-nez v1, :cond_3

    .line 355
    const/4 v6, 0x1

    goto :goto_0

    .line 356
    :cond_3
    iget-object v7, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    if-eqz v7, :cond_0

    if-eqz v1, :cond_0

    .line 360
    iget-object v7, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 361
    .local v5, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 362
    .local v4, "pValueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v5, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 363
    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 366
    :cond_4
    iget-object v6, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    instance-of v6, v6, [B

    if-eqz v6, :cond_5

    .line 367
    iget-object v6, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    check-cast v6, [B

    check-cast v1, [B

    .end local v1    # "pValue":Ljava/lang/Object;
    invoke-static {v6, v1}, Lorg/apache/poi/hpsf/Util;->equal([B[B)Z

    move-result v6

    goto :goto_0

    .line 369
    .restart local v1    # "pValue":Ljava/lang/Object;
    :cond_5
    iget-object v6, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lorg/apache/poi/hpsf/Property;->id:J

    return-wide v0
.end method

.method protected getSize()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 306
    iget-wide v6, p0, Lorg/apache/poi/hpsf/Property;->type:J

    invoke-static {v6, v7}, Lorg/apache/poi/hpsf/VariantSupport;->getVariantLength(J)I

    move-result v2

    .line 307
    .local v2, "length":I
    if-ltz v2, :cond_0

    move v3, v2

    .line 331
    .end local v2    # "length":I
    .local v3, "length":I
    :goto_0
    return v3

    .line 309
    .end local v3    # "length":I
    .restart local v2    # "length":I
    :cond_0
    const/4 v5, -0x2

    if-ne v2, v5, :cond_1

    .line 311
    new-instance v5, Lorg/apache/poi/hpsf/WritingNotSupportedException;

    iget-wide v6, p0, Lorg/apache/poi/hpsf/Property;->type:J

    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Lorg/apache/poi/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v5

    .line 314
    :cond_1
    const/4 v0, 0x4

    .line 315
    .local v0, "PADDING":I
    iget-wide v6, p0, Lorg/apache/poi/hpsf/Property;->type:J

    long-to-int v5, v6

    sparse-switch v5, :sswitch_data_0

    .line 329
    new-instance v5, Lorg/apache/poi/hpsf/WritingNotSupportedException;

    iget-wide v6, p0, Lorg/apache/poi/hpsf/Property;->type:J

    iget-object v8, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    invoke-direct {v5, v6, v7, v8}, Lorg/apache/poi/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v5

    .line 319
    :sswitch_0
    iget-object v5, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v1, v5, 0x1

    .line 320
    .local v1, "l":I
    rem-int/lit8 v4, v1, 0x4

    .line 321
    .local v4, "r":I
    if-lez v4, :cond_2

    .line 322
    rsub-int/lit8 v5, v4, 0x4

    add-int/2addr v1, v5

    .line 323
    :cond_2
    add-int/2addr v2, v1

    .end local v1    # "l":I
    .end local v4    # "r":I
    :sswitch_1
    move v3, v2

    .line 331
    .end local v2    # "length":I
    .restart local v3    # "length":I
    goto :goto_0

    .line 315
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1e -> :sswitch_0
    .end sparse-switch
.end method

.method public getType()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lorg/apache/poi/hpsf/Property;->type:J

    return-wide v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 391
    const-wide/16 v0, 0x0

    .line 392
    .local v0, "hashCode":J
    iget-wide v4, p0, Lorg/apache/poi/hpsf/Property;->id:J

    add-long/2addr v0, v4

    .line 393
    iget-wide v4, p0, Lorg/apache/poi/hpsf/Property;->type:J

    add-long/2addr v0, v4

    .line 394
    iget-object v3, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    if-eqz v3, :cond_0

    .line 395
    iget-object v3, p0, Lorg/apache/poi/hpsf/Property;->value:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 396
    :cond_0
    const-wide v4, 0xffffffffL

    and-long/2addr v4, v0

    long-to-int v2, v4

    .line 397
    .local v2, "returnHashCode":I
    return v2
.end method

.method protected readDictionary([BJII)Ljava/util/Map;
    .locals 22
    .param p1, "src"    # [B
    .param p2, "offset"    # J
    .param p4, "length"    # I
    .param p5, "codepage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BJII)",
            "Ljava/util/Map",
            "<**>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 201
    const-wide/16 v18, 0x0

    cmp-long v18, p2, v18

    if-ltz v18, :cond_0

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    cmp-long v18, p2, v18

    if-lez v18, :cond_1

    .line 202
    :cond_0
    new-instance v18, Lorg/apache/poi/hpsf/HPSFRuntimeException;

    .line 203
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "Illegal offset "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, " while HPSF stream contains "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 204
    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, " bytes."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 203
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 202
    invoke-direct/range {v18 .. v19}, Lorg/apache/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 205
    :cond_1
    move-wide/from16 v0, p2

    long-to-int v13, v0

    .line 210
    .local v13, "o":I
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v14

    .line 211
    .local v14, "nrEntries":J
    add-int/lit8 v13, v13, 0x4

    .line 213
    new-instance v11, Ljava/util/LinkedHashMap;

    .line 214
    long-to-int v0, v14

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    .line 213
    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v11, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IF)V

    .line 218
    .local v11, "m":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    int-to-long v0, v7

    move-wide/from16 v18, v0

    cmp-long v18, v18, v14

    if-ltz v18, :cond_2

    .line 290
    :goto_1
    return-object v11

    .line 221
    :cond_2
    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 222
    .local v9, "id":Ljava/lang/Long;
    add-int/lit8 v13, v13, 0x4

    .line 229
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v16

    .line 230
    .local v16, "sLength":J
    add-int/lit8 v13, v13, 0x4

    .line 233
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 234
    .local v4, "b":Ljava/lang/StringBuffer;
    sparse-switch p5, :sswitch_data_0

    .line 262
    new-instance v18, Ljava/lang/String;

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v19, v0

    .line 263
    invoke-static/range {p5 .. p5}, Lorg/apache/poi/hpsf/VariantSupport;->codepageToEncoding(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v13, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 262
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 269
    :goto_2
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    if-lez v18, :cond_3

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    if-eqz v18, :cond_6

    .line 271
    :cond_3
    const/16 v18, 0x4b0

    move/from16 v0, p5

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 273
    const-wide/16 v18, 0x2

    rem-long v18, v16, v18

    const-wide/16 v20, 0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_4

    .line 274
    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    .line 275
    :cond_4
    int-to-long v0, v13

    move-wide/from16 v18, v0

    add-long v20, v16, v16

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 279
    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 240
    :sswitch_0
    new-instance v18, Ljava/lang/String;

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-direct {v0, v1, v13, v2}, Ljava/lang/String;-><init>([BII)V

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 282
    .end local v4    # "b":Ljava/lang/StringBuffer;
    .end local v9    # "id":Ljava/lang/Long;
    .end local v16    # "sLength":J
    :catch_0
    move-exception v5

    .line 284
    .local v5, "ex":Ljava/lang/RuntimeException;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v10

    .line 285
    .local v10, "l":Lorg/apache/poi/util/POILogger;
    const/16 v18, 0x5

    .line 286
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "The property set\'s dictionary contains bogus data. All dictionary entries starting with the one with ID "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 288
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/hpsf/Property;->id:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, " will be ignored."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 286
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 285
    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1, v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 247
    .end local v5    # "ex":Ljava/lang/RuntimeException;
    .end local v10    # "l":Lorg/apache/poi/util/POILogger;
    .restart local v4    # "b":Ljava/lang/StringBuffer;
    .restart local v9    # "id":Ljava/lang/Long;
    .restart local v16    # "sLength":J
    :sswitch_1
    const-wide/16 v18, 0x2

    mul-long v18, v18, v16

    move-wide/from16 v0, v18

    long-to-int v12, v0

    .line 248
    .local v12, "nrBytes":I
    :try_start_1
    new-array v6, v12, [B

    .line 249
    .local v6, "h":[B
    const/4 v8, 0x0

    .local v8, "i2":I
    :goto_4
    if-lt v8, v12, :cond_5

    .line 254
    new-instance v18, Ljava/lang/String;

    const/16 v19, 0x0

    .line 255
    invoke-static/range {p5 .. p5}, Lorg/apache/poi/hpsf/VariantSupport;->codepageToEncoding(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v6, v1, v12, v2}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 254
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 251
    :cond_5
    add-int v18, v13, v8

    add-int/lit8 v18, v18, 0x1

    aget-byte v18, p1, v18

    aput-byte v18, v6, v8

    .line 252
    add-int/lit8 v18, v8, 0x1

    add-int v19, v13, v8

    aget-byte v19, p1, v19

    aput-byte v19, v6, v18

    .line 249
    add-int/lit8 v8, v8, 0x2

    goto :goto_4

    .line 270
    .end local v6    # "h":[B
    .end local v8    # "i2":I
    .end local v12    # "nrBytes":I
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->setLength(I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 278
    :cond_7
    int-to-long v0, v13

    move-wide/from16 v18, v0

    add-long v18, v18, v16

    move-wide/from16 v0, v18

    long-to-int v13, v0

    goto/16 :goto_3

    .line 234
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x4b0 -> :sswitch_1
    .end sparse-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 13

    .prologue
    .line 408
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 409
    .local v0, "b":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 410
    const/16 v10, 0x5b

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 411
    const-string/jumbo v10, "id: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 412
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Property;->getID()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 413
    const-string/jumbo v10, ", type: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 414
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Property;->getType()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 415
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v9

    .line 416
    .local v9, "value":Ljava/lang/Object;
    const-string/jumbo v10, ", value: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 417
    instance-of v10, v9, Ljava/lang/String;

    if-eqz v10, :cond_3

    .line 419
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object v8, v9

    .line 420
    check-cast v8, Ljava/lang/String;

    .line 421
    .local v8, "s":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v6

    .line 422
    .local v6, "l":I
    mul-int/lit8 v10, v6, 0x2

    new-array v1, v10, [B

    .line 423
    .local v1, "bytes":[B
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v6, :cond_2

    .line 431
    const-string/jumbo v10, " ["

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 432
    array-length v10, v1

    if-lez v10, :cond_0

    .line 433
    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v1, v10, v11, v12}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v3

    .line 434
    .local v3, "hex":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 436
    .end local v3    # "hex":Ljava/lang/String;
    :cond_0
    const-string/jumbo v10, "]"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 450
    .end local v1    # "bytes":[B
    .end local v5    # "i":I
    .end local v6    # "l":I
    .end local v8    # "s":Ljava/lang/String;
    :cond_1
    :goto_1
    const/16 v10, 0x5d

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 451
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .line 425
    .restart local v1    # "bytes":[B
    .restart local v5    # "i":I
    .restart local v6    # "l":I
    .restart local v8    # "s":Ljava/lang/String;
    :cond_2
    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 426
    .local v2, "c":C
    const v10, 0xff00

    and-int/2addr v10, v2

    shr-int/lit8 v10, v10, 0x8

    int-to-byte v4, v10

    .line 427
    .local v4, "high":B
    and-int/lit16 v10, v2, 0xff

    shr-int/lit8 v10, v10, 0x0

    int-to-byte v7, v10

    .line 428
    .local v7, "low":B
    mul-int/lit8 v10, v5, 0x2

    aput-byte v4, v1, v10

    .line 429
    mul-int/lit8 v10, v5, 0x2

    add-int/lit8 v10, v10, 0x1

    aput-byte v7, v1, v10

    .line 423
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 438
    .end local v1    # "bytes":[B
    .end local v2    # "c":C
    .end local v4    # "high":B
    .end local v5    # "i":I
    .end local v6    # "l":I
    .end local v7    # "low":B
    .end local v8    # "s":Ljava/lang/String;
    :cond_3
    instance-of v10, v9, [B

    if-eqz v10, :cond_4

    move-object v1, v9

    .line 440
    check-cast v1, [B

    .line 441
    .restart local v1    # "bytes":[B
    array-length v10, v1

    if-lez v10, :cond_1

    .line 442
    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v1, v10, v11, v12}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v3

    .line 443
    .restart local v3    # "hex":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 448
    .end local v1    # "bytes":[B
    .end local v3    # "hex":Ljava/lang/String;
    :cond_4
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
