.class public Lorg/apache/poi/hpsf/DocumentSummaryInformation;
.super Lorg/apache/poi/hpsf/SpecialPropertySet;
.source "DocumentSummaryInformation.java"


# static fields
.field public static final DEFAULT_STREAM_NAME:Ljava/lang/String; = "\u0005DocumentSummaryInformation"


# direct methods
.method public constructor <init>(Lorg/apache/poi/hpsf/PropertySet;)V
    .locals 3
    .param p1, "ps"    # Lorg/apache/poi/hpsf/PropertySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lorg/apache/poi/hpsf/SpecialPropertySet;-><init>(Lorg/apache/poi/hpsf/PropertySet;)V

    .line 64
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->isDocumentSummaryInformation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Not a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-direct {v0, v1}, Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    return-void
.end method

.method private ensureSection2()V
    .locals 3

    .prologue
    .line 639
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSectionCount()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 641
    new-instance v0, Lorg/apache/poi/hpsf/MutableSection;

    invoke-direct {v0}, Lorg/apache/poi/hpsf/MutableSection;-><init>()V

    .line 642
    .local v0, "s2":Lorg/apache/poi/hpsf/MutableSection;
    sget-object v1, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hpsf/MutableSection;->setFormatID([B)V

    .line 643
    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->addSection(Lorg/apache/poi/hpsf/Section;)V

    .line 645
    .end local v0    # "s2":Lorg/apache/poi/hpsf/MutableSection;
    :cond_0
    return-void
.end method

.method private notYetImplemented(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 671
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " is not yet implemented."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getByteCount()I
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCompany()Ljava/lang/String;
    .locals 1

    .prologue
    .line 511
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCustomProperties()Lorg/apache/poi/hpsf/CustomProperties;
    .locals 12

    .prologue
    .line 575
    const/4 v1, 0x0

    .line 576
    .local v1, "cps":Lorg/apache/poi/hpsf/CustomProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSectionCount()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_0

    .line 578
    new-instance v1, Lorg/apache/poi/hpsf/CustomProperties;

    .end local v1    # "cps":Lorg/apache/poi/hpsf/CustomProperties;
    invoke-direct {v1}, Lorg/apache/poi/hpsf/CustomProperties;-><init>()V

    .line 579
    .restart local v1    # "cps":Lorg/apache/poi/hpsf/CustomProperties;
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSections()Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/hpsf/Section;

    .line 580
    .local v9, "section":Lorg/apache/poi/hpsf/Section;
    invoke-virtual {v9}, Lorg/apache/poi/hpsf/Section;->getDictionary()Ljava/util/Map;

    move-result-object v2

    .line 581
    .local v2, "dictionary":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-virtual {v9}, Lorg/apache/poi/hpsf/Section;->getProperties()[Lorg/apache/poi/hpsf/Property;

    move-result-object v7

    .line 582
    .local v7, "properties":[Lorg/apache/poi/hpsf/Property;
    const/4 v8, 0x0

    .line 583
    .local v8, "propertyCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v10, v7

    if-lt v3, v10, :cond_1

    .line 595
    invoke-virtual {v1}, Lorg/apache/poi/hpsf/CustomProperties;->size()I

    move-result v10

    if-eq v10, v8, :cond_0

    .line 596
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lorg/apache/poi/hpsf/CustomProperties;->setPure(Z)V

    .line 598
    .end local v2    # "dictionary":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v3    # "i":I
    .end local v7    # "properties":[Lorg/apache/poi/hpsf/Property;
    .end local v8    # "propertyCount":I
    .end local v9    # "section":Lorg/apache/poi/hpsf/Section;
    :cond_0
    return-object v1

    .line 585
    .restart local v2    # "dictionary":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    .restart local v3    # "i":I
    .restart local v7    # "properties":[Lorg/apache/poi/hpsf/Property;
    .restart local v8    # "propertyCount":I
    .restart local v9    # "section":Lorg/apache/poi/hpsf/Section;
    :cond_1
    aget-object v6, v7, v3

    .line 586
    .local v6, "p":Lorg/apache/poi/hpsf/Property;
    invoke-virtual {v6}, Lorg/apache/poi/hpsf/Property;->getID()J

    move-result-wide v4

    .line 587
    .local v4, "id":J
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-eqz v10, :cond_2

    const-wide/16 v10, 0x1

    cmp-long v10, v4, v10

    if-eqz v10, :cond_2

    .line 589
    add-int/lit8 v8, v8, 0x1

    .line 590
    new-instance v0, Lorg/apache/poi/hpsf/CustomProperty;

    .line 591
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v2, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 590
    invoke-direct {v0, v6, v10}, Lorg/apache/poi/hpsf/CustomProperty;-><init>(Lorg/apache/poi/hpsf/Property;Ljava/lang/String;)V

    .line 592
    .local v0, "cp":Lorg/apache/poi/hpsf/CustomProperty;
    invoke-virtual {v0}, Lorg/apache/poi/hpsf/CustomProperty;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10, v0}, Lorg/apache/poi/hpsf/CustomProperties;->put(Ljava/lang/String;Lorg/apache/poi/hpsf/CustomProperty;)Lorg/apache/poi/hpsf/CustomProperty;

    .line 583
    .end local v0    # "cp":Lorg/apache/poi/hpsf/CustomProperty;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getDocparts()[B
    .locals 1

    .prologue
    .line 445
    const-string/jumbo v0, "Reading byte arrays"

    invoke-direct {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 446
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public getHeadingPair()[B
    .locals 1

    .prologue
    .line 411
    const-string/jumbo v0, "Reading byte arrays "

    invoke-direct {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 412
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public getHiddenCount()I
    .locals 1

    .prologue
    .line 310
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getLineCount()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getLinksDirty()Z
    .locals 1

    .prologue
    .line 543
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyBooleanValue(I)Z

    move-result v0

    return v0
.end method

.method public getMMClipCount()I
    .locals 1

    .prologue
    .line 344
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getManager()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNoteCount()I
    .locals 1

    .prologue
    .line 276
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getParCount()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getPresentationFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPropertySetIDMap()Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;->getDocumentSummaryInformationProperties()Lorg/apache/poi/hpsf/wellknown/PropertyIDMap;

    move-result-object v0

    return-object v0
.end method

.method public getScale()Z
    .locals 1

    .prologue
    .line 377
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyBooleanValue(I)Z

    move-result v0

    return v0
.end method

.method public getSlideCount()I
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public removeByteCount()V
    .locals 4

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 164
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 165
    return-void
.end method

.method public removeCategory()V
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 98
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 99
    return-void
.end method

.method public removeCompany()V
    .locals 4

    .prologue
    .line 530
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 531
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 532
    return-void
.end method

.method public removeCustomProperties()V
    .locals 2

    .prologue
    .line 654
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSectionCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 655
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSections()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 658
    return-void

    .line 657
    :cond_0
    new-instance v0, Lorg/apache/poi/hpsf/HPSFRuntimeException;

    const-string/jumbo v1, "Illegal internal format of Document SummaryInformation stream: second section is missing."

    invoke-direct {v0, v1}, Lorg/apache/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeDocparts()V
    .locals 4

    .prologue
    .line 466
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 467
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xd

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 468
    return-void
.end method

.method public removeHeadingPair()V
    .locals 4

    .prologue
    .line 430
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 431
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 432
    return-void
.end method

.method public removeHiddenCount()V
    .locals 4

    .prologue
    .line 329
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 330
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x9

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 331
    return-void
.end method

.method public removeLineCount()V
    .locals 4

    .prologue
    .line 196
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 197
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 198
    return-void
.end method

.method public removeLinksDirty()V
    .locals 4

    .prologue
    .line 562
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 563
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 564
    return-void
.end method

.method public removeMMClipCount()V
    .locals 4

    .prologue
    .line 363
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 364
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 365
    return-void
.end method

.method public removeManager()V
    .locals 4

    .prologue
    .line 498
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 499
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xe

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 500
    return-void
.end method

.method public removeNoteCount()V
    .locals 4

    .prologue
    .line 295
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 296
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 297
    return-void
.end method

.method public removeParCount()V
    .locals 4

    .prologue
    .line 229
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 230
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x6

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 231
    return-void
.end method

.method public removePresentationFormat()V
    .locals 4

    .prologue
    .line 130
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 131
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 132
    return-void
.end method

.method public removeScale()V
    .locals 4

    .prologue
    .line 396
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 397
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xb

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 398
    return-void
.end method

.method public removeSlideCount()V
    .locals 4

    .prologue
    .line 262
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 263
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 264
    return-void
.end method

.method public setByteCount(I)V
    .locals 2
    .param p1, "byteCount"    # I

    .prologue
    .line 154
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 155
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 156
    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 2
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 89
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 90
    return-void
.end method

.method public setCompany(Ljava/lang/String;)V
    .locals 2
    .param p1, "company"    # Ljava/lang/String;

    .prologue
    .line 521
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 522
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 523
    return-void
.end method

.method public setCustomProperties(Lorg/apache/poi/hpsf/CustomProperties;)V
    .locals 7
    .param p1, "customProperties"    # Lorg/apache/poi/hpsf/CustomProperties;

    .prologue
    .line 608
    invoke-direct {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->ensureSection2()V

    .line 609
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSections()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hpsf/MutableSection;

    .line 610
    .local v4, "section":Lorg/apache/poi/hpsf/MutableSection;
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/CustomProperties;->getDictionary()Ljava/util/Map;

    move-result-object v1

    .line 611
    .local v1, "dictionary":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-virtual {v4}, Lorg/apache/poi/hpsf/MutableSection;->clear()V

    .line 616
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/CustomProperties;->getCodepage()I

    move-result v0

    .line 617
    .local v0, "cpCodepage":I
    if-gez v0, :cond_0

    .line 618
    invoke-virtual {v4}, Lorg/apache/poi/hpsf/MutableSection;->getCodepage()I

    move-result v0

    .line 619
    :cond_0
    if-gez v0, :cond_1

    .line 620
    const/16 v0, 0x4b0

    .line 621
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/poi/hpsf/CustomProperties;->setCodepage(I)V

    .line 622
    invoke-virtual {v4, v0}, Lorg/apache/poi/hpsf/MutableSection;->setCodepage(I)V

    .line 623
    invoke-virtual {v4, v1}, Lorg/apache/poi/hpsf/MutableSection;->setDictionary(Ljava/util/Map;)V

    .line 624
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/CustomProperties;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hpsf/CustomProperty;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 629
    return-void

    .line 626
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hpsf/Property;

    .line 627
    .local v3, "p":Lorg/apache/poi/hpsf/Property;
    invoke-virtual {v4, v3}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(Lorg/apache/poi/hpsf/Property;)V

    goto :goto_0
.end method

.method public setDocparts([B)V
    .locals 1
    .param p1, "docparts"    # [B

    .prologue
    .line 458
    const-string/jumbo v0, "Writing byte arrays"

    invoke-direct {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 459
    return-void
.end method

.method public setHeadingPair([B)V
    .locals 1
    .param p1, "headingPair"    # [B

    .prologue
    .line 422
    const-string/jumbo v0, "Writing byte arrays "

    invoke-direct {p0, v0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 423
    return-void
.end method

.method public setHiddenCount(I)V
    .locals 3
    .param p1, "hiddenCount"    # I

    .prologue
    .line 320
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getSections()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 321
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 322
    return-void
.end method

.method public setLineCount(I)V
    .locals 2
    .param p1, "lineCount"    # I

    .prologue
    .line 187
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 188
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 189
    return-void
.end method

.method public setLinksDirty(Z)V
    .locals 2
    .param p1, "linksDirty"    # Z

    .prologue
    .line 553
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 554
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(IZ)V

    .line 555
    return-void
.end method

.method public setMMClipCount(I)V
    .locals 2
    .param p1, "mmClipCount"    # I

    .prologue
    .line 354
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 355
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 356
    return-void
.end method

.method public setManager(Ljava/lang/String;)V
    .locals 2
    .param p1, "manager"    # Ljava/lang/String;

    .prologue
    .line 489
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 490
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0xe

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 491
    return-void
.end method

.method public setNoteCount(I)V
    .locals 2
    .param p1, "noteCount"    # I

    .prologue
    .line 286
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 287
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 288
    return-void
.end method

.method public setParCount(I)V
    .locals 2
    .param p1, "parCount"    # I

    .prologue
    .line 220
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 221
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 222
    return-void
.end method

.method public setPresentationFormat(Ljava/lang/String;)V
    .locals 2
    .param p1, "presentationFormat"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 122
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 123
    return-void
.end method

.method public setScale(Z)V
    .locals 2
    .param p1, "scale"    # Z

    .prologue
    .line 387
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 388
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(IZ)V

    .line 389
    return-void
.end method

.method public setSlideCount(I)V
    .locals 2
    .param p1, "slideCount"    # I

    .prologue
    .line 253
    invoke-virtual {p0}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hpsf/MutableSection;

    .line 254
    .local v0, "s":Lorg/apache/poi/hpsf/MutableSection;
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hpsf/MutableSection;->setProperty(II)V

    .line 255
    return-void
.end method
