.class public Lorg/apache/poi/hpsf/PropertySetFactory;
.super Ljava/lang/Object;
.source "PropertySetFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Ljava/io/InputStream;)Lorg/apache/poi/hpsf/PropertySet;
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v1, Lorg/apache/poi/hpsf/PropertySet;

    invoke-direct {v1, p0}, Lorg/apache/poi/hpsf/PropertySet;-><init>(Ljava/io/InputStream;)V

    .line 51
    .local v1, "ps":Lorg/apache/poi/hpsf/PropertySet;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/poi/hpsf/PropertySet;->isSummaryInformation()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    new-instance v2, Lorg/apache/poi/hpsf/SummaryInformation;

    invoke-direct {v2, v1}, Lorg/apache/poi/hpsf/SummaryInformation;-><init>(Lorg/apache/poi/hpsf/PropertySet;)V

    move-object v1, v2

    .line 56
    .end local v1    # "ps":Lorg/apache/poi/hpsf/PropertySet;
    :cond_0
    :goto_0
    return-object v1

    .line 53
    .restart local v1    # "ps":Lorg/apache/poi/hpsf/PropertySet;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/hpsf/PropertySet;->isDocumentSummaryInformation()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    new-instance v2, Lorg/apache/poi/hpsf/DocumentSummaryInformation;

    invoke-direct {v2, v1}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;-><init>(Lorg/apache/poi/hpsf/PropertySet;)V
    :try_end_0
    .catch Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 64
    .local v0, "ex":Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;
    new-instance v2, Ljava/lang/Exception;

    invoke-virtual {v0}, Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static newDocumentSummaryInformation()Lorg/apache/poi/hpsf/DocumentSummaryInformation;
    .locals 5

    .prologue
    .line 100
    new-instance v1, Lorg/apache/poi/hpsf/MutablePropertySet;

    invoke-direct {v1}, Lorg/apache/poi/hpsf/MutablePropertySet;-><init>()V

    .line 101
    .local v1, "ps":Lorg/apache/poi/hpsf/MutablePropertySet;
    invoke-virtual {v1}, Lorg/apache/poi/hpsf/MutablePropertySet;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hpsf/MutableSection;

    .line 102
    .local v2, "s":Lorg/apache/poi/hpsf/MutableSection;
    sget-object v3, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->setFormatID([B)V

    .line 105
    :try_start_0
    new-instance v3, Lorg/apache/poi/hpsf/DocumentSummaryInformation;

    invoke-direct {v3, v1}, Lorg/apache/poi/hpsf/DocumentSummaryInformation;-><init>(Lorg/apache/poi/hpsf/PropertySet;)V
    :try_end_0
    .catch Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 107
    :catch_0
    move-exception v0

    .line 110
    .local v0, "ex":Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;
    new-instance v3, Lorg/apache/poi/hpsf/HPSFRuntimeException;

    invoke-direct {v3, v0}, Lorg/apache/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static newSummaryInformation()Lorg/apache/poi/hpsf/SummaryInformation;
    .locals 4

    .prologue
    .line 77
    new-instance v1, Lorg/apache/poi/hpsf/MutablePropertySet;

    invoke-direct {v1}, Lorg/apache/poi/hpsf/MutablePropertySet;-><init>()V

    .line 78
    .local v1, "ps":Lorg/apache/poi/hpsf/MutablePropertySet;
    invoke-virtual {v1}, Lorg/apache/poi/hpsf/MutablePropertySet;->getFirstSection()Lorg/apache/poi/hpsf/Section;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hpsf/MutableSection;

    .line 79
    .local v2, "s":Lorg/apache/poi/hpsf/MutableSection;
    sget-object v3, Lorg/apache/poi/hpsf/wellknown/SectionIDMap;->SUMMARY_INFORMATION_ID:[B

    invoke-virtual {v2, v3}, Lorg/apache/poi/hpsf/MutableSection;->setFormatID([B)V

    .line 82
    :try_start_0
    new-instance v3, Lorg/apache/poi/hpsf/SummaryInformation;

    invoke-direct {v3, v1}, Lorg/apache/poi/hpsf/SummaryInformation;-><init>(Lorg/apache/poi/hpsf/PropertySet;)V
    :try_end_0
    .catch Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 84
    :catch_0
    move-exception v0

    .line 87
    .local v0, "ex":Lorg/apache/poi/hpsf/UnexpectedPropertySetTypeException;
    new-instance v3, Lorg/apache/poi/hpsf/HPSFRuntimeException;

    invoke-direct {v3, v0}, Lorg/apache/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
