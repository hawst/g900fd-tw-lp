.class public final Lorg/apache/poi/hslf/util/SystemTimeUtils;
.super Ljava/lang/Object;
.source "SystemTimeUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDate([B)Ljava/util/Date;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/poi/hslf/util/SystemTimeUtils;->getDate([BI)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static getDate([BI)Ljava/util/Date;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 50
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 52
    .local v0, "cal":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-static {p0, p1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 53
    const/4 v1, 0x2

    add-int/lit8 v2, p1, 0x2

    invoke-static {p0, v2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 56
    const/4 v1, 0x5

    add-int/lit8 v2, p1, 0x6

    invoke-static {p0, v2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 57
    const/16 v1, 0xb

    add-int/lit8 v2, p1, 0x8

    invoke-static {p0, v2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 58
    const/16 v1, 0xc

    add-int/lit8 v2, p1, 0xa

    invoke-static {p0, v2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 59
    const/16 v1, 0xd

    add-int/lit8 v2, p1, 0xc

    invoke-static {p0, v2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 60
    const/16 v1, 0xe

    add-int/lit8 v2, p1, 0xe

    invoke-static {p0, v2}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 62
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method public static storeDate(Ljava/util/Date;[B)V
    .locals 1
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "dest"    # [B

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/poi/hslf/util/SystemTimeUtils;->storeDate(Ljava/util/Date;[BI)V

    .line 71
    return-void
.end method

.method public static storeDate(Ljava/util/Date;[BI)V
    .locals 3
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "dest"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 77
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 78
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 80
    add-int/lit8 v1, p2, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 81
    add-int/lit8 v1, p2, 0x2

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 82
    add-int/lit8 v1, p2, 0x4

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    add-int/lit8 v1, p2, 0x6

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 84
    add-int/lit8 v1, p2, 0x8

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 85
    add-int/lit8 v1, p2, 0xa

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 86
    add-int/lit8 v1, p2, 0xc

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 87
    add-int/lit8 v1, p2, 0xe

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-short v2, v2

    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 88
    return-void
.end method
