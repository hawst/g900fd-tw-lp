.class public final Lorg/apache/poi/hslf/record/Slide;
.super Lorg/apache/poi/hslf/record/SheetContainer;
.source "Slide.java"


# static fields
.field private static _type:J


# instance fields
.field private _colorScheme:Lorg/apache/poi/hslf/record/ColorSchemeAtom;

.field private _header:[B

.field private ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

.field private slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/16 v0, 0x3ee

    sput-wide v0, Lorg/apache/poi/hslf/record/Slide;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 84
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/SheetContainer;-><init>()V

    .line 85
    const/16 v1, 0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    .line 86
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    const/16 v2, 0xf

    invoke-static {v1, v4, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 87
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    sget-wide v2, Lorg/apache/poi/hslf/record/Slide;->_type:J

    long-to-int v2, v2

    invoke-static {v1, v5, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 88
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    const/4 v2, 0x4

    invoke-static {v1, v2, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 90
    new-instance v1, Lorg/apache/poi/hslf/record/SlideAtom;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/SlideAtom;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

    .line 91
    new-instance v1, Lorg/apache/poi/hslf/record/PPDrawing;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/PPDrawing;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    .line 93
    new-instance v0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;-><init>()V

    .line 95
    .local v0, "colorAtom":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    const/4 v1, 0x3

    new-array v1, v1, [Lorg/apache/poi/hslf/record/Record;

    .line 96
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Slide;->slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    .line 97
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    aput-object v3, v1, v2

    .line 98
    aput-object v0, v1, v5

    .line 95
    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 100
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    .line 57
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/SheetContainer;-><init>()V

    .line 59
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    .line 60
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    add-int/lit8 v1, p2, 0x8

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v1, v2}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 78
    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/SlideAtom;

    if-eqz v1, :cond_3

    .line 68
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/SlideAtom;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

    .line 74
    :cond_1
    :goto_1
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    if-eqz v1, :cond_2

    .line 75
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_colorScheme:Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    .line 66
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_3
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/PPDrawing;

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/PPDrawing;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    goto :goto_1
.end method


# virtual methods
.method public getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Slide;->_colorScheme:Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    return-object v0
.end method

.method public getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lorg/apache/poi/hslf/record/Slide;->_type:J

    return-wide v0
.end method

.method public getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Slide;->slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/Slide;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/Slide;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/Slide;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/Slide;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 113
    return-void
.end method
