.class public final Lorg/apache/poi/hslf/dev/SlideIdListing;
.super Ljava/lang/Object;
.source "SlideIdListing.java"


# static fields
.field private static fileContents:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findRecordAtPos(I)Lorg/apache/poi/hslf/record/Record;
    .locals 7
    .param p0, "pos"    # I

    .prologue
    .line 157
    sget-object v1, Lorg/apache/poi/hslf/dev/SlideIdListing;->fileContents:[B

    add-int/lit8 v6, p0, 0x2

    invoke-static {v1, v6}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    int-to-long v4, v1

    .line 158
    .local v4, "type":J
    sget-object v1, Lorg/apache/poi/hslf/dev/SlideIdListing;->fileContents:[B

    add-int/lit8 v6, p0, 0x4

    invoke-static {v1, v6}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v2

    .line 160
    .local v2, "rlen":J
    sget-object v1, Lorg/apache/poi/hslf/dev/SlideIdListing;->fileContents:[B

    long-to-int v6, v2

    add-int/lit8 v6, v6, 0x8

    invoke-static {v4, v5, v1, p0, v6}, Lorg/apache/poi/hslf/record/Record;->createRecordForType(J[BII)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 162
    .local v0, "r":Lorg/apache/poi/hslf/record/Record;
    return-object v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 24
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    .line 39
    sget-object v20, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v21, "Need to give a filename"

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 40
    const/16 v20, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/System;->exit(I)V

    .line 45
    :cond_0
    new-instance v6, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/16 v20, 0x0

    aget-object v20, p0, v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 46
    .local v6, "hss":Lorg/apache/poi/hslf/HSLFSlideShow;
    new-instance v19, Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-object/from16 v0, v19

    invoke-direct {v0, v6}, Lorg/apache/poi/hslf/usermodel/SlideShow;-><init>(Lorg/apache/poi/hslf/HSLFSlideShow;)V

    .line 49
    .local v19, "ss":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v6}, Lorg/apache/poi/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    move-result-object v20

    sput-object v20, Lorg/apache/poi/hslf/dev/SlideIdListing;->fileContents:[B

    .line 50
    invoke-virtual {v6}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v15

    .line 51
    .local v15, "records":[Lorg/apache/poi/hslf/record/Record;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getMostRecentCoreRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v10

    .line 54
    .local v10, "latestRecords":[Lorg/apache/poi/hslf/record/Record;
    const/4 v5, 0x0

    .line 55
    .local v5, "document":Lorg/apache/poi/hslf/record/Document;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v7, v0, :cond_2

    .line 67
    if-eqz v5, :cond_1

    .line 68
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Document;->getSlideListWithTexts()[Lorg/apache/poi/hslf/record/SlideListWithText;

    move-result-object v18

    .line 69
    .local v18, "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    const/4 v7, 0x0

    :goto_1
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v7, v0, :cond_4

    .line 81
    .end local v18    # "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    :cond_1
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 84
    const/4 v7, 0x0

    :goto_2
    array-length v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v7, v0, :cond_6

    .line 97
    const/4 v7, 0x0

    :goto_3
    array-length v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v7, v0, :cond_8

    .line 112
    const/4 v12, 0x0

    .line 113
    .local v12, "pos":I
    const/4 v7, 0x0

    :goto_4
    array-length v0, v15

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v7, v0, :cond_a

    .line 152
    return-void

    .line 56
    .end local v12    # "pos":I
    :cond_2
    aget-object v20, v10, v7

    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/hslf/record/Document;

    move/from16 v20, v0

    if-eqz v20, :cond_3

    .line 57
    aget-object v5, v10, v7

    .end local v5    # "document":Lorg/apache/poi/hslf/record/Document;
    check-cast v5, Lorg/apache/poi/hslf/record/Document;

    .line 55
    .restart local v5    # "document":Lorg/apache/poi/hslf/record/Document;
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 70
    .restart local v18    # "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    :cond_4
    aget-object v20, v18, v7

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlideListWithText;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v4

    .line 71
    .local v4, "cr":[Lorg/apache/poi/hslf/record/Record;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_5
    array-length v0, v4

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v9, v0, :cond_5

    .line 69
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 72
    :cond_5
    aget-object v20, v4, v9

    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move/from16 v20, v0

    .line 71
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 85
    .end local v4    # "cr":[Lorg/apache/poi/hslf/record/Record;
    .end local v9    # "j":I
    .end local v18    # "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    :cond_6
    aget-object v20, v10, v7

    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/hslf/record/Slide;

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 86
    aget-object v20, v10, v7

    check-cast v20, Lorg/apache/poi/hslf/record/Slide;

    .line 84
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 98
    :cond_8
    aget-object v20, v10, v7

    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/hslf/record/Notes;

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 99
    aget-object v20, v10, v7

    check-cast v20, Lorg/apache/poi/hslf/record/Notes;

    .line 97
    :cond_9
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 114
    .restart local v12    # "pos":I
    :cond_a
    aget-object v14, v15, v7

    .line 116
    .local v14, "r":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v14}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    .line 120
    invoke-virtual {v14}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v20

    const-wide/16 v22, 0x1772

    cmp-long v20, v20, v22

    if-nez v20, :cond_b

    move-object v13, v14

    .line 123
    check-cast v13, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    .line 126
    .local v13, "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    invoke-virtual {v13}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v16

    .line 127
    .local v16, "sheetIDs":[I
    invoke-virtual {v13}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v17

    .line 128
    .local v17, "sheetOffsets":Ljava/util/Hashtable;
    const/4 v9, 0x0

    .restart local v9    # "j":I
    :goto_6
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v9, v0, :cond_c

    .line 146
    .end local v9    # "j":I
    .end local v13    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v16    # "sheetIDs":[I
    .end local v17    # "sheetOffsets":Ljava/util/Hashtable;
    :cond_b
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 147
    .local v3, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v14, v3}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 148
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v20

    add-int v12, v12, v20

    .line 113
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4

    .line 129
    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v9    # "j":I
    .restart local v13    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .restart local v16    # "sheetIDs":[I
    .restart local v17    # "sheetOffsets":Ljava/util/Hashtable;
    :cond_c
    aget v20, v16, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 130
    .local v8, "id":Ljava/lang/Integer;
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    .line 135
    .local v11, "offset":Ljava/lang/Integer;
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v20

    invoke-static/range {v20 .. v20}, Lorg/apache/poi/hslf/dev/SlideIdListing;->findRecordAtPos(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    .line 139
    .local v2, "atPos":Lorg/apache/poi/hslf/record/Record;
    instance-of v0, v2, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    move/from16 v20, v0

    .line 128
    add-int/lit8 v9, v9, 0x1

    goto :goto_6
.end method
