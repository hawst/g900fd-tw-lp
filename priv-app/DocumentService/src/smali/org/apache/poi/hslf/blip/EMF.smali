.class public final Lorg/apache/poi/hslf/blip/EMF;
.super Lorg/apache/poi/hslf/blip/Metafile;
.source "EMF.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/poi/hslf/blip/Metafile;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 10

    .prologue
    .line 42
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/blip/EMF;->getRawData()[B

    move-result-object v7

    .line 44
    .local v7, "rawdata":[B
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 45
    .local v6, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 46
    .local v5, "is":Ljava/io/InputStream;
    new-instance v3, Lorg/apache/poi/hslf/blip/Metafile$Header;

    invoke-direct {v3}, Lorg/apache/poi/hslf/blip/Metafile$Header;-><init>()V

    .line 47
    .local v3, "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    const/16 v8, 0x10

    invoke-virtual {v3, v7, v8}, Lorg/apache/poi/hslf/blip/Metafile$Header;->read([BI)V

    .line 48
    invoke-virtual {v3}, Lorg/apache/poi/hslf/blip/Metafile$Header;->getSize()I

    move-result v8

    add-int/lit8 v8, v8, 0x10

    int-to-long v8, v8

    invoke-virtual {v5, v8, v9}, Ljava/io/InputStream;->skip(J)J

    .line 50
    new-instance v4, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v4, v5}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 51
    .local v4, "inflater":Ljava/util/zip/InflaterInputStream;
    const/16 v8, 0x1000

    new-array v0, v8, [B

    .line 53
    .local v0, "chunk":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/util/zip/InflaterInputStream;->read([B)I

    move-result v1

    .local v1, "count":I
    if-gez v1, :cond_0

    .line 56
    invoke-virtual {v4}, Ljava/util/zip/InflaterInputStream;->close()V

    .line 57
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    return-object v8

    .line 54
    :cond_0
    const/4 v8, 0x0

    invoke-virtual {v6, v0, v8, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    .end local v0    # "chunk":[B
    .end local v1    # "count":I
    .end local v3    # "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    .end local v4    # "inflater":Ljava/util/zip/InflaterInputStream;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v7    # "rawdata":[B
    :catch_0
    move-exception v2

    .line 59
    .local v2, "e":Ljava/io/IOException;
    new-instance v8, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v8, v2}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v8
.end method

.method public getSignature()I
    .locals 1

    .prologue
    .line 92
    const/16 v0, 0x3d40

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x2

    return v0
.end method

.method public setData([B)V
    .locals 7
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0xc8

    const/4 v5, 0x0

    .line 64
    array-length v4, p1

    invoke-virtual {p0, p1, v5, v4}, Lorg/apache/poi/hslf/blip/EMF;->compress([BII)[B

    move-result-object v1

    .line 66
    .local v1, "compressed":[B
    new-instance v2, Lorg/apache/poi/hslf/blip/Metafile$Header;

    invoke-direct {v2}, Lorg/apache/poi/hslf/blip/Metafile$Header;-><init>()V

    .line 67
    .local v2, "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    array-length v4, p1

    iput v4, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->wmfsize:I

    .line 69
    new-instance v4, Lorg/apache/poi/java/awt/Rectangle;

    invoke-direct {v4, v5, v5, v6, v6}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    iput-object v4, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    .line 70
    new-instance v4, Lorg/apache/poi/java/awt/Dimension;

    iget-object v5, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v5, v5, Lorg/apache/poi/java/awt/Rectangle;->width:I

    mul-int/lit16 v5, v5, 0x319c

    iget-object v6, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v6, v6, Lorg/apache/poi/java/awt/Rectangle;->height:I

    mul-int/lit16 v6, v6, 0x319c

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    iput-object v4, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->size:Lorg/apache/poi/java/awt/Dimension;

    .line 71
    array-length v4, v1

    iput v4, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->zipsize:I

    .line 73
    invoke-static {p1}, Lorg/apache/poi/hslf/blip/EMF;->getChecksum([B)[B

    move-result-object v0

    .line 74
    .local v0, "checksum":[B
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 75
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 76
    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/blip/Metafile$Header;->write(Ljava/io/OutputStream;)V

    .line 77
    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 79
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hslf/blip/EMF;->setRawData([B)V

    .line 80
    return-void
.end method
