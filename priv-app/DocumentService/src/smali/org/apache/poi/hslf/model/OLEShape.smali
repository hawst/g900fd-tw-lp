.class public final Lorg/apache/poi/hslf/model/OLEShape;
.super Lorg/apache/poi/hslf/model/Picture;
.source "OLEShape.java"


# instance fields
.field protected _exEmbed:Lorg/apache/poi/hslf/record/ExEmbed;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Picture;-><init>(I)V

    .line 44
    return-void
.end method

.method public constructor <init>(ILorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "idx"    # I
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Picture;-><init>(ILorg/apache/poi/hslf/model/Shape;)V

    .line 54
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Picture;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 65
    return-void
.end method


# virtual methods
.method public getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;
    .locals 9

    .prologue
    .line 119
    iget-object v6, p0, Lorg/apache/poi/hslf/model/OLEShape;->_exEmbed:Lorg/apache/poi/hslf/record/ExEmbed;

    if-nez v6, :cond_1

    .line 120
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v5

    .line 122
    .local v5, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Document;->getExObjList()Lorg/apache/poi/hslf/record/ExObjList;

    move-result-object v4

    .line 123
    .local v4, "lst":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v4, :cond_0

    .line 124
    iget-object v6, p0, Lorg/apache/poi/hslf/model/OLEShape;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x5

    const-string/jumbo v8, "ExObjList not found"

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 125
    const/4 v6, 0x0

    .line 137
    .end local v4    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    .end local v5    # "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    :goto_0
    return-object v6

    .line 128
    .restart local v4    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    .restart local v5    # "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getObjectID()I

    move-result v3

    .line 129
    .local v3, "id":I
    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/ExObjList;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 130
    .local v0, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v0

    if-lt v2, v6, :cond_2

    .line 137
    .end local v0    # "ch":[Lorg/apache/poi/hslf/record/Record;
    .end local v2    # "i":I
    .end local v3    # "id":I
    .end local v4    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    .end local v5    # "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/hslf/model/OLEShape;->_exEmbed:Lorg/apache/poi/hslf/record/ExEmbed;

    goto :goto_0

    .line 131
    .restart local v0    # "ch":[Lorg/apache/poi/hslf/record/Record;
    .restart local v2    # "i":I
    .restart local v3    # "id":I
    .restart local v4    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    .restart local v5    # "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    :cond_2
    aget-object v6, v0, v2

    instance-of v6, v6, Lorg/apache/poi/hslf/record/ExEmbed;

    if-eqz v6, :cond_3

    .line 132
    aget-object v1, v0, v2

    check-cast v1, Lorg/apache/poi/hslf/record/ExEmbed;

    .line 133
    .local v1, "embd":Lorg/apache/poi/hslf/record/ExEmbed;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/ExEmbed;->getExOleObjAtom()Lorg/apache/poi/hslf/record/ExOleObjAtom;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->getObjID()I

    move-result v6

    if-ne v6, v3, :cond_3

    iput-object v1, p0, Lorg/apache/poi/hslf/model/OLEShape;->_exEmbed:Lorg/apache/poi/hslf/record/ExEmbed;

    .line 130
    .end local v1    # "embd":Lorg/apache/poi/hslf/record/ExEmbed;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getFullName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExEmbed;->getClipboardName()Ljava/lang/String;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInstanceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExEmbed;->getMenuName()Ljava/lang/String;

    move-result-object v0

    .line 150
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObjectData()Lorg/apache/poi/hslf/usermodel/ObjectData;
    .locals 9

    .prologue
    .line 82
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v4

    .line 83
    .local v4, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getEmbeddedObjects()[Lorg/apache/poi/hslf/usermodel/ObjectData;

    move-result-object v3

    .line 86
    .local v3, "ole":[Lorg/apache/poi/hslf/usermodel/ObjectData;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v1

    .line 87
    .local v1, "exEmbed":Lorg/apache/poi/hslf/record/ExEmbed;
    const/4 v0, 0x0

    .line 88
    .local v0, "data":Lorg/apache/poi/hslf/usermodel/ObjectData;
    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/ExEmbed;->getExOleObjAtom()Lorg/apache/poi/hslf/record/ExOleObjAtom;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->getObjStgDataRef()I

    move-result v5

    .line 91
    .local v5, "ref":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v3

    if-lt v2, v6, :cond_2

    .line 97
    .end local v2    # "i":I
    .end local v5    # "ref":I
    :cond_0
    if-nez v0, :cond_1

    .line 98
    iget-object v6, p0, Lorg/apache/poi/hslf/model/OLEShape;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x5

    const-string/jumbo v8, "OLE data not found"

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 101
    :cond_1
    return-object v0

    .line 92
    .restart local v2    # "i":I
    .restart local v5    # "ref":I
    :cond_2
    aget-object v6, v3, v2

    invoke-virtual {v6}, Lorg/apache/poi/hslf/usermodel/ObjectData;->getExOleObjStg()Lorg/apache/poi/hslf/record/ExOleObjStg;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/ExOleObjStg;->getPersistId()I

    move-result v6

    if-ne v6, v5, :cond_3

    .line 93
    aget-object v0, v3, v2

    .line 91
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getObjectID()I
    .locals 1

    .prologue
    .line 73
    const/16 v0, 0x10b

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/OLEShape;->getEscherProperty(S)I

    move-result v0

    return v0
.end method

.method public getProgID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/OLEShape;->getExEmbed()Lorg/apache/poi/hslf/record/ExEmbed;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExEmbed;->getProgId()Ljava/lang/String;

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
