.class public Lorg/apache/poi/hslf/model/ShapeGroup;
.super Lorg/apache/poi/hslf/model/Shape;
.source "ShapeGroup.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0, v0}, Lorg/apache/poi/hslf/model/ShapeGroup;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/ShapeGroup;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 48
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Shape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 58
    return-void
.end method


# virtual methods
.method public addShape(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 3
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 202
    iget-object v1, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 204
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ShapeGroup;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v0

    .line 205
    .local v0, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    invoke-virtual {p1, v0}, Lorg/apache/poi/hslf/model/Shape;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 206
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/Sheet;->allocateShapeId()I

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/poi/hslf/model/Shape;->setShapeId(I)V

    .line 207
    invoke-virtual {p1, v0}, Lorg/apache/poi/hslf/model/Shape;->afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 208
    return-void
.end method

.method protected createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 8
    .param p1, "isChild"    # Z

    .prologue
    const/16 v7, 0xf

    .line 170
    new-instance v4, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 171
    .local v4, "spgr":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v6, -0xffd

    invoke-virtual {v4, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 172
    invoke-virtual {v4, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 175
    new-instance v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v2}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 176
    .local v2, "spcont":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v6, -0xffc

    invoke-virtual {v2, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 177
    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 179
    new-instance v3, Lorg/apache/poi/ddf/EscherSpgrRecord;

    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherSpgrRecord;-><init>()V

    .line 180
    .local v3, "spg":Lorg/apache/poi/ddf/EscherSpgrRecord;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setOptions(S)V

    .line 181
    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 183
    new-instance v1, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherSpRecord;-><init>()V

    .line 184
    .local v1, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    const/4 v5, 0x2

    .line 185
    .local v5, "type":S
    invoke-virtual {v1, v5}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 186
    const/16 v6, 0x201

    invoke-virtual {v1, v6}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 187
    invoke-virtual {v2, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 189
    new-instance v0, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;-><init>()V

    .line 190
    .local v0, "anchor":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    invoke-virtual {v2, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 192
    invoke-virtual {v4, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 193
    return-object v4
.end method

.method public getAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 11

    .prologue
    const/high16 v10, 0x44100000    # 576.0f

    const/high16 v9, 0x42900000    # 72.0f

    .line 238
    iget-object v4, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 239
    .local v3, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v4, -0xff0

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/ShapeGroup;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 240
    .local v1, "clientAnchor":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>()V

    .line 241
    .local v0, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    if-nez v1, :cond_0

    .line 242
    iget-object v4, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v5, 0x3

    const-string/jumbo v6, "EscherClientAnchorRecord was not found for shape group. Searching for EscherChildAnchorRecord."

    invoke-virtual {v4, v5, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 243
    const/16 v4, -0xff1

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/ShapeGroup;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherChildAnchorRecord;

    .line 244
    .local v2, "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    .line 245
    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx1()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    div-float/2addr v4, v10

    .line 246
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy1()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v9

    div-float/2addr v5, v10

    .line 247
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx2()I

    move-result v6

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx1()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v6, v9

    div-float/2addr v6, v10

    .line 248
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy2()I

    move-result v7

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy1()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v7, v9

    div-float/2addr v7, v10

    .line 244
    invoke-direct {v0, v4, v5, v6, v7}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 257
    .end local v2    # "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    :goto_0
    return-object v0

    .line 251
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    div-float/2addr v4, v10

    iput v4, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    .line 252
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    div-float/2addr v4, v10

    iput v4, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    .line 253
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v4

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v4, v9

    div-float/2addr v4, v10

    iput v4, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    .line 254
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v4

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v4, v9

    div-float/2addr v4, v10

    iput v4, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    goto :goto_0
.end method

.method public getCoordinates()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 7

    .prologue
    const/high16 v6, 0x44100000    # 576.0f

    const/high16 v5, 0x42900000    # 72.0f

    .line 154
    iget-object v3, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 155
    .local v1, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v3, -0xff7

    invoke-static {v1, v3}, Lorg/apache/poi/hslf/model/ShapeGroup;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSpgrRecord;

    .line 157
    .local v2, "spgr":Lorg/apache/poi/ddf/EscherSpgrRecord;
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>()V

    .line 158
    .local v0, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->getRectX1()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float/2addr v3, v6

    iput v3, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    .line 159
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->getRectY1()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float/2addr v3, v6

    iput v3, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    .line 160
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->getRectX2()I

    move-result v3

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->getRectX1()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float/2addr v3, v6

    iput v3, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    .line 161
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->getRectY2()I

    move-result v3

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->getRectY1()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float/2addr v3, v6

    iput v3, v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    .line 163
    return-object v0
.end method

.method public getHyperlink()Lorg/apache/poi/hslf/model/Hyperlink;
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShapeType()I
    .locals 4

    .prologue
    .line 267
    iget-object v2, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 268
    .local v0, "groupInfoContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v2, -0xff6

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 269
    .local v1, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v2

    shr-int/lit8 v2, v2, 0x4

    return v2
.end method

.method public getShapes()[Lorg/apache/poi/hslf/model/Shape;
    .locals 10

    .prologue
    .line 66
    iget-object v6, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 69
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 70
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 72
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v4, "shapeList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/Shape;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 89
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/poi/hslf/model/Shape;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lorg/apache/poi/hslf/model/Shape;

    .line 90
    .local v5, "shapes":[Lorg/apache/poi/hslf/model/Shape;
    return-object v5

    .line 74
    .end local v5    # "shapes":[Lorg/apache/poi/hslf/model/Shape;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 75
    .local v2, "r":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v6, v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    if-eqz v6, :cond_2

    move-object v0, v2

    .line 77
    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 78
    .local v0, "container":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-static {v0, p0}, Lorg/apache/poi/hslf/model/ShapeFactory;->createShape(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Shape;

    move-result-object v3

    .line 79
    .local v3, "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ShapeGroup;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/poi/hslf/model/Shape;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 80
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    .end local v0    # "container":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v3    # "shape":Lorg/apache/poi/hslf/model/Shape;
    :cond_2
    iget-object v6, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Shape contained non container escher record, was "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public moveTo(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 217
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ShapeGroup;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 218
    .local v0, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    sub-int v2, p1, v6

    .line 219
    .local v2, "dx":I
    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    sub-int v3, p2, v6

    .line 220
    .local v3, "dy":I
    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/java/awt/Rectangle;->translate(II)V

    .line 221
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/ShapeGroup;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 223
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ShapeGroup;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v5

    .line 224
    .local v5, "shape":[Lorg/apache/poi/hslf/model/Shape;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v6, v5

    if-lt v4, v6, :cond_0

    .line 229
    return-void

    .line 225
    :cond_0
    aget-object v6, v5, v4

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/Shape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v1

    .line 226
    .local v1, "chanchor":Lorg/apache/poi/java/awt/Rectangle;
    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/java/awt/Rectangle;->translate(II)V

    .line 227
    aget-object v6, v5, v4

    invoke-virtual {v6, v1}, Lorg/apache/poi/hslf/model/Shape;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 224
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V
    .locals 7
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    const/4 v6, 0x0

    .line 101
    iget-object v4, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v4, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 103
    .local v2, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v4, -0xff0

    invoke-static {v2, v4}, Lorg/apache/poi/hslf/model/ShapeGroup;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 106
    .local v0, "clientAnchor":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    const/16 v4, 0x10

    new-array v1, v4, [B

    .line 107
    .local v1, "header":[B
    invoke-static {v1, v6, v6}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 108
    const/4 v4, 0x2

    invoke-static {v1, v4, v6}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 109
    const/4 v4, 0x4

    const/16 v5, 0x8

    invoke-static {v1, v4, v5}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 110
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v6, v4}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 112
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    int-to-short v4, v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setFlag(S)V

    .line 113
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    int-to-short v4, v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setCol1(S)V

    .line 114
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    add-int/2addr v4, v5

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    int-to-short v4, v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setDx1(S)V

    .line 115
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    add-int/2addr v4, v5

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    int-to-short v4, v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setRow1(S)V

    .line 117
    const/16 v4, -0xff7

    invoke-static {v2, v4}, Lorg/apache/poi/hslf/model/ShapeGroup;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSpgrRecord;

    .line 119
    .local v3, "spgr":Lorg/apache/poi/ddf/EscherSpgrRecord;
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 120
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 121
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 122
    iget v4, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    mul-int/lit16 v4, v4, 0x240

    div-int/lit8 v4, v4, 0x48

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 123
    return-void
.end method

.method public setCoordinates(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 14
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    const-wide/high16 v12, 0x4082000000000000L    # 576.0

    const-wide/high16 v10, 0x4052000000000000L    # 72.0

    .line 132
    iget-object v6, p0, Lorg/apache/poi/hslf/model/ShapeGroup;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 133
    .local v0, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v6, -0xff7

    invoke-static {v0, v6}, Lorg/apache/poi/hslf/model/ShapeGroup;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSpgrRecord;

    .line 135
    .local v1, "spgr":Lorg/apache/poi/ddf/EscherSpgrRecord;
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    mul-double/2addr v6, v12

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v2, v6

    .line 136
    .local v2, "x1":I
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v6

    mul-double/2addr v6, v12

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v4, v6

    .line 137
    .local v4, "y1":I
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double/2addr v6, v8

    mul-double/2addr v6, v12

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v3, v6

    .line 138
    .local v3, "x2":I
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double/2addr v6, v8

    mul-double/2addr v6, v12

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v5, v6

    .line 140
    .local v5, "y2":I
    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 141
    invoke-virtual {v1, v4}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 142
    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 143
    invoke-virtual {v1, v5}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 145
    return-void
.end method
