.class public final Lorg/apache/poi/hslf/record/FontCollection;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "FontCollection.java"


# instance fields
.field private _header:[B

.field private fonts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>([BII)V
    .locals 6
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v4, 0x8

    .line 36
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 38
    new-array v2, v4, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->_header:[B

    .line 39
    iget-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->_header:[B

    const/4 v3, 0x0

    invoke-static {p1, p2, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 41
    add-int/lit8 v2, p2, 0x8

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v2, v3}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 44
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    .line 45
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 53
    return-void

    .line 46
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v1

    instance-of v2, v2, Lorg/apache/poi/hslf/record/FontEntityAtom;

    if-eqz v2, :cond_1

    .line 47
    iget-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v2, v1

    check-cast v0, Lorg/apache/poi/hslf/record/FontEntityAtom;

    .line 48
    .local v0, "atom":Lorg/apache/poi/hslf/record/FontEntityAtom;
    iget-object v2, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getFontName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    .end local v0    # "atom":Lorg/apache/poi/hslf/record/FontEntityAtom;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_1
    sget-object v2, Lorg/apache/poi/hslf/record/FontCollection;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Warning: FontCollection child wasn\'t a FontEntityAtom, was "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/poi/hslf/record/FontCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public addFont(Ljava/lang/String;)I
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/record/FontCollection;->getFontIndex(Ljava/lang/String;)I

    move-result v6

    .line 78
    .local v6, "idx":I
    const/4 v0, -0x1

    if-eq v6, v0, :cond_0

    .line 80
    .end local v6    # "idx":I
    :goto_0
    return v6

    .restart local v6    # "idx":I
    :cond_0
    const/4 v4, 0x4

    const/16 v5, 0x22

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hslf/record/FontCollection;->addFont(Ljava/lang/String;IIII)I

    move-result v6

    goto :goto_0
.end method

.method public addFont(Ljava/lang/String;IIII)I
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "charset"    # I
    .param p3, "flags"    # I
    .param p4, "type"    # I
    .param p5, "pitch"    # I

    .prologue
    .line 84
    new-instance v0, Lorg/apache/poi/hslf/record/FontEntityAtom;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/FontEntityAtom;-><init>()V

    .line 85
    .local v0, "fnt":Lorg/apache/poi/hslf/record/FontEntityAtom;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    shl-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->setFontIndex(I)V

    .line 86
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->setFontName(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/record/FontEntityAtom;->setCharSet(I)V

    .line 88
    invoke-virtual {v0, p3}, Lorg/apache/poi/hslf/record/FontEntityAtom;->setFontFlags(I)V

    .line 89
    invoke-virtual {v0, p4}, Lorg/apache/poi/hslf/record/FontEntityAtom;->setFontType(I)V

    .line 90
    invoke-virtual {v0, p5}, Lorg/apache/poi/hslf/record/FontEntityAtom;->setPitchAndFamily(I)V

    .line 91
    iget-object v1, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/record/FontCollection;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 96
    iget-object v1, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    return v1
.end method

.method public getFontIndex(Ljava/lang/String;)I
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 109
    const/4 v0, -0x1

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 104
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFontWithId(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNumberOfFonts()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontCollection;->fonts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->FontCollection:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontCollection;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontCollection;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/FontCollection;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/FontCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/FontCollection;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 68
    return-void
.end method
