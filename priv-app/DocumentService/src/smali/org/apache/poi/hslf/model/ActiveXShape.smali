.class public final Lorg/apache/poi/hslf/model/ActiveXShape;
.super Lorg/apache/poi/hslf/model/Picture;
.source "ActiveXShape.java"


# static fields
.field public static final DEFAULT_ACTIVEX_THUMBNAIL:I = -0x1


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "movieIdx"    # I
    .param p2, "pictureIdx"    # I

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lorg/apache/poi/hslf/model/Picture;-><init>(ILorg/apache/poi/hslf/model/Shape;)V

    .line 49
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/model/ActiveXShape;->setActiveXIndex(I)V

    .line 50
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Picture;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 61
    return-void
.end method


# virtual methods
.method protected afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 9
    .param p1, "sheet"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 160
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ActiveXShape;->getExControl()Lorg/apache/poi/hslf/record/ExControl;

    move-result-object v0

    .line 162
    .local v0, "ctrl":Lorg/apache/poi/hslf/record/ExControl;
    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExControl;->getExControlAtom()Lorg/apache/poi/hslf/record/ExControlAtom;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Sheet;->_getSheetNumber()I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/poi/hslf/record/ExControlAtom;->setSlideId(I)V

    .line 166
    :cond_0
    const/4 v3, 0x0

    .line 167
    .local v3, "name":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 168
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExControl;->getProgId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ActiveXShape;->getControlIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 169
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "UTF-16LE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 170
    .local v1, "data":[B
    new-instance v5, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 171
    const/16 v6, 0x380

    const/4 v7, 0x0

    .line 170
    invoke-direct {v5, v6, v7, v1}, Lorg/apache/poi/ddf/EscherComplexProperty;-><init>(SZ[B)V

    .line 173
    .local v5, "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    iget-object v6, p0, Lorg/apache/poi/hslf/model/ActiveXShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xff5

    .line 172
    invoke-static {v6, v7}, Lorg/apache/poi/hslf/model/ActiveXShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 174
    .local v4, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    if-eqz v4, :cond_2

    .line 175
    invoke-virtual {v4, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 181
    .end local v1    # "data":[B
    .end local v4    # "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    .end local v5    # "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    :cond_2
    :goto_0
    return-void

    .line 176
    :catch_0
    move-exception v2

    .line 177
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v6, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v6, v2}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 178
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v2

    .line 179
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "DocumentService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Exception: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 7
    .param p1, "idx"    # I
    .param p2, "isChild"    # Z

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hslf/model/Picture;->createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/poi/hslf/model/ActiveXShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 71
    iget-object v5, p0, Lorg/apache/poi/hslf/model/ActiveXShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v6, -0xff6

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 72
    .local v4, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v4, :cond_0

    .line 73
    const/16 v5, 0xa10

    invoke-virtual {v4, v5}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 75
    :cond_0
    const/16 v5, 0xc9

    invoke-virtual {p0, v5}, Lorg/apache/poi/hslf/model/ActiveXShape;->setShapeType(I)V

    .line 76
    const/16 v5, 0x10b

    invoke-virtual {p0, v5, p1}, Lorg/apache/poi/hslf/model/ActiveXShape;->setEscherProperty(SI)V

    .line 77
    const/16 v5, 0x1c0

    const v6, 0x8000001

    invoke-virtual {p0, v5, v6}, Lorg/apache/poi/hslf/model/ActiveXShape;->setEscherProperty(SI)V

    .line 78
    const/16 v5, 0x1ff

    const v6, 0x80008

    invoke-virtual {p0, v5, v6}, Lorg/apache/poi/hslf/model/ActiveXShape;->setEscherProperty(SI)V

    .line 79
    const/16 v5, 0x201

    const v6, 0x8000002

    invoke-virtual {p0, v5, v6}, Lorg/apache/poi/hslf/model/ActiveXShape;->setEscherProperty(SI)V

    .line 80
    const/16 v5, 0x7f

    const/4 v6, -0x1

    invoke-virtual {p0, v5, v6}, Lorg/apache/poi/hslf/model/ActiveXShape;->setEscherProperty(SI)V

    .line 82
    new-instance v0, Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientDataRecord;-><init>()V

    .line 83
    .local v0, "cldata":Lorg/apache/poi/ddf/EscherClientDataRecord;
    const/16 v5, 0xf

    invoke-virtual {v0, v5}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setOptions(S)V

    .line 84
    iget-object v5, p0, Lorg/apache/poi/hslf/model/ActiveXShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 86
    new-instance v2, Lorg/apache/poi/hslf/record/OEShapeAtom;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/OEShapeAtom;-><init>()V

    .line 89
    .local v2, "oe":Lorg/apache/poi/hslf/record/OEShapeAtom;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 91
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/record/OEShapeAtom;->writeOut(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setRemainingData([B)V

    .line 97
    iget-object v5, p0, Lorg/apache/poi/hslf/model/ActiveXShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v5

    .line 92
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v5, v1}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public getControlIndex()I
    .locals 3

    .prologue
    .line 119
    const/4 v0, -0x1

    .line 120
    .local v0, "idx":I
    sget-object v2, Lorg/apache/poi/hslf/record/RecordTypes;->OEShapeAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v2, v2, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v2}, Lorg/apache/poi/hslf/model/ActiveXShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/record/OEShapeAtom;

    .line 121
    .local v1, "oe":Lorg/apache/poi/hslf/record/OEShapeAtom;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/OEShapeAtom;->getOptions()I

    move-result v0

    .line 122
    :cond_0
    return v0
.end method

.method public getExControl()Lorg/apache/poi/hslf/record/ExControl;
    .locals 10

    .prologue
    .line 140
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ActiveXShape;->getControlIndex()I

    move-result v5

    .line 141
    .local v5, "idx":I
    const/4 v2, 0x0

    .line 142
    .local v2, "ctrl":Lorg/apache/poi/hslf/record/ExControl;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ActiveXShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v3

    .line 143
    .local v3, "doc":Lorg/apache/poi/hslf/record/Document;
    sget-object v7, Lorg/apache/poi/hslf/record/RecordTypes;->ExObjList:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v7, v7, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v8, v7

    invoke-virtual {v3, v8, v9}, Lorg/apache/poi/hslf/record/Document;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/hslf/record/ExObjList;

    .line 144
    .local v6, "lst":Lorg/apache/poi/hslf/record/ExObjList;
    if-eqz v6, :cond_0

    .line 145
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/ExObjList;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 146
    .local v1, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v7, v1

    if-lt v4, v7, :cond_1

    .line 156
    .end local v1    # "ch":[Lorg/apache/poi/hslf/record/Record;
    .end local v4    # "i":I
    :cond_0
    :goto_1
    return-object v2

    .line 147
    .restart local v1    # "ch":[Lorg/apache/poi/hslf/record/Record;
    .restart local v4    # "i":I
    :cond_1
    aget-object v7, v1, v4

    instance-of v7, v7, Lorg/apache/poi/hslf/record/ExControl;

    if-eqz v7, :cond_2

    .line 148
    aget-object v0, v1, v4

    check-cast v0, Lorg/apache/poi/hslf/record/ExControl;

    .line 149
    .local v0, "c":Lorg/apache/poi/hslf/record/ExControl;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExControl;->getExOleObjAtom()Lorg/apache/poi/hslf/record/ExOleObjAtom;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->getObjID()I

    move-result v7

    if-ne v7, v5, :cond_2

    .line 150
    move-object v2, v0

    .line 151
    goto :goto_1

    .line 146
    .end local v0    # "c":Lorg/apache/poi/hslf/record/ExControl;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public setActiveXIndex(I)V
    .locals 7
    .param p1, "idx"    # I

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/ActiveXShape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v4

    .line 108
    .local v4, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 116
    return-void

    .line 109
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 110
    .local v2, "obj":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v5

    const/16 v6, -0xfef

    if-ne v5, v6, :cond_0

    move-object v0, v2

    .line 111
    check-cast v0, Lorg/apache/poi/ddf/EscherClientDataRecord;

    .line 112
    .local v0, "clientRecord":Lorg/apache/poi/ddf/EscherClientDataRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientDataRecord;->getRemainingData()[B

    move-result-object v3

    .line 113
    .local v3, "recdata":[B
    const/16 v5, 0x8

    invoke-static {v3, v5, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    goto :goto_0
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 132
    return-void
.end method
