.class public Lorg/apache/poi/hslf/record/ExMCIMovie;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "ExMCIMovie.java"


# instance fields
.field private _header:[B

.field private exVideo:Lorg/apache/poi/hslf/record/ExVideoContainer;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 54
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    .line 56
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    const/16 v1, 0xf

    aput-byte v1, v0, v4

    .line 57
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExMCIMovie;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 59
    new-instance v0, Lorg/apache/poi/hslf/record/ExVideoContainer;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/ExVideoContainer;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->exVideo:Lorg/apache/poi/hslf/record/ExVideoContainer;

    .line 60
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->exVideo:Lorg/apache/poi/hslf/record/ExVideoContainer;

    aput-object v1, v0, v4

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 62
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 40
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 42
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    .line 43
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 47
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExMCIMovie;->findInterestingChildren()V

    .line 48
    return-void
.end method

.method private findInterestingChildren()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 72
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v4

    instance-of v0, v0, Lorg/apache/poi/hslf/record/ExVideoContainer;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v4

    check-cast v0, Lorg/apache/poi/hslf/record/ExVideoContainer;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->exVideo:Lorg/apache/poi/hslf/record/ExVideoContainer;

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    sget-object v0, Lorg/apache/poi/hslf/record/ExMCIMovie;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "First child record wasn\'t a ExVideoContainer, was of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getExVideo()Lorg/apache/poi/hslf/record/ExVideoContainer;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->exVideo:Lorg/apache/poi/hslf/record/ExVideoContainer;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 83
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExMCIMovie:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExMCIMovie;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/ExMCIMovie;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/ExMCIMovie;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 92
    return-void
.end method
