.class public final Lorg/apache/poi/hslf/record/SoundCollection;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "SoundCollection.java"


# instance fields
.field private _header:[B


# direct methods
.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 43
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 45
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/SoundCollection;->_header:[B

    .line 46
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SoundCollection;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/SoundCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 50
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->SoundCollection:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SoundCollection;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/SoundCollection;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/SoundCollection;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/SoundCollection;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/SoundCollection;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 71
    return-void
.end method
