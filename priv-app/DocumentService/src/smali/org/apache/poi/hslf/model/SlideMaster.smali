.class public final Lorg/apache/poi/hslf/model/SlideMaster;
.super Lorg/apache/poi/hslf/model/MasterSheet;
.source "SlideMaster.java"


# instance fields
.field private _runs:[Lorg/apache/poi/hslf/model/TextRun;

.field private _txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/MainMaster;I)V
    .locals 2
    .param p1, "record"    # Lorg/apache/poi/hslf/record/MainMaster;
    .param p2, "sheetNo"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/MasterSheet;-><init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V

    .line 47
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SlideMaster;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/hslf/model/SlideMaster;->findTextRuns(Lorg/apache/poi/hslf/record/PPDrawing;)[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 49
    return-void

    .line 48
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lorg/apache/poi/hslf/model/TextRun;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 5
    .param p1, "txtype"    # I
    .param p2, "level"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "isCharacter"    # Z

    .prologue
    const/4 v3, 0x0

    .line 71
    const/4 v1, 0x0

    .line 72
    .local v1, "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_1

    .line 78
    :cond_0
    if-nez v1, :cond_4

    .line 79
    if-eqz p4, :cond_5

    .line 80
    packed-switch p1, :pswitch_data_0

    .line 108
    :goto_1
    return-object v3

    .line 74
    :cond_1
    if-eqz p4, :cond_3

    iget-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getCharacterStyles()[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    move-result-object v2

    .line 75
    .local v2, "styles":[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :goto_2
    array-length v4, v2

    if-ge v0, v4, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {v4, p3}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    .line 76
    :cond_2
    if-nez v1, :cond_0

    .line 72
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 74
    .end local v2    # "styles":[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_3
    iget-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getParagraphStyles()[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    move-result-object v2

    goto :goto_2

    .line 84
    :pswitch_0
    const/4 p1, 0x1

    .line 106
    :goto_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/poi/hslf/model/SlideMaster;->getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    :cond_4
    move-object v3, v1

    .line 108
    goto :goto_1

    .line 87
    :pswitch_1
    const/4 p1, 0x0

    .line 88
    goto :goto_3

    .line 93
    :cond_5
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    .line 97
    :pswitch_2
    const/4 p1, 0x1

    .line 98
    goto :goto_3

    .line 100
    :pswitch_3
    const/4 p1, 0x0

    .line 101
    goto :goto_3

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 93
    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    return-object v0
.end method

.method public getTxMasterStyleAtoms()[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    return-object v0
.end method

.method protected onAddTextShape(Lorg/apache/poi/hslf/model/TextShape;)V
    .locals 5
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/TextShape;

    .prologue
    const/4 v4, 0x0

    .line 134
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 136
    .local v0, "run":Lorg/apache/poi/hslf/model/TextRun;
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    if-nez v2, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/poi/hslf/model/TextRun;

    aput-object v0, v2, v4

    iput-object v2, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    .line 143
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lorg/apache/poi/hslf/model/TextRun;

    .line 139
    .local v1, "tmp":[Lorg/apache/poi/hslf/model/TextRun;
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    iget-object v3, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    .line 141
    iput-object v1, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    goto :goto_0
.end method

.method public setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V
    .locals 6
    .param p1, "ss"    # Lorg/apache/poi/hslf/usermodel/SlideShow;

    .prologue
    .line 116
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/MasterSheet;->setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 119
    iget-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    if-nez v4, :cond_0

    .line 120
    const/16 v4, 0x9

    new-array v4, v4, [Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    iput-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    .line 122
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SlideMaster;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/Document;->getEnvironment()Lorg/apache/poi/hslf/record/Environment;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/Environment;->getTxMasterStyleAtom()Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    move-result-object v2

    .line 123
    .local v2, "txdoc":Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    iget-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getTextType()I

    move-result v5

    aput-object v2, v4, v5

    .line 125
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SlideMaster;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/record/MainMaster;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/MainMaster;->getTxMasterStyleAtoms()[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    move-result-object v3

    .line 126
    .local v3, "txrec":[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    .line 131
    .end local v0    # "i":I
    .end local v2    # "txdoc":Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    .end local v3    # "txrec":[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    :cond_0
    return-void

    .line 127
    .restart local v0    # "i":I
    .restart local v2    # "txdoc":Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    .restart local v3    # "txrec":[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    :cond_1
    aget-object v4, v3, v0

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getTextType()I

    move-result v1

    .line 128
    .local v1, "txType":I
    iget-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    aget-object v4, v4, v1

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/apache/poi/hslf/model/SlideMaster;->_txmaster:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    aget-object v5, v3, v0

    aput-object v5, v4, v1

    .line 126
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
