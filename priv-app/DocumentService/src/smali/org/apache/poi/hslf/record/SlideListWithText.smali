.class public final Lorg/apache/poi/hslf/record/SlideListWithText;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "SlideListWithText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    }
.end annotation


# static fields
.field public static final MASTER:I = 0x1

.field public static final NOTES:I = 0x2

.field public static final SLIDES:I

.field private static _type:J


# instance fields
.field private _header:[B

.field private slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const-wide/16 v0, 0xff0

    sput-wide v0, Lorg/apache/poi/hslf/record/SlideListWithText;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 117
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 118
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    .line 119
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    const/16 v1, 0xf

    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 120
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/poi/hslf/record/SlideListWithText;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 121
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 124
    new-array v0, v4, [Lorg/apache/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 125
    new-array v0, v4, [Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 126
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 10
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v8, 0x8

    const/4 v9, 0x0

    .line 74
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 76
    new-array v7, v8, [B

    iput-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    .line 77
    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    invoke-static {p1, p2, v7, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    add-int/lit8 v7, p2, 0x8

    add-int/lit8 v8, p3, -0x8

    invoke-static {p1, v7, v8}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 85
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 86
    .local v5, "sets":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v7, v7

    if-lt v3, v7, :cond_0

    .line 111
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v7

    new-array v7, v7, [Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    invoke-virtual {v5, v7}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iput-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 112
    return-void

    .line 87
    :cond_0
    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v7, v7, v3

    instance-of v7, v7, Lorg/apache/poi/hslf/record/SlidePersistAtom;

    if-eqz v7, :cond_3

    .line 89
    add-int/lit8 v2, v3, 0x1

    .line 90
    .local v2, "endPos":I
    :goto_1
    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v7, v7

    if-ge v2, v7, :cond_1

    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v7, v7, v2

    instance-of v7, v7, Lorg/apache/poi/hslf/record/SlidePersistAtom;

    if-eqz v7, :cond_4

    .line 94
    :cond_1
    sub-int v7, v2, v3

    add-int/lit8 v0, v7, -0x1

    .line 95
    .local v0, "clen":I
    const/4 v1, 0x0

    .line 96
    .local v1, "emptySet":Z
    if-nez v0, :cond_2

    const/4 v1, 0x1

    .line 100
    :cond_2
    new-array v6, v0, [Lorg/apache/poi/hslf/record/Record;

    .line 101
    .local v6, "spaChildren":[Lorg/apache/poi/hslf/record/Record;
    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    add-int/lit8 v8, v3, 0x1

    invoke-static {v7, v8, v6, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 102
    new-instance v4, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iget-object v7, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v7, v7, v3

    check-cast v7, Lorg/apache/poi/hslf/record/SlidePersistAtom;

    invoke-direct {v4, p0, v7, v6}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;-><init>(Lorg/apache/poi/hslf/record/SlideListWithText;Lorg/apache/poi/hslf/record/SlidePersistAtom;[Lorg/apache/poi/hslf/record/Record;)V

    .line 103
    .local v4, "set":Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    invoke-virtual {v5, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 106
    add-int/2addr v3, v0

    .line 86
    .end local v0    # "clen":I
    .end local v1    # "emptySet":Z
    .end local v2    # "endPos":I
    .end local v4    # "set":Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .end local v6    # "spaChildren":[Lorg/apache/poi/hslf/record/Record;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 91
    .restart local v2    # "endPos":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addSlidePersistAtom(Lorg/apache/poi/hslf/record/SlidePersistAtom;)V
    .locals 5
    .param p1, "spa"    # Lorg/apache/poi/hslf/record/SlidePersistAtom;

    .prologue
    const/4 v4, 0x0

    .line 135
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/record/SlideListWithText;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 137
    new-instance v0, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    new-array v2, v4, [Lorg/apache/poi/hslf/record/Record;

    invoke-direct {v0, p0, p1, v2}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;-><init>(Lorg/apache/poi/hslf/record/SlideListWithText;Lorg/apache/poi/hslf/record/SlidePersistAtom;[Lorg/apache/poi/hslf/record/Record;)V

    .line 140
    .local v0, "newSAS":Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    iget-object v2, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 141
    .local v1, "sas":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    iget-object v2, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iget-object v3, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    .line 143
    iput-object v1, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 144
    return-void
.end method

.method public getInstance()I
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 167
    sget-wide v0, Lorg/apache/poi/hslf/record/SlideListWithText;->_type:J

    return-wide v0
.end method

.method public getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    return-object v0
.end method

.method public setInstance(I)V
    .locals 2
    .param p1, "inst"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    shl-int/lit8 v1, p1, 0x4

    or-int/lit8 v1, v1, 0xf

    int-to-short v1, v1

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 152
    return-void
.end method

.method public setSlideAtomsSets([Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;)V
    .locals 0
    .param p1, "sas"    # [Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .prologue
    .line 162
    iput-object p1, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/SlideListWithText;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/SlideListWithText;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 175
    return-void
.end method
