.class public final Lorg/apache/poi/hslf/model/Notes;
.super Lorg/apache/poi/hslf/model/Sheet;
.source "Notes.java"


# instance fields
.field private _runs:[Lorg/apache/poi/hslf/model/TextRun;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/Notes;)V
    .locals 2
    .param p1, "notes"    # Lorg/apache/poi/hslf/record/Notes;

    .prologue
    .line 39
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/Notes;->getNotesAtom()Lorg/apache/poi/hslf/record/NotesAtom;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/NotesAtom;->getSlideID()I

    move-result v1

    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hslf/model/Sheet;-><init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V

    .line 44
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Notes;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/hslf/model/Notes;->findTextRuns(Lorg/apache/poi/hslf/record/PPDrawing;)[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/model/Notes;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Notes;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 49
    return-void

    .line 48
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Notes;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lorg/apache/poi/hslf/model/TextRun;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Notes;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    return-object v0
.end method
