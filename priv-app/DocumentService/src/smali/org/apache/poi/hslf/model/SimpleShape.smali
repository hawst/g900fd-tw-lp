.class public abstract Lorg/apache/poi/hslf/model/SimpleShape;
.super Lorg/apache/poi/hslf/model/Shape;
.source "SimpleShape.java"


# static fields
.field public static final DEFAULT_LINE_WIDTH:D = 0.75


# instance fields
.field protected _clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

.field protected _clientRecords:[Lorg/apache/poi/hslf/record/Record;


# direct methods
.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Shape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 53
    return-void
.end method


# virtual methods
.method protected createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 8
    .param p1, "isChild"    # Z

    .prologue
    const/4 v7, 0x0

    .line 62
    new-instance v5, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v5}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 63
    iget-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v6, -0xffc

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 64
    iget-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v6, 0xf

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 66
    new-instance v4, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-direct {v4}, Lorg/apache/poi/ddf/EscherSpRecord;-><init>()V

    .line 67
    .local v4, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    const/16 v1, 0xa00

    .line 68
    .local v1, "flags":I
    if-eqz p1, :cond_0

    or-int/lit8 v1, v1, 0x2

    .line 69
    :cond_0
    invoke-virtual {v4, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 70
    iget-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v5, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 72
    new-instance v3, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherOptRecord;-><init>()V

    .line 73
    .local v3, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v5, -0xff5

    invoke-virtual {v3, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->setRecordId(S)V

    .line 74
    iget-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v5, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 77
    if-eqz p1, :cond_1

    new-instance v0, Lorg/apache/poi/ddf/EscherChildAnchorRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;-><init>()V

    .line 89
    .local v0, "anchor":Lorg/apache/poi/ddf/EscherRecord;
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 91
    iget-object v5, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v5

    .line 79
    .end local v0    # "anchor":Lorg/apache/poi/ddf/EscherRecord;
    :cond_1
    new-instance v0, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;-><init>()V

    .line 83
    .restart local v0    # "anchor":Lorg/apache/poi/ddf/EscherRecord;
    const/16 v5, 0x10

    new-array v2, v5, [B

    .line 84
    .local v2, "header":[B
    invoke-static {v2, v7, v7}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 85
    const/4 v5, 0x2

    invoke-static {v2, v5, v7}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 86
    const/4 v5, 0x4

    const/16 v6, 0x8

    invoke-static {v2, v5, v6}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 87
    const/4 v5, 0x0

    invoke-virtual {v0, v2, v7, v5}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    goto :goto_0
.end method

.method protected getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;
    .locals 6
    .param p1, "recordType"    # I

    .prologue
    .line 252
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SimpleShape;->getClientRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 253
    .local v1, "records":[Lorg/apache/poi/hslf/record/Record;
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    .line 258
    .end local v0    # "i":I
    :cond_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 254
    .restart local v0    # "i":I
    :cond_1
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    int-to-long v4, p1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 255
    aget-object v2, v1, v0

    goto :goto_1

    .line 253
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getClientRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 267
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

    if-nez v2, :cond_1

    .line 268
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SimpleShape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xfef

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    .line 271
    .local v1, "r":Lorg/apache/poi/ddf/EscherRecord;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lorg/apache/poi/ddf/EscherClientDataRecord;

    if-nez v2, :cond_0

    .line 272
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherRecord;->serialize()[B

    move-result-object v0

    .line 273
    .local v0, "data":[B
    new-instance v1, Lorg/apache/poi/ddf/EscherClientDataRecord;

    .end local v1    # "r":Lorg/apache/poi/ddf/EscherRecord;
    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherClientDataRecord;-><init>()V

    .line 274
    .restart local v1    # "r":Lorg/apache/poi/ddf/EscherRecord;
    new-instance v2, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v2}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    invoke-virtual {v1, v0, v4, v2}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 276
    .end local v0    # "data":[B
    :cond_0
    check-cast v1, Lorg/apache/poi/ddf/EscherClientDataRecord;

    .end local v1    # "r":Lorg/apache/poi/ddf/EscherRecord;
    iput-object v1, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

    .line 278
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientRecords:[Lorg/apache/poi/hslf/record/Record;

    if-nez v2, :cond_2

    .line 279
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherClientDataRecord;->getRemainingData()[B

    move-result-object v0

    .line 280
    .restart local v0    # "data":[B
    array-length v2, v0

    invoke-static {v0, v4, v2}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientRecords:[Lorg/apache/poi/hslf/record/Record;

    .line 282
    .end local v0    # "data":[B
    :cond_2
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientRecords:[Lorg/apache/poi/hslf/record/Record;

    return-object v2
.end method

.method public getFillColor()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 190
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SimpleShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public getFlipHorizontal()Z
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff6

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 209
    .local v0, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v1

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFlipVertical()Z
    .locals 3

    .prologue
    .line 218
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff6

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 219
    .local v0, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLineColor()Lorg/apache/poi/java/awt/Color;
    .locals 6

    .prologue
    .line 133
    iget-object v3, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff5

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 135
    .local v1, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v3, 0x1ff

    invoke-static {v1, v3}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 136
    .local v2, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-nez v3, :cond_1

    const/4 v0, 0x0

    .line 139
    :cond_0
    :goto_0
    return-object v0

    .line 138
    :cond_1
    const/16 v3, 0x1c0

    const/16 v4, 0x1c1

    const/4 v5, -0x1

    invoke-virtual {p0, v3, v4, v5}, Lorg/apache/poi/hslf/model/SimpleShape;->getColor(SSI)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 139
    .local v0, "clr":Lorg/apache/poi/java/awt/Color;
    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    goto :goto_0
.end method

.method public getLineDashing()I
    .locals 4

    .prologue
    .line 148
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 150
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x1ce

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 151
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getLineStyle()I
    .locals 4

    .prologue
    .line 181
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 182
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x1cd

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 183
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getLineWidth()D
    .locals 8

    .prologue
    .line 98
    iget-object v4, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v5, -0xff5

    invoke-static {v4, v5}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 99
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v4, 0x1cb

    invoke-static {v0, v4}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 100
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    .line 101
    .local v2, "width":D
    :goto_0
    return-wide v2

    .line 100
    .end local v2    # "width":D
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x40c8ce0000000000L    # 12700.0

    div-double v2, v4, v6

    goto :goto_0
.end method

.method public getRotation()I
    .locals 3

    .prologue
    .line 228
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherProperty(S)I

    move-result v1

    .line 229
    .local v1, "rot":I
    shr-int/lit8 v2, v1, 0x10

    rem-int/lit16 v0, v2, 0x168

    .line 231
    .local v0, "angle":I
    return v0
.end method

.method public setFillColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 1
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 199
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SimpleShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/model/Fill;->setForegroundColor(Lorg/apache/poi/java/awt/Color;)V

    .line 200
    return-void
.end method

.method public setHyperlink(Lorg/apache/poi/hslf/model/Hyperlink;)V
    .locals 12
    .param p1, "link"    # Lorg/apache/poi/hslf/model/Hyperlink;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x3

    .line 300
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 301
    new-instance v5, Lorg/apache/poi/hslf/exceptions/HSLFException;

    const-string/jumbo v6, "You must call SlideShow.addHyperlink(Hyperlink link) first"

    invoke-direct {v5, v6}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 304
    :cond_0
    new-instance v0, Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientDataRecord;-><init>()V

    .line 305
    .local v0, "cldata":Lorg/apache/poi/ddf/EscherClientDataRecord;
    const/16 v5, 0xf

    invoke-virtual {v0, v5}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setOptions(S)V

    .line 306
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/SimpleShape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v5

    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 308
    new-instance v2, Lorg/apache/poi/hslf/record/InteractiveInfo;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/InteractiveInfo;-><init>()V

    .line 309
    .local v2, "info":Lorg/apache/poi/hslf/record/InteractiveInfo;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    move-result-object v3

    .line 311
    .local v3, "infoAtom":Lorg/apache/poi/hslf/record/InteractiveInfoAtom;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 339
    :goto_0
    :pswitch_0
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getId()I

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkID(I)V

    .line 341
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 343
    .local v4, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v2, v4}, Lorg/apache/poi/hslf/record/InteractiveInfo;->writeOut(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setRemainingData([B)V

    .line 349
    return-void

    .line 313
    .end local v4    # "out":Ljava/io/ByteArrayOutputStream;
    :pswitch_1
    invoke-virtual {v3, v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 314
    invoke-virtual {v3, v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 315
    invoke-virtual {v3, v10}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    goto :goto_0

    .line 318
    :pswitch_2
    invoke-virtual {v3, v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 319
    invoke-virtual {v3, v11}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 320
    invoke-virtual {v3, v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    goto :goto_0

    .line 323
    :pswitch_3
    invoke-virtual {v3, v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 324
    invoke-virtual {v3, v9}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 325
    invoke-virtual {v3, v8}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    goto :goto_0

    .line 328
    :pswitch_4
    invoke-virtual {v3, v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 329
    invoke-virtual {v3, v10}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 330
    invoke-virtual {v3, v9}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    goto :goto_0

    .line 333
    :pswitch_5
    invoke-virtual {v3, v11}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 334
    invoke-virtual {v3, v8}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 335
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    goto :goto_0

    .line 344
    .restart local v4    # "out":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v1

    .line 345
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v5, v1}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 311
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public setLineColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 8
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    const/16 v7, 0x1ff

    .line 119
    iget-object v2, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 120
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    if-nez p1, :cond_0

    .line 121
    const/high16 v2, 0x80000

    invoke-static {v0, v7, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 127
    :goto_0
    return-void

    .line 123
    :cond_0
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v1

    .line 124
    .local v1, "rgb":I
    const/16 v2, 0x1c0

    invoke-static {v0, v2, v1}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 125
    if-nez p1, :cond_1

    const v2, 0x180010

    :goto_1
    invoke-static {v0, v7, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    goto :goto_0

    :cond_1
    const v2, 0x180018

    goto :goto_1
.end method

.method public setLineDashing(I)V
    .locals 3
    .param p1, "pen"    # I

    .prologue
    .line 160
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff5

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 162
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v1, 0x1ce

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/4 p1, -0x1

    .end local p1    # "pen":I
    :cond_0
    invoke-static {v0, v1, p1}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 163
    return-void
.end method

.method public setLineStyle(I)V
    .locals 3
    .param p1, "style"    # I

    .prologue
    .line 171
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff5

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 172
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v1, 0x1cd

    if-nez p1, :cond_0

    const/4 p1, -0x1

    .end local p1    # "style":I
    :cond_0
    invoke-static {v0, v1, p1}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 173
    return-void
.end method

.method public setLineWidth(D)V
    .locals 5
    .param p1, "width"    # D

    .prologue
    .line 109
    iget-object v1, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff5

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 110
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v1, 0x1cb

    const-wide v2, 0x40c8ce0000000000L    # 12700.0

    mul-double/2addr v2, p1

    double-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 111
    return-void
.end method

.method public setRotation(I)V
    .locals 2
    .param p1, "theta"    # I

    .prologue
    .line 240
    const/4 v0, 0x4

    shl-int/lit8 v1, p1, 0x10

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/SimpleShape;->setEscherProperty(SI)V

    .line 241
    return-void
.end method

.method protected updateClientData()V
    .locals 5

    .prologue
    .line 286
    iget-object v3, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientRecords:[Lorg/apache/poi/hslf/record/Record;

    if-eqz v3, :cond_0

    .line 287
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 289
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientRecords:[Lorg/apache/poi/hslf/record/Record;

    array-length v3, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v1, v3, :cond_1

    .line 295
    iget-object v3, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientData:Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setRemainingData([B)V

    .line 297
    .end local v1    # "i":I
    .end local v2    # "out":Ljava/io/ByteArrayOutputStream;
    :cond_0
    return-void

    .line 290
    .restart local v1    # "i":I
    .restart local v2    # "out":Ljava/io/ByteArrayOutputStream;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/SimpleShape;->_clientRecords:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 289
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 292
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v3, v0}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
