.class public final Lorg/apache/poi/hslf/model/Placeholder;
.super Lorg/apache/poi/hslf/model/TextBox;
.source "Placeholder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/poi/hslf/model/TextBox;-><init>()V

    .line 43
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/TextBox;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/TextBox;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 8
    .param p1, "isChild"    # Z

    .prologue
    .line 51
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/TextBox;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/poi/hslf/model/Placeholder;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 53
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Placeholder;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 54
    const/16 v7, -0xff6

    invoke-virtual {v6, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 55
    .local v5, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v5, :cond_0

    .line 56
    const/16 v6, 0x220

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 59
    :cond_0
    new-instance v0, Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientDataRecord;-><init>()V

    .line 60
    .local v0, "cldata":Lorg/apache/poi/ddf/EscherClientDataRecord;
    const/16 v6, 0xf

    invoke-virtual {v0, v6}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setOptions(S)V

    .line 63
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Placeholder;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xff5

    .line 62
    invoke-static {v6, v7}, Lorg/apache/poi/hslf/model/Placeholder;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 65
    .local v3, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    if-eqz v3, :cond_1

    .line 67
    const/16 v6, 0x7f

    const/high16 v7, 0x40000

    .line 66
    invoke-static {v3, v6, v7}, Lorg/apache/poi/hslf/model/Placeholder;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 71
    :cond_1
    new-instance v2, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;-><init>()V

    .line 79
    .local v2, "oep":Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
    const/4 v6, -0x1

    invoke-virtual {v2, v6}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->setPlacementId(I)V

    .line 81
    const/16 v6, 0xe

    invoke-virtual {v2, v6}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->setPlaceholderId(B)V

    .line 84
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 86
    .local v4, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v2, v4}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->writeOut(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setRemainingData([B)V

    .line 93
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Placeholder;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xff3

    invoke-virtual {v6, v0, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildBefore(Lorg/apache/poi/ddf/EscherRecord;I)V

    .line 94
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Placeholder;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v6

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    new-instance v6, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v6, v1}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method
