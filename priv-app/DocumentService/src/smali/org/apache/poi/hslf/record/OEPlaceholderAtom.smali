.class public final Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "OEPlaceholderAtom.java"


# static fields
.field public static final Body:B = 0xet

.field public static final CenteredTitle:B = 0xft

.field public static final ClipArt:B = 0x16t

.field public static final Graph:B = 0x14t

.field public static final MasterBody:B = 0x2t

.field public static final MasterCenteredTitle:B = 0x3t

.field public static final MasterDate:B = 0x7t

.field public static final MasterFooter:B = 0x9t

.field public static final MasterHeader:B = 0xat

.field public static final MasterNotesBody:B = 0x6t

.field public static final MasterNotesSlideImage:B = 0x5t

.field public static final MasterSlideNumber:B = 0x8t

.field public static final MasterSubTitle:B = 0x4t

.field public static final MasterTitle:B = 0x1t

.field public static final MediaClip:B = 0x18t

.field public static final None:B = 0x0t

.field public static final NotesBody:B = 0xct

.field public static final NotesSlideImage:B = 0xbt

.field public static final Object:B = 0x13t

.field public static final OrganizationChart:B = 0x17t

.field public static final PLACEHOLDER_FULLSIZE:I = 0x0

.field public static final PLACEHOLDER_HALFSIZE:I = 0x1

.field public static final PLACEHOLDER_QUARTSIZE:I = 0x2

.field public static final Subtitle:B = 0x10t

.field public static final Table:B = 0x15t

.field public static final Title:B = 0xdt

.field public static final VerticalTextBody:B = 0x12t

.field public static final VerticalTextTitle:B = 0x11t


# instance fields
.field private _header:[B

.field private placeholderId:I

.field private placeholderSize:I

.field private placementId:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 209
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 210
    new-array v0, v5, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    .line 211
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    invoke-static {v0, v4, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 212
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 213
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v5}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 215
    iput v4, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placementId:I

    .line 216
    iput v4, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderId:I

    .line 217
    iput v4, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderSize:I

    .line 218
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    .line 223
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 224
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    .line 225
    move v0, p2

    .line 226
    .local v0, "offset":I
    iget-object v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    iget-object v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 229
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placementId:I

    add-int/lit8 v0, v0, 0x4

    .line 230
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderId:I

    add-int/lit8 v0, v0, 0x1

    .line 231
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderSize:I

    add-int/lit8 v0, v0, 0x1

    .line 232
    return-void
.end method


# virtual methods
.method public getPlaceholderId()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderId:I

    return v0
.end method

.method public getPlaceholderSize()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderSize:I

    return v0
.end method

.method public getPlacementId()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placementId:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 237
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->OEPlaceholderAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setPlaceholderId(B)V
    .locals 0
    .param p1, "id"    # B

    .prologue
    .line 291
    iput p1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderId:I

    .line 292
    return-void
.end method

.method public setPlaceholderSize(B)V
    .locals 0
    .param p1, "size"    # B

    .prologue
    .line 311
    iput p1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderSize:I

    .line 312
    return-void
.end method

.method public setPlacementId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 264
    iput p1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placementId:I

    .line 265
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    iget-object v1, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 321
    const/16 v1, 0x8

    new-array v0, v1, [B

    .line 322
    .local v0, "recdata":[B
    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placementId:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 323
    const/4 v1, 0x4

    iget v2, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderId:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 324
    const/4 v1, 0x5

    iget v2, p0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->placeholderSize:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 326
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 327
    return-void
.end method
