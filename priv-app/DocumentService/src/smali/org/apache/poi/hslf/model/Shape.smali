.class public abstract Lorg/apache/poi/hslf/model/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# static fields
.field public static final EMU_PER_CENTIMETER:I = 0x57e40

.field public static final EMU_PER_INCH:I = 0xdf3e0

.field public static final EMU_PER_POINT:I = 0x319c

.field public static final MASTER_DPI:I = 0x240

.field public static final PIXEL_DPI:I = 0x60

.field public static final POINT_DPI:I = 0x48


# instance fields
.field protected _escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

.field protected _fill:Lorg/apache/poi/hslf/model/Fill;

.field protected _parent:Lorg/apache/poi/hslf/model/Shape;

.field protected _sheet:Lorg/apache/poi/hslf/model/Sheet;

.field protected logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->logger:Lorg/apache/poi/util/POILogger;

    .line 106
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 107
    iput-object p2, p0, Lorg/apache/poi/hslf/model/Shape;->_parent:Lorg/apache/poi/hslf/model/Shape;

    .line 108
    return-void
.end method

.method public static getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;
    .locals 3
    .param p0, "owner"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "recordId"    # I

    .prologue
    .line 252
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 258
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 254
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 255
    .local v0, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v2

    if-ne v2, p1, :cond_0

    goto :goto_0
.end method

.method public static getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;
    .locals 3
    .param p0, "opt"    # Lorg/apache/poi/ddf/EscherOptRecord;
    .param p1, "propId"    # I

    .prologue
    .line 267
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 273
    .end local v0    # "iterator":Ljava/util/Iterator;
    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 269
    .restart local v0    # "iterator":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherProperty;

    .line 270
    .local v1, "prop":Lorg/apache/poi/ddf/EscherProperty;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v2

    if-ne v2, p1, :cond_0

    goto :goto_0
.end method

.method public static normalizeAngle(I)I
    .locals 2
    .param p0, "paramInt"    # I

    .prologue
    .line 690
    if-gez p0, :cond_0

    .line 692
    neg-int v1, p0

    rem-int/lit16 v0, v1, 0x168

    .line 693
    .local v0, "i":I
    rsub-int p0, v0, 0x168

    .line 695
    .end local v0    # "i":I
    :cond_0
    const/16 v1, 0x168

    if-lt p0, v1, :cond_1

    .line 696
    rem-int/lit16 p0, p0, 0x168

    .line 697
    :cond_1
    return p0
.end method

.method public static setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V
    .locals 4
    .param p0, "opt"    # Lorg/apache/poi/ddf/EscherOptRecord;
    .param p1, "propId"    # S
    .param p2, "value"    # I

    .prologue
    .line 284
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v2

    .line 285
    .local v2, "props":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 291
    const/4 v3, -0x1

    if-eq p2, v3, :cond_1

    .line 292
    new-instance v3, Lorg/apache/poi/ddf/EscherSimpleProperty;

    invoke-direct {v3, p1, p2}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {p0, v3}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 293
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherOptRecord;->sortProperties()V

    .line 295
    :cond_1
    return-void

    .line 286
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherProperty;

    .line 287
    .local v1, "prop":Lorg/apache/poi/ddf/EscherProperty;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherProperty;->getId()S

    move-result v3

    if-ne v3, p1, :cond_0

    .line 288
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method


# virtual methods
.method protected afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 1
    .param p1, "sh"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 519
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_fill:Lorg/apache/poi/hslf/model/Fill;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_fill:Lorg/apache/poi/hslf/model/Fill;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/model/Fill;->afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 522
    :cond_0
    return-void
.end method

.method protected abstract createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
.end method

.method public draw(Lorg/apache/poi/java/awt/Graphics2D;)V
    .locals 4
    .param p1, "graphics"    # Lorg/apache/poi/java/awt/Graphics2D;

    .prologue
    .line 670
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Rendering "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getShapeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 671
    return-void
.end method

.method public getAnchor()Lorg/apache/poi/java/awt/Rectangle;
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v0

    .line 156
    .local v0, "anchor2d":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getBounds()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v1

    return-object v1
.end method

.method public getAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 12

    .prologue
    const/16 v8, -0xff0

    const/high16 v11, 0x44100000    # 576.0f

    const/high16 v10, 0x42900000    # 72.0f

    .line 166
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v6, -0xff6

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 167
    .local v4, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v2

    .line 168
    .local v2, "flags":I
    const/4 v0, 0x0

    .line 169
    .local v0, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    and-int/lit8 v5, v2, 0x2

    if-eqz v5, :cond_1

    .line 170
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v6, -0xff1

    invoke-static {v5, v6}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherChildAnchorRecord;

    .line 171
    .local v3, "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    new-instance v0, Lorg/apache/poi/java/awt/Rectangle;

    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-direct {v0}, Lorg/apache/poi/java/awt/Rectangle;-><init>()V

    .line 172
    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    if-nez v3, :cond_0

    .line 173
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Shape;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v6, 0x5

    const-string/jumbo v7, "EscherSpRecord.FLAG_CHILD is set but EscherChildAnchorRecord was not found"

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 174
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-static {v5, v8}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 175
    .local v1, "clrec":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    new-instance v0, Lorg/apache/poi/java/awt/Rectangle;

    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-direct {v0}, Lorg/apache/poi/java/awt/Rectangle;-><init>()V

    .line 176
    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    .line 177
    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v10

    div-float/2addr v5, v11

    .line 178
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v11

    .line 179
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v7

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v7, v10

    div-float/2addr v7, v11

    .line 180
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v8

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v10

    div-float/2addr v8, v11

    .line 176
    invoke-direct {v0, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 201
    .end local v1    # "clrec":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    .end local v3    # "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :goto_0
    return-object v0

    .line 183
    .restart local v3    # "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    :cond_0
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    .line 184
    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx1()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v10

    div-float/2addr v5, v11

    .line 185
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy1()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v11

    .line 186
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx2()I

    move-result v7

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDx1()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v7, v10

    div-float/2addr v7, v11

    .line 187
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy2()I

    move-result v8

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->getDy1()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v10

    div-float/2addr v8, v11

    .line 183
    invoke-direct {v0, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 190
    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    goto :goto_0

    .line 192
    .end local v3    # "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-static {v5, v8}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 193
    .local v3, "rec":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    new-instance v0, Lorg/apache/poi/java/awt/Rectangle;

    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-direct {v0}, Lorg/apache/poi/java/awt/Rectangle;-><init>()V

    .line 194
    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    .line 195
    .end local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v10

    div-float/2addr v5, v11

    .line 196
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v10

    div-float/2addr v6, v11

    .line 197
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v7

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v7, v10

    div-float/2addr v7, v11

    .line 198
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v8

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v10

    div-float/2addr v8, v11

    .line 194
    invoke-direct {v0, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .restart local v0    # "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    goto :goto_0
.end method

.method getColor(SSI)Lorg/apache/poi/java/awt/Color;
    .locals 32
    .param p1, "colorProperty"    # S
    .param p2, "opacityProperty"    # S
    .param p3, "defaultColor"    # I

    .prologue
    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    move-object/from16 v26, v0

    const/16 v27, -0xff5

    invoke-static/range {v26 .. v27}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v20

    check-cast v20, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 542
    .local v20, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    move-object/from16 v0, v20

    move/from16 v1, p1

    invoke-static {v0, v1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v21

    check-cast v21, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 543
    .local v21, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v21, :cond_0

    const/16 v26, -0x1

    move/from16 v0, p3

    move/from16 v1, v26

    if-ne v0, v1, :cond_0

    const/16 v26, 0x0

    .line 584
    :goto_0
    return-object v26

    .line 545
    :cond_0
    if-nez v21, :cond_2

    move/from16 v25, p3

    .line 547
    .local v25, "val":I
    :goto_1
    shr-int/lit8 v26, v25, 0x18

    move/from16 v0, v26

    and-int/lit16 v4, v0, 0xff

    .line 548
    .local v4, "a":I
    shr-int/lit8 v26, v25, 0x10

    move/from16 v0, v26

    and-int/lit16 v5, v0, 0xff

    .line 549
    .local v5, "b":I
    shr-int/lit8 v26, v25, 0x8

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 550
    .local v16, "g":I
    shr-int/lit8 v26, v25, 0x0

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    .line 552
    .local v22, "r":I
    and-int/lit8 v26, v4, 0x1

    if-eqz v26, :cond_3

    const/4 v11, 0x1

    .line 553
    .local v11, "fPaletteIndex":Z
    :goto_2
    and-int/lit8 v26, v4, 0x2

    if-eqz v26, :cond_4

    const/4 v12, 0x1

    .line 554
    .local v12, "fPaletteRGB":Z
    :goto_3
    and-int/lit8 v26, v4, 0x4

    if-eqz v26, :cond_5

    const/4 v15, 0x1

    .line 555
    .local v15, "fSystemRGB":Z
    :goto_4
    and-int/lit8 v26, v4, 0x8

    if-eqz v26, :cond_6

    const/4 v13, 0x1

    .line 556
    .local v13, "fSchemeIndex":Z
    :goto_5
    and-int/lit8 v26, v4, 0x10

    if-eqz v26, :cond_7

    const/4 v14, 0x1

    .line 558
    .local v14, "fSysIndex":Z
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v24

    .line 559
    .local v24, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    if-eqz v13, :cond_8

    if-eqz v24, :cond_8

    .line 562
    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hslf/model/Sheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v8

    .line 563
    .local v8, "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    move/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->getColor(I)I

    move-result v23

    .line 565
    .local v23, "schemeColor":I
    shr-int/lit8 v26, v23, 0x0

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    .line 566
    shr-int/lit8 v26, v23, 0x8

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 567
    shr-int/lit8 v26, v23, 0x10

    move/from16 v0, v26

    and-int/lit16 v5, v0, 0xff

    .line 578
    .end local v8    # "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .end local v23    # "schemeColor":I
    :cond_1
    :goto_7
    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-static {v0, v1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v18

    check-cast v18, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 579
    .local v18, "op":Lorg/apache/poi/ddf/EscherSimpleProperty;
    const/high16 v9, 0x10000

    .line 580
    .local v9, "defaultOpacity":I
    if-nez v18, :cond_9

    move/from16 v19, v9

    .line 581
    .local v19, "opacity":I
    :goto_8
    shr-int/lit8 v17, v19, 0x10

    .line 582
    .local v17, "i":I
    shr-int/lit8 v26, v19, 0x0

    const v27, 0xffff

    and-int v10, v26, v27

    .line 583
    .local v10, "f":I
    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v26, v0

    int-to-double v0, v10

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x40f0000000000000L    # 65536.0

    div-double v28, v28, v30

    add-double v26, v26, v28

    const-wide v28, 0x406fe00000000000L    # 255.0

    mul-double v6, v26, v28

    .line 584
    .local v6, "alpha":D
    new-instance v26, Lorg/apache/poi/java/awt/Color;

    double-to-int v0, v6

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v22

    move/from16 v2, v16

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v5, v3}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    goto/16 :goto_0

    .line 545
    .end local v4    # "a":I
    .end local v5    # "b":I
    .end local v6    # "alpha":D
    .end local v9    # "defaultOpacity":I
    .end local v10    # "f":I
    .end local v11    # "fPaletteIndex":Z
    .end local v12    # "fPaletteRGB":Z
    .end local v13    # "fSchemeIndex":Z
    .end local v14    # "fSysIndex":Z
    .end local v15    # "fSystemRGB":Z
    .end local v16    # "g":I
    .end local v17    # "i":I
    .end local v18    # "op":Lorg/apache/poi/ddf/EscherSimpleProperty;
    .end local v19    # "opacity":I
    .end local v22    # "r":I
    .end local v24    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    .end local v25    # "val":I
    :cond_2
    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v25

    goto/16 :goto_1

    .line 552
    .restart local v4    # "a":I
    .restart local v5    # "b":I
    .restart local v16    # "g":I
    .restart local v22    # "r":I
    .restart local v25    # "val":I
    :cond_3
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 553
    .restart local v11    # "fPaletteIndex":Z
    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_3

    .line 554
    .restart local v12    # "fPaletteRGB":Z
    :cond_5
    const/4 v15, 0x0

    goto :goto_4

    .line 555
    .restart local v15    # "fSystemRGB":Z
    :cond_6
    const/4 v13, 0x0

    goto :goto_5

    .line 556
    .restart local v13    # "fSchemeIndex":Z
    :cond_7
    const/4 v14, 0x0

    goto :goto_6

    .line 568
    .restart local v14    # "fSysIndex":Z
    .restart local v24    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    :cond_8
    if-nez v11, :cond_1

    .line 570
    if-nez v12, :cond_1

    .line 572
    if-nez v15, :cond_1

    goto :goto_7

    .line 580
    .restart local v9    # "defaultOpacity":I
    .restart local v18    # "op":Lorg/apache/poi/ddf/EscherSimpleProperty;
    :cond_9
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v19

    goto :goto_8
.end method

.method public getEscherProperty(S)I
    .locals 8
    .param p1, "propId"    # S

    .prologue
    const/4 v5, 0x0

    .line 314
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xff5

    invoke-static {v6, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 315
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    invoke-static {v2, p1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    .line 316
    .local v3, "prop":Lorg/apache/poi/ddf/EscherProperty;
    if-nez v3, :cond_1

    .line 339
    :cond_0
    :goto_0
    return v5

    .line 318
    :cond_1
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherProperty;->isComplex()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 320
    invoke-static {v2, p1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 322
    .local v0, "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v1

    .line 325
    .local v1, "data":[B
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherComplexProperty;->getPropertyNumber()S

    move-result v6

    const/16 v7, 0x145

    if-ne v6, v7, :cond_0

    .line 326
    invoke-static {v1}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v5

    goto :goto_0

    .line 337
    .end local v0    # "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    .end local v1    # "data":[B
    :cond_2
    invoke-static {v2, p1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 339
    .local v4, "propSimple":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v5

    goto :goto_0
.end method

.method public getEscherProperty(SI)I
    .locals 4
    .param p1, "propId"    # S
    .param p2, "defaultValue"    # I

    .prologue
    .line 497
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 498
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    invoke-static {v0, p1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 499
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    .end local p2    # "defaultValue":I
    :goto_0
    return p2

    .restart local p2    # "defaultValue":I
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result p2

    goto :goto_0
.end method

.method public getFill()Lorg/apache/poi/hslf/model/Fill;
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_fill:Lorg/apache/poi/hslf/model/Fill;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/poi/hslf/model/Fill;

    invoke-direct {v0, p0}, Lorg/apache/poi/hslf/model/Fill;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_fill:Lorg/apache/poi/hslf/model/Fill;

    .line 655
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_fill:Lorg/apache/poi/hslf/model/Fill;

    return-object v0
.end method

.method public getHyperlink()Lorg/apache/poi/hslf/model/Hyperlink;
    .locals 1

    .prologue
    .line 666
    invoke-static {p0}, Lorg/apache/poi/hslf/model/Hyperlink;->find(Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Hyperlink;

    move-result-object v0

    return-object v0
.end method

.method public getLineColor()Lorg/apache/poi/java/awt/Color;
    .locals 12

    .prologue
    const/high16 v11, 0x8000000

    const/4 v7, 0x0

    .line 609
    iget-object v9, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v10, -0xff5

    invoke-static {v9, v10}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 611
    .local v3, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v9, 0x1c0

    invoke-static {v3, v9}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 612
    .local v4, "p1":Lorg/apache/poi/ddf/EscherSimpleProperty;
    const/16 v9, 0x1ff

    invoke-static {v3, v9}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 613
    .local v5, "p2":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v5, :cond_3

    move v6, v7

    .line 614
    .local v6, "p2val":I
    :goto_0
    const/4 v1, 0x0

    .line 615
    .local v1, "clr":Lorg/apache/poi/java/awt/Color;
    and-int/lit8 v9, v6, 0x8

    if-nez v9, :cond_0

    and-int/lit8 v9, v6, 0x10

    if-eqz v9, :cond_2

    .line 616
    :cond_0
    if-nez v4, :cond_4

    .line 617
    .local v7, "rgb":I
    :goto_1
    if-lt v7, v11, :cond_1

    .line 618
    rem-int v2, v7, v11

    .line 619
    .local v2, "idx":I
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 620
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/hslf/model/Sheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v0

    .line 621
    .local v0, "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    if-ltz v2, :cond_1

    const/4 v9, 0x7

    if-gt v2, v9, :cond_1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->getColor(I)I

    move-result v7

    .line 624
    .end local v0    # "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .end local v2    # "idx":I
    :cond_1
    new-instance v8, Lorg/apache/poi/java/awt/Color;

    const/4 v9, 0x1

    invoke-direct {v8, v7, v9}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 625
    .local v8, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    .end local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v9

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v10

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v11

    invoke-direct {v1, v9, v10, v11}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 627
    .end local v7    # "rgb":I
    .end local v8    # "tmp":Lorg/apache/poi/java/awt/Color;
    .restart local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    :cond_2
    return-object v1

    .line 613
    .end local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    .end local v6    # "p2val":I
    :cond_3
    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v6

    goto :goto_0

    .line 616
    .restart local v1    # "clr":Lorg/apache/poi/java/awt/Color;
    .restart local v6    # "p2val":I
    :cond_4
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v7

    goto :goto_1
.end method

.method public getLogicalAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v0

    return-object v0
.end method

.method public getOutline()Lorg/apache/poi/java/awt/Shape;
    .locals 1

    .prologue
    .line 679
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getLogicalAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v0

    return-object v0
.end method

.method public getParent()Lorg/apache/poi/hslf/model/Shape;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_parent:Lorg/apache/poi/hslf/model/Shape;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(S)I

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/hslf/model/Shape;->normalizeAngle(I)I

    move-result v0

    return v0
.end method

.method public getSegmentCount(S)[I
    .locals 12
    .param p1, "propId"    # S

    .prologue
    .line 385
    iget-object v10, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v11, -0xff5

    invoke-static {v10, v11}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 386
    .local v5, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    invoke-static {v5, p1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 389
    .local v1, "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-nez v1, :cond_1

    .line 390
    const/4 v6, 0x0

    .line 408
    :cond_0
    return-object v6

    .line 393
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v2

    .line 395
    .local v2, "data":[B
    array-length v10, v2

    add-int/lit8 v10, v10, -0x6

    div-int/lit8 v0, v10, 0x2

    .line 396
    .local v0, "array_lenght":I
    new-array v6, v0, [I

    .line 398
    .local v6, "segmentCountArray":[I
    const/4 v7, 0x6

    .line 400
    .local v7, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    move v8, v7

    .end local v7    # "size":I
    .local v8, "size":I
    :goto_0
    if-ge v4, v0, :cond_0

    .line 402
    const/4 v10, 0x2

    new-array v3, v10, [B

    const/4 v10, 0x0

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "size":I
    .restart local v7    # "size":I
    aget-byte v11, v2, v8

    aput-byte v11, v3, v10

    const/4 v10, 0x1

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "size":I
    .restart local v8    # "size":I
    aget-byte v11, v2, v7

    aput-byte v11, v3, v10

    .line 403
    .local v3, "dx":[B
    invoke-static {v3}, Lorg/apache/poi/util/LittleEndian;->getUShort([B)I

    move-result v9

    .line 404
    .local v9, "x":I
    and-int/lit16 v10, v9, 0x1fff

    aput v10, v6, v4

    .line 400
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getSegmentType(S)[I
    .locals 12
    .param p1, "propId"    # S

    .prologue
    .line 350
    iget-object v10, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v11, -0xff5

    invoke-static {v10, v11}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 351
    .local v5, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    invoke-static {v5, p1}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 354
    .local v1, "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-nez v1, :cond_1

    .line 355
    const/4 v6, 0x0

    .line 373
    :cond_0
    return-object v6

    .line 358
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v2

    .line 360
    .local v2, "data":[B
    array-length v10, v2

    add-int/lit8 v10, v10, -0x6

    div-int/lit8 v0, v10, 0x2

    .line 361
    .local v0, "array_lenght":I
    new-array v6, v0, [I

    .line 363
    .local v6, "segmentTypeArray":[I
    const/4 v7, 0x6

    .line 365
    .local v7, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    move v8, v7

    .end local v7    # "size":I
    .local v8, "size":I
    :goto_0
    if-ge v4, v0, :cond_0

    .line 367
    const/4 v10, 0x2

    new-array v3, v10, [B

    const/4 v10, 0x0

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "size":I
    .restart local v7    # "size":I
    aget-byte v11, v2, v8

    aput-byte v11, v3, v10

    const/4 v10, 0x1

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "size":I
    .restart local v8    # "size":I
    aget-byte v11, v2, v7

    aput-byte v11, v3, v10

    .line 368
    .local v3, "dx":[B
    invoke-static {v3}, Lorg/apache/poi/util/LittleEndian;->getUShort([B)I

    move-result v9

    .line 369
    .local v9, "x":I
    shr-int/lit8 v10, v9, 0xd

    aput v10, v6, v4

    .line 365
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getShapeId()I
    .locals 3

    .prologue
    .line 634
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff6

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 635
    .local v0, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v1

    goto :goto_0
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getShapeType()I

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/hslf/model/ShapeTypes;->typeName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShapeType()I
    .locals 3

    .prologue
    .line 134
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff6

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 135
    .local v0, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeType()S

    move-result v1

    return v1
.end method

.method public getSheet()Lorg/apache/poi/hslf/model/Sheet;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    return-object v0
.end method

.method public getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getVertices(S)[Landroid/graphics/Point;
    .locals 16
    .param p1, "propId"    # S

    .prologue
    .line 419
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v15, -0xff5

    invoke-static {v14, v15}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 420
    .local v8, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    move/from16 v0, p1

    invoke-static {v8, v0}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 423
    .local v2, "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-nez v2, :cond_1

    .line 424
    const/4 v9, 0x0

    .line 484
    :cond_0
    return-object v9

    .line 427
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v3

    .line 429
    .local v3, "data":[B
    const/4 v14, 0x4

    aget-byte v14, v3, v14

    const/16 v15, 0x8

    if-ne v14, v15, :cond_3

    .line 431
    array-length v14, v3

    add-int/lit8 v14, v14, -0x6

    div-int/lit8 v1, v14, 0x8

    .line 433
    .local v1, "array_lenght":I
    new-array v9, v1, [Landroid/graphics/Point;

    .line 435
    .local v9, "point":[Landroid/graphics/Point;
    const/4 v10, 0x6

    .line 436
    .local v10, "size":I
    array-length v14, v3

    add-int/lit8 v7, v14, -0x6

    .line 438
    .local v7, "length":I
    :goto_0
    if-eqz v7, :cond_0

    .line 440
    const/4 v6, 0x0

    .local v6, "i":I
    move v11, v10

    .end local v10    # "size":I
    .local v11, "size":I
    :goto_1
    if-lt v6, v1, :cond_2

    move v10, v11

    .end local v11    # "size":I
    .restart local v10    # "size":I
    goto :goto_0

    .line 442
    .end local v10    # "size":I
    .restart local v11    # "size":I
    :cond_2
    const/4 v14, 0x4

    new-array v4, v14, [B

    const/4 v14, 0x0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "size":I
    .restart local v10    # "size":I
    aget-byte v15, v3, v11

    aput-byte v15, v4, v14

    const/4 v14, 0x1

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "size":I
    .restart local v11    # "size":I
    aget-byte v15, v3, v10

    aput-byte v15, v4, v14

    const/4 v14, 0x2

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "size":I
    .restart local v10    # "size":I
    aget-byte v15, v3, v11

    aput-byte v15, v4, v14

    const/4 v14, 0x3

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "size":I
    .restart local v11    # "size":I
    aget-byte v15, v3, v10

    aput-byte v15, v4, v14

    .line 443
    .local v4, "dx":[B
    invoke-static {v4}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v14

    div-int/lit16 v12, v14, 0x319c

    .line 444
    .local v12, "x":I
    add-int/lit8 v7, v7, -0x4

    .line 446
    const/4 v14, 0x4

    new-array v5, v14, [B

    const/4 v14, 0x0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "size":I
    .restart local v10    # "size":I
    aget-byte v15, v3, v11

    aput-byte v15, v5, v14

    const/4 v14, 0x1

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "size":I
    .restart local v11    # "size":I
    aget-byte v15, v3, v10

    aput-byte v15, v5, v14

    const/4 v14, 0x2

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "size":I
    .restart local v10    # "size":I
    aget-byte v15, v3, v11

    aput-byte v15, v5, v14

    const/4 v14, 0x3

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "size":I
    .restart local v11    # "size":I
    aget-byte v15, v3, v10

    aput-byte v15, v5, v14

    .line 447
    .local v5, "dy":[B
    invoke-static {v5}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v14

    div-int/lit16 v13, v14, 0x319c

    .line 448
    .local v13, "y":I
    add-int/lit8 v7, v7, -0x4

    .line 450
    new-instance v14, Landroid/graphics/Point;

    invoke-direct {v14, v12, v13}, Landroid/graphics/Point;-><init>(II)V

    aput-object v14, v9, v6

    .line 440
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 461
    .end local v1    # "array_lenght":I
    .end local v4    # "dx":[B
    .end local v5    # "dy":[B
    .end local v6    # "i":I
    .end local v7    # "length":I
    .end local v9    # "point":[Landroid/graphics/Point;
    .end local v11    # "size":I
    .end local v12    # "x":I
    .end local v13    # "y":I
    :cond_3
    array-length v14, v3

    add-int/lit8 v14, v14, -0x6

    div-int/lit8 v1, v14, 0x4

    .line 462
    .restart local v1    # "array_lenght":I
    new-array v9, v1, [Landroid/graphics/Point;

    .line 464
    .restart local v9    # "point":[Landroid/graphics/Point;
    const/4 v10, 0x6

    .line 465
    .restart local v10    # "size":I
    array-length v14, v3

    add-int/lit8 v7, v14, -0x6

    .line 467
    .restart local v7    # "length":I
    :goto_2
    if-eqz v7, :cond_0

    .line 469
    const/4 v6, 0x0

    .restart local v6    # "i":I
    move v11, v10

    .end local v10    # "size":I
    .restart local v11    # "size":I
    :goto_3
    if-lt v6, v1, :cond_4

    move v10, v11

    .end local v11    # "size":I
    .restart local v10    # "size":I
    goto :goto_2

    .line 471
    .end local v10    # "size":I
    .restart local v11    # "size":I
    :cond_4
    const/4 v14, 0x2

    new-array v4, v14, [B

    const/4 v14, 0x0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "size":I
    .restart local v10    # "size":I
    aget-byte v15, v3, v11

    aput-byte v15, v4, v14

    const/4 v14, 0x1

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "size":I
    .restart local v11    # "size":I
    aget-byte v15, v3, v10

    aput-byte v15, v4, v14

    .line 472
    .restart local v4    # "dx":[B
    invoke-static {v4}, Lorg/apache/poi/util/LittleEndian;->getShort([B)S

    move-result v14

    mul-int/lit8 v14, v14, 0x48

    div-int/lit16 v12, v14, 0x240

    .line 473
    .restart local v12    # "x":I
    add-int/lit8 v7, v7, -0x2

    .line 475
    const/4 v14, 0x2

    new-array v5, v14, [B

    const/4 v14, 0x0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "size":I
    .restart local v10    # "size":I
    aget-byte v15, v3, v11

    aput-byte v15, v5, v14

    const/4 v14, 0x1

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "size":I
    .restart local v11    # "size":I
    aget-byte v15, v3, v10

    aput-byte v15, v5, v14

    .line 476
    .restart local v5    # "dy":[B
    invoke-static {v5}, Lorg/apache/poi/util/LittleEndian;->getShort([B)S

    move-result v14

    mul-int/lit8 v14, v14, 0x48

    div-int/lit16 v13, v14, 0x240

    .line 477
    .restart local v13    # "y":I
    add-int/lit8 v7, v7, -0x2

    .line 479
    new-instance v14, Landroid/graphics/Point;

    invoke-direct {v14, v12, v13}, Landroid/graphics/Point;-><init>(II)V

    aput-object v14, v9, v6

    .line 469
    add-int/lit8 v6, v6, 0x1

    goto :goto_3
.end method

.method public moveTo(FF)V
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 241
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getAnchor2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v1

    .line 242
    .local v1, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    float-to-double v2, p1

    float-to-double v4, p2

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    invoke-virtual/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 243
    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/Shape;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 244
    return-void
.end method

.method public setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 12
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    const-wide/high16 v10, 0x4082000000000000L    # 576.0

    const-wide/high16 v8, 0x4052000000000000L    # 72.0

    .line 215
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff6

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 216
    .local v2, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v0

    .line 217
    .local v0, "flags":I
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_0

    .line 218
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff1

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherChildAnchorRecord;

    .line 219
    .local v1, "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v4

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->setDx1(I)V

    .line 220
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->setDy1(I)V

    .line 221
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    add-double/2addr v4, v6

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->setDx2(I)V

    .line 222
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v6

    add-double/2addr v4, v6

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherChildAnchorRecord;->setDy2(I)V

    .line 232
    .end local v1    # "rec":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff0

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 226
    .local v1, "rec":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    int-to-short v3, v3

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setFlag(S)V

    .line 227
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v4

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    int-to-short v3, v3

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setCol1(S)V

    .line 228
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    add-double/2addr v4, v6

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    int-to-short v3, v3

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setDx1(S)V

    .line 229
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v6

    add-double/2addr v4, v6

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    double-to-int v3, v4

    int-to-short v3, v3

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setRow1(S)V

    goto :goto_0
.end method

.method public setEscherProperty(SI)V
    .locals 3
    .param p1, "propId"    # S
    .param p2, "value"    # I

    .prologue
    .line 304
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff5

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 305
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    invoke-static {v0, p1, p2}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 306
    return-void
.end method

.method public setShapeId(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 644
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff6

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 645
    .local v0, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    .line 646
    :cond_0
    return-void
.end method

.method public setShapeType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 143
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Shape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff6

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 144
    .local v0, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeType(S)V

    .line 145
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setVersion(S)V

    .line 146
    return-void
.end method

.method public setSheet(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 0
    .param p1, "sheet"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 537
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Shape;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    .line 538
    return-void
.end method

.method toRGB(I)Lorg/apache/poi/java/awt/Color;
    .locals 7
    .param p1, "val"    # I

    .prologue
    .line 588
    shr-int/lit8 v6, p1, 0x18

    and-int/lit16 v0, v6, 0xff

    .line 589
    .local v0, "a":I
    shr-int/lit8 v6, p1, 0x10

    and-int/lit16 v1, v6, 0xff

    .line 590
    .local v1, "b":I
    shr-int/lit8 v6, p1, 0x8

    and-int/lit16 v3, v6, 0xff

    .line 591
    .local v3, "g":I
    shr-int/lit8 v6, p1, 0x0

    and-int/lit16 v4, v6, 0xff

    .line 593
    .local v4, "r":I
    const/16 v6, 0xfe

    if-eq v0, v6, :cond_0

    .line 595
    const/16 v6, 0xff

    if-eq v0, v6, :cond_0

    .line 599
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/Sheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v2

    .line 600
    .local v2, "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    invoke-virtual {v2, v0}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->getColor(I)I

    move-result v5

    .line 602
    .local v5, "schemeColor":I
    shr-int/lit8 v6, v5, 0x0

    and-int/lit16 v4, v6, 0xff

    .line 603
    shr-int/lit8 v6, v5, 0x8

    and-int/lit16 v3, v6, 0xff

    .line 604
    shr-int/lit8 v6, v5, 0x10

    and-int/lit16 v1, v6, 0xff

    .line 606
    .end local v2    # "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .end local v5    # "schemeColor":I
    :cond_0
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v6, v4, v3, v1}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    return-object v6
.end method
