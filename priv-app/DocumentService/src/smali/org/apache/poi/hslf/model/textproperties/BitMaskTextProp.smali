.class public Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
.super Lorg/apache/poi/hslf/model/textproperties/TextProp;
.source "BitMaskTextProp.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private subPropMasks:[I

.field private subPropMatches:[Z

.field private subPropNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "sizeOfDataBlock"    # I
    .param p2, "maskInHeader"    # I
    .param p3, "overallName"    # Ljava/lang/String;
    .param p4, "subPropNames"    # [Ljava/lang/String;

    .prologue
    .line 38
    const-string/jumbo v1, "bitmask"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 39
    iput-object p4, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropNames:[Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->propName:Ljava/lang/String;

    .line 41
    array-length v1, p4

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMasks:[I

    .line 42
    array-length v1, p4

    new-array v1, v1, [Z

    iput-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    .line 45
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMasks:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 48
    return-void

    .line 46
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMasks:[I

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    aput v2, v1, v0

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    .line 99
    .local v0, "newObj":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    array-length v1, v1

    new-array v1, v1, [Z

    iput-object v1, v0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    .line 101
    return-object v0
.end method

.method public getSubPropMatches()[Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    return-object v0
.end method

.method public getSubPropNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getSubValue(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public getWriteMask()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    return v0
.end method

.method public setSubValue(ZI)V
    .locals 2
    .param p1, "value"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    aget-boolean v0, v0, p2

    if-ne v0, p1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 86
    :cond_0
    if-eqz p1, :cond_1

    .line 87
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMasks:[I

    aget v1, v1, p2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    .line 91
    :goto_1
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    aput-boolean p1, v0, p2

    goto :goto_0

    .line 89
    :cond_1
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMasks:[I

    aget v1, v1, p2

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    goto :goto_1
.end method

.method public setValue(I)V
    .locals 3
    .param p1, "val"    # I

    .prologue
    .line 63
    iput p1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 72
    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 68
    iget v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->dataValue:I

    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMasks:[I

    aget v2, v2, v0

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    .line 69
    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->subPropMatches:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 66
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
