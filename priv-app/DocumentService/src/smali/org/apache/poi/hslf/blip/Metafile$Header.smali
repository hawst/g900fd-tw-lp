.class public Lorg/apache/poi/hslf/blip/Metafile$Header;
.super Ljava/lang/Object;
.source "Metafile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hslf/blip/Metafile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Header"
.end annotation


# instance fields
.field public bounds:Lorg/apache/poi/java/awt/Rectangle;

.field public compression:I

.field public filter:I

.field public size:Lorg/apache/poi/java/awt/Dimension;

.field public wmfsize:I

.field public zipsize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/16 v0, 0xfe

    iput v0, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->filter:I

    .line 40
    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .prologue
    .line 113
    const/16 v0, 0x22

    return v0
.end method

.method public read([BI)V
    .locals 10
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 73
    move v3, p2

    .line 74
    .local v3, "pos":I
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v7

    iput v7, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->wmfsize:I

    add-int/lit8 v3, v3, 0x4

    .line 76
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    .local v2, "left":I
    add-int/lit8 v3, v3, 0x4

    .line 77
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v5

    .local v5, "top":I
    add-int/lit8 v3, v3, 0x4

    .line 78
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    .local v4, "right":I
    add-int/lit8 v3, v3, 0x4

    .line 79
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .local v0, "bottom":I
    add-int/lit8 v3, v3, 0x4

    .line 81
    new-instance v7, Lorg/apache/poi/java/awt/Rectangle;

    sub-int v8, v4, v2

    sub-int v9, v0, v5

    invoke-direct {v7, v2, v5, v8, v9}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    iput-object v7, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    .line 82
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v6

    .local v6, "width":I
    add-int/lit8 v3, v3, 0x4

    .line 83
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .local v1, "height":I
    add-int/lit8 v3, v3, 0x4

    .line 85
    new-instance v7, Lorg/apache/poi/java/awt/Dimension;

    invoke-direct {v7, v6, v1}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    iput-object v7, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->size:Lorg/apache/poi/java/awt/Dimension;

    .line 87
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v7

    iput v7, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->zipsize:I

    add-int/lit8 v3, v3, 0x4

    .line 89
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v7

    iput v7, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->compression:I

    add-int/lit8 v3, v3, 0x1

    .line 90
    invoke-static {p1, v3}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v7

    iput v7, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->filter:I

    add-int/lit8 v3, v3, 0x1

    .line 91
    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    const/16 v2, 0x22

    new-array v0, v2, [B

    .line 95
    .local v0, "header":[B
    const/4 v1, 0x0

    .line 96
    .local v1, "pos":I
    iget v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->wmfsize:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 98
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v2, v2, Lorg/apache/poi/java/awt/Rectangle;->x:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 99
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v2, v2, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 100
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v2, v2, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget-object v3, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v3, v3, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 101
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v2, v2, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget-object v3, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v3, v3, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 102
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->size:Lorg/apache/poi/java/awt/Dimension;

    iget v2, v2, Lorg/apache/poi/java/awt/Dimension;->width:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 103
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->size:Lorg/apache/poi/java/awt/Dimension;

    iget v2, v2, Lorg/apache/poi/java/awt/Dimension;->height:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 104
    iget v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->zipsize:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 106
    const/4 v2, 0x0

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    .line 107
    iget v2, p0, Lorg/apache/poi/hslf/blip/Metafile$Header;->filter:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    .line 109
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 110
    return-void
.end method
