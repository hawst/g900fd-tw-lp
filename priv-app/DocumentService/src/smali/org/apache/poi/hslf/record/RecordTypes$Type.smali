.class public Lorg/apache/poi/hslf/record/RecordTypes$Type;
.super Ljava/lang/Object;
.source "RecordTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hslf/record/RecordTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Type"
.end annotation


# instance fields
.field public handlingClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/poi/hslf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field public typeID:I


# direct methods
.method public constructor <init>(ILjava/lang/Class;)V
    .locals 0
    .param p1, "typeID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/poi/hslf/record/Record;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p2, "handlingClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/hslf/record/Record;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iput p1, p0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    .line 279
    iput-object p2, p0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->handlingClass:Ljava/lang/Class;

    .line 280
    return-void
.end method
