.class public Lorg/apache/poi/hslf/record/InteractiveInfo;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "InteractiveInfo.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private infoAtom:Lorg/apache/poi/hslf/record/InteractiveInfoAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0xff2

    sput-wide v0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 72
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 73
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 77
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    const/16 v1, 0xf

    aput-byte v1, v0, v4

    .line 78
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/poi/hslf/record/InteractiveInfo;->_type:J

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 81
    new-instance v0, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->infoAtom:Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    .line 82
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    iget-object v1, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->infoAtom:Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    aput-object v1, v0, v4

    .line 83
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 45
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 47
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    .line 48
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 51
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 52
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/InteractiveInfo;->findInterestingChildren()V

    .line 53
    return-void
.end method

.method private findInterestingChildren()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v3

    instance-of v0, v0, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v3

    check-cast v0, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->infoAtom:Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    .line 67
    return-void

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "First child record wasn\'t a InteractiveInfoAtom, was of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->infoAtom:Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 88
    sget-wide v0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_type:J

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/InteractiveInfo;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/InteractiveInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/InteractiveInfo;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 96
    return-void
.end method
