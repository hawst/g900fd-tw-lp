.class public final Lorg/apache/poi/hslf/record/HeadersFootersContainer;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "HeadersFootersContainer.java"


# static fields
.field public static final FOOTERATOM:I = 0x2

.field public static final HEADERATOM:I = 0x1

.field public static final NotesHeadersFootersContainer:S = 0x4fs

.field public static final SlideHeadersFootersContainer:S = 0x3fs

.field public static final USERDATEATOM:I


# instance fields
.field private _header:[B

.field private csDate:Lorg/apache/poi/hslf/record/CString;

.field private csFooter:Lorg/apache/poi/hslf/record/CString;

.field private csHeader:Lorg/apache/poi/hslf/record/CString;

.field private hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;


# direct methods
.method public constructor <init>(S)V
    .locals 5
    .param p1, "options"    # S

    .prologue
    const/4 v4, 0x0

    .line 86
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 87
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    .line 88
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    invoke-static {v0, v4, p1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 89
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 91
    new-instance v0, Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    .line 93
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    aput-object v1, v0, v4

    .line 92
    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    .line 97
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 7
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v6, 0x5

    .line 60
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 62
    new-array v3, v5, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    .line 63
    iget-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v4, 0x0

    invoke-static {p1, p2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 65
    add-int/lit8 v3, p2, 0x8

    add-int/lit8 v4, p3, -0x8

    invoke-static {p1, v3, v4}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 66
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 84
    return-void

    .line 67
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    check-cast v3, Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    .line 66
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/CString;

    if-eqz v3, :cond_2

    .line 69
    iget-object v3, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v3, v1

    check-cast v0, Lorg/apache/poi/hslf/record/CString;

    .line 70
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getOptions()I

    move-result v3

    shr-int/lit8 v2, v3, 0x4

    .line 71
    .local v2, "opts":I
    packed-switch v2, :pswitch_data_0

    .line 76
    sget-object v3, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Unexpected CString.Options in HeadersFootersContainer: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 72
    :pswitch_0
    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1

    .line 73
    :pswitch_1
    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1

    .line 74
    :pswitch_2
    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1

    .line 80
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    .end local v2    # "opts":I
    :cond_2
    sget-object v3, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Unexpected record in HeadersFootersContainer: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addFooterAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 3

    .prologue
    .line 199
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    .line 209
    :goto_0
    return-object v1

    .line 201
    :cond_0
    new-instance v1, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    .line 202
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 204
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    .line 205
    .local v0, "r":Lorg/apache/poi/hslf/record/Record;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    .line 207
    :cond_1
    :goto_1
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {p0, v1, v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 209
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    goto :goto_0

    .line 206
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1
.end method

.method public addHeaderAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 3

    .prologue
    .line 181
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    .line 190
    :goto_0
    return-object v1

    .line 183
    :cond_0
    new-instance v1, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    .line 184
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 186
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    .line 187
    .local v0, "r":Lorg/apache/poi/hslf/record/Record;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    .line 188
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {p0, v1, v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 190
    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    goto :goto_0
.end method

.method public addUserDateAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    .line 172
    :goto_0
    return-object v0

    .line 167
    :cond_0
    new-instance v0, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    .line 168
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 170
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    iget-object v1, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 172
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    goto :goto_0
.end method

.method public getFooterAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csFooter:Lorg/apache/poi/hslf/record/CString;

    return-object v0
.end method

.method public getHeaderAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csHeader:Lorg/apache/poi/hslf/record/CString;

    return-object v0
.end method

.method public getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->hdAtom:Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    return-object v0
.end method

.method public getOptions()I
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->HeadersFooters:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getUserDateAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->csDate:Lorg/apache/poi/hslf/record/CString;

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 120
    return-void
.end method
