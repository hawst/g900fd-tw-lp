.class public final enum Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;
.super Ljava/lang/Enum;
.source "TextAutoNumberSchemeEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$poi$hslf$record$TextAutoNumberSchemeEnum:[I

.field public static final enum ANM_AlphaLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_AlphaLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_AlphaLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_AlphaUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_AlphaUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_AlphaUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_Arabic1Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_Arabic2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ArabicDbPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ArabicDbPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ArabicParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ArabicParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ArabicPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ArabicPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ChsPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ChsPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ChtPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ChtPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_CircleNumDBPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_CircleNumWDBBlackPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_CircleNumWDBWhitePlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_Hebrew2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_HindiAlpha1Period:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_HindiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_HindiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_HindiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_JpnChsDBPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_JpnKorPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_JpnKorPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_RomanLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_RomanLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_RomanLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_RomanUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_RomanUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_RomanUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ThaiAlphaParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ThaiAlphaParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ThaiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ThaiNumParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ThaiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field public static final enum ANM_ThaiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;


# instance fields
.field private final value:S


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$poi$hslf$record$TextAutoNumberSchemeEnum()[I
    .locals 3

    .prologue
    .line 22
    sget-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->$SWITCH_TABLE$org$apache$poi$hslf$record$TextAutoNumberSchemeEnum:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->values()[Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Arabic1Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Arabic2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicDbPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicDbPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChsPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChsPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChtPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChtPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumDBPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumWDBBlackPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumWDBWhitePlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Hebrew2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiAlpha1Period:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnChsDBPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnKorPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnKorPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->$SWITCH_TABLE$org$apache$poi$hslf$record$TextAutoNumberSchemeEnum:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_AlphaLcPeriod"

    .line 24
    invoke-direct {v0, v1, v4, v4}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 25
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_AlphaUcPeriod"

    invoke-direct {v0, v1, v5, v5}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 26
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ArabicParenRight"

    invoke-direct {v0, v1, v6, v6}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 27
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ArabicPeriod"

    invoke-direct {v0, v1, v7, v7}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 28
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_RomanLcParenBoth"

    invoke-direct {v0, v1, v8, v8}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 29
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_RomanLcParenRight"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 30
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_RomanLcPeriod"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 31
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_RomanUcPeriod"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 32
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_AlphaLcParenBoth"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 33
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_AlphaLcParenRight"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 34
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_AlphaUcParenBoth"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 35
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_AlphaUcParenRight"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 36
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ArabicParenBoth"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 37
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ArabicPlain"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 38
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_RomanUcParenBoth"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 39
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_RomanUcParenRight"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 40
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ChsPlain"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChsPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 41
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ChsPeriod"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChsPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 42
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_CircleNumDBPlain"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumDBPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 43
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_CircleNumWDBWhitePlain"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumWDBWhitePlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 44
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_CircleNumWDBBlackPlain"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumWDBBlackPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 45
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ChtPlain"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChtPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 46
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ChtPeriod"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChtPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 47
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_Arabic1Minus"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Arabic1Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 48
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_Arabic2Minus"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Arabic2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 49
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_Hebrew2Minus"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Hebrew2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 50
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_JpnKorPlain"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnKorPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 51
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_JpnKorPeriod"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnKorPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 52
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ArabicDbPlain"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicDbPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 53
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ArabicDbPeriod"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicDbPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 54
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ThaiAlphaPeriod"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 55
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ThaiAlphaParenRight"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 56
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ThaiAlphaParenBoth"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 57
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ThaiNumPeriod"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 58
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ThaiNumParenRight"

    const/16 v2, 0x22

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 59
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_ThaiNumParenBoth"

    const/16 v2, 0x23

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 60
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_HindiAlphaPeriod"

    const/16 v2, 0x24

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 61
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_HindiNumPeriod"

    const/16 v2, 0x25

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 62
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_JpnChsDBPeriod"

    const/16 v2, 0x26

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnChsDBPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 63
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_HindiNumParenRight"

    const/16 v2, 0x27

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 64
    new-instance v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    const-string/jumbo v1, "ANM_HindiAlpha1Period"

    const/16 v2, 0x28

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiAlpha1Period:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 22
    const/16 v0, 0x29

    new-array v0, v0, [Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v1, v0, v7

    sget-object v1, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanLcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaLcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_AlphaUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_RomanUcParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChsPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChsPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumDBPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumWDBWhitePlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_CircleNumWDBBlackPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChtPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ChtPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Arabic1Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Arabic2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_Hebrew2Minus:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnKorPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnKorPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicDbPlain:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicDbPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiAlphaParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ThaiNumParenBoth:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiAlphaPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiNumPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_JpnChsDBPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiNumParenRight:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_HindiAlpha1Period:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ENUM$VALUES:[Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .param p3, "code"    # S

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput-short p3, p0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->value:S

    .line 69
    return-void
.end method

.method public static getDescription(Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;)Ljava/lang/String;
    .locals 2
    .param p0, "code"    # Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .prologue
    .line 75
    invoke-static {}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->$SWITCH_TABLE$org$apache$poi$hslf$record$TextAutoNumberSchemeEnum()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 117
    const-string/jumbo v0, "Unknown Numbered Scheme"

    :goto_0
    return-object v0

    .line 76
    :pswitch_0
    const-string/jumbo v0, "Lowercase Latin character followed by a period. Example: a., b., c., ..."

    goto :goto_0

    .line 77
    :pswitch_1
    const-string/jumbo v0, "Uppercase Latin character followed by a period. Example: A., B., C., ..."

    goto :goto_0

    .line 78
    :pswitch_2
    const-string/jumbo v0, "Arabic numeral followed by a closing parenthesis. Example: 1), 2), 3), ..."

    goto :goto_0

    .line 79
    :pswitch_3
    const-string/jumbo v0, "Arabic numeral followed by a period. Example: 1., 2., 3., ..."

    goto :goto_0

    .line 80
    :pswitch_4
    const-string/jumbo v0, "Lowercase Roman numeral enclosed in parentheses. Example: (i), (ii), (iii), ..."

    goto :goto_0

    .line 81
    :pswitch_5
    const-string/jumbo v0, "Lowercase Roman numeral followed by a closing parenthesis. Example: i), ii), iii), ..."

    goto :goto_0

    .line 82
    :pswitch_6
    const-string/jumbo v0, "Lowercase Roman numeral followed by a period. Example: i., ii., iii., ..."

    goto :goto_0

    .line 83
    :pswitch_7
    const-string/jumbo v0, "Uppercase Roman numeral followed by a period. Example: I., II., III., ..."

    goto :goto_0

    .line 84
    :pswitch_8
    const-string/jumbo v0, "Lowercase alphabetic character enclosed in parentheses. Example: (a), (b), (c), ..."

    goto :goto_0

    .line 85
    :pswitch_9
    const-string/jumbo v0, "Lowercase alphabetic character followed by a closing parenthesis. Example: a), b), c), ..."

    goto :goto_0

    .line 86
    :pswitch_a
    const-string/jumbo v0, "Uppercase alphabetic character enclosed in parentheses. Example: (A), (B), (C), ..."

    goto :goto_0

    .line 87
    :pswitch_b
    const-string/jumbo v0, "Uppercase alphabetic character followed by a closing parenthesis. Example: A), B), C), ..."

    goto :goto_0

    .line 88
    :pswitch_c
    const-string/jumbo v0, "Arabic numeral enclosed in parentheses. Example: (1), (2), (3), ..."

    goto :goto_0

    .line 89
    :pswitch_d
    const-string/jumbo v0, "Arabic numeral. Example: 1, 2, 3, ..."

    goto :goto_0

    .line 90
    :pswitch_e
    const-string/jumbo v0, "Uppercase Roman numeral enclosed in parentheses. Example: (I), (II), (III), ..."

    goto :goto_0

    .line 91
    :pswitch_f
    const-string/jumbo v0, "Uppercase Roman numeral followed by a closing parenthesis. Example: I), II), III), ..."

    goto :goto_0

    .line 92
    :pswitch_10
    const-string/jumbo v0, "Simplified Chinese."

    goto :goto_0

    .line 93
    :pswitch_11
    const-string/jumbo v0, "Simplified Chinese with single-byte period."

    goto :goto_0

    .line 94
    :pswitch_12
    const-string/jumbo v0, "Double byte circle numbers."

    goto :goto_0

    .line 95
    :pswitch_13
    const-string/jumbo v0, "Wingdings white circle numbers."

    goto :goto_0

    .line 96
    :pswitch_14
    const-string/jumbo v0, "Wingdings black circle numbers."

    goto :goto_0

    .line 97
    :pswitch_15
    const-string/jumbo v0, "Traditional Chinese."

    goto :goto_0

    .line 98
    :pswitch_16
    const-string/jumbo v0, "Traditional Chinese with single-byte period."

    goto :goto_0

    .line 99
    :pswitch_17
    const-string/jumbo v0, "Bidi Arabic 1 (AraAlpha) with ANSI minus symbol."

    goto :goto_0

    .line 100
    :pswitch_18
    const-string/jumbo v0, "Bidi Arabic 2 (AraAbjad) with ANSI minus symbol."

    goto :goto_0

    .line 101
    :pswitch_19
    const-string/jumbo v0, "Bidi Hebrew 2 with ANSI minus symbol."

    goto :goto_0

    .line 102
    :pswitch_1a
    const-string/jumbo v0, "Japanese/Korean."

    goto :goto_0

    .line 103
    :pswitch_1b
    const-string/jumbo v0, "Japanese/Korean with single-byte period."

    goto :goto_0

    .line 104
    :pswitch_1c
    const-string/jumbo v0, "Double-byte Arabic numbers."

    goto :goto_0

    .line 105
    :pswitch_1d
    const-string/jumbo v0, "Double-byte Arabic numbers with double-byte period."

    goto :goto_0

    .line 106
    :pswitch_1e
    const-string/jumbo v0, "Thai alphabetic character followed by a period."

    goto :goto_0

    .line 107
    :pswitch_1f
    const-string/jumbo v0, "Thai alphabetic character followed by a closing parenthesis."

    goto :goto_0

    .line 108
    :pswitch_20
    const-string/jumbo v0, "Thai alphabetic character enclosed by parentheses."

    goto/16 :goto_0

    .line 109
    :pswitch_21
    const-string/jumbo v0, "Thai numeral followed by a period."

    goto/16 :goto_0

    .line 110
    :pswitch_22
    const-string/jumbo v0, "Thai numeral followed by a closing parenthesis."

    goto/16 :goto_0

    .line 111
    :pswitch_23
    const-string/jumbo v0, "Thai numeral enclosed in parentheses."

    goto/16 :goto_0

    .line 112
    :pswitch_24
    const-string/jumbo v0, "Hindi alphabetic character followed by a period."

    goto/16 :goto_0

    .line 113
    :pswitch_25
    const-string/jumbo v0, "Hindi numeric character followed by a period."

    goto/16 :goto_0

    .line 114
    :pswitch_26
    const-string/jumbo v0, "Japanese with double-byte period."

    goto/16 :goto_0

    .line 115
    :pswitch_27
    const-string/jumbo v0, "Hindi numeric character followed by a closing parenthesis."

    goto/16 :goto_0

    .line 116
    :pswitch_28
    const-string/jumbo v0, "Hindi alphabetic character followed by a period."

    goto/16 :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method

.method private getValue()S
    .locals 1

    .prologue
    .line 70
    iget-short v0, p0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->value:S

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    return-object v0
.end method

.method public static valueOf(S)Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;
    .locals 5
    .param p0, "autoNumberScheme"    # S

    .prologue
    .line 121
    invoke-static {}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->values()[Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 126
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 121
    :cond_1
    aget-object v0, v2, v1

    .line 122
    .local v0, "item":Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;
    invoke-direct {v0}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->getValue()S

    move-result v4

    if-eq p0, v4, :cond_0

    .line 121
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static values()[Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ENUM$VALUES:[Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->getDescription(Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
