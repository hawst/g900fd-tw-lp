.class public final Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "TxMasterStyleAtom.java"


# static fields
.field private static final MAX_INDENT:I = 0x5

.field private static _type:J


# instance fields
.field private _data:[B

.field private _header:[B

.field private chstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

.field private prstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const-wide/16 v0, 0xfa3

    sput-wide v0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 63
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 64
    new-array v1, v2, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_header:[B

    .line 65
    iget-object v1, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_header:[B

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    add-int/lit8 v1, p3, -0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    .line 68
    add-int/lit8 v1, p2, 0x8

    iget-object v2, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    iget-object v3, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    array-length v3, v3

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected getCharacterProps(II)[Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 8
    .param p1, "type"    # I
    .param p2, "level"    # I

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x2

    .line 209
    if-nez p2, :cond_0

    if-lt p1, v7, :cond_1

    .line 210
    :cond_0
    sget-object v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 212
    :goto_0
    return-object v0

    :cond_1
    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/4 v1, 0x0

    .line 213
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;

    invoke-direct {v2}, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 214
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x10000

    const-string/jumbo v4, "font.index"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 215
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v2, 0x20000

    const-string/jumbo v3, "char_unknown_1"

    invoke-direct {v1, v5, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v5

    const/4 v1, 0x3

    .line 216
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x40000

    const-string/jumbo v4, "char_unknown_2"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 217
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v2, 0x80000

    const-string/jumbo v3, "font.size"

    invoke-direct {v1, v5, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v6

    .line 218
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v2, 0x100000

    const-string/jumbo v3, "char_unknown_3"

    invoke-direct {v1, v5, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 219
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x200000

    const-string/jumbo v4, "font.color"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 220
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x800000

    const-string/jumbo v4, "char_unknown_4"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public getCharacterStyles()[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->chstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    return-object v0
.end method

.method protected getParagraphProps(II)[Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 9
    .param p1, "type"    # I
    .param p2, "level"    # I

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x2

    .line 178
    if-nez p2, :cond_0

    if-lt p1, v7, :cond_1

    .line 179
    :cond_0
    sget-object v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 181
    :goto_0
    return-object v0

    :cond_1
    new-array v0, v8, [Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/4 v1, 0x0

    .line 182
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;

    invoke-direct {v2}, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 183
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x80

    const-string/jumbo v4, "bullet.char"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 184
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "bullet.font"

    invoke-direct {v1, v5, v8, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v5

    const/4 v1, 0x3

    .line 185
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x40

    const-string/jumbo v4, "bullet.size"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 186
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v2, 0x20

    const-string/jumbo v3, "bullet.color"

    invoke-direct {v1, v6, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v6

    .line 187
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v2, 0xd00

    const-string/jumbo v3, "alignment"

    invoke-direct {v1, v5, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 188
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x1000

    const-string/jumbo v4, "linespacing"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 189
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x2000

    const-string/jumbo v4, "spacebefore"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 190
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x4000

    const-string/jumbo v4, "spaceafter"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 191
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const v3, 0x8000

    const-string/jumbo v4, "text.offset"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 192
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x10000

    const-string/jumbo v4, "bullet.offset"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 193
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x20000

    const-string/jumbo v4, "defaulttab"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 194
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x40000

    const-string/jumbo v4, "para_unknown_2"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 195
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x80000

    const-string/jumbo v4, "para_unknown_3"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 196
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x100000

    const-string/jumbo v4, "para_unknown_4"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 197
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x200000

    const-string/jumbo v4, "para_unknown_5"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    goto/16 :goto_0
.end method

.method public getParagraphStyles()[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->prstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 85
    sget-wide v0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_type:J

    return-wide v0
.end method

.method public getTextType()I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_header:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method protected init()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 136
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getTextType()I

    move-result v6

    .line 139
    .local v6, "type":I
    const/4 v4, 0x0

    .line 142
    .local v4, "pos":I
    iget-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    invoke-static {v7, v9}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    .line 143
    .local v3, "levels":S
    add-int/lit8 v4, v4, 0x2

    .line 145
    new-array v7, v3, [Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    iput-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->prstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 146
    new-array v7, v3, [Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    iput-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->chstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 148
    const/4 v2, 0x0

    .local v2, "j":S
    :goto_0
    if-lt v2, v3, :cond_0

    .line 169
    return-void

    .line 150
    :cond_0
    const/4 v7, 0x5

    if-lt v6, v7, :cond_1

    .line 153
    add-int/lit8 v4, v4, 0x2

    .line 156
    :cond_1
    iget-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    invoke-static {v7, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 157
    .local v1, "head":I
    add-int/lit8 v4, v4, 0x4

    .line 158
    new-instance v5, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v5, v9}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(I)V

    .line 159
    .local v5, "prprops":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {p0, v6, v2}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getParagraphProps(II)[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    invoke-virtual {v5, v1, v7, v8, v4}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->buildTextPropList(I[Lorg/apache/poi/hslf/model/textproperties/TextProp;[BI)I

    move-result v7

    add-int/2addr v4, v7

    .line 160
    iget-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->prstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    aput-object v5, v7, v2

    .line 162
    iget-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    invoke-static {v7, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 163
    add-int/lit8 v4, v4, 0x4

    .line 164
    new-instance v0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v0, v9}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(I)V

    .line 165
    .local v0, "chprops":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {p0, v6, v2}, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->getCharacterProps(II)[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    invoke-virtual {v0, v1, v7, v8, v4}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->buildTextPropList(I[Lorg/apache/poi/hslf/model/textproperties/TextProp;[BI)I

    move-result v7

    add-int/2addr v4, v7

    .line 166
    iget-object v7, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->chstyles:[Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    aput-object v0, v7, v2

    .line 148
    add-int/lit8 v7, v2, 0x1

    int-to-short v2, v7

    goto :goto_0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 97
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 99
    return-void
.end method
