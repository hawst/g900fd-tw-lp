.class public final Lorg/apache/poi/hslf/EncryptedSlideShow;
.super Ljava/lang/Object;
.source "EncryptedSlideShow.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkIfEncrypted(Lorg/apache/poi/hslf/HSLFSlideShow;)Z
    .locals 4
    .param p0, "hss"    # Lorg/apache/poi/hslf/HSLFSlideShow;

    .prologue
    const/4 v1, 0x1

    .line 51
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getPOIFSDirectory()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v2

    const-string/jumbo v3, "EncryptedSummary"

    invoke-virtual {v2, v3}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 53
    :catch_0
    move-exception v2

    .line 62
    invoke-static {p0}, Lorg/apache/poi/hslf/EncryptedSlideShow;->fetchDocumentEncryptionAtom(Lorg/apache/poi/hslf/HSLFSlideShow;)Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;

    move-result-object v0

    .line 63
    .local v0, "dea":Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;
    if-nez v0, :cond_0

    .line 66
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static fetchDocumentEncryptionAtom(Lorg/apache/poi/hslf/HSLFSlideShow;)Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;
    .locals 16
    .param p0, "hss"    # Lorg/apache/poi/hslf/HSLFSlideShow;

    .prologue
    .line 78
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getCurrentUserAtom()Lorg/apache/poi/hslf/record/CurrentUserAtom;

    move-result-object v0

    .line 79
    .local v0, "cua":Lorg/apache/poi/hslf/record/CurrentUserAtom;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-eqz v11, :cond_7

    .line 81
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    move-result-object v11

    array-length v11, v11

    int-to-long v14, v11

    cmp-long v11, v12, v14

    if-lez v11, :cond_0

    .line 82
    new-instance v11, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    const-string/jumbo v12, "The CurrentUserAtom claims that the offset of last edit details are past the end of the file"

    invoke-direct {v11, v12}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 87
    :cond_0
    const/4 v6, 0x0

    .line 90
    .local v6, "r":Lorg/apache/poi/hslf/record/Record;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    move-result-object v11

    .line 91
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v12

    long-to-int v12, v12

    .line 89
    invoke-static {v11, v12}, Lorg/apache/poi/hslf/record/Record;->buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 96
    if-nez v6, :cond_1

    const/4 v8, 0x0

    .line 131
    .end local v6    # "r":Lorg/apache/poi/hslf/record/Record;
    :goto_0
    return-object v8

    .line 93
    .restart local v6    # "r":Lorg/apache/poi/hslf/record/Record;
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v8, 0x0

    goto :goto_0

    .line 97
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    instance-of v11, v6, Lorg/apache/poi/hslf/record/UserEditAtom;

    if-nez v11, :cond_2

    const/4 v8, 0x0

    goto :goto_0

    :cond_2
    move-object v10, v6

    .line 98
    check-cast v10, Lorg/apache/poi/hslf/record/UserEditAtom;

    .line 102
    .local v10, "uea":Lorg/apache/poi/hslf/record/UserEditAtom;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    move-result-object v11

    .line 103
    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/UserEditAtom;->getPersistPointersOffset()I

    move-result v12

    .line 101
    invoke-static {v11, v12}, Lorg/apache/poi/hslf/record/Record;->buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    .line 105
    .local v7, "r2":Lorg/apache/poi/hslf/record/Record;
    instance-of v11, v7, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    if-nez v11, :cond_3

    const/4 v8, 0x0

    goto :goto_0

    :cond_3
    move-object v5, v7

    .line 106
    check-cast v5, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    .line 109
    .local v5, "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v9

    .line 110
    .local v9, "slideIds":[I
    const/4 v3, -0x1

    .line 111
    .local v3, "maxSlideId":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v11, v9

    if-lt v2, v11, :cond_4

    .line 114
    const/4 v11, -0x1

    if-ne v3, v11, :cond_6

    const/4 v8, 0x0

    goto :goto_0

    .line 112
    :cond_4
    aget v11, v9, v2

    if-le v11, v3, :cond_5

    aget v3, v9, v2

    .line 111
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 117
    :cond_6
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v11

    .line 118
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 117
    invoke-virtual {v11, v12}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    .line 119
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 121
    .local v4, "offset":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    move-result-object v11

    .line 120
    invoke-static {v11, v4}, Lorg/apache/poi/hslf/record/Record;->buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;

    move-result-object v8

    .line 126
    .local v8, "r3":Lorg/apache/poi/hslf/record/Record;
    instance-of v11, v8, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;

    if-eqz v11, :cond_7

    .line 127
    check-cast v8, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;

    goto :goto_0

    .line 131
    .end local v2    # "i":I
    .end local v3    # "maxSlideId":I
    .end local v4    # "offset":I
    .end local v5    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v6    # "r":Lorg/apache/poi/hslf/record/Record;
    .end local v7    # "r2":Lorg/apache/poi/hslf/record/Record;
    .end local v8    # "r3":Lorg/apache/poi/hslf/record/Record;
    .end local v9    # "slideIds":[I
    .end local v10    # "uea":Lorg/apache/poi/hslf/record/UserEditAtom;
    :cond_7
    const/4 v8, 0x0

    goto :goto_0
.end method
