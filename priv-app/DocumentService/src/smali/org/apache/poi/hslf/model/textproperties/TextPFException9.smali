.class public Lorg/apache/poi/hslf/model/textproperties/TextPFException9;
.super Ljava/lang/Object;
.source "TextPFException9.java"


# static fields
.field private static final DEFAULT_AUTONUMBER_SHEME:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field private static final DEFAULT_START_NUMBER:Ljava/lang/Short;


# instance fields
.field private final autoNumberScheme:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

.field private final autoNumberStartNumber:Ljava/lang/Short;

.field private final bulletBlipRef:Ljava/lang/Short;

.field private final fBulletHasAutoNumber:Ljava/lang/Short;

.field private final mask3:B

.field private final mask4:B

.field private final recordLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->ANM_ArabicPeriod:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    sput-object v0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->DEFAULT_AUTONUMBER_SHEME:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 47
    new-instance v0, Ljava/lang/Short;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Short;-><init>(S)V

    sput-object v0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->DEFAULT_START_NUMBER:Ljava/lang/Short;

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "startIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    add-int/lit8 v2, p2, 0x2

    aget-byte v2, p1, v2

    iput-byte v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->mask3:B

    .line 53
    add-int/lit8 v2, p2, 0x3

    aget-byte v2, p1, v2

    iput-byte v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->mask4:B

    .line 54
    const/4 v1, 0x4

    .line 55
    .local v1, "length":I
    add-int/lit8 v0, p2, 0x4

    .line 56
    .local v0, "index":I
    iget-byte v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->mask3:B

    and-int/lit8 v2, v2, -0x80

    if-nez v2, :cond_0

    .line 57
    iput-object v3, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->bulletBlipRef:Ljava/lang/Short;

    .line 63
    :goto_0
    iget-byte v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->mask4:B

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    .line 64
    iput-object v3, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    .line 70
    :goto_1
    iget-byte v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->mask4:B

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_2

    .line 71
    iput-object v3, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberScheme:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 72
    iput-object v3, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberStartNumber:Ljava/lang/Short;

    .line 80
    :goto_2
    iput v1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->recordLength:I

    .line 81
    return-void

    .line 59
    :cond_0
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->bulletBlipRef:Ljava/lang/Short;

    .line 60
    add-int/lit8 v0, v0, 0x2

    .line 61
    const/4 v1, 0x6

    goto :goto_0

    .line 66
    :cond_1
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    .line 67
    add-int/lit8 v0, v0, 0x2

    .line 68
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 74
    :cond_2
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;->valueOf(S)Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberScheme:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 75
    add-int/lit8 v0, v0, 0x2

    .line 76
    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberStartNumber:Ljava/lang/Short;

    .line 77
    add-int/lit8 v0, v0, 0x2

    .line 78
    add-int/lit8 v1, v1, 0x4

    goto :goto_2
.end method


# virtual methods
.method public getAutoNumberScheme()Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberScheme:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberScheme:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    .line 95
    :goto_0
    return-object v0

    .line 92
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    if-ne v0, v1, :cond_1

    .line 93
    sget-object v0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->DEFAULT_AUTONUMBER_SHEME:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    goto :goto_0

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAutoNumberStartNumber()Ljava/lang/Short;
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberStartNumber:Ljava/lang/Short;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberStartNumber:Ljava/lang/Short;

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    if-ne v0, v1, :cond_1

    .line 102
    sget-object v0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->DEFAULT_START_NUMBER:Ljava/lang/Short;

    goto :goto_0

    .line 104
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBulletBlipRef()Ljava/lang/Short;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->bulletBlipRef:Ljava/lang/Short;

    return-object v0
.end method

.method public getRecordLength()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->recordLength:I

    return v0
.end method

.method public getfBulletHasAutoNumber()Ljava/lang/Short;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Record length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->recordLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 112
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "bulletBlipRef: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->bulletBlipRef:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v1, "fBulletHasAutoNumber: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->fBulletHasAutoNumber:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v1, "autoNumberScheme: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberScheme:Lorg/apache/poi/hslf/record/TextAutoNumberSchemeEnum;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v1, "autoNumberStartNumber: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->autoNumberStartNumber:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
