.class public Lorg/apache/poi/hslf/model/Picture;
.super Lorg/apache/poi/hslf/model/SimpleShape;
.source "Picture.java"


# static fields
.field public static final DIB:B = 0x7t

.field public static final EMF:I = 0x2

.field public static final JPEG:I = 0x5

.field public static final PICT:I = 0x4

.field public static final PNG:I = 0x6

.field public static final WMF:I = 0x3


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hslf/model/Picture;-><init>(ILorg/apache/poi/hslf/model/Shape;)V

    .line 75
    return-void
.end method

.method public constructor <init>(ILorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "idx"    # I
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hslf/model/SimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 85
    instance-of v0, p2, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hslf/model/Picture;->createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 86
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/SimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 97
    return-void
.end method


# virtual methods
.method protected createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 4
    .param p1, "idx"    # I
    .param p2, "isChild"    # Z

    .prologue
    .line 119
    invoke-super {p0, p2}, Lorg/apache/poi/hslf/model/SimpleShape;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 120
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 122
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff6

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 123
    .local v1, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    const/16 v2, 0x4b2

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 126
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Picture;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 127
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x7f

    const v3, 0x800080

    invoke-static {v0, v2, v3}, Lorg/apache/poi/hslf/model/Picture;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 130
    const/16 v2, 0x4104

    invoke-static {v0, v2, p1}, Lorg/apache/poi/hslf/model/Picture;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 132
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v2
.end method

.method protected getEscherBSERecord()Lorg/apache/poi/ddf/EscherBSERecord;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 161
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Picture;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v5

    .line 162
    .local v5, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v2

    .line 163
    .local v2, "doc":Lorg/apache/poi/hslf/record/Document;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Document;->getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/PPDrawingGroup;->getDggContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 164
    .local v1, "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v7, -0xfff

    invoke-static {v1, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 165
    .local v0, "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v0, :cond_0

    .line 166
    iget-object v7, p0, Lorg/apache/poi/hslf/model/Picture;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v8, "EscherContainerRecord.BSTORE_CONTAINER was not found "

    invoke-virtual {v7, v9, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 175
    :goto_0
    return-object v6

    .line 169
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v4

    .line 170
    .local v4, "lst":Ljava/util/List;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Picture;->getPictureIndex()I

    move-result v3

    .line 171
    .local v3, "idx":I
    if-nez v3, :cond_1

    .line 172
    iget-object v7, p0, Lorg/apache/poi/hslf/model/Picture;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v8, "picture index was not found, returning "

    invoke-virtual {v7, v9, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 175
    :cond_1
    add-int/lit8 v6, v3, -0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherBSERecord;

    goto :goto_0
.end method

.method public getPictureData()Lorg/apache/poi/hslf/usermodel/PictureData;
    .locals 8

    .prologue
    const/4 v7, 0x7

    .line 143
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Picture;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v3

    .line 144
    .local v3, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getPictureData()[Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v2

    .line 146
    .local v2, "pict":[Lorg/apache/poi/hslf/usermodel/PictureData;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Picture;->getEscherBSERecord()Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v0

    .line 147
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    if-nez v0, :cond_0

    .line 148
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Picture;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v5, "no reference to picture data found "

    invoke-virtual {v4, v7, v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 157
    :goto_0
    const/4 v4, 0x0

    :goto_1
    return-object v4

    .line 150
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    array-length v4, v2

    if-lt v1, v4, :cond_1

    .line 155
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Picture;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "no picture found for our BSE offset "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v7, v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 151
    :cond_1
    aget-object v4, v2, v1

    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/PictureData;->getOffset()I

    move-result v4

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 152
    aget-object v4, v2, v1

    goto :goto_1

    .line 150
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public getPictureIndex()I
    .locals 4

    .prologue
    .line 107
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Picture;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 108
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x104

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/Picture;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 109
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getPictureName()Ljava/lang/String;
    .locals 8

    .prologue
    .line 184
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xff5

    invoke-static {v6, v7}, Lorg/apache/poi/hslf/model/Picture;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 185
    .local v4, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v6, 0x105

    invoke-static {v4, v6}, Lorg/apache/poi/hslf/model/Picture;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 186
    .local v5, "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    const/4 v2, 0x0

    .line 187
    .local v2, "name":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 189
    :try_start_0
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v6

    const-string/jumbo v7, "UTF-16LE"

    invoke-direct {v3, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v2    # "name":Ljava/lang/String;
    .local v3, "name":Ljava/lang/String;
    const/4 v6, 0x0

    :try_start_1
    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 191
    .local v1, "idx":I
    const/4 v6, -0x1

    if-ne v1, v6, :cond_0

    move-object v6, v3

    :goto_0
    move-object v2, v3

    .line 196
    .end local v1    # "idx":I
    .end local v3    # "name":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 191
    .end local v2    # "name":Ljava/lang/String;
    .restart local v1    # "idx":I
    .restart local v3    # "name":Ljava/lang/String;
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    goto :goto_0

    .line 192
    .end local v1    # "idx":I
    .end local v3    # "name":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    :goto_2
    new-instance v6, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v6, v0}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_1
    move-object v6, v2

    .line 196
    goto :goto_1

    .line 192
    .end local v2    # "name":Ljava/lang/String;
    .restart local v3    # "name":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "name":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    goto :goto_2
.end method

.method public setPictureName(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 205
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Picture;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v5, -0xff5

    invoke-static {v4, v5}, Lorg/apache/poi/hslf/model/Picture;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 207
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "UTF-16LE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 208
    .local v0, "data":[B
    new-instance v3, Lorg/apache/poi/ddf/EscherComplexProperty;

    const/16 v4, 0x105

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5, v0}, Lorg/apache/poi/ddf/EscherComplexProperty;-><init>(SZ[B)V

    .line 209
    .local v3, "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    return-void

    .line 210
    .end local v0    # "data":[B
    .end local v3    # "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    :catch_0
    move-exception v1

    .line 211
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v4, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v4, v1}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method
