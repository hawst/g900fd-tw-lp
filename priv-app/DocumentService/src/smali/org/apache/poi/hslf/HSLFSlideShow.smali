.class public final Lorg/apache/poi/hslf/HSLFSlideShow;
.super Lorg/apache/poi/POIDocument;
.source "HSLFSlideShow.java"


# instance fields
.field private _docstream:[B

.field private _objects:[Lorg/apache/poi/hslf/usermodel/ObjectData;

.field private _pictures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hslf/usermodel/PictureData;",
            ">;"
        }
    .end annotation
.end field

.field private _records:[Lorg/apache/poi/hslf/record/Record;

.field private currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

.field private logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v0, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/io/InputStream;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 3
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lorg/apache/poi/POIDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/poi/util/POILogger;

    .line 181
    invoke-direct {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->readCurrentUserStream()V

    .line 185
    invoke-direct {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->readPowerPointStream()V

    .line 189
    invoke-static {p0}, Lorg/apache/poi/hslf/EncryptedSlideShow;->checkIfEncrypted(Lorg/apache/poi/hslf/HSLFSlideShow;)Z

    move-result v0

    .line 190
    .local v0, "encrypted":Z
    if-eqz v0, :cond_0

    .line 191
    new-instance v1, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;

    const-string/jumbo v2, "Encrypted PowerPoint files are not supported"

    invoke-direct {v1, v2}, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    invoke-direct {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->buildRecords()V

    .line 198
    invoke-direct {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->readOtherStreams()V

    .line 199
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "filesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 165
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Z)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "isThumbnail"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lorg/apache/poi/POIDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/poi/util/POILogger;

    .line 205
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;)V
    .locals 1
    .param p1, "filesystem"    # Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 149
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "filesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 137
    return-void
.end method

.method private buildRecords()V
    .locals 4

    .prologue
    .line 295
    iget-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_docstream:[B

    iget-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hslf/HSLFSlideShow;->read([BI)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    .line 296
    return-void
.end method

.method public static final create()Lorg/apache/poi/hslf/HSLFSlideShow;
    .locals 4

    .prologue
    .line 212
    const-class v2, Lorg/apache/poi/hslf/HSLFSlideShow;

    const-string/jumbo v3, "data/empty.ppt"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 213
    .local v1, "is":Ljava/io/InputStream;
    if-nez v1, :cond_0

    .line 214
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Missing resource \'empty.ppt\'"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 217
    :cond_0
    :try_start_0
    new-instance v2, Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-direct {v2, v1}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static getThumbnail(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)[B
    .locals 10
    .param p0, "filesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    .prologue
    .line 652
    const/4 v4, 0x0

    .line 656
    .local v4, "outputArray":[B
    :try_start_0
    new-instance v5, Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v7

    const/4 v8, 0x1

    invoke-direct {v5, v7, v8}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Z)V

    .line 658
    .local v5, "pptObj":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/HSLFSlideShow;->getSummaryInformation()Lorg/apache/poi/hpsf/SummaryInformation;

    move-result-object v3

    .line 660
    .local v3, "infro":Lorg/apache/poi/hpsf/SummaryInformation;
    if-nez v3, :cond_0

    .line 661
    new-instance v7, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;

    const-string/jumbo v8, "Password protected file"

    invoke-direct {v7, v8}, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 683
    .end local v3    # "infro":Lorg/apache/poi/hpsf/SummaryInformation;
    .end local v5    # "pptObj":Lorg/apache/poi/hslf/HSLFSlideShow;
    :catch_0
    move-exception v2

    .line 685
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v7, "DocumentService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    const/4 v4, 0x0

    .line 688
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    return-object v4

    .line 664
    .restart local v3    # "infro":Lorg/apache/poi/hpsf/SummaryInformation;
    .restart local v5    # "pptObj":Lorg/apache/poi/hslf/HSLFSlideShow;
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lorg/apache/poi/hpsf/SummaryInformation;->getThumbnail()[B

    move-result-object v0

    .line 666
    .local v0, "_docstreamthumbnail":[B
    new-instance v6, Lorg/apache/poi/hpsf/Thumbnail;

    invoke-direct {v6}, Lorg/apache/poi/hpsf/Thumbnail;-><init>()V

    .line 668
    .local v6, "thumbnailWMF":Lorg/apache/poi/hpsf/Thumbnail;
    invoke-virtual {v6, v0}, Lorg/apache/poi/hpsf/Thumbnail;->setThumbnail([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 672
    :try_start_2
    invoke-virtual {v6}, Lorg/apache/poi/hpsf/Thumbnail;->getThumbnailAsWMF()[B
    :try_end_2
    .catch Lorg/apache/poi/hpsf/HPSFException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    .line 674
    .local v1, "_docstreamthumbnailasWMF":[B
    move-object v4, v1

    goto :goto_0

    .line 677
    .end local v1    # "_docstreamthumbnailasWMF":[B
    :catch_1
    move-exception v2

    .line 679
    .local v2, "e":Lorg/apache/poi/hpsf/HPSFException;
    :try_start_3
    const-string/jumbo v7, "DocumentService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/poi/hpsf/HPSFException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 680
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private read([BI)[Lorg/apache/poi/hslf/record/Record;
    .locals 14
    .param p1, "docstream"    # [B
    .param p2, "usrOffset"    # I

    .prologue
    .line 299
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 300
    .local v4, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 301
    .local v6, "offset2id":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    if-nez p2, :cond_0

    .line 319
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/Integer;

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 320
    .local v0, "a":[Ljava/lang/Integer;
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 321
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v10, v12, [Lorg/apache/poi/hslf/record/Record;

    .line 322
    .local v10, "rec":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v12, v0

    if-lt v2, v12, :cond_2

    .line 332
    return-object v10

    .line 302
    .end local v0    # "a":[Ljava/lang/Integer;
    .end local v2    # "i":I
    .end local v10    # "rec":[Lorg/apache/poi/hslf/record/Record;
    :cond_0
    invoke-static/range {p1 .. p2}, Lorg/apache/poi/hslf/record/Record;->buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hslf/record/UserEditAtom;

    .line 303
    .local v11, "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/UserEditAtom;->getPersistPointersOffset()I

    move-result v8

    .line 306
    .local v8, "psrOffset":I
    invoke-static {p1, v8}, Lorg/apache/poi/hslf/record/Record;->buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    .line 307
    .local v9, "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v1

    .line 309
    .local v1, "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_1

    .line 315
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/UserEditAtom;->getLastUserEditAtomOffset()I

    move-result p2

    goto :goto_0

    .line 309
    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 310
    .local v3, "id":Ljava/lang/Integer;
    invoke-virtual {v1, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 311
    .local v5, "offset":Ljava/lang/Integer;
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    invoke-virtual {v6, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 323
    .end local v1    # "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v3    # "id":Ljava/lang/Integer;
    .end local v5    # "offset":Ljava/lang/Integer;
    .end local v8    # "psrOffset":I
    .end local v9    # "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v11    # "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    .restart local v0    # "a":[Ljava/lang/Integer;
    .restart local v2    # "i":I
    .restart local v10    # "rec":[Lorg/apache/poi/hslf/record/Record;
    :cond_2
    aget-object v5, v0, v2

    .line 324
    .restart local v5    # "offset":Ljava/lang/Integer;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-static {p1, v12}, Lorg/apache/poi/hslf/record/Record;->buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;

    move-result-object v12

    aput-object v12, v10, v2

    .line 325
    aget-object v12, v10, v2

    instance-of v12, v12, Lorg/apache/poi/hslf/record/PersistRecord;

    if-eqz v12, :cond_3

    .line 326
    aget-object v7, v10, v2

    check-cast v7, Lorg/apache/poi/hslf/record/PersistRecord;

    .line 327
    .local v7, "psr":Lorg/apache/poi/hslf/record/PersistRecord;
    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 328
    .restart local v3    # "id":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-interface {v7, v12}, Lorg/apache/poi/hslf/record/PersistRecord;->setPersistId(I)V

    .line 322
    .end local v3    # "id":Ljava/lang/Integer;
    .end local v7    # "psr":Lorg/apache/poi/hslf/record/PersistRecord;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private readCurrentUserStream()V
    .locals 5

    .prologue
    .line 340
    :try_start_0
    new-instance v1, Lorg/apache/poi/hslf/record/CurrentUserAtom;

    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    invoke-direct {v1, v2}, Lorg/apache/poi/hslf/record/CurrentUserAtom;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    iput-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :goto_0
    return-void

    .line 341
    :catch_0
    move-exception v0

    .line 342
    .local v0, "ie":Ljava/io/IOException;
    iget-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error finding Current User Atom:\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 343
    new-instance v1, Lorg/apache/poi/hslf/record/CurrentUserAtom;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/CurrentUserAtom;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

    goto :goto_0
.end method

.method private readOtherStreams()V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method private readPictures()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v14, 0xf018

    const/4 v13, 0x7

    .line 359
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 362
    const/4 v4, 0x0

    .line 365
    .local v4, "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    iget-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .line 366
    const-string/jumbo v11, "Pictures"

    invoke-virtual {v10, v11}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v1

    .line 365
    check-cast v1, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 367
    .local v1, "entry":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v1}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v10

    new-array v7, v10, [B

    .line 368
    .local v7, "pictstream":[B
    iget-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    const-string/jumbo v11, "Pictures"

    invoke-virtual {v10, v11}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v4

    .line 369
    invoke-virtual {v4, v7}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    if-eqz v4, :cond_0

    .line 378
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 385
    :cond_0
    :goto_0
    const/4 v8, 0x0

    .line 387
    .local v8, "pos":I
    :goto_1
    array-length v10, v7

    add-int/lit8 v10, v10, -0x8

    if-le v8, v10, :cond_3

    .line 435
    .end local v1    # "entry":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    .end local v7    # "pictstream":[B
    .end local v8    # "pos":I
    :cond_1
    :goto_2
    return-void

    .line 370
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Exception: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 376
    if-eqz v4, :cond_1

    .line 378
    :try_start_3
    invoke-virtual {v4}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 379
    :catch_1
    move-exception v0

    .line 380
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Exception: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 375
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    .line 376
    if-eqz v4, :cond_2

    .line 378
    :try_start_4
    invoke-virtual {v4}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 383
    :cond_2
    :goto_3
    throw v10

    .line 379
    :catch_2
    move-exception v0

    .line 380
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v11, "DocumentService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Exception: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 379
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    .restart local v7    # "pictstream":[B
    :catch_3
    move-exception v0

    .line 380
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Exception: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 388
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v8    # "pos":I
    :cond_3
    move v5, v8

    .line 392
    .local v5, "offset":I
    add-int/lit8 v8, v8, 0x2

    .line 394
    invoke-static {v7, v8}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v9

    .line 395
    .local v9, "type":I
    add-int/lit8 v8, v8, 0x2

    .line 397
    invoke-static {v7, v8}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    .line 398
    .local v3, "imgsize":I
    add-int/lit8 v8, v8, 0x4

    .line 402
    const v10, 0xf007

    if-eq v9, v10, :cond_4

    if-lt v9, v14, :cond_1

    const v10, 0xf117

    if-gt v9, v10, :cond_1

    .line 408
    :cond_4
    if-gez v3, :cond_5

    .line 409
    new-instance v10, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "The file contains a picture, at position "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", which has a negatively sized data length, so we can\'t trust any of the picture data"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 413
    :cond_5
    if-nez v9, :cond_6

    .line 414
    iget-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Problem reading picture: Invalid image type 0, on picture with length "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ".\nYou document will probably become corrupted if you save it!"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v13, v11}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 415
    iget-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v13, v11}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 433
    :goto_4
    add-int/2addr v8, v3

    goto/16 :goto_1

    .line 419
    :cond_6
    sub-int v10, v9, v14

    :try_start_5
    invoke-static {v10}, Lorg/apache/poi/hslf/usermodel/PictureData;->create(I)Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v6

    .line 422
    .local v6, "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    new-array v2, v3, [B

    .line 423
    .local v2, "imgdata":[B
    const/4 v10, 0x0

    array-length v11, v2

    invoke-static {v7, v8, v2, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 424
    invoke-virtual {v6, v2}, Lorg/apache/poi/hslf/usermodel/PictureData;->setRawData([B)V

    .line 426
    invoke-virtual {v6, v5}, Lorg/apache/poi/hslf/usermodel/PictureData;->setOffset(I)V

    .line 427
    iget-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_4

    .line 428
    .end local v2    # "imgdata":[B
    .end local v6    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :catch_4
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v10, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Problem reading picture: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\nYou document will probably become corrupted if you save it!"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v13, v11}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_4
.end method

.method private readPowerPointStream()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .line 232
    const-string/jumbo v4, "PowerPoint Document"

    invoke-virtual {v3, v4}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v1

    .line 231
    check-cast v1, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 235
    .local v1, "docProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v1}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_docstream:[B

    .line 237
    const/4 v0, 0x0

    .line 240
    .local v0, "docInputStream":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .line 241
    const-string/jumbo v4, "PowerPoint Document"

    invoke-virtual {v3, v4}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    .line 242
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_docstream:[B

    invoke-virtual {v0, v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    if-eqz v0, :cond_0

    .line 248
    :try_start_1
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 243
    :catch_0
    move-exception v2

    .line 244
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    if-eqz v0, :cond_0

    .line 248
    :try_start_3
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 249
    :catch_1
    move-exception v2

    .line 250
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 245
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 246
    if-eqz v0, :cond_1

    .line 248
    :try_start_4
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 253
    :cond_1
    :goto_1
    throw v3

    .line 249
    :catch_2
    move-exception v2

    .line 250
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Exception: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 249
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v2

    .line 250
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addPicture(Lorg/apache/poi/hslf/usermodel/PictureData;)I
    .locals 5
    .param p1, "img"    # Lorg/apache/poi/hslf/usermodel/PictureData;

    .prologue
    .line 593
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    if-nez v3, :cond_0

    .line 595
    :try_start_0
    invoke-direct {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->readPictures()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    :cond_0
    const/4 v1, 0x0

    .line 603
    .local v1, "offset":I
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 604
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    iget-object v4, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/usermodel/PictureData;

    .line 605
    .local v2, "prev":Lorg/apache/poi/hslf/usermodel/PictureData;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/usermodel/PictureData;->getOffset()I

    move-result v3

    invoke-virtual {v2}, Lorg/apache/poi/hslf/usermodel/PictureData;->getRawData()[B

    move-result-object v4

    array-length v4, v4

    add-int/2addr v3, v4

    add-int/lit8 v1, v3, 0x8

    .line 607
    .end local v2    # "prev":Lorg/apache/poi/hslf/usermodel/PictureData;
    :cond_1
    invoke-virtual {p1, v1}, Lorg/apache/poi/hslf/usermodel/PictureData;->setOffset(I)V

    .line 608
    iget-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    return v1

    .line 596
    .end local v1    # "offset":I
    :catch_0
    move-exception v0

    .line 597
    .local v0, "e":Ljava/io/IOException;
    new-instance v3, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public declared-synchronized appendRootLevelRecord(Lorg/apache/poi/hslf/record/Record;)I
    .locals 6
    .param p1, "newRecord"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 566
    monitor-enter p0

    const/4 v1, -0x1

    .line 567
    .local v1, "addedAt":I
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v4, v4

    add-int/lit8 v4, v4, 0x1

    new-array v3, v4, [Lorg/apache/poi/hslf/record/Record;

    .line 568
    .local v3, "r":[Lorg/apache/poi/hslf/record/Record;
    const/4 v0, 0x0

    .line 569
    .local v0, "added":Z
    iget-object v4, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v4, v4

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_0
    if-gez v2, :cond_0

    .line 582
    iput-object v3, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 583
    monitor-exit p0

    return v1

    .line 570
    :cond_0
    if-eqz v0, :cond_2

    .line 572
    :try_start_1
    iget-object v4, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v4, v4, v2

    aput-object v4, v3, v2

    .line 569
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 574
    :cond_2
    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v5, v5, v2

    aput-object v5, v3, v4

    .line 575
    iget-object v4, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v4, v4, v2

    instance-of v4, v4, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    if-eqz v4, :cond_1

    .line 576
    aput-object p1, v3, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577
    const/4 v0, 0x1

    .line 578
    move v1, v2

    goto :goto_1

    .line 566
    .end local v0    # "added":Z
    .end local v2    # "i":I
    .end local v3    # "r":[Lorg/apache/poi/hslf/record/Record;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public getCurrentUserAtom()Lorg/apache/poi/hslf/record/CurrentUserAtom;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

    return-object v0
.end method

.method public getEmbeddedObjects()[Lorg/apache/poi/hslf/usermodel/ObjectData;
    .locals 4

    .prologue
    .line 698
    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_objects:[Lorg/apache/poi/hslf/usermodel/ObjectData;

    if-nez v2, :cond_0

    .line 699
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 700
    .local v1, "objects":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/usermodel/ObjectData;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 705
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hslf/usermodel/ObjectData;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hslf/usermodel/ObjectData;

    iput-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_objects:[Lorg/apache/poi/hslf/usermodel/ObjectData;

    .line 707
    .end local v0    # "i":I
    .end local v1    # "objects":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/usermodel/ObjectData;>;"
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_objects:[Lorg/apache/poi/hslf/usermodel/ObjectData;

    return-object v2

    .line 701
    .restart local v0    # "i":I
    .restart local v1    # "objects":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/usermodel/ObjectData;>;"
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/ExOleObjStg;

    if-eqz v2, :cond_2

    .line 702
    new-instance v3, Lorg/apache/poi/hslf/usermodel/ObjectData;

    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/poi/hslf/record/ExOleObjStg;

    invoke-direct {v3, v2}, Lorg/apache/poi/hslf/usermodel/ObjectData;-><init>(Lorg/apache/poi/hslf/record/ExOleObjStg;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getPOIFSDirectory()Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    return-object v0
.end method

.method protected getPOIFSFileSystem()Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getFileSystem()Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    return-object v0
.end method

.method public getPictures()[Lorg/apache/poi/hslf/usermodel/PictureData;
    .locals 3

    .prologue
    .line 638
    iget-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    if-nez v1, :cond_0

    .line 640
    :try_start_0
    invoke-direct {p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->readPictures()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hslf/usermodel/PictureData;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/poi/hslf/usermodel/PictureData;

    return-object v1

    .line 641
    :catch_0
    move-exception v0

    .line 642
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    return-object v0
.end method

.method public getUnderlyingBytes()[B
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lorg/apache/poi/hslf/HSLFSlideShow;->_docstream:[B

    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 449
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hslf/HSLFSlideShow;->write(Ljava/io/OutputStream;Z)V

    .line 450
    return-void
.end method

.method public write(Ljava/io/OutputStream;Z)V
    .locals 18
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 463
    new-instance v10, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v10}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>()V

    .line 466
    .local v10, "outFS":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v14, Ljava/util/ArrayList;

    const/4 v15, 0x1

    invoke-direct {v14, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 469
    .local v14, "writtenEntries":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v14}, Lorg/apache/poi/hslf/HSLFSlideShow;->writeProperties(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V

    .line 475
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    .line 481
    .local v9, "oldToNewPositions":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 482
    .local v3, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v15, v15

    if-lt v4, v15, :cond_0

    .line 497
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 498
    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v15, v15

    if-lt v4, v15, :cond_2

    .line 515
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_docstream:[B

    .line 518
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    invoke-direct {v2, v15}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 519
    .local v2, "bais":Ljava/io/ByteArrayInputStream;
    const-string/jumbo v15, "PowerPoint Document"

    invoke-virtual {v10, v2, v15}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 520
    const-string/jumbo v15, "PowerPoint Document"

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v7, v0

    .line 525
    .local v7, "oldLastUserEditAtomPos":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v9, v15}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 526
    .local v5, "newLastUserEditAtomPos":Ljava/lang/Integer;
    if-nez v5, :cond_4

    .line 527
    new-instance v15, Lorg/apache/poi/hslf/exceptions/HSLFException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Couldn\'t find the new location of the UserEditAtom that used to be at "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 483
    .end local v2    # "bais":Ljava/io/ByteArrayInputStream;
    .end local v5    # "newLastUserEditAtomPos":Ljava/lang/Integer;
    .end local v7    # "oldLastUserEditAtomPos":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v15, v15, v4

    instance-of v15, v15, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    if-eqz v15, :cond_1

    .line 484
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v12, v15, v4

    check-cast v12, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    .line 485
    .local v12, "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    invoke-interface {v12}, Lorg/apache/poi/hslf/record/PositionDependentRecord;->getLastOnDiskOffset()I

    move-result v8

    .line 486
    .local v8, "oldPos":I
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v6

    .line 487
    .local v6, "newPos":I
    invoke-interface {v12, v6}, Lorg/apache/poi/hslf/record/PositionDependentRecord;->setLastOnDiskOffset(I)V

    .line 488
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    .end local v6    # "newPos":I
    .end local v8    # "oldPos":I
    .end local v12    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v15, v15, v4

    invoke-virtual {v15, v3}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 482
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 503
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v15, v15, v4

    instance-of v15, v15, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    if-eqz v15, :cond_3

    .line 507
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v12, v15, v4

    check-cast v12, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    .line 508
    .restart local v12    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    invoke-interface {v12, v9}, Lorg/apache/poi/hslf/record/PositionDependentRecord;->updateOtherRecordReferences(Ljava/util/Hashtable;)V

    .line 512
    .end local v12    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v15, v15, v4

    invoke-virtual {v15, v3}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 498
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 529
    .restart local v2    # "bais":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "newLastUserEditAtomPos":Ljava/lang/Integer;
    .restart local v7    # "oldLastUserEditAtomPos":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v16

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->setCurrentEditOffset(J)V

    .line 530
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/poi/hslf/record/CurrentUserAtom;

    invoke-virtual {v15, v10}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->writeToFS(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 531
    const-string/jumbo v15, "Current User"

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 535
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    if-nez v15, :cond_5

    .line 536
    invoke-direct/range {p0 .. p0}, Lorg/apache/poi/hslf/HSLFSlideShow;->readPictures()V

    .line 538
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_6

    .line 539
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 540
    .local v13, "pict":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_8

    .line 544
    new-instance v15, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string/jumbo v16, "Pictures"

    .line 543
    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 546
    const-string/jumbo v15, "Pictures"

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 550
    .end local v13    # "pict":Ljava/io/ByteArrayOutputStream;
    :cond_6
    if-eqz p2, :cond_7

    .line 551
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    invoke-virtual {v15}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getFileSystem()Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v10, v14}, Lorg/apache/poi/hslf/HSLFSlideShow;->copyNodes(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V

    .line 555
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->writeFilesystem(Ljava/io/OutputStream;)V

    .line 556
    return-void

    .line 540
    .restart local v13    # "pict":Ljava/io/ByteArrayOutputStream;
    :cond_8
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hslf/usermodel/PictureData;

    .line 541
    .local v11, "p":Lorg/apache/poi/hslf/usermodel/PictureData;
    invoke-virtual {v11, v13}, Lorg/apache/poi/hslf/usermodel/PictureData;->write(Ljava/io/OutputStream;)V

    goto :goto_2
.end method
