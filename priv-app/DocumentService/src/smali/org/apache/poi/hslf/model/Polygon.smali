.class public final Lorg/apache/poi/hslf/model/Polygon;
.super Lorg/apache/poi/hslf/model/AutoShape;
.source "Polygon.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/Polygon;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 61
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 2
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 52
    const/4 v0, 0x0

    instance-of v1, p1, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/Polygon;->createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Polygon;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 53
    return-void
.end method

.method private findBiggest([F)F
    .locals 3
    .param p1, "values"    # [F

    .prologue
    .line 141
    const/4 v1, 0x1

    .line 142
    .local v1, "result":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_0

    .line 147
    return v1

    .line 144
    :cond_0
    aget v2, p1, v0

    cmpl-float v2, v2, v1

    if-lez v2, :cond_1

    .line 145
    aget v1, p1, v0

    .line 142
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private findSmallest([F)F
    .locals 3
    .param p1, "values"    # [F

    .prologue
    .line 152
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 153
    .local v1, "result":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_0

    .line 158
    return v1

    .line 155
    :cond_0
    aget v2, p1, v0

    cmpg-float v2, v2, v1

    if-gez v2, :cond_1

    .line 156
    aget v1, p1, v0

    .line 153
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public setPoints([F[F)V
    .locals 15
    .param p1, "xPoints"    # [F
    .param p2, "yPoints"    # [F

    .prologue
    .line 71
    invoke-direct/range {p0 .. p1}, Lorg/apache/poi/hslf/model/Polygon;->findBiggest([F)F

    move-result v7

    .line 72
    .local v7, "right":F
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/Polygon;->findBiggest([F)F

    move-result v1

    .line 73
    .local v1, "bottom":F
    invoke-direct/range {p0 .. p1}, Lorg/apache/poi/hslf/model/Polygon;->findSmallest([F)F

    move-result v4

    .line 74
    .local v4, "left":F
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/Polygon;->findSmallest([F)F

    move-result v9

    .line 77
    .local v9, "top":F
    iget-object v11, p0, Lorg/apache/poi/hslf/model/Polygon;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v12, -0xff5

    .line 76
    invoke-static {v11, v12}, Lorg/apache/poi/hslf/model/Polygon;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 78
    .local v6, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    if-nez v6, :cond_0

    .line 135
    :goto_0
    return-void

    .line 80
    :cond_0
    new-instance v11, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 81
    const/16 v12, 0x142

    sub-float v13, v7, v4

    .line 82
    const/high16 v14, 0x42900000    # 72.0f

    mul-float/2addr v13, v14

    const/high16 v14, 0x44100000    # 576.0f

    div-float/2addr v13, v14

    float-to-int v13, v13

    invoke-direct {v11, v12, v13}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 80
    invoke-virtual {v6, v11}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 83
    new-instance v11, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 84
    const/16 v12, 0x143

    sub-float v13, v1, v9

    .line 85
    const/high16 v14, 0x42900000    # 72.0f

    mul-float/2addr v13, v14

    const/high16 v14, 0x44100000    # 576.0f

    div-float/2addr v13, v14

    float-to-int v13, v13

    invoke-direct {v11, v12, v13}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 83
    invoke-virtual {v6, v11}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 87
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v11, v0

    if-lt v3, v11, :cond_1

    .line 92
    move-object/from16 v0, p1

    array-length v5, v0

    .line 94
    .local v5, "numpoints":I
    new-instance v10, Lorg/apache/poi/ddf/EscherArrayProperty;

    .line 95
    const/16 v11, 0x145

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-array v13, v13, [B

    .line 94
    invoke-direct {v10, v11, v12, v13}, Lorg/apache/poi/ddf/EscherArrayProperty;-><init>(SZ[B)V

    .line 96
    .local v10, "verticesProp":Lorg/apache/poi/ddf/EscherArrayProperty;
    add-int/lit8 v11, v5, 0x1

    invoke-virtual {v10, v11}, Lorg/apache/poi/ddf/EscherArrayProperty;->setNumberOfElementsInArray(I)V

    .line 97
    add-int/lit8 v11, v5, 0x1

    invoke-virtual {v10, v11}, Lorg/apache/poi/ddf/EscherArrayProperty;->setNumberOfElementsInMemory(I)V

    .line 98
    const v11, 0xfff0

    invoke-virtual {v10, v11}, Lorg/apache/poi/ddf/EscherArrayProperty;->setSizeOfElements(I)V

    .line 99
    const/4 v3, 0x0

    :goto_2
    if-lt v3, v5, :cond_2

    .line 107
    const/4 v11, 0x4

    new-array v2, v11, [B

    .line 108
    .local v2, "data":[B
    const/4 v11, 0x0

    .line 109
    const/4 v12, 0x0

    aget v12, p1, v12

    const/high16 v13, 0x42900000    # 72.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x44100000    # 576.0f

    div-float/2addr v12, v13

    float-to-int v12, v12

    int-to-short v12, v12

    .line 108
    invoke-static {v2, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 110
    const/4 v11, 0x2

    .line 111
    const/4 v12, 0x0

    aget v12, p2, v12

    const/high16 v13, 0x42900000    # 72.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x44100000    # 576.0f

    div-float/2addr v12, v13

    float-to-int v12, v12

    int-to-short v12, v12

    .line 110
    invoke-static {v2, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 112
    invoke-virtual {v10, v5, v2}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 113
    invoke-virtual {v6, v10}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 115
    new-instance v8, Lorg/apache/poi/ddf/EscherArrayProperty;

    .line 116
    const/16 v11, 0x146

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 115
    invoke-direct {v8, v11, v12, v13}, Lorg/apache/poi/ddf/EscherArrayProperty;-><init>(SZ[B)V

    .line 117
    .local v8, "segmentsProp":Lorg/apache/poi/ddf/EscherArrayProperty;
    const/4 v11, 0x2

    invoke-virtual {v8, v11}, Lorg/apache/poi/ddf/EscherArrayProperty;->setSizeOfElements(I)V

    .line 118
    mul-int/lit8 v11, v5, 0x2

    add-int/lit8 v11, v11, 0x4

    invoke-virtual {v8, v11}, Lorg/apache/poi/ddf/EscherArrayProperty;->setNumberOfElementsInArray(I)V

    .line 119
    mul-int/lit8 v11, v5, 0x2

    add-int/lit8 v11, v11, 0x4

    invoke-virtual {v8, v11}, Lorg/apache/poi/ddf/EscherArrayProperty;->setNumberOfElementsInMemory(I)V

    .line 120
    const/4 v11, 0x0

    const/4 v12, 0x2

    new-array v12, v12, [B

    const/4 v13, 0x1

    const/16 v14, 0x40

    aput-byte v14, v12, v13

    invoke-virtual {v8, v11, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 121
    const/4 v11, 0x1

    const/4 v12, 0x2

    new-array v12, v12, [B

    const/4 v13, 0x1

    const/16 v14, -0x54

    aput-byte v14, v12, v13

    invoke-virtual {v8, v11, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 122
    const/4 v3, 0x0

    :goto_3
    if-lt v3, v5, :cond_3

    .line 128
    invoke-virtual {v8}, Lorg/apache/poi/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    move-result v11

    add-int/lit8 v11, v11, -0x2

    .line 129
    const/4 v12, 0x2

    new-array v12, v12, [B

    fill-array-data v12, :array_0

    .line 128
    invoke-virtual {v8, v11, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 130
    invoke-virtual {v8}, Lorg/apache/poi/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    .line 131
    const/4 v12, 0x2

    new-array v12, v12, [B

    const/4 v13, 0x1

    const/16 v14, -0x80

    aput-byte v14, v12, v13

    .line 130
    invoke-virtual {v8, v11, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 132
    invoke-virtual {v6, v8}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 134
    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherOptRecord;->sortProperties()V

    goto/16 :goto_0

    .line 88
    .end local v2    # "data":[B
    .end local v5    # "numpoints":I
    .end local v8    # "segmentsProp":Lorg/apache/poi/ddf/EscherArrayProperty;
    .end local v10    # "verticesProp":Lorg/apache/poi/ddf/EscherArrayProperty;
    :cond_1
    aget v11, p1, v3

    neg-float v12, v4

    add-float/2addr v11, v12

    aput v11, p1, v3

    .line 89
    aget v11, p2, v3

    neg-float v12, v9

    add-float/2addr v11, v12

    aput v11, p2, v3

    .line 87
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 100
    .restart local v5    # "numpoints":I
    .restart local v10    # "verticesProp":Lorg/apache/poi/ddf/EscherArrayProperty;
    :cond_2
    const/4 v11, 0x4

    new-array v2, v11, [B

    .line 101
    .restart local v2    # "data":[B
    const/4 v11, 0x0

    .line 102
    aget v12, p1, v3

    const/high16 v13, 0x42900000    # 72.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x44100000    # 576.0f

    div-float/2addr v12, v13

    float-to-int v12, v12

    int-to-short v12, v12

    .line 101
    invoke-static {v2, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 103
    const/4 v11, 0x2

    .line 104
    aget v12, p2, v3

    const/high16 v13, 0x42900000    # 72.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x44100000    # 576.0f

    div-float/2addr v12, v13

    float-to-int v12, v12

    int-to-short v12, v12

    .line 103
    invoke-static {v2, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 105
    invoke-virtual {v10, v3, v2}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 123
    .restart local v8    # "segmentsProp":Lorg/apache/poi/ddf/EscherArrayProperty;
    :cond_3
    mul-int/lit8 v11, v3, 0x2

    add-int/lit8 v11, v11, 0x2

    const/4 v12, 0x2

    new-array v12, v12, [B

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput-byte v14, v12, v13

    invoke-virtual {v8, v11, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 125
    mul-int/lit8 v11, v3, 0x2

    add-int/lit8 v11, v11, 0x3

    const/4 v12, 0x2

    new-array v12, v12, [B

    const/4 v13, 0x1

    .line 126
    const/16 v14, -0x54

    aput-byte v14, v12, v13

    .line 125
    invoke-virtual {v8, v11, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 122
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 129
    nop

    :array_0
    .array-data 1
        0x1t
        0x60t
    .end array-data
.end method
