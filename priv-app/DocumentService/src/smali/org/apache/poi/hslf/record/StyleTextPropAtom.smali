.class public final Lorg/apache/poi/hslf/record/StyleTextPropAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "StyleTextPropAtom.java"


# static fields
.field private static _type:J

.field public static characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

.field public static paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;


# instance fields
.field private _header:[B

.field private charStyles:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation
.end field

.field private initialised:Z

.field private paragraphStyles:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation
.end field

.field private rawContents:[B

.field private reserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 58
    const-wide/16 v0, 0xfa1

    sput-wide v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_type:J

    .line 125
    const/16 v0, 0x14

    new-array v0, v0, [Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 126
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "hasBullet"

    invoke-direct {v1, v5, v8, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v5

    .line 127
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "hasBulletFont"

    invoke-direct {v1, v5, v6, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v8

    .line 128
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "hasBulletColor"

    invoke-direct {v1, v5, v7, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v6

    const/4 v1, 0x3

    .line 129
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v3, "hasBulletSize"

    invoke-direct {v2, v5, v9, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 130
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;

    invoke-direct {v1}, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;-><init>()V

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 131
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x80

    const-string/jumbo v4, "bullet.char"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 132
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x10

    const-string/jumbo v4, "bullet.font"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 133
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x40

    const-string/jumbo v4, "bullet.size"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 134
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v2, 0x20

    const-string/jumbo v3, "bullet.color"

    invoke-direct {v1, v7, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v9

    const/16 v1, 0x9

    .line 135
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/AlignmentTextProp;

    invoke-direct {v2}, Lorg/apache/poi/hslf/model/textproperties/AlignmentTextProp;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 136
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x100

    const-string/jumbo v4, "text.offset"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 137
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x400

    const-string/jumbo v4, "bullet.offset"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 138
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x1000

    const-string/jumbo v4, "linespacing"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 139
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x2000

    const-string/jumbo v4, "spacebefore"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 140
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x4000

    const-string/jumbo v4, "spaceafter"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 141
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const v3, 0x8000

    const-string/jumbo v4, "defaultTabSize"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 142
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x100000

    const-string/jumbo v4, "tabStops"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 143
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x10000

    const-string/jumbo v4, "fontAlign"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 144
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0xa0000

    const-string/jumbo v4, "wrapFlags"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 145
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x200000

    const-string/jumbo v4, "textDirection"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 125
    sput-object v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 148
    const/16 v0, 0x19

    new-array v0, v0, [Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 149
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "bold"

    invoke-direct {v1, v5, v8, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v5

    .line 150
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "italic"

    invoke-direct {v1, v5, v6, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v8

    .line 151
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v2, "underline"

    invoke-direct {v1, v5, v7, v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v6

    const/4 v1, 0x3

    .line 152
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const-string/jumbo v3, "unused1"

    invoke-direct {v2, v5, v9, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 153
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v2, 0x10

    const-string/jumbo v3, "shadow"

    invoke-direct {v1, v5, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 154
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x20

    const-string/jumbo v4, "fehint"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 155
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x40

    const-string/jumbo v4, "unused2"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 156
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x80

    const-string/jumbo v4, "kumi"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 157
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v2, 0x100

    const-string/jumbo v3, "unused3"

    invoke-direct {v1, v5, v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v1, v0, v9

    const/16 v1, 0x9

    .line 158
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x200

    const-string/jumbo v4, "emboss"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 159
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x400

    const-string/jumbo v4, "nibble1"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 160
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x800

    const-string/jumbo v4, "nibble2"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 161
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x1000

    const-string/jumbo v4, "nibble3"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 162
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x2000

    const-string/jumbo v4, "nibble4"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 163
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/16 v3, 0x4000

    const-string/jumbo v4, "unused4"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 164
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const v3, 0x8000

    const-string/jumbo v4, "unused5"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 165
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;

    invoke-direct {v2}, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 166
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x10000

    const-string/jumbo v4, "font.index"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 167
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x100000

    const-string/jumbo v4, "pp10ext"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 168
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x200000

    const-string/jumbo v4, "asian.font.index"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 169
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x400000

    const-string/jumbo v4, "ansi.font.index"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 170
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x800000

    const-string/jumbo v4, "symbol.font.index"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 171
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x20000

    const-string/jumbo v4, "font.size"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 172
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x40000

    const-string/jumbo v4, "font.color"

    invoke-direct {v2, v7, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 173
    new-instance v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    const/high16 v3, 0x80000

    const-string/jumbo v4, "superscript"

    invoke-direct {v2, v6, v3, v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 148
    sput-object v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 174
    return-void
.end method

.method public constructor <init>(I)V
    .locals 7
    .param p1, "parentTextSize"    # I

    .prologue
    const/4 v6, 0x0

    .line 209
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 67
    iput-boolean v6, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 210
    const/16 v2, 0x8

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    .line 211
    new-array v2, v6, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 212
    new-array v2, v6, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 215
    iget-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    const/4 v3, 0x2

    sget-wide v4, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_type:J

    long-to-int v4, v4

    int-to-short v4, v4

    invoke-static {v2, v3, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 217
    iget-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    const/4 v3, 0x4

    const/16 v4, 0xa

    invoke-static {v2, v3, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 220
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 221
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 224
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v1, p1, v6}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 225
    .local v1, "defaultParagraphTextProps":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    iget-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 228
    new-instance v0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v0, p1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(I)V

    .line 229
    .local v0, "defaultCharacterTextProps":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    iget-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 232
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 233
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x12

    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 181
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 67
    iput-boolean v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 183
    if-ge p3, v2, :cond_0

    .line 184
    const/16 p3, 0x12

    .line 185
    array-length v0, p1

    sub-int/2addr v0, p2

    if-ge v0, v2, :cond_0

    .line 186
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Not enough data to form a StyleTextPropAtom (min size 18 bytes long) - found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    sub-int/2addr v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_0
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    .line 192
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 196
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 197
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    iget-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v2, v2

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    new-array v0, v3, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 201
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 202
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 203
    return-void
.end method

.method private checkTextLength(III)I
    .locals 4
    .param p1, "readLength"    # I
    .param p2, "handledSoFar"    # I
    .param p3, "overallSize"    # I

    .prologue
    .line 356
    add-int v0, p1, p2

    add-int/lit8 v1, p3, 0x1

    if-le v0, v1, :cond_0

    .line 357
    sget-object v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Style length of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 358
    const-string/jumbo v3, " larger than stated size of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", truncating"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 357
    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 359
    add-int/lit8 v0, p3, 0x1

    sub-int p1, v0, p2

    .line 361
    .end local p1    # "readLength":I
    :cond_0
    return p1
.end method

.method private getCharactersCovered(Ljava/util/LinkedList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "styles":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;>;"
    const/4 v0, 0x0

    .line 118
    .local v0, "length":I
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 121
    return v0

    .line 118
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 119
    .local v1, "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method private updateRawContents()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 369
    iget-boolean v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    if-nez v3, :cond_0

    .line 390
    :goto_0
    return-void

    .line 375
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 378
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 384
    const/4 v1, 0x0

    :goto_2
    iget-object v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 389
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    goto :goto_0

    .line 379
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 380
    .local v2, "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {v2, v0}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->writeOut(Ljava/io/OutputStream;)V

    .line 378
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 385
    .end local v2    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 386
    .restart local v2    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {v2, v0}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->writeOut(Ljava/io/OutputStream;)V

    .line 384
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method


# virtual methods
.method public addCharacterTextPropCollection(I)Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .locals 2
    .param p1, "charactersCovered"    # I

    .prologue
    .line 414
    new-instance v0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v0, p1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(I)V

    .line 415
    .local v0, "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 416
    return-object v0
.end method

.method public addParagraphTextPropCollection(I)Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .locals 2
    .param p1, "charactersCovered"    # I

    .prologue
    .line 404
    new-instance v0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 405
    .local v0, "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 406
    return-object v0
.end method

.method public getCharacterStyles()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getCharacterTextLengthCovered()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharactersCovered(Ljava/util/LinkedList;)I

    move-result v0

    return v0
.end method

.method public getParagraphStyles()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getParagraphTextLengthCovered()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharactersCovered(Ljava/util/LinkedList;)I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 239
    sget-wide v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_type:J

    return-wide v0
.end method

.method public setCharacterStyles(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "cs":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;>;"
    iput-object p1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    return-void
.end method

.method public setParagraphStyles(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "ps":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;>;"
    iput-object p1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    return-void
.end method

.method public setParentTextSize(I)V
    .locals 18
    .param p1, "size"    # I

    .prologue
    .line 271
    const/4 v9, 0x0

    .line 272
    .local v9, "pos":I
    const/4 v11, 0x0

    .line 276
    .local v11, "textHandled":I
    move/from16 v10, p1

    .line 277
    .local v10, "prsize":I
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-ge v9, v14, :cond_1

    if-lt v11, v10, :cond_7

    .line 306
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-lez v14, :cond_2

    add-int/lit8 v14, p1, 0x1

    if-eq v11, v14, :cond_2

    .line 307
    sget-object v14, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v15, 0x5

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Problem reading paragraph style runs: textHandled = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, ", text.size+1 = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    add-int/lit8 v17, p1, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 311
    :cond_2
    const/4 v11, 0x0

    .line 312
    move/from16 v4, p1

    .line 313
    .local v4, "chsize":I
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-ge v9, v14, :cond_4

    if-lt v11, v4, :cond_8

    .line 342
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-lez v14, :cond_5

    add-int/lit8 v14, p1, 0x1

    if-eq v11, v14, :cond_5

    .line 343
    sget-object v14, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v15, 0x5

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Problem reading character style runs: textHandled = "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, ", text.size+1 = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    add-int/lit8 v17, p1, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 347
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-ge v9, v14, :cond_6

    .line 348
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    sub-int/2addr v14, v9

    new-array v14, v14, [B

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 349
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v14, v9, v15, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    :cond_6
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 353
    return-void

    .line 279
    .end local v4    # "chsize":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-static {v14, v9}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v12

    .line 280
    .local v12, "textLen":I
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v12, v11, v1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->checkTextLength(III)I

    move-result v12

    .line 281
    add-int/2addr v11, v12

    .line 282
    add-int/lit8 v9, v9, 0x4

    .line 284
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-static {v14, v9}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .line 285
    .local v5, "indent":S
    add-int/lit8 v9, v9, 0x2

    .line 288
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-static {v14, v9}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v7

    .line 289
    .local v7, "paraFlags":I
    add-int/lit8 v9, v9, 0x4

    .line 292
    new-instance v13, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v13, v12, v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 294
    .local v13, "thisCollection":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    sget-object v14, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 293
    invoke-virtual {v13, v7, v14, v15, v9}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->buildTextPropList(I[Lorg/apache/poi/hslf/model/textproperties/TextProp;[BI)I

    move-result v8

    .line 295
    .local v8, "plSize":I
    add-int/2addr v9, v8

    .line 298
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-virtual {v14, v13}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 301
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-ge v9, v14, :cond_0

    move/from16 v0, p1

    if-ne v11, v0, :cond_0

    .line 302
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 315
    .end local v5    # "indent":S
    .end local v7    # "paraFlags":I
    .end local v8    # "plSize":I
    .end local v12    # "textLen":I
    .end local v13    # "thisCollection":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .restart local v4    # "chsize":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-static {v14, v9}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v12

    .line 316
    .restart local v12    # "textLen":I
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v12, v11, v1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->checkTextLength(III)I

    move-result v12

    .line 317
    add-int/2addr v11, v12

    .line 318
    add-int/lit8 v9, v9, 0x4

    .line 321
    const/4 v6, -0x1

    .line 324
    .local v6, "no_val":S
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-static {v14, v9}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    .line 325
    .local v3, "charFlags":I
    add-int/lit8 v9, v9, 0x4

    .line 329
    new-instance v13, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v13, v12, v6}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 331
    .restart local v13    # "thisCollection":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    sget-object v14, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 330
    invoke-virtual {v13, v3, v14, v15, v9}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->buildTextPropList(I[Lorg/apache/poi/hslf/model/textproperties/TextProp;[BI)I

    move-result v2

    .line 332
    .local v2, "chSize":I
    add-int/2addr v9, v2

    .line 335
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-virtual {v14, v13}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 338
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v14, v14

    if-ge v9, v14, :cond_3

    move/from16 v0, p1

    if-ne v11, v0, :cond_3

    .line 339
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1
.end method

.method public setRawContents([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    const/4 v1, 0x0

    .line 393
    iput-object p1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 394
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 395
    iput-boolean v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 396
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    .line 428
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 430
    .local v3, "out":Ljava/lang/StringBuffer;
    const-string/jumbo v6, "StyleTextPropAtom:\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 431
    iget-boolean v6, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->initialised:Z

    if-nez v6, :cond_1

    .line 432
    const-string/jumbo v6, "Uninitialised, dumping Raw Style Data\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 479
    :cond_0
    const-string/jumbo v6, "  original byte stream \n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 480
    iget-object v6, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-static {v6, v12, v13, v10}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 482
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 435
    :cond_1
    const-string/jumbo v6, "Paragraph properties\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 437
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 457
    const-string/jumbo v6, "Character properties\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 458
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 459
    .local v5, "pr":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "  chars covered: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 460
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "  special mask flags: 0x"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getSpecialMask()I

    move-result v8

    invoke-static {v8}, Lorg/apache/poi/util/HexDump;->toHex(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 461
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getTextPropList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 466
    const-string/jumbo v7, "  char bytes that would be written: \n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 469
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 470
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v5, v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->writeOut(Ljava/io/OutputStream;)V

    .line 471
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 472
    .local v0, "b":[B
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    invoke-static {v0, v8, v9, v7}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 473
    .end local v0    # "b":[B
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v2

    .line 474
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v7, "DocumentService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 437
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "pr":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 438
    .restart local v5    # "pr":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "  chars covered: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 439
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "  special mask flags: 0x"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getSpecialMask()I

    move-result v8

    invoke-static {v8}, Lorg/apache/poi/util/HexDump;->toHex(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 440
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getTextPropList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 445
    const-string/jumbo v7, "  para bytes that would be written: \n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 448
    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 449
    .restart local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v5, v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->writeOut(Ljava/io/OutputStream;)V

    .line 450
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 451
    .restart local v0    # "b":[B
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    invoke-static {v0, v8, v9, v7}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 452
    .end local v0    # "b":[B
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_1
    move-exception v2

    .line 453
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string/jumbo v7, "DocumentService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 440
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 441
    .local v4, "p":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 442
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " (0x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v9

    invoke-static {v9}, Lorg/apache/poi/util/HexDump;->toHex(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ")\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 461
    .end local v4    # "p":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 462
    .restart local v4    # "p":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 463
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " (0x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v9

    invoke-static {v9}, Lorg/apache/poi/util/HexDump;->toHex(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ")\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->updateRawContents()V

    .line 252
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    array-length v1, v1

    iget-object v2, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    array-length v2, v2

    add-int v0, v1, v2

    .line 253
    .local v0, "newSize":I
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 256
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 259
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->rawContents:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 262
    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->reserved:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 263
    return-void
.end method
