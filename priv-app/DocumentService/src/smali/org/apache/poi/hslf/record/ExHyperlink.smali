.class public Lorg/apache/poi/hslf/record/ExHyperlink;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "ExHyperlink.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private linkAtom:Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

.field private linkDetailsA:Lorg/apache/poi/hslf/record/CString;

.field private linkDetailsB:Lorg/apache/poi/hslf/record/CString;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-wide/16 v0, 0xfd7

    sput-wide v0, Lorg/apache/poi/hslf/record/ExHyperlink;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 137
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 138
    const/16 v2, 0x8

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    .line 139
    const/4 v2, 0x3

    new-array v2, v2, [Lorg/apache/poi/hslf/record/Record;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 142
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    const/16 v3, 0xf

    aput-byte v3, v2, v6

    .line 143
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    sget-wide v4, Lorg/apache/poi/hslf/record/ExHyperlink;->_type:J

    long-to-int v3, v4

    int-to-short v3, v3

    invoke-static {v2, v7, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 146
    new-instance v0, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 147
    .local v0, "csa":Lorg/apache/poi/hslf/record/CString;
    new-instance v1, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 148
    .local v1, "csb":Lorg/apache/poi/hslf/record/CString;
    invoke-virtual {v0, v6}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 149
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 150
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    new-instance v3, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    invoke-direct {v3}, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;-><init>()V

    aput-object v3, v2, v6

    .line 151
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 152
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aput-object v1, v2, v7

    .line 153
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExHyperlink;->findInterestingChildren()V

    .line 154
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 99
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 101
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    .line 102
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 106
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExHyperlink;->findInterestingChildren()V

    .line 107
    return-void
.end method

.method private findInterestingChildren()V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v4, 0x0

    .line 117
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v4

    instance-of v1, v1, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v4

    check-cast v1, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkAtom:Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    .line 123
    :goto_0
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 132
    return-void

    .line 120
    .end local v0    # "i":I
    :cond_0
    sget-object v1, Lorg/apache/poi/hslf/record/ExHyperlink;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "First child record wasn\'t a ExHyperlinkAtom, was of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 124
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_3

    .line 125
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/CString;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    .line 123
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 126
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/CString;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    goto :goto_2

    .line 128
    :cond_3
    sget-object v1, Lorg/apache/poi/hslf/record/ExHyperlink;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Record after ExHyperlinkAtom wasn\'t a CString, was of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method public _getDetailsA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public _getDetailsB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getExHyperlinkAtom()Lorg/apache/poi/hslf/record/ExHyperlinkAtom;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkAtom:Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    return-object v0
.end method

.method public getLinkTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLinkURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 159
    sget-wide v0, Lorg/apache/poi/hslf/record/ExHyperlink;->_type:J

    return-wide v0
.end method

.method public setLinkTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method

.method public setLinkURL(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 69
    :cond_0
    return-void
.end method

.method public setLinkURL(Ljava/lang/String;I)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 75
    :cond_0
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/ExHyperlink;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/ExHyperlink;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 167
    return-void
.end method
