.class public abstract Lorg/apache/poi/hslf/blip/Bitmap;
.super Lorg/apache/poi/hslf/usermodel/PictureData;
.source "Bitmap.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 5

    .prologue
    .line 34
    invoke-virtual {p0}, Lorg/apache/poi/hslf/blip/Bitmap;->getRawData()[B

    move-result-object v1

    .line 35
    .local v1, "rawdata":[B
    array-length v2, v1

    add-int/lit8 v2, v2, -0x11

    new-array v0, v2, [B

    .line 36
    .local v0, "imgdata":[B
    const/16 v2, 0x11

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    return-object v0
.end method

.method public setData([B)V
    .locals 3
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 42
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    invoke-static {p1}, Lorg/apache/poi/hslf/blip/Bitmap;->getChecksum([B)[B

    move-result-object v0

    .line 43
    .local v0, "checksum":[B
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 44
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 45
    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 47
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/poi/hslf/blip/Bitmap;->setRawData([B)V

    .line 48
    return-void
.end method
