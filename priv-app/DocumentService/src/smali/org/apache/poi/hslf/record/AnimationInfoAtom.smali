.class public final Lorg/apache/poi/hslf/record/AnimationInfoAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "AnimationInfoAtom.java"


# static fields
.field public static final AnimateBg:I = 0x4000

.field public static final Automatic:I = 0x4

.field public static final Hide:I = 0x1000

.field public static final Play:I = 0x100

.field public static final Reverse:I = 0x1

.field public static final Sound:I = 0x10

.field public static final StopSound:I = 0x40

.field public static final Synchronous:I = 0x400


# instance fields
.field private _header:[B

.field private _recdata:[B


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 79
    const/16 v0, 0x1c

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    .line 81
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    .line 82
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 84
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 85
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 95
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 97
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    .line 98
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    .line 102
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    return-void
.end method


# virtual methods
.method public getDelayTime()I
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getDimColor()I
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getFlag(I)Z
    .locals 1
    .param p1, "bit"    # I

    .prologue
    .line 166
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getMask()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMask()I
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getOrderID()I
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 110
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->AnimationInfoAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSlideCount()I
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0x12

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getSoundIdRef()I
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public setDelayTime(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0xc

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 213
    return-void
.end method

.method public setDimColor(I)V
    .locals 2
    .param p1, "rgb"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 141
    return-void
.end method

.method public setFlag(IZ)V
    .locals 2
    .param p1, "bit"    # I
    .param p2, "value"    # Z

    .prologue
    .line 174
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getMask()I

    move-result v0

    .line 175
    .local v0, "mask":I
    if-eqz p2, :cond_0

    or-int/2addr v0, p1

    .line 177
    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setMask(I)V

    .line 178
    return-void

    .line 176
    :cond_0
    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    goto :goto_0
.end method

.method public setMask(I)V
    .locals 2
    .param p1, "mask"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 159
    return-void
.end method

.method public setOrderID(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0x10

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 233
    return-void
.end method

.method public setSlideCount(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0x12

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 251
    return-void
.end method

.method public setSoundIdRef(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    const/16 v1, 0x8

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 198
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 254
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 255
    .local v0, "buf":Ljava/lang/StringBuffer;
    const-string/jumbo v2, "AnimationInfoAtom\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 256
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\tDimColor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getDimColor()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 257
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getMask()I

    move-result v1

    .line 258
    .local v1, "mask":I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\tMask: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  Reverse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 260
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  Automatic: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  Sound: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x10

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  StopSound: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x40

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 263
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  Play: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x100

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 264
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  Synchronous: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x400

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 265
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  Hide: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1000

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\t  AnimateBg: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x4000

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\tSoundIdRef: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getSoundIdRef()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\tDelayTime: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getDelayTime()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 269
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\tOrderID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getOrderID()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\tSlideCount: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getSlideCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 271
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 122
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->_recdata:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 123
    return-void
.end method
