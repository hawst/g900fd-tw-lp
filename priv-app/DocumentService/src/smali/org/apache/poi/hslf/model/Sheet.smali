.class public abstract Lorg/apache/poi/hslf/model/Sheet;
.super Ljava/lang/Object;
.source "Sheet.java"


# static fields
.field private static logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private _background:Lorg/apache/poi/hslf/model/Background;

.field private _container:Lorg/apache/poi/hslf/record/SheetContainer;

.field private _sheetNo:I

.field private _slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/poi/hslf/model/Sheet;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hslf/model/Sheet;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V
    .locals 0
    .param p1, "container"    # Lorg/apache/poi/hslf/record/SheetContainer;
    .param p2, "sheetNo"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Sheet;->_container:Lorg/apache/poi/hslf/record/SheetContainer;

    .line 63
    iput p2, p0, Lorg/apache/poi/hslf/model/Sheet;->_sheetNo:I

    .line 64
    return-void
.end method

.method protected static findTextRuns(Lorg/apache/poi/hslf/record/EscherTextboxWrapper;Ljava/util/List;)V
    .locals 2
    .param p0, "wrapper"    # Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hslf/record/EscherTextboxWrapper;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hslf/model/TextRun;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "found":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/TextRun;>;"
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getStyleTextProp9Atom()Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/poi/hslf/model/Sheet;->findTextRuns([Lorg/apache/poi/hslf/record/Record;Ljava/util/List;Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V

    .line 164
    return-void
.end method

.method protected static findTextRuns([Lorg/apache/poi/hslf/record/Record;Ljava/util/List;)V
    .locals 1
    .param p0, "records"    # [Lorg/apache/poi/hslf/record/Record;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/poi/hslf/record/Record;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hslf/model/TextRun;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "found":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/TextRun;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/poi/hslf/model/Sheet;->findTextRuns([Lorg/apache/poi/hslf/record/Record;Ljava/util/List;Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V

    .line 153
    return-void
.end method

.method protected static findTextRuns([Lorg/apache/poi/hslf/record/Record;Ljava/util/List;Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V
    .locals 18
    .param p0, "records"    # [Lorg/apache/poi/hslf/record/Record;
    .param p2, "styleTextProp9Atom"    # Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/poi/hslf/record/Record;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hslf/model/TextRun;",
            ">;",
            "Lorg/apache/poi/hslf/record/StyleTextProp9Atom;",
            ")V"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "found":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/TextRun;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v7, 0x0

    .local v7, "slwtIndex":I
    :goto_0
    move-object/from16 v0, p0

    array-length v13, v0

    add-int/lit8 v13, v13, -0x1

    if-lt v2, v13, :cond_0

    .line 227
    return-void

    .line 176
    :cond_0
    aget-object v13, p0, v2

    instance-of v13, v13, Lorg/apache/poi/hslf/record/TextHeaderAtom;

    if-eqz v13, :cond_5

    .line 177
    aget-object v11, p0, v2

    check-cast v11, Lorg/apache/poi/hslf/record/TextHeaderAtom;

    .line 178
    .local v11, "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    const/4 v8, 0x0

    .line 179
    .local v8, "stpa":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    const/4 v12, 0x0

    .line 180
    .local v12, "trun":Lorg/apache/poi/hslf/model/TextRun;
    const/4 v5, 0x0

    .line 184
    .local v5, "next":Lorg/apache/poi/hslf/record/Record;
    move-object/from16 v0, p0

    array-length v13, v0

    add-int/lit8 v13, v13, -0x2

    if-ge v2, v13, :cond_1

    .line 185
    add-int/lit8 v13, v2, 0x2

    aget-object v5, p0, v13

    .line 186
    instance-of v13, v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v13, :cond_1

    move-object v8, v5

    .line 187
    check-cast v8, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 192
    :cond_1
    add-int/lit8 v13, v2, 0x1

    aget-object v5, p0, v13

    .line 193
    instance-of v13, v5, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v13, :cond_6

    move-object v10, v5

    .line 194
    check-cast v10, Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 195
    .local v10, "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    new-instance v12, Lorg/apache/poi/hslf/model/TextRun;

    .end local v12    # "trun":Lorg/apache/poi/hslf/model/TextRun;
    invoke-direct {v12, v11, v10, v8}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    .line 208
    .end local v10    # "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    .restart local v12    # "trun":Lorg/apache/poi/hslf/model/TextRun;
    :cond_2
    :goto_1
    if-eqz v12, :cond_4

    .line 209
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v4, "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/record/Record;>;"
    move v3, v2

    .local v3, "j":I
    :goto_2
    move-object/from16 v0, p0

    array-length v13, v0

    if-lt v3, v13, :cond_9

    .line 214
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    new-array v6, v13, [Lorg/apache/poi/hslf/record/Record;

    .line 215
    .local v6, "recs":[Lorg/apache/poi/hslf/record/Record;
    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 216
    iput-object v6, v12, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    .line 217
    invoke-virtual {v12, v7}, Lorg/apache/poi/hslf/model/TextRun;->setIndex(I)V

    .line 218
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lorg/apache/poi/hslf/model/TextRun;->setStyleTextProp9Atom(Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V

    .line 219
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    add-int/lit8 v2, v2, 0x1

    .line 224
    .end local v3    # "j":I
    .end local v4    # "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/record/Record;>;"
    .end local v6    # "recs":[Lorg/apache/poi/hslf/record/Record;
    :cond_4
    add-int/lit8 v7, v7, 0x1

    .line 175
    .end local v5    # "next":Lorg/apache/poi/hslf/record/Record;
    .end local v8    # "stpa":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .end local v11    # "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    .end local v12    # "trun":Lorg/apache/poi/hslf/model/TextRun;
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 196
    .restart local v5    # "next":Lorg/apache/poi/hslf/record/Record;
    .restart local v8    # "stpa":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .restart local v11    # "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    .restart local v12    # "trun":Lorg/apache/poi/hslf/model/TextRun;
    :cond_6
    instance-of v13, v5, Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v13, :cond_7

    move-object v9, v5

    .line 197
    check-cast v9, Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 198
    .local v9, "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    new-instance v12, Lorg/apache/poi/hslf/model/TextRun;

    .end local v12    # "trun":Lorg/apache/poi/hslf/model/TextRun;
    invoke-direct {v12, v11, v9, v8}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextBytesAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    .line 199
    .restart local v12    # "trun":Lorg/apache/poi/hslf/model/TextRun;
    goto :goto_1

    .end local v9    # "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    :cond_7
    instance-of v13, v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v13, :cond_8

    move-object v8, v5

    .line 200
    check-cast v8, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 201
    goto :goto_1

    :cond_8
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v14

    sget-object v13, Lorg/apache/poi/hslf/record/RecordTypes;->TextSpecInfoAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v13, v13, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v13

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-eqz v13, :cond_2

    .line 202
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v14

    sget-object v13, Lorg/apache/poi/hslf/record/RecordTypes;->BaseTextPropAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v13, v13, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v13

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-eqz v13, :cond_2

    .line 205
    sget-object v13, Lorg/apache/poi/hslf/model/Sheet;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v14, 0x7

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "Found a TextHeaderAtom not followed by a TextBytesAtom or TextCharsAtom: Followed by "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 211
    .restart local v3    # "j":I
    .restart local v4    # "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/record/Record;>;"
    :cond_9
    if-le v3, v2, :cond_a

    aget-object v13, p0, v3

    instance-of v13, v13, Lorg/apache/poi/hslf/record/TextHeaderAtom;

    if-nez v13, :cond_3

    .line 212
    :cond_a
    aget-object v13, p0, v3

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2
.end method

.method public static findTextRuns(Lorg/apache/poi/hslf/record/PPDrawing;)[Lorg/apache/poi/hslf/model/TextRun;
    .locals 7
    .param p0, "ppdrawing"    # Lorg/apache/poi/hslf/record/PPDrawing;

    .prologue
    .line 127
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v1, "runsV":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/TextRun;>;"
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/PPDrawing;->getTextboxWrappers()[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v5

    .line 129
    .local v5, "wrappers":[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v5

    if-lt v0, v6, :cond_0

    .line 141
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/poi/hslf/model/TextRun;

    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/poi/hslf/model/TextRun;

    return-object v6

    .line 130
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 133
    .local v2, "s1":I
    aget-object v6, v5, v0

    invoke-static {v6}, Lorg/apache/poi/hslf/record/RecordContainer;->handleParentAwareRecords(Lorg/apache/poi/hslf/record/RecordContainer;)V

    .line 134
    aget-object v6, v5, v0

    invoke-static {v6, v1}, Lorg/apache/poi/hslf/model/Sheet;->findTextRuns(Lorg/apache/poi/hslf/record/EscherTextboxWrapper;Ljava/util/List;)V

    .line 135
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 136
    .local v3, "s2":I
    if-eq v3, v2, :cond_1

    .line 137
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/model/TextRun;

    .line 138
    .local v4, "t":Lorg/apache/poi/hslf/model/TextRun;
    aget-object v6, v5, v0

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getShapeId()I

    move-result v6

    invoke-virtual {v4, v6}, Lorg/apache/poi/hslf/model/TextRun;->setShapeId(I)V

    .line 129
    .end local v4    # "t":Lorg/apache/poi/hslf/model/TextRun;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public _getSheetNumber()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/poi/hslf/model/Sheet;->_sheetNo:I

    return v0
.end method

.method public _getSheetRefId()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Sheet;->_container:Lorg/apache/poi/hslf/record/SheetContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SheetContainer;->getSheetId()I

    move-result v0

    return v0
.end method

.method public addShape(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 5
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 273
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v1

    .line 275
    .local v1, "ppdrawing":Lorg/apache/poi/hslf/record/PPDrawing;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v0, v3, v4

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 276
    .local v0, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v3, -0xffd

    invoke-static {v0, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 277
    .local v2, "spgr":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 279
    invoke-virtual {p1, p0}, Lorg/apache/poi/hslf/model/Shape;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 280
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->allocateShapeId()I

    move-result v3

    invoke-virtual {p1, v3}, Lorg/apache/poi/hslf/model/Shape;->setShapeId(I)V

    .line 281
    invoke-virtual {p1, p0}, Lorg/apache/poi/hslf/model/Shape;->afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 282
    return-void
.end method

.method public allocateShapeId()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 291
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Sheet;->_slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v5}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Document;->getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/PPDrawingGroup;->getEscherDggRecord()Lorg/apache/poi/ddf/EscherDggRecord;

    move-result-object v2

    .line 292
    .local v2, "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Sheet;->_container:Lorg/apache/poi/hslf/record/SheetContainer;

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/SheetContainer;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherDgRecord()Lorg/apache/poi/ddf/EscherDgRecord;

    move-result-object v1

    .line 294
    .local v1, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getNumShapesSaved()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Lorg/apache/poi/ddf/EscherDggRecord;->setNumShapesSaved(I)V

    .line 297
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v5

    array-length v5, v5

    if-lt v3, v5, :cond_1

    .line 313
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getDrawingGroupId()S

    move-result v5

    invoke-virtual {v2, v5, v7, v7}, Lorg/apache/poi/ddf/EscherDggRecord;->addCluster(IIZ)V

    .line 314
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v5

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v6

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 315
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getNumShapes()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v5}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 316
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v5

    array-length v5, v5

    mul-int/lit16 v4, v5, 0x400

    .line 317
    .local v4, "result":I
    invoke-virtual {v1, v4}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 318
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v5

    if-lt v4, v5, :cond_0

    .line 319
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v2, v5}, Lorg/apache/poi/ddf/EscherDggRecord;->setShapeIdMax(I)V

    .line 320
    :cond_0
    :goto_1
    return v4

    .line 299
    .end local v4    # "result":I
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v5

    aget-object v0, v5, v3

    .line 300
    .local v0, "c":Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    move-result v5

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getDrawingGroupId()S

    move-result v6

    if-ne v5, v6, :cond_2

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    move-result v5

    const/16 v6, 0x400

    if-eq v5, v6, :cond_2

    .line 302
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    move-result v5

    add-int/lit8 v6, v3, 0x1

    mul-int/lit16 v6, v6, 0x400

    add-int v4, v5, v6

    .line 303
    .restart local v4    # "result":I
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 304
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getNumShapes()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v5}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 305
    invoke-virtual {v1, v4}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 306
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v5

    if-lt v4, v5, :cond_0

    .line 307
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v2, v5}, Lorg/apache/poi/ddf/EscherDggRecord;->setShapeIdMax(I)V

    goto :goto_1

    .line 297
    .end local v4    # "result":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public draw(Lorg/apache/poi/java/awt/Graphics2D;)V
    .locals 0
    .param p1, "graphics"    # Lorg/apache/poi/java/awt/Graphics2D;

    .prologue
    .line 398
    return-void
.end method

.method public getBackground()Lorg/apache/poi/hslf/model/Background;
    .locals 7

    .prologue
    .line 377
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Sheet;->_background:Lorg/apache/poi/hslf/model/Background;

    if-nez v5, :cond_1

    .line 378
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v2

    .line 380
    .local v2, "ppdrawing":Lorg/apache/poi/hslf/record/PPDrawing;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v0, v5, v6

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 381
    .local v0, "dg":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/4 v4, 0x0

    .line 383
    .local v4, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 390
    :goto_0
    new-instance v5, Lorg/apache/poi/hslf/model/Background;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6}, Lorg/apache/poi/hslf/model/Background;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    iput-object v5, p0, Lorg/apache/poi/hslf/model/Sheet;->_background:Lorg/apache/poi/hslf/model/Background;

    .line 391
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Sheet;->_background:Lorg/apache/poi/hslf/model/Background;

    invoke-virtual {v5, p0}, Lorg/apache/poi/hslf/model/Background;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 393
    .end local v0    # "dg":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v2    # "ppdrawing":Lorg/apache/poi/hslf/record/PPDrawing;
    .end local v4    # "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Sheet;->_background:Lorg/apache/poi/hslf/model/Background;

    return-object v5

    .line 384
    .restart local v0    # "dg":Lorg/apache/poi/ddf/EscherContainerRecord;
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    .restart local v2    # "ppdrawing":Lorg/apache/poi/hslf/record/PPDrawing;
    .restart local v4    # "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 385
    .local v3, "rec":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v5

    const/16 v6, -0xffc

    if-ne v5, v6, :cond_0

    move-object v4, v3

    .line 386
    check-cast v4, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 387
    goto :goto_0
.end method

.method public getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Sheet;->_container:Lorg/apache/poi/hslf/record/SheetContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SheetContainer;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v0

    return-object v0
.end method

.method public abstract getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;
.end method

.method protected getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Sheet;->_container:Lorg/apache/poi/hslf/record/SheetContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SheetContainer;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v0

    return-object v0
.end method

.method public getPlaceholder(I)Lorg/apache/poi/hslf/model/TextShape;
    .locals 7
    .param p1, "type"    # I

    .prologue
    .line 437
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v4

    .line 438
    .local v4, "shape":[Lorg/apache/poi/hslf/model/Shape;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v4

    if-lt v1, v6, :cond_1

    .line 455
    const/4 v5, 0x0

    :cond_0
    return-object v5

    .line 439
    :cond_1
    aget-object v6, v4, v1

    instance-of v6, v6, Lorg/apache/poi/hslf/model/TextShape;

    if-eqz v6, :cond_3

    .line 440
    aget-object v5, v4, v1

    check-cast v5, Lorg/apache/poi/hslf/model/TextShape;

    .line 441
    .local v5, "tx":Lorg/apache/poi/hslf/model/TextShape;
    const/4 v3, 0x0

    .line 442
    .local v3, "placeholderId":I
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/TextShape;->getPlaceholderAtom()Lorg/apache/poi/hslf/record/OEPlaceholderAtom;

    move-result-object v2

    .line 443
    .local v2, "oep":Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
    if-eqz v2, :cond_4

    .line 444
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    move-result v3

    .line 450
    :cond_2
    :goto_1
    if-eq v3, p1, :cond_0

    .line 438
    .end local v2    # "oep":Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
    .end local v3    # "placeholderId":I
    .end local v5    # "tx":Lorg/apache/poi/hslf/model/TextShape;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 447
    .restart local v2    # "oep":Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
    .restart local v3    # "placeholderId":I
    .restart local v5    # "tx":Lorg/apache/poi/hslf/model/TextShape;
    :cond_4
    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {v5, v6}, Lorg/apache/poi/hslf/model/TextShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;

    .line 448
    .local v0, "hldr":Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;->getPlaceholderId()I

    move-result v3

    goto :goto_1
.end method

.method public getPlaceholderByTextType(I)Lorg/apache/poi/hslf/model/TextShape;
    .locals 5
    .param p1, "type"    # I

    .prologue
    .line 417
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v2

    .line 418
    .local v2, "shape":[Lorg/apache/poi/hslf/model/Shape;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_1

    .line 427
    const/4 v3, 0x0

    :cond_0
    return-object v3

    .line 419
    :cond_1
    aget-object v4, v2, v0

    instance-of v4, v4, Lorg/apache/poi/hslf/model/TextShape;

    if-eqz v4, :cond_2

    .line 420
    aget-object v3, v2, v0

    check-cast v3, Lorg/apache/poi/hslf/model/TextShape;

    .line 421
    .local v3, "tx":Lorg/apache/poi/hslf/model/TextShape;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v1

    .line 422
    .local v1, "run":Lorg/apache/poi/hslf/model/TextRun;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/TextRun;->getRunType()I

    move-result v4

    if-eq v4, p1, :cond_0

    .line 418
    .end local v1    # "run":Lorg/apache/poi/hslf/model/TextRun;
    .end local v3    # "tx":Lorg/apache/poi/hslf/model/TextShape;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getProgrammableTag()Ljava/lang/String;
    .locals 8

    .prologue
    .line 464
    const/4 v3, 0x0

    .line 466
    .local v3, "tag":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v4

    .line 467
    sget-object v5, Lorg/apache/poi/hslf/record/RecordTypes;->ProgTags:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v5, v5, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v5

    .line 466
    invoke-virtual {v4, v6, v7}, Lorg/apache/poi/hslf/record/SheetContainer;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    .line 465
    check-cast v2, Lorg/apache/poi/hslf/record/RecordContainer;

    .line 469
    .local v2, "progTags":Lorg/apache/poi/hslf/record/RecordContainer;
    if-eqz v2, :cond_0

    .line 472
    sget-object v4, Lorg/apache/poi/hslf/record/RecordTypes;->ProgBinaryTag:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v4, v4, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v4, v4

    .line 471
    invoke-virtual {v2, v4, v5}, Lorg/apache/poi/hslf/record/RecordContainer;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 470
    check-cast v1, Lorg/apache/poi/hslf/record/RecordContainer;

    .line 474
    .local v1, "progBinaryTag":Lorg/apache/poi/hslf/record/RecordContainer;
    if-eqz v1, :cond_0

    .line 477
    sget-object v4, Lorg/apache/poi/hslf/record/RecordTypes;->CString:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v4, v4, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v4, v4

    .line 476
    invoke-virtual {v1, v4, v5}, Lorg/apache/poi/hslf/record/RecordContainer;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 475
    check-cast v0, Lorg/apache/poi/hslf/record/CString;

    .line 479
    .local v0, "binaryTag":Lorg/apache/poi/hslf/record/CString;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v3

    .line 483
    .end local v0    # "binaryTag":Lorg/apache/poi/hslf/record/CString;
    .end local v1    # "progBinaryTag":Lorg/apache/poi/hslf/record/RecordContainer;
    :cond_0
    return-object v3
.end method

.method public getShapes()[Lorg/apache/poi/hslf/model/Shape;
    .locals 10

    .prologue
    .line 235
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v2

    .line 237
    .local v2, "ppdrawing":Lorg/apache/poi/hslf/record/PPDrawing;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v0, v8, v9

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 238
    .local v0, "dg":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/4 v7, 0x0

    .line 240
    .local v7, "spgr":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 247
    :goto_0
    if-nez v7, :cond_2

    .line 248
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string/jumbo v9, "spgr not found"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 241
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 242
    .local v3, "rec":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v8

    const/16 v9, -0xffd

    if-ne v8, v9, :cond_0

    move-object v7, v3

    .line 243
    check-cast v7, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 244
    goto :goto_0

    .line 251
    .end local v3    # "rec":Lorg/apache/poi/ddf/EscherRecord;
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v5, "shapes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/Shape;>;"
    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 253
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 255
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 257
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 264
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Lorg/apache/poi/hslf/model/Shape;

    invoke-interface {v5, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/poi/hslf/model/Shape;

    return-object v8

    .line 258
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 259
    .local v6, "sp":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/4 v8, 0x0

    invoke-static {v6, v8}, Lorg/apache/poi/hslf/model/ShapeFactory;->createShape(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Shape;

    move-result-object v4

    .line 260
    .local v4, "sh":Lorg/apache/poi/hslf/model/Shape;
    invoke-virtual {v4, p0}, Lorg/apache/poi/hslf/model/Shape;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 261
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Sheet;->_container:Lorg/apache/poi/hslf/record/SheetContainer;

    return-object v0
.end method

.method public getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Sheet;->_slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    return-object v0
.end method

.method public abstract getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;
.end method

.method protected onAddTextShape(Lorg/apache/poi/hslf/model/TextShape;)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/TextShape;

    .prologue
    .line 408
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 357
    return-void
.end method

.method public removeShape(Lorg/apache/poi/hslf/model/Shape;)Z
    .locals 9
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    const/4 v5, 0x0

    .line 330
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v3

    .line 332
    .local v3, "ppdrawing":Lorg/apache/poi/hslf/record/PPDrawing;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v7

    aget-object v0, v7, v5

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 333
    .local v0, "dg":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/4 v6, 0x0

    .line 335
    .local v6, "spgr":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 342
    :goto_0
    if-nez v6, :cond_2

    .line 349
    :goto_1
    return v5

    .line 336
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherRecord;

    .line 337
    .local v4, "rec":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    const/16 v8, -0xffd

    if-ne v7, v8, :cond_0

    move-object v6, v4

    .line 338
    check-cast v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 339
    goto :goto_0

    .line 346
    .end local v4    # "rec":Lorg/apache/poi/ddf/EscherRecord;
    :cond_2
    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v2

    .line 347
    .local v2, "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v5

    .line 348
    .local v5, "result":Z
    invoke-virtual {v6, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->setChildRecords(Ljava/util/List;)V

    goto :goto_1
.end method

.method public setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V
    .locals 4
    .param p1, "ss"    # Lorg/apache/poi/hslf/usermodel/SlideShow;

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Sheet;->_slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    .line 114
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Sheet;->getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v1

    .line 115
    .local v1, "trs":[Lorg/apache/poi/hslf/model/TextRun;
    if-eqz v1, :cond_0

    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    .line 120
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 117
    .restart local v0    # "i":I
    :cond_1
    aget-object v2, v1, v0

    iget-object v3, p0, Lorg/apache/poi/hslf/model/Sheet;->_slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/model/TextRun;->supplySlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
