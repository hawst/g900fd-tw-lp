.class public Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
.super Ljava/lang/Object;
.source "TextPropCollection.java"


# instance fields
.field private charactersCovered:I

.field private maskSpecial:I

.field private reservedField:S

.field private textPropList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextProp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "textSize"    # I

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    .line 148
    iput p1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->charactersCovered:I

    .line 149
    const/4 v0, -0x1

    iput-short v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->reservedField:S

    .line 150
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    .line 151
    return-void
.end method

.method public constructor <init>(IS)V
    .locals 1
    .param p1, "charactersCovered"    # I
    .param p2, "reservedField"    # S

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    .line 138
    iput p1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->charactersCovered:I

    .line 139
    iput-short p2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->reservedField:S

    .line 140
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    .line 141
    return-void
.end method


# virtual methods
.method public addWithName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "base":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    array-length v5, v5

    if-lt v2, v5, :cond_0

    .line 66
    const/4 v2, 0x0

    :goto_1
    sget-object v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    array-length v5, v5

    if-lt v2, v5, :cond_2

    .line 71
    if-nez v0, :cond_4

    .line 72
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "No TextProp with name "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " is defined to add from"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 62
    :cond_0
    sget-object v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 63
    sget-object v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    aget-object v0, v5, v2

    .line 61
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    :cond_2
    sget-object v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 68
    sget-object v5, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lorg/apache/poi/hslf/model/textproperties/TextProp;

    aget-object v0, v5, v2

    .line 66
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    :cond_4
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 77
    .local v4, "textProp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    const/4 v3, 0x0

    .line 78
    .local v3, "pos":I
    const/4 v2, 0x0

    :goto_2
    iget-object v5, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-lt v2, v5, :cond_5

    .line 84
    iget-object v5, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v5, v3, v4}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 85
    return-object v4

    .line 79
    :cond_5
    iget-object v5, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 80
    .local v1, "curProp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getMask()I

    move-result v5

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getMask()I

    move-result v6

    if-le v5, v6, :cond_6

    .line 81
    add-int/lit8 v3, v3, 0x1

    .line 78
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public buildTextPropList(I[Lorg/apache/poi/hslf/model/textproperties/TextProp;[BI)I
    .locals 6
    .param p1, "containsField"    # I
    .param p2, "potentialProperties"    # [Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .param p3, "data"    # [B
    .param p4, "dataOffset"    # I

    .prologue
    .line 94
    const/4 v0, 0x0

    .line 98
    .local v0, "bytesPassed":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-lt v1, v4, :cond_0

    .line 129
    :goto_1
    return v0

    .line 102
    :cond_0
    aget-object v4, p2, v1

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getMask()I

    move-result v4

    and-int/2addr v4, p1

    if-eqz v4, :cond_3

    .line 103
    add-int v4, p4, v0

    array-length v5, p3

    if-lt v4, v5, :cond_1

    .line 106
    iget v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    aget-object v5, p2, v1

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getMask()I

    move-result v5

    or-int/2addr v4, v5

    iput v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    goto :goto_1

    .line 111
    :cond_1
    aget-object v4, p2, v1

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 112
    .local v2, "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    const/4 v3, 0x0

    .line 113
    .local v3, "val":I
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getSize()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 114
    add-int v4, p4, v0

    invoke-static {p3, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    .line 122
    :cond_2
    :goto_2
    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->setValue(I)V

    .line 123
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getSize()I

    move-result v4

    add-int/2addr v0, v4

    .line 124
    iget-object v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 98
    .end local v2    # "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .end local v3    # "val":I
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    .restart local v2    # "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .restart local v3    # "val":I
    :cond_4
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getSize()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 116
    add-int v4, p4, v0

    invoke-static {p3, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    .line 117
    goto :goto_2

    :cond_5
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getSize()I

    move-result v4

    if-nez v4, :cond_2

    .line 119
    iget v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    aget-object v5, p2, v1

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getMask()I

    move-result v5

    or-int/2addr v4, v5

    iput v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    goto :goto_3
.end method

.method public findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 3
    .param p1, "textPropName"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 54
    const/4 v1, 0x0

    :cond_0
    return-object v1

    .line 49
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 50
    .local v1, "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCharactersCovered()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->charactersCovered:I

    return v0
.end method

.method public getReservedField()S
    .locals 1

    .prologue
    .line 201
    iget-short v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->reservedField:S

    return v0
.end method

.method public getSpecialMask()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    return v0
.end method

.method public getTextPropList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/hslf/model/textproperties/TextProp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public setReservedField(S)V
    .locals 0
    .param p1, "val"    # S

    .prologue
    .line 205
    iput-short p1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->reservedField:S

    .line 206
    return-void
.end method

.method public updateTextSize(I)V
    .locals 0
    .param p1, "textSize"    # I

    .prologue
    .line 158
    iput p1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->charactersCovered:I

    .line 159
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 6
    .param p1, "o"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->charactersCovered:I

    invoke-static {v4, p1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 169
    iget-short v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->reservedField:S

    const/4 v5, -0x1

    if-le v4, v5, :cond_0

    .line 170
    iget-short v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->reservedField:S

    invoke-static {v4, p1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 174
    :cond_0
    iget v1, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->maskSpecial:I

    .line 175
    .local v1, "mask":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 186
    invoke-static {v1, p1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 189
    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lt v0, v4, :cond_4

    .line 198
    return-void

    .line 176
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 179
    .local v2, "textProp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    instance-of v4, v2, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    if-eqz v4, :cond_3

    .line 180
    if-nez v1, :cond_2

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getWriteMask()I

    move-result v4

    or-int/2addr v1, v4

    .line 175
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_3
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getWriteMask()I

    move-result v4

    or-int/2addr v1, v4

    goto :goto_2

    .line 190
    .end local v2    # "textProp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    :cond_4
    iget-object v4, p0, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->textPropList:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 191
    .restart local v2    # "textProp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v3

    .line 192
    .local v3, "val":I
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getSize()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 193
    int-to-short v4, v3

    invoke-static {v4, p1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 189
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 194
    :cond_6
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getSize()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 195
    invoke-static {v3, p1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    goto :goto_3
.end method
