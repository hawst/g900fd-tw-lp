.class public final Lorg/apache/poi/hslf/dev/PPDrawingTextListing;
.super Ljava/lang/Object;
.source "PPDrawingTextListing.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 17
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    move-object/from16 v0, p0

    array-length v15, v0

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 32
    sget-object v15, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v16, "Need to give a filename"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 33
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/System;->exit(I)V

    .line 36
    :cond_0
    new-instance v9, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/4 v15, 0x0

    aget-object v15, p0, v15

    invoke-direct {v9, v15}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 39
    .local v9, "ss":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v9}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v8

    .line 40
    .local v8, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v15, v8

    if-lt v2, v15, :cond_1

    .line 80
    return-void

    .line 41
    :cond_1
    aget-object v15, v8, v2

    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 42
    .local v1, "children":[Lorg/apache/poi/hslf/record/Record;
    if-eqz v1, :cond_2

    array-length v15, v1

    if-eqz v15, :cond_2

    .line 43
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v15, v1

    if-lt v3, v15, :cond_3

    .line 40
    .end local v3    # "j":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 44
    .restart local v3    # "j":I
    :cond_3
    aget-object v15, v1, v3

    instance-of v15, v15, Lorg/apache/poi/hslf/record/PPDrawing;

    if-eqz v15, :cond_4

    .line 48
    aget-object v6, v1, v3

    check-cast v6, Lorg/apache/poi/hslf/record/PPDrawing;

    .line 49
    .local v6, "ppd":Lorg/apache/poi/hslf/record/PPDrawing;
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/PPDrawing;->getTextboxWrappers()[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v14

    .line 53
    .local v14, "wrappers":[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    array-length v15, v14

    if-lt v4, v15, :cond_5

    .line 43
    .end local v4    # "k":I
    .end local v6    # "ppd":Lorg/apache/poi/hslf/record/PPDrawing;
    .end local v14    # "wrappers":[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 54
    .restart local v4    # "k":I
    .restart local v6    # "ppd":Lorg/apache/poi/hslf/record/PPDrawing;
    .restart local v14    # "wrappers":[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    :cond_5
    aget-object v11, v14, v4

    .line 58
    .local v11, "tbw":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    .line 59
    .local v7, "pptatoms":[Lorg/apache/poi/hslf/record/Record;
    const/4 v5, 0x0

    .local v5, "l":I
    :goto_3
    array-length v15, v7

    if-lt v5, v15, :cond_6

    .line 53
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 60
    :cond_6
    const/4 v13, 0x0

    .line 61
    .local v13, "text":Ljava/lang/String;
    aget-object v15, v7, v5

    instance-of v15, v15, Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v15, :cond_7

    .line 62
    aget-object v10, v7, v5

    check-cast v10, Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 63
    .local v10, "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/TextBytesAtom;->getText()Ljava/lang/String;

    move-result-object v13

    .line 65
    .end local v10    # "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    :cond_7
    aget-object v15, v7, v5

    instance-of v15, v15, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v15, :cond_8

    .line 66
    aget-object v12, v7, v5

    check-cast v12, Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 67
    .local v12, "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    invoke-virtual {v12}, Lorg/apache/poi/hslf/record/TextCharsAtom;->getText()Ljava/lang/String;

    move-result-object v13

    .line 70
    .end local v12    # "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    :cond_8
    if-eqz v13, :cond_9

    .line 71
    const/16 v15, 0xd

    const/16 v16, 0xa

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 59
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_3
.end method
