.class public final Lorg/apache/poi/hslf/record/TextSpecInfoAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "TextSpecInfoAtom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;
    }
.end annotation


# instance fields
.field private _data:[B

.field private _header:[B


# direct methods
.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 51
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 53
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_header:[B

    .line 54
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    .line 58
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 60
    return-void
.end method


# virtual methods
.method public getCharactersCovered()I
    .locals 4

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "covered":I
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->getTextSpecInfoRuns()[Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;

    move-result-object v2

    .line 113
    .local v2, "runs":[Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_0

    .line 114
    return v0

    .line 113
    :cond_0
    aget-object v3, v2, v1

    iget v3, v3, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->len:I

    add-int/2addr v0, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->TextSpecInfoAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getTextSpecInfoRuns()[Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x1

    .line 118
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v2, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;>;"
    const/4 v3, 0x0

    .line 120
    .local v3, "pos":I
    const/4 v5, 0x3

    new-array v0, v5, [I

    const/4 v5, 0x0

    aput v7, v0, v5

    aput v6, v0, v6

    .line 121
    .local v0, "bits":[I
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    array-length v5, v5

    if-lt v3, v5, :cond_0

    .line 142
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;

    return-object v5

    .line 122
    :cond_0
    new-instance v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;

    invoke-direct {v4}, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;-><init>()V

    .line 123
    .local v4, "run":Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;
    iget-object v5, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-static {v5, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v5

    iput v5, v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->len:I

    add-int/lit8 v3, v3, 0x4

    .line 124
    iget-object v5, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-static {v5, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v5

    iput v5, v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->mask:I

    add-int/lit8 v3, v3, 0x4

    .line 125
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v5, v0

    if-lt v1, v5, :cond_1

    .line 140
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_1
    iget v5, v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->mask:I

    aget v6, v0, v1

    shl-int v6, v7, v6

    and-int/2addr v5, v6

    if-eqz v5, :cond_2

    .line 127
    aget v5, v0, v1

    packed-switch v5, :pswitch_data_0

    .line 125
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 129
    :pswitch_0
    iget-object v5, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-static {v5, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    iput-short v5, v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->spellInfo:S

    add-int/lit8 v3, v3, 0x2

    .line 130
    goto :goto_2

    .line 132
    :pswitch_1
    iget-object v5, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-static {v5, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    iput-short v5, v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->langId:S

    add-int/lit8 v3, v3, 0x2

    .line 133
    goto :goto_2

    .line 135
    :pswitch_2
    iget-object v5, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-static {v5, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    iput-short v5, v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->altLangId:S

    add-int/lit8 v3, v3, 0x2

    goto :goto_2

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public reset(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 93
    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    .line 95
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-static {v0, v2, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 97
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    const/4 v1, 0x1

    invoke-static {v0, v3, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 99
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    const/16 v1, 0x8

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 102
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_header:[B

    iget-object v1, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    array-length v1, v1

    invoke-static {v0, v3, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 103
    return-void
.end method

.method public setTextSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 86
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 76
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 77
    return-void
.end method
