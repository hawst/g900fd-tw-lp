.class public final Lorg/apache/poi/hslf/usermodel/SoundData;
.super Ljava/lang/Object;
.source "SoundData.java"


# instance fields
.field private _container:Lorg/apache/poi/hslf/record/Sound;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/Sound;)V
    .locals 0
    .param p1, "container"    # Lorg/apache/poi/hslf/record/Sound;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/SoundData;->_container:Lorg/apache/poi/hslf/record/Sound;

    .line 42
    return-void
.end method

.method public static find(Lorg/apache/poi/hslf/record/Document;)[Lorg/apache/poi/hslf/usermodel/SoundData;
    .locals 10
    .param p0, "document"    # Lorg/apache/poi/hslf/record/Document;

    .prologue
    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v4, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/usermodel/SoundData;>;"
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/Document;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 80
    .local v0, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0

    if-lt v2, v6, :cond_0

    .line 92
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/poi/hslf/usermodel/SoundData;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/poi/hslf/usermodel/SoundData;

    return-object v6

    .line 81
    :cond_0
    aget-object v6, v0, v2

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v6

    sget-object v8, Lorg/apache/poi/hslf/record/RecordTypes;->SoundCollection:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v8, v8, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 82
    aget-object v1, v0, v2

    check-cast v1, Lorg/apache/poi/hslf/record/RecordContainer;

    .line 83
    .local v1, "col":Lorg/apache/poi/hslf/record/RecordContainer;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v5

    .line 84
    .local v5, "sr":[Lorg/apache/poi/hslf/record/Record;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v6, v5

    if-lt v3, v6, :cond_2

    .line 80
    .end local v1    # "col":Lorg/apache/poi/hslf/record/RecordContainer;
    .end local v3    # "j":I
    .end local v5    # "sr":[Lorg/apache/poi/hslf/record/Record;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 85
    .restart local v1    # "col":Lorg/apache/poi/hslf/record/RecordContainer;
    .restart local v3    # "j":I
    .restart local v5    # "sr":[Lorg/apache/poi/hslf/record/Record;
    :cond_2
    aget-object v6, v5, v3

    instance-of v6, v6, Lorg/apache/poi/hslf/record/Sound;

    if-eqz v6, :cond_3

    .line 86
    new-instance v7, Lorg/apache/poi/hslf/usermodel/SoundData;

    aget-object v6, v5, v3

    check-cast v6, Lorg/apache/poi/hslf/record/Sound;

    invoke-direct {v7, v6}, Lorg/apache/poi/hslf/usermodel/SoundData;-><init>(Lorg/apache/poi/hslf/record/Sound;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SoundData;->_container:Lorg/apache/poi/hslf/record/Sound;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Sound;->getSoundData()[B

    move-result-object v0

    return-object v0
.end method

.method public getSoundName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SoundData;->_container:Lorg/apache/poi/hslf/record/Sound;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Sound;->getSoundName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSoundType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SoundData;->_container:Lorg/apache/poi/hslf/record/Sound;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Sound;->getSoundType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
