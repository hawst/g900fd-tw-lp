.class public Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;
.super Ljava/lang/Object;
.source "TextSpecInfoAtom.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hslf/record/TextSpecInfoAtom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TextSpecInfoRun"
.end annotation


# instance fields
.field protected altLangId:S

.field protected langId:S

.field protected len:I

.field protected mask:I

.field protected spellInfo:S


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-short v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->spellInfo:S

    .line 155
    iput-short v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->langId:S

    .line 156
    iput-short v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->altLangId:S

    .line 145
    return-void
.end method


# virtual methods
.method public getAltLangId()S
    .locals 1

    .prologue
    .line 190
    iget-short v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->altLangId:S

    return v0
.end method

.method public getLangId()S
    .locals 1

    .prologue
    .line 179
    iget-short v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->spellInfo:S

    return v0
.end method

.method public getSpellInfo()S
    .locals 1

    .prologue
    .line 170
    iget-short v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->spellInfo:S

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lorg/apache/poi/hslf/record/TextSpecInfoAtom$TextSpecInfoRun;->len:I

    return v0
.end method
