.class public final Lorg/apache/poi/hslf/dev/SlideAndNotesAtomListing;
.super Ljava/lang/Object;
.source "SlideAndNotesAtomListing.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 9
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 32
    array-length v6, p0

    if-ge v6, v8, :cond_0

    .line 33
    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v7, "Need to give a filename"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 34
    invoke-static {v8}, Ljava/lang/System;->exit(I)V

    .line 37
    :cond_0
    new-instance v5, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/4 v6, 0x0

    aget-object v6, p0, v6

    invoke-direct {v5, v6}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 41
    .local v5, "ss":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    .line 42
    .local v3, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v3

    if-lt v0, v6, :cond_1

    .line 62
    return-void

    .line 43
    :cond_1
    aget-object v2, v3, v0

    .line 46
    .local v2, "r":Lorg/apache/poi/hslf/record/Record;
    instance-of v6, v2, Lorg/apache/poi/hslf/record/Slide;

    if-eqz v6, :cond_2

    move-object v4, v2

    .line 47
    check-cast v4, Lorg/apache/poi/hslf/record/Slide;

    .line 48
    .local v4, "s":Lorg/apache/poi/hslf/record/Slide;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    .line 54
    .end local v4    # "s":Lorg/apache/poi/hslf/record/Slide;
    :cond_2
    instance-of v6, v2, Lorg/apache/poi/hslf/record/Notes;

    if-eqz v6, :cond_3

    move-object v1, v2

    .line 55
    check-cast v1, Lorg/apache/poi/hslf/record/Notes;

    .line 56
    .local v1, "n":Lorg/apache/poi/hslf/record/Notes;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Notes;->getNotesAtom()Lorg/apache/poi/hslf/record/NotesAtom;

    .line 42
    .end local v1    # "n":Lorg/apache/poi/hslf/record/Notes;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
