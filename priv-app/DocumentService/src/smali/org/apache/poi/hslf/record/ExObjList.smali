.class public Lorg/apache/poi/hslf/record/ExObjList;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "ExObjList.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private exObjListAtom:Lorg/apache/poi/hslf/record/ExObjListAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0x409

    sput-wide v0, Lorg/apache/poi/hslf/record/ExObjList;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 86
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 87
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    .line 88
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 91
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    const/16 v1, 0xf

    aput-byte v1, v0, v4

    .line 92
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/poi/hslf/record/ExObjList;->_type:J

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 95
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    new-instance v1, Lorg/apache/poi/hslf/record/ExObjListAtom;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/ExObjListAtom;-><init>()V

    aput-object v1, v0, v4

    .line 96
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExObjList;->findInterestingChildren()V

    .line 97
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 59
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 61
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    .line 62
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 65
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 66
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExObjList;->findInterestingChildren()V

    .line 67
    return-void
.end method

.method private findInterestingChildren()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v3

    instance-of v0, v0, Lorg/apache/poi/hslf/record/ExObjListAtom;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v3

    check-cast v0, Lorg/apache/poi/hslf/record/ExObjListAtom;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->exObjListAtom:Lorg/apache/poi/hslf/record/ExObjListAtom;

    .line 81
    return-void

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "First child record wasn\'t a ExObjListAtom, was of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public get(I)Lorg/apache/poi/hslf/record/ExHyperlink;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 127
    const/4 v1, 0x0

    :cond_0
    return-object v1

    .line 120
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/ExHyperlink;

    if-eqz v2, :cond_2

    .line 121
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v2, v0

    check-cast v1, Lorg/apache/poi/hslf/record/ExHyperlink;

    .line 122
    .local v1, "rec":Lorg/apache/poi/hslf/record/ExHyperlink;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/ExHyperlink;->getExHyperlinkAtom()Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->getNumber()I

    move-result v2

    if-eq v2, p1, :cond_0

    .line 119
    .end local v1    # "rec":Lorg/apache/poi/hslf/record/ExHyperlink;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getExHyperlinks()[Lorg/apache/poi/hslf/record/ExHyperlink;
    .locals 3

    .prologue
    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v1, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/ExHyperlink;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 53
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hslf/record/ExHyperlink;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hslf/record/ExHyperlink;

    return-object v2

    .line 48
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/ExHyperlink;

    if-eqz v2, :cond_1

    .line 49
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/poi/hslf/record/ExHyperlink;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getExObjListAtom()Lorg/apache/poi/hslf/record/ExObjListAtom;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->exObjListAtom:Lorg/apache/poi/hslf/record/ExObjListAtom;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 102
    sget-wide v0, Lorg/apache/poi/hslf/record/ExObjList;->_type:J

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExObjList;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/ExObjList;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/ExObjList;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/ExObjList;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 110
    return-void
.end method
