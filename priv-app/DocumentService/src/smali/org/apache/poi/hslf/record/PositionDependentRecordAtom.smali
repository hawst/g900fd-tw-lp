.class public abstract Lorg/apache/poi/hslf/record/PositionDependentRecordAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "PositionDependentRecordAtom.java"

# interfaces
.implements Lorg/apache/poi/hslf/record/PositionDependentRecord;


# instance fields
.field protected myLastOnDiskOffset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    return-void
.end method


# virtual methods
.method public getLastOnDiskOffset()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lorg/apache/poi/hslf/record/PositionDependentRecordAtom;->myLastOnDiskOffset:I

    return v0
.end method

.method public setLastOnDiskOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 42
    iput p1, p0, Lorg/apache/poi/hslf/record/PositionDependentRecordAtom;->myLastOnDiskOffset:I

    .line 43
    return-void
.end method

.method public abstract updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method
