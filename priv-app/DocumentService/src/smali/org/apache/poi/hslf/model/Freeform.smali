.class public final Lorg/apache/poi/hslf/model/Freeform;
.super Lorg/apache/poi/hslf/model/AutoShape;
.source "Freeform.java"


# static fields
.field public static final SEGMENTINFO_CLOSE:[B

.field public static final SEGMENTINFO_CUBICTO:[B

.field public static final SEGMENTINFO_CUBICTO2:[B

.field public static final SEGMENTINFO_END:[B

.field public static final SEGMENTINFO_ESCAPE:[B

.field public static final SEGMENTINFO_ESCAPE2:[B

.field public static final SEGMENTINFO_LINETO:[B

.field public static final SEGMENTINFO_MOVETO:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 33
    new-array v0, v2, [B

    const/16 v1, 0x40

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_MOVETO:[B

    .line 34
    new-array v0, v2, [B

    const/16 v1, -0x54

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_LINETO:[B

    .line 35
    new-array v0, v2, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_ESCAPE:[B

    .line 36
    new-array v0, v2, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_ESCAPE2:[B

    .line 37
    new-array v0, v2, [B

    const/16 v1, -0x53

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_CUBICTO:[B

    .line 38
    new-array v0, v2, [B

    const/16 v1, -0x4d

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_CUBICTO2:[B

    .line 39
    new-array v0, v2, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_CLOSE:[B

    .line 40
    new-array v0, v2, [B

    const/16 v1, -0x80

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/poi/hslf/model/Freeform;->SEGMENTINFO_END:[B

    return-void

    .line 36
    :array_0
    .array-data 1
        0x1t
        0x20t
    .end array-data

    .line 39
    nop

    :array_1
    .array-data 1
        0x1t
        0x60t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/Freeform;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 70
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 2
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 61
    const/4 v0, 0x0

    instance-of v1, p1, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/Freeform;->createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Freeform;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 62
    return-void
.end method
