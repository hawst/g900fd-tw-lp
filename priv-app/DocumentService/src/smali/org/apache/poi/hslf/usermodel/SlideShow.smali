.class public final Lorg/apache/poi/hslf/usermodel/SlideShow;
.super Ljava/lang/Object;
.source "SlideShow.java"


# instance fields
.field private _documentRecord:Lorg/apache/poi/hslf/record/Document;

.field private _fonts:Lorg/apache/poi/hslf/record/FontCollection;

.field private _hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

.field private _masters:[Lorg/apache/poi/hslf/model/SlideMaster;

.field private _mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

.field private _notes:[Lorg/apache/poi/hslf/model/Notes;

.field private _records:[Lorg/apache/poi/hslf/record/Record;

.field private _sheetIdToCoreRecordsLookup:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private _slides:[Lorg/apache/poi/hslf/model/Slide;

.field private _titleMasters:[Lorg/apache/poi/hslf/model/TitleMaster;

.field private logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-static {}, Lorg/apache/poi/hslf/HSLFSlideShow;->create()Lorg/apache/poi/hslf/HSLFSlideShow;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/SlideShow;-><init>(Lorg/apache/poi/hslf/HSLFSlideShow;)V

    .line 117
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-direct {v0, p1}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/SlideShow;-><init>(Lorg/apache/poi/hslf/HSLFSlideShow;)V

    .line 124
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/HSLFSlideShow;)V
    .locals 5
    .param p1, "hslfSlideShow"    # Lorg/apache/poi/hslf/HSLFSlideShow;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    .line 95
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    .line 96
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    .line 99
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 106
    invoke-direct {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->findMostRecentCoreRecords()V

    .line 109
    invoke-direct {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->buildSlidesAndNotes()V

    .line 110
    return-void

    .line 99
    :cond_0
    aget-object v0, v2, v1

    .line 100
    .local v0, "record":Lorg/apache/poi/hslf/record/Record;
    instance-of v4, v0, Lorg/apache/poi/hslf/record/RecordContainer;

    if-eqz v4, :cond_1

    .line 101
    check-cast v0, Lorg/apache/poi/hslf/record/RecordContainer;

    .end local v0    # "record":Lorg/apache/poi/hslf/record/Record;
    invoke-static {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->handleParentAwareRecords(Lorg/apache/poi/hslf/record/RecordContainer;)V

    .line 99
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private buildSlidesAndNotes()V
    .locals 30

    .prologue
    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    if-nez v2, :cond_0

    .line 258
    new-instance v2, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    .line 259
    const-string/jumbo v3, "The PowerPoint file didn\'t contain a Document Record in its PersistPtr blocks. It is probably corrupt."

    .line 258
    invoke-direct {v2, v3}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 279
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Document;->getMasterSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;

    move-result-object v10

    .line 280
    .local v10, "masterSLWT":Lorg/apache/poi/hslf/record/SlideListWithText;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Document;->getSlideSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;

    move-result-object v25

    .line 281
    .local v25, "slidesSLWT":Lorg/apache/poi/hslf/record/SlideListWithText;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Document;->getNotesSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;

    move-result-object v18

    .line 289
    .local v18, "notesSLWT":Lorg/apache/poi/hslf/record/SlideListWithText;
    const/4 v11, 0x0

    .line 290
    .local v11, "masterSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    if-eqz v10, :cond_2

    .line 291
    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/SlideListWithText;->getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-result-object v11

    .line 293
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v12, "mmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/SlideMaster;>;"
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 296
    .local v28, "tmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/TitleMaster;>;"
    if-eqz v11, :cond_1

    .line 297
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v2, v11

    if-lt v8, v2, :cond_6

    .line 315
    .end local v8    # "i":I
    :cond_1
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hslf/model/SlideMaster;

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_masters:[Lorg/apache/poi/hslf/model/SlideMaster;

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_masters:[Lorg/apache/poi/hslf/model/SlideMaster;

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 318
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hslf/model/TitleMaster;

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_titleMasters:[Lorg/apache/poi/hslf/model/TitleMaster;

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_titleMasters:[Lorg/apache/poi/hslf/model/TitleMaster;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 326
    .end local v12    # "mmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/SlideMaster;>;"
    .end local v28    # "tmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/TitleMaster;>;"
    :cond_2
    const/16 v16, 0x0

    .line 329
    .local v16, "notesRecords":[Lorg/apache/poi/hslf/record/Notes;
    const/16 v19, 0x0

    .line 330
    .local v19, "notesSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    new-instance v23, Ljava/util/Hashtable;

    invoke-direct/range {v23 .. v23}, Ljava/util/Hashtable;-><init>()V

    .line 331
    .local v23, "slideIdToNotes":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v18, :cond_9

    .line 333
    const/4 v2, 0x0

    new-array v0, v2, [Lorg/apache/poi/hslf/record/Notes;

    move-object/from16 v16, v0

    .line 368
    :cond_3
    :goto_1
    const/4 v2, 0x0

    new-array v0, v2, [Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-object/from16 v26, v0

    .line 369
    .local v26, "slidesSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    if-nez v25, :cond_c

    .line 371
    const/4 v2, 0x0

    new-array v0, v2, [Lorg/apache/poi/hslf/record/Slide;

    move-object/from16 v24, v0

    .line 395
    .local v24, "slidesRecords":[Lorg/apache/poi/hslf/record/Slide;
    :cond_4
    if-eqz v16, :cond_5

    .line 396
    move-object/from16 v0, v16

    array-length v2, v0

    new-array v2, v2, [Lorg/apache/poi/hslf/model/Notes;

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    .line 397
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    array-length v2, v2

    if-lt v8, v2, :cond_e

    .line 403
    .end local v8    # "i":I
    :cond_5
    move-object/from16 v0, v24

    array-length v2, v0

    new-array v2, v2, [Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    .line 404
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    array-length v2, v2

    if-lt v8, v2, :cond_f

    .line 425
    return-void

    .line 298
    .end local v16    # "notesRecords":[Lorg/apache/poi/hslf/record/Notes;
    .end local v19    # "notesSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .end local v23    # "slideIdToNotes":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v24    # "slidesRecords":[Lorg/apache/poi/hslf/record/Slide;
    .end local v26    # "slidesSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .restart local v12    # "mmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/SlideMaster;>;"
    .restart local v28    # "tmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/TitleMaster;>;"
    :cond_6
    aget-object v2, v11, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getCoreRecordForSAS(Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v20

    .line 299
    .local v20, "r":Lorg/apache/poi/hslf/record/Record;
    aget-object v5, v11, v8

    .line 300
    .local v5, "sas":Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v21

    .line 301
    .local v21, "sheetNo":I
    move-object/from16 v0, v20

    instance-of v2, v0, Lorg/apache/poi/hslf/record/Slide;

    if-eqz v2, :cond_8

    .line 302
    new-instance v9, Lorg/apache/poi/hslf/model/TitleMaster;

    check-cast v20, Lorg/apache/poi/hslf/record/Slide;

    .end local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v9, v0, v1}, Lorg/apache/poi/hslf/model/TitleMaster;-><init>(Lorg/apache/poi/hslf/record/Slide;I)V

    .line 304
    .local v9, "master":Lorg/apache/poi/hslf/model/TitleMaster;
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Lorg/apache/poi/hslf/model/TitleMaster;->setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 305
    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    .end local v9    # "master":Lorg/apache/poi/hslf/model/TitleMaster;
    :cond_7
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 306
    .restart local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    :cond_8
    move-object/from16 v0, v20

    instance-of v2, v0, Lorg/apache/poi/hslf/record/MainMaster;

    if-eqz v2, :cond_7

    .line 307
    new-instance v9, Lorg/apache/poi/hslf/model/SlideMaster;

    check-cast v20, Lorg/apache/poi/hslf/record/MainMaster;

    .end local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v9, v0, v1}, Lorg/apache/poi/hslf/model/SlideMaster;-><init>(Lorg/apache/poi/hslf/record/MainMaster;I)V

    .line 309
    .local v9, "master":Lorg/apache/poi/hslf/model/SlideMaster;
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Lorg/apache/poi/hslf/model/SlideMaster;->setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 310
    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 336
    .end local v5    # "sas":Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .end local v8    # "i":I
    .end local v9    # "master":Lorg/apache/poi/hslf/model/SlideMaster;
    .end local v12    # "mmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/SlideMaster;>;"
    .end local v21    # "sheetNo":I
    .end local v28    # "tmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/model/TitleMaster;>;"
    .restart local v16    # "notesRecords":[Lorg/apache/poi/hslf/record/Notes;
    .restart local v19    # "notesSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .restart local v23    # "slideIdToNotes":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_9
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hslf/record/SlideListWithText;->getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-result-object v19

    .line 337
    if-eqz v19, :cond_3

    .line 339
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 340
    .local v17, "notesRecordsL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/Notes;>;"
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_5
    move-object/from16 v0, v19

    array-length v2, v0

    if-lt v8, v2, :cond_a

    .line 361
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [Lorg/apache/poi/hslf/record/Notes;

    move-object/from16 v16, v0

    .line 362
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "notesRecords":[Lorg/apache/poi/hslf/record/Notes;
    check-cast v16, [Lorg/apache/poi/hslf/record/Notes;

    .restart local v16    # "notesRecords":[Lorg/apache/poi/hslf/record/Notes;
    goto/16 :goto_1

    .line 342
    :cond_a
    aget-object v2, v19, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getCoreRecordForSAS(Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v20

    .line 345
    .restart local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    move-object/from16 v0, v20

    instance-of v2, v0, Lorg/apache/poi/hslf/record/Notes;

    if-eqz v2, :cond_b

    move-object/from16 v15, v20

    .line 346
    check-cast v15, Lorg/apache/poi/hslf/record/Notes;

    .line 347
    .local v15, "notesRecord":Lorg/apache/poi/hslf/record/Notes;
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    aget-object v2, v19, v8

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v27

    .line 351
    .local v27, "spa":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    .line 352
    .local v22, "slideId":Ljava/lang/Integer;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    .end local v15    # "notesRecord":Lorg/apache/poi/hslf/record/Notes;
    .end local v22    # "slideId":Ljava/lang/Integer;
    .end local v27    # "spa":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 354
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v29, "A Notes SlideAtomSet at "

    move-object/from16 v0, v29

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 355
    const-string/jumbo v29, " said its record was at refID "

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 356
    aget-object v29, v19, v8

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getRefID()I

    move-result v29

    move/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 357
    const-string/jumbo v29, ", but that was actually a "

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 354
    invoke-virtual {v2, v3, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_6

    .line 374
    .end local v8    # "i":I
    .end local v17    # "notesRecordsL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/Notes;>;"
    .end local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    .restart local v26    # "slidesSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    :cond_c
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hslf/record/SlideListWithText;->getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-result-object v26

    .line 375
    move-object/from16 v0, v26

    array-length v2, v0

    new-array v0, v2, [Lorg/apache/poi/hslf/record/Slide;

    move-object/from16 v24, v0

    .line 376
    .restart local v24    # "slidesRecords":[Lorg/apache/poi/hslf/record/Slide;
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_7
    move-object/from16 v0, v26

    array-length v2, v0

    if-ge v8, v2, :cond_4

    .line 378
    aget-object v2, v26, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getCoreRecordForSAS(Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v20

    .line 381
    .restart local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    move-object/from16 v0, v20

    instance-of v2, v0, Lorg/apache/poi/hslf/record/Slide;

    if-eqz v2, :cond_d

    .line 382
    check-cast v20, Lorg/apache/poi/hslf/record/Slide;

    .end local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    aput-object v20, v24, v8

    .line 376
    :goto_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 384
    .restart local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v29, "A Slide SlideAtomSet at "

    move-object/from16 v0, v29

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 385
    const-string/jumbo v29, " said its record was at refID "

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 386
    aget-object v29, v26, v8

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getRefID()I

    move-result v29

    move/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 387
    const-string/jumbo v29, ", but that was actually a "

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 384
    invoke-virtual {v2, v3, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_8

    .line 398
    .end local v20    # "r":Lorg/apache/poi/hslf/record/Record;
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    new-instance v3, Lorg/apache/poi/hslf/model/Notes;

    aget-object v7, v16, v8

    invoke-direct {v3, v7}, Lorg/apache/poi/hslf/model/Notes;-><init>(Lorg/apache/poi/hslf/record/Notes;)V

    aput-object v3, v2, v8

    .line 399
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    aget-object v2, v2, v8

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lorg/apache/poi/hslf/model/Notes;->setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 397
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 405
    :cond_f
    aget-object v5, v26, v8

    .line 406
    .restart local v5    # "sas":Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v6

    .line 409
    .local v6, "slideIdentifier":I
    const/4 v4, 0x0

    .line 412
    .local v4, "notes":Lorg/apache/poi/hslf/model/Notes;
    aget-object v2, v24, v8

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/SlideAtom;->getNotesID()I

    move-result v13

    .line 413
    .local v13, "noteId":I
    if-eqz v13, :cond_10

    .line 414
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    .line 415
    .local v14, "notesPos":Ljava/lang/Integer;
    if-eqz v14, :cond_11

    .line 416
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v4, v2, v3

    .line 422
    .end local v14    # "notesPos":Ljava/lang/Integer;
    :cond_10
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v29, v0

    new-instance v2, Lorg/apache/poi/hslf/model/Slide;

    aget-object v3, v24, v8

    add-int/lit8 v7, v8, 0x1

    invoke-direct/range {v2 .. v7}, Lorg/apache/poi/hslf/model/Slide;-><init>(Lorg/apache/poi/hslf/record/Slide;Lorg/apache/poi/hslf/model/Notes;Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;II)V

    aput-object v2, v29, v8

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    aget-object v2, v2, v8

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lorg/apache/poi/hslf/model/Slide;->setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 404
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3

    .line 418
    .restart local v14    # "notesPos":Ljava/lang/Integer;
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v29, "Notes not found for noteId="

    move-object/from16 v0, v29

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_9
.end method

.method private findMostRecentCoreRecords()V
    .locals 22

    .prologue
    .line 134
    new-instance v8, Ljava/util/Hashtable;

    invoke-direct {v8}, Ljava/util/Hashtable;-><init>()V

    .line 135
    .local v8, "mostRecentByBytes":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v3, v0, :cond_0

    .line 160
    invoke-virtual {v8}, Ljava/util/Hashtable;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    .line 164
    new-instance v18, Ljava/util/Hashtable;

    invoke-direct/range {v18 .. v18}, Ljava/util/Hashtable;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hslf/usermodel/SlideShow;->_sheetIdToCoreRecordsLookup:Ljava/util/Hashtable;

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v2, v0, [I

    .line 166
    .local v2, "allIDs":[I
    invoke-virtual {v8}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v6

    .line 167
    .local v6, "ids":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    :goto_1
    array-length v0, v2

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v3, v0, :cond_5

    .line 171
    invoke-static {v2}, Ljava/util/Arrays;->sort([I)V

    .line 172
    const/4 v3, 0x0

    :goto_2
    array-length v0, v2

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v3, v0, :cond_6

    .line 177
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v3, v0, :cond_7

    .line 206
    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v3, v0, :cond_c

    .line 219
    return-void

    .line 136
    .end local v2    # "allIDs":[I
    .end local v6    # "ids":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v18, v18, v3

    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    move/from16 v18, v0

    if-eqz v18, :cond_1

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v11, v18, v3

    check-cast v11, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    .line 141
    .local v11, "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v5

    .line 142
    .local v5, "ids":[I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_5
    array-length v0, v5

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v7, v0, :cond_2

    .line 150
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v17

    .line 151
    .local v17, "thisSetOfLocations":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    :goto_6
    array-length v0, v5

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v7, v0, :cond_4

    .line 135
    .end local v5    # "ids":[I
    .end local v7    # "j":I
    .end local v11    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v17    # "thisSetOfLocations":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 143
    .restart local v5    # "ids":[I
    .restart local v7    # "j":I
    .restart local v11    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    :cond_2
    aget v18, v5, v7

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 144
    .local v4, "id":Ljava/lang/Integer;
    invoke-virtual {v8, v4}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 145
    invoke-virtual {v8, v4}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 152
    .end local v4    # "id":Ljava/lang/Integer;
    .restart local v17    # "thisSetOfLocations":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_4
    aget v18, v5, v7

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 153
    .restart local v4    # "id":Ljava/lang/Integer;
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    move-object/from16 v0, v18

    invoke-virtual {v8, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 168
    .end local v4    # "id":Ljava/lang/Integer;
    .end local v5    # "ids":[I
    .end local v7    # "j":I
    .end local v11    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v17    # "thisSetOfLocations":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .restart local v2    # "allIDs":[I
    .restart local v6    # "ids":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    :cond_5
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 169
    .restart local v4    # "id":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v18

    aput v18, v2, v3

    .line 167
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 173
    .end local v4    # "id":Ljava/lang/Integer;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_sheetIdToCoreRecordsLookup:Ljava/util/Hashtable;

    move-object/from16 v18, v0

    aget v19, v2, v3

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 178
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v18, v18, v3

    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    move/from16 v18, v0

    if-eqz v18, :cond_8

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v9, v18, v3

    check-cast v9, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    .line 180
    .local v9, "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    invoke-interface {v9}, Lorg/apache/poi/hslf/record/PositionDependentRecord;->getLastOnDiskOffset()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 183
    .local v12, "recordAt":Ljava/lang/Integer;
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_7
    array-length v0, v2

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v7, v0, :cond_9

    .line 177
    .end local v7    # "j":I
    .end local v9    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    .end local v12    # "recordAt":Ljava/lang/Integer;
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 184
    .restart local v7    # "j":I
    .restart local v9    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    .restart local v12    # "recordAt":Ljava/lang/Integer;
    :cond_9
    aget v18, v2, v7

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 185
    .local v16, "thisID":Ljava/lang/Integer;
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    .line 187
    .local v15, "thatRecordAt":Ljava/lang/Integer;
    invoke-virtual {v15, v12}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_sheetIdToCoreRecordsLookup:Ljava/util/Hashtable;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    .line 190
    .local v14, "storeAtI":Ljava/lang/Integer;
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 193
    .local v13, "storeAt":I
    instance-of v0, v9, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;

    move/from16 v18, v0

    if-eqz v18, :cond_a

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v10, v18, v3

    check-cast v10, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;

    .line 195
    .local v10, "pdrc":Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;->setSheetId(I)V

    .line 199
    .end local v10    # "pdrc":Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v19, v0

    aget-object v19, v19, v3

    aput-object v19, v18, v13

    .line 183
    .end local v13    # "storeAt":I
    .end local v14    # "storeAtI":Ljava/lang/Integer;
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 208
    .end local v7    # "j":I
    .end local v9    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    .end local v12    # "recordAt":Ljava/lang/Integer;
    .end local v15    # "thatRecordAt":Ljava/lang/Integer;
    .end local v16    # "thisID":Ljava/lang/Integer;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v18, v18, v3

    if-eqz v18, :cond_d

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v18, v18, v3

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v18

    sget-object v20, Lorg/apache/poi/hslf/record/RecordTypes;->Document:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-nez v18, :cond_d

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v18, v0

    aget-object v18, v18, v3

    check-cast v18, Lorg/apache/poi/hslf/record/Document;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hslf/record/Document;->getEnvironment()Lorg/apache/poi/hslf/record/Environment;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hslf/record/Environment;->getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hslf/usermodel/SlideShow;->_fonts:Lorg/apache/poi/hslf/record/FontCollection;

    .line 206
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4
.end method

.method private getCoreRecordForRefID(I)Lorg/apache/poi/hslf/record/Record;
    .locals 6
    .param p1, "refID"    # I

    .prologue
    .line 239
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_sheetIdToCoreRecordsLookup:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 240
    .local v0, "coreRecordId":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 241
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, v2, v3

    .line 247
    :goto_0
    return-object v1

    .line 244
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x7

    .line 245
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "We tried to look up a reference to a core record, but there was no core ID for reference ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 246
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 245
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 244
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 247
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCoreRecordForSAS(Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;)Lorg/apache/poi/hslf/record/Record;
    .locals 3
    .param p1, "sas"    # Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .prologue
    .line 226
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v1

    .line 227
    .local v1, "spa":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getRefID()I

    move-result v0

    .line 228
    .local v0, "refID":I
    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getCoreRecordForRefID(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public addControl(Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "progId"    # Ljava/lang/String;

    .prologue
    .line 1042
    iget-object v5, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->ExObjList:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/hslf/record/Document;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/record/ExObjList;

    .line 1043
    .local v1, "lst":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v1, :cond_0

    .line 1044
    new-instance v1, Lorg/apache/poi/hslf/record/ExObjList;

    .end local v1    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    invoke-direct {v1}, Lorg/apache/poi/hslf/record/ExObjList;-><init>()V

    .line 1045
    .restart local v1    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    iget-object v5, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Document;->getDocumentAtom()Lorg/apache/poi/hslf/record/DocumentAtom;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lorg/apache/poi/hslf/record/Document;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 1047
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/ExObjList;->getExObjListAtom()Lorg/apache/poi/hslf/record/ExObjListAtom;

    move-result-object v2

    .line 1049
    .local v2, "objAtom":Lorg/apache/poi/hslf/record/ExObjListAtom;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/ExObjListAtom;->getObjectIDSeed()J

    move-result-wide v6

    long-to-int v5, v6

    add-int/lit8 v3, v5, 0x1

    .line 1050
    .local v3, "objectId":I
    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/record/ExObjListAtom;->setObjectIDSeed(I)V

    .line 1051
    new-instance v0, Lorg/apache/poi/hslf/record/ExControl;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/ExControl;-><init>()V

    .line 1052
    .local v0, "ctrl":Lorg/apache/poi/hslf/record/ExControl;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExControl;->getExOleObjAtom()Lorg/apache/poi/hslf/record/ExOleObjAtom;

    move-result-object v4

    .line 1053
    .local v4, "oleObj":Lorg/apache/poi/hslf/record/ExOleObjAtom;
    invoke-virtual {v4, v3}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->setObjID(I)V

    .line 1054
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->setDrawAspect(I)V

    .line 1055
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->setType(I)V

    .line 1056
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/poi/hslf/record/ExOleObjAtom;->setSubType(I)V

    .line 1058
    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/record/ExControl;->setProgId(Ljava/lang/String;)V

    .line 1059
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/ExControl;->setMenuName(Ljava/lang/String;)V

    .line 1060
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/ExControl;->setClipboardName(Ljava/lang/String;)V

    .line 1061
    invoke-virtual {v1, v0, v2}, Lorg/apache/poi/hslf/record/ExObjList;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 1063
    return v3
.end method

.method public addFont(Lorg/apache/poi/hslf/model/PPFont;)I
    .locals 7
    .param p1, "font"    # Lorg/apache/poi/hslf/model/PPFont;

    .prologue
    .line 894
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Document;->getEnvironment()Lorg/apache/poi/hslf/record/Environment;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Environment;->getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;

    move-result-object v0

    .line 895
    .local v0, "fonts":Lorg/apache/poi/hslf/record/FontCollection;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/PPFont;->getFontName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/FontCollection;->getFontIndex(Ljava/lang/String;)I

    move-result v6

    .line 896
    .local v6, "idx":I
    const/4 v1, -0x1

    if-ne v6, v1, :cond_0

    .line 897
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/PPFont;->getFontName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/PPFont;->getCharSet()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/PPFont;->getFontFlags()I

    move-result v3

    .line 898
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/PPFont;->getFontType()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/PPFont;->getPitchAndFamily()I

    move-result v5

    .line 897
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hslf/record/FontCollection;->addFont(Ljava/lang/String;IIII)I

    move-result v6

    .line 900
    :cond_0
    return v6
.end method

.method public addHyperlink(Lorg/apache/poi/hslf/model/Hyperlink;)I
    .locals 8
    .param p1, "link"    # Lorg/apache/poi/hslf/model/Hyperlink;

    .prologue
    .line 1072
    iget-object v5, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->ExObjList:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/hslf/record/Document;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/record/ExObjList;

    .line 1073
    .local v1, "lst":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v1, :cond_0

    .line 1074
    new-instance v1, Lorg/apache/poi/hslf/record/ExObjList;

    .end local v1    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    invoke-direct {v1}, Lorg/apache/poi/hslf/record/ExObjList;-><init>()V

    .line 1075
    .restart local v1    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    iget-object v5, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Document;->getDocumentAtom()Lorg/apache/poi/hslf/record/DocumentAtom;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lorg/apache/poi/hslf/record/Document;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 1077
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/ExObjList;->getExObjListAtom()Lorg/apache/poi/hslf/record/ExObjListAtom;

    move-result-object v3

    .line 1079
    .local v3, "objAtom":Lorg/apache/poi/hslf/record/ExObjListAtom;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/ExObjListAtom;->getObjectIDSeed()J

    move-result-wide v6

    long-to-int v5, v6

    add-int/lit8 v4, v5, 0x1

    .line 1080
    .local v4, "objectId":I
    invoke-virtual {v3, v4}, Lorg/apache/poi/hslf/record/ExObjListAtom;->setObjectIDSeed(I)V

    .line 1082
    new-instance v0, Lorg/apache/poi/hslf/record/ExHyperlink;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/ExHyperlink;-><init>()V

    .line 1083
    .local v0, "ctrl":Lorg/apache/poi/hslf/record/ExHyperlink;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExHyperlink;->getExHyperlinkAtom()Lorg/apache/poi/hslf/record/ExHyperlinkAtom;

    move-result-object v2

    .line 1084
    .local v2, "obj":Lorg/apache/poi/hslf/record/ExHyperlinkAtom;
    invoke-virtual {v2, v4}, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->setNumber(I)V

    .line 1085
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getType()I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_1

    .line 1086
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getAddress()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x30

    invoke-virtual {v0, v5, v6}, Lorg/apache/poi/hslf/record/ExHyperlink;->setLinkURL(Ljava/lang/String;I)V

    .line 1090
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/record/ExHyperlink;->setLinkTitle(Ljava/lang/String;)V

    .line 1091
    invoke-virtual {v1, v0, v3}, Lorg/apache/poi/hslf/record/ExObjList;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 1092
    invoke-virtual {p1, v4}, Lorg/apache/poi/hslf/model/Hyperlink;->setId(I)V

    .line 1094
    return v4

    .line 1088
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Hyperlink;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/record/ExHyperlink;->setLinkURL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addMovie(Ljava/lang/String;I)I
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 1001
    iget-object v5, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->ExObjList:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/hslf/record/Document;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/record/ExObjList;

    .line 1002
    .local v1, "lst":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v1, :cond_0

    .line 1003
    new-instance v1, Lorg/apache/poi/hslf/record/ExObjList;

    .end local v1    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    invoke-direct {v1}, Lorg/apache/poi/hslf/record/ExObjList;-><init>()V

    .line 1004
    .restart local v1    # "lst":Lorg/apache/poi/hslf/record/ExObjList;
    iget-object v5, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Document;->getDocumentAtom()Lorg/apache/poi/hslf/record/DocumentAtom;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lorg/apache/poi/hslf/record/Document;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 1007
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/ExObjList;->getExObjListAtom()Lorg/apache/poi/hslf/record/ExObjListAtom;

    move-result-object v3

    .line 1009
    .local v3, "objAtom":Lorg/apache/poi/hslf/record/ExObjListAtom;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/ExObjListAtom;->getObjectIDSeed()J

    move-result-wide v6

    long-to-int v5, v6

    add-int/lit8 v4, v5, 0x1

    .line 1010
    .local v4, "objectId":I
    invoke-virtual {v3, v4}, Lorg/apache/poi/hslf/record/ExObjListAtom;->setObjectIDSeed(I)V

    .line 1012
    packed-switch p2, :pswitch_data_0

    .line 1020
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Unsupported Movie: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1014
    :pswitch_0
    new-instance v2, Lorg/apache/poi/hslf/record/ExMCIMovie;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/ExMCIMovie;-><init>()V

    .line 1023
    .local v2, "mci":Lorg/apache/poi/hslf/record/ExMCIMovie;
    :goto_0
    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/record/ExObjList;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 1024
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/ExMCIMovie;->getExVideo()Lorg/apache/poi/hslf/record/ExVideoContainer;

    move-result-object v0

    .line 1025
    .local v0, "exVideo":Lorg/apache/poi/hslf/record/ExVideoContainer;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getExMediaAtom()Lorg/apache/poi/hslf/record/ExMediaAtom;

    move-result-object v5

    invoke-virtual {v5, v4}, Lorg/apache/poi/hslf/record/ExMediaAtom;->setObjectId(I)V

    .line 1026
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getExMediaAtom()Lorg/apache/poi/hslf/record/ExMediaAtom;

    move-result-object v5

    const/high16 v6, 0xe80000

    invoke-virtual {v5, v6}, Lorg/apache/poi/hslf/record/ExMediaAtom;->setMask(I)V

    .line 1027
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getPathAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v5

    invoke-virtual {v5, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 1028
    return v4

    .line 1017
    .end local v0    # "exVideo":Lorg/apache/poi/hslf/record/ExVideoContainer;
    .end local v2    # "mci":Lorg/apache/poi/hslf/record/ExMCIMovie;
    :pswitch_1
    new-instance v2, Lorg/apache/poi/hslf/record/ExAviMovie;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/ExAviMovie;-><init>()V

    .line 1018
    .restart local v2    # "mci":Lorg/apache/poi/hslf/record/ExMCIMovie;
    goto :goto_0

    .line 1012
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public addPicture(Ljava/io/File;I)I
    .locals 6
    .param p1, "pict"    # Ljava/io/File;
    .param p2, "format"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 874
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v3, v4

    .line 875
    .local v3, "length":I
    new-array v0, v3, [B

    .line 876
    .local v0, "data":[B
    const/4 v1, 0x0

    .line 878
    .local v1, "is":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 879
    .end local v1    # "is":Ljava/io/FileInputStream;
    .local v2, "is":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 881
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 883
    :cond_0
    invoke-virtual {p0, v0, p2}, Lorg/apache/poi/hslf/usermodel/SlideShow;->addPicture([BI)I

    move-result v4

    return v4

    .line 880
    .end local v2    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "is":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v4

    .line 881
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 882
    :cond_1
    throw v4

    .line 880
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v2    # "is":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "is":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public addPicture([BI)I
    .locals 10
    .param p1, "data"    # [B
    .param p2, "format"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 809
    invoke-static {p1}, Lorg/apache/poi/hslf/usermodel/PictureData;->getChecksum([B)[B

    move-result-object v8

    .line 813
    .local v8, "uid":[B
    iget-object v9, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/Document;->getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/PPDrawingGroup;->getDggContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    .line 815
    .local v3, "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v9, -0xfff

    .line 814
    invoke-static {v3, v9}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 816
    .local v1, "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v1, :cond_2

    .line 817
    new-instance v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .end local v1    # "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 818
    .restart local v1    # "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v9, -0xfff

    invoke-virtual {v1, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 820
    const/16 v9, -0xff5

    invoke-virtual {v3, v1, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildBefore(Lorg/apache/poi/ddf/EscherRecord;I)V

    .line 831
    :cond_0
    invoke-static {p2}, Lorg/apache/poi/hslf/usermodel/PictureData;->create(I)Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v7

    .line 832
    .local v7, "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    invoke-virtual {v7, p1}, Lorg/apache/poi/hslf/usermodel/PictureData;->setData([B)V

    .line 834
    iget-object v9, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {v9, v7}, Lorg/apache/poi/hslf/HSLFSlideShow;->addPicture(Lorg/apache/poi/hslf/usermodel/PictureData;)I

    move-result v6

    .line 836
    .local v6, "offset":I
    new-instance v0, Lorg/apache/poi/ddf/EscherBSERecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherBSERecord;-><init>()V

    .line 837
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    const/16 v9, -0xff9

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setRecordId(S)V

    .line 838
    shl-int/lit8 v9, p2, 0x4

    or-int/lit8 v9, v9, 0x2

    int-to-short v9, v9

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setOptions(S)V

    .line 839
    invoke-virtual {v7}, Lorg/apache/poi/hslf/usermodel/PictureData;->getRawData()[B

    move-result-object v9

    array-length v9, v9

    add-int/lit8 v9, v9, 0x8

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setSize(I)V

    .line 840
    invoke-virtual {v0, v8}, Lorg/apache/poi/ddf/EscherBSERecord;->setUid([B)V

    .line 842
    int-to-byte v9, p2

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeMacOS(B)V

    .line 843
    int-to-byte v9, p2

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeWin32(B)V

    .line 845
    const/4 v9, 0x2

    if-ne p2, v9, :cond_4

    .line 846
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeMacOS(B)V

    .line 852
    :cond_1
    :goto_0
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 853
    invoke-virtual {v0, v6}, Lorg/apache/poi/ddf/EscherBSERecord;->setOffset(I)V

    .line 854
    const/4 v9, 0x0

    new-array v9, v9, [B

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setRemainingData([B)V

    .line 856
    invoke-virtual {v1, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 857
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    .line 858
    .local v2, "count":I
    shl-int/lit8 v9, v2, 0x4

    or-int/lit8 v9, v9, 0xf

    int-to-short v9, v9

    invoke-virtual {v1, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 860
    .end local v2    # "count":I
    .end local v6    # "offset":I
    .end local v7    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :goto_1
    return v2

    .line 822
    .end local v0    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    :cond_2
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v5

    .line 823
    .local v5, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 824
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 825
    .restart local v0    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getUid()[B

    move-result-object v9

    invoke-static {v9, v8}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 826
    add-int/lit8 v2, v4, 0x1

    goto :goto_1

    .line 823
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 847
    .end local v4    # "i":I
    .end local v5    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    .restart local v6    # "offset":I
    .restart local v7    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :cond_4
    const/4 v9, 0x3

    if-ne p2, v9, :cond_5

    .line 848
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeMacOS(B)V

    goto :goto_0

    .line 849
    :cond_5
    const/4 v9, 0x4

    if-ne p2, v9, :cond_1

    .line 850
    const/4 v9, 0x3

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeWin32(B)V

    goto :goto_0
.end method

.method public createSlide()Lorg/apache/poi/hslf/model/Slide;
    .locals 28

    .prologue
    .line 682
    const/16 v19, 0x0

    .line 687
    .local v19, "slist":Lorg/apache/poi/hslf/record/SlideListWithText;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/record/Document;->getSlideSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;

    move-result-object v19

    .line 688
    if-nez v19, :cond_0

    .line 690
    new-instance v19, Lorg/apache/poi/hslf/record/SlideListWithText;

    .end local v19    # "slist":Lorg/apache/poi/hslf/record/SlideListWithText;
    invoke-direct/range {v19 .. v19}, Lorg/apache/poi/hslf/record/SlideListWithText;-><init>()V

    .line 691
    .restart local v19    # "slist":Lorg/apache/poi/hslf/record/SlideListWithText;
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/SlideListWithText;->setInstance(I)V

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/Document;->addSlideListWithText(Lorg/apache/poi/hslf/record/SlideListWithText;)V

    .line 698
    :cond_0
    const/4 v9, 0x0

    .line 699
    .local v9, "prev":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hslf/record/SlideListWithText;->getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-result-object v14

    .line 700
    .local v14, "sas":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    array-length v0, v14

    move/from16 v23, v0

    move/from16 v0, v23

    if-lt v6, v0, :cond_3

    .line 717
    new-instance v20, Lorg/apache/poi/hslf/record/SlidePersistAtom;

    invoke-direct/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlidePersistAtom;-><init>()V

    .line 720
    .local v20, "sp":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    if-nez v9, :cond_6

    const/16 v23, 0x100

    :goto_1
    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->setSlideIdentifier(I)V

    .line 723
    invoke-virtual/range {v19 .. v20}, Lorg/apache/poi/hslf/record/SlideListWithText;->addSlidePersistAtom(Lorg/apache/poi/hslf/record/SlidePersistAtom;)V

    .line 726
    new-instance v15, Lorg/apache/poi/hslf/model/Slide;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v23

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getRefID()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v15, v0, v1, v2}, Lorg/apache/poi/hslf/model/Slide;-><init>(III)V

    .line 727
    .local v15, "slide":Lorg/apache/poi/hslf/model/Slide;
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lorg/apache/poi/hslf/model/Slide;->setSlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 728
    invoke-virtual {v15}, Lorg/apache/poi/hslf/model/Slide;->onCreate()V

    .line 731
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    new-array v13, v0, [Lorg/apache/poi/hslf/model/Slide;

    .line 732
    .local v13, "s":[Lorg/apache/poi/hslf/model/Slide;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v13, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 733
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    aput-object v15, v13, v23

    .line 734
    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    .line 735
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    new-instance v25, Ljava/lang/StringBuilder;

    const-string/jumbo v26, "Added slide "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " with ref "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getRefID()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 736
    const-string/jumbo v26, " and identifier "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 735
    invoke-virtual/range {v23 .. v25}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 739
    invoke-virtual {v15}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v17

    .line 740
    .local v17, "slideRecord":Lorg/apache/poi/hslf/record/Slide;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/HSLFSlideShow;->appendRootLevelRecord(Lorg/apache/poi/hslf/record/Record;)I

    move-result v18

    .line 741
    .local v18, "slideRecordPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    .line 744
    const/4 v7, 0x0

    .line 745
    .local v7, "offset":I
    const/16 v16, 0x0

    .line 746
    .local v16, "slideOffset":I
    const/4 v11, 0x0

    .line 747
    .local v11, "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    const/16 v22, 0x0

    .line 748
    .local v22, "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-lt v5, v0, :cond_7

    .line 772
    if-eqz v22, :cond_1

    .line 774
    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hslf/record/UserEditAtom;->getMaxPersistWritten()I

    move-result v23

    add-int/lit8 v10, v23, 0x1

    .line 775
    .local v10, "psrId":I
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->setRefID(I)V

    .line 776
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lorg/apache/poi/hslf/record/Slide;->setSheetId(I)V

    .line 779
    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Lorg/apache/poi/hslf/record/UserEditAtom;->setLastViewType(S)V

    .line 781
    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/apache/poi/hslf/record/UserEditAtom;->setMaxPersistWritten(I)V

    .line 786
    .end local v10    # "psrId":I
    :cond_1
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/Slide;->setLastOnDiskOffset(I)V

    .line 788
    if-eqz v11, :cond_2

    .line 789
    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getRefID()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v11, v0, v1}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->addSlideLookup(II)V

    .line 791
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->logger:Lorg/apache/poi/util/POILogger;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    new-instance v25, Ljava/lang/StringBuilder;

    const-string/jumbo v26, "New slide ended up at "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_masters:[Lorg/apache/poi/hslf/model/SlideMaster;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v23, v23, v24

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Lorg/apache/poi/hslf/model/Slide;->setMasterSheet(Lorg/apache/poi/hslf/model/MasterSheet;)V

    .line 795
    return-object v15

    .line 701
    .end local v5    # "i":I
    .end local v7    # "offset":I
    .end local v11    # "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v13    # "s":[Lorg/apache/poi/hslf/model/Slide;
    .end local v15    # "slide":Lorg/apache/poi/hslf/model/Slide;
    .end local v16    # "slideOffset":I
    .end local v17    # "slideRecord":Lorg/apache/poi/hslf/record/Slide;
    .end local v18    # "slideRecordPos":I
    .end local v20    # "sp":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    .end local v22    # "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    :cond_3
    aget-object v23, v14, v6

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlidePersistAtom()Lorg/apache/poi/hslf/record/SlidePersistAtom;

    move-result-object v21

    .line 702
    .local v21, "spa":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v23

    if-ltz v23, :cond_5

    .line 707
    if-nez v9, :cond_4

    .line 708
    move-object/from16 v9, v21

    .line 710
    :cond_4
    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v23

    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    .line 711
    move-object/from16 v9, v21

    .line 700
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 720
    .end local v21    # "spa":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    .restart local v20    # "sp":Lorg/apache/poi/hslf/record/SlidePersistAtom;
    :cond_6
    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/SlidePersistAtom;->getSlideIdentifier()I

    move-result v23

    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_1

    .line 749
    .restart local v5    # "i":I
    .restart local v7    # "offset":I
    .restart local v11    # "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .restart local v13    # "s":[Lorg/apache/poi/hslf/model/Slide;
    .restart local v15    # "slide":Lorg/apache/poi/hslf/model/Slide;
    .restart local v16    # "slideOffset":I
    .restart local v17    # "slideRecord":Lorg/apache/poi/hslf/record/Slide;
    .restart local v18    # "slideRecordPos":I
    .restart local v22    # "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v23, v0

    aget-object v12, v23, v5

    .line 750
    .local v12, "record":Lorg/apache/poi/hslf/record/Record;
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 752
    .local v8, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v12, v8}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 758
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v23, v0

    aget-object v23, v23, v5

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v24

    sget-object v23, Lorg/apache/poi/hslf/record/RecordTypes;->PersistPtrIncrementalBlock:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v26, v0

    cmp-long v23, v24, v26

    if-nez v23, :cond_8

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v23, v0

    aget-object v11, v23, v5

    .end local v11    # "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    check-cast v11, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    .line 761
    .restart local v11    # "ptr":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v23, v0

    aget-object v23, v23, v5

    invoke-virtual/range {v23 .. v23}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v24

    sget-object v23, Lorg/apache/poi/hslf/record/RecordTypes;->UserEditAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v26, v0

    cmp-long v23, v24, v26

    if-nez v23, :cond_9

    .line 762
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_records:[Lorg/apache/poi/hslf/record/Record;

    move-object/from16 v23, v0

    aget-object v22, v23, v5

    .end local v22    # "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    check-cast v22, Lorg/apache/poi/hslf/record/UserEditAtom;

    .line 765
    .restart local v22    # "usr":Lorg/apache/poi/hslf/record/UserEditAtom;
    :cond_9
    move/from16 v0, v18

    if-ne v5, v0, :cond_a

    .line 766
    move/from16 v16, v7

    .line 768
    :cond_a
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v23

    add-int v7, v7, v23

    .line 748
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 753
    :catch_0
    move-exception v4

    .line 754
    .local v4, "e":Ljava/io/IOException;
    new-instance v23, Lorg/apache/poi/hslf/exceptions/HSLFException;

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v23
.end method

.method public getDocumentRecord()Lorg/apache/poi/hslf/record/Document;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    return-object v0
.end method

.method public getEmbeddedObjects()[Lorg/apache/poi/hslf/usermodel/ObjectData;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getEmbeddedObjects()[Lorg/apache/poi/hslf/usermodel/ObjectData;

    move-result-object v0

    return-object v0
.end method

.method public getFont(I)Lorg/apache/poi/hslf/model/PPFont;
    .locals 6
    .param p1, "idx"    # I

    .prologue
    .line 912
    const/4 v2, 0x0

    .line 913
    .local v2, "font":Lorg/apache/poi/hslf/model/PPFont;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Document;->getEnvironment()Lorg/apache/poi/hslf/record/Environment;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Environment;->getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;

    move-result-object v3

    .line 914
    .local v3, "fonts":Lorg/apache/poi/hslf/record/FontCollection;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/FontCollection;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 915
    .local v1, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v1

    if-lt v4, v5, :cond_0

    .line 924
    :goto_1
    return-object v2

    .line 916
    :cond_0
    aget-object v5, v1, v4

    instance-of v5, v5, Lorg/apache/poi/hslf/record/FontEntityAtom;

    if-eqz v5, :cond_1

    .line 917
    aget-object v0, v1, v4

    check-cast v0, Lorg/apache/poi/hslf/record/FontEntityAtom;

    .line 918
    .local v0, "atom":Lorg/apache/poi/hslf/record/FontEntityAtom;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getFontIndex()I

    move-result v5

    if-ne v5, p1, :cond_1

    .line 919
    new-instance v2, Lorg/apache/poi/hslf/model/PPFont;

    .end local v2    # "font":Lorg/apache/poi/hslf/model/PPFont;
    invoke-direct {v2, v0}, Lorg/apache/poi/hslf/model/PPFont;-><init>(Lorg/apache/poi/hslf/record/FontEntityAtom;)V

    .line 920
    .restart local v2    # "font":Lorg/apache/poi/hslf/model/PPFont;
    goto :goto_1

    .line 915
    .end local v0    # "atom":Lorg/apache/poi/hslf/record/FontEntityAtom;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_fonts:Lorg/apache/poi/hslf/record/FontCollection;

    return-object v0
.end method

.method public getMostRecentCoreRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_mostRecentCoreRecords:[Lorg/apache/poi/hslf/record/Record;

    return-object v0
.end method

.method public getNotes()[Lorg/apache/poi/hslf/model/Notes;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    return-object v0
.end method

.method public getNotesHeadersFooters()Lorg/apache/poi/hslf/model/HeadersFooters;
    .locals 9

    .prologue
    const/16 v7, 0x4f

    const/4 v8, 0x0

    .line 970
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;

    move-result-object v6

    aget-object v6, v6, v8

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/SlideMaster;->getProgrammableTag()Ljava/lang/String;

    move-result-object v5

    .line 971
    .local v5, "tag":Ljava/lang/String;
    const-string/jumbo v6, "___PPT12"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 973
    .local v4, "ppt2007":Z
    const/4 v1, 0x0

    .line 974
    .local v1, "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Document;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 975
    .local v0, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0

    if-lt v2, v6, :cond_1

    .line 982
    :goto_1
    const/4 v3, 0x0

    .line 983
    .local v3, "newRecord":Z
    if-nez v1, :cond_0

    .line 984
    new-instance v1, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .end local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    invoke-direct {v1, v7}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;-><init>(S)V

    .line 985
    .restart local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    const/4 v3, 0x1

    .line 987
    :cond_0
    if-eqz v4, :cond_3

    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    array-length v6, v6

    if-lez v6, :cond_3

    .line 988
    new-instance v6, Lorg/apache/poi/hslf/model/HeadersFooters;

    iget-object v7, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_notes:[Lorg/apache/poi/hslf/model/Notes;

    aget-object v7, v7, v8

    invoke-direct {v6, v1, v7, v3, v4}, Lorg/apache/poi/hslf/model/HeadersFooters;-><init>(Lorg/apache/poi/hslf/record/HeadersFootersContainer;Lorg/apache/poi/hslf/model/Sheet;ZZ)V

    .line 990
    :goto_2
    return-object v6

    .line 976
    .end local v3    # "newRecord":Z
    :cond_1
    aget-object v6, v0, v2

    instance-of v6, v6, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    if-eqz v6, :cond_2

    .line 977
    aget-object v6, v0, v2

    check-cast v6, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getOptions()I

    move-result v6

    if-ne v6, v7, :cond_2

    .line 978
    aget-object v1, v0, v2

    .end local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    check-cast v1, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .line 979
    .restart local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    goto :goto_1

    .line 975
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 990
    .restart local v3    # "newRecord":Z
    :cond_3
    new-instance v6, Lorg/apache/poi/hslf/model/HeadersFooters;

    invoke-direct {v6, v1, p0, v3, v4}, Lorg/apache/poi/hslf/model/HeadersFooters;-><init>(Lorg/apache/poi/hslf/record/HeadersFootersContainer;Lorg/apache/poi/hslf/usermodel/SlideShow;ZZ)V

    goto :goto_2
.end method

.method public getNumberOfFonts()I
    .locals 1

    .prologue
    .line 933
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Document;->getEnvironment()Lorg/apache/poi/hslf/record/Environment;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Environment;->getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/FontCollection;->getNumberOfFonts()I

    move-result v0

    return v0
.end method

.method public getPageSize()Lorg/apache/poi/java/awt/Dimension;
    .locals 6

    .prologue
    .line 508
    iget-object v3, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Document;->getDocumentAtom()Lorg/apache/poi/hslf/record/DocumentAtom;

    move-result-object v0

    .line 509
    .local v0, "docatom":Lorg/apache/poi/hslf/record/DocumentAtom;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/DocumentAtom;->getSlideSizeX()J

    move-result-wide v4

    long-to-int v3, v4

    mul-int/lit8 v3, v3, 0x48

    div-int/lit16 v1, v3, 0x240

    .line 510
    .local v1, "pgx":I
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/DocumentAtom;->getSlideSizeY()J

    move-result-wide v4

    long-to-int v3, v4

    mul-int/lit8 v3, v3, 0x48

    div-int/lit16 v2, v3, 0x240

    .line 511
    .local v2, "pgy":I
    new-instance v3, Lorg/apache/poi/java/awt/Dimension;

    invoke-direct {v3, v1, v2}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    return-object v3
.end method

.method public getPictureData()[Lorg/apache/poi/hslf/usermodel/PictureData;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getPictures()[Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v0

    return-object v0
.end method

.method public getSlideHeadersFooters()Lorg/apache/poi/hslf/model/HeadersFooters;
    .locals 9

    .prologue
    const/16 v8, 0x3f

    .line 943
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/SlideMaster;->getProgrammableTag()Ljava/lang/String;

    move-result-object v5

    .line 944
    .local v5, "tag":Ljava/lang/String;
    const-string/jumbo v6, "___PPT12"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 946
    .local v4, "ppt2007":Z
    const/4 v1, 0x0

    .line 947
    .local v1, "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Document;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 948
    .local v0, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0

    if-lt v2, v6, :cond_1

    .line 955
    :goto_1
    const/4 v3, 0x0

    .line 956
    .local v3, "newRecord":Z
    if-nez v1, :cond_0

    .line 957
    new-instance v1, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .end local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    invoke-direct {v1, v8}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;-><init>(S)V

    .line 958
    .restart local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    const/4 v3, 0x1

    .line 960
    :cond_0
    new-instance v6, Lorg/apache/poi/hslf/model/HeadersFooters;

    invoke-direct {v6, v1, p0, v3, v4}, Lorg/apache/poi/hslf/model/HeadersFooters;-><init>(Lorg/apache/poi/hslf/record/HeadersFootersContainer;Lorg/apache/poi/hslf/usermodel/SlideShow;ZZ)V

    return-object v6

    .line 949
    .end local v3    # "newRecord":Z
    :cond_1
    aget-object v6, v0, v2

    instance-of v6, v6, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    if-eqz v6, :cond_2

    .line 950
    aget-object v6, v0, v2

    check-cast v6, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getOptions()I

    move-result v6

    if-ne v6, v8, :cond_2

    .line 951
    aget-object v1, v0, v2

    .end local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    check-cast v1, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .line 952
    .restart local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    goto :goto_1

    .line 948
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getSlides()[Lorg/apache/poi/hslf/model/Slide;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_slides:[Lorg/apache/poi/hslf/model/Slide;

    return-object v0
.end method

.method public getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_masters:[Lorg/apache/poi/hslf/model/SlideMaster;

    return-object v0
.end method

.method public getSoundData()[Lorg/apache/poi/hslf/usermodel/SoundData;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-static {v0}, Lorg/apache/poi/hslf/usermodel/SoundData;->find(Lorg/apache/poi/hslf/record/Document;)[Lorg/apache/poi/hslf/usermodel/SoundData;

    move-result-object v0

    return-object v0
.end method

.method public getTitleMasters()[Lorg/apache/poi/hslf/model/TitleMaster;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_titleMasters:[Lorg/apache/poi/hslf/model/TitleMaster;

    return-object v0
.end method

.method public setPageSize(Lorg/apache/poi/java/awt/Dimension;)V
    .locals 4
    .param p1, "pgsize"    # Lorg/apache/poi/java/awt/Dimension;

    .prologue
    .line 521
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_documentRecord:Lorg/apache/poi/hslf/record/Document;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Document;->getDocumentAtom()Lorg/apache/poi/hslf/record/DocumentAtom;

    move-result-object v0

    .line 522
    .local v0, "docatom":Lorg/apache/poi/hslf/record/DocumentAtom;
    iget v1, p1, Lorg/apache/poi/java/awt/Dimension;->width:I

    mul-int/lit16 v1, v1, 0x240

    div-int/lit8 v1, v1, 0x48

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hslf/record/DocumentAtom;->setSlideSizeX(J)V

    .line 523
    iget v1, p1, Lorg/apache/poi/java/awt/Dimension;->height:I

    mul-int/lit16 v1, v1, 0x240

    div-int/lit8 v1, v1, 0x48

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hslf/record/DocumentAtom;->setSlideSizeY(J)V

    .line 524
    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/SlideShow;->_hslfSlideShow:Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/HSLFSlideShow;->write(Ljava/io/OutputStream;)V

    .line 439
    return-void
.end method
