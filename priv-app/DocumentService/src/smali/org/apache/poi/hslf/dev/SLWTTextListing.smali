.class public final Lorg/apache/poi/hslf/dev/SLWTTextListing;
.super Ljava/lang/Object;
.source "SLWTTextListing.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 17
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    move-object/from16 v0, p0

    array-length v15, v0

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 31
    sget-object v15, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v16, "Need to give a filename"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 32
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/System;->exit(I)V

    .line 35
    :cond_0
    new-instance v10, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/4 v15, 0x0

    aget-object v15, p0, v15

    invoke-direct {v10, v15}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 38
    .local v10, "ss":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v10}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    .line 39
    .local v7, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v15, v7

    if-lt v3, v15, :cond_1

    .line 85
    return-void

    .line 40
    :cond_1
    aget-object v15, v7, v3

    instance-of v15, v15, Lorg/apache/poi/hslf/record/Document;

    if-eqz v15, :cond_2

    .line 41
    aget-object v2, v7, v3

    .line 42
    .local v2, "docRecord":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 43
    .local v1, "docChildren":[Lorg/apache/poi/hslf/record/Record;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    array-length v15, v1

    if-lt v4, v15, :cond_3

    .line 39
    .end local v1    # "docChildren":[Lorg/apache/poi/hslf/record/Record;
    .end local v2    # "docRecord":Lorg/apache/poi/hslf/record/Record;
    .end local v4    # "j":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 44
    .restart local v1    # "docChildren":[Lorg/apache/poi/hslf/record/Record;
    .restart local v2    # "docRecord":Lorg/apache/poi/hslf/record/Record;
    .restart local v4    # "j":I
    :cond_3
    aget-object v15, v1, v4

    instance-of v15, v15, Lorg/apache/poi/hslf/record/SlideListWithText;

    if-eqz v15, :cond_4

    .line 51
    aget-object v8, v1, v4

    check-cast v8, Lorg/apache/poi/hslf/record/SlideListWithText;

    .line 52
    .local v8, "slwt":Lorg/apache/poi/hslf/record/SlideListWithText;
    invoke-virtual {v8}, Lorg/apache/poi/hslf/record/SlideListWithText;->getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-result-object v14

    .line 56
    .local v14, "thisSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_2
    array-length v15, v14

    if-lt v5, v15, :cond_5

    .line 43
    .end local v5    # "k":I
    .end local v8    # "slwt":Lorg/apache/poi/hslf/record/SlideListWithText;
    .end local v14    # "thisSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 63
    .restart local v5    # "k":I
    .restart local v8    # "slwt":Lorg/apache/poi/hslf/record/SlideListWithText;
    .restart local v14    # "thisSets":[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    :cond_5
    aget-object v15, v14, v5

    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlideRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v9

    .line 64
    .local v9, "slwtc":[Lorg/apache/poi/hslf/record/Record;
    const/4 v6, 0x0

    .local v6, "l":I
    :goto_3
    array-length v15, v9

    if-lt v6, v15, :cond_6

    .line 56
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 65
    :cond_6
    const/4 v13, 0x0

    .line 66
    .local v13, "text":Ljava/lang/String;
    aget-object v15, v9, v6

    instance-of v15, v15, Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v15, :cond_7

    .line 67
    aget-object v11, v9, v6

    check-cast v11, Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 68
    .local v11, "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/TextBytesAtom;->getText()Ljava/lang/String;

    move-result-object v13

    .line 70
    .end local v11    # "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    :cond_7
    aget-object v15, v9, v6

    instance-of v15, v15, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v15, :cond_8

    .line 71
    aget-object v12, v9, v6

    check-cast v12, Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 72
    .local v12, "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    invoke-virtual {v12}, Lorg/apache/poi/hslf/record/TextCharsAtom;->getText()Ljava/lang/String;

    move-result-object v13

    .line 75
    .end local v12    # "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    :cond_8
    if-eqz v13, :cond_9

    .line 76
    const/16 v15, 0xd

    const/16 v16, 0xa

    move/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 64
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_3
.end method
