.class public final Lorg/apache/poi/hslf/record/OEShapeAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "OEShapeAtom.java"


# instance fields
.field private _header:[B

.field private _recdata:[B


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 46
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 47
    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    .line 49
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_header:[B

    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/OEShapeAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 51
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_header:[B

    iget-object v1, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    array-length v1, v1

    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 52
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 62
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 64
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_header:[B

    .line 65
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    .line 69
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    return-void
.end method


# virtual methods
.method public getOptions()I
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->OEShapeAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setOptions(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 106
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 87
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OEShapeAtom;->_recdata:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 88
    return-void
.end method
