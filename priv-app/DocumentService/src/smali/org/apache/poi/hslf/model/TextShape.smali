.class public abstract Lorg/apache/poi/hslf/model/TextShape;
.super Lorg/apache/poi/hslf/model/SimpleShape;
.source "TextShape.java"


# static fields
.field public static final AlignCenter:I = 0x1

.field public static final AlignJustify:I = 0x3

.field public static final AlignLeft:I = 0x0

.field public static final AlignRight:I = 0x2

.field public static final AnchorBottom:I = 0x2

.field public static final AnchorBottomBaseline:I = 0x7

.field public static final AnchorBottomCentered:I = 0x5

.field public static final AnchorBottomCenteredBaseline:I = 0x9

.field public static final AnchorMiddle:I = 0x1

.field public static final AnchorMiddleCentered:I = 0x4

.field public static final AnchorTop:I = 0x0

.field public static final AnchorTopBaseline:I = 0x6

.field public static final AnchorTopCentered:I = 0x3

.field public static final AnchorTopCenteredBaseline:I = 0x8

.field public static final WrapByPoints:I = 0x1

.field public static final WrapNone:I = 0x2

.field public static final WrapSquare:I = 0x0

.field public static final WrapThrough:I = 0x4

.field public static final WrapTopBottom:I = 0x3


# instance fields
.field protected _txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

.field protected _txtrun:Lorg/apache/poi/hslf/model/TextRun;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/TextShape;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 119
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/SimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/model/SimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 110
    instance-of v0, p1, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/TextShape;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 111
    return-void
.end method


# virtual methods
.method public createTextRun()Lorg/apache/poi/hslf/model/TextRun;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 122
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getEscherTextboxWrapper()Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 123
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-direct {v3}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;-><init>()V

    iput-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 125
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    .line 126
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    if-nez v3, :cond_1

    .line 127
    new-instance v2, Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/TextHeaderAtom;-><init>()V

    .line 128
    .local v2, "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->setParentRecord(Lorg/apache/poi/hslf/record/RecordContainer;)V

    .line 129
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v3, v2}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 131
    new-instance v1, Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/TextCharsAtom;-><init>()V

    .line 132
    .local v1, "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v3, v1}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 134
    new-instance v0, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-direct {v0, v5}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;-><init>(I)V

    .line 135
    .local v0, "sta":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v3, v0}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 137
    new-instance v3, Lorg/apache/poi/hslf/model/TextRun;

    invoke-direct {v3, v2, v1, v0}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    iput-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    .line 138
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    const/4 v4, 0x3

    new-array v4, v4, [Lorg/apache/poi/hslf/record/Record;

    aput-object v2, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    aput-object v0, v4, v5

    iput-object v4, v3, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    .line 139
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Lorg/apache/poi/hslf/model/TextRun;->setText(Ljava/lang/String;)V

    .line 141
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getEscherRecord()Lorg/apache/poi/ddf/EscherTextboxRecord;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 143
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/model/TextShape;->setDefaultTextProperties(Lorg/apache/poi/hslf/model/TextRun;)V

    .line 146
    .end local v0    # "sta":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .end local v1    # "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    .end local v2    # "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    return-object v3
.end method

.method protected getEscherTextboxWrapper()Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    .locals 3

    .prologue
    .line 187
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    if-nez v1, :cond_0

    .line 188
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff3

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 189
    .local v0, "textRecord":Lorg/apache/poi/ddf/EscherTextboxRecord;
    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-direct {v1, v0}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;-><init>(Lorg/apache/poi/ddf/EscherTextboxRecord;)V

    iput-object v1, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 191
    .end local v0    # "textRecord":Lorg/apache/poi/ddf/EscherTextboxRecord;
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    return-object v1
.end method

.method public getHorizontalAlignment()I
    .locals 3

    .prologue
    .line 261
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 262
    .local v0, "tx":Lorg/apache/poi/hslf/model/TextRun;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/TextRun;->getRichTextRuns()[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getAlignment()I

    move-result v1

    goto :goto_0
.end method

.method public getMarginBottom()F
    .locals 5

    .prologue
    .line 273
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff5

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 274
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v3, 0x84

    invoke-static {v0, v3}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 275
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const v2, 0xb298

    .line 276
    .local v2, "val":I
    :goto_0
    int-to-float v3, v2

    const v4, 0x46467000    # 12700.0f

    div-float/2addr v3, v4

    return v3

    .line 275
    .end local v2    # "val":I
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getMarginLeft()F
    .locals 5

    .prologue
    .line 298
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff5

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 299
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v3, 0x81

    invoke-static {v0, v3}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 300
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const v2, 0x16530

    .line 301
    .local v2, "val":I
    :goto_0
    int-to-float v3, v2

    const v4, 0x46467000    # 12700.0f

    div-float/2addr v3, v4

    return v3

    .line 300
    .end local v2    # "val":I
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getMarginRight()F
    .locals 5

    .prologue
    .line 323
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff5

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 324
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v3, 0x83

    invoke-static {v0, v3}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 325
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const v2, 0x16530

    .line 326
    .local v2, "val":I
    :goto_0
    int-to-float v3, v2

    const v4, 0x46467000    # 12700.0f

    div-float/2addr v3, v4

    return v3

    .line 325
    .end local v2    # "val":I
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getMarginTop()F
    .locals 5

    .prologue
    .line 347
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff5

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 348
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v3, 0x82

    invoke-static {v0, v3}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 349
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const v2, 0xb298

    .line 350
    .local v2, "val":I
    :goto_0
    int-to-float v3, v2

    const v4, 0x46467000    # 12700.0f

    div-float/2addr v3, v4

    return v3

    .line 349
    .end local v2    # "val":I
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getPlaceholderAtom()Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
    .locals 1

    .prologue
    .line 509
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->OEPlaceholderAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/TextShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 167
    .local v0, "tx":Lorg/apache/poi/hslf/model/TextRun;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/TextRun;->getText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getTextId()I
    .locals 4

    .prologue
    .line 390
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 391
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x80

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 392
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getTextRun()Lorg/apache/poi/hslf/model/TextRun;
    .locals 9

    .prologue
    .line 408
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->initTextRun()V

    .line 409
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    if-nez v6, :cond_1

    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    if-eqz v6, :cond_1

    .line 410
    const/4 v5, 0x0

    .line 411
    .local v5, "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    const/4 v3, 0x0

    .line 412
    .local v3, "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    const/4 v4, 0x0

    .line 413
    .local v4, "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    const/4 v2, 0x0

    .line 414
    .local v2, "sta":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 415
    .local v0, "childRecords":[Lorg/apache/poi/hslf/record/Record;
    array-length v7, v0

    const/4 v6, 0x0

    :goto_0
    if-lt v6, v7, :cond_2

    .line 426
    if-eqz v3, :cond_7

    .line 427
    new-instance v6, Lorg/apache/poi/hslf/model/TextRun;

    invoke-direct {v6, v5, v3, v2}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextBytesAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    iput-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    .line 432
    .end local v0    # "childRecords":[Lorg/apache/poi/hslf/record/Record;
    .end local v2    # "sta":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .end local v3    # "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    .end local v4    # "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    .end local v5    # "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    :cond_1
    :goto_1
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    return-object v6

    .line 415
    .restart local v0    # "childRecords":[Lorg/apache/poi/hslf/record/Record;
    .restart local v2    # "sta":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .restart local v3    # "tba":Lorg/apache/poi/hslf/record/TextBytesAtom;
    .restart local v4    # "tca":Lorg/apache/poi/hslf/record/TextCharsAtom;
    .restart local v5    # "tha":Lorg/apache/poi/hslf/record/TextHeaderAtom;
    :cond_2
    aget-object v1, v0, v6

    .line 416
    .local v1, "r":Lorg/apache/poi/hslf/record/Record;
    instance-of v8, v1, Lorg/apache/poi/hslf/record/TextHeaderAtom;

    if-eqz v8, :cond_4

    move-object v5, v1

    .line 417
    check-cast v5, Lorg/apache/poi/hslf/record/TextHeaderAtom;

    .line 415
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 418
    :cond_4
    instance-of v8, v1, Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v8, :cond_5

    move-object v3, v1

    .line 419
    check-cast v3, Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 420
    goto :goto_2

    :cond_5
    instance-of v8, v1, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v8, :cond_6

    move-object v4, v1

    .line 421
    check-cast v4, Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 422
    goto :goto_2

    :cond_6
    instance-of v8, v1, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v8, :cond_3

    move-object v2, v1

    .line 423
    check-cast v2, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    goto :goto_2

    .line 428
    .end local v1    # "r":Lorg/apache/poi/hslf/record/Record;
    :cond_7
    if-eqz v4, :cond_1

    .line 429
    new-instance v6, Lorg/apache/poi/hslf/model/TextRun;

    invoke-direct {v6, v5, v4, v2}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    iput-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    goto :goto_1
.end method

.method public getVerticalAlignment()I
    .locals 8

    .prologue
    .line 202
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xff5

    invoke-static {v6, v7}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 203
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v6, 0x87

    invoke-static {v2, v6}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 204
    .local v3, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    const/4 v5, 0x0

    .line 205
    .local v5, "valign":I
    if-nez v3, :cond_2

    .line 210
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TextRun;->getRunType()I

    move-result v4

    .line 211
    .local v4, "type":I
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/Sheet;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    .line 212
    .local v0, "master":Lorg/apache/poi/hslf/model/MasterSheet;
    if-eqz v0, :cond_1

    .line 213
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/MasterSheet;->getPlaceholderByTextType(I)Lorg/apache/poi/hslf/model/TextShape;

    move-result-object v1

    .line 214
    .local v1, "masterShape":Lorg/apache/poi/hslf/model/TextShape;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/TextShape;->getVerticalAlignment()I

    move-result v5

    .line 230
    .end local v0    # "master":Lorg/apache/poi/hslf/model/MasterSheet;
    .end local v1    # "masterShape":Lorg/apache/poi/hslf/model/TextShape;
    .end local v4    # "type":I
    :cond_0
    :goto_0
    return v5

    .line 217
    .restart local v0    # "master":Lorg/apache/poi/hslf/model/MasterSheet;
    .restart local v4    # "type":I
    :cond_1
    sparse-switch v4, :sswitch_data_0

    .line 223
    const/4 v5, 0x0

    .line 227
    goto :goto_0

    .line 220
    :sswitch_0
    const/4 v5, 0x1

    .line 221
    goto :goto_0

    .line 228
    .end local v0    # "master":Lorg/apache/poi/hslf/model/MasterSheet;
    .end local v4    # "type":I
    :cond_2
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v5

    goto :goto_0

    .line 217
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method public getWordWrap()I
    .locals 4

    .prologue
    .line 371
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/TextShape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 372
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x85

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/TextShape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 373
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method protected initTextRun()V
    .locals 20

    .prologue
    .line 455
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/model/TextShape;->getEscherTextboxWrapper()Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v11

    .line 456
    .local v11, "txtbox":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/model/TextShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v10

    .line 458
    .local v10, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    if-eqz v10, :cond_0

    if-nez v11, :cond_1

    .line 500
    :cond_0
    return-void

    .line 460
    :cond_1
    const/4 v6, 0x0

    .line 462
    .local v6, "ota":Lorg/apache/poi/hslf/record/OutlineTextRefAtom;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    .line 463
    .local v2, "child":[Lorg/apache/poi/hslf/record/Record;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v12, v2

    if-lt v4, v12, :cond_3

    .line 470
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/Sheet;->getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v8

    .line 471
    .local v8, "runs":[Lorg/apache/poi/hslf/model/TextRun;
    if-eqz v6, :cond_7

    .line 472
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->getTextIndex()I

    move-result v5

    .line 473
    .local v5, "idx":I
    const/4 v4, 0x0

    :goto_2
    array-length v12, v8

    if-lt v4, v12, :cond_5

    .line 479
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    if-nez v12, :cond_2

    .line 480
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x5

    new-instance v14, Ljava/lang/StringBuilder;

    const-string/jumbo v15, "text run not found for OutlineTextRefAtom.TextIndex="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 493
    .end local v5    # "idx":I
    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    if-eqz v12, :cond_0

    const/4 v4, 0x0

    :goto_5
    array-length v12, v2

    if-ge v4, v12, :cond_0

    .line 494
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/TextRun;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v13

    array-length v14, v13

    const/4 v12, 0x0

    :goto_6
    if-lt v12, v14, :cond_9

    .line 493
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 464
    .end local v8    # "runs":[Lorg/apache/poi/hslf/model/TextRun;
    :cond_3
    aget-object v12, v2, v4

    instance-of v12, v12, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;

    if-eqz v12, :cond_4

    .line 465
    aget-object v6, v2, v4

    .end local v6    # "ota":Lorg/apache/poi/hslf/record/OutlineTextRefAtom;
    check-cast v6, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;

    .line 466
    .restart local v6    # "ota":Lorg/apache/poi/hslf/record/OutlineTextRefAtom;
    goto :goto_1

    .line 463
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 474
    .restart local v5    # "idx":I
    .restart local v8    # "runs":[Lorg/apache/poi/hslf/model/TextRun;
    :cond_5
    aget-object v12, v8, v4

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/TextRun;->getIndex()I

    move-result v12

    if-ne v12, v5, :cond_6

    .line 475
    aget-object v12, v8, v4

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    goto :goto_3

    .line 473
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 483
    .end local v5    # "idx":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v13, -0xff6

    invoke-virtual {v12, v13}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 484
    .local v3, "escherSpRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v9

    .line 485
    .local v9, "shapeId":I
    if-eqz v8, :cond_2

    const/4 v4, 0x0

    :goto_7
    array-length v12, v8

    if-ge v4, v12, :cond_2

    .line 486
    aget-object v12, v8, v4

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/TextRun;->getShapeId()I

    move-result v12

    if-ne v12, v9, :cond_8

    .line 487
    aget-object v12, v8, v4

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/poi/hslf/model/TextShape;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    goto :goto_4

    .line 485
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 494
    .end local v3    # "escherSpRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    .end local v9    # "shapeId":I
    :cond_9
    aget-object v7, v13, v12

    .line 495
    .local v7, "r":Lorg/apache/poi/hslf/record/Record;
    aget-object v15, v2, v4

    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v16

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-nez v15, :cond_a

    .line 496
    aput-object v7, v2, v4

    .line 494
    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto :goto_6
.end method

.method protected setDefaultTextProperties(Lorg/apache/poi/hslf/model/TextRun;)V
    .locals 0
    .param p1, "_txtrun"    # Lorg/apache/poi/hslf/model/TextRun;

    .prologue
    .line 158
    return-void
.end method

.method public setHorizontalAlignment(I)V
    .locals 3
    .param p1, "align"    # I

    .prologue
    .line 250
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 251
    .local v0, "tx":Lorg/apache/poi/hslf/model/TextRun;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/TextRun;->getRichTextRuns()[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setAlignment(I)V

    .line 252
    :cond_0
    return-void
.end method

.method public setHyperlink(III)V
    .locals 4
    .param p1, "linkId"    # I
    .param p2, "beginIndex"    # I
    .param p3, "endIndex"    # I

    .prologue
    .line 524
    new-instance v0, Lorg/apache/poi/hslf/record/InteractiveInfo;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/InteractiveInfo;-><init>()V

    .line 525
    .local v0, "info":Lorg/apache/poi/hslf/record/InteractiveInfo;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    move-result-object v1

    .line 526
    .local v1, "infoAtom":Lorg/apache/poi/hslf/record/InteractiveInfoAtom;
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 527
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 528
    invoke-virtual {v1, p1}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkID(I)V

    .line 530
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v3, v0}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 532
    new-instance v2, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;-><init>()V

    .line 533
    .local v2, "txiatom":Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;
    invoke-virtual {v2, p2}, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;->setStartIndex(I)V

    .line 534
    invoke-virtual {v2, p3}, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;->setEndIndex(I)V

    .line 535
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_txtbox:Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v3, v2}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 537
    return-void
.end method

.method public setMarginBottom(F)V
    .locals 2
    .param p1, "margin"    # F

    .prologue
    .line 286
    const/16 v0, 0x84

    const v1, 0x46467000    # 12700.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 287
    return-void
.end method

.method public setMarginLeft(F)V
    .locals 2
    .param p1, "margin"    # F

    .prologue
    .line 311
    const/16 v0, 0x81

    const v1, 0x46467000    # 12700.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 312
    return-void
.end method

.method public setMarginRight(F)V
    .locals 2
    .param p1, "margin"    # F

    .prologue
    .line 336
    const/16 v0, 0x83

    const v1, 0x46467000    # 12700.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 337
    return-void
.end method

.method public setMarginTop(F)V
    .locals 2
    .param p1, "margin"    # F

    .prologue
    .line 360
    const/16 v0, 0x82

    const v1, 0x46467000    # 12700.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 361
    return-void
.end method

.method public setSheet(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 5
    .param p1, "sheet"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 436
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TextShape;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    .line 442
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v2

    .line 443
    .local v2, "tx":Lorg/apache/poi/hslf/model/TextRun;
    if-eqz v2, :cond_0

    .line 445
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextShape;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/model/TextRun;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 446
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/TextRun;->getRichTextRuns()[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-result-object v1

    .line 447
    .local v1, "rt":[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-lt v0, v3, :cond_1

    .line 452
    .end local v0    # "i":I
    .end local v1    # "rt":[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    :cond_0
    return-void

    .line 448
    .restart local v0    # "i":I
    .restart local v1    # "rt":[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    :cond_1
    aget-object v3, v1, v0

    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextShape;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->supplySlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 447
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 177
    .local v0, "tx":Lorg/apache/poi/hslf/model/TextRun;
    if-nez v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextShape;->createTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 180
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/model/TextRun;->setText(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/TextShape;->setTextId(I)V

    .line 182
    return-void
.end method

.method public setTextId(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 401
    const/16 v0, 0x80

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 402
    return-void
.end method

.method public setVerticalAlignment(I)V
    .locals 1
    .param p1, "align"    # I

    .prologue
    .line 240
    const/16 v0, 0x87

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 241
    return-void
.end method

.method public setWordWrap(I)V
    .locals 1
    .param p1, "wrap"    # I

    .prologue
    .line 383
    const/16 v0, 0x85

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TextShape;->setEscherProperty(SI)V

    .line 384
    return-void
.end method
