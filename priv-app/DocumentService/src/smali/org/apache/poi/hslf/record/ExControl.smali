.class public final Lorg/apache/poi/hslf/record/ExControl;
.super Lorg/apache/poi/hslf/record/ExEmbed;
.source "ExControl.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExEmbed;-><init>()V

    .line 55
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExControl;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/poi/hslf/record/ExControlAtom;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/ExControlAtom;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hslf/record/ExControl;->embedAtom:Lorg/apache/poi/hslf/record/RecordAtom;

    aput-object v2, v0, v1

    .line 56
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 0
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hslf/record/ExEmbed;-><init>([BII)V

    .line 47
    return-void
.end method


# virtual methods
.method public getExControlAtom()Lorg/apache/poi/hslf/record/ExControlAtom;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExControl;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Lorg/apache/poi/hslf/record/ExControlAtom;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExControl:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method
