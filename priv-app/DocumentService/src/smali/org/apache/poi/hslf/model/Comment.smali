.class public final Lorg/apache/poi/hslf/model/Comment;
.super Ljava/lang/Object;
.source "Comment.java"


# instance fields
.field private _comment2000:Lorg/apache/poi/hslf/record/Comment2000;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/Comment2000;)V
    .locals 0
    .param p1, "comment2000"    # Lorg/apache/poi/hslf/record/Comment2000;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    .line 31
    return-void
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Comment2000;->getAuthor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorInitials()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Comment2000;->getAuthorInitials()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getComment2000()Lorg/apache/poi/hslf/record/Comment2000;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Comment2000;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/Comment2000;->setAuthor(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public setAuthorInitials(Ljava/lang/String;)V
    .locals 1
    .param p1, "initials"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/Comment2000;->setAuthorInitials(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Comment;->_comment2000:Lorg/apache/poi/hslf/record/Comment2000;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/Comment2000;->setText(Ljava/lang/String;)V

    .line 74
    return-void
.end method
