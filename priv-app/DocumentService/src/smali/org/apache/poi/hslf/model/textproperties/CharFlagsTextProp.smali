.class public Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;
.super Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
.source "CharFlagsTextProp.java"


# static fields
.field public static final BOLD_IDX:I = 0x0

.field public static final ENABLE_NUMBERING_1_IDX:I = 0xb

.field public static final ENABLE_NUMBERING_2_IDX:I = 0xc

.field public static final ITALIC_IDX:I = 0x1

.field public static NAME:Ljava/lang/String; = null

.field public static final RELIEF_IDX:I = 0x9

.field public static final RESET_NUMBERING_IDX:I = 0xa

.field public static final SHADOW_IDX:I = 0x4

.field public static final STRIKETHROUGH_IDX:I = 0x8

.field public static final UNDERLINE_IDX:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "char_flags"

    sput-object v0, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 37
    const v0, 0xffff

    sget-object v1, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;->NAME:Ljava/lang/String;

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 38
    const-string/jumbo v4, "bold"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 39
    const-string/jumbo v4, "italic"

    aput-object v4, v2, v3

    .line 40
    const-string/jumbo v3, "underline"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    .line 41
    const-string/jumbo v4, "char_unknown_1"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 42
    const-string/jumbo v4, "shadow"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 43
    const-string/jumbo v4, "fehint"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    .line 44
    const-string/jumbo v4, "char_unknown_2"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 45
    const-string/jumbo v4, "kumi"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 46
    const-string/jumbo v4, "strikethrough"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 47
    const-string/jumbo v4, "emboss"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    .line 48
    const-string/jumbo v4, "char_unknown_3"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    .line 49
    const-string/jumbo v4, "char_unknown_4"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    .line 50
    const-string/jumbo v4, "char_unknown_5"

    aput-object v4, v2, v3

    invoke-direct {p0, v5, v0, v1, v2}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;-><init>(IILjava/lang/String;[Ljava/lang/String;)V

    .line 53
    return-void
.end method
