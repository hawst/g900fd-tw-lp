.class public Lorg/apache/poi/hslf/AbstractPPTUtils;
.super Ljava/lang/Object;
.source "AbstractPPTUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AbstractPPTUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDataBlock(Lorg/apache/poi/poifs/common/POIFSBigBlockSize;Ljava/util/ArrayList;Ljava/io/File;)Ljava/util/HashMap;
    .locals 13
    .param p0, "bigBlockSize"    # Lorg/apache/poi/poifs/common/POIFSBigBlockSize;
    .param p2, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/poifs/common/POIFSBigBlockSize;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/io/File;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/poifs/storage/RawDataBlock;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "locArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 60
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 61
    .local v6, "xBatMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lorg/apache/poi/poifs/storage/RawDataBlock;>;"
    const/4 v7, 0x0

    .line 62
    .local v7, "xBatStream":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 63
    .local v0, "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 65
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 95
    return-object v6

    .line 66
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 68
    .local v5, "locNum":I
    if-ltz v5, :cond_0

    .line 72
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .local v8, "xBatStream":Ljava/io/FileInputStream;
    const/16 v9, 0x200

    :try_start_1
    new-array v2, v9, [B

    .line 74
    .local v2, "data1":[B
    invoke-static {v8, v2}, Lorg/apache/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 77
    invoke-virtual {p0}, Lorg/apache/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v9

    mul-int/2addr v9, v5

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/io/FileInputStream;->skip(J)J

    .line 78
    new-instance v1, Lorg/apache/poi/poifs/storage/RawDataBlock;

    .line 79
    invoke-virtual {p0}, Lorg/apache/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v9

    .line 78
    invoke-direct {v1, v8, v9}, Lorg/apache/poi/poifs/storage/RawDataBlock;-><init>(Ljava/io/InputStream;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 80
    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .local v1, "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    :try_start_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 87
    if-eqz v8, :cond_3

    .line 88
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v7, v8

    .line 89
    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 81
    .end local v2    # "data1":[B
    :catch_0
    move-exception v3

    .line 82
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string/jumbo v9, "AbstractPPTUtils"

    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 87
    if-eqz v7, :cond_0

    .line 88
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 89
    :catch_1
    move-exception v3

    .line 90
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v9, "AbstractPPTUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception while closing : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 83
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 84
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string/jumbo v9, "AbstractPPTUtils"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 87
    if-eqz v7, :cond_0

    .line 88
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_0

    .line 89
    :catch_3
    move-exception v3

    .line 90
    const-string/jumbo v9, "AbstractPPTUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception while closing : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 85
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 87
    :goto_3
    if-eqz v7, :cond_2

    .line 88
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 92
    :cond_2
    :goto_4
    throw v9

    .line 89
    :catch_4
    move-exception v3

    .line 90
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "AbstractPPTUtils"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Exception while closing : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 89
    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v2    # "data1":[B
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    .line 90
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "AbstractPPTUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception while closing : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 85
    .end local v2    # "data1":[B
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v2    # "data1":[B
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v9

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 83
    .end local v2    # "data1":[B
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v2    # "data1":[B
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v3

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 81
    .end local v2    # "data1":[B
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catch_8
    move-exception v3

    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v7    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v2    # "data1":[B
    .restart local v8    # "xBatStream":Ljava/io/FileInputStream;
    :catch_9
    move-exception v3

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v7, v8

    .end local v8    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v7    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method public static getDataBlock(Lorg/apache/poi/poifs/common/POIFSBigBlockSize;ILjava/io/File;)Lorg/apache/poi/poifs/storage/RawDataBlock;
    .locals 10
    .param p0, "bigBlockSize"    # Lorg/apache/poi/poifs/common/POIFSBigBlockSize;
    .param p1, "locNum"    # I
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 100
    const/4 v4, 0x0

    .line 101
    .local v4, "xBatStream":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 103
    .local v0, "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v4    # "xBatStream":Ljava/io/FileInputStream;
    .local v5, "xBatStream":Ljava/io/FileInputStream;
    const/16 v6, 0x200

    :try_start_1
    new-array v2, v6, [B

    .line 105
    .local v2, "data1":[B
    invoke-static {v5, v2}, Lorg/apache/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 107
    invoke-virtual {p0}, Lorg/apache/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v6

    mul-int/2addr v6, p1

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Ljava/io/FileInputStream;->skip(J)J

    .line 108
    new-instance v1, Lorg/apache/poi/poifs/storage/RawDataBlock;

    invoke-virtual {p0}, Lorg/apache/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v6

    invoke-direct {v1, v5, v6}, Lorg/apache/poi/poifs/storage/RawDataBlock;-><init>(Ljava/io/InputStream;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 116
    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .local v1, "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    if-eqz v5, :cond_2

    .line 117
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v4, v5

    .line 123
    .end local v2    # "data1":[B
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v4    # "xBatStream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-object v0

    .line 110
    :catch_0
    move-exception v3

    .line 111
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string/jumbo v6, "AbstractPPTUtils"

    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 116
    if-eqz v4, :cond_0

    .line 117
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 118
    :catch_1
    move-exception v3

    .line 119
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v6, "AbstractPPTUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Exception while closing : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 112
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 113
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    const-string/jumbo v6, "AbstractPPTUtils"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 116
    if-eqz v4, :cond_0

    .line 117
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 118
    :catch_3
    move-exception v3

    .line 119
    const-string/jumbo v6, "AbstractPPTUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Exception while closing : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 116
    :goto_3
    if-eqz v4, :cond_1

    .line 117
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 121
    :cond_1
    :goto_4
    throw v6

    .line 118
    :catch_4
    move-exception v3

    .line 119
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v7, "AbstractPPTUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception while closing : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 118
    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v2    # "data1":[B
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    .line 119
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v6, "AbstractPPTUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Exception while closing : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v4, v5

    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v4    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 114
    .end local v2    # "data1":[B
    .end local v4    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v4    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 112
    .end local v4    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    move-object v4, v5

    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v4    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 110
    .end local v4    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v3

    move-object v4, v5

    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v4    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method public static getDataBlock(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;ILjava/io/File;)[B
    .locals 11
    .param p0, "filesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .param p1, "locNum"    # I
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 26
    const/4 v5, 0x0

    .line 27
    .local v5, "xBatStream":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 28
    .local v0, "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    const/4 v2, 0x0

    .line 31
    .local v2, "data":[B
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .local v6, "xBatStream":Ljava/io/FileInputStream;
    const/16 v7, 0x200

    :try_start_1
    new-array v3, v7, [B

    .line 33
    .local v3, "data1":[B
    invoke-static {v6, v3}, Lorg/apache/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 36
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getBigBlockSize()I

    move-result v7

    mul-int/2addr v7, p1

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/io/FileInputStream;->skip(J)J

    .line 37
    new-instance v1, Lorg/apache/poi/poifs/storage/RawDataBlock;

    .line 38
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getBigBlockSize()I

    move-result v7

    .line 37
    invoke-direct {v1, v6, v7}, Lorg/apache/poi/poifs/storage/RawDataBlock;-><init>(Ljava/io/InputStream;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 39
    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .local v1, "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    :try_start_2
    invoke-virtual {v1}, Lorg/apache/poi/poifs/storage/RawDataBlock;->getData()[B
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v2

    .line 46
    if-eqz v6, :cond_2

    .line 47
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v5, v6

    .line 53
    .end local v3    # "data1":[B
    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-object v2

    .line 40
    :catch_0
    move-exception v4

    .line 41
    .local v4, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string/jumbo v7, "AbstractPPTUtils"

    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 46
    if-eqz v5, :cond_0

    .line 47
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 48
    :catch_1
    move-exception v4

    .line 49
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v7, "AbstractPPTUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception while closing : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    .end local v4    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 43
    .restart local v4    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string/jumbo v7, "AbstractPPTUtils"

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 46
    if-eqz v5, :cond_0

    .line 47
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_0

    .line 48
    :catch_3
    move-exception v4

    .line 49
    const-string/jumbo v7, "AbstractPPTUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception while closing : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 44
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 46
    :goto_3
    if-eqz v5, :cond_1

    .line 47
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 51
    :cond_1
    :goto_4
    throw v7

    .line 48
    :catch_4
    move-exception v4

    .line 49
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "AbstractPPTUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception while closing : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 48
    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v3    # "data1":[B
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v4

    .line 49
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v7, "AbstractPPTUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception while closing : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 44
    .end local v3    # "data1":[B
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v3    # "data1":[B
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 42
    .end local v3    # "data1":[B
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v4

    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v3    # "data1":[B
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v4

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 40
    .end local v3    # "data1":[B
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catch_8
    move-exception v4

    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .end local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .end local v5    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v3    # "data1":[B
    .restart local v6    # "xBatStream":Ljava/io/FileInputStream;
    :catch_9
    move-exception v4

    move-object v0, v1

    .end local v1    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    .restart local v0    # "block":Lorg/apache/poi/poifs/storage/RawDataBlock;
    move-object v5, v6

    .end local v6    # "xBatStream":Ljava/io/FileInputStream;
    .restart local v5    # "xBatStream":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method
