.class public final Lorg/apache/poi/hslf/record/CString;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "CString.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private _text:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, 0xfba

    sput-wide v0, Lorg/apache/poi/hslf/record/CString;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 92
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 94
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x2

    const/16 v2, -0x46

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xf

    aput-byte v2, v0, v1

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    .line 96
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    .line 97
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 77
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 79
    if-ge p3, v1, :cond_0

    const/16 p3, 0x8

    .line 82
    :cond_0
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    .line 83
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    .line 87
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    return-void
.end method


# virtual methods
.method public getOptions()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    invoke-static {v0}, Lorg/apache/poi/util/LittleEndian;->getShort([B)S

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 102
    sget-wide v0, Lorg/apache/poi/hslf/record/CString;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    invoke-static {v0}, Lorg/apache/poi/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setOptions(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    int-to-short v1, p1

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 70
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 53
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 54
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 113
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CString;->_text:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 114
    return-void
.end method
