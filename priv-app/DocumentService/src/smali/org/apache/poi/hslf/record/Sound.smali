.class public final Lorg/apache/poi/hslf/record/Sound;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "Sound.java"


# instance fields
.field private _data:Lorg/apache/poi/hslf/record/SoundData;

.field private _header:[B

.field private _name:Lorg/apache/poi/hslf/record/CString;

.field private _type:Lorg/apache/poi/hslf/record/CString;


# direct methods
.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 56
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 58
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_header:[B

    .line 59
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 63
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/Sound;->findInterestingChildren()V

    .line 64
    return-void
.end method

.method private findInterestingChildren()V
    .locals 8

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 68
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v4

    instance-of v1, v1, Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v4

    check-cast v1, Lorg/apache/poi/hslf/record/CString;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_name:Lorg/apache/poi/hslf/record/CString;

    .line 75
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v6

    instance-of v1, v1, Lorg/apache/poi/hslf/record/CString;

    if-eqz v1, :cond_1

    .line 76
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v6

    check-cast v1, Lorg/apache/poi/hslf/record/CString;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_type:Lorg/apache/poi/hslf/record/CString;

    .line 81
    :goto_1
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_2
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 88
    :goto_3
    return-void

    .line 71
    .end local v0    # "i":I
    :cond_0
    sget-object v1, Lorg/apache/poi/hslf/record/Sound;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "First child record wasn\'t a CString, was of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 78
    :cond_1
    sget-object v1, Lorg/apache/poi/hslf/record/Sound;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Second child record wasn\'t a CString, was of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 82
    .restart local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/SoundData;

    if-eqz v1, :cond_3

    .line 83
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/SoundData;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Sound;->_data:Lorg/apache/poi/hslf/record/SoundData;

    goto :goto_3

    .line 81
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->Sound:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSoundData()[B
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_data:Lorg/apache/poi/hslf/record/SoundData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_data:Lorg/apache/poi/hslf/record/SoundData;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SoundData;->getData()[B

    move-result-object v0

    goto :goto_0
.end method

.method public getSoundName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_name:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSoundType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_type:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/Sound;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/Sound;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/Sound;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/Sound;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 109
    return-void
.end method
