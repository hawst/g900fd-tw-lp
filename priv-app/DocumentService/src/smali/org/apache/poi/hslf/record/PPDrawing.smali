.class public final Lorg/apache/poi/hslf/record/PPDrawing;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "PPDrawing.java"


# instance fields
.field private _header:[B

.field private _type:J

.field private childRecords:[Lorg/apache/poi/ddf/EscherRecord;

.field private dg:Lorg/apache/poi/ddf/EscherDgRecord;

.field private textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 150
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    .line 151
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    const/16 v1, 0xf

    invoke-static {v0, v3, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 152
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v1, 0x2

    sget-object v2, Lorg/apache/poi/hslf/record/RecordTypes;->PPDrawing:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v2, v2, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 153
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 155
    new-array v0, v3, [Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 156
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/PPDrawing;->create()V

    .line 157
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 10
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v7, 0x0

    .line 73
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 75
    new-array v0, v3, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    .line 76
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    invoke-static {p1, p2, v0, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v4, 0x2

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    int-to-long v8, v0

    iput-wide v8, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_type:J

    .line 82
    new-array v2, p3, [B

    .line 83
    .local v2, "contents":[B
    invoke-static {p1, p2, v2, v7, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    new-instance v1, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v1}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 87
    .local v1, "erf":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 88
    .local v5, "escherChildren":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/poi/ddf/EscherRecord;>;"
    add-int/lit8 v4, p3, -0x8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hslf/record/PPDrawing;->findEscherChildren(Lorg/apache/poi/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V

    .line 89
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/poi/ddf/EscherRecord;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/ddf/EscherRecord;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    .line 91
    const/4 v0, 0x1

    iget-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    array-length v3, v3

    if-ne v0, v3, :cond_0

    const/16 v0, -0xffe

    iget-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    aget-object v3, v3, v7

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v3

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    aget-object v0, v0, v7

    instance-of v0, v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    aget-object v0, v0, v7

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/record/PPDrawing;->findInDgContainer(Lorg/apache/poi/ddf/EscherContainerRecord;)[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_0
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 96
    .local v6, "textboxes":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/poi/hslf/record/EscherTextboxWrapper;>;"
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    invoke-direct {p0, v0, v6}, Lorg/apache/poi/hslf/record/PPDrawing;->findEscherTextboxRecord([Lorg/apache/poi/ddf/EscherRecord;Ljava/util/Vector;)V

    .line 97
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v6, v0}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    goto :goto_0
.end method

.method private create()V
    .locals 11

    .prologue
    const/16 v9, -0xffc

    const/4 v10, 0x1

    const/16 v8, 0xf

    .line 273
    new-instance v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 274
    .local v1, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v7, -0xffe

    invoke-virtual {v1, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 275
    invoke-virtual {v1, v8}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 277
    new-instance v0, Lorg/apache/poi/ddf/EscherDgRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherDgRecord;-><init>()V

    .line 278
    .local v0, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    const/16 v7, 0x10

    invoke-virtual {v0, v7}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    .line 279
    invoke-virtual {v0, v10}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 280
    invoke-virtual {v1, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 282
    new-instance v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v6}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 283
    .local v6, "spgrContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v6, v8}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 284
    const/16 v7, -0xffd

    invoke-virtual {v6, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 286
    new-instance v4, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 287
    .local v4, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v4, v8}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 288
    invoke-virtual {v4, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 290
    new-instance v5, Lorg/apache/poi/ddf/EscherSpgrRecord;

    invoke-direct {v5}, Lorg/apache/poi/ddf/EscherSpgrRecord;-><init>()V

    .line 291
    .local v5, "spgr":Lorg/apache/poi/ddf/EscherSpgrRecord;
    invoke-virtual {v5, v10}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setOptions(S)V

    .line 292
    invoke-virtual {v4, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 294
    new-instance v3, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherSpRecord;-><init>()V

    .line 295
    .local v3, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 296
    const/4 v7, 0x5

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 297
    invoke-virtual {v4, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 298
    invoke-virtual {v6, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 299
    invoke-virtual {v1, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 301
    new-instance v4, Lorg/apache/poi/ddf/EscherContainerRecord;

    .end local v4    # "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-direct {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 302
    .restart local v4    # "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v4, v8}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 303
    invoke-virtual {v4, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 304
    new-instance v3, Lorg/apache/poi/ddf/EscherSpRecord;

    .end local v3    # "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherSpRecord;-><init>()V

    .line 305
    .restart local v3    # "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    const/16 v7, 0x12

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 306
    const/16 v7, 0xc00

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 307
    invoke-virtual {v4, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 309
    new-instance v2, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-direct {v2}, Lorg/apache/poi/ddf/EscherOptRecord;-><init>()V

    .line 310
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v7, -0xff5

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->setRecordId(S)V

    .line 311
    new-instance v7, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v8, 0x181

    const/high16 v9, 0x8000000

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 312
    new-instance v7, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v8, 0x183

    const v9, 0x8000005

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 313
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x193

    const v9, 0x99936e

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 314
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x194

    const v9, 0x76b1be

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 315
    new-instance v7, Lorg/apache/poi/ddf/EscherBoolProperty;

    const/16 v8, 0x1bf

    const v9, 0x120012

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 316
    new-instance v7, Lorg/apache/poi/ddf/EscherBoolProperty;

    const/16 v8, 0x1ff

    const/high16 v9, 0x80000

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 317
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x304

    const/16 v9, 0x9

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 318
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x33f

    const v9, 0x10001

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 319
    invoke-virtual {v4, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 321
    invoke-virtual {v1, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 323
    new-array v7, v10, [Lorg/apache/poi/ddf/EscherRecord;

    const/4 v8, 0x0

    .line 324
    aput-object v1, v7, v8

    .line 323
    iput-object v7, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    .line 326
    return-void
.end method

.method private findEscherChildren(Lorg/apache/poi/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V
    .locals 8
    .param p1, "erf"    # Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    .param p2, "source"    # [B
    .param p3, "startPos"    # I
    .param p4, "lenToGo"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/ddf/DefaultEscherRecordFactory;",
            "[BII",
            "Ljava/util/Vector",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "found":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/poi/ddf/EscherRecord;>;"
    const/16 v7, 0x8

    const/4 v6, 0x5

    .line 164
    add-int/lit8 v3, p3, 0x4

    invoke-static {p2, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    add-int/lit8 v0, v3, 0x8

    .line 167
    .local v0, "escherBytes":I
    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    .line 169
    .local v1, "r":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v1, p2, p3, p1}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 171
    invoke-virtual {p5, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v2

    .line 175
    .local v2, "size":I
    if-ge v2, v7, :cond_0

    .line 176
    sget-object v3, Lorg/apache/poi/hslf/record/PPDrawing;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Hit short DDF record at "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 185
    :cond_0
    if-eq v2, v0, :cond_1

    .line 186
    sget-object v3, Lorg/apache/poi/hslf/record/PPDrawing;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Record length="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " but getRecordSize() returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "; record: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 187
    move v2, v0

    .line 189
    :cond_1
    add-int/2addr p3, v2

    .line 190
    sub-int/2addr p4, v2

    .line 191
    if-lt p4, v7, :cond_2

    .line 192
    invoke-direct/range {p0 .. p5}, Lorg/apache/poi/hslf/record/PPDrawing;->findEscherChildren(Lorg/apache/poi/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V

    .line 194
    :cond_2
    return-void
.end method

.method private findEscherTextboxRecord([Lorg/apache/poi/ddf/EscherRecord;Ljava/util/Vector;)V
    .locals 8
    .param p1, "toSearch"    # [Lorg/apache/poi/ddf/EscherRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/poi/ddf/EscherRecord;",
            "Ljava/util/Vector",
            "<",
            "Lorg/apache/poi/hslf/record/EscherTextboxWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 200
    .local p2, "found":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/poi/hslf/record/EscherTextboxWrapper;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, p1

    if-lt v2, v7, :cond_0

    .line 222
    return-void

    .line 201
    :cond_0
    aget-object v7, p1, v2

    instance-of v7, v7, Lorg/apache/poi/ddf/EscherTextboxRecord;

    if-eqz v7, :cond_4

    .line 202
    aget-object v5, p1, v2

    check-cast v5, Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 203
    .local v5, "tbr":Lorg/apache/poi/ddf/EscherTextboxRecord;
    new-instance v6, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-direct {v6, v5}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;-><init>(Lorg/apache/poi/ddf/EscherTextboxRecord;)V

    .line 204
    .local v6, "w":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual {p2, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 205
    move v3, v2

    .local v3, "j":I
    :goto_1
    if-gez v3, :cond_2

    .line 200
    .end local v3    # "j":I
    .end local v5    # "tbr":Lorg/apache/poi/ddf/EscherTextboxRecord;
    .end local v6    # "w":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 206
    .restart local v3    # "j":I
    .restart local v5    # "tbr":Lorg/apache/poi/ddf/EscherTextboxRecord;
    .restart local v6    # "w":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    :cond_2
    aget-object v7, p1, v3

    instance-of v7, v7, Lorg/apache/poi/ddf/EscherSpRecord;

    if-eqz v7, :cond_3

    .line 207
    aget-object v4, p1, v3

    check-cast v4, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 208
    .local v4, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->setShapeId(I)V

    goto :goto_2

    .line 205
    .end local v4    # "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 214
    .end local v3    # "j":I
    .end local v5    # "tbr":Lorg/apache/poi/ddf/EscherTextboxRecord;
    .end local v6    # "w":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    :cond_4
    aget-object v7, p1, v2

    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 215
    aget-object v7, p1, v2

    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v1

    .line 216
    .local v1, "childrenL":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    new-array v0, v7, [Lorg/apache/poi/ddf/EscherRecord;

    .line 217
    .local v0, "children":[Lorg/apache/poi/ddf/EscherRecord;
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 218
    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hslf/record/PPDrawing;->findEscherTextboxRecord([Lorg/apache/poi/ddf/EscherRecord;Ljava/util/Vector;)V

    goto :goto_2
.end method

.method private findInDgContainer(Lorg/apache/poi/ddf/EscherContainerRecord;)[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    .locals 13
    .param p1, "escherContainerF002"    # Lorg/apache/poi/ddf/EscherContainerRecord;

    .prologue
    .line 101
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 102
    .local v4, "found":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/record/EscherTextboxWrapper;>;"
    const/16 v10, -0xffd

    invoke-virtual {p0, v10, p1}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    .line 103
    .local v0, "SpgrContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v10, -0xffc

    invoke-virtual {p0, v10, v0}, Lorg/apache/poi/hslf/record/PPDrawing;->findAllEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)[Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    .line 104
    .local v3, "escherContainersF004":[Lorg/apache/poi/ddf/EscherContainerRecord;
    array-length v11, v3

    const/4 v10, 0x0

    :goto_0
    if-lt v10, v11, :cond_0

    .line 125
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-interface {v4, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    return-object v10

    .line 104
    :cond_0
    aget-object v7, v3, v10

    .line 105
    .local v7, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-direct {p0, v7}, Lorg/apache/poi/hslf/record/PPDrawing;->findInSpContainer(Lorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    move-result-object v5

    .line 106
    .local v5, "nineAtom":Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    const/4 v6, 0x0

    .line 107
    .local v6, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    const/16 v12, -0xff6

    invoke-virtual {p0, v12, v7}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    .line 108
    .local v1, "escherContainerF00A":Lorg/apache/poi/ddf/EscherRecord;
    if-eqz v1, :cond_1

    .line 109
    instance-of v12, v1, Lorg/apache/poi/ddf/EscherSpRecord;

    if-eqz v12, :cond_1

    move-object v6, v1

    .line 110
    check-cast v6, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 113
    :cond_1
    const/16 v12, -0xff3

    invoke-virtual {p0, v12, v7}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    .line 114
    .local v2, "escherContainerF00D":Lorg/apache/poi/ddf/EscherRecord;
    if-nez v2, :cond_3

    .line 104
    :cond_2
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 115
    :cond_3
    instance-of v12, v2, Lorg/apache/poi/ddf/EscherTextboxRecord;

    if-eqz v12, :cond_2

    move-object v8, v2

    .line 116
    check-cast v8, Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 117
    .local v8, "tbr":Lorg/apache/poi/ddf/EscherTextboxRecord;
    new-instance v9, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    invoke-direct {v9, v8}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;-><init>(Lorg/apache/poi/ddf/EscherTextboxRecord;)V

    .line 118
    .local v9, "w":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual {v9, v5}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->setStyleTextProp9Atom(Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V

    .line 119
    if-eqz v6, :cond_4

    .line 120
    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v12

    invoke-virtual {v9, v12}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->setShapeId(I)V

    .line 122
    :cond_4
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private findInSpContainer(Lorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    .locals 11
    .param p1, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 128
    const/16 v7, -0xfef

    invoke-virtual {p0, v7, p1}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    .line 129
    .local v3, "escherContainerF011":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v3, :cond_0

    move-object v7, v8

    .line 143
    :goto_0
    return-object v7

    .line 130
    :cond_0
    const/16 v7, 0x1388

    invoke-virtual {p0, v7, v3}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 131
    .local v1, "escherContainer1388":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v1, :cond_1

    move-object v7, v8

    goto :goto_0

    .line 132
    :cond_1
    const/16 v7, 0x138a

    invoke-virtual {p0, v7, v1}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 133
    .local v2, "escherContainer138A":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v2, :cond_2

    move-object v7, v8

    goto :goto_0

    .line 134
    :cond_2
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    .line 135
    .local v6, "size":I
    const/4 v7, 0x2

    if-eq v7, v6, :cond_3

    move-object v7, v8

    goto :goto_0

    .line 136
    :cond_3
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/ddf/UnknownEscherRecord;

    invoke-virtual {p0, v7}, Lorg/apache/poi/hslf/record/PPDrawing;->buildFromUnknownEscherRecord(Lorg/apache/poi/ddf/UnknownEscherRecord;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v4

    .line 137
    .local v4, "r0":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v2, v10}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/ddf/UnknownEscherRecord;

    invoke-virtual {p0, v7}, Lorg/apache/poi/hslf/record/PPDrawing;->buildFromUnknownEscherRecord(Lorg/apache/poi/ddf/UnknownEscherRecord;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v5

    .line 138
    .local v5, "r1":Lorg/apache/poi/hslf/record/Record;
    instance-of v7, v4, Lorg/apache/poi/hslf/record/CString;

    if-nez v7, :cond_4

    move-object v7, v8

    goto :goto_0

    .line 139
    :cond_4
    const-string/jumbo v7, "___PPT9"

    check-cast v4, Lorg/apache/poi/hslf/record/CString;

    .end local v4    # "r0":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    move-object v7, v8

    goto :goto_0

    .line 140
    :cond_5
    instance-of v7, v5, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;

    if-nez v7, :cond_6

    move-object v7, v8

    goto :goto_0

    :cond_6
    move-object v0, v5

    .line 141
    check-cast v0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;

    .line 142
    .local v0, "blob":Lorg/apache/poi/hslf/record/BinaryTagDataBlob;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    array-length v7, v7

    if-eq v10, v7, :cond_7

    move-object v7, v8

    goto :goto_0

    .line 143
    :cond_7
    const-wide/16 v8, 0xfac

    invoke-virtual {v0, v8, v9}, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    goto :goto_0
.end method


# virtual methods
.method public addTextboxWrapper(Lorg/apache/poi/hslf/record/EscherTextboxWrapper;)V
    .locals 4
    .param p1, "txtbox"    # Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .prologue
    const/4 v3, 0x0

    .line 332
    iget-object v1, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 333
    .local v0, "tw":[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    iget-object v2, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 335
    iget-object v1, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    array-length v1, v1

    aput-object p1, v0, v1

    .line 336
    iput-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    .line 337
    return-void
.end method

.method protected buildFromUnknownEscherRecord(Lorg/apache/poi/ddf/UnknownEscherRecord;)Lorg/apache/poi/hslf/record/Record;
    .locals 9
    .param p1, "unknown"    # Lorg/apache/poi/ddf/UnknownEscherRecord;

    .prologue
    const/4 v8, 0x0

    .line 390
    invoke-virtual {p1}, Lorg/apache/poi/ddf/UnknownEscherRecord;->getData()[B

    move-result-object v0

    .line 391
    .local v0, "bingo":[B
    array-length v5, v0

    add-int/lit8 v5, v5, 0x8

    new-array v4, v5, [B

    .line 392
    .local v4, "restoredRecord":[B
    const/16 v5, 0x8

    array-length v6, v0

    invoke-static {v0, v8, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 393
    invoke-virtual {p1}, Lorg/apache/poi/ddf/UnknownEscherRecord;->getVersion()S

    move-result v3

    .line 394
    .local v3, "recordVersion":S
    invoke-virtual {p1}, Lorg/apache/poi/ddf/UnknownEscherRecord;->getRecordId()S

    move-result v1

    .line 395
    .local v1, "recordId":S
    invoke-virtual {p1}, Lorg/apache/poi/ddf/UnknownEscherRecord;->getRecordSize()I

    move-result v2

    .line 396
    .local v2, "recordLength":I
    invoke-static {v4, v8, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 397
    const/4 v5, 0x2

    invoke-static {v4, v5, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 398
    const/4 v5, 0x4

    invoke-static {v4, v5, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 399
    int-to-long v6, v1

    array-length v5, v4

    invoke-static {v6, v7, v4, v8, v5}, Lorg/apache/poi/hslf/record/Record;->createRecordForType(J[BII)Lorg/apache/poi/hslf/record/Record;

    move-result-object v5

    return-object v5
.end method

.method protected findAllEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)[Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 5
    .param p1, "type"    # S
    .param p2, "parent"    # Lorg/apache/poi/ddf/EscherContainerRecord;

    .prologue
    .line 379
    if-nez p2, :cond_0

    const/4 v3, 0x0

    new-array v3, v3, [Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 387
    :goto_0
    return-object v3

    .line 380
    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v1

    .line 381
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 382
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 387
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/poi/ddf/EscherContainerRecord;

    goto :goto_0

    .line 382
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 383
    .local v0, "child":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v4

    if-ne p1, v4, :cond_1

    .line 384
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 5
    .param p1, "type"    # S
    .param p2, "parent"    # Lorg/apache/poi/ddf/EscherContainerRecord;

    .prologue
    const/4 v2, 0x0

    .line 359
    if-nez p2, :cond_0

    move-object v0, v2

    .line 366
    :goto_0
    return-object v0

    .line 360
    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v1

    .line 361
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    move-object v0, v2

    .line 366
    goto :goto_0

    .line 361
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 362
    .local v0, "child":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v4

    if-ne p1, v4, :cond_1

    goto :goto_0
.end method

.method protected findFirstEscherRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherRecord;
    .locals 5
    .param p1, "type"    # S
    .param p2, "parent"    # Lorg/apache/poi/ddf/EscherContainerRecord;

    .prologue
    const/4 v2, 0x0

    .line 369
    if-nez p2, :cond_0

    move-object v0, v2

    .line 376
    :goto_0
    return-object v0

    .line 370
    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v1

    .line 371
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    move-object v0, v2

    .line 376
    goto :goto_0

    .line 371
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 372
    .local v0, "child":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v4

    if-ne p1, v4, :cond_1

    goto :goto_0
.end method

.method public getChildRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEscherDgRecord()Lorg/apache/poi/ddf/EscherDgRecord;
    .locals 5

    .prologue
    .line 345
    iget-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawing;->dg:Lorg/apache/poi/ddf/EscherDgRecord;

    if-nez v3, :cond_1

    .line 346
    iget-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    const/4 v4, 0x0

    aget-object v0, v3, v4

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 347
    .local v0, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 355
    .end local v0    # "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_1
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawing;->dg:Lorg/apache/poi/ddf/EscherDgRecord;

    return-object v3

    .line 348
    .restart local v0    # "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 349
    .local v2, "r":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v3, v2, Lorg/apache/poi/ddf/EscherDgRecord;

    if-eqz v3, :cond_0

    .line 350
    check-cast v2, Lorg/apache/poi/ddf/EscherDgRecord;

    .end local v2    # "r":Lorg/apache/poi/ddf/EscherRecord;
    iput-object v2, p0, Lorg/apache/poi/hslf/record/PPDrawing;->dg:Lorg/apache/poi/ddf/EscherDgRecord;

    goto :goto_0
.end method

.method public getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    return-object v0
.end method

.method public getNumberedListInfo()[Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    .locals 24

    .prologue
    .line 403
    new-instance v14, Ljava/util/LinkedList;

    invoke-direct {v14}, Ljava/util/LinkedList;-><init>()V

    .line 404
    .local v14, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/record/StyleTextProp9Atom;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v11

    .line 405
    .local v11, "escherRecords":[Lorg/apache/poi/ddf/EscherRecord;
    array-length v0, v11

    move/from16 v19, v0

    const/16 v16, 0x0

    move/from16 v18, v16

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_0

    .line 430
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v16

    move/from16 v0, v16

    new-array v0, v0, [Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    return-object v16

    .line 405
    :cond_0
    aget-object v10, v11, v18

    .line 406
    .local v10, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherContainerRecord;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    const/16 v16, -0xffe

    invoke-virtual {v10}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    move-object v6, v10

    .line 407
    check-cast v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 408
    .local v6, "escherContainerF002":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v16, -0xffd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v6}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v7

    .line 409
    .local v7, "escherContainerF003":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v16, -0xffc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v7}, Lorg/apache/poi/hslf/record/PPDrawing;->findAllEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)[Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v9

    .line 410
    .local v9, "escherContainersF004":[Lorg/apache/poi/ddf/EscherContainerRecord;
    array-length v0, v9

    move/from16 v20, v0

    const/16 v16, 0x0

    move/from16 v17, v16

    :goto_1
    move/from16 v0, v17

    move/from16 v1, v20

    if-lt v0, v1, :cond_2

    .line 405
    .end local v6    # "escherContainerF002":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v7    # "escherContainerF003":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v9    # "escherContainersF004":[Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_1
    add-int/lit8 v16, v18, 0x1

    move/from16 v18, v16

    goto :goto_0

    .line 410
    .restart local v6    # "escherContainerF002":Lorg/apache/poi/ddf/EscherContainerRecord;
    .restart local v7    # "escherContainerF003":Lorg/apache/poi/ddf/EscherContainerRecord;
    .restart local v9    # "escherContainersF004":[Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_2
    aget-object v3, v9, v17

    .line 411
    .local v3, "containerF004":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v16, -0xfef

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v3}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v8

    .line 412
    .local v8, "escherContainerF011":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v8, :cond_4

    .line 410
    :cond_3
    :goto_2
    add-int/lit8 v16, v17, 0x1

    move/from16 v17, v16

    goto :goto_1

    .line 413
    :cond_4
    const/16 v16, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v8}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v4

    .line 414
    .local v4, "escherContainer1388":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v4, :cond_3

    .line 415
    const/16 v16, 0x138a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v4}, Lorg/apache/poi/hslf/record/PPDrawing;->findFirstEscherContainerRecordOfType(SLorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v5

    .line 416
    .local v5, "escherContainer138A":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v5, :cond_3

    .line 417
    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v15

    .line 418
    .local v15, "size":I
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v0, v15, :cond_3

    .line 419
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/ddf/UnknownEscherRecord;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/PPDrawing;->buildFromUnknownEscherRecord(Lorg/apache/poi/ddf/UnknownEscherRecord;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v12

    .line 420
    .local v12, "r0":Lorg/apache/poi/hslf/record/Record;
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/ddf/UnknownEscherRecord;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/PPDrawing;->buildFromUnknownEscherRecord(Lorg/apache/poi/ddf/UnknownEscherRecord;)Lorg/apache/poi/hslf/record/Record;

    move-result-object v13

    .line 421
    .local v13, "r1":Lorg/apache/poi/hslf/record/Record;
    instance-of v0, v12, Lorg/apache/poi/hslf/record/CString;

    move/from16 v16, v0

    if-eqz v16, :cond_3

    .line 422
    const-string/jumbo v16, "___PPT9"

    check-cast v12, Lorg/apache/poi/hslf/record/CString;

    .end local v12    # "r0":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v12}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 423
    instance-of v0, v13, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;

    move/from16 v16, v0

    if-eqz v16, :cond_3

    move-object v2, v13

    .line 424
    check-cast v2, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;

    .line 425
    .local v2, "blob":Lorg/apache/poi/hslf/record/BinaryTagDataBlob;
    const/16 v16, 0x1

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v16

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 426
    const-wide/16 v22, 0xfac

    move-wide/from16 v0, v22

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 227
    iget-wide v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_type:J

    return-wide v0
.end method

.method public getTextboxWrappers()[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    array-length v5, v5

    if-lt v2, v5, :cond_0

    .line 246
    const/4 v3, 0x0

    .line 247
    .local v3, "newSize":I
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    array-length v5, v5

    if-lt v2, v5, :cond_1

    .line 252
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v6, 0x4

    invoke-static {v5, v6, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 255
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->_header:[B

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 258
    new-array v0, v3, [B

    .line 259
    .local v0, "b":[B
    const/4 v1, 0x0

    .line 260
    .local v1, "done":I
    const/4 v2, 0x0

    :goto_2
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    array-length v5, v5

    if-lt v2, v5, :cond_2

    .line 266
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 267
    return-void

    .line 242
    .end local v0    # "b":[B
    .end local v1    # "done":I
    .end local v3    # "newSize":I
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    aget-object v5, v5, v2

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->writeOut(Ljava/io/OutputStream;)V

    .line 241
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 248
    .restart local v3    # "newSize":I
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v5

    add-int/2addr v3, v5

    .line 247
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 261
    .restart local v0    # "b":[B
    .restart local v1    # "done":I
    :cond_2
    iget-object v5, p0, Lorg/apache/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/poi/ddf/EscherRecord;

    aget-object v5, v5, v2

    invoke-virtual {v5, v1, v0}, Lorg/apache/poi/ddf/EscherRecord;->serialize(I[B)I

    move-result v4

    .line 262
    .local v4, "written":I
    add-int/2addr v1, v4

    .line 260
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method
