.class public Lorg/apache/poi/hslf/record/ExOleObjStg;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "ExOleObjStg.java"

# interfaces
.implements Lorg/apache/poi/hslf/record/PersistRecord;
.implements Lorg/apache/poi/hslf/record/PositionDependentRecord;


# instance fields
.field private _data:[B

.field private _header:[B

.field private _persistId:I

.field protected myLastOnDiskOffset:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 51
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    .line 52
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    .line 54
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    const/16 v1, 0x10

    invoke-static {v0, v2, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 55
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExOleObjStg;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 56
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 57
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 67
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 69
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    .line 70
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    .line 74
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    return-void
.end method


# virtual methods
.method public getData()Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 100
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExOleObjStg;->isCompressed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    invoke-static {v2}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    .line 103
    .local v1, "size":I
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    const/4 v3, 0x4

    iget-object v4, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    array-length v4, v4

    invoke-direct {v0, v2, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 104
    .local v0, "compressedStream":Ljava/io/InputStream;
    new-instance v2, Lorg/apache/poi/util/BoundedInputStream;

    new-instance v3, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v3, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    int-to-long v4, v1

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/util/BoundedInputStream;-><init>(Ljava/io/InputStream;J)V

    .line 106
    .end local v0    # "compressedStream":Ljava/io/InputStream;
    .end local v1    # "size":I
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    array-length v5, v5

    invoke-direct {v2, v3, v4, v5}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    goto :goto_0
.end method

.method public getDataLength()I
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExOleObjStg;->isCompressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 90
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    array-length v0, v0

    goto :goto_0
.end method

.method public getLastOnDiskOffset()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->myLastOnDiskOffset:I

    return v0
.end method

.method public getPersistId()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_persistId:I

    return v0
.end method

.method public getRawData()[B
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExOleObjStg:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public isCompressed()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    invoke-static {v1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public setData([B)V
    .locals 6
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    .line 120
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 122
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    new-array v0, v5, [B

    .line 123
    .local v0, "b":[B
    array-length v3, p1

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 124
    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 126
    new-instance v1, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v1, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 127
    .local v1, "def":Ljava/util/zip/DeflaterOutputStream;
    const/4 v3, 0x0

    array-length v4, p1

    invoke-virtual {v1, p1, v3, v4}, Ljava/util/zip/DeflaterOutputStream;->write([BII)V

    .line 128
    invoke-virtual {v1}, Ljava/util/zip/DeflaterOutputStream;->finish()V

    .line 129
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    .line 130
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    iget-object v4, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    array-length v4, v4

    invoke-static {v3, v5, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 131
    return-void
.end method

.method public setLastOnDiskOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 180
    iput p1, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->myLastOnDiskOffset:I

    .line 181
    return-void
.end method

.method public setPersistId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 166
    iput p1, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_persistId:I

    .line 167
    return-void
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "oldToNewReferencesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 151
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExOleObjStg;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 152
    return-void
.end method
