.class public final Lorg/apache/poi/hslf/record/FontEntityAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "FontEntityAtom.java"


# instance fields
.field private _header:[B

.field private _recdata:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 62
    const/16 v0, 0x44

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    .line 64
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    .line 65
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 66
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 67
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 48
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 50
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    .line 51
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    .line 55
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    return-void
.end method


# virtual methods
.method public getCharSet()I
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x40

    aget-byte v0, v0, v1

    return v0
.end method

.method public getFontFlags()I
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x41

    aget-byte v0, v0, v1

    return v0
.end method

.method public getFontIndex()I
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 80
    const/4 v2, 0x0

    .line 82
    .local v2, "name":Ljava/lang/String;
    const/4 v1, 0x0

    .line 83
    .local v1, "i":I
    :goto_0
    const/16 v3, 0x40

    if-lt v1, v3, :cond_0

    .line 94
    :goto_1
    return-object v2

    .line 85
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    aget-byte v3, v3, v1

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v3, v3, v4

    if-nez v3, :cond_1

    .line 86
    new-instance v2, Ljava/lang/String;

    .end local v2    # "name":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/4 v4, 0x0

    const-string/jumbo v5, "UTF-16LE"

    invoke-direct {v2, v3, v4, v1, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    .restart local v2    # "name":Ljava/lang/String;
    goto :goto_1

    .line 89
    :cond_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 91
    .end local v2    # "name":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getFontType()I
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x42

    aget-byte v0, v0, v1

    return v0
.end method

.method public getPitchAndFamily()I
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x43

    aget-byte v0, v0, v1

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->FontEntityAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setCharSet(I)V
    .locals 3
    .param p1, "charset"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x40

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 139
    return-void
.end method

.method public setFontFlags(I)V
    .locals 3
    .param p1, "flags"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x41

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 158
    return-void
.end method

.method public setFontIndex(I)V
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    const/4 v1, 0x0

    int-to-short v2, p1

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 126
    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 106
    const-string/jumbo v2, "\u0000"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "\u0000"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 111
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x20

    if-le v2, v3, :cond_1

    .line 112
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "The length of the font name, including null termination, must not exceed 32 characters"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 117
    :cond_1
    :try_start_0
    const-string/jumbo v2, "UTF-16LE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 118
    .local v0, "bytes":[B
    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/4 v4, 0x0

    array-length v5, v0

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    return-void

    .line 119
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setFontType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x42

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 182
    return-void
.end method

.method public setPitchAndFamily(I)V
    .locals 3
    .param p1, "val"    # I

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    const/16 v1, 0x43

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 206
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 222
    iget-object v0, p0, Lorg/apache/poi/hslf/record/FontEntityAtom;->_recdata:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 223
    return-void
.end method
