.class public Lorg/apache/poi/hslf/model/textproperties/TextProp;
.super Ljava/lang/Object;
.source "TextProp.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field protected dataValue:I

.field protected maskInHeader:I

.field protected propName:Ljava/lang/String;

.field protected sizeOfDataBlock:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "sizeOfDataBlock"    # I
    .param p2, "maskInHeader"    # I
    .param p3, "propName"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->sizeOfDataBlock:I

    .line 42
    iput p2, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->maskInHeader:I

    .line 43
    iput-object p3, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->propName:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->dataValue:I

    .line 45
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 84
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getMask()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->maskInHeader:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->propName:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->sizeOfDataBlock:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->dataValue:I

    return v0
.end method

.method public getWriteMask()I
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getMask()I

    move-result v0

    return v0
.end method

.method public setValue(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 77
    iput p1, p0, Lorg/apache/poi/hslf/model/textproperties/TextProp;->dataValue:I

    return-void
.end method
