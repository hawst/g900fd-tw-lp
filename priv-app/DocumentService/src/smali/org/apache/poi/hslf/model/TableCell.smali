.class public final Lorg/apache/poi/hslf/model/TableCell;
.super Lorg/apache/poi/hslf/model/TextBox;
.source "TableCell.java"


# static fields
.field protected static final DEFAULT_HEIGHT:I = 0x28

.field protected static final DEFAULT_WIDTH:I = 0x64


# instance fields
.field private borderBottom:Lorg/apache/poi/hslf/model/Line;

.field private borderLeft:Lorg/apache/poi/hslf/model/Line;

.field private borderRight:Lorg/apache/poi/hslf/model/Line;

.field private borderTop:Lorg/apache/poi/hslf/model/Line;


# direct methods
.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/TextBox;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/TextBox;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/TableCell;->setShapeType(I)V

    .line 60
    return-void
.end method


# virtual methods
.method protected anchorBorder(ILorg/apache/poi/hslf/model/Line;)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    const/4 v4, 0x0

    .line 75
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 76
    .local v0, "cellAnchor":Lorg/apache/poi/java/awt/Rectangle;
    new-instance v1, Lorg/apache/poi/java/awt/Rectangle;

    invoke-direct {v1}, Lorg/apache/poi/java/awt/Rectangle;-><init>()V

    .line 77
    .local v1, "lineAnchor":Lorg/apache/poi/java/awt/Rectangle;
    packed-switch p1, :pswitch_data_0

    .line 103
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unknown border type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 79
    :pswitch_0
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 80
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 81
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 82
    iput v4, v1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 105
    :goto_0
    invoke-virtual {p2, v1}, Lorg/apache/poi/hslf/model/Line;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 106
    return-void

    .line 85
    :pswitch_1
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v2, v3

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 86
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 87
    iput v4, v1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 88
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    goto :goto_0

    .line 91
    :pswitch_2
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 92
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v2, v3

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 93
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 94
    iput v4, v1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    goto :goto_0

    .line 97
    :pswitch_3
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 98
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 99
    iput v4, v1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 100
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    iput v2, v1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 4
    .param p1, "isChild"    # Z

    .prologue
    const/high16 v3, 0x20000

    .line 63
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/TextBox;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 64
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v2, -0xff5

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/TableCell;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 65
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v1, 0x80

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hslf/model/TableCell;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 66
    const/16 v1, 0xbf

    invoke-static {v0, v1, v3}, Lorg/apache/poi/hslf/model/TableCell;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 67
    const/16 v1, 0x1bf

    const v2, 0x150001

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hslf/model/TableCell;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 68
    const/16 v1, 0x23f

    invoke-static {v0, v1, v3}, Lorg/apache/poi/hslf/model/TableCell;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 69
    const/16 v1, 0x7f

    const/high16 v2, 0x40000

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hslf/model/TableCell;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 71
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v1
.end method

.method public getBorderBottom()Lorg/apache/poi/hslf/model/Line;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderBottom:Lorg/apache/poi/hslf/model/Line;

    return-object v0
.end method

.method public getBorderLeft()Lorg/apache/poi/hslf/model/Line;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderLeft:Lorg/apache/poi/hslf/model/Line;

    return-object v0
.end method

.method public getBorderRight()Lorg/apache/poi/hslf/model/Line;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderRight:Lorg/apache/poi/hslf/model/Line;

    return-object v0
.end method

.method public getBorderTop()Lorg/apache/poi/hslf/model/Line;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderTop:Lorg/apache/poi/hslf/model/Line;

    return-object v0
.end method

.method public setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V
    .locals 2
    .param p1, "anchor"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 145
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/TextBox;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 147
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderTop:Lorg/apache/poi/hslf/model/Line;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderTop:Lorg/apache/poi/hslf/model/Line;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderRight:Lorg/apache/poi/hslf/model/Line;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderRight:Lorg/apache/poi/hslf/model/Line;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 149
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderBottom:Lorg/apache/poi/hslf/model/Line;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderBottom:Lorg/apache/poi/hslf/model/Line;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 150
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TableCell;->borderLeft:Lorg/apache/poi/hslf/model/Line;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderLeft:Lorg/apache/poi/hslf/model/Line;

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 151
    :cond_3
    return-void
.end method

.method public setBorderBottom(Lorg/apache/poi/hslf/model/Line;)V
    .locals 1
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 140
    if-eqz p1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 141
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderBottom:Lorg/apache/poi/hslf/model/Line;

    .line 142
    return-void
.end method

.method public setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V
    .locals 1
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 113
    if-eqz p1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 114
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderLeft:Lorg/apache/poi/hslf/model/Line;

    .line 115
    return-void
.end method

.method public setBorderRight(Lorg/apache/poi/hslf/model/Line;)V
    .locals 1
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 122
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 123
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderRight:Lorg/apache/poi/hslf/model/Line;

    .line 124
    return-void
.end method

.method public setBorderTop(Lorg/apache/poi/hslf/model/Line;)V
    .locals 1
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 131
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/model/TableCell;->anchorBorder(ILorg/apache/poi/hslf/model/Line;)V

    .line 132
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TableCell;->borderTop:Lorg/apache/poi/hslf/model/Line;

    .line 133
    return-void
.end method
