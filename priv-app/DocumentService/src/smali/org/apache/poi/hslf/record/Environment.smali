.class public final Lorg/apache/poi/hslf/record/Environment;
.super Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;
.source "Environment.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private fontCollection:Lorg/apache/poi/hslf/record/FontCollection;

.field private txmaster:Lorg/apache/poi/hslf/record/TxMasterStyleAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0x3f2

    sput-wide v0, Lorg/apache/poi/hslf/record/Environment;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    .line 48
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 50
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_header:[B

    .line 51
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_header:[B

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    add-int/lit8 v1, p2, 0x8

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v1, v2}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 65
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->fontCollection:Lorg/apache/poi/hslf/record/FontCollection;

    if-nez v1, :cond_3

    .line 66
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Environment didn\'t contain a FontCollection record!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/FontCollection;

    if-eqz v1, :cond_2

    .line 59
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/FontCollection;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->fontCollection:Lorg/apache/poi/hslf/record/FontCollection;

    .line 57
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    iput-object v1, p0, Lorg/apache/poi/hslf/record/Environment;->txmaster:Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    goto :goto_1

    .line 68
    :cond_3
    return-void
.end method


# virtual methods
.method public getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Environment;->fontCollection:Lorg/apache/poi/hslf/record/FontCollection;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 77
    sget-wide v0, Lorg/apache/poi/hslf/record/Environment;->_type:J

    return-wide v0
.end method

.method public getTxMasterStyleAtom()Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Environment;->txmaster:Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Environment;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/Environment;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/Environment;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/Environment;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/Environment;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 85
    return-void
.end method
