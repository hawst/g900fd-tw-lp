.class public final Lorg/apache/poi/hslf/record/ExControlAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "ExControlAtom.java"


# instance fields
.field private _header:[B

.field private _id:I


# direct methods
.method protected constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 46
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 47
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_header:[B

    .line 49
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExControlAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_header:[B

    invoke-static {v0, v4, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 52
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 61
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 63
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_header:[B

    .line 64
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 66
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_id:I

    .line 67
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExControlAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSlideId()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_id:I

    return v0
.end method

.method public setSlideId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 92
    iput p1, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_id:I

    .line 93
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 112
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 113
    .local v0, "data":[B
    iget v1, p0, Lorg/apache/poi/hslf/record/ExControlAtom;->_id:I

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 114
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 115
    return-void
.end method
