.class public final Lorg/apache/poi/hslf/model/TitleMaster;
.super Lorg/apache/poi/hslf/model/MasterSheet;
.source "TitleMaster.java"


# instance fields
.field private _runs:[Lorg/apache/poi/hslf/model/TextRun;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/Slide;I)V
    .locals 2
    .param p1, "record"    # Lorg/apache/poi/hslf/record/Slide;
    .param p2, "sheetNo"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/MasterSheet;-><init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V

    .line 38
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TitleMaster;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/hslf/model/TitleMaster;->findTextRuns(Lorg/apache/poi/hslf/record/PPDrawing;)[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hslf/model/TitleMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    .line 39
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TitleMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 40
    return-void

    .line 39
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TitleMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lorg/apache/poi/hslf/model/TextRun;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;
    .locals 5

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TitleMaster;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;

    move-result-object v1

    .line 62
    .local v1, "master":[Lorg/apache/poi/hslf/model/SlideMaster;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TitleMaster;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/record/Slide;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v3

    .line 63
    .local v3, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/SlideAtom;->getMasterID()I

    move-result v2

    .line 64
    .local v2, "masterId":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-lt v0, v4, :cond_0

    .line 67
    const/4 v4, 0x0

    :goto_1
    return-object v4

    .line 65
    :cond_0
    aget-object v4, v1, v0

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/SlideMaster;->_getSheetNumber()I

    move-result v4

    if-ne v2, v4, :cond_1

    aget-object v4, v1, v0

    goto :goto_1

    .line 64
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 2
    .param p1, "txtype"    # I
    .param p2, "level"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "isCharacter"    # Z

    .prologue
    .line 53
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TitleMaster;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    .line 54
    .local v0, "master":Lorg/apache/poi/hslf/model/MasterSheet;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/poi/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    goto :goto_0
.end method

.method public getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TitleMaster;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    return-object v0
.end method
