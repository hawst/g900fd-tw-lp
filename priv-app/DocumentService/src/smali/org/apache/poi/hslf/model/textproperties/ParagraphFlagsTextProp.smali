.class public final Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;
.super Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
.source "ParagraphFlagsTextProp.java"


# static fields
.field public static final BULLET_HARDCOLOR_IDX:I = 0x2

.field public static final BULLET_HARDFONT_IDX:I = 0x1

.field public static final BULLET_HARDSIZE_IDX:I = 0x4

.field public static final BULLET_IDX:I

.field public static NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "paragraph_flags"

    sput-object v0, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 34
    const/16 v0, 0xf

    sget-object v1, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 35
    const-string/jumbo v4, "bullet"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 36
    const-string/jumbo v4, "bullet.hardfont"

    aput-object v4, v2, v3

    .line 37
    const-string/jumbo v3, "bullet.hardcolor"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    .line 38
    const-string/jumbo v4, "bullet.hardsize"

    aput-object v4, v2, v3

    invoke-direct {p0, v5, v0, v1, v2}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;-><init>(IILjava/lang/String;[Ljava/lang/String;)V

    .line 40
    return-void
.end method
