.class public final Lorg/apache/poi/hslf/model/Slide;
.super Lorg/apache/poi/hslf/model/Sheet;
.source "Slide.java"


# instance fields
.field private _atomSet:Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

.field private _notes:Lorg/apache/poi/hslf/model/Notes;

.field private _runs:[Lorg/apache/poi/hslf/model/TextRun;

.field private _slideNo:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "sheetNumber"    # I
    .param p2, "sheetRefId"    # I
    .param p3, "slideNumber"    # I

    .prologue
    .line 107
    new-instance v0, Lorg/apache/poi/hslf/record/Slide;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/Slide;-><init>()V

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/model/Sheet;-><init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V

    .line 108
    iput p3, p0, Lorg/apache/poi/hslf/model/Slide;->_slideNo:I

    .line 109
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/record/SheetContainer;->setSheetId(I)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/record/Slide;Lorg/apache/poi/hslf/model/Notes;Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;II)V
    .locals 6
    .param p1, "slide"    # Lorg/apache/poi/hslf/record/Slide;
    .param p2, "notes"    # Lorg/apache/poi/hslf/model/Notes;
    .param p3, "atomSet"    # Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .param p4, "slideIdentifier"    # I
    .param p5, "slideNumber"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p4}, Lorg/apache/poi/hslf/model/Sheet;-><init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V

    .line 68
    iput-object p2, p0, Lorg/apache/poi/hslf/model/Slide;->_notes:Lorg/apache/poi/hslf/model/Notes;

    .line 69
    iput-object p3, p0, Lorg/apache/poi/hslf/model/Slide;->_atomSet:Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 70
    iput p5, p0, Lorg/apache/poi/hslf/model/Slide;->_slideNo:I

    .line 73
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/poi/hslf/model/Slide;->findTextRuns(Lorg/apache/poi/hslf/record/PPDrawing;)[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 78
    .local v0, "_otherRuns":[Lorg/apache/poi/hslf/model/TextRun;
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 79
    .local v3, "textRuns":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/TextRun;>;"
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_atomSet:Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    if-eqz v4, :cond_0

    .line 80
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_atomSet:Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;->getSlideRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v4

    invoke-static {v4, v3}, Lorg/apache/poi/hslf/model/Slide;->findTextRuns([Lorg/apache/poi/hslf/record/Record;Ljava/util/List;)V

    .line 86
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    array-length v5, v0

    add-int/2addr v4, v5

    new-array v4, v4, [Lorg/apache/poi/hslf/model/TextRun;

    iput-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    .line 88
    const/4 v1, 0x0

    .line 89
    .local v1, "i":I
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 94
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_2

    .line 99
    return-void

    .line 90
    .end local v2    # "k":I
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hslf/model/TextRun;

    aput-object v4, v5, v1

    .line 91
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v4, v4, v1

    invoke-virtual {v4, p0}, Lorg/apache/poi/hslf/model/TextRun;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 95
    .restart local v2    # "k":I
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v5, v0, v2

    aput-object v5, v4, v1

    .line 96
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v4, v4, v1

    invoke-virtual {v4, p0}, Lorg/apache/poi/hslf/model/TextRun;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 97
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    aget-object v4, v4, v1

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lorg/apache/poi/hslf/model/TextRun;->setIndex(I)V

    .line 94
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addTitle()Lorg/apache/poi/hslf/model/TextBox;
    .locals 6

    .prologue
    .line 181
    new-instance v0, Lorg/apache/poi/hslf/model/Placeholder;

    invoke-direct {v0}, Lorg/apache/poi/hslf/model/Placeholder;-><init>()V

    .line 182
    .local v0, "pl":Lorg/apache/poi/hslf/model/Placeholder;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Placeholder;->setShapeType(I)V

    .line 183
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/Placeholder;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/model/TextRun;->setRunType(I)V

    .line 184
    const-string/jumbo v1, "Click to edit title"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Placeholder;->setText(Ljava/lang/String;)V

    .line 185
    new-instance v1, Lorg/apache/poi/java/awt/Rectangle;

    const/16 v2, 0x36

    const/16 v3, 0x30

    const/16 v4, 0x264

    const/16 v5, 0x5a

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Placeholder;->setAnchor(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 186
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/Slide;->addShape(Lorg/apache/poi/hslf/model/Shape;)V

    .line 187
    return-object v0
.end method

.method public draw(Lorg/apache/poi/java/awt/Graphics2D;)V
    .locals 5
    .param p1, "graphics"    # Lorg/apache/poi/java/awt/Graphics2D;

    .prologue
    .line 431
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v2

    .line 432
    .local v2, "master":Lorg/apache/poi/hslf/model/MasterSheet;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getBackground()Lorg/apache/poi/hslf/model/Background;

    move-result-object v0

    .line 433
    .local v0, "bg":Lorg/apache/poi/hslf/model/Background;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/model/Background;->draw(Lorg/apache/poi/java/awt/Graphics2D;)V

    .line 435
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getFollowMasterObjects()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 437
    const/4 v3, 0x0

    .line 438
    .local v3, "sh":[Lorg/apache/poi/hslf/model/Shape;
    if-eqz v2, :cond_1

    .line 439
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/MasterSheet;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v3

    .line 440
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-lt v1, v4, :cond_2

    .line 449
    .end local v1    # "i":I
    .end local v3    # "sh":[Lorg/apache/poi/hslf/model/Shape;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v3

    .line 450
    .restart local v3    # "sh":[Lorg/apache/poi/hslf/model/Shape;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    array-length v4, v3

    if-lt v1, v4, :cond_4

    .line 453
    return-void

    .line 441
    :cond_2
    aget-object v4, v3, v1

    invoke-static {v4}, Lorg/apache/poi/hslf/model/MasterSheet;->isPlaceholder(Lorg/apache/poi/hslf/model/Shape;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 440
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 444
    :cond_3
    aget-object v4, v3, v1

    invoke-virtual {v4, p1}, Lorg/apache/poi/hslf/model/Shape;->draw(Lorg/apache/poi/java/awt/Graphics2D;)V

    goto :goto_2

    .line 451
    :cond_4
    aget-object v4, v3, v1

    invoke-virtual {v4, p1}, Lorg/apache/poi/hslf/model/Shape;->draw(Lorg/apache/poi/java/awt/Graphics2D;)V

    .line 450
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getBackground()Lorg/apache/poi/hslf/model/Background;
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getFollowMasterBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/MasterSheet;->getBackground()Lorg/apache/poi/hslf/model/Background;

    move-result-object v0

    .line 361
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/poi/hslf/model/Sheet;->getBackground()Lorg/apache/poi/hslf/model/Background;

    move-result-object v0

    goto :goto_0
.end method

.method public getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .locals 1

    .prologue
    .line 368
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getFollowMasterScheme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/MasterSheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v0

    .line 374
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/poi/hslf/model/Sheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v0

    goto :goto_0
.end method

.method public getComments()[Lorg/apache/poi/hslf/model/Comment;
    .locals 10

    .prologue
    .line 387
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v6

    .line 388
    sget-object v7, Lorg/apache/poi/hslf/record/RecordTypes;->ProgTags:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v7, v7, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v8, v7

    .line 387
    invoke-virtual {v6, v8, v9}, Lorg/apache/poi/hslf/record/SheetContainer;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v5

    .line 386
    check-cast v5, Lorg/apache/poi/hslf/record/RecordContainer;

    .line 390
    .local v5, "progTags":Lorg/apache/poi/hslf/record/RecordContainer;
    if-eqz v5, :cond_4

    .line 393
    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->ProgBinaryTag:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v6

    .line 392
    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/hslf/record/RecordContainer;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v4

    .line 391
    check-cast v4, Lorg/apache/poi/hslf/record/RecordContainer;

    .line 395
    .local v4, "progBinaryTag":Lorg/apache/poi/hslf/record/RecordContainer;
    if-eqz v4, :cond_4

    .line 398
    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->BinaryTagData:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v6

    .line 397
    invoke-virtual {v4, v6, v7}, Lorg/apache/poi/hslf/record/RecordContainer;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 396
    check-cast v0, Lorg/apache/poi/hslf/record/RecordContainer;

    .line 400
    .local v0, "binaryTags":Lorg/apache/poi/hslf/record/RecordContainer;
    if-eqz v0, :cond_4

    .line 402
    const/4 v2, 0x0

    .line 403
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    array-length v6, v6

    if-lt v3, v6, :cond_0

    .line 410
    new-array v1, v2, [Lorg/apache/poi/hslf/model/Comment;

    .line 411
    .local v1, "comments":[Lorg/apache/poi/hslf/model/Comment;
    const/4 v2, 0x0

    .line 412
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    array-length v6, v6

    if-lt v3, v6, :cond_2

    .line 427
    .end local v0    # "binaryTags":Lorg/apache/poi/hslf/record/RecordContainer;
    .end local v1    # "comments":[Lorg/apache/poi/hslf/model/Comment;
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "progBinaryTag":Lorg/apache/poi/hslf/record/RecordContainer;
    :goto_2
    return-object v1

    .line 404
    .restart local v0    # "binaryTags":Lorg/apache/poi/hslf/record/RecordContainer;
    .restart local v2    # "count":I
    .restart local v3    # "i":I
    .restart local v4    # "progBinaryTag":Lorg/apache/poi/hslf/record/RecordContainer;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    aget-object v6, v6, v3

    instance-of v6, v6, Lorg/apache/poi/hslf/record/Comment2000;

    if-eqz v6, :cond_1

    .line 405
    add-int/lit8 v2, v2, 0x1

    .line 403
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 413
    .restart local v1    # "comments":[Lorg/apache/poi/hslf/model/Comment;
    :cond_2
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    aget-object v6, v6, v3

    instance-of v6, v6, Lorg/apache/poi/hslf/record/Comment2000;

    if-eqz v6, :cond_3

    .line 414
    new-instance v7, Lorg/apache/poi/hslf/model/Comment;

    .line 415
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    aget-object v6, v6, v3

    check-cast v6, Lorg/apache/poi/hslf/record/Comment2000;

    invoke-direct {v7, v6}, Lorg/apache/poi/hslf/model/Comment;-><init>(Lorg/apache/poi/hslf/record/Comment2000;)V

    .line 414
    aput-object v7, v1, v3

    .line 417
    add-int/lit8 v2, v2, 0x1

    .line 412
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 427
    .end local v0    # "binaryTags":Lorg/apache/poi/hslf/record/RecordContainer;
    .end local v1    # "comments":[Lorg/apache/poi/hslf/model/Comment;
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "progBinaryTag":Lorg/apache/poi/hslf/record/RecordContainer;
    :cond_4
    const/4 v6, 0x0

    new-array v1, v6, [Lorg/apache/poi/hslf/model/Comment;

    goto :goto_2
.end method

.method public getFollowMasterBackground()Z
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 304
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SlideAtom;->getFollowMasterBackground()Z

    move-result v1

    return v1
.end method

.method public getFollowMasterObjects()Z
    .locals 2

    .prologue
    .line 347
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 348
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SlideAtom;->getFollowMasterObjects()Z

    move-result v1

    return v1
.end method

.method public getFollowMasterScheme()Z
    .locals 2

    .prologue
    .line 325
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 326
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/SlideAtom;->getFollowMasterScheme()Z

    move-result v1

    return v1
.end method

.method public getHeadersFooters()Lorg/apache/poi/hslf/model/HeadersFooters;
    .locals 10

    .prologue
    .line 461
    const/4 v1, 0x0

    .line 462
    .local v1, "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/SheetContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 463
    .local v0, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v4, 0x0

    .line 464
    .local v4, "ppt2007":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    .line 471
    const/4 v3, 0x0

    .line 472
    .local v3, "newRecord":Z
    if-nez v1, :cond_3

    if-nez v4, :cond_3

    .line 473
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlideHeadersFooters()Lorg/apache/poi/hslf/model/HeadersFooters;

    move-result-object v5

    .line 479
    :goto_1
    return-object v5

    .line 465
    .end local v3    # "newRecord":Z
    :cond_0
    aget-object v5, v0, v2

    instance-of v5, v5, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    if-eqz v5, :cond_2

    .line 466
    aget-object v1, v0, v2

    .end local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    check-cast v1, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .line 464
    .restart local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 467
    :cond_2
    aget-object v5, v0, v2

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v6

    sget-object v5, Lorg/apache/poi/hslf/record/RecordTypes;->RoundTripContentMasterId:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v5, v5, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v8, v5

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 468
    const/4 v4, 0x1

    goto :goto_2

    .line 475
    .restart local v3    # "newRecord":Z
    :cond_3
    if-nez v1, :cond_4

    .line 476
    new-instance v1, Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .end local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    const/16 v5, 0x3f

    invoke-direct {v1, v5}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;-><init>(S)V

    .line 477
    .restart local v1    # "hdd":Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    const/4 v3, 0x1

    .line 479
    :cond_4
    new-instance v5, Lorg/apache/poi/hslf/model/HeadersFooters;

    invoke-direct {v5, v1, p0, v3, v4}, Lorg/apache/poi/hslf/model/HeadersFooters;-><init>(Lorg/apache/poi/hslf/record/HeadersFootersContainer;Lorg/apache/poi/hslf/model/Sheet;ZZ)V

    goto :goto_1
.end method

.method public getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;
    .locals 7

    .prologue
    .line 254
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;

    move-result-object v1

    .line 255
    .local v1, "master":[Lorg/apache/poi/hslf/model/SlideMaster;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v3

    .line 256
    .local v3, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/SlideAtom;->getMasterID()I

    move-result v2

    .line 257
    .local v2, "masterId":I
    const/4 v4, 0x0

    .line 258
    .local v4, "sheet":Lorg/apache/poi/hslf/model/MasterSheet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v1

    if-lt v0, v6, :cond_1

    .line 264
    :goto_1
    if-nez v4, :cond_0

    .line 265
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getTitleMasters()[Lorg/apache/poi/hslf/model/TitleMaster;

    move-result-object v5

    .line 266
    .local v5, "titleMaster":[Lorg/apache/poi/hslf/model/TitleMaster;
    if-eqz v5, :cond_0

    const/4 v0, 0x0

    :goto_2
    array-length v6, v5

    if-lt v0, v6, :cond_3

    .line 273
    .end local v5    # "titleMaster":[Lorg/apache/poi/hslf/model/TitleMaster;
    :cond_0
    :goto_3
    return-object v4

    .line 259
    :cond_1
    aget-object v6, v1, v0

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/SlideMaster;->_getSheetNumber()I

    move-result v6

    if-ne v2, v6, :cond_2

    .line 260
    aget-object v4, v1, v0

    .line 261
    goto :goto_1

    .line 258
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    .restart local v5    # "titleMaster":[Lorg/apache/poi/hslf/model/TitleMaster;
    :cond_3
    aget-object v6, v5, v0

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TitleMaster;->_getSheetNumber()I

    move-result v6

    if-ne v2, v6, :cond_4

    .line 268
    aget-object v4, v5, v0

    .line 269
    goto :goto_3

    .line 266
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public getNotesSheet()Lorg/apache/poi/hslf/model/Notes;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Slide;->_notes:Lorg/apache/poi/hslf/model/Notes;

    return-object v0
.end method

.method public getNumberedListInfo()[Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    .locals 1

    .prologue
    .line 496
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/PPDrawing;->getNumberedListInfo()[Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    move-result-object v0

    return-object v0
.end method

.method protected getSlideAtomsSet()Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Slide;->_atomSet:Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    return-object v0
.end method

.method public getSlideNumber()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lorg/apache/poi/hslf/model/Slide;->_slideNo:I

    return v0
.end method

.method public getSlideRecord()Lorg/apache/poi/hslf/record/Slide;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/Slide;

    return-object v0
.end method

.method public getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    return-object v0
.end method

.method public getTextboxWrappers()[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/PPDrawing;->getTextboxWrappers()[Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 5

    .prologue
    .line 205
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getTextRuns()[Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v2

    .line 206
    .local v2, "txt":[Lorg/apache/poi/hslf/model/TextRun;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_0

    .line 214
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 207
    :cond_0
    aget-object v4, v2, v0

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/TextRun;->getRunType()I

    move-result v3

    .line 208
    .local v3, "type":I
    const/4 v4, 0x6

    if-eq v3, v4, :cond_1

    .line 209
    if-nez v3, :cond_2

    .line 210
    :cond_1
    aget-object v4, v2, v0

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/TextRun;->getText()Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "title":Ljava/lang/String;
    goto :goto_1

    .line 206
    .end local v1    # "title":Ljava/lang/String;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onAddTextShape(Lorg/apache/poi/hslf/model/TextShape;)V
    .locals 5
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/TextShape;

    .prologue
    const/4 v4, 0x0

    .line 483
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/TextShape;->getTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    .line 485
    .local v0, "run":Lorg/apache/poi/hslf/model/TextRun;
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    if-nez v2, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/poi/hslf/model/TextRun;

    aput-object v0, v2, v4

    iput-object v2, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    .line 492
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lorg/apache/poi/hslf/model/TextRun;

    .line 488
    .local v1, "tmp":[Lorg/apache/poi/hslf/model/TextRun;
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    iget-object v3, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 489
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    .line 490
    iput-object v1, p0, Lorg/apache/poi/hslf/model/Slide;->_runs:[Lorg/apache/poi/hslf/model/TextRun;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, -0xff6

    .line 149
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/Document;->getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/PPDrawingGroup;->getEscherDggRecord()Lorg/apache/poi/ddf/EscherDggRecord;

    move-result-object v5

    .line 150
    .local v5, "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSheetContainer()Lorg/apache/poi/hslf/record/SheetContainer;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/SheetContainer;->getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/PPDrawing;->getEscherRecords()[Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v7

    aget-object v3, v7, v10

    check-cast v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 151
    .local v3, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v7, -0xff8

    invoke-static {v3, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherDgRecord;

    .line 152
    .local v2, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherDggRecord;->getMaxDrawingGroupId()I

    move-result v7

    add-int/lit8 v4, v7, 0x1

    .line 153
    .local v4, "dgId":I
    shl-int/lit8 v7, v4, 0x4

    int-to-short v7, v7

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    .line 154
    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherDggRecord;->getDrawingsSaved()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 155
    invoke-virtual {v5, v4}, Lorg/apache/poi/ddf/EscherDggRecord;->setMaxDrawingGroupId(I)V

    .line 157
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 172
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 173
    return-void

    .line 157
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 158
    .local v0, "c":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/4 v6, 0x0

    .line 159
    .local v6, "spr":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 168
    :goto_1
    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->allocateShapeId()I

    move-result v8

    invoke-virtual {v6, v8}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    goto :goto_0

    .line 161
    :pswitch_0
    invoke-virtual {v0, v10}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 162
    .local v1, "dc":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v1, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v6

    .end local v6    # "spr":Lorg/apache/poi/ddf/EscherSpRecord;
    check-cast v6, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 163
    .restart local v6    # "spr":Lorg/apache/poi/ddf/EscherSpRecord;
    goto :goto_1

    .line 165
    .end local v1    # "dc":Lorg/apache/poi/ddf/EscherContainerRecord;
    :pswitch_1
    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v6

    .end local v6    # "spr":Lorg/apache/poi/ddf/EscherSpRecord;
    check-cast v6, Lorg/apache/poi/ddf/EscherSpRecord;

    .restart local v6    # "spr":Lorg/apache/poi/ddf/EscherSpRecord;
    goto :goto_1

    .line 159
    :pswitch_data_0
    .packed-switch -0xffd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setFollowMasterBackground(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 292
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 293
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/SlideAtom;->setFollowMasterBackground(Z)V

    .line 294
    return-void
.end method

.method public setFollowMasterObjects(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 314
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 315
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/SlideAtom;->setFollowMasterObjects(Z)V

    .line 316
    return-void
.end method

.method public setFollowMasterScheme(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 336
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 337
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/SlideAtom;->setFollowMasterScheme(Z)V

    .line 338
    return-void
.end method

.method public setMasterSheet(Lorg/apache/poi/hslf/model/MasterSheet;)V
    .locals 3
    .param p1, "master"    # Lorg/apache/poi/hslf/model/MasterSheet;

    .prologue
    .line 280
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 281
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/MasterSheet;->_getSheetNumber()I

    move-result v1

    .line 282
    .local v1, "sheetNo":I
    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/SlideAtom;->setMasterID(I)V

    .line 283
    return-void
.end method

.method public setNotes(Lorg/apache/poi/hslf/model/Notes;)V
    .locals 2
    .param p1, "notes"    # Lorg/apache/poi/hslf/model/Notes;

    .prologue
    .line 117
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Slide;->_notes:Lorg/apache/poi/hslf/model/Notes;

    .line 120
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Slide;->getSlideRecord()Lorg/apache/poi/hslf/record/Slide;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Slide;->getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;

    move-result-object v0

    .line 122
    .local v0, "sa":Lorg/apache/poi/hslf/record/SlideAtom;
    if-nez p1, :cond_0

    .line 124
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/SlideAtom;->setNotesID(I)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Notes;->_getSheetNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/SlideAtom;->setNotesID(I)V

    goto :goto_0
.end method

.method public setSlideNumber(I)V
    .locals 0
    .param p1, "newSlideNumber"    # I

    .prologue
    .line 136
    iput p1, p0, Lorg/apache/poi/hslf/model/Slide;->_slideNo:I

    .line 137
    return-void
.end method
