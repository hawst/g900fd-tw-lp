.class public final Lorg/apache/poi/hslf/record/ColorSchemeAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "ColorSchemeAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private accentAndFollowingHyperlinkColourRGB:I

.field private accentAndHyperlinkColourRGB:I

.field private accentColourRGB:I

.field private backgroundColourRGB:I

.field private fillsColourRGB:I

.field private shadowsColourRGB:I

.field private textAndLinesColourRGB:I

.field private titleTextColourRGB:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/16 v0, 0x7f0

    sput-wide v0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 122
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 123
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    .line 124
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    const/16 v1, 0x10

    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 125
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 126
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    const/4 v1, 0x4

    const/16 v2, 0x20

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 129
    const v0, 0xffffff

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->backgroundColourRGB:I

    .line 130
    iput v4, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->textAndLinesColourRGB:I

    .line 131
    const v0, 0x808080

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->shadowsColourRGB:I

    .line 132
    iput v4, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->titleTextColourRGB:I

    .line 133
    const v0, 0x99cc00

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->fillsColourRGB:I

    .line 134
    const v0, 0xcc3333

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentColourRGB:I

    .line 135
    const v0, 0xffcccc

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndHyperlinkColourRGB:I

    .line 136
    const v0, 0xb2b2b2

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndFollowingHyperlinkColourRGB:I

    .line 137
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x28

    const/16 v2, 0x8

    .line 95
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 97
    if-ge p3, v1, :cond_0

    .line 98
    const/16 p3, 0x28

    .line 99
    array-length v0, p1

    sub-int/2addr v0, p2

    if-ge v0, v1, :cond_0

    .line 100
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Not enough data to form a ColorSchemeAtom (always 40 bytes long) - found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    sub-int/2addr v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    .line 106
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 109
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->backgroundColourRGB:I

    .line 110
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->textAndLinesColourRGB:I

    .line 111
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->shadowsColourRGB:I

    .line 112
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->titleTextColourRGB:I

    .line 113
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->fillsColourRGB:I

    .line 114
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentColourRGB:I

    .line 115
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndHyperlinkColourRGB:I

    .line 116
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x1c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndFollowingHyperlinkColourRGB:I

    .line 117
    return-void
.end method

.method public static joinRGB(BBB)I
    .locals 2
    .param p0, "r"    # B
    .param p1, "g"    # B
    .param p2, "b"    # B

    .prologue
    .line 170
    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p0, v0, v1

    const/4 v1, 0x1

    aput-byte p1, v0, v1

    const/4 v1, 0x2

    aput-byte p2, v0, v1

    invoke-static {v0}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->joinRGB([B)I

    move-result v0

    return v0
.end method

.method public static joinRGB([B)I
    .locals 5
    .param p0, "rgb"    # [B

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 176
    array-length v2, p0

    if-eq v2, v4, :cond_0

    .line 177
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "joinRGB accepts a byte array of 3 values, but got one of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, p0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " values!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 179
    :cond_0
    const/4 v2, 0x4

    new-array v1, v2, [B

    .line 180
    .local v1, "with_zero":[B
    invoke-static {p0, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 181
    aput-byte v3, v1, v4

    .line 182
    invoke-static {v1, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 183
    .local v0, "ret":I
    return v0
.end method

.method public static splitRGB(I)[B
    .locals 6
    .param p0, "rgb"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 150
    new-array v3, v5, [B

    .line 153
    .local v3, "ret":[B
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 155
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-static {p0, v1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 161
    .local v0, "b":[B
    invoke-static {v0, v4, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    return-object v3

    .line 156
    .end local v0    # "b":[B
    :catch_0
    move-exception v2

    .line 158
    .local v2, "ie":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method public getAccentAndFollowingHyperlinkColourRGB()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndFollowingHyperlinkColourRGB:I

    return v0
.end method

.method public getAccentAndHyperlinkColourRGB()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndHyperlinkColourRGB:I

    return v0
.end method

.method public getAccentColourRGB()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentColourRGB:I

    return v0
.end method

.method public getBackgroundColourRGB()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->backgroundColourRGB:I

    return v0
.end method

.method public getColor(I)I
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 213
    const/16 v1, 0x8

    new-array v0, v1, [I

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->backgroundColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->textAndLinesColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->shadowsColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->titleTextColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x4

    .line 214
    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->fillsColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndHyperlinkColourRGB:I

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndFollowingHyperlinkColourRGB:I

    aput v2, v0, v1

    .line 215
    .local v0, "clr":[I
    aget v1, v0, p1

    return v1
.end method

.method public getFillsColourRGB()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->fillsColourRGB:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 143
    sget-wide v0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_type:J

    return-wide v0
.end method

.method public getShadowsColourRGB()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->shadowsColourRGB:I

    return v0
.end method

.method public getTextAndLinesColourRGB()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->textAndLinesColourRGB:I

    return v0
.end method

.method public getTitleTextColourRGB()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->titleTextColourRGB:I

    return v0
.end method

.method public setAccentAndFollowingHyperlinkColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 88
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndFollowingHyperlinkColourRGB:I

    return-void
.end method

.method public setAccentAndHyperlinkColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 81
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndHyperlinkColourRGB:I

    return-void
.end method

.method public setAccentColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 74
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentColourRGB:I

    return-void
.end method

.method public setBackgroundColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 49
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->backgroundColourRGB:I

    return-void
.end method

.method public setFillsColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 69
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->fillsColourRGB:I

    return-void
.end method

.method public setShadowsColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 59
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->shadowsColourRGB:I

    return-void
.end method

.method public setTextAndLinesColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 54
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->textAndLinesColourRGB:I

    return-void
.end method

.method public setTitleTextColourRGB(I)V
    .locals 0
    .param p1, "rgb"    # I

    .prologue
    .line 64
    iput p1, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->titleTextColourRGB:I

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 196
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->backgroundColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 197
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->textAndLinesColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 198
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->shadowsColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 199
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->titleTextColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 200
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->fillsColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 201
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 202
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndHyperlinkColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 203
    iget v0, p0, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->accentAndFollowingHyperlinkColourRGB:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 204
    return-void
.end method
