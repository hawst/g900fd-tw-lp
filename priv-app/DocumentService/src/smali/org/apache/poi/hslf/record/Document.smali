.class public final Lorg/apache/poi/hslf/record/Document;
.super Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;
.source "Document.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private documentAtom:Lorg/apache/poi/hslf/record/DocumentAtom;

.field private environment:Lorg/apache/poi/hslf/record/Environment;

.field private exObjList:Lorg/apache/poi/hslf/record/ExObjList;

.field private ppDrawing:Lorg/apache/poi/hslf/record/PPDrawingGroup;

.field private slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lorg/apache/poi/hslf/record/Document;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 6
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 116
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 118
    new-array v2, v3, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_header:[B

    .line 119
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_header:[B

    invoke-static {p1, p2, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    add-int/lit8 v2, p2, 0x8

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v2, v3}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 125
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v4

    instance-of v2, v2, Lorg/apache/poi/hslf/record/DocumentAtom;

    if-nez v2, :cond_0

    .line 126
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "The first child of a Document must be a DocumentAtom"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 128
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v4

    check-cast v2, Lorg/apache/poi/hslf/record/DocumentAtom;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->documentAtom:Lorg/apache/poi/hslf/record/DocumentAtom;

    .line 133
    const/4 v1, 0x0

    .line 134
    .local v1, "slwtcount":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_3

    .line 152
    if-nez v1, :cond_1

    .line 153
    sget-object v2, Lorg/apache/poi/hslf/record/Document;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v3, "No SlideListWithText\'s found - there should normally be at least one!"

    invoke-virtual {v2, v5, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 155
    :cond_1
    const/4 v2, 0x3

    if-le v1, v2, :cond_2

    .line 156
    sget-object v2, Lorg/apache/poi/hslf/record/Document;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " SlideListWithTexts - normally there should only be three!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v5, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 160
    :cond_2
    new-array v2, v1, [Lorg/apache/poi/hslf/record/SlideListWithText;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    .line 161
    const/4 v1, 0x0

    .line 162
    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_8

    .line 168
    return-void

    .line 135
    :cond_3
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/SlideListWithText;

    if-eqz v2, :cond_4

    .line 136
    add-int/lit8 v1, v1, 0x1

    .line 138
    :cond_4
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/Environment;

    if-eqz v2, :cond_5

    .line 139
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/poi/hslf/record/Environment;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->environment:Lorg/apache/poi/hslf/record/Environment;

    .line 141
    :cond_5
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/PPDrawingGroup;

    if-eqz v2, :cond_6

    .line 142
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/poi/hslf/record/PPDrawingGroup;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawingGroup;

    .line 144
    :cond_6
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/ExObjList;

    if-eqz v2, :cond_7

    .line 145
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/poi/hslf/record/ExObjList;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->exObjList:Lorg/apache/poi/hslf/record/ExObjList;

    .line 134
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_8
    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/poi/hslf/record/SlideListWithText;

    if-eqz v2, :cond_9

    .line 164
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    iget-object v2, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/poi/hslf/record/SlideListWithText;

    aput-object v2, v3, v1

    .line 165
    add-int/lit8 v1, v1, 0x1

    .line 162
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addSlideListWithText(Lorg/apache/poi/hslf/record/SlideListWithText;)V
    .locals 9
    .param p1, "slwt"    # Lorg/apache/poi/hslf/record/SlideListWithText;

    .prologue
    const/4 v8, 0x0

    .line 177
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    iget-object v4, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v3, v4

    .line 178
    .local v0, "endDoc":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    sget-object v3, Lorg/apache/poi/hslf/record/RecordTypes;->EndDocument:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v3, v3, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 179
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "The last child record of a Document should be EndDocument, but it was "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 183
    :cond_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hslf/record/Document;->addChildBefore(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 186
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    array-length v3, v3

    add-int/lit8 v1, v3, 0x1

    .line 187
    .local v1, "newSize":I
    new-array v2, v1, [Lorg/apache/poi/hslf/record/SlideListWithText;

    .line 188
    .local v2, "nl":[Lorg/apache/poi/hslf/record/SlideListWithText;
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    iget-object v4, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    array-length v4, v4

    invoke-static {v3, v8, v2, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 189
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aput-object p1, v2, v3

    .line 190
    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    .line 191
    return-void
.end method

.method public getDocumentAtom()Lorg/apache/poi/hslf/record/DocumentAtom;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->documentAtom:Lorg/apache/poi/hslf/record/DocumentAtom;

    return-object v0
.end method

.method public getEnvironment()Lorg/apache/poi/hslf/record/Environment;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->environment:Lorg/apache/poi/hslf/record/Environment;

    return-object v0
.end method

.method public getExObjList()Lorg/apache/poi/hslf/record/ExObjList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->exObjList:Lorg/apache/poi/hslf/record/ExObjList;

    return-object v0
.end method

.method public getMasterSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;
    .locals 3

    .prologue
    .line 79
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 84
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 80
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/SlideListWithText;->getInstance()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 81
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    goto :goto_1

    .line 79
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getNotesSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;
    .locals 3

    .prologue
    .line 104
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 109
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 105
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/SlideListWithText;->getInstance()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 106
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    goto :goto_1

    .line 104
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawingGroup;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 207
    sget-wide v0, Lorg/apache/poi/hslf/record/Document;->_type:J

    return-wide v0
.end method

.method public getSlideListWithTexts()[Lorg/apache/poi/hslf/record/SlideListWithText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    return-object v0
.end method

.method public getSlideSlideListWithText()Lorg/apache/poi/hslf/record/SlideListWithText;
    .locals 2

    .prologue
    .line 92
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 97
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 93
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/SlideListWithText;->getInstance()I

    move-result v1

    if-nez v1, :cond_1

    .line 94
    iget-object v1, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    goto :goto_1

    .line 92
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public removeSlideListWithText(Lorg/apache/poi/hslf/record/SlideListWithText;)V
    .locals 5
    .param p1, "slwt"    # Lorg/apache/poi/hslf/record/SlideListWithText;

    .prologue
    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v0, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/SlideListWithText;>;"
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 201
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/poi/hslf/record/SlideListWithText;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hslf/record/SlideListWithText;

    iput-object v2, p0, Lorg/apache/poi/hslf/record/Document;->slwts:[Lorg/apache/poi/hslf/record/SlideListWithText;

    .line 202
    return-void

    .line 195
    :cond_0
    aget-object v1, v3, v2

    .line 196
    .local v1, "s":Lorg/apache/poi/hslf/record/SlideListWithText;
    if-eq v1, p1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/record/Document;->removeChild(Lorg/apache/poi/hslf/record/Record;)Lorg/apache/poi/hslf/record/Record;

    goto :goto_1
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/Document;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/Document;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/Document;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/Document;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 215
    return-void
.end method
