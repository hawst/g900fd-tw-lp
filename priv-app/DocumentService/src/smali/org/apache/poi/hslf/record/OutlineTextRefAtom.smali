.class public final Lorg/apache/poi/hslf/record/OutlineTextRefAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "OutlineTextRefAtom.java"


# instance fields
.field private _header:[B

.field private _index:I


# direct methods
.method protected constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 66
    iput v1, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_index:I

    .line 68
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    .line 69
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    invoke-static {v0, v1, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 70
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 71
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    invoke-static {v0, v4, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 72
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 55
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    .line 56
    iget-object v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_index:I

    .line 60
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->OutlineTextRefAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getTextIndex()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_index:I

    return v0
.end method

.method public setTextIndex(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 96
    iput p1, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_index:I

    .line 97
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 84
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 85
    .local v0, "recdata":[B
    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/poi/hslf/record/OutlineTextRefAtom;->_index:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 86
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 87
    return-void
.end method
