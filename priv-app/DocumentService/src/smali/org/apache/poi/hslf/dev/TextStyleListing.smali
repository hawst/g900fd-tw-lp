.class public final Lorg/apache/poi/hslf/dev/TextStyleListing;
.super Ljava/lang/Object;
.source "TextStyleListing.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 14
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 34
    array-length v10, p0

    if-ge v10, v12, :cond_0

    .line 35
    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v11, "Need to give a filename"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 36
    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 39
    :cond_0
    new-instance v8, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-direct {v8, v10}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 42
    .local v8, "ss":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v8}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    .line 43
    .local v6, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v10, v6

    if-lt v2, v10, :cond_1

    .line 72
    return-void

    .line 44
    :cond_1
    aget-object v10, v6, v2

    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    cmp-long v10, v10, v12

    if-nez v10, :cond_2

    .line 45
    aget-object v1, v6, v2

    .line 46
    .local v1, "docRecord":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 47
    .local v0, "docChildren":[Lorg/apache/poi/hslf/record/Record;
    if-eqz v0, :cond_2

    .line 48
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v10, v0

    if-lt v3, v10, :cond_3

    .line 43
    .end local v0    # "docChildren":[Lorg/apache/poi/hslf/record/Record;
    .end local v1    # "docRecord":Lorg/apache/poi/hslf/record/Record;
    .end local v3    # "j":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 49
    .restart local v0    # "docChildren":[Lorg/apache/poi/hslf/record/Record;
    .restart local v1    # "docRecord":Lorg/apache/poi/hslf/record/Record;
    .restart local v3    # "j":I
    :cond_3
    aget-object v10, v0, v3

    instance-of v10, v10, Lorg/apache/poi/hslf/record/SlideListWithText;

    if-eqz v10, :cond_4

    .line 50
    aget-object v10, v0, v3

    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    .line 52
    .local v7, "slwtChildren":[Lorg/apache/poi/hslf/record/Record;
    const/4 v5, -0x1

    .line 53
    .local v5, "lastTextLen":I
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    array-length v10, v7

    if-lt v4, v10, :cond_5

    .line 48
    .end local v4    # "k":I
    .end local v5    # "lastTextLen":I
    .end local v7    # "slwtChildren":[Lorg/apache/poi/hslf/record/Record;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 54
    .restart local v4    # "k":I
    .restart local v5    # "lastTextLen":I
    .restart local v7    # "slwtChildren":[Lorg/apache/poi/hslf/record/Record;
    :cond_5
    aget-object v10, v7, v4

    instance-of v10, v10, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v10, :cond_6

    .line 55
    aget-object v10, v7, v4

    check-cast v10, Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/TextCharsAtom;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    .line 57
    :cond_6
    aget-object v10, v7, v4

    instance-of v10, v10, Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v10, :cond_7

    .line 58
    aget-object v10, v7, v4

    check-cast v10, Lorg/apache/poi/hslf/record/TextBytesAtom;

    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/TextBytesAtom;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    .line 61
    :cond_7
    aget-object v10, v7, v4

    instance-of v10, v10, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v10, :cond_8

    .line 62
    aget-object v9, v7, v4

    check-cast v9, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 63
    .local v9, "stpa":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    invoke-virtual {v9, v5}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->setParentTextSize(I)V

    .line 64
    invoke-static {v9}, Lorg/apache/poi/hslf/dev/TextStyleListing;->showStyleTextPropAtom(Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    .line 53
    .end local v9    # "stpa":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public static showStyleTextPropAtom(Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V
    .locals 5
    .param p0, "stpa"    # Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .prologue
    .line 77
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    move-result-object v2

    .line 79
    .local v2, "paragraphStyles":Ljava/util/LinkedList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 86
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    move-result-object v0

    .line 88
    .local v0, "charStyles":Ljava/util/LinkedList;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 94
    return-void

    .line 80
    .end local v0    # "charStyles":Ljava/util/LinkedList;
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 83
    .local v3, "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-static {v3}, Lorg/apache/poi/hslf/dev/TextStyleListing;->showTextProps(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;)V

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v3    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .restart local v0    # "charStyles":Ljava/util/LinkedList;
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 92
    .restart local v3    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-static {v3}, Lorg/apache/poi/hslf/dev/TextStyleListing;->showTextProps(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;)V

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static showTextProps(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;)V
    .locals 8
    .param p0, "tpc"    # Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .prologue
    .line 97
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getTextPropList()Ljava/util/LinkedList;

    move-result-object v5

    .line 99
    .local v5, "textProps":Ljava/util/LinkedList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-lt v1, v7, :cond_0

    .line 115
    return-void

    .line 100
    :cond_0
    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/hslf/model/textproperties/TextProp;

    .line 105
    .local v6, "tp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    instance-of v7, v6, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    if-eqz v7, :cond_1

    move-object v0, v6

    .line 106
    check-cast v0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    .line 107
    .local v0, "bmtp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->getSubPropNames()[Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "subPropNames":[Ljava/lang/String;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->getSubPropMatches()[Z

    move-result-object v3

    .line 109
    .local v3, "subPropMatches":[Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v7, v4

    if-lt v2, v7, :cond_2

    .line 99
    .end local v0    # "bmtp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    .end local v2    # "j":I
    .end local v3    # "subPropMatches":[Z
    .end local v4    # "subPropNames":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    .restart local v0    # "bmtp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    .restart local v2    # "j":I
    .restart local v3    # "subPropMatches":[Z
    .restart local v4    # "subPropNames":[Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
