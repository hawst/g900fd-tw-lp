.class public abstract Lorg/apache/poi/hslf/usermodel/PictureData;
.super Ljava/lang/Object;
.source "PictureData.java"


# static fields
.field protected static final CHECKSUM_SIZE:I = 0x10

.field protected static painters:[Lorg/apache/poi/hslf/blip/ImagePainter;


# instance fields
.field protected logger:Lorg/apache/poi/util/POILogger;

.field protected offset:I

.field private rawdata:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/poi/hslf/blip/ImagePainter;

    sput-object v0, Lorg/apache/poi/hslf/usermodel/PictureData;->painters:[Lorg/apache/poi/hslf/blip/ImagePainter;

    .line 82
    const/4 v0, 0x6

    new-instance v1, Lorg/apache/poi/hslf/blip/BitmapPainter;

    invoke-direct {v1}, Lorg/apache/poi/hslf/blip/BitmapPainter;-><init>()V

    invoke-static {v0, v1}, Lorg/apache/poi/hslf/usermodel/PictureData;->setImagePainter(ILorg/apache/poi/hslf/blip/ImagePainter;)V

    .line 83
    const/4 v0, 0x5

    new-instance v1, Lorg/apache/poi/hslf/blip/BitmapPainter;

    invoke-direct {v1}, Lorg/apache/poi/hslf/blip/BitmapPainter;-><init>()V

    invoke-static {v0, v1}, Lorg/apache/poi/hslf/usermodel/PictureData;->setImagePainter(ILorg/apache/poi/hslf/blip/ImagePainter;)V

    .line 84
    const/4 v0, 0x7

    new-instance v1, Lorg/apache/poi/hslf/blip/BitmapPainter;

    invoke-direct {v1}, Lorg/apache/poi/hslf/blip/BitmapPainter;-><init>()V

    invoke-static {v0, v1}, Lorg/apache/poi/hslf/usermodel/PictureData;->setImagePainter(ILorg/apache/poi/hslf/blip/ImagePainter;)V

    .line 85
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->logger:Lorg/apache/poi/util/POILogger;

    .line 38
    return-void
.end method

.method public static create(I)Lorg/apache/poi/hslf/usermodel/PictureData;
    .locals 4
    .param p0, "type"    # I

    .prologue
    .line 176
    packed-switch p0, :pswitch_data_0

    .line 196
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unsupported picture type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 178
    :pswitch_0
    new-instance v0, Lorg/apache/poi/hslf/blip/EMF;

    invoke-direct {v0}, Lorg/apache/poi/hslf/blip/EMF;-><init>()V

    .line 198
    .local v0, "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :goto_0
    return-object v0

    .line 181
    .end local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :pswitch_1
    new-instance v0, Lorg/apache/poi/hslf/blip/WMF;

    invoke-direct {v0}, Lorg/apache/poi/hslf/blip/WMF;-><init>()V

    .line 182
    .restart local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    goto :goto_0

    .line 184
    .end local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :pswitch_2
    new-instance v0, Lorg/apache/poi/hslf/blip/PICT;

    invoke-direct {v0}, Lorg/apache/poi/hslf/blip/PICT;-><init>()V

    .line 185
    .restart local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    goto :goto_0

    .line 187
    .end local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :pswitch_3
    new-instance v0, Lorg/apache/poi/hslf/blip/JPEG;

    invoke-direct {v0}, Lorg/apache/poi/hslf/blip/JPEG;-><init>()V

    .line 188
    .restart local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    goto :goto_0

    .line 190
    .end local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :pswitch_4
    new-instance v0, Lorg/apache/poi/hslf/blip/PNG;

    invoke-direct {v0}, Lorg/apache/poi/hslf/blip/PNG;-><init>()V

    .line 191
    .restart local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    goto :goto_0

    .line 193
    .end local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    :pswitch_5
    new-instance v0, Lorg/apache/poi/hslf/blip/DIB;

    invoke-direct {v0}, Lorg/apache/poi/hslf/blip/DIB;-><init>()V

    .line 194
    .restart local v0    # "pict":Lorg/apache/poi/hslf/usermodel/PictureData;
    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getChecksum([B)[B
    .locals 4
    .param p0, "data"    # [B

    .prologue
    .line 136
    :try_start_0
    const-string/jumbo v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 140
    .local v1, "sha":Ljava/security/MessageDigest;
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 141
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    return-object v2

    .line 137
    .end local v1    # "sha":Ljava/security/MessageDigest;
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static getImagePainter(I)Lorg/apache/poi/hslf/blip/ImagePainter;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 251
    sget-object v0, Lorg/apache/poi/hslf/usermodel/PictureData;->painters:[Lorg/apache/poi/hslf/blip/ImagePainter;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static setImagePainter(ILorg/apache/poi/hslf/blip/ImagePainter;)V
    .locals 1
    .param p0, "type"    # I
    .param p1, "painter"    # Lorg/apache/poi/hslf/blip/ImagePainter;

    .prologue
    .line 241
    sget-object v0, Lorg/apache/poi/hslf/usermodel/PictureData;->painters:[Lorg/apache/poi/hslf/blip/ImagePainter;

    aput-object p1, v0, p0

    .line 242
    return-void
.end method


# virtual methods
.method public draw(Lorg/apache/poi/java/awt/Graphics2D;Lorg/apache/poi/hslf/model/Picture;)V
    .locals 5
    .param p1, "graphics"    # Lorg/apache/poi/java/awt/Graphics2D;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Picture;

    .prologue
    .line 229
    sget-object v1, Lorg/apache/poi/hslf/usermodel/PictureData;->painters:[Lorg/apache/poi/hslf/blip/ImagePainter;

    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getType()I

    move-result v2

    aget-object v0, v1, v2

    .line 230
    .local v0, "painter":Lorg/apache/poi/hslf/blip/ImagePainter;
    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p0, p2}, Lorg/apache/poi/hslf/blip/ImagePainter;->paint(Lorg/apache/poi/java/awt/Graphics2D;Lorg/apache/poi/hslf/usermodel/PictureData;Lorg/apache/poi/hslf/model/Picture;)V

    .line 232
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x5

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Rendering is not supported: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public abstract getData()[B
.end method

.method public getHeader()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 211
    const/16 v1, 0x18

    new-array v0, v1, [B

    .line 212
    .local v0, "header":[B
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getSignature()I

    move-result v1

    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 213
    const/4 v1, 0x4

    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getRawData()[B

    move-result-object v2

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 214
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->rawdata:[B

    const/16 v2, 0x8

    const/16 v3, 0x10

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->offset:I

    return v0
.end method

.method public getRawData()[B
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->rawdata:[B

    return-object v0
.end method

.method protected abstract getSignature()I
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getData()[B

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public abstract getType()I
.end method

.method public getUID()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 124
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 125
    .local v0, "uid":[B
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->rawdata:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    return-object v0
.end method

.method public abstract setData([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 117
    iput p1, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->offset:I

    .line 118
    return-void
.end method

.method public setRawData([B)V
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 98
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/PictureData;->rawdata:[B

    .line 99
    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 5
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 150
    new-array v0, v3, [B

    .line 151
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getSignature()I

    move-result v2

    invoke-static {v0, v4, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 152
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 154
    new-array v0, v3, [B

    .line 155
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getType()I

    move-result v2

    const v3, 0xf018

    add-int/2addr v2, v3

    invoke-static {v0, v4, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 156
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 158
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/PictureData;->getRawData()[B

    move-result-object v1

    .line 160
    .local v1, "rawdata":[B
    const/4 v2, 0x4

    new-array v0, v2, [B

    .line 161
    array-length v2, v1

    invoke-static {v0, v4, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 162
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 164
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 165
    return-void
.end method
