.class public Lorg/apache/poi/hslf/model/TextBox;
.super Lorg/apache/poi/hslf/model/TextShape;
.source "TextBox.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/TextBox;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 60
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/TextShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/TextShape;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 52
    return-void
.end method


# virtual methods
.method protected createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 2
    .param p1, "isChild"    # Z

    .prologue
    .line 68
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/TextShape;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/TextBox;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 70
    const/16 v0, 0xca

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/TextBox;->setShapeType(I)V

    .line 73
    const/16 v0, 0x181

    const v1, 0x8000004

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 74
    const/16 v0, 0x183

    const/high16 v1, 0x8000000

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 75
    const/16 v0, 0x1bf

    const/high16 v1, 0x100000

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 76
    const/16 v0, 0x1c0

    const v1, 0x8000001

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 77
    const/16 v0, 0x1ff

    const/high16 v1, 0x80000

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 78
    const/16 v0, 0x201

    const v1, 0x8000002

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 80
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextBox;->createTextRun()Lorg/apache/poi/hslf/model/TextRun;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/TextBox;->_txtrun:Lorg/apache/poi/hslf/model/TextRun;

    .line 82
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextBox;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method protected setDefaultTextProperties(Lorg/apache/poi/hslf/model/TextRun;)V
    .locals 2
    .param p1, "_txtrun"    # Lorg/apache/poi/hslf/model/TextRun;

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/TextBox;->setVerticalAlignment(I)V

    .line 87
    const/16 v0, 0xbf

    const v1, 0x20002

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/TextBox;->setEscherProperty(SI)V

    .line 88
    return-void
.end method
