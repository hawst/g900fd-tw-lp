.class public final Lorg/apache/poi/hslf/model/AutoShapes;
.super Ljava/lang/Object;
.source "AutoShapes.java"


# static fields
.field protected static shapes:[Lorg/apache/poi/hslf/model/ShapeOutline;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/16 v0, 0xff

    new-array v0, v0, [Lorg/apache/poi/hslf/model/ShapeOutline;

    sput-object v0, Lorg/apache/poi/hslf/model/AutoShapes;->shapes:[Lorg/apache/poi/hslf/model/ShapeOutline;

    .line 49
    sget-object v0, Lorg/apache/poi/hslf/model/AutoShapes;->shapes:[Lorg/apache/poi/hslf/model/ShapeOutline;

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/poi/hslf/model/AutoShapes$1;

    invoke-direct {v2}, Lorg/apache/poi/hslf/model/AutoShapes$1;-><init>()V

    aput-object v2, v0, v1

    .line 55
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getShapeOutline(I)Lorg/apache/poi/hslf/model/ShapeOutline;
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 42
    sget-object v1, Lorg/apache/poi/hslf/model/AutoShapes;->shapes:[Lorg/apache/poi/hslf/model/ShapeOutline;

    aget-object v0, v1, p0

    .line 43
    .local v0, "outline":Lorg/apache/poi/hslf/model/ShapeOutline;
    return-object v0
.end method
