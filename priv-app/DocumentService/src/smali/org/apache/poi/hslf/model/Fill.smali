.class public final Lorg/apache/poi/hslf/model/Fill;
.super Ljava/lang/Object;
.source "Fill.java"


# static fields
.field public static final FILL_BACKGROUND:I = 0x9

.field public static final FILL_PATTERN:I = 0x1

.field public static final FILL_PICTURE:I = 0x3

.field public static final FILL_SHADE:I = 0x4

.field public static final FILL_SHADE_CENTER:I = 0x5

.field public static final FILL_SHADE_SCALE:I = 0x7

.field public static final FILL_SHADE_SHAPE:I = 0x6

.field public static final FILL_SHADE_TITLE:I = 0x8

.field public static final FILL_SOLID:I = 0x0

.field public static final FILL_TEXTURE:I = 0x2

.field public static final PROPERTY_NOSOLIDFILL_LINEFILL:I = 0x100000

.field public static final PROPERTY_NOSOLIDFILL_NOLINEFILL:I = 0x110001

.field public static final PROPERTY_SOLIDFILL_LINEFILL:I = 0x100010


# instance fields
.field protected logger:Lorg/apache/poi/util/POILogger;

.field protected shape:Lorg/apache/poi/hslf/model/Shape;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Fill;->logger:Lorg/apache/poi/util/POILogger;

    .line 110
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    .line 111
    return-void
.end method


# virtual methods
.method protected afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 6
    .param p1, "sh"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 128
    iget-object v4, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v4

    const/16 v5, -0xff5

    invoke-static {v4, v5}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 129
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v4, 0x186

    invoke-static {v2, v4}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 130
    .local v3, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v3, :cond_0

    .line 131
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    .line 132
    .local v1, "idx":I
    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/Fill;->getEscherBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v0

    .line 134
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getRef()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 137
    .end local v0    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    .end local v1    # "idx":I
    :cond_0
    return-void
.end method

.method public getBackgroundColor()Lorg/apache/poi/java/awt/Color;
    .locals 6

    .prologue
    .line 212
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 213
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x1bf

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 215
    .local v1, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 217
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    const/16 v3, 0x183

    const/16 v4, 0x182

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/poi/hslf/model/Shape;->getColor(SSI)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    goto :goto_0
.end method

.method protected getEscherBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;
    .locals 10
    .param p1, "idx"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 140
    iget-object v7, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v5

    .line 141
    .local v5, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    if-nez v5, :cond_0

    .line 142
    iget-object v7, p0, Lorg/apache/poi/hslf/model/Fill;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v8, "Fill has not yet been assigned to a sheet"

    invoke-virtual {v7, v9, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 154
    :goto_0
    return-object v6

    .line 145
    :cond_0
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v4

    .line 146
    .local v4, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v2

    .line 147
    .local v2, "doc":Lorg/apache/poi/hslf/record/Document;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Document;->getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/PPDrawingGroup;->getDggContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 148
    .local v1, "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v7, -0xfff

    invoke-static {v1, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 149
    .local v0, "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v0, :cond_1

    .line 150
    iget-object v7, p0, Lorg/apache/poi/hslf/model/Fill;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v8, "EscherContainerRecord.BSTORE_CONTAINER was not found "

    invoke-virtual {v7, v9, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 153
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    .line 154
    .local v3, "lst":Ljava/util/List;
    add-int/lit8 v6, p1, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherBSERecord;

    goto :goto_0
.end method

.method public getFillType()I
    .locals 4

    .prologue
    .line 120
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 121
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x180

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 122
    .local v1, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    goto :goto_0
.end method

.method public getForegroundColor()Lorg/apache/poi/java/awt/Color;
    .locals 6

    .prologue
    .line 172
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 173
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x1bf

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 175
    .local v1, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 177
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    const/16 v3, 0x181

    const/16 v4, 0x182

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/poi/hslf/model/Shape;->getColor(SSI)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    goto :goto_0
.end method

.method public getNofillpropertyvalue()I
    .locals 4

    .prologue
    .line 198
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    .line 197
    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 200
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x1bf

    invoke-static {v0, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    .line 199
    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 201
    .local v1, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v1, :cond_0

    .line 202
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    .line 204
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getPictureData()Lorg/apache/poi/hslf/usermodel/PictureData;
    .locals 15

    .prologue
    const/4 v11, 0x0

    .line 238
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v12

    const/16 v13, -0xff5

    invoke-static {v12, v13}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 239
    .local v7, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v12, 0x186

    invoke-static {v7, v12}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 240
    .local v8, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v8, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-object v11

    .line 242
    :cond_1
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v10

    .line 243
    .local v10, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v10}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getPictureData()[Lorg/apache/poi/hslf/usermodel/PictureData;

    move-result-object v9

    .line 244
    .local v9, "pict":[Lorg/apache/poi/hslf/usermodel/PictureData;
    invoke-virtual {v10}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v3

    .line 246
    .local v3, "doc":Lorg/apache/poi/hslf/record/Document;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Document;->getPPDrawingGroup()Lorg/apache/poi/hslf/record/PPDrawingGroup;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/hslf/record/PPDrawingGroup;->getDggContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 247
    .local v2, "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v12, -0xfff

    invoke-static {v2, v12}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 249
    .local v1, "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v6

    .line 250
    .local v6, "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-virtual {v8}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v5

    .line 251
    .local v5, "idx":I
    if-nez v5, :cond_2

    .line 252
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Fill;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x5

    const-string/jumbo v14, "no reference to picture data found "

    invoke-virtual {v12, v13, v14}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 254
    :cond_2
    add-int/lit8 v12, v5, -0x1

    invoke-interface {v6, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 255
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v12, v9

    if-ge v4, v12, :cond_0

    .line 256
    aget-object v12, v9, v4

    invoke-virtual {v12}, Lorg/apache/poi/hslf/usermodel/PictureData;->getOffset()I

    move-result v12

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getOffset()I

    move-result v13

    if-ne v12, v13, :cond_3

    .line 257
    aget-object v11, v9, v4

    goto :goto_0

    .line 255
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public setBackgroundColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 8
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    const/16 v7, 0x183

    .line 224
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 225
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    if-nez p1, :cond_0

    .line 226
    const/4 v2, -0x1

    invoke-static {v0, v7, v2}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 232
    :goto_0
    return-void

    .line 229
    :cond_0
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v1

    .line 230
    .local v1, "rgb":I
    invoke-static {v0, v7, v1}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    goto :goto_0
.end method

.method public setFillType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 164
    iget-object v1, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    const/16 v2, -0xff5

    invoke-static {v1, v2}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 165
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v1, 0x180

    invoke-static {v0, v1, p1}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 166
    return-void
.end method

.method public setForegroundColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 8
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    const/16 v7, 0x1bf

    .line 185
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 186
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    if-nez p1, :cond_0

    .line 187
    const/high16 v2, 0x150000

    invoke-static {v0, v7, v2}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 194
    :goto_0
    return-void

    .line 190
    :cond_0
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v1

    .line 191
    .local v1, "rgb":I
    const/16 v2, 0x181

    invoke-static {v0, v2, v1}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 192
    const v2, 0x150011

    invoke-static {v0, v7, v2}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    goto :goto_0
.end method

.method public setPictureData(I)V
    .locals 4
    .param p1, "idx"    # I

    .prologue
    .line 271
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 272
    .local v1, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x4186

    invoke-static {v1, v2, p1}, Lorg/apache/poi/hslf/model/Shape;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 273
    if-eqz p1, :cond_0

    .line 274
    iget-object v2, p0, Lorg/apache/poi/hslf/model/Fill;->shape:Lorg/apache/poi/hslf/model/Shape;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 275
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/model/Fill;->getEscherBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v0

    .line 277
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getRef()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 281
    .end local v0    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    :cond_0
    return-void
.end method
