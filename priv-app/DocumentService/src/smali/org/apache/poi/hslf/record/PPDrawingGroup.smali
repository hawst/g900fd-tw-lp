.class public final Lorg/apache/poi/hslf/record/PPDrawingGroup;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "PPDrawingGroup.java"


# instance fields
.field private _header:[B

.field private dgg:Lorg/apache/poi/ddf/EscherDggRecord;

.field private dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;


# direct methods
.method protected constructor <init>([BII)V
    .locals 6
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 43
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 45
    new-array v3, v5, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->_header:[B

    .line 46
    iget-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->_header:[B

    invoke-static {p1, p2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    new-array v1, p3, [B

    .line 50
    .local v1, "contents":[B
    invoke-static {p1, p2, v1, v4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    new-instance v2, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v2}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 53
    .local v2, "erf":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    .line 54
    .local v0, "child":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0, v1, v4, v2}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 55
    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 56
    return-void
.end method


# virtual methods
.method public getChildRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDggContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getEscherDggRecord()Lorg/apache/poi/ddf/EscherDggRecord;
    .locals 3

    .prologue
    .line 122
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    if-nez v2, :cond_1

    .line 123
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 131
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_1
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    return-object v2

    .line 124
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 125
    .local v1, "r":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v2, v1, Lorg/apache/poi/ddf/EscherDggRecord;

    if-eqz v2, :cond_0

    .line 126
    check-cast v1, Lorg/apache/poi/ddf/EscherDggRecord;

    .end local v1    # "r":Lorg/apache/poi/ddf/EscherRecord;
    iput-object v1, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 62
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->PPDrawingGroup:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 14
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 74
    .local v2, "bout":Ljava/io/ByteArrayOutputStream;
    iget-object v11, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v11}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v8

    .line 75
    .local v8, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_0

    .line 98
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v10

    .line 101
    .local v10, "size":I
    iget-object v11, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->_header:[B

    const/4 v12, 0x4

    add-int/lit8 v13, v10, 0x8

    invoke-static {v11, v12, v13}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 104
    iget-object v11, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->_header:[B

    invoke-virtual {p1, v11}, Ljava/io/OutputStream;->write([B)V

    .line 106
    const/16 v11, 0x8

    new-array v6, v11, [B

    .line 107
    .local v6, "dgghead":[B
    const/4 v11, 0x0

    iget-object v12, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v12}, Lorg/apache/poi/ddf/EscherContainerRecord;->getOptions()S

    move-result v12

    invoke-static {v6, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 108
    const/4 v11, 0x2

    iget-object v12, p0, Lorg/apache/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v12}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v12

    invoke-static {v6, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 109
    const/4 v11, 0x4

    invoke-static {v6, v11, v10}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 110
    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write([B)V

    .line 113
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v11

    invoke-virtual {p1, v11}, Ljava/io/OutputStream;->write([B)V

    .line 115
    return-void

    .line 76
    .end local v6    # "dgghead":[B
    .end local v10    # "size":I
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/ddf/EscherRecord;

    .line 77
    .local v9, "r":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v9}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v11

    const/16 v12, -0xfff

    if-ne v11, v12, :cond_2

    move-object v4, v9

    .line 78
    check-cast v4, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 80
    .local v4, "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 81
    .local v1, "b2":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    .line 87
    const/16 v11, 0x8

    new-array v5, v11, [B

    .line 88
    .local v5, "bstorehead":[B
    const/4 v11, 0x0

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getOptions()S

    move-result v12

    invoke-static {v5, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 89
    const/4 v11, 0x2

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v12

    invoke-static {v5, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 90
    const/4 v11, 0x4

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v12

    invoke-static {v5, v11, v12}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 91
    invoke-virtual {v2, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 92
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_0

    .line 82
    .end local v5    # "bstorehead":[B
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherBSERecord;

    .line 83
    .local v3, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    const/16 v11, 0x2c

    new-array v0, v11, [B

    .line 84
    .local v0, "b":[B
    const/4 v11, 0x0

    invoke-virtual {v3, v11, v0}, Lorg/apache/poi/ddf/EscherBSERecord;->serialize(I[B)I

    .line 85
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_1

    .line 95
    .end local v0    # "b":[B
    .end local v1    # "b2":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    .end local v4    # "bstore":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v7    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_2
    invoke-virtual {v9}, Lorg/apache/poi/ddf/EscherRecord;->serialize()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_0
.end method
