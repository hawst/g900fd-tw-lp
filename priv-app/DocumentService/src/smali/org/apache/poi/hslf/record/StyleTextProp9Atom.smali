.class public final Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "StyleTextProp9Atom.java"


# instance fields
.field private final autoNumberSchemes:[Lorg/apache/poi/hslf/model/textproperties/TextPFException9;

.field private data:[B

.field private header:[B

.field private length:I

.field private recordId:S

.field private version:S


# direct methods
.method protected constructor <init>([BII)V
    .locals 8
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v7, 0x0

    .line 51
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 53
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 54
    .local v2, "schemes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/model/textproperties/TextPFException9;>;"
    new-array v4, v5, [B

    iput-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    .line 55
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    invoke-static {p1, p2, v4, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    invoke-static {v4, v7}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    iput-short v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->version:S

    .line 57
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    iput-short v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->recordId:S

    .line 58
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->length:I

    .line 61
    add-int/lit8 v4, p3, -0x8

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    .line 62
    add-int/lit8 v4, p2, 0x8

    iget-object v5, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    add-int/lit8 v6, p3, -0x8

    invoke-static {p1, v4, v5, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    array-length v4, v4

    if-lt v0, v4, :cond_1

    .line 79
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lorg/apache/poi/hslf/model/textproperties/TextPFException9;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/apache/poi/hslf/model/textproperties/TextPFException9;

    iput-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->autoNumberSchemes:[Lorg/apache/poi/hslf/model/textproperties/TextPFException9;

    .line 80
    return-void

    .line 64
    :cond_1
    new-instance v1, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;

    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    invoke-direct {v1, v4, v0}, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;-><init>([BI)V

    .line 65
    .local v1, "item":Lorg/apache/poi/hslf/model/textproperties/TextPFException9;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/textproperties/TextPFException9;->getRecordLength()I

    move-result v4

    add-int/2addr v0, v4

    .line 69
    add-int/lit8 v0, v0, 0x4

    .line 70
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    invoke-static {v4, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    .line 71
    .local v3, "textSiException":I
    add-int/lit8 v0, v0, 0x4

    .line 72
    and-int/lit8 v4, v3, 0x40

    if-eqz v4, :cond_2

    .line 73
    add-int/lit8 v0, v0, 0x2

    .line 75
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    array-length v4, v4

    if-lt v0, v4, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public getAutoNumberTypes()[Lorg/apache/poi/hslf/model/textproperties/TextPFException9;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->autoNumberSchemes:[Lorg/apache/poi/hslf/model/textproperties/TextPFException9;

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->length:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 86
    iget-short v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->recordId:S

    int-to-long v0, v0

    return-wide v0
.end method

.method public getVersion()S
    .locals 1

    .prologue
    .line 89
    iget-short v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->version:S

    return v0
.end method

.method public reset(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 125
    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    .line 127
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    invoke-static {v0, v2, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 129
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    const/4 v1, 0x1

    invoke-static {v0, v3, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 131
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    const/16 v1, 0x8

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 134
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    iget-object v1, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    array-length v1, v1

    invoke-static {v0, v3, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 135
    return-void
.end method

.method public setTextSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 118
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 108
    iget-object v0, p0, Lorg/apache/poi/hslf/record/StyleTextProp9Atom;->data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 109
    return-void
.end method
