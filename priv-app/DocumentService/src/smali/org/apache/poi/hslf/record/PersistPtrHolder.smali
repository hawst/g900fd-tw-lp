.class public final Lorg/apache/poi/hslf/record/PersistPtrHolder;
.super Lorg/apache/poi/hslf/record/PositionDependentRecordAtom;
.source "PersistPtrHolder.java"


# instance fields
.field private _header:[B

.field private _ptrData:[B

.field private _slideLocations:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private _slideOffsetDataLocation:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private _type:J


# direct methods
.method protected constructor <init>([BII)V
    .locals 15
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 126
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/PositionDependentRecordAtom;-><init>()V

    .line 129
    const/16 v9, 0x8

    move/from16 v0, p3

    if-ge v0, v9, :cond_0

    const/16 p3, 0x8

    .line 132
    :cond_0
    const/16 v9, 0x8

    new-array v9, v9, [B

    iput-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_header:[B

    .line 133
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_header:[B

    const/4 v12, 0x0

    const/16 v13, 0x8

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v9, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_header:[B

    const/4 v12, 0x2

    invoke-static {v9, v12}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v9

    int-to-long v12, v9

    iput-wide v12, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_type:J

    .line 143
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    iput-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    .line 144
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    iput-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    .line 145
    add-int/lit8 v9, p3, -0x8

    new-array v9, v9, [B

    iput-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    .line 146
    add-int/lit8 v9, p2, 0x8

    iget-object v12, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    const/4 v13, 0x0

    iget-object v14, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v14, v14

    move-object/from16 v0, p1

    invoke-static {v0, v9, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    const/4 v7, 0x0

    .line 149
    .local v7, "pos":I
    :cond_1
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v9, v9

    if-lt v7, v9, :cond_2

    .line 173
    return-void

    .line 151
    :cond_2
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-static {v9, v7}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    .line 155
    .local v4, "info":J
    const/16 v9, 0x14

    shr-long v12, v4, v9

    long-to-int v3, v12

    .line 156
    .local v3, "offset_count":I
    shl-int/lit8 v9, v3, 0x14

    int-to-long v12, v9

    sub-long v12, v4, v12

    long-to-int v6, v12

    .line 160
    .local v6, "offset_no":I
    add-int/lit8 v7, v7, 0x4

    .line 163
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 164
    add-int v8, v6, v2

    .line 165
    .local v8, "sheet_no":I
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-static {v9, v7}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v10

    .line 166
    .local v10, "sheet_offset":J
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    long-to-int v13, v10

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v9, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    add-int/lit8 v7, v7, 0x4

    .line 163
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addSlideLookup(II)V
    .locals 5
    .param p1, "slideID"    # I
    .param p2, "posOnDisk"    # I

    .prologue
    const/4 v4, 0x0

    .line 97
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x8

    new-array v1, v2, [B

    .line 98
    .local v1, "newPtrData":[B
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    iget-object v3, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 104
    iget-object v4, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v4, v4

    add-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 103
    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    move v0, p1

    .line 110
    .local v0, "infoBlock":I
    const/high16 v2, 0x100000

    add-int/2addr v0, v2

    .line 113
    array-length v2, v1

    add-int/lit8 v2, v2, -0x8

    invoke-static {v1, v2, v0}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 114
    array-length v2, v1

    add-int/lit8 v2, v2, -0x4

    invoke-static {v1, v2, p2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 117
    iput-object v1, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    .line 120
    iget-object v2, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_header:[B

    const/4 v3, 0x4

    array-length v4, v1

    invoke-static {v2, v3, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 121
    return-void
.end method

.method public getKnownSlideIDs()[I
    .locals 5

    .prologue
    .line 65
    iget-object v4, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->size()I

    move-result v4

    new-array v3, v4, [I

    .line 66
    .local v3, "ids":[I
    iget-object v4, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 67
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-lt v1, v4, :cond_0

    .line 71
    return-object v3

    .line 68
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 69
    .local v2, "id":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v3, v1

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 178
    iget-wide v0, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_type:J

    return-wide v0
.end method

.method public getSlideLocationsLookup()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getSlideOffsetDataLocationsLookup()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    return-object v0
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "oldToNewReferencesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v9, 0x5

    .line 185
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v5

    .line 190
    .local v5, "slideIDs":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v5

    if-lt v1, v6, :cond_0

    .line 209
    return-void

    .line 191
    :cond_0
    aget v6, v5, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 192
    .local v2, "id":Ljava/lang/Integer;
    iget-object v6, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v6, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 193
    .local v4, "oldPos":Ljava/lang/Integer;
    invoke-virtual {p1, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 195
    .local v3, "newPos":Ljava/lang/Integer;
    if-nez v3, :cond_1

    .line 196
    sget-object v6, Lorg/apache/poi/hslf/record/PersistPtrHolder;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Couldn\'t find the new location of the \"slide\" with id "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " that used to be at "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v9, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 197
    sget-object v6, Lorg/apache/poi/hslf/record/PersistPtrHolder;->logger:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v7, "Not updating the position of it, you probably won\'t be able to find it any more (if you ever could!)"

    invoke-virtual {v6, v9, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 198
    move-object v3, v4

    .line 202
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    invoke-virtual {v6, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 203
    .local v0, "dataOffset":Ljava/lang/Integer;
    iget-object v6, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v6, v7, v8}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 206
    iget-object v6, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v6, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v6, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v6, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 217
    iget-object v0, p0, Lorg/apache/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 218
    return-void
.end method
