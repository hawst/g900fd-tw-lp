.class public final Lorg/apache/poi/hslf/dev/UserEditAndPersistListing;
.super Ljava/lang/Object;
.source "UserEditAndPersistListing.java"


# static fields
.field private static fileContents:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findRecordAtPos(I)Lorg/apache/poi/hslf/record/Record;
    .locals 7
    .param p0, "pos"    # I

    .prologue
    .line 124
    sget-object v1, Lorg/apache/poi/hslf/dev/UserEditAndPersistListing;->fileContents:[B

    add-int/lit8 v6, p0, 0x2

    invoke-static {v1, v6}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    int-to-long v4, v1

    .line 125
    .local v4, "type":J
    sget-object v1, Lorg/apache/poi/hslf/dev/UserEditAndPersistListing;->fileContents:[B

    add-int/lit8 v6, p0, 0x4

    invoke-static {v1, v6}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v2

    .line 127
    .local v2, "rlen":J
    sget-object v1, Lorg/apache/poi/hslf/dev/UserEditAndPersistListing;->fileContents:[B

    long-to-int v6, v2

    add-int/lit8 v6, v6, 0x8

    invoke-static {v4, v5, v1, p0, v6}, Lorg/apache/poi/hslf/record/Record;->createRecordForType(J[BII)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 129
    .local v0, "r":Lorg/apache/poi/hslf/record/Record;
    return-object v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 20
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    move-object/from16 v0, p0

    array-length v15, v0

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 37
    sget-object v15, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v16, "Need to give a filename"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 38
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/System;->exit(I)V

    .line 43
    :cond_0
    new-instance v14, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/4 v15, 0x0

    aget-object v15, p0, v15

    invoke-direct {v14, v15}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 44
    .local v14, "ss":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v14}, Lorg/apache/poi/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    move-result-object v15

    sput-object v15, Lorg/apache/poi/hslf/dev/UserEditAndPersistListing;->fileContents:[B

    .line 48
    invoke-virtual {v14}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v11

    .line 49
    .local v11, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v8, 0x0

    .line 50
    .local v8, "pos":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v15, v11

    if-lt v4, v15, :cond_1

    .line 90
    const/4 v8, 0x0

    .line 92
    const/4 v4, 0x0

    :goto_1
    array-length v15, v11

    if-lt v4, v15, :cond_4

    .line 119
    return-void

    .line 51
    :cond_1
    aget-object v10, v11, v4

    .line 53
    .local v10, "r":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    .line 57
    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v16

    const-wide/16 v18, 0x1772

    cmp-long v15, v16, v18

    if-nez v15, :cond_2

    move-object v9, v10

    .line 60
    check-cast v9, Lorg/apache/poi/hslf/record/PersistPtrHolder;

    .line 63
    .local v9, "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v12

    .line 64
    .local v12, "sheetIDs":[I
    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v13

    .line 65
    .local v13, "sheetOffsets":Ljava/util/Hashtable;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_2
    array-length v15, v12

    if-lt v6, v15, :cond_3

    .line 83
    .end local v6    # "j":I
    .end local v9    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v12    # "sheetIDs":[I
    .end local v13    # "sheetOffsets":Ljava/util/Hashtable;
    :cond_2
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 84
    .local v3, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v10, v3}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 85
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v15

    add-int/2addr v8, v15

    .line 50
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 66
    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "j":I
    .restart local v9    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .restart local v12    # "sheetIDs":[I
    .restart local v13    # "sheetOffsets":Ljava/util/Hashtable;
    :cond_3
    aget v15, v12, v6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 67
    .local v5, "id":Ljava/lang/Integer;
    invoke-virtual {v13, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 72
    .local v7, "offset":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-static {v15}, Lorg/apache/poi/hslf/dev/UserEditAndPersistListing;->findRecordAtPos(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    .line 76
    .local v2, "atPos":Lorg/apache/poi/hslf/record/Record;
    instance-of v15, v2, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    .line 65
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 93
    .end local v2    # "atPos":Lorg/apache/poi/hslf/record/Record;
    .end local v5    # "id":Ljava/lang/Integer;
    .end local v6    # "j":I
    .end local v7    # "offset":Ljava/lang/Integer;
    .end local v9    # "pph":Lorg/apache/poi/hslf/record/PersistPtrHolder;
    .end local v10    # "r":Lorg/apache/poi/hslf/record/Record;
    .end local v12    # "sheetIDs":[I
    .end local v13    # "sheetOffsets":Ljava/util/Hashtable;
    :cond_4
    aget-object v10, v11, v4

    .line 95
    .restart local v10    # "r":Lorg/apache/poi/hslf/record/Record;
    instance-of v15, v10, Lorg/apache/poi/hslf/record/UserEditAtom;

    .line 105
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 106
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v10, v3}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 107
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v15

    add-int/2addr v8, v15

    .line 92
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
