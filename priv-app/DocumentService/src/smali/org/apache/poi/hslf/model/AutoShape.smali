.class public Lorg/apache/poi/hslf/model/AutoShape;
.super Lorg/apache/poi/hslf/model/TextShape;
.source "AutoShape.java"


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(ILorg/apache/poi/hslf/model/Shape;)V

    .line 48
    return-void
.end method

.method public constructor <init>(ILorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hslf/model/TextShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 43
    instance-of v0, p2, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hslf/model/AutoShape;->createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/AutoShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 44
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/TextShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 4
    .param p1, "shapeType"    # I
    .param p2, "isChild"    # Z

    .prologue
    const v3, 0x8000004

    const/16 v2, 0x181

    .line 51
    invoke-super {p0, p2}, Lorg/apache/poi/hslf/model/TextShape;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/AutoShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 53
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/model/AutoShape;->setShapeType(I)V

    .line 56
    const/16 v0, 0x7f

    const/high16 v1, 0x40000

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 57
    invoke-virtual {p0, v2, v3}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 58
    invoke-virtual {p0, v2, v3}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 59
    const/16 v0, 0x183

    const/high16 v1, 0x8000000

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 60
    const/16 v0, 0x1bf

    const v1, 0x100010

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 61
    const/16 v0, 0x1c0

    const v1, 0x8000001

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 62
    const/16 v0, 0x1ff

    const v1, 0x80008

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 63
    const/16 v0, 0x201

    const v1, 0x8000002

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 65
    iget-object v0, p0, Lorg/apache/poi/hslf/model/AutoShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getAdjustmentValue(I)I
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 87
    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The index of an adjustment value must be in the [0, 9] range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    add-int/lit16 v0, p1, 0x147

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/AutoShape;->getEscherProperty(S)I

    move-result v0

    return v0
.end method

.method public isShapeType(I)Z
    .locals 5
    .param p1, "type"    # I

    .prologue
    .line 117
    move-object v3, p0

    .line 118
    .local v3, "tx":Lorg/apache/poi/hslf/model/TextShape;
    const/4 v2, 0x0

    .line 119
    .local v2, "placeholderId":I
    invoke-virtual {v3}, Lorg/apache/poi/hslf/model/TextShape;->getPlaceholderAtom()Lorg/apache/poi/hslf/record/OEPlaceholderAtom;

    move-result-object v1

    .line 120
    .local v1, "oep":Lorg/apache/poi/hslf/record/OEPlaceholderAtom;
    if-eqz v1, :cond_1

    .line 121
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    move-result v2

    .line 127
    :cond_0
    :goto_0
    if-ne v2, p1, :cond_2

    .line 128
    const/4 v4, 0x1

    .line 130
    :goto_1
    return v4

    .line 124
    :cond_1
    sget-object v4, Lorg/apache/poi/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v4, v4, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {v3, v4}, Lorg/apache/poi/hslf/model/TextShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;

    .line 125
    .local v0, "hldr":Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;->getPlaceholderId()I

    move-result v2

    goto :goto_0

    .line 130
    .end local v0    # "hldr":Lorg/apache/poi/hslf/record/RoundTripHFPlaceholder12;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public setAdjustmentValue(II)V
    .locals 2
    .param p1, "idx"    # I
    .param p2, "val"    # I

    .prologue
    .line 105
    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The index of an adjustment value must be in the [0, 9] range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    add-int/lit16 v0, p1, 0x147

    int-to-short v0, v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/poi/hslf/model/AutoShape;->setEscherProperty(SI)V

    .line 108
    return-void
.end method

.method protected setDefaultTextProperties(Lorg/apache/poi/hslf/model/TextRun;)V
    .locals 1
    .param p1, "_txtrun"    # Lorg/apache/poi/hslf/model/TextRun;

    .prologue
    const/4 v0, 0x1

    .line 69
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/AutoShape;->setVerticalAlignment(I)V

    .line 70
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/AutoShape;->setHorizontalAlignment(I)V

    .line 71
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/AutoShape;->setWordWrap(I)V

    .line 72
    return-void
.end method
