.class public final Lorg/apache/poi/hslf/blip/PICT;
.super Lorg/apache/poi/hslf/blip/Metafile;
.source "PICT.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/hslf/blip/Metafile;-><init>()V

    .line 38
    return-void
.end method

.method private read([BI)[B
    .locals 8
    .param p1, "data"    # [B
    .param p2, "pos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 67
    .local v5, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 68
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    new-instance v3, Lorg/apache/poi/hslf/blip/Metafile$Header;

    invoke-direct {v3}, Lorg/apache/poi/hslf/blip/Metafile$Header;-><init>()V

    .line 69
    .local v3, "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    invoke-virtual {v3, p1, p2}, Lorg/apache/poi/hslf/blip/Metafile$Header;->read([BI)V

    .line 70
    invoke-virtual {v3}, Lorg/apache/poi/hslf/blip/Metafile$Header;->getSize()I

    move-result v6

    add-int/2addr v6, p2

    int-to-long v6, v6

    invoke-virtual {v0, v6, v7}, Ljava/io/ByteArrayInputStream;->skip(J)J

    .line 71
    new-instance v4, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v4, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 72
    .local v4, "inflater":Ljava/util/zip/InflaterInputStream;
    const/16 v6, 0x1000

    new-array v1, v6, [B

    .line 74
    .local v1, "chunk":[B
    :goto_0
    invoke-virtual {v4, v1}, Ljava/util/zip/InflaterInputStream;->read([B)I

    move-result v2

    .local v2, "count":I
    if-gez v2, :cond_0

    .line 77
    invoke-virtual {v4}, Ljava/util/zip/InflaterInputStream;->close()V

    .line 78
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .line 75
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0
.end method


# virtual methods
.method public getData()[B
    .locals 7

    .prologue
    .line 44
    invoke-virtual {p0}, Lorg/apache/poi/hslf/blip/PICT;->getRawData()[B

    move-result-object v5

    .line 46
    .local v5, "rawdata":[B
    const/16 v6, 0x200

    :try_start_0
    new-array v1, v6, [B

    .line 47
    .local v1, "macheader":[B
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 48
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v2, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 49
    const/16 v4, 0x10

    .line 52
    .local v4, "pos":I
    :try_start_1
    invoke-direct {p0, v5, v4}, Lorg/apache/poi/hslf/blip/PICT;->read([BI)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    .line 58
    .local v3, "pict":[B
    :goto_0
    :try_start_2
    invoke-virtual {v2, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 59
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .line 53
    .end local v3    # "pict":[B
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/IOException;
    const/16 v6, 0x20

    invoke-direct {p0, v5, v6}, Lorg/apache/poi/hslf/blip/PICT;->read([BI)[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v3

    .restart local v3    # "pict":[B
    goto :goto_0

    .line 60
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "macheader":[B
    .end local v2    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "pict":[B
    .end local v4    # "pos":I
    :catch_1
    move-exception v0

    .line 61
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v6, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v6, v0}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method

.method public getSignature()I
    .locals 1

    .prologue
    .line 117
    const/16 v0, 0x5430

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x4

    return v0
.end method

.method public setData([B)V
    .locals 8
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xc8

    const/4 v6, 0x0

    .line 82
    const/16 v4, 0x200

    .line 83
    .local v4, "pos":I
    array-length v5, p1

    sub-int/2addr v5, v4

    invoke-virtual {p0, p1, v4, v5}, Lorg/apache/poi/hslf/blip/PICT;->compress([BII)[B

    move-result-object v1

    .line 85
    .local v1, "compressed":[B
    new-instance v2, Lorg/apache/poi/hslf/blip/Metafile$Header;

    invoke-direct {v2}, Lorg/apache/poi/hslf/blip/Metafile$Header;-><init>()V

    .line 86
    .local v2, "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    array-length v5, p1

    add-int/lit16 v5, v5, -0x200

    iput v5, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->wmfsize:I

    .line 88
    new-instance v5, Lorg/apache/poi/java/awt/Rectangle;

    invoke-direct {v5, v6, v6, v7, v7}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    iput-object v5, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    .line 89
    new-instance v5, Lorg/apache/poi/java/awt/Dimension;

    iget-object v6, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v6, v6, Lorg/apache/poi/java/awt/Rectangle;->width:I

    mul-int/lit16 v6, v6, 0x319c

    .line 90
    iget-object v7, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v7, v7, Lorg/apache/poi/java/awt/Rectangle;->height:I

    mul-int/lit16 v7, v7, 0x319c

    invoke-direct {v5, v6, v7}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    .line 89
    iput-object v5, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->size:Lorg/apache/poi/java/awt/Dimension;

    .line 91
    array-length v5, v1

    iput v5, v2, Lorg/apache/poi/hslf/blip/Metafile$Header;->zipsize:I

    .line 93
    invoke-static {p1}, Lorg/apache/poi/hslf/blip/PICT;->getChecksum([B)[B

    move-result-object v0

    .line 94
    .local v0, "checksum":[B
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 95
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 97
    const/16 v5, 0x10

    new-array v5, v5, [B

    invoke-virtual {v3, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 98
    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/blip/Metafile$Header;->write(Ljava/io/OutputStream;)V

    .line 99
    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 101
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/poi/hslf/blip/PICT;->setRawData([B)V

    .line 102
    return-void
.end method
