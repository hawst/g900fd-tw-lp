.class public final Lorg/apache/poi/hslf/blip/DIB;
.super Lorg/apache/poi/hslf/blip/Bitmap;
.source "DIB.java"


# static fields
.field public static final HEADER_SIZE:I = 0xe


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/poi/hslf/blip/Bitmap;-><init>()V

    return-void
.end method

.method public static addBMPHeader([B)[B
    .locals 8
    .param p0, "data"    # [B

    .prologue
    const/4 v7, 0x0

    .line 61
    const/16 v5, 0xe

    new-array v2, v5, [B

    .line 63
    .local v2, "header":[B
    const/16 v5, 0x4d42

    invoke-static {v2, v7, v5}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 67
    const/16 v5, 0x14

    invoke-static {p0, v5}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    .line 68
    .local v3, "imageSize":I
    array-length v5, p0

    add-int/lit8 v1, v5, 0xe

    .line 69
    .local v1, "fileSize":I
    sub-int v4, v1, v3

    .line 72
    .local v4, "offset":I
    const/4 v5, 0x2

    invoke-static {v2, v5, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 74
    const/4 v5, 0x6

    invoke-static {v2, v5, v7}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 76
    const/16 v5, 0xa

    invoke-static {v2, v5, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 79
    array-length v5, v2

    array-length v6, p0

    add-int/2addr v5, v6

    new-array v0, v5, [B

    .line 80
    .local v0, "dib":[B
    array-length v5, v2

    invoke-static {v2, v7, v0, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    array-length v5, v2

    array-length v6, p0

    invoke-static {p0, v7, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    return-object v0
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lorg/apache/poi/hslf/blip/Bitmap;->getData()[B

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hslf/blip/DIB;->addBMPHeader([B)[B

    move-result-object v0

    return-object v0
.end method

.method public getSignature()I
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x7a80

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x7

    return v0
.end method

.method public setData([B)V
    .locals 4
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    array-length v1, p1

    add-int/lit8 v1, v1, -0xe

    new-array v0, v1, [B

    .line 89
    .local v0, "dib":[B
    const/16 v1, 0xe

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    invoke-super {p0, v0}, Lorg/apache/poi/hslf/blip/Bitmap;->setData([B)V

    .line 91
    return-void
.end method
