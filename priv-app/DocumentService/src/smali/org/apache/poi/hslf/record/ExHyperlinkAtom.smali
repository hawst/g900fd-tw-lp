.class public final Lorg/apache/poi/hslf/record/ExHyperlinkAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "ExHyperlinkAtom.java"


# instance fields
.field private _data:[B

.field private _header:[B


# direct methods
.method protected constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 45
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 46
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_header:[B

    .line 47
    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    .line 49
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_header:[B

    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    array-length v1, v1

    invoke-static {v0, v4, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 53
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 63
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 65
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_header:[B

    .line 66
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 69
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    .line 70
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    array-length v0, v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The length of the data for a ExHyperlinkAtom must be at least 4 bytes, but was only "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    return-void
.end method


# virtual methods
.method public getNumber()I
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExHyperlinkAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setNumber(I)V
    .locals 2
    .param p1, "number"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 93
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 110
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExHyperlinkAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 111
    return-void
.end method
