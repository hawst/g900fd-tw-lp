.class public final Lorg/apache/poi/hslf/record/ExAviMovie;
.super Lorg/apache/poi/hslf/record/ExMCIMovie;
.source "ExAviMovie.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExMCIMovie;-><init>()V

    .line 41
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 0
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hslf/record/ExMCIMovie;-><init>([BII)V

    .line 33
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 46
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExAviMovie:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method
