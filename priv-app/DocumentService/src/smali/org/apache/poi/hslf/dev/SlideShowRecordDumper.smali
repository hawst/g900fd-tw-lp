.class public final Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;
.super Ljava/lang/Object;
.source "SlideShowRecordDumper.java"


# instance fields
.field private doc:Lorg/apache/poi/hslf/HSLFSlideShow;

.field private optEscher:Z

.field private optVerbose:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "verbose"    # Z
    .param p3, "escher"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-boolean p2, p0, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->optVerbose:Z

    .line 105
    iput-boolean p3, p0, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->optEscher:Z

    .line 106
    new-instance v0, Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-direct {v0, p1}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->doc:Lorg/apache/poi/hslf/HSLFSlideShow;

    .line 107
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    const-string/jumbo v1, ""

    .line 56
    .local v1, "filename":Ljava/lang/String;
    const/4 v4, 0x0

    .line 57
    .local v4, "verbose":Z
    const/4 v0, 0x0

    .line 59
    .local v0, "escher":Z
    const/4 v3, 0x0

    .line 60
    .local v3, "ndx":I
    :goto_0
    array-length v5, p0

    if-lt v3, v5, :cond_1

    .line 75
    :cond_0
    array-length v5, p0

    add-int/lit8 v5, v5, -0x1

    if-eq v3, v5, :cond_4

    .line 76
    invoke-static {}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->printUsage()V

    .line 85
    :goto_1
    return-void

    .line 61
    :cond_1
    aget-object v5, p0, v3

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 64
    aget-object v5, p0, v3

    const-string/jumbo v6, "-escher"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 65
    const/4 v0, 0x1

    .line 60
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 66
    :cond_2
    aget-object v5, p0, v3

    const-string/jumbo v6, "-verbose"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 67
    const/4 v4, 0x1

    .line 68
    goto :goto_2

    .line 69
    :cond_3
    invoke-static {}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->printUsage()V

    goto :goto_1

    .line 80
    :cond_4
    aget-object v1, p0, v3

    .line 82
    new-instance v2, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;

    invoke-direct {v2, v1, v4, v0}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;-><init>(Ljava/lang/String;ZZ)V

    .line 84
    .local v2, "foo":Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->printDump()V

    goto :goto_1
.end method

.method public static printUsage()V
    .locals 2

    .prologue
    .line 88
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "Usage: SlideShowRecordDumper [-escher] [-verbose] <filename>"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 89
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "Valid Options:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 90
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "-escher\t\t: dump contents of escher records"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 91
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "-verbose\t: dump binary contents of each record"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 92
    return-void
.end method


# virtual methods
.method public getDiskLen(Lorg/apache/poi/hslf/record/Record;)I
    .locals 3
    .param p1, "r"    # Lorg/apache/poi/hslf/record/Record;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    if-nez p1, :cond_0

    const/4 v2, 0x0

    .line 145
    :goto_0
    return v2

    .line 142
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 143
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p1, v1}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 144
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 145
    .local v0, "b":[B
    array-length v2, v0

    goto :goto_0
.end method

.method public getPrintableRecordContents(Lorg/apache/poi/hslf/record/Record;)Ljava/lang/String;
    .locals 5
    .param p1, "r"    # Lorg/apache/poi/hslf/record/Record;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    if-nez p1, :cond_0

    const-string/jumbo v2, "<<null>>"

    .line 154
    :goto_0
    return-object v2

    .line 151
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 152
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p1, v1}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 153
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 154
    .local v0, "b":[B
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public makeHex(II)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # I
    .param p2, "padding"    # I

    .prologue
    .line 116
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "hex":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, p2, :cond_0

    .line 120
    return-object v0

    .line 118
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public printDump()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->doc:Lorg/apache/poi/hslf/HSLFSlideShow;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v0}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->walkTree(II[Lorg/apache/poi/hslf/record/Record;)V

    .line 113
    return-void
.end method

.method public printEscherContainerRecord(Lorg/apache/poi/ddf/EscherContainerRecord;)Ljava/lang/String;
    .locals 9
    .param p1, "ecr"    # Lorg/apache/poi/ddf/EscherContainerRecord;

    .prologue
    .line 198
    const-string/jumbo v2, ""

    .line 200
    .local v2, "indent":Ljava/lang/String;
    const-string/jumbo v7, "line.separator"

    invoke-static {v7}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 202
    .local v5, "nl":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 203
    .local v0, "children":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 204
    .local v1, "count":I
    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 220
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "):"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 221
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  isContainer: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->isContainerRecord()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 222
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  options: 0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getOptions()S

    move-result v8

    invoke-static {v8}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 223
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  recordId: 0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v8

    invoke-static {v8}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 224
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  numchildren: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 225
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 220
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 219
    return-object v7

    .line 206
    :cond_0
    const/4 v7, 0x1

    if-ge v1, v7, :cond_1

    .line 207
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "  children: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 209
    :cond_1
    const-string/jumbo v4, "   "

    .line 211
    .local v4, "newIndent":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherRecord;

    .line 212
    .local v6, "record":Lorg/apache/poi/ddf/EscherRecord;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "Child "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 214
    invoke-virtual {p0, v6}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->printEscherRecord(Lorg/apache/poi/ddf/EscherRecord;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public printEscherRecord(Lorg/apache/poi/ddf/EscherRecord;)Ljava/lang/String;
    .locals 9
    .param p1, "er"    # Lorg/apache/poi/ddf/EscherRecord;

    .prologue
    .line 158
    const-string/jumbo v7, "line.separator"

    invoke-static {v7}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 159
    .local v4, "nl":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 161
    .local v0, "buf":Ljava/lang/StringBuffer;
    instance-of v7, p1, Lorg/apache/poi/ddf/EscherContainerRecord;

    if-eqz v7, :cond_1

    .line 162
    check-cast p1, Lorg/apache/poi/ddf/EscherContainerRecord;

    .end local p1    # "er":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->printEscherContainerRecord(Lorg/apache/poi/ddf/EscherContainerRecord;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 194
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 163
    .restart local p1    # "er":Lorg/apache/poi/ddf/EscherRecord;
    :cond_1
    instance-of v7, p1, Lorg/apache/poi/ddf/EscherTextboxRecord;

    if-eqz v7, :cond_6

    .line 164
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "EscherTextboxRecord:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 166
    new-instance v2, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;

    check-cast p1, Lorg/apache/poi/ddf/EscherTextboxRecord;

    .end local p1    # "er":Lorg/apache/poi/ddf/EscherRecord;
    invoke-direct {v2, p1}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;-><init>(Lorg/apache/poi/ddf/EscherTextboxRecord;)V

    .line 167
    .local v2, "etw":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    .line 168
    .local v1, "children":[Lorg/apache/poi/hslf/record/Record;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v7, v1

    if-ge v3, v7, :cond_0

    .line 169
    aget-object v7, v1, v3

    instance-of v7, v7, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v7, :cond_5

    .line 172
    if-lez v3, :cond_4

    add-int/lit8 v7, v3, -0x1

    aget-object v7, v1, v7

    instance-of v7, v7, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-nez v7, :cond_2

    .line 173
    add-int/lit8 v7, v3, -0x1

    aget-object v7, v1, v7

    instance-of v7, v7, Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v7, :cond_4

    .line 175
    :cond_2
    add-int/lit8 v7, v3, -0x1

    aget-object v7, v1, v7

    instance-of v7, v7, Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v7, :cond_3

    .line 176
    add-int/lit8 v7, v3, -0x1

    aget-object v7, v1, v7

    check-cast v7, Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/TextCharsAtom;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    .line 179
    .local v5, "size":I
    :goto_2
    aget-object v6, v1, v3

    check-cast v6, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 180
    .local v6, "tsp":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    invoke-virtual {v6, v5}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->setParentTextSize(I)V

    .line 186
    .end local v5    # "size":I
    .end local v6    # "tsp":Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    :goto_3
    new-instance v7, Ljava/lang/StringBuilder;

    aget-object v8, v1, v3

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 177
    :cond_3
    add-int/lit8 v7, v3, -0x1

    aget-object v7, v1, v7

    check-cast v7, Lorg/apache/poi/hslf/record/TextBytesAtom;

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/TextBytesAtom;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    goto :goto_2

    .line 183
    :cond_4
    const-string/jumbo v7, "Error! Couldn\'t find preceding TextAtom for style\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 188
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    aget-object v8, v1, v3

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 192
    .end local v1    # "children":[Lorg/apache/poi/hslf/record/Record;
    .end local v2    # "etw":Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
    .end local v3    # "j":I
    .restart local p1    # "er":Lorg/apache/poi/ddf/EscherRecord;
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method

.method public reverseHex(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 124
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 127
    .local v2, "ret":Ljava/lang/StringBuffer;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "0"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 130
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 131
    .local v0, "c":[C
    array-length v1, v0

    .local v1, "i":I
    :goto_0
    if-gtz v1, :cond_1

    .line 136
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 132
    :cond_1
    add-int/lit8 v3, v1, -0x2

    aget-char v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 133
    add-int/lit8 v3, v1, -0x1

    aget-char v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 134
    const/4 v3, 0x2

    if-eq v1, v3, :cond_2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 131
    :cond_2
    add-int/lit8 v1, v1, -0x2

    goto :goto_0
.end method

.method public walkTree(II[Lorg/apache/poi/hslf/record/Record;)V
    .locals 20
    .param p1, "depth"    # I
    .param p2, "pos"    # I
    .param p3, "records"    # [Lorg/apache/poi/hslf/record/Record;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    move/from16 v13, p1

    .line 235
    .local v13, "indent":I
    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v17, ""

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 236
    .local v12, "ind":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-lt v11, v13, :cond_0

    .line 240
    const/4 v11, 0x0

    :goto_1
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v11, v0, :cond_1

    .line 299
    return-void

    .line 237
    :cond_0
    const-string/jumbo v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 241
    :cond_1
    aget-object v15, p3, v11

    .line 242
    .local v15, "r":Lorg/apache/poi/hslf/record/Record;
    if-nez v15, :cond_2

    .line 240
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 249
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->getDiskLen(Lorg/apache/poi/hslf/record/Record;)I

    move-result v14

    .line 252
    .local v14, "len":I
    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v17, v0

    const/16 v18, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->makeHex(II)Ljava/lang/String;

    move-result-object v10

    .line 253
    .local v10, "hexType":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->reverseHex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 256
    .local v16, "rHexType":Ljava/lang/String;
    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    .line 257
    .local v6, "c":Ljava/lang/Class;
    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v7

    .line 258
    .local v7, "cname":Ljava/lang/String;
    const-string/jumbo v17, "class "

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 259
    const/16 v17, 0x6

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 261
    :cond_3
    const-string/jumbo v17, "org.apache.poi.hslf.record."

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 262
    const/16 v17, 0x1b

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 272
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->optEscher:Z

    move/from16 v17, v0

    if-eqz v17, :cond_7

    const-string/jumbo v17, "PPDrawing"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 273
    new-instance v9, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v9}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 275
    .local v9, "factory":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 276
    .local v5, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v15, v5}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 277
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 279
    .local v4, "b":[B
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v4, v0}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v8

    .line 280
    .local v8, "er":Lorg/apache/poi/ddf/EscherRecord;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v8, v4, v0, v9}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 292
    .end local v4    # "b":[B
    .end local v5    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "er":Lorg/apache/poi/ddf/EscherRecord;
    .end local v9    # "factory":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    :cond_5
    :goto_3
    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v17

    if-eqz v17, :cond_6

    .line 293
    add-int/lit8 v17, p1, 0x3

    add-int/lit8 v18, p2, 0x8

    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->walkTree(II[Lorg/apache/poi/hslf/record/Record;)V

    .line 297
    :cond_6
    add-int p2, p2, v14

    goto/16 :goto_2

    .line 284
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->optVerbose:Z

    move/from16 v17, v0

    if-eqz v17, :cond_5

    invoke-virtual {v15}, Lorg/apache/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v17

    if-nez v17, :cond_5

    .line 285
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/poi/hslf/dev/SlideShowRecordDumper;->getPrintableRecordContents(Lorg/apache/poi/hslf/record/Record;)Ljava/lang/String;

    goto :goto_3
.end method
