.class public final Lorg/apache/poi/hslf/model/Hyperlink;
.super Ljava/lang/Object;
.source "Hyperlink.java"


# static fields
.field public static final LINK_FIRSTSLIDE:B = 0x2t

.field public static final LINK_LASTSLIDE:B = 0x3t

.field public static final LINK_NEXTSLIDE:B = 0x0t

.field public static final LINK_NULL:B = -0x1t

.field public static final LINK_PREVIOUSSLIDE:B = 0x1t

.field public static final LINK_SLIDENUMBER:B = 0x7t

.field public static final LINK_URL:B = 0x8t


# instance fields
.field private address:Ljava/lang/String;

.field private endIndex:I

.field private id:I

.field private startIndex:I

.field private title:Ljava/lang/String;

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->id:I

    .line 35
    return-void
.end method

.method protected static find(Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Hyperlink;
    .locals 11
    .param p0, "shape"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    const/4 v8, 0x0

    .line 180
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v3, "lst":Ljava/util/ArrayList;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v5

    .line 183
    .local v5, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/Document;->getExObjList()Lorg/apache/poi/hslf/record/ExObjList;

    move-result-object v1

    .line 184
    .local v1, "exobj":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-object v8

    .line 188
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Shape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v7

    .line 189
    .local v7, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 198
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/hslf/model/Hyperlink;

    goto :goto_0

    .line 190
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherRecord;

    .line 191
    .local v4, "obj":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v9

    const/16 v10, -0xfef

    if-ne v9, v10, :cond_2

    .line 192
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherRecord;->serialize()[B

    move-result-object v0

    .line 193
    .local v0, "data":[B
    const/16 v9, 0x8

    array-length v10, v0

    add-int/lit8 v10, v10, -0x8

    invoke-static {v0, v9, v10}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    .line 194
    .local v6, "records":[Lorg/apache/poi/hslf/record/Record;
    if-eqz v6, :cond_2

    invoke-static {v6, v1, v3}, Lorg/apache/poi/hslf/model/Hyperlink;->find([Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/ExObjList;Ljava/util/List;)V

    goto :goto_1
.end method

.method private static find([Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/ExObjList;Ljava/util/List;)V
    .locals 8
    .param p0, "records"    # [Lorg/apache/poi/hslf/record/Record;
    .param p1, "exobj"    # Lorg/apache/poi/hslf/record/ExObjList;
    .param p2, "out"    # Ljava/util/List;

    .prologue
    .line 202
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, p0

    if-lt v1, v7, :cond_0

    .line 224
    return-void

    .line 204
    :cond_0
    aget-object v7, p0, v1

    instance-of v7, v7, Lorg/apache/poi/hslf/record/InteractiveInfo;

    if-eqz v7, :cond_2

    .line 205
    aget-object v0, p0, v1

    check-cast v0, Lorg/apache/poi/hslf/record/InteractiveInfo;

    .line 206
    .local v0, "hldr":Lorg/apache/poi/hslf/record/InteractiveInfo;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    move-result-object v3

    .line 207
    .local v3, "info":Lorg/apache/poi/hslf/record/InteractiveInfoAtom;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->getHyperlinkID()I

    move-result v2

    .line 208
    .local v2, "id":I
    invoke-virtual {p1, v2}, Lorg/apache/poi/hslf/record/ExObjList;->get(I)Lorg/apache/poi/hslf/record/ExHyperlink;

    move-result-object v5

    .line 209
    .local v5, "linkRecord":Lorg/apache/poi/hslf/record/ExHyperlink;
    if-eqz v5, :cond_2

    .line 210
    new-instance v4, Lorg/apache/poi/hslf/model/Hyperlink;

    invoke-direct {v4}, Lorg/apache/poi/hslf/model/Hyperlink;-><init>()V

    .line 211
    .local v4, "link":Lorg/apache/poi/hslf/model/Hyperlink;
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/ExHyperlink;->getLinkTitle()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 212
    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/ExHyperlink;->getLinkURL()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 213
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->getAction()B

    move-result v7

    iput v7, v4, Lorg/apache/poi/hslf/model/Hyperlink;->type:I

    .line 215
    add-int/lit8 v1, v1, 0x1

    array-length v7, p0

    if-ge v1, v7, :cond_1

    aget-object v7, p0, v1

    instance-of v7, v7, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;

    if-eqz v7, :cond_1

    .line 216
    aget-object v6, p0, v1

    check-cast v6, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;

    .line 217
    .local v6, "txinfo":Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;->getStartIndex()I

    move-result v7

    iput v7, v4, Lorg/apache/poi/hslf/model/Hyperlink;->startIndex:I

    .line 218
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;->getEndIndex()I

    move-result v7

    iput v7, v4, Lorg/apache/poi/hslf/model/Hyperlink;->endIndex:I

    .line 220
    .end local v6    # "txinfo":Lorg/apache/poi/hslf/record/TxInteractiveInfoAtom;
    :cond_1
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    .end local v0    # "hldr":Lorg/apache/poi/hslf/record/InteractiveInfo;
    .end local v2    # "id":I
    .end local v3    # "info":Lorg/apache/poi/hslf/record/InteractiveInfoAtom;
    .end local v4    # "link":Lorg/apache/poi/hslf/model/Hyperlink;
    .end local v5    # "linkRecord":Lorg/apache/poi/hslf/record/ExHyperlink;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static find(Lorg/apache/poi/hslf/model/TextRun;)[Lorg/apache/poi/hslf/model/Hyperlink;
    .locals 6
    .param p0, "run"    # Lorg/apache/poi/hslf/model/TextRun;

    .prologue
    .line 155
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v2, "lst":Ljava/util/ArrayList;
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v3

    .line 158
    .local v3, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Document;->getExObjList()Lorg/apache/poi/hslf/record/ExObjList;

    move-result-object v0

    .line 159
    .local v0, "exobj":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v0, :cond_1

    .line 160
    const/4 v1, 0x0

    .line 170
    :cond_0
    :goto_0
    return-object v1

    .line 162
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    .line 163
    .local v4, "records":[Lorg/apache/poi/hslf/record/Record;
    if-eqz v4, :cond_2

    invoke-static {v4, v0, v2}, Lorg/apache/poi/hslf/model/Hyperlink;->find([Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/ExObjList;Ljava/util/List;)V

    .line 165
    :cond_2
    const/4 v1, 0x0

    .line 166
    .local v1, "links":[Lorg/apache/poi/hslf/model/Hyperlink;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 167
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v1, v5, [Lorg/apache/poi/hslf/model/Hyperlink;

    .line 168
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->endIndex:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->id:I

    return v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->startIndex:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->type:I

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setAddress(Lorg/apache/poi/hslf/model/Slide;)V
    .locals 3
    .param p1, "slide"    # Lorg/apache/poi/hslf/model/Slide;

    .prologue
    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Slide;->_getSheetNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Slide;->getSlideNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",Slide "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Slide;->getSlideNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "href":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/Hyperlink;->setAddress(Ljava/lang/String;)V

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Slide "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Slide;->getSlideNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/Hyperlink;->setTitle(Ljava/lang/String;)V

    .line 102
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/Hyperlink;->setType(I)V

    .line 103
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 114
    iput p1, p0, Lorg/apache/poi/hslf/model/Hyperlink;->id:I

    .line 115
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public setType(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 62
    iput p1, p0, Lorg/apache/poi/hslf/model/Hyperlink;->type:I

    .line 63
    iget v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->type:I

    packed-switch v0, :pswitch_data_0

    .line 83
    :pswitch_0
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 84
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 87
    :goto_0
    :pswitch_1
    return-void

    .line 65
    :pswitch_2
    const-string/jumbo v0, "NEXT"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 66
    const-string/jumbo v0, "1,-1,NEXT"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    goto :goto_0

    .line 69
    :pswitch_3
    const-string/jumbo v0, "PREV"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 70
    const-string/jumbo v0, "1,-1,PREV"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    goto :goto_0

    .line 73
    :pswitch_4
    const-string/jumbo v0, "FIRST"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 74
    const-string/jumbo v0, "1,-1,FIRST"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    goto :goto_0

    .line 77
    :pswitch_5
    const-string/jumbo v0, "LAST"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 78
    const-string/jumbo v0, "1,-1,LAST"

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Hyperlink;->address:Ljava/lang/String;

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
