.class public final Lorg/apache/poi/hslf/model/ShapeFactory;
.super Ljava/lang/Object;
.source "ShapeFactory.java"


# static fields
.field protected static logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/poi/hslf/model/ShapeFactory;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hslf/model/ShapeFactory;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createShape(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Shape;
    .locals 2
    .param p0, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 41
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v0

    const/16 v1, -0xffd

    if-ne v0, v1, :cond_0

    .line 42
    invoke-static {p0, p1}, Lorg/apache/poi/hslf/model/ShapeFactory;->createShapeGroup(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/ShapeGroup;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lorg/apache/poi/hslf/model/ShapeFactory;->createSimpeShape(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Shape;

    move-result-object v0

    goto :goto_0
.end method

.method public static createShapeGroup(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/ShapeGroup;
    .locals 9
    .param p0, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    const/4 v6, 0x0

    .line 48
    const/4 v2, 0x0

    .line 49
    .local v2, "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    invoke-virtual {p0, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v7, -0xede

    invoke-static {v6, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    .line 50
    .local v3, "opt":Lorg/apache/poi/ddf/EscherRecord;
    if-eqz v3, :cond_1

    .line 52
    :try_start_0
    new-instance v1, Lorg/apache/poi/ddf/EscherPropertyFactory;

    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherPropertyFactory;-><init>()V

    .line 53
    .local v1, "f":Lorg/apache/poi/ddf/EscherPropertyFactory;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->serialize()[B

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getInstance()S

    move-result v8

    invoke-virtual {v1, v6, v7, v8}, Lorg/apache/poi/ddf/EscherPropertyFactory;->createProperties([BIS)Ljava/util/List;

    move-result-object v5

    .line 54
    .local v5, "props":Ljava/util/List;
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 55
    .local v4, "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyNumber()S

    move-result v6

    const/16 v7, 0x39f

    if-ne v6, v7, :cond_0

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 56
    new-instance v2, Lorg/apache/poi/hslf/model/Table;

    .end local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    invoke-direct {v2, p0, p1}, Lorg/apache/poi/hslf/model/Table;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 68
    .end local v1    # "f":Lorg/apache/poi/ddf/EscherPropertyFactory;
    .end local v4    # "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    .end local v5    # "props":Ljava/util/List;
    .restart local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    :goto_0
    return-object v2

    .line 58
    .restart local v1    # "f":Lorg/apache/poi/ddf/EscherPropertyFactory;
    .restart local v4    # "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    .restart local v5    # "props":Ljava/util/List;
    :cond_0
    new-instance v2, Lorg/apache/poi/hslf/model/ShapeGroup;

    .end local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    invoke-direct {v2, p0, p1}, Lorg/apache/poi/hslf/model/ShapeGroup;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    goto :goto_0

    .line 60
    .end local v1    # "f":Lorg/apache/poi/ddf/EscherPropertyFactory;
    .end local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    .end local v4    # "p":Lorg/apache/poi/ddf/EscherSimpleProperty;
    .end local v5    # "props":Ljava/util/List;
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lorg/apache/poi/hslf/model/ShapeFactory;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 62
    new-instance v2, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-direct {v2, p0, p1}, Lorg/apache/poi/hslf/model/ShapeGroup;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 64
    .restart local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v2, Lorg/apache/poi/hslf/model/ShapeGroup;

    .end local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    invoke-direct {v2, p0, p1}, Lorg/apache/poi/hslf/model/ShapeGroup;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .restart local v2    # "group":Lorg/apache/poi/hslf/model/ShapeGroup;
    goto :goto_0
.end method

.method public static createSimpeShape(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)Lorg/apache/poi/hslf/model/Shape;
    .locals 10
    .param p0, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 72
    const/4 v4, 0x0

    .line 73
    .local v4, "shape":Lorg/apache/poi/hslf/model/Shape;
    const/16 v7, -0xff6

    invoke-virtual {p0, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 75
    .local v5, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeType()S

    move-result v6

    .line 76
    .local v6, "type":I
    sparse-switch v6, :sswitch_data_0

    .line 118
    new-instance v4, Lorg/apache/poi/hslf/model/AutoShape;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 121
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    :cond_0
    :goto_0
    return-object v4

    .line 78
    :sswitch_0
    new-instance v4, Lorg/apache/poi/hslf/model/TextBox;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/TextBox;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 79
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_0

    .line 82
    :sswitch_1
    sget-object v7, Lorg/apache/poi/hslf/record/RecordTypes;->InteractiveInfo:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v7, v7, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {p0, v7}, Lorg/apache/poi/hslf/model/ShapeFactory;->getClientDataRecord(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/InteractiveInfo;

    .line 83
    .local v0, "info":Lorg/apache/poi/hslf/record/InteractiveInfo;
    sget-object v7, Lorg/apache/poi/hslf/record/RecordTypes;->OEShapeAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v7, v7, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {p0, v7}, Lorg/apache/poi/hslf/model/ShapeFactory;->getClientDataRecord(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/record/OEShapeAtom;

    .line 84
    .local v1, "oes":Lorg/apache/poi/hslf/record/OEShapeAtom;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 85
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->getAction()B

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 99
    :cond_1
    :goto_1
    if-nez v4, :cond_0

    new-instance v4, Lorg/apache/poi/hslf/model/Picture;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/Picture;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 100
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_0

    .line 87
    :pswitch_0
    new-instance v4, Lorg/apache/poi/hslf/model/OLEShape;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/OLEShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 88
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_1

    .line 90
    :pswitch_1
    new-instance v4, Lorg/apache/poi/hslf/model/MovieShape;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/MovieShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 91
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_1

    .line 95
    :cond_2
    if-eqz v1, :cond_1

    .line 96
    new-instance v4, Lorg/apache/poi/hslf/model/OLEShape;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/OLEShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_1

    .line 103
    .end local v0    # "info":Lorg/apache/poi/hslf/record/InteractiveInfo;
    .end local v1    # "oes":Lorg/apache/poi/hslf/record/OEShapeAtom;
    :sswitch_2
    new-instance v4, Lorg/apache/poi/hslf/model/Line;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/Line;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 104
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_0

    .line 106
    :sswitch_3
    const/16 v7, -0xff5

    invoke-static {p0, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 107
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v7, 0x145

    invoke-static {v2, v7}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    .line 108
    .local v3, "prop":Lorg/apache/poi/ddf/EscherProperty;
    if-eqz v3, :cond_3

    .line 109
    new-instance v4, Lorg/apache/poi/hslf/model/Freeform;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/Freeform;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_0

    .line 112
    :cond_3
    sget-object v7, Lorg/apache/poi/hslf/model/ShapeFactory;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v8, 0x5

    const-string/jumbo v9, "Creating AutoShape for a NotPrimitive shape"

    invoke-virtual {v7, v8, v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 113
    new-instance v4, Lorg/apache/poi/hslf/model/AutoShape;

    .end local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    invoke-direct {v4, p0, p1}, Lorg/apache/poi/hslf/model/AutoShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 115
    .restart local v4    # "shape":Lorg/apache/poi/hslf/model/Shape;
    goto :goto_0

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x14 -> :sswitch_2
        0x4b -> :sswitch_1
        0xc9 -> :sswitch_1
        0xca -> :sswitch_0
    .end sparse-switch

    .line 85
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static getClientDataRecord(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/hslf/record/Record;
    .locals 10
    .param p0, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "recordType"    # I

    .prologue
    .line 126
    const/4 v4, 0x0

    .line 127
    .local v4, "oep":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 139
    .end local v4    # "oep":Lorg/apache/poi/hslf/record/Record;
    :goto_0
    return-object v4

    .line 128
    .restart local v4    # "oep":Lorg/apache/poi/hslf/record/Record;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 129
    .local v3, "obj":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v6

    const/16 v7, -0xfef

    if-ne v6, v7, :cond_0

    .line 130
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->serialize()[B

    move-result-object v0

    .line 131
    .local v0, "data":[B
    const/16 v6, 0x8

    array-length v7, v0

    add-int/lit8 v7, v7, -0x8

    invoke-static {v0, v6, v7}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v5

    .line 132
    .local v5, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_0

    .line 133
    aget-object v6, v5, v2

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v6

    int-to-long v8, p1

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 134
    aget-object v4, v5, v2

    goto :goto_0

    .line 132
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
