.class public final Lorg/apache/poi/hslf/dev/SLWTListing;
.super Ljava/lang/Object;
.source "SLWTListing.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 15
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v14, 0x1

    .line 33
    array-length v12, p0

    if-ge v12, v14, :cond_0

    .line 34
    sget-object v12, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v13, "Need to give a filename"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 35
    invoke-static {v14}, Ljava/lang/System;->exit(I)V

    .line 38
    :cond_0
    new-instance v10, Lorg/apache/poi/hslf/HSLFSlideShow;

    const/4 v12, 0x0

    aget-object v12, p0, v12

    invoke-direct {v10, v12}, Lorg/apache/poi/hslf/HSLFSlideShow;-><init>(Ljava/lang/String;)V

    .line 41
    .local v10, "ss":Lorg/apache/poi/hslf/HSLFSlideShow;
    invoke-virtual {v10}, Lorg/apache/poi/hslf/HSLFSlideShow;->getRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v7

    .line 42
    .local v7, "records":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v12, v7

    if-lt v2, v12, :cond_1

    .line 88
    return-void

    .line 43
    :cond_1
    aget-object v12, v7, v2

    instance-of v12, v12, Lorg/apache/poi/hslf/record/Document;

    if-eqz v12, :cond_4

    .line 44
    aget-object v1, v7, v2

    check-cast v1, Lorg/apache/poi/hslf/record/Document;

    .line 45
    .local v1, "doc":Lorg/apache/poi/hslf/record/Document;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Document;->getSlideListWithTexts()[Lorg/apache/poi/hslf/record/SlideListWithText;

    move-result-object v9

    .line 48
    .local v9, "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    array-length v12, v9

    if-nez v12, :cond_2

    .line 49
    sget-object v12, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v13, "** Warning: Should have had at least 1! **"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 51
    :cond_2
    array-length v12, v9

    const/4 v13, 0x3

    if-le v12, v13, :cond_3

    .line 52
    sget-object v12, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v13, "** Warning: Shouldn\'t have more than 3!"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 56
    :cond_3
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v12, v9

    if-lt v3, v12, :cond_5

    .line 42
    .end local v1    # "doc":Lorg/apache/poi/hslf/record/Document;
    .end local v3    # "j":I
    .end local v9    # "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 57
    .restart local v1    # "doc":Lorg/apache/poi/hslf/record/Document;
    .restart local v3    # "j":I
    .restart local v9    # "slwts":[Lorg/apache/poi/hslf/record/SlideListWithText;
    :cond_5
    aget-object v8, v9, v3

    .line 58
    .local v8, "slwt":Lorg/apache/poi/hslf/record/SlideListWithText;
    invoke-virtual {v8}, Lorg/apache/poi/hslf/record/SlideListWithText;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 63
    .local v0, "children":[Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v8}, Lorg/apache/poi/hslf/record/SlideListWithText;->getSlideAtomsSets()[Lorg/apache/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    move-result-object v12

    array-length v5, v12

    .line 64
    .local v5, "numSAS":I
    if-ne v3, v14, :cond_6

    .line 77
    :cond_6
    const/4 v11, 0x5

    .line 78
    .local v11, "upTo":I
    array-length v12, v0

    const/4 v13, 0x5

    if-ge v12, v13, :cond_7

    array-length v11, v0

    .line 79
    :cond_7
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    if-lt v4, v11, :cond_8

    .line 56
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 80
    :cond_8
    aget-object v6, v0, v4

    .line 81
    .local v6, "r":Lorg/apache/poi/hslf/record/Record;
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v12

    long-to-int v12, v12

    .line 79
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method
