.class public final Lorg/apache/poi/hslf/record/BinaryTagDataBlob;
.super Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;
.source "BinaryTagDataBlob.java"


# instance fields
.field private _header:[B

.field private _type:J


# direct methods
.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 45
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 47
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_header:[B

    .line 48
    iget-object v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    iget-object v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_header:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_type:J

    .line 52
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 53
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_type:J

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    iget-wide v4, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/BinaryTagDataBlob;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 66
    return-void
.end method
