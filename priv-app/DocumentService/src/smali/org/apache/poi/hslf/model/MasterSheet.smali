.class public abstract Lorg/apache/poi/hslf/model/MasterSheet;
.super Lorg/apache/poi/hslf/model/Sheet;
.source "MasterSheet.java"


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V
    .locals 0
    .param p1, "container"    # Lorg/apache/poi/hslf/record/SheetContainer;
    .param p2, "sheetNo"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Sheet;-><init>(Lorg/apache/poi/hslf/record/SheetContainer;I)V

    .line 33
    return-void
.end method

.method public static isPlaceholder(Lorg/apache/poi/hslf/model/Shape;)Z
    .locals 3
    .param p0, "shape"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    const/4 v1, 0x0

    .line 50
    instance-of v2, p0, Lorg/apache/poi/hslf/model/TextShape;

    if-nez v2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p0

    .line 52
    check-cast v0, Lorg/apache/poi/hslf/model/TextShape;

    .line 53
    .local v0, "tx":Lorg/apache/poi/hslf/model/TextShape;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/TextShape;->getPlaceholderAtom()Lorg/apache/poi/hslf/record/OEPlaceholderAtom;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public abstract getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;
.end method
