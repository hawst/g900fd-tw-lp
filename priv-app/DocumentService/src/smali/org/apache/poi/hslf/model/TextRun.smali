.class public final Lorg/apache/poi/hslf/model/TextRun;
.super Ljava/lang/Object;
.source "TextRun.java"


# instance fields
.field protected _byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

.field protected _charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

.field protected _headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

.field protected _isUnicode:Z

.field protected _records:[Lorg/apache/poi/hslf/record/Record;

.field protected _rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

.field protected _ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

.field private _sheet:Lorg/apache/poi/hslf/model/Sheet;

.field protected _styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

.field private shapeId:I

.field private slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

.field private slwtIndex:I

.field private styleTextProp9Atom:Lorg/apache/poi/hslf/record/StyleTextProp9Atom;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextBytesAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V
    .locals 1
    .param p1, "tha"    # Lorg/apache/poi/hslf/record/TextHeaderAtom;
    .param p2, "tba"    # Lorg/apache/poi/hslf/record/TextBytesAtom;
    .param p3, "sta"    # Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextBytesAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    .line 81
    return-void
.end method

.method private constructor <init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextBytesAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V
    .locals 5
    .param p1, "tha"    # Lorg/apache/poi/hslf/record/TextHeaderAtom;
    .param p2, "tba"    # Lorg/apache/poi/hslf/record/TextBytesAtom;
    .param p3, "tca"    # Lorg/apache/poi/hslf/record/TextCharsAtom;
    .param p4, "sta"    # Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/poi/hslf/model/TextRun;->slwtIndex:I

    .line 87
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    .line 88
    iput-object p4, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 89
    if-eqz p2, :cond_1

    .line 90
    iput-object p2, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 91
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_isUnicode:Z

    .line 96
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getText()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "runRawText":Ljava/lang/String;
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 100
    .local v1, "pStyles":Ljava/util/LinkedList;
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 101
    .local v0, "cStyles":Ljava/util/LinkedList;
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v3, :cond_0

    .line 103
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->setParentTextSize(I)V

    .line 104
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    move-result-object v1

    .line 105
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    move-result-object v0

    .line 107
    :cond_0
    invoke-virtual {p0, v1, v0, v2}, Lorg/apache/poi/hslf/model/TextRun;->buildRichTextRuns(Ljava/util/LinkedList;Ljava/util/LinkedList;Ljava/lang/String;)V

    .line 108
    return-void

    .line 93
    .end local v0    # "cStyles":Ljava/util/LinkedList;
    .end local v1    # "pStyles":Ljava/util/LinkedList;
    .end local v2    # "runRawText":Ljava/lang/String;
    :cond_1
    iput-object p3, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 94
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_isUnicode:Z

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V
    .locals 1
    .param p1, "tha"    # Lorg/apache/poi/hslf/record/TextHeaderAtom;
    .param p2, "tca"    # Lorg/apache/poi/hslf/record/TextCharsAtom;
    .param p3, "sta"    # Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/poi/hslf/model/TextRun;-><init>(Lorg/apache/poi/hslf/record/TextHeaderAtom;Lorg/apache/poi/hslf/record/TextBytesAtom;Lorg/apache/poi/hslf/record/TextCharsAtom;Lorg/apache/poi/hslf/record/StyleTextPropAtom;)V

    .line 70
    return-void
.end method

.method private storeText(Ljava/lang/String;)V
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 327
    iget-boolean v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_isUnicode:Z

    if-eqz v6, :cond_1

    .line 329
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-virtual {v6, p1}, Lorg/apache/poi/hslf/record/TextCharsAtom;->setText(Ljava/lang/String;)V

    .line 366
    :goto_0
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    if-eqz v6, :cond_0

    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v6, v6

    if-lt v2, v6, :cond_5

    .line 374
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 332
    :cond_1
    invoke-static {p1}, Lorg/apache/poi/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    move-result v1

    .line 333
    .local v1, "hasMultibyte":Z
    if-nez v1, :cond_2

    .line 335
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    new-array v5, v6, [B

    .line 336
    .local v5, "text":[B
    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, Lorg/apache/poi/util/StringUtil;->putCompressedUnicode(Ljava/lang/String;[BI)V

    .line 337
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    invoke-virtual {v6, v5}, Lorg/apache/poi/hslf/record/TextBytesAtom;->setText([B)V

    goto :goto_0

    .line 342
    .end local v5    # "text":[B
    :cond_2
    new-instance v6, Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-direct {v6}, Lorg/apache/poi/hslf/record/TextCharsAtom;-><init>()V

    iput-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 343
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-virtual {v6, p1}, Lorg/apache/poi/hslf/record/TextCharsAtom;->setText(Ljava/lang/String;)V

    .line 346
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->getParentRecord()Lorg/apache/poi/hslf/record/RecordContainer;

    move-result-object v3

    .line 347
    .local v3, "parent":Lorg/apache/poi/hslf/record/RecordContainer;
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 348
    .local v0, "cr":[Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    array-length v6, v0

    if-lt v2, v6, :cond_3

    .line 358
    :goto_3
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 359
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_isUnicode:Z

    goto :goto_0

    .line 350
    :cond_3
    aget-object v6, v0, v2

    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 352
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    aput-object v6, v0, v2

    goto :goto_3

    .line 348
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 367
    .end local v0    # "cr":[Lorg/apache/poi/hslf/record/Record;
    .end local v1    # "hasMultibyte":Z
    .end local v3    # "parent":Lorg/apache/poi/hslf/record/RecordContainer;
    :cond_5
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v6, v6, v2

    instance-of v6, v6, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;

    if-eqz v6, :cond_6

    .line 368
    iget-object v6, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v4, v6, v2

    check-cast v4, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;

    .line 369
    .local v4, "specAtom":Lorg/apache/poi/hslf/record/TextSpecInfoAtom;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->getCharactersCovered()I

    move-result v7

    if-eq v6, v7, :cond_6

    .line 370
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v6}, Lorg/apache/poi/hslf/record/TextSpecInfoAtom;->reset(I)V

    .line 366
    .end local v4    # "specAtom":Lorg/apache/poi/hslf/record/TextSpecInfoAtom;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)Lorg/apache/poi/hslf/usermodel/RichTextRun;
    .locals 12
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 270
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 274
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getRawText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 276
    .local v2, "oldSize":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getRawText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-direct {p0, v1}, Lorg/apache/poi/hslf/model/TextRun;->storeText(Ljava/lang/String;)V

    .line 282
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphTextLengthCovered()I

    move-result v1

    sub-int v10, v1, v2

    .line 283
    .local v10, "pOverRun":I
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterTextLengthCovered()I

    move-result v1

    sub-int v8, v1, v2

    .line 284
    .local v8, "cOverRun":I
    if-lez v10, :cond_0

    .line 286
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 288
    .local v11, "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v1

    sub-int/2addr v1, v10

    .line 287
    invoke-virtual {v11, v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->updateTextSize(I)V

    .line 291
    .end local v11    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_0
    if-lez v8, :cond_1

    .line 293
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 295
    .restart local v11    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {v11}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v1

    sub-int/2addr v1, v8

    .line 294
    invoke-virtual {v11, v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->updateTextSize(I)V

    .line 301
    .end local v11    # "tpc":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v10

    invoke-virtual {v1, v3}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->addParagraphTextPropCollection(I)Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    move-result-object v4

    .line 303
    .local v4, "newPTP":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v8

    invoke-virtual {v1, v3}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->addCharacterTextPropCollection(I)Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    move-result-object v5

    .line 306
    .local v5, "newCTP":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    new-instance v0, Lorg/apache/poi/hslf/usermodel/RichTextRun;

    .line 307
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    move-object v1, p0

    move v7, v6

    .line 306
    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hslf/usermodel/RichTextRun;-><init>(Lorg/apache/poi/hslf/model/TextRun;IILorg/apache/poi/hslf/model/textproperties/TextPropCollection;Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;ZZ)V

    .line 312
    .local v0, "nr":Lorg/apache/poi/hslf/usermodel/RichTextRun;
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v9, v1, [Lorg/apache/poi/hslf/usermodel/RichTextRun;

    .line 313
    .local v9, "newRuns":[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v3, v3

    invoke-static {v1, v6, v9, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 314
    array-length v1, v9

    add-int/lit8 v1, v1, -0x1

    aput-object v0, v9, v1

    .line 315
    iput-object v9, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    .line 318
    return-object v0
.end method

.method public buildRichTextRuns(Ljava/util/LinkedList;Ljava/util/LinkedList;Ljava/lang/String;)V
    .locals 25
    .param p1, "pStyles"    # Ljava/util/LinkedList;
    .param p2, "cStyles"    # Ljava/util/LinkedList;
    .param p3, "runRawText"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-virtual/range {p1 .. p1}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 114
    :cond_0
    const/4 v5, 0x1

    new-array v5, v5, [Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    .line 115
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    const/16 v21, 0x0

    new-instance v22, Lorg/apache/poi/hslf/usermodel/RichTextRun;

    const/16 v23, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v24

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hslf/usermodel/RichTextRun;-><init>(Lorg/apache/poi/hslf/model/TextRun;II)V

    aput-object v22, v5, v21

    .line 257
    :goto_0
    return-void

    .line 119
    :cond_1
    new-instance v20, Ljava/util/Vector;

    invoke-direct/range {v20 .. v20}, Ljava/util/Vector;-><init>()V

    .line 121
    .local v20, "rtrs":Ljava/util/Vector;
    const/16 v19, 0x0

    .line 123
    .local v19, "pos":I
    const/4 v15, 0x0

    .line 124
    .local v15, "curP":I
    const/4 v14, 0x0

    .line 125
    .local v14, "curC":I
    const/16 v18, -0x1

    .line 126
    .local v18, "pLenRemain":I
    const/4 v13, -0x1

    .line 129
    .local v13, "cLenRemain":I
    :goto_1
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v5

    move/from16 v0, v19

    if-gt v0, v5, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-ge v15, v5, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-lt v14, v5, :cond_3

    .line 253
    :cond_2
    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    .line 254
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 132
    .local v8, "pProps":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 134
    .local v9, "cProps":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {v8}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v17

    .line 135
    .local v17, "pLen":I
    invoke-virtual {v9}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v12

    .line 138
    .local v12, "cLen":I
    const/16 v16, 0x0

    .line 139
    .local v16, "freshSet":Z
    const/4 v5, -0x1

    move/from16 v0, v18

    if-ne v0, v5, :cond_4

    const/4 v5, -0x1

    if-ne v13, v5, :cond_4

    const/16 v16, 0x1

    .line 140
    :cond_4
    const/4 v5, -0x1

    move/from16 v0, v18

    if-ne v0, v5, :cond_5

    move/from16 v18, v17

    .line 141
    :cond_5
    const/4 v5, -0x1

    if-ne v13, v5, :cond_6

    move v13, v12

    .line 144
    :cond_6
    const/4 v7, -0x1

    .line 145
    .local v7, "runLen":I
    const/4 v10, 0x0

    .line 146
    .local v10, "pShared":Z
    const/4 v11, 0x0

    .line 149
    .local v11, "cShared":Z
    move/from16 v0, v17

    if-ne v0, v12, :cond_8

    if-eqz v16, :cond_8

    .line 150
    move v7, v12

    .line 151
    const/4 v10, 0x0

    .line 152
    const/4 v11, 0x0

    .line 153
    add-int/lit8 v15, v15, 0x1

    .line 154
    add-int/lit8 v14, v14, 0x1

    .line 155
    const/16 v18, -0x1

    .line 156
    const/4 v13, -0x1

    .line 240
    :goto_2
    move/from16 v6, v19

    .line 241
    .local v6, "prevPos":I
    add-int v19, v19, v7

    .line 243
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v5

    move/from16 v0, v19

    if-le v0, v5, :cond_7

    .line 244
    add-int/lit8 v7, v7, -0x1

    .line 248
    :cond_7
    new-instance v4, Lorg/apache/poi/hslf/usermodel/RichTextRun;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v11}, Lorg/apache/poi/hslf/usermodel/RichTextRun;-><init>(Lorg/apache/poi/hslf/model/TextRun;IILorg/apache/poi/hslf/model/textproperties/TextPropCollection;Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;ZZ)V

    .line 249
    .local v4, "rtr":Lorg/apache/poi/hslf/usermodel/RichTextRun;
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 161
    .end local v4    # "rtr":Lorg/apache/poi/hslf/usermodel/RichTextRun;
    .end local v6    # "prevPos":I
    :cond_8
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_b

    .line 163
    const/4 v10, 0x1

    .line 166
    move/from16 v0, v18

    if-ne v0, v13, :cond_9

    .line 168
    const/4 v11, 0x0

    .line 169
    move/from16 v7, v18

    .line 170
    add-int/lit8 v15, v15, 0x1

    .line 171
    add-int/lit8 v14, v14, 0x1

    .line 172
    const/16 v18, -0x1

    .line 173
    const/4 v13, -0x1

    .line 174
    goto :goto_2

    :cond_9
    move/from16 v0, v18

    if-ge v0, v13, :cond_a

    .line 176
    const/4 v11, 0x1

    .line 177
    move/from16 v7, v18

    .line 178
    add-int/lit8 v15, v15, 0x1

    .line 179
    sub-int v13, v13, v18

    .line 180
    const/16 v18, -0x1

    .line 181
    goto :goto_2

    .line 183
    :cond_a
    const/4 v11, 0x0

    .line 184
    move v7, v13

    .line 185
    add-int/lit8 v14, v14, 0x1

    .line 186
    sub-int v18, v18, v13

    .line 187
    const/4 v13, -0x1

    .line 189
    goto :goto_2

    :cond_b
    if-ge v13, v12, :cond_e

    .line 191
    const/4 v11, 0x1

    .line 194
    move/from16 v0, v18

    if-ne v0, v13, :cond_c

    .line 196
    const/4 v10, 0x0

    .line 197
    move v7, v13

    .line 198
    add-int/lit8 v15, v15, 0x1

    .line 199
    add-int/lit8 v14, v14, 0x1

    .line 200
    const/16 v18, -0x1

    .line 201
    const/4 v13, -0x1

    .line 202
    goto :goto_2

    :cond_c
    move/from16 v0, v18

    if-ge v13, v0, :cond_d

    .line 204
    const/4 v10, 0x1

    .line 205
    move v7, v13

    .line 206
    add-int/lit8 v14, v14, 0x1

    .line 207
    sub-int v18, v18, v13

    .line 208
    const/4 v13, -0x1

    .line 209
    goto :goto_2

    .line 211
    :cond_d
    const/4 v10, 0x0

    .line 212
    move/from16 v7, v18

    .line 213
    add-int/lit8 v15, v15, 0x1

    .line 214
    sub-int v13, v13, v18

    .line 215
    const/16 v18, -0x1

    .line 217
    goto :goto_2

    .line 219
    :cond_e
    move/from16 v0, v18

    if-ge v0, v13, :cond_f

    .line 221
    const/4 v10, 0x0

    .line 222
    const/4 v11, 0x1

    .line 223
    move/from16 v7, v18

    .line 224
    add-int/lit8 v15, v15, 0x1

    .line 225
    sub-int v13, v13, v18

    .line 226
    const/16 v18, -0x1

    .line 227
    goto :goto_2

    .line 229
    :cond_f
    const/4 v10, 0x1

    .line 230
    const/4 v11, 0x0

    .line 231
    move v7, v13

    .line 232
    add-int/lit8 v14, v14, 0x1

    .line 233
    sub-int v18, v18, v13

    .line 234
    const/4 v13, -0x1

    goto/16 :goto_2
.end method

.method public changeTextInRichTextRun(Lorg/apache/poi/hslf/usermodel/RichTextRun;Ljava/lang/String;)V
    .locals 9
    .param p1, "run"    # Lorg/apache/poi/hslf/usermodel/RichTextRun;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 383
    const/4 v6, -0x1

    .line 384
    .local v6, "runID":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v7, v7

    if-lt v1, v7, :cond_0

    .line 389
    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    .line 390
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v8, "Supplied RichTextRun wasn\'t from this TextRun"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 385
    :cond_0
    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v7, v7, v1

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 386
    move v6, v1

    .line 384
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 394
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 406
    invoke-virtual {p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_getRawParagraphStyle()Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    move-result-object v5

    .line 407
    .local v5, "pCol":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_getRawCharacterStyle()Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    move-result-object v0

    .line 408
    .local v0, "cCol":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    .line 409
    .local v2, "newSize":I
    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_3

    .line 410
    add-int/lit8 v2, v2, 0x1

    .line 413
    :cond_3
    invoke-virtual {p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_isParagraphStyleShared()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 414
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v7

    invoke-virtual {p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getLength()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v5, v7}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->updateTextSize(I)V

    .line 418
    :goto_1
    invoke-virtual {p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_isCharacterStyleShared()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 419
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    move-result v7

    invoke-virtual {p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getLength()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v0, v7}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->updateTextSize(I)V

    .line 427
    :goto_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 428
    .local v4, "newText":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    :goto_3
    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v7, v7

    if-lt v1, v7, :cond_6

    .line 451
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lorg/apache/poi/hslf/model/TextRun;->storeText(Ljava/lang/String;)V

    .line 452
    return-void

    .line 416
    .end local v4    # "newText":Ljava/lang/StringBuffer;
    :cond_4
    invoke-virtual {v5, v2}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->updateTextSize(I)V

    goto :goto_1

    .line 421
    :cond_5
    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->updateTextSize(I)V

    goto :goto_2

    .line 429
    .restart local v4    # "newText":Ljava/lang/StringBuffer;
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 432
    .local v3, "newStartPos":I
    if-eq v1, v6, :cond_8

    .line 434
    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v7, v7, v1

    invoke-virtual {v7}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getRawText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 442
    :goto_4
    if-le v1, v6, :cond_7

    .line 446
    iget-object v7, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v7, v7, v1

    invoke-virtual {v7, v3}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->updateStartPosition(I)V

    .line 428
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 437
    :cond_8
    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4
.end method

.method public createTextRuler()Lorg/apache/poi/hslf/record/TextRulerAtom;
    .locals 2

    .prologue
    .line 687
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getTextRuler()Lorg/apache/poi/hslf/record/TextRulerAtom;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    .line 688
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    if-nez v0, :cond_0

    .line 689
    invoke-static {}, Lorg/apache/poi/hslf/record/TextRulerAtom;->getParagraphInstance()Lorg/apache/poi/hslf/record/TextRulerAtom;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    .line 690
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->getParentRecord()Lorg/apache/poi/hslf/record/RecordContainer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/RecordContainer;->appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V

    .line 692
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    return-object v0
.end method

.method public ensureStyleAtomPresent()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 503
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v2, :cond_0

    .line 530
    :goto_0
    return-void

    .line 509
    :cond_0
    new-instance v2, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getRawText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v3}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;-><init>(I)V

    iput-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 512
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->getParentRecord()Lorg/apache/poi/hslf/record/RecordContainer;

    move-result-object v1

    .line 515
    .local v1, "runAtomsParent":Lorg/apache/poi/hslf/record/RecordContainer;
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    .line 516
    .local v0, "addAfter":Lorg/apache/poi/hslf/record/Record;
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-nez v2, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    .line 517
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hslf/record/RecordContainer;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 520
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v2, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 521
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Needed to add StyleTextPropAtom when had many rich text runs"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 524
    :cond_2
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v4, v2, v5

    .line 525
    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 526
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 524
    invoke-virtual {v4, v2, v3, v5, v5}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->supplyTextProps(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;ZZ)V

    goto :goto_0
.end method

.method public getHyperlinks()[Lorg/apache/poi/hslf/model/Hyperlink;
    .locals 1

    .prologue
    .line 654
    invoke-static {p0}, Lorg/apache/poi/hslf/model/Hyperlink;->find(Lorg/apache/poi/hslf/model/TextRun;)[Lorg/apache/poi/hslf/model/Hyperlink;

    move-result-object v0

    return-object v0
.end method

.method protected getIndex()I
    .locals 1

    .prologue
    .line 637
    iget v0, p0, Lorg/apache/poi/hslf/model/TextRun;->slwtIndex:I

    return v0
.end method

.method public getRawText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_isUnicode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_charAtom:Lorg/apache/poi/hslf/record/TextCharsAtom;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/TextCharsAtom;->getText()Ljava/lang/String;

    move-result-object v0

    .line 568
    :goto_0
    return-object v0

    .line 566
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_byteAtom:Lorg/apache/poi/hslf/record/TextBytesAtom;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/TextBytesAtom;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 568
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    return-object v0
.end method

.method public getRichTextRunAt(I)Lorg/apache/poi/hslf/usermodel/RichTextRun;
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 664
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 669
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 665
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getStartIndex()I

    move-result v2

    .line 666
    .local v2, "start":I
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getEndIndex()I

    move-result v0

    .line 667
    .local v0, "end":I
    if-lt p1, v2, :cond_1

    if-ge p1, v0, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v3, v3, v1

    goto :goto_1

    .line 664
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRichTextRuns()[Lorg/apache/poi/hslf/usermodel/RichTextRun;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    return-object v0
.end method

.method public getRunType()I
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->getTextType()I

    move-result v0

    return v0
.end method

.method protected getShapeId()I
    .locals 1

    .prologue
    .line 623
    iget v0, p0, Lorg/apache/poi/hslf/model/TextRun;->shapeId:I

    return v0
.end method

.method public getSheet()Lorg/apache/poi/hslf/model/Sheet;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    return-object v0
.end method

.method public getStyleTextProp9Atom()Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->styleTextProp9Atom:Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    return-object v0
.end method

.method public getStyleTextPropAtom()Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/16 v4, 0xa

    .line 539
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/TextRun;->getRawText()Ljava/lang/String;

    move-result-object v0

    .line 544
    .local v0, "rawText":Ljava/lang/String;
    const/16 v3, 0xd

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 546
    .local v1, "text":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    if-nez v3, :cond_1

    const/4 v2, 0x0

    .line 547
    .local v2, "type":I
    :goto_0
    if-eqz v2, :cond_0

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 549
    :cond_0
    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 553
    :goto_1
    return-object v1

    .line 546
    .end local v2    # "type":I
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->getTextType()I

    move-result v2

    goto :goto_0

    .line 551
    .restart local v2    # "type":I
    :cond_2
    const/16 v3, 0x20

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getTextRuler()Lorg/apache/poi/hslf/record/TextRulerAtom;
    .locals 2

    .prologue
    .line 673
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    if-nez v1, :cond_0

    .line 674
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 682
    .end local v0    # "i":I
    :cond_0
    :goto_1
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    return-object v1

    .line 675
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/poi/hslf/record/TextRulerAtom;

    if-eqz v1, :cond_2

    .line 676
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_records:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/poi/hslf/record/TextRulerAtom;

    iput-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_ruler:Lorg/apache/poi/hslf/record/TextRulerAtom;

    goto :goto_1

    .line 674
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 699
    const-string/jumbo v1, "\\r?\\n"

    const-string/jumbo v2, "\r"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 700
    .local v0, "ns":Ljava/lang/String;
    return-object v0
.end method

.method protected setIndex(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 644
    iput p1, p0, Lorg/apache/poi/hslf/model/TextRun;->slwtIndex:I

    .line 645
    return-void
.end method

.method public setRawText(Ljava/lang/String;)V
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 461
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/TextRun;->storeText(Ljava/lang/String;)V

    .line 462
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v1, v4, v7

    .line 465
    .local v1, "fst":Lorg/apache/poi/hslf/usermodel/RichTextRun;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v4, v4

    if-lt v2, v4, :cond_0

    .line 466
    new-array v4, v6, [Lorg/apache/poi/hslf/usermodel/RichTextRun;

    iput-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    .line 467
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aput-object v1, v4, v7

    .line 474
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v4, :cond_3

    .line 475
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    move-result-object v3

    .line 476
    .local v3, "pStyles":Ljava/util/LinkedList;
    :goto_1
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v6, :cond_1

    .line 478
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_styleAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    move-result-object v0

    .line 479
    .local v0, "cStyles":Ljava/util/LinkedList;
    :goto_2
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v6, :cond_2

    .line 481
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v4, v4, v7

    invoke-virtual {v4, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setText(Ljava/lang/String;)V

    .line 487
    .end local v0    # "cStyles":Ljava/util/LinkedList;
    .end local v3    # "pStyles":Ljava/util/LinkedList;
    :goto_3
    return-void

    .line 465
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    const/4 v5, 0x0

    aput-object v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 476
    .restart local v3    # "pStyles":Ljava/util/LinkedList;
    :cond_1
    invoke-virtual {v3}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 479
    .restart local v0    # "cStyles":Ljava/util/LinkedList;
    :cond_2
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_2

    .line 484
    .end local v0    # "cStyles":Ljava/util/LinkedList;
    .end local v3    # "pStyles":Ljava/util/LinkedList;
    :cond_3
    iget-object v4, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    new-instance v5, Lorg/apache/poi/hslf/usermodel/RichTextRun;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-direct {v5, p0, v7, v6}, Lorg/apache/poi/hslf/usermodel/RichTextRun;-><init>(Lorg/apache/poi/hslf/model/TextRun;II)V

    aput-object v5, v4, v7

    goto :goto_3
.end method

.method public setRunType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 595
    iget-object v0, p0, Lorg/apache/poi/hslf/model/TextRun;->_headerAtom:Lorg/apache/poi/hslf/record/TextHeaderAtom;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->setTextType(I)V

    .line 596
    return-void
.end method

.method protected setShapeId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 630
    iput p1, p0, Lorg/apache/poi/hslf/model/TextRun;->shapeId:I

    .line 631
    return-void
.end method

.method public setSheet(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 0
    .param p1, "sheet"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 612
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TextRun;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    .line 613
    return-void
.end method

.method public setStyleTextProp9Atom(Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V
    .locals 0
    .param p1, "styleTextProp9Atom"    # Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    .prologue
    .line 713
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TextRun;->styleTextProp9Atom:Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    .line 714
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 494
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/model/TextRun;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 495
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/TextRun;->setRawText(Ljava/lang/String;)V

    .line 496
    return-void
.end method

.method public supplySlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V
    .locals 3
    .param p1, "ss"    # Lorg/apache/poi/hslf/usermodel/SlideShow;

    .prologue
    .line 603
    iput-object p1, p0, Lorg/apache/poi/hslf/model/TextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    .line 604
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    if-eqz v1, :cond_0

    .line 605
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 609
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 606
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/model/TextRun;->_rtRuns:[Lorg/apache/poi/hslf/usermodel/RichTextRun;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/poi/hslf/model/TextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->supplySlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V

    .line 605
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
