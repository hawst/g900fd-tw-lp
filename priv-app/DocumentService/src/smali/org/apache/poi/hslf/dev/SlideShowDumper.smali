.class public final Lorg/apache/poi/hslf/dev/SlideShowDumper;
.super Ljava/lang/Object;
.source "SlideShowDumper.java"


# instance fields
.field private _docstream:[B

.field private basicEscher:Z

.field private ddfEscher:Z

.field private istream:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v0, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/dev/SlideShowDumper;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 111
    iput-object p1, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->istream:Ljava/io/InputStream;

    .line 112
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/dev/SlideShowDumper;-><init>(Ljava/io/InputStream;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 4
    .param p1, "filesystem"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v2, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->ddfEscher:Z

    .line 55
    iput-boolean v2, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->basicEscher:Z

    .line 127
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v2

    const-string/jumbo v3, "PowerPoint Document"

    invoke-virtual {v2, v3}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 130
    .local v0, "docProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    .line 132
    const/4 v1, 0x0

    .line 134
    .local v1, "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    const-string/jumbo v2, "PowerPoint Document"

    invoke-virtual {p1, v2}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v1

    .line 135
    iget-object v2, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    invoke-virtual {v1, v2}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    if-eqz v1, :cond_0

    .line 139
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 141
    :cond_0
    return-void

    .line 137
    :catchall_0
    move-exception v2

    .line 138
    if-eqz v1, :cond_1

    .line 139
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 140
    :cond_1
    throw v2
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 63
    array-length v2, p0

    if-nez v2, :cond_0

    .line 64
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v3, "Useage: SlideShowDumper [-escher|-basicescher] <filename>"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 85
    :goto_0
    return-void

    .line 68
    :cond_0
    aget-object v0, p0, v3

    .line 69
    .local v0, "filename":Ljava/lang/String;
    array-length v2, p0

    if-le v2, v4, :cond_1

    .line 70
    aget-object v0, p0, v4

    .line 73
    :cond_1
    new-instance v1, Lorg/apache/poi/hslf/dev/SlideShowDumper;

    invoke-direct {v1, v0}, Lorg/apache/poi/hslf/dev/SlideShowDumper;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, "foo":Lorg/apache/poi/hslf/dev/SlideShowDumper;
    array-length v2, p0

    if-le v2, v4, :cond_2

    .line 76
    aget-object v2, p0, v3

    const-string/jumbo v3, "-escher"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 77
    invoke-virtual {v1, v4}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->setDDFEscher(Z)V

    .line 83
    :cond_2
    :goto_1
    invoke-virtual {v1}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->printDump()V

    .line 84
    invoke-virtual {v1}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->close()V

    goto :goto_0

    .line 79
    :cond_3
    invoke-virtual {v1, v4}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->setBasicEscher(Z)V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->istream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->istream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 171
    :cond_0
    return-void
.end method

.method public makeHex(I)Ljava/lang/String;
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 213
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "hex":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "000"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    .end local v0    # "hex":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 215
    .restart local v0    # "hex":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "00"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public makeHex(S)Ljava/lang/String;
    .locals 3
    .param p1, "s"    # S

    .prologue
    .line 208
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "hex":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    .end local v0    # "hex":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public printDump()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 204
    iget-object v0, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    array-length v0, v0

    invoke-virtual {p0, v1, v1, v0}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkTree(III)V

    .line 205
    return-void
.end method

.method public setBasicEscher(Z)V
    .locals 1
    .param p1, "grok"    # Z

    .prologue
    .line 156
    iput-boolean p1, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->basicEscher:Z

    .line 157
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->ddfEscher:Z

    .line 158
    return-void

    .line 157
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDDFEscher(Z)V
    .locals 1
    .param p1, "grok"    # Z

    .prologue
    .line 147
    iput-boolean p1, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->ddfEscher:Z

    .line 148
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->basicEscher:Z

    .line 149
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public walkEscherBasic(III)V
    .locals 11
    .param p1, "indent"    # I
    .param p2, "pos"    # I
    .param p3, "len"    # I

    .prologue
    .line 372
    const/16 v8, 0x8

    if-ge p3, v8, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v8, ""

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 379
    .local v5, "ind":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-lt v4, p1, :cond_5

    .line 383
    iget-object v8, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    add-int/lit8 v9, p2, 0x2

    invoke-static {v8, v9}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v8

    int-to-long v6, v8

    .line 384
    .local v6, "type":J
    iget-object v8, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    add-int/lit8 v9, p2, 0x4

    invoke-static {v8, v9}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    .line 409
    .local v0, "atomlen":J
    const-wide/32 v8, 0xf00d

    cmp-long v8, v6, v8

    if-nez v8, :cond_2

    .line 412
    const/16 v4, 0x8

    :goto_2
    const/16 v8, 0x10

    if-lt v4, v8, :cond_6

    .line 419
    const/16 v4, 0x14

    :goto_3
    const/16 v8, 0x1c

    if-lt v4, v8, :cond_8

    .line 432
    :cond_2
    const-wide/32 v8, 0xf003

    cmp-long v8, v6, v8

    if-eqz v8, :cond_3

    const-wide/32 v8, 0xf004

    cmp-long v8, v6, v8

    if-nez v8, :cond_4

    .line 433
    :cond_3
    add-int/lit8 v8, p1, 0x3

    add-int/lit8 v9, p2, 0x8

    long-to-int v10, v0

    invoke-virtual {p0, v8, v9, v10}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherBasic(III)V

    .line 437
    :cond_4
    int-to-long v8, p3

    cmp-long v8, v0, v8

    if-gez v8, :cond_0

    .line 438
    long-to-int v2, v0

    .line 439
    .local v2, "atomleni":I
    add-int v8, p2, v2

    add-int/lit8 v8, v8, 0x8

    sub-int v9, p3, v2

    add-int/lit8 v9, v9, -0x8

    invoke-virtual {p0, p1, v8, v9}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherBasic(III)V

    goto :goto_0

    .line 380
    .end local v0    # "atomlen":J
    .end local v2    # "atomleni":I
    .end local v6    # "type":J
    :cond_5
    const-string/jumbo v8, " "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 413
    .restart local v0    # "atomlen":J
    .restart local v6    # "type":J
    :cond_6
    iget-object v8, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    add-int v9, v4, p2

    aget-byte v3, v8, v9

    .line 414
    .local v3, "bv":S
    if-gez v3, :cond_7

    add-int/lit16 v8, v3, 0x100

    int-to-short v8, v8

    .line 412
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 420
    .end local v3    # "bv":S
    :cond_8
    iget-object v8, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    add-int v9, v4, p2

    aget-byte v3, v8, v9

    .line 421
    .restart local v3    # "bv":S
    if-gez v3, :cond_9

    add-int/lit16 v8, v3, 0x100

    int-to-short v8, v8

    .line 419
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method public walkEscherDDF(III)V
    .locals 15
    .param p1, "indent"    # I
    .param p2, "pos"    # I
    .param p3, "len"    # I

    .prologue
    .line 287
    const/16 v12, 0x8

    move/from16 v0, p3

    if-ge v0, v12, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v12, ""

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 294
    .local v9, "ind":Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    move/from16 v0, p1

    if-lt v8, v0, :cond_6

    .line 298
    move/from16 v0, p3

    new-array v6, v0, [B

    .line 299
    .local v6, "contents":[B
    iget-object v12, p0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    const/4 v13, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v12, v0, v6, v13, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 300
    new-instance v7, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v7}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 301
    .local v7, "erf":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    const/4 v12, 0x0

    invoke-virtual {v7, v6, v12}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v10

    .line 304
    .local v10, "record":Lorg/apache/poi/ddf/EscherRecord;
    const/4 v12, 0x0

    invoke-virtual {v10, v6, v12, v7}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 306
    const/4 v12, 0x2

    invoke-static {v6, v12}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v12

    int-to-long v4, v12

    .line 308
    .local v4, "atomType":J
    const/4 v12, 0x4

    invoke-static {v6, v12}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v12

    int-to-long v2, v12

    .line 310
    .local v2, "atomLen":J
    invoke-virtual {v10}, Lorg/apache/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v11

    .line 317
    .local v11, "recordLen":I
    const/16 v12, 0x8

    if-eq v11, v12, :cond_2

    .line 322
    :cond_2
    instance-of v12, v10, Lorg/apache/poi/ddf/EscherContainerRecord;

    if-eqz v12, :cond_3

    .line 325
    add-int/lit8 v12, p1, 0x3

    add-int/lit8 v13, p2, 0x8

    long-to-int v14, v2

    invoke-virtual {p0, v12, v13, v14}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherDDF(III)V

    .line 331
    :cond_3
    const-wide/32 v12, 0xf00b

    cmp-long v12, v4, v12

    if-nez v12, :cond_4

    .line 333
    long-to-int v12, v2

    add-int/lit8 v11, v12, 0x8

    .line 335
    :cond_4
    const-wide/32 v12, 0xf00d

    cmp-long v12, v4, v12

    if-nez v12, :cond_5

    .line 337
    long-to-int v12, v2

    add-int/lit8 v11, v12, 0x8

    .line 338
    const/4 v12, 0x0

    invoke-virtual {v10, v6, v12, v7}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 339
    instance-of v12, v10, Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 345
    :cond_5
    const/16 v12, 0x8

    if-ne v11, v12, :cond_7

    const-wide/16 v12, 0x8

    cmp-long v12, v2, v12

    if-lez v12, :cond_7

    .line 347
    add-int/lit8 v12, p1, 0x3

    add-int/lit8 v13, p2, 0x8

    long-to-int v14, v2

    invoke-virtual {p0, v12, v13, v14}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherDDF(III)V

    .line 350
    move/from16 v0, p2

    int-to-long v12, v0

    add-long/2addr v12, v2

    long-to-int v0, v12

    move/from16 p2, v0

    .line 351
    add-int/lit8 p2, p2, 0x8

    .line 352
    move/from16 v0, p3

    int-to-long v12, v0

    sub-long/2addr v12, v2

    long-to-int v0, v12

    move/from16 p3, v0

    .line 353
    add-int/lit8 p3, p3, -0x8

    .line 363
    :goto_2
    const/16 v12, 0x8

    move/from16 v0, p3

    if-lt v0, v12, :cond_0

    .line 364
    invoke-virtual/range {p0 .. p3}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherDDF(III)V

    goto/16 :goto_0

    .line 295
    .end local v2    # "atomLen":J
    .end local v4    # "atomType":J
    .end local v6    # "contents":[B
    .end local v7    # "erf":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    .end local v10    # "record":Lorg/apache/poi/ddf/EscherRecord;
    .end local v11    # "recordLen":I
    :cond_6
    const-string/jumbo v12, " "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 356
    .restart local v2    # "atomLen":J
    .restart local v4    # "atomType":J
    .restart local v6    # "contents":[B
    .restart local v7    # "erf":Lorg/apache/poi/ddf/DefaultEscherRecordFactory;
    .restart local v10    # "record":Lorg/apache/poi/ddf/EscherRecord;
    .restart local v11    # "recordLen":I
    :cond_7
    move/from16 v0, p2

    int-to-long v12, v0

    add-long/2addr v12, v2

    long-to-int v0, v12

    move/from16 p2, v0

    .line 357
    add-int/lit8 p2, p2, 0x8

    .line 358
    move/from16 v0, p3

    int-to-long v12, v0

    sub-long/2addr v12, v2

    long-to-int v0, v12

    move/from16 p3, v0

    .line 359
    add-int/lit8 p3, p3, -0x8

    goto :goto_2
.end method

.method public walkTree(III)V
    .locals 18
    .param p1, "depth"    # I
    .param p2, "startPos"    # I
    .param p3, "maxLen"    # I

    .prologue
    .line 221
    move/from16 v10, p2

    .line 222
    .local v10, "pos":I
    add-int v3, p2, p3

    .line 223
    .local v3, "endPos":I
    move/from16 v6, p1

    .line 224
    .local v6, "indent":I
    :goto_0
    add-int/lit8 v14, v3, -0x8

    if-le v10, v14, :cond_0

    .line 281
    return-void

    .line 225
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    add-int/lit8 v15, v10, 0x2

    invoke-static {v14, v15}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v14

    int-to-long v12, v14

    .line 226
    .local v12, "type":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    add-int/lit8 v15, v10, 0x4

    invoke-static {v14, v15}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v8

    .line 227
    .local v8, "len":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->_docstream:[B

    aget-byte v7, v14, v10

    .line 233
    .local v7, "opt":B
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v14, ""

    invoke-direct {v5, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 234
    .local v5, "ind":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-lt v4, v6, :cond_4

    .line 242
    long-to-int v14, v12

    invoke-static {v14}, Lorg/apache/poi/hslf/record/RecordTypes;->recordName(I)Ljava/lang/String;

    move-result-object v11

    .line 245
    .local v11, "recordName":Ljava/lang/String;
    add-int/lit8 v10, v10, 0x8

    .line 246
    if-eqz v11, :cond_3

    .line 250
    and-int/lit8 v2, v7, 0xf

    .line 254
    .local v2, "container":I
    const-wide/16 v14, 0x138b

    cmp-long v14, v12, v14

    if-nez v14, :cond_1

    int-to-long v14, v7

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_1

    .line 255
    const/16 v2, 0xf

    .line 258
    :cond_1
    const-wide/16 v14, 0x0

    cmp-long v14, v12, v14

    if-eqz v14, :cond_3

    const/16 v14, 0xf

    if-ne v2, v14, :cond_3

    .line 260
    const-wide/16 v14, 0x40b

    cmp-long v14, v12, v14

    if-eqz v14, :cond_2

    const-wide/16 v14, 0x40c

    cmp-long v14, v12, v14

    if-nez v14, :cond_6

    .line 264
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->ddfEscher:Z

    if-eqz v14, :cond_5

    .line 266
    add-int/lit8 v14, v6, 0x3

    add-int/lit8 v15, v10, 0x8

    long-to-int v0, v8

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x8

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v14, v15, v1}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherDDF(III)V

    .line 279
    .end local v2    # "container":I
    :cond_3
    :goto_2
    long-to-int v14, v8

    add-int/2addr v10, v14

    goto :goto_0

    .line 235
    .end local v11    # "recordName":Ljava/lang/String;
    :cond_4
    const-string/jumbo v14, " "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 267
    .restart local v2    # "container":I
    .restart local v11    # "recordName":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/poi/hslf/dev/SlideShowDumper;->basicEscher:Z

    if-eqz v14, :cond_3

    .line 268
    add-int/lit8 v14, v6, 0x3

    add-int/lit8 v15, v10, 0x8

    long-to-int v0, v8

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x8

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v14, v15, v1}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkEscherBasic(III)V

    goto :goto_2

    .line 273
    :cond_6
    add-int/lit8 v14, v6, 0x2

    long-to-int v15, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v10, v15}, Lorg/apache/poi/hslf/dev/SlideShowDumper;->walkTree(III)V

    goto :goto_2
.end method
