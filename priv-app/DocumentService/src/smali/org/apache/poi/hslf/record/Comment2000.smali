.class public final Lorg/apache/poi/hslf/record/Comment2000;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "Comment2000.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private authorInitialsRecord:Lorg/apache/poi/hslf/record/CString;

.field private authorRecord:Lorg/apache/poi/hslf/record/CString;

.field private commentAtom:Lorg/apache/poi/hslf/record/Comment2000Atom;

.field private commentRecord:Lorg/apache/poi/hslf/record/CString;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x2ee0

    sput-wide v0, Lorg/apache/poi/hslf/record/Comment2000;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 140
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 141
    const/16 v3, 0x8

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    .line 142
    const/4 v3, 0x4

    new-array v3, v3, [Lorg/apache/poi/hslf/record/Record;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 145
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    const/16 v4, 0xf

    aput-byte v4, v3, v6

    .line 146
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    sget-wide v4, Lorg/apache/poi/hslf/record/Comment2000;->_type:J

    long-to-int v4, v4

    int-to-short v4, v4

    invoke-static {v3, v7, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 149
    new-instance v0, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 150
    .local v0, "csa":Lorg/apache/poi/hslf/record/CString;
    new-instance v1, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 151
    .local v1, "csb":Lorg/apache/poi/hslf/record/CString;
    new-instance v2, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 152
    .local v2, "csc":Lorg/apache/poi/hslf/record/CString;
    invoke-virtual {v0, v6}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 153
    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 154
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 155
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    aput-object v0, v3, v6

    .line 156
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 157
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    aput-object v2, v3, v7

    .line 158
    iget-object v3, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v4, 0x3

    new-instance v5, Lorg/apache/poi/hslf/record/Comment2000Atom;

    invoke-direct {v5}, Lorg/apache/poi/hslf/record/Comment2000Atom;-><init>()V

    aput-object v5, v3, v4

    .line 159
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/Comment2000;->findInterestingChildren()V

    .line 160
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 102
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 104
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    .line 105
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 109
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/Comment2000;->findInterestingChildren()V

    .line 110
    return-void
.end method

.method private findInterestingChildren()V
    .locals 12

    .prologue
    .line 119
    iget-object v4, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 135
    return-void

    .line 119
    :cond_0
    aget-object v1, v4, v3

    .line 120
    .local v1, "r":Lorg/apache/poi/hslf/record/Record;
    instance-of v6, v1, Lorg/apache/poi/hslf/record/CString;

    if-eqz v6, :cond_1

    move-object v0, v1

    .line 121
    check-cast v0, Lorg/apache/poi/hslf/record/CString;

    .line 122
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getOptions()I

    move-result v6

    shr-int/lit8 v2, v6, 0x4

    .line 123
    .local v2, "recInstance":I
    packed-switch v2, :pswitch_data_0

    .line 119
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    .end local v1    # "r":Lorg/apache/poi/hslf/record/Record;
    .end local v2    # "recInstance":I
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 124
    .restart local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    .restart local v1    # "r":Lorg/apache/poi/hslf/record/Record;
    .restart local v2    # "recInstance":I
    :pswitch_0
    iput-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1

    .line 125
    :pswitch_1
    iput-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1

    .line 126
    :pswitch_2
    iput-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/poi/hslf/record/CString;

    goto :goto_1

    .line 128
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    .end local v2    # "recInstance":I
    :cond_1
    instance-of v6, v1, Lorg/apache/poi/hslf/record/Comment2000Atom;

    if-eqz v6, :cond_2

    .line 129
    check-cast v1, Lorg/apache/poi/hslf/record/Comment2000Atom;

    .end local v1    # "r":Lorg/apache/poi/hslf/record/Record;
    iput-object v1, p0, Lorg/apache/poi/hslf/record/Comment2000;->commentAtom:Lorg/apache/poi/hslf/record/Comment2000Atom;

    goto :goto_1

    .line 131
    .restart local v1    # "r":Lorg/apache/poi/hslf/record/Record;
    :cond_2
    sget-object v6, Lorg/apache/poi/hslf/record/Comment2000;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Unexpected record with type="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " in Comment2000: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAuthorInitials()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getComment2000Atom()Lorg/apache/poi/hslf/record/Comment2000Atom;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->commentAtom:Lorg/apache/poi/hslf/record/Comment2000Atom;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 165
    sget-wide v0, Lorg/apache/poi/hslf/record/Comment2000;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public setAuthorInitials(Ljava/lang/String;)V
    .locals 1
    .param p1, "initials"    # Ljava/lang/String;

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/Comment2000;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/Comment2000;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/Comment2000;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/Comment2000;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 173
    return-void
.end method
