.class public final Lorg/apache/poi/hslf/record/MainMaster;
.super Lorg/apache/poi/hslf/record/SheetContainer;
.source "MainMaster.java"


# static fields
.field private static _type:J


# instance fields
.field private _colorScheme:Lorg/apache/poi/hslf/record/ColorSchemeAtom;

.field private _header:[B

.field private clrscheme:[Lorg/apache/poi/hslf/record/ColorSchemeAtom;

.field private ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

.field private slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

.field private txmasters:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-wide/16 v0, 0x3f8

    sput-wide v0, Lorg/apache/poi/hslf/record/MainMaster;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 6
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v5, 0x8

    .line 58
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/SheetContainer;-><init>()V

    .line 60
    new-array v3, v5, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_header:[B

    .line 61
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_header:[B

    const/4 v4, 0x0

    invoke-static {p1, p2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    add-int/lit8 v3, p2, 0x8

    add-int/lit8 v4, p3, -0x8

    invoke-static {p1, v3, v4}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v2, "tx":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/TxMasterStyleAtom;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v0, "clr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/ColorSchemeAtom;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 85
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->txmasters:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    .line 86
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->clrscheme:[Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    .line 87
    return-void

    .line 70
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/SlideAtom;

    if-eqz v3, :cond_3

    .line 71
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    check-cast v3, Lorg/apache/poi/hslf/record/SlideAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

    .line 80
    :cond_1
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    if-eqz v3, :cond_2

    .line 81
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    check-cast v3, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_colorScheme:Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    .line 69
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    :cond_3
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/PPDrawing;

    if-eqz v3, :cond_4

    .line 73
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    check-cast v3, Lorg/apache/poi/hslf/record/PPDrawing;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    goto :goto_1

    .line 74
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    if-eqz v3, :cond_5

    .line 75
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    check-cast v3, Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 76
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    if-eqz v3, :cond_1

    .line 77
    iget-object v3, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    check-cast v3, Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->_colorScheme:Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    return-object v0
.end method

.method public getColorSchemeAtoms()[Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->clrscheme:[Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    return-object v0
.end method

.method public getPPDrawing()Lorg/apache/poi/hslf/record/PPDrawing;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->ppDrawing:Lorg/apache/poi/hslf/record/PPDrawing;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 92
    sget-wide v0, Lorg/apache/poi/hslf/record/MainMaster;->_type:J

    return-wide v0
.end method

.method public getSlideAtom()Lorg/apache/poi/hslf/record/SlideAtom;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->slideAtom:Lorg/apache/poi/hslf/record/SlideAtom;

    return-object v0
.end method

.method public getTxMasterStyleAtoms()[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->txmasters:[Lorg/apache/poi/hslf/record/TxMasterStyleAtom;

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/MainMaster;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    sget-wide v4, Lorg/apache/poi/hslf/record/MainMaster;->_type:J

    iget-object v6, p0, Lorg/apache/poi/hslf/record/MainMaster;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/MainMaster;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 100
    return-void
.end method
