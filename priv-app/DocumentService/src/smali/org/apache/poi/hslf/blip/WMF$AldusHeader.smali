.class public Lorg/apache/poi/hslf/blip/WMF$AldusHeader;
.super Ljava/lang/Object;
.source "WMF.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hslf/blip/WMF;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AldusHeader"
.end annotation


# static fields
.field public static final APMHEADER_KEY:I = -0x65393229


# instance fields
.field public bottom:I

.field public checksum:I

.field public handle:I

.field public inch:I

.field public left:I

.field public reserved:I

.field public right:I

.field final synthetic this$0:Lorg/apache/poi/hslf/blip/WMF;

.field public top:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/blip/WMF;)V
    .locals 1

    .prologue
    .line 123
    iput-object p1, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->this$0:Lorg/apache/poi/hslf/blip/WMF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    const/16 v0, 0x48

    iput v0, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->inch:I

    return-void
.end method


# virtual methods
.method public getChecksum()I
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    .line 158
    .local v0, "checksum":I
    const v1, 0xcdd7

    xor-int/2addr v0, v1

    .line 159
    xor-int/lit16 v0, v0, -0x653a

    .line 160
    iget v1, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->left:I

    xor-int/2addr v0, v1

    .line 161
    iget v1, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->top:I

    xor-int/2addr v0, v1

    .line 162
    iget v1, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->right:I

    xor-int/2addr v0, v1

    .line 163
    iget v1, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->bottom:I

    xor-int/2addr v0, v1

    .line 164
    iget v1, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->inch:I

    xor-int/2addr v0, v1

    .line 165
    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 187
    const/16 v0, 0x16

    return v0
.end method

.method public read([BI)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 133
    move v1, p2

    .line 134
    .local v1, "pos":I
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .local v0, "key":I
    add-int/lit8 v1, v1, 0x4

    .line 135
    const v2, -0x65393229

    if-eq v0, v2, :cond_0

    new-instance v2, Lorg/apache/poi/hslf/exceptions/HSLFException;

    const-string/jumbo v3, "Not a valid WMF file"

    invoke-direct {v2, v3}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 137
    :cond_0
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->handle:I

    add-int/lit8 v1, v1, 0x2

    .line 138
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->left:I

    add-int/lit8 v1, v1, 0x2

    .line 139
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->top:I

    add-int/lit8 v1, v1, 0x2

    .line 140
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->right:I

    add-int/lit8 v1, v1, 0x2

    .line 141
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->bottom:I

    add-int/lit8 v1, v1, 0x2

    .line 143
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->inch:I

    add-int/lit8 v1, v1, 0x2

    .line 144
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->reserved:I

    add-int/lit8 v1, v1, 0x4

    .line 146
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->checksum:I

    add-int/lit8 v1, v1, 0x2

    .line 147
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->checksum:I

    invoke-virtual {p0}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->getChecksum()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 148
    iget-object v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->this$0:Lorg/apache/poi/hslf/blip/WMF;

    # getter for: Lorg/apache/poi/hslf/blip/WMF;->logger:Lorg/apache/poi/util/POILogger;
    invoke-static {v2}, Lorg/apache/poi/hslf/blip/WMF;->access$0(Lorg/apache/poi/hslf/blip/WMF;)Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const/4 v3, 0x5

    const-string/jumbo v4, "WMF checksum does not match the header data"

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 150
    :cond_1
    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 169
    const/16 v2, 0x16

    new-array v0, v2, [B

    .line 170
    .local v0, "header":[B
    const/4 v1, 0x0

    .line 171
    .local v1, "pos":I
    const v2, -0x65393229

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 172
    invoke-static {v0, v1, v3}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    add-int/lit8 v1, v1, 0x2

    .line 173
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->left:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    add-int/lit8 v1, v1, 0x2

    .line 174
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->top:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    add-int/lit8 v1, v1, 0x2

    .line 175
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->right:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    add-int/lit8 v1, v1, 0x2

    .line 176
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->bottom:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    add-int/lit8 v1, v1, 0x2

    .line 177
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->inch:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    add-int/lit8 v1, v1, 0x2

    .line 178
    invoke-static {v0, v1, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v1, v1, 0x4

    .line 180
    invoke-virtual {p0}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->getChecksum()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->checksum:I

    .line 181
    iget v2, p0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->checksum:I

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 183
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 184
    return-void
.end method
