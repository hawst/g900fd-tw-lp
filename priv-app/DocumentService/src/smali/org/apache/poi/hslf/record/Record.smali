.class public abstract Lorg/apache/poi/hslf/record/Record;
.super Ljava/lang/Object;
.source "Record.java"


# static fields
.field protected static logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/poi/hslf/record/Record;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hslf/record/Record;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildRecordAtOffset([BI)Lorg/apache/poi/hslf/record/Record;
    .locals 6
    .param p0, "b"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 96
    add-int/lit8 v3, p1, 0x2

    invoke-static {p0, v3}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v3

    int-to-long v4, v3

    .line 97
    .local v4, "type":J
    add-int/lit8 v3, p1, 0x4

    invoke-static {p0, v3}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    .line 100
    .local v0, "rlen":J
    long-to-int v2, v0

    .line 101
    .local v2, "rleni":I
    if-gez v2, :cond_0

    const/4 v2, 0x0

    .line 103
    :cond_0
    add-int/lit8 v3, v2, 0x8

    invoke-static {v4, v5, p0, p1, v3}, Lorg/apache/poi/hslf/record/Record;->createRecordForType(J[BII)Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    return-object v3
.end method

.method public static createRecordForType(J[BII)Lorg/apache/poi/hslf/record/Record;
    .locals 14
    .param p0, "type"    # J
    .param p2, "b"    # [B
    .param p3, "start"    # I
    .param p4, "len"    # I

    .prologue
    .line 153
    const/4 v8, 0x0

    .line 157
    .local v8, "toReturn":Lorg/apache/poi/hslf/record/Record;
    add-int v9, p3, p4

    move-object/from16 v0, p2

    array-length v10, v0

    if-le v9, v10, :cond_0

    .line 158
    sget-object v9, Lorg/apache/poi/hslf/record/Record;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v10, 0x5

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Warning: Skipping record of type "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " at position "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " which claims to be longer than the file! ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " vs "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p2

    array-length v12, v0

    sub-int v12, v12, p3

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 159
    const/4 v9, 0x0

    .line 201
    :goto_0
    return-object v9

    .line 167
    :cond_0
    const/4 v1, 0x0

    .line 169
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/hslf/record/Record;>;"
    long-to-int v9, p0

    :try_start_0
    invoke-static {v9}, Lorg/apache/poi/hslf/record/RecordTypes;->recordHandlingClass(I)Ljava/lang/Class;

    move-result-object v1

    .line 170
    if-nez v1, :cond_1

    .line 175
    sget-object v9, Lorg/apache/poi/hslf/record/RecordTypes;->Unknown:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v9, v9, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {v9}, Lorg/apache/poi/hslf/record/RecordTypes;->recordHandlingClass(I)Ljava/lang/Class;

    move-result-object v1

    .line 179
    :cond_1
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, [B

    aput-object v11, v9, v10

    const/4 v10, 0x1

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    invoke-virtual {v1, v9}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 181
    .local v2, "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/poi/hslf/record/Record;>;"
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    const/4 v10, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "toReturn":Lorg/apache/poi/hslf/record/Record;
    check-cast v8, Lorg/apache/poi/hslf/record/Record;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    .line 195
    .restart local v8    # "toReturn":Lorg/apache/poi/hslf/record/Record;
    instance-of v9, v8, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    if-eqz v9, :cond_2

    move-object v7, v8

    .line 196
    check-cast v7, Lorg/apache/poi/hslf/record/PositionDependentRecord;

    .line 197
    .local v7, "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    move/from16 v0, p3

    invoke-interface {v7, v0}, Lorg/apache/poi/hslf/record/PositionDependentRecord;->setLastOnDiskOffset(I)V

    .end local v7    # "pdr":Lorg/apache/poi/hslf/record/PositionDependentRecord;
    :cond_2
    move-object v9, v8

    .line 201
    goto :goto_0

    .line 182
    .end local v2    # "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/poi/hslf/record/Record;>;"
    .end local v8    # "toReturn":Lorg/apache/poi/hslf/record/Record;
    :catch_0
    move-exception v4

    .line 183
    .local v4, "ie":Ljava/lang/InstantiationException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t instantiate the class for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 184
    .end local v4    # "ie":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v5

    .line 185
    .local v5, "ite":Ljava/lang/reflect/InvocationTargetException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t instantiate the class for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\nCause was : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 186
    .end local v5    # "ite":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v3

    .line 187
    .local v3, "iae":Ljava/lang/IllegalAccessException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t access the constructor for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 188
    .end local v3    # "iae":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v6

    .line 189
    .local v6, "nsme":Ljava/lang/NoSuchMethodException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t access the constructor for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
.end method

.method public static findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;
    .locals 12
    .param p0, "b"    # [B
    .param p1, "start"    # I
    .param p2, "len"    # I

    .prologue
    .line 110
    new-instance v1, Ljava/util/ArrayList;

    const/4 v7, 0x5

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hslf/record/Record;>;"
    move v2, p1

    .line 114
    .local v2, "pos":I
    :goto_0
    add-int v7, p1, p2

    add-int/lit8 v7, v7, -0x8

    if-le v2, v7, :cond_0

    .line 139
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Lorg/apache/poi/hslf/record/Record;

    invoke-interface {v1, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/hslf/record/Record;

    .line 140
    .local v0, "cRecords":[Lorg/apache/poi/hslf/record/Record;
    return-object v0

    .line 115
    .end local v0    # "cRecords":[Lorg/apache/poi/hslf/record/Record;
    :cond_0
    add-int/lit8 v7, v2, 0x2

    invoke-static {p0, v7}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v7

    int-to-long v8, v7

    .line 116
    .local v8, "type":J
    add-int/lit8 v7, v2, 0x4

    invoke-static {p0, v7}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    .line 119
    .local v4, "rlen":J
    long-to-int v6, v4

    .line 120
    .local v6, "rleni":I
    if-gez v6, :cond_1

    const/4 v6, 0x0

    .line 124
    :cond_1
    if-nez v2, :cond_2

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-nez v7, :cond_2

    const v7, 0xffff

    if-ne v6, v7, :cond_2

    .line 125
    new-instance v7, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    const-string/jumbo v10, "Corrupt document - starts with record of type 0000 and length 0xFFFF"

    invoke-direct {v7, v10}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 128
    :cond_2
    add-int/lit8 v7, v6, 0x8

    invoke-static {v8, v9, p0, v2, v7}, Lorg/apache/poi/hslf/record/Record;->createRecordForType(J[BII)Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    .line 129
    .local v3, "r":Lorg/apache/poi/hslf/record/Record;
    if-eqz v3, :cond_3

    .line 130
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_3
    add-int/lit8 v2, v2, 0x8

    .line 135
    add-int/2addr v2, v6

    goto :goto_0
.end method

.method public static writeLittleEndian(ILjava/io/OutputStream;)V
    .locals 2
    .param p0, "i"    # I
    .param p1, "o"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 77
    .local v0, "bi":[B
    invoke-static {v0, p0}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 78
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 79
    return-void
.end method

.method public static writeLittleEndian(SLjava/io/OutputStream;)V
    .locals 2
    .param p0, "s"    # S
    .param p1, "o"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 85
    .local v0, "bs":[B
    invoke-static {v0, p0}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 86
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 87
    return-void
.end method


# virtual methods
.method public abstract getChildRecords()[Lorg/apache/poi/hslf/record/Record;
.end method

.method public abstract getRecordType()J
.end method

.method public abstract isAnAtom()Z
.end method

.method public abstract writeOut(Ljava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
