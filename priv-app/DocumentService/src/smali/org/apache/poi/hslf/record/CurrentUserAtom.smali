.class public Lorg/apache/poi/hslf/record/CurrentUserAtom;
.super Ljava/lang/Object;
.source "CurrentUserAtom.java"


# static fields
.field public static final atomHeader:[B

.field public static final encHeaderToken:[B

.field public static final headerToken:[B

.field private static logger:Lorg/apache/poi/util/POILogger;

.field public static final ppt97FileVer:[B


# instance fields
.field private _contents:[B

.field private currentEditOffset:J

.field private docFinalVersion:I

.field private docMajorNo:B

.field private docMinorNo:B

.field private lastEditUser:Ljava/lang/String;

.field private releaseVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 44
    const-class v0, Lorg/apache/poi/hslf/record/CurrentUserAtom;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->logger:Lorg/apache/poi/util/POILogger;

    .line 47
    new-array v0, v4, [B

    const/16 v1, -0xa

    aput-byte v1, v0, v5

    const/16 v1, 0xf

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->atomHeader:[B

    .line 49
    new-array v0, v4, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->headerToken:[B

    .line 51
    new-array v0, v4, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    .line 53
    const/4 v0, 0x6

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x8

    aput-byte v2, v0, v1

    const/16 v1, -0xd

    aput-byte v1, v0, v5

    aput-byte v3, v0, v3

    aput-byte v3, v0, v4

    sput-object v0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->ppt97FileVer:[B

    return-void

    .line 49
    :array_0
    .array-data 1
        0x5ft
        -0x40t
        -0x6ft
        -0x1dt
    .end array-data

    .line 51
    :array_1
    .array-data 1
        -0x21t
        -0x3ct
        -0x2ft
        -0xdt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 97
    const/16 v0, 0x3f4

    iput v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    .line 98
    const/4 v0, 0x3

    iput-byte v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 99
    iput-byte v1, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    .line 100
    const-wide/16 v0, 0x8

    iput-wide v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 101
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 102
    const-string/jumbo v0, "Apache POI"

    iput-object v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 6
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const-string/jumbo v3, "Current User"

    invoke-virtual {p1, v3}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 120
    .local v0, "docProps":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v3

    const/high16 v4, 0x20000

    if-le v3, v4, :cond_0

    .line 121
    new-instance v3, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "The Current User stream is implausably long. It\'s normally 28-200 bytes long, but was "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " bytes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 125
    :cond_0
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 126
    const-string/jumbo v3, "Current User"

    invoke-virtual {p1, v3}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v1

    .line 130
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    if-eqz v1, :cond_1

    .line 135
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 136
    const/4 v1, 0x0

    .line 141
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v3, v3

    const/16 v4, 0x1c

    if-ge v3, v4, :cond_5

    .line 142
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v3, v3

    const/4 v4, 0x4

    if-lt v3, v4, :cond_4

    .line 144
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v3}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v2

    .line 146
    .local v2, "size":I
    add-int/lit8 v3, v2, 0x4

    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v4, v4

    if-ne v3, v4, :cond_4

    .line 148
    if-eqz v1, :cond_2

    .line 149
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 150
    const/4 v1, 0x0

    .line 152
    :cond_2
    new-instance v3, Lorg/apache/poi/hslf/exceptions/OldPowerPointFormatException;

    const-string/jumbo v4, "Based on the Current User stream, you seem to have supplied a PowerPoint95 file, which isn\'t supported"

    invoke-direct {v3, v4}, Lorg/apache/poi/hslf/exceptions/OldPowerPointFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 133
    .end local v2    # "size":I
    :catchall_0
    move-exception v3

    .line 134
    if-eqz v1, :cond_3

    .line 135
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 136
    const/4 v1, 0x0

    .line 138
    :cond_3
    throw v3

    .line 155
    :cond_4
    new-instance v3, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "The Current User stream must be at least 28 bytes long, but was only "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 159
    :cond_5
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->init()V

    .line 161
    if-eqz v1, :cond_6

    .line 162
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 163
    const/4 v1, 0x0

    .line 165
    :cond_6
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/record/CurrentUserAtom;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 110
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "b"    # [B

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput-object p1, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 172
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->init()V

    .line 173
    return-void
.end method

.method private init()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 181
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xc

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    aget-byte v6, v6, v9

    if-ne v3, v6, :cond_0

    .line 182
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xd

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    const/4 v7, 0x1

    aget-byte v6, v6, v7

    if-ne v3, v6, :cond_0

    .line 183
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xe

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    const/4 v7, 0x2

    aget-byte v6, v6, v7

    if-ne v3, v6, :cond_0

    .line 184
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xf

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    const/4 v7, 0x3

    aget-byte v6, v6, v7

    if-ne v3, v6, :cond_0

    .line 185
    new-instance v3, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;

    const-string/jumbo v6, "The CurrentUserAtom specifies that the document is encrypted"

    invoke-direct {v3, v6}, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 189
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x10

    invoke-static {v3, v6}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 192
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x16

    invoke-static {v3, v6}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    .line 193
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x18

    aget-byte v3, v3, v6

    iput-byte v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 194
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x19

    aget-byte v3, v3, v6

    iput-byte v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    .line 197
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x14

    invoke-static {v3, v6}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v3

    int-to-long v4, v3

    .line 198
    .local v4, "usernameLen":J
    const-wide/16 v6, 0x200

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 200
    sget-object v3, Lorg/apache/poi/hslf/record/CurrentUserAtom;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v6, 0x5

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Warning - invalid username length "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " found, treating as if there was no username set"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 201
    const-wide/16 v4, 0x0

    .line 206
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v3, v3

    long-to-int v6, v4

    add-int/lit8 v6, v6, 0x1c

    add-int/lit8 v6, v6, 0x4

    if-lt v3, v6, :cond_2

    .line 207
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    long-to-int v6, v4

    add-int/lit8 v6, v6, 0x1c

    invoke-static {v3, v6}, Lorg/apache/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 214
    :goto_0
    long-to-int v3, v4

    add-int/lit8 v3, v3, 0x1c

    add-int/lit8 v1, v3, 0x4

    .line 215
    .local v1, "start":I
    long-to-int v3, v4

    mul-int/lit8 v0, v3, 0x2

    .line 217
    .local v0, "len":I
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v3, v3

    add-int v6, v1, v0

    if-lt v3, v6, :cond_3

    .line 218
    new-array v2, v0, [B

    .line 219
    .local v2, "textBytes":[B
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v3, v1, v2, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 220
    invoke-static {v2}, Lorg/apache/poi/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 227
    :goto_1
    return-void

    .line 210
    .end local v0    # "len":I
    .end local v1    # "start":I
    .end local v2    # "textBytes":[B
    :cond_2
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    goto :goto_0

    .line 223
    .restart local v0    # "len":I
    .restart local v1    # "start":I
    :cond_3
    long-to-int v3, v4

    new-array v2, v3, [B

    .line 224
    .restart local v2    # "textBytes":[B
    iget-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x1c

    long-to-int v7, v4

    invoke-static {v3, v6, v2, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 225
    long-to-int v3, v4

    invoke-static {v2, v9, v3}, Lorg/apache/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public getCurrentEditOffset()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    return-wide v0
.end method

.method public getDocFinalVersion()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    return v0
.end method

.method public getDocMajorNo()B
    .locals 1

    .prologue
    .line 74
    iget-byte v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    return v0
.end method

.method public getDocMinorNo()B
    .locals 1

    .prologue
    .line 75
    iget-byte v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    return v0
.end method

.method public getLastEditUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    return-object v0
.end method

.method public getReleaseVersion()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    return-wide v0
.end method

.method public setCurrentEditOffset(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 82
    iput-wide p1, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    return-void
.end method

.method public setLastEditUsername(Ljava/lang/String;)V
    .locals 0
    .param p1, "u"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    return-void
.end method

.method public setReleaseVersion(J)V
    .locals 1
    .param p1, "rv"    # J

    .prologue
    .line 78
    iput-wide p1, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 10
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x14

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 239
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x3

    add-int/lit8 v2, v4, 0x20

    .line 240
    .local v2, "size":I
    new-array v4, v2, [B

    iput-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 243
    sget-object v4, Lorg/apache/poi/hslf/record/CurrentUserAtom;->atomHeader:[B

    iget-object v5, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v4, v8, v5, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 245
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v1, v4, 0x18

    .line 246
    .local v1, "atomSize":I
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v4, v7, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 249
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x8

    invoke-static {v4, v5, v9}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 252
    sget-object v4, Lorg/apache/poi/hslf/record/CurrentUserAtom;->headerToken:[B

    iget-object v5, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xc

    invoke-static {v4, v8, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x10

    iget-wide v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    long-to-int v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 259
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [B

    .line 260
    .local v0, "asciiUN":[B
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-static {v4, v0, v8}, Lorg/apache/poi/util/StringUtil;->putCompressedUnicode(Ljava/lang/String;[BI)V

    .line 263
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v0

    int-to-short v5, v5

    invoke-static {v4, v9, v5}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 266
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x16

    iget v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 267
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x18

    iget-byte v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    aput-byte v6, v4, v5

    .line 268
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x19

    iget-byte v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    aput-byte v6, v4, v5

    .line 271
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x1a

    aput-byte v8, v4, v5

    .line 272
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x1b

    aput-byte v8, v4, v5

    .line 275
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x1c

    array-length v6, v0

    invoke-static {v0, v8, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 278
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v0

    add-int/lit8 v5, v5, 0x1c

    iget-wide v6, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    long-to-int v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 281
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    new-array v3, v4, [B

    .line 282
    .local v3, "ucUN":[B
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-static {v4, v3, v8}, Lorg/apache/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 283
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v0

    add-int/lit8 v5, v5, 0x1c

    add-int/lit8 v5, v5, 0x4

    array-length v6, v3

    invoke-static {v3, v8, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 286
    iget-object v4, p0, Lorg/apache/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write([B)V

    .line 287
    return-void
.end method

.method public writeToFS(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 3
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 295
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/record/CurrentUserAtom;->writeOut(Ljava/io/OutputStream;)V

    .line 297
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 300
    .local v0, "bais":Ljava/io/ByteArrayInputStream;
    const-string/jumbo v2, "Current User"

    invoke-virtual {p1, v0, v2}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 301
    return-void
.end method
