.class public final Lorg/apache/poi/hslf/record/TextBytesAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "TextBytesAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private _text:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0xfa8

    sput-wide v0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 78
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 79
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    .line 80
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    invoke-static {v0, v4, v4}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 81
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/poi/hslf/record/TextBytesAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 82
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 84
    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 85
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 62
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 64
    if-ge p3, v1, :cond_0

    const/16 p3, 0x8

    .line 67
    :cond_0
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    .line 68
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 72
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 90
    sget-wide v0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setText([B)V
    .locals 3
    .param p1, "b"    # [B

    .prologue
    .line 51
    iput-object p1, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 54
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 110
    .local v0, "out":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "TextBytesAtom:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    iget-object v1, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 112
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 101
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextBytesAtom;->_text:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 102
    return-void
.end method
