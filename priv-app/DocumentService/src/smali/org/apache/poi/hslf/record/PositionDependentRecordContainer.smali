.class public abstract Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "PositionDependentRecordContainer.java"

# interfaces
.implements Lorg/apache/poi/hslf/record/PositionDependentRecord;


# instance fields
.field protected myLastOnDiskOffset:I

.field private sheetId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    return-void
.end method


# virtual methods
.method public getLastOnDiskOffset()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;->myLastOnDiskOffset:I

    return v0
.end method

.method public getSheetId()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;->sheetId:I

    return v0
.end method

.method public setLastOnDiskOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 56
    iput p1, p0, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;->myLastOnDiskOffset:I

    .line 57
    return-void
.end method

.method public setSheetId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 42
    iput p1, p0, Lorg/apache/poi/hslf/record/PositionDependentRecordContainer;->sheetId:I

    return-void
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "oldToNewReferencesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    return-void
.end method
