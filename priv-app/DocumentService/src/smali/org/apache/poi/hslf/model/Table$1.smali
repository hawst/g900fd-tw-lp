.class Lorg/apache/poi/hslf/model/Table$1;
.super Ljava/lang/Object;
.source "Table.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/hslf/model/Table;->initTable()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/poi/hslf/model/Table;


# direct methods
.method constructor <init>(Lorg/apache/poi/hslf/model/Table;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/poi/hslf/model/Table$1;->this$0:Lorg/apache/poi/hslf/model/Table;

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5
    .param p1, "o1"    # Ljava/lang/Object;
    .param p2, "o2"    # Ljava/lang/Object;

    .prologue
    .line 154
    check-cast p1, Lorg/apache/poi/hslf/model/Shape;

    .end local p1    # "o1":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Shape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 155
    .local v0, "anchor1":Lorg/apache/poi/java/awt/Rectangle;
    check-cast p2, Lorg/apache/poi/hslf/model/Shape;

    .end local p2    # "o2":Ljava/lang/Object;
    invoke-virtual {p2}, Lorg/apache/poi/hslf/model/Shape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v1

    .line 156
    .local v1, "anchor2":Lorg/apache/poi/java/awt/Rectangle;
    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v4, v1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    sub-int v2, v3, v4

    .line 157
    .local v2, "delta":I
    if-nez v2, :cond_0

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v4, v1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    sub-int v2, v3, v4

    .line 158
    :cond_0
    return v2
.end method
