.class public Lorg/apache/poi/hslf/record/ExEmbedAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "ExEmbedAtom.java"


# static fields
.field public static final DOES_NOT_FOLLOW_COLOR_SCHEME:I = 0x0

.field public static final FOLLOWS_ENTIRE_COLOR_SCHEME:I = 0x1

.field public static final FOLLOWS_TEXT_AND_BACKGROUND_SCHEME:I = 0x2


# instance fields
.field private _data:[B

.field private _header:[B


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 71
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 72
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_header:[B

    .line 73
    const/4 v0, 0x7

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    .line 75
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExEmbedAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 76
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 79
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 88
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 90
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_header:[B

    .line 91
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    .line 95
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 98
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    array-length v0, v0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The length of the data for a ExEmbedAtom must be at least 4 bytes, but was only "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method public getCantLockServerB()Z
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFollowColorScheme()I
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getIsTable()Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNoSizeToServerB()Z
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 146
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExEmbedAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 158
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbedAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 159
    return-void
.end method
