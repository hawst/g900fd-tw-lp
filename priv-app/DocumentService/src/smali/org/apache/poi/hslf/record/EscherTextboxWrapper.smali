.class public final Lorg/apache/poi/hslf/record/EscherTextboxWrapper;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "EscherTextboxWrapper.java"


# instance fields
.field private _escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

.field private _type:J

.field private shapeId:I

.field private styleTextProp9Atom:Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

.field private styleTextPropAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 66
    new-instance v0, Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherTextboxRecord;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 67
    iget-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    const/16 v1, -0xff3

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherTextboxRecord;->setRecordId(S)V

    .line 68
    iget-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherTextboxRecord;->setOptions(S)V

    .line 70
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ddf/EscherTextboxRecord;)V
    .locals 6
    .param p1, "textbox"    # Lorg/apache/poi/ddf/EscherTextboxRecord;

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 52
    iget-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherTextboxRecord;->getRecordId()S

    move-result v3

    int-to-long v4, v3

    iput-wide v4, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_type:J

    .line 55
    iget-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherTextboxRecord;->getData()[B

    move-result-object v0

    .line 56
    .local v0, "data":[B
    array-length v3, v0

    invoke-static {v0, v2, v3}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 57
    iget-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v4, v3

    :goto_0
    if-lt v2, v4, :cond_0

    .line 60
    return-void

    .line 57
    :cond_0
    aget-object v1, v3, v2

    .line 58
    .local v1, "r":Lorg/apache/poi/hslf/record/Record;
    instance-of v5, v1, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    if-eqz v5, :cond_1

    check-cast v1, Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .end local v1    # "r":Lorg/apache/poi/hslf/record/Record;
    iput-object v1, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->styleTextPropAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    .line 57
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getEscherRecord()Lorg/apache/poi/ddf/EscherTextboxRecord;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_type:J

    return-wide v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->shapeId:I

    return v0
.end method

.method public getStyleTextProp9Atom()Lorg/apache/poi/hslf/record/StyleTextProp9Atom;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->styleTextProp9Atom:Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    return-object v0
.end method

.method public getStyleTextPropAtom()Lorg/apache/poi/hslf/record/StyleTextPropAtom;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->styleTextPropAtom:Lorg/apache/poi/hslf/record/StyleTextPropAtom;

    return-object v0
.end method

.method public setShapeId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 110
    iput p1, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->shapeId:I

    .line 111
    return-void
.end method

.method public setStyleTextProp9Atom(Lorg/apache/poi/hslf/record/StyleTextProp9Atom;)V
    .locals 0
    .param p1, "nineAtom"    # Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    .prologue
    .line 118
    iput-object p1, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->styleTextProp9Atom:Lorg/apache/poi/hslf/record/StyleTextProp9Atom;

    .line 119
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 90
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 93
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 96
    .local v1, "data":[B
    iget-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v3, v1}, Lorg/apache/poi/ddf/EscherTextboxRecord;->setData([B)V

    .line 97
    return-void

    .line 91
    .end local v1    # "data":[B
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
