.class public final Lorg/apache/poi/hslf/model/Table;
.super Lorg/apache/poi/hslf/model/ShapeGroup;
.source "Table.java"


# static fields
.field protected static final BORDERS_ALL:I = 0x5

.field protected static final BORDERS_INSIDE:I = 0x7

.field protected static final BORDERS_NONE:I = 0x8

.field protected static final BORDERS_OUTSIDE:I = 0x6

.field protected static final BORDER_BOTTOM:I = 0x3

.field protected static final BORDER_LEFT:I = 0x4

.field protected static final BORDER_RIGHT:I = 0x2

.field protected static final BORDER_TOP:I = 0x1


# instance fields
.field protected cells:[[Lorg/apache/poi/hslf/model/TableCell;


# direct methods
.method public constructor <init>(II)V
    .locals 15
    .param p1, "numrows"    # I
    .param p2, "numcols"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hslf/model/ShapeGroup;-><init>()V

    .line 55
    const/4 v12, 0x1

    move/from16 v0, p1

    if-ge v0, v12, :cond_0

    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v13, "The number of rows must be greater than 1"

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 56
    :cond_0
    const/4 v12, 0x1

    move/from16 v0, p2

    if-ge v0, v12, :cond_1

    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v13, "The number of columns must be greater than 1"

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 58
    :cond_1
    const/4 v10, 0x0

    .local v10, "x":I
    const/4 v11, 0x0

    .local v11, "y":I
    const/4 v9, 0x0

    .local v9, "tblWidth":I
    const/4 v8, 0x0

    .line 59
    .local v8, "tblHeight":I
    filled-new-array/range {p1 .. p2}, [I

    move-result-object v12

    const-class v13, Lorg/apache/poi/hslf/model/TableCell;

    invoke-static {v13, v12}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [[Lorg/apache/poi/hslf/model/TableCell;

    iput-object v12, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    .line 60
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v12, v12

    if-lt v2, v12, :cond_2

    .line 70
    move v9, v10

    .line 71
    move v8, v11

    .line 72
    new-instance v12, Lorg/apache/poi/java/awt/Rectangle;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v12, v13, v14, v9, v8}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    invoke-virtual {p0, v12}, Lorg/apache/poi/hslf/model/Table;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 74
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 75
    .local v7, "spCont":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v5, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-direct {v5}, Lorg/apache/poi/ddf/EscherOptRecord;-><init>()V

    .line 76
    .local v5, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v12, -0xede

    invoke-virtual {v5, v12}, Lorg/apache/poi/ddf/EscherOptRecord;->setRecordId(S)V

    .line 77
    new-instance v12, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v13, 0x39f

    const/4 v14, 0x1

    invoke-direct {v12, v13, v14}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v5, v12}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 78
    new-instance v6, Lorg/apache/poi/ddf/EscherArrayProperty;

    const/16 v12, 0x43a0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v6, v12, v13, v14}, Lorg/apache/poi/ddf/EscherArrayProperty;-><init>(SZ[B)V

    .line 79
    .local v6, "p":Lorg/apache/poi/ddf/EscherArrayProperty;
    const/4 v12, 0x4

    invoke-virtual {v6, v12}, Lorg/apache/poi/ddf/EscherArrayProperty;->setSizeOfElements(I)V

    .line 80
    move/from16 v0, p1

    invoke-virtual {v6, v0}, Lorg/apache/poi/ddf/EscherArrayProperty;->setNumberOfElementsInArray(I)V

    .line 81
    move/from16 v0, p1

    invoke-virtual {v6, v0}, Lorg/apache/poi/ddf/EscherArrayProperty;->setNumberOfElementsInMemory(I)V

    .line 82
    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 83
    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v4

    .line 84
    .local v4, "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-interface {v4, v12, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 85
    invoke-virtual {v7, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->setChildRecords(Ljava/util/List;)V

    .line 86
    return-void

    .line 61
    .end local v4    # "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v5    # "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    .end local v6    # "p":Lorg/apache/poi/ddf/EscherArrayProperty;
    .end local v7    # "spCont":Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_2
    const/4 v10, 0x0

    .line 62
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v12, v12, v2

    array-length v12, v12

    if-lt v3, v12, :cond_3

    .line 68
    add-int/lit8 v11, v11, 0x28

    .line 60
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    :cond_3
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v12, v12, v2

    new-instance v13, Lorg/apache/poi/hslf/model/TableCell;

    invoke-direct {v13, p0}, Lorg/apache/poi/hslf/model/TableCell;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    aput-object v13, v12, v3

    .line 64
    new-instance v1, Lorg/apache/poi/java/awt/Rectangle;

    const/16 v12, 0x64

    const/16 v13, 0x28

    invoke-direct {v1, v10, v11, v12, v13}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    .line 65
    .local v1, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    iget-object v12, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v12, v12, v2

    aget-object v12, v12, v3

    invoke-virtual {v12, v1}, Lorg/apache/poi/hslf/model/TableCell;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 66
    add-int/lit8 v10, v10, 0x64

    .line 62
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/ShapeGroup;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 96
    return-void
.end method

.method private cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;
    .locals 4
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 319
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->createBorder()Lorg/apache/poi/hslf/model/Line;

    move-result-object v0

    .line 320
    .local v0, "border":Lorg/apache/poi/hslf/model/Line;
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Line;->getLineWidth()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hslf/model/Line;->setLineWidth(D)V

    .line 321
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Line;->getLineStyle()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Line;->setLineStyle(I)V

    .line 322
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Line;->getLineDashing()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Line;->setLineDashing(I)V

    .line 323
    invoke-virtual {p1}, Lorg/apache/poi/hslf/model/Line;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Line;->setLineColor(Lorg/apache/poi/java/awt/Color;)V

    .line 324
    return-object v0
.end method


# virtual methods
.method protected afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 17
    .param p1, "sh"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 117
    invoke-super/range {p0 .. p1}, Lorg/apache/poi/hslf/model/ShapeGroup;->afterInsert(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 119
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hslf/model/Table;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 120
    .local v13, "spCont":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v13}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v9

    .line 121
    .local v9, "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x2

    invoke-interface {v9, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 122
    .local v10, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/4 v15, 0x1

    invoke-virtual {v10, v15}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperty(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/ddf/EscherArrayProperty;

    .line 123
    .local v11, "p":Lorg/apache/poi/ddf/EscherArrayProperty;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v15, v15

    if-lt v7, v15, :cond_0

    .line 148
    return-void

    .line 124
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v15, v15, v7

    const/16 v16, 0x0

    aget-object v6, v15, v16

    .line 125
    .local v6, "cell":Lorg/apache/poi/hslf/model/TableCell;
    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v15

    iget v15, v15, Lorg/apache/poi/java/awt/Rectangle;->height:I

    mul-int/lit16 v15, v15, 0x240

    div-int/lit8 v12, v15, 0x48

    .line 126
    .local v12, "rowHeight":I
    const/4 v15, 0x4

    new-array v14, v15, [B

    .line 127
    .local v14, "val":[B
    invoke-static {v14, v12}, Lorg/apache/poi/util/LittleEndian;->putInt([BI)V

    .line 128
    invoke-virtual {v11, v7, v14}, Lorg/apache/poi/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 129
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v15, v15, v7

    array-length v15, v15

    if-lt v8, v15, :cond_1

    .line 123
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 130
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v15, v15, v7

    aget-object v5, v15, v8

    .line 131
    .local v5, "c":Lorg/apache/poi/hslf/model/TableCell;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/Table;->addShape(Lorg/apache/poi/hslf/model/Shape;)V

    .line 133
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/TableCell;->getBorderTop()Lorg/apache/poi/hslf/model/Line;

    move-result-object v4

    .line 134
    .local v4, "bt":Lorg/apache/poi/hslf/model/Line;
    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/Table;->addShape(Lorg/apache/poi/hslf/model/Shape;)V

    .line 136
    :cond_2
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/TableCell;->getBorderRight()Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    .line 137
    .local v3, "br":Lorg/apache/poi/hslf/model/Line;
    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/Table;->addShape(Lorg/apache/poi/hslf/model/Shape;)V

    .line 139
    :cond_3
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/TableCell;->getBorderBottom()Lorg/apache/poi/hslf/model/Line;

    move-result-object v1

    .line 140
    .local v1, "bb":Lorg/apache/poi/hslf/model/Line;
    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/Table;->addShape(Lorg/apache/poi/hslf/model/Shape;)V

    .line 142
    :cond_4
    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/TableCell;->getBorderLeft()Lorg/apache/poi/hslf/model/Line;

    move-result-object v2

    .line 143
    .local v2, "bl":Lorg/apache/poi/hslf/model/Line;
    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/Table;->addShape(Lorg/apache/poi/hslf/model/Shape;)V

    .line 129
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public createBorder()Lorg/apache/poi/hslf/model/Line;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 333
    new-instance v0, Lorg/apache/poi/hslf/model/Line;

    invoke-direct {v0, p0}, Lorg/apache/poi/hslf/model/Line;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 335
    .local v0, "line":Lorg/apache/poi/hslf/model/Line;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/Line;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xff5

    invoke-static {v2, v3}, Lorg/apache/poi/hslf/model/Table;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 336
    .local v1, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x144

    invoke-static {v1, v2, v4}, Lorg/apache/poi/hslf/model/Table;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 337
    const/16 v2, 0x17f

    invoke-static {v1, v2, v4}, Lorg/apache/poi/hslf/model/Table;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 338
    const/16 v2, 0x23f

    const/high16 v3, 0x20000

    invoke-static {v1, v2, v3}, Lorg/apache/poi/hslf/model/Table;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 339
    const/16 v2, 0x2bf

    const/high16 v3, 0x80000

    invoke-static {v1, v2, v3}, Lorg/apache/poi/hslf/model/Table;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 341
    return-object v0
.end method

.method public getCell(II)Lorg/apache/poi/hslf/model/TableCell;
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    return-object v0
.end method

.method public getNumberOfColumns()I
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    array-length v0, v0

    return v0
.end method

.method public getNumberOfRows()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v0, v0

    return v0
.end method

.method protected initTable()V
    .locals 13

    .prologue
    .line 151
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->getShapes()[Lorg/apache/poi/hslf/model/Shape;

    move-result-object v6

    .line 152
    .local v6, "sh":[Lorg/apache/poi/hslf/model/Shape;
    new-instance v9, Lorg/apache/poi/hslf/model/Table$1;

    invoke-direct {v9, p0}, Lorg/apache/poi/hslf/model/Table$1;-><init>(Lorg/apache/poi/hslf/model/Table;)V

    invoke-static {v6, v9}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 161
    const/4 v8, -0x1

    .line 162
    .local v8, "y0":I
    const/4 v4, 0x0

    .line 163
    .local v4, "maxrowlen":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v3, "lst":Ljava/util/ArrayList;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v5, "row":Ljava/util/ArrayList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v9, v6

    if-lt v1, v9, :cond_0

    .line 178
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    filled-new-array {v9, v4}, [I

    move-result-object v9

    const-class v10, Lorg/apache/poi/hslf/model/TableCell;

    invoke-static {v10, v9}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [[Lorg/apache/poi/hslf/model/TableCell;

    iput-object v9, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    .line 179
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v1, v9, :cond_3

    .line 187
    return-void

    .line 167
    :cond_0
    aget-object v9, v6, v1

    instance-of v9, v9, Lorg/apache/poi/hslf/model/TextShape;

    if-eqz v9, :cond_2

    .line 168
    aget-object v9, v6, v1

    invoke-virtual {v9}, Lorg/apache/poi/hslf/model/Shape;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 169
    .local v0, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    iget v9, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    if-eq v9, v8, :cond_1

    .line 170
    iget v8, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 171
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "row":Ljava/util/ArrayList;
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .restart local v5    # "row":Ljava/util/ArrayList;
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_1
    aget-object v9, v6, v1

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 166
    .end local v0    # "anchor":Lorg/apache/poi/java/awt/Rectangle;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    :cond_3
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "row":Ljava/util/ArrayList;
    check-cast v5, Ljava/util/ArrayList;

    .line 181
    .restart local v5    # "row":Ljava/util/ArrayList;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v2, v9, :cond_4

    .line 179
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 182
    :cond_4
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hslf/model/TextShape;

    .line 183
    .local v7, "tx":Lorg/apache/poi/hslf/model/TextShape;
    iget-object v9, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v9, v9, v1

    new-instance v10, Lorg/apache/poi/hslf/model/TableCell;

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/TextShape;->getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v11

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->getParent()Lorg/apache/poi/hslf/model/Shape;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/poi/hslf/model/TableCell;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    aput-object v10, v9, v2

    .line 184
    iget-object v9, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v9, v9, v1

    aget-object v9, v9, v2

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/TextShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/apache/poi/hslf/model/TableCell;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public setAllBorders(Lorg/apache/poi/hslf/model/Line;)V
    .locals 4
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    .line 255
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 264
    return-void

    .line 256
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 255
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 257
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    aget-object v0, v3, v2

    .line 258
    .local v0, "cell":Lorg/apache/poi/hslf/model/TableCell;
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderTop(Lorg/apache/poi/hslf/model/Line;)V

    .line 259
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V

    .line 260
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_2

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderRight(Lorg/apache/poi/hslf/model/Line;)V

    .line 261
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_3

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderBottom(Lorg/apache/poi/hslf/model/Line;)V

    .line 256
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public setColumnWidth(II)V
    .locals 8
    .param p1, "col"    # I
    .param p2, "width"    # I

    .prologue
    .line 230
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v6

    iget v1, v6, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 231
    .local v1, "currentWidth":I
    sub-int v2, p2, v1

    .line 232
    .local v2, "dx":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v6, v6

    if-lt v3, v6, :cond_0

    .line 243
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v5

    .line 244
    .local v5, "tblanchor":Lorg/apache/poi/java/awt/Rectangle;
    iget v6, v5, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v6, v2

    iput v6, v5, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 245
    invoke-virtual {p0, v5}, Lorg/apache/poi/hslf/model/Table;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 246
    return-void

    .line 233
    .end local v5    # "tblanchor":Lorg/apache/poi/java/awt/Rectangle;
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 234
    .local v0, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    iput p2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 235
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    aget-object v6, v6, p1

    invoke-virtual {v6, v0}, Lorg/apache/poi/hslf/model/TableCell;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 237
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge p1, v6, :cond_1

    add-int/lit8 v4, p1, 0x1

    .local v4, "j":I
    :goto_1
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    array-length v6, v6

    if-lt v4, v6, :cond_2

    .line 232
    .end local v4    # "j":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 238
    .restart local v4    # "j":I
    :cond_2
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 239
    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    add-int/2addr v6, v2

    iput v6, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 240
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    aget-object v6, v6, v4

    invoke-virtual {v6, v0}, Lorg/apache/poi/hslf/model/TableCell;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 237
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public setInsideBorders(Lorg/apache/poi/hslf/model/Line;)V
    .locals 5
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    const/4 v4, 0x0

    .line 299
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 316
    return-void

    .line 300
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 299
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 301
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    aget-object v0, v3, v2

    .line 303
    .local v0, "cell":Lorg/apache/poi/hslf/model/TableCell;
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_2

    .line 304
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderRight(Lorg/apache/poi/hslf/model/Line;)V

    .line 309
    :goto_2
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_3

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderBottom(Lorg/apache/poi/hslf/model/Line;)V

    .line 300
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 306
    :cond_2
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V

    .line 307
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V

    goto :goto_2

    .line 311
    :cond_3
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderTop(Lorg/apache/poi/hslf/model/Line;)V

    .line 312
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderBottom(Lorg/apache/poi/hslf/model/Line;)V

    goto :goto_3
.end method

.method public setOutsideBorders(Lorg/apache/poi/hslf/model/Line;)V
    .locals 5
    .param p1, "line"    # Lorg/apache/poi/hslf/model/Line;

    .prologue
    const/4 v4, 0x0

    .line 272
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 291
    return-void

    .line 273
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 272
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 274
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    aget-object v0, v3, v2

    .line 276
    .local v0, "cell":Lorg/apache/poi/hslf/model/TableCell;
    if-nez v2, :cond_2

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V

    .line 277
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v3, v3, v1

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_3

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderRight(Lorg/apache/poi/hslf/model/Line;)V

    .line 283
    :goto_2
    if-nez v1, :cond_4

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderTop(Lorg/apache/poi/hslf/model/Line;)V

    .line 273
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 279
    :cond_3
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V

    .line 280
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderLeft(Lorg/apache/poi/hslf/model/Line;)V

    goto :goto_2

    .line 284
    :cond_4
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_5

    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/model/Table;->cloneBorder(Lorg/apache/poi/hslf/model/Line;)Lorg/apache/poi/hslf/model/Line;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/TableCell;->setBorderBottom(Lorg/apache/poi/hslf/model/Line;)V

    goto :goto_3

    .line 286
    :cond_5
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderTop(Lorg/apache/poi/hslf/model/Line;)V

    .line 287
    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/TableCell;->setBorderBottom(Lorg/apache/poi/hslf/model/Line;)V

    goto :goto_3
.end method

.method public setRowHeight(II)V
    .locals 8
    .param p1, "row"    # I
    .param p2, "height"    # I

    .prologue
    .line 206
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, p1

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v6

    iget v1, v6, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 207
    .local v1, "currentHeight":I
    sub-int v2, p2, v1

    .line 209
    .local v2, "dy":I
    move v3, p1

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    array-length v6, v6

    if-lt v3, v6, :cond_0

    .line 217
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v5

    .line 218
    .local v5, "tblanchor":Lorg/apache/poi/java/awt/Rectangle;
    iget v6, v5, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v6, v2

    iput v6, v5, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 219
    invoke-virtual {p0, v5}, Lorg/apache/poi/hslf/model/Table;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 221
    return-void

    .line 210
    .end local v5    # "tblanchor":Lorg/apache/poi/java/awt/Rectangle;
    :cond_0
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    array-length v6, v6

    if-lt v4, v6, :cond_1

    .line 209
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 211
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TableCell;->getAnchor()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 212
    .local v0, "anchor":Lorg/apache/poi/java/awt/Rectangle;
    if-ne v3, p1, :cond_2

    iput p2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 214
    :goto_2
    iget-object v6, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    aget-object v6, v6, v3

    aget-object v6, v6, v4

    invoke-virtual {v6, v0}, Lorg/apache/poi/hslf/model/TableCell;->setAnchor(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 210
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 213
    :cond_2
    iget v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    add-int/2addr v6, v2

    iput v6, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    goto :goto_2
.end method

.method public setSheet(Lorg/apache/poi/hslf/model/Sheet;)V
    .locals 1
    .param p1, "sheet"    # Lorg/apache/poi/hslf/model/Sheet;

    .prologue
    .line 195
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/ShapeGroup;->setSheet(Lorg/apache/poi/hslf/model/Sheet;)V

    .line 196
    iget-object v0, p0, Lorg/apache/poi/hslf/model/Table;->cells:[[Lorg/apache/poi/hslf/model/TableCell;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/Table;->initTable()V

    .line 197
    :cond_0
    return-void
.end method
