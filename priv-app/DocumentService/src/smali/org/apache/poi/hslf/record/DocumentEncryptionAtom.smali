.class public final Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "DocumentEncryptionAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private data:[B

.field private encryptionProviderName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x2f14

    sput-wide v0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 7
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v6, 0x0

    .line 41
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 43
    new-array v3, v4, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->_header:[B

    .line 44
    iget-object v3, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->_header:[B

    invoke-static {p1, p2, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    add-int/lit8 v3, p3, -0x8

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->data:[B

    .line 48
    add-int/lit8 v3, p2, 0x8

    iget-object v4, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->data:[B

    add-int/lit8 v5, p3, -0x8

    invoke-static {p1, v3, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    const/4 v0, -0x1

    .line 53
    .local v0, "endPos":I
    add-int/lit8 v3, p2, 0x8

    add-int/lit8 v1, v3, 0x2c

    .line 54
    .local v1, "pos":I
    :goto_0
    add-int v3, p2, p3

    if-ge v1, v3, :cond_0

    if-ltz v0, :cond_1

    .line 61
    :cond_0
    add-int/lit8 v3, p2, 0x8

    add-int/lit8 v1, v3, 0x2c

    .line 62
    sub-int v3, v0, v1

    div-int/lit8 v2, v3, 0x2

    .line 63
    .local v2, "stringLen":I
    invoke-static {p1, v1, v2}, Lorg/apache/poi/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->encryptionProviderName:Ljava/lang/String;

    .line 64
    return-void

    .line 55
    .end local v2    # "stringLen":I
    :cond_1
    aget-byte v3, p1, v1

    if-nez v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    aget-byte v3, p1, v3

    if-nez v3, :cond_2

    .line 57
    move v0, v1

    .line 59
    :cond_2
    add-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method


# virtual methods
.method public getEncryptionProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->encryptionProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyLength()I
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->data:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 84
    sget-wide v0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->_type:J

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 95
    iget-object v0, p0, Lorg/apache/poi/hslf/record/DocumentEncryptionAtom;->data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 96
    return-void
.end method
