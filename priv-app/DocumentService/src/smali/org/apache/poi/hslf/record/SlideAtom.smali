.class public final Lorg/apache/poi/hslf/record/SlideAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "SlideAtom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;
    }
.end annotation


# static fields
.field public static final MASTER_SLIDE_ID:I = 0x0

.field public static final USES_MASTER_SLIDE_ID:I = -0x80000000

.field private static _type:J


# instance fields
.field private _header:[B

.field private followMasterBackground:Z

.field private followMasterObjects:Z

.field private followMasterScheme:Z

.field private layoutAtom:Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

.field private masterID:I

.field private notesID:I

.field private reserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/16 v0, 0x3ef

    sput-wide v0, Lorg/apache/poi/hslf/record/SlideAtom;->_type:J

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 119
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 120
    const/16 v1, 0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    .line 121
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    invoke-static {v1, v6, v5}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 122
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    sget-wide v2, Lorg/apache/poi/hslf/record/SlideAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v1, v5, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 123
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    const/4 v2, 0x4

    const/16 v3, 0x18

    invoke-static {v1, v2, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 125
    const/16 v1, 0xc

    new-array v0, v1, [B

    .line 126
    .local v0, "ssdate":[B
    new-instance v1, Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    invoke-direct {v1, p0, v0}, Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;-><init>(Lorg/apache/poi/hslf/record/SlideAtom;[B)V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 127
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->setGeometryType(I)V

    .line 129
    iput-boolean v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    .line 130
    iput-boolean v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    .line 131
    iput-boolean v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    .line 132
    const/high16 v1, -0x80000000

    iput v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->masterID:I

    .line 133
    iput v6, p0, Lorg/apache/poi/hslf/record/SlideAtom;->notesID:I

    .line 134
    new-array v1, v5, [B

    iput-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->reserved:[B

    .line 135
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 7
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v6, 0xc

    const/16 v3, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 74
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 76
    const/16 v2, 0x1e

    if-ge p3, v2, :cond_0

    const/16 p3, 0x1e

    .line 79
    :cond_0
    new-array v2, v3, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    .line 80
    iget-object v2, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    invoke-static {p1, p2, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    new-array v0, v6, [B

    .line 84
    .local v0, "SSlideLayoutAtomData":[B
    add-int/lit8 v2, p2, 0x8

    invoke-static {p1, v2, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    new-instance v2, Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    invoke-direct {v2, p0, v0}, Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;-><init>(Lorg/apache/poi/hslf/record/SlideAtom;[B)V

    iput-object v2, p0, Lorg/apache/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 89
    add-int/lit8 v2, p2, 0xc

    add-int/lit8 v2, v2, 0x8

    invoke-static {p1, v2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/record/SlideAtom;->masterID:I

    .line 90
    add-int/lit8 v2, p2, 0x10

    add-int/lit8 v2, v2, 0x8

    invoke-static {p1, v2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hslf/record/SlideAtom;->notesID:I

    .line 93
    add-int/lit8 v2, p2, 0x14

    add-int/lit8 v2, v2, 0x8

    invoke-static {p1, v2}, Lorg/apache/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    .line 94
    .local v1, "flags":I
    and-int/lit8 v2, v1, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 95
    iput-boolean v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    .line 99
    :goto_0
    and-int/lit8 v2, v1, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 100
    iput-boolean v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    .line 104
    :goto_1
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_3

    .line 105
    iput-boolean v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    .line 112
    :goto_2
    add-int/lit8 v2, p3, -0x1e

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/poi/hslf/record/SlideAtom;->reserved:[B

    .line 113
    add-int/lit8 v2, p2, 0x1e

    iget-object v3, p0, Lorg/apache/poi/hslf/record/SlideAtom;->reserved:[B

    iget-object v4, p0, Lorg/apache/poi/hslf/record/SlideAtom;->reserved:[B

    array-length v4, v4

    invoke-static {p1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 114
    return-void

    .line 97
    :cond_1
    iput-boolean v5, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    goto :goto_0

    .line 102
    :cond_2
    iput-boolean v5, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    goto :goto_1

    .line 107
    :cond_3
    iput-boolean v5, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    goto :goto_2
.end method


# virtual methods
.method public getFollowMasterBackground()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    return v0
.end method

.method public getFollowMasterObjects()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    return v0
.end method

.method public getFollowMasterScheme()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    return v0
.end method

.method public getMasterID()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/poi/hslf/record/SlideAtom;->masterID:I

    return v0
.end method

.method public getNotesID()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lorg/apache/poi/hslf/record/SlideAtom;->notesID:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 140
    sget-wide v0, Lorg/apache/poi/hslf/record/SlideAtom;->_type:J

    return-wide v0
.end method

.method public getSSlideLayoutAtom()Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    return-object v0
.end method

.method public setFollowMasterBackground(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    return-void
.end method

.method public setFollowMasterObjects(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    return-void
.end method

.method public setFollowMasterScheme(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    return-void
.end method

.method public setMasterID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 52
    iput p1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->masterID:I

    return-void
.end method

.method public setNotesID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 59
    iput p1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->notesID:I

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 151
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->writeOut(Ljava/io/OutputStream;)V

    .line 154
    iget v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->masterID:I

    invoke-static {v1, p1}, Lorg/apache/poi/hslf/record/SlideAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 155
    iget v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->notesID:I

    invoke-static {v1, p1}, Lorg/apache/poi/hslf/record/SlideAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "flags":S
    iget-boolean v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    int-to-short v0, v1

    .line 160
    :cond_0
    iget-boolean v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, 0x2

    int-to-short v0, v1

    .line 161
    :cond_1
    iget-boolean v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, 0x4

    int-to-short v0, v1

    .line 162
    :cond_2
    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/SlideAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 165
    iget-object v1, p0, Lorg/apache/poi/hslf/record/SlideAtom;->reserved:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 166
    return-void
.end method
