.class public abstract Lorg/apache/poi/hslf/record/RecordContainer;
.super Lorg/apache/poi/hslf/record/Record;
.source "RecordContainer.java"


# instance fields
.field protected _children:[Lorg/apache/poi/hslf/record/Record;

.field private changingChildRecordsLock:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/Record;-><init>()V

    .line 39
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    .line 36
    return-void
.end method

.method private addChildAt(Lorg/apache/poi/hslf/record/Record;I)V
    .locals 3
    .param p1, "newChild"    # Lorg/apache/poi/hslf/record/Record;
    .param p2, "position"    # I

    .prologue
    .line 95
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v1

    .line 97
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/record/RecordContainer;->appendChild(Lorg/apache/poi/hslf/record/Record;)V

    .line 100
    iget-object v0, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    const/4 v2, 0x1

    invoke-direct {p0, v0, p2, v2}, Lorg/apache/poi/hslf/record/RecordContainer;->moveChildRecords(III)V

    .line 95
    monitor-exit v1

    .line 102
    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private appendChild(Lorg/apache/poi/hslf/record/Record;)V
    .locals 6
    .param p1, "newChild"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 78
    iget-object v2, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v2

    .line 80
    :try_start_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Lorg/apache/poi/hslf/record/Record;

    .line 81
    .local v0, "nc":[Lorg/apache/poi/hslf/record/Record;
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v5, v5

    invoke-static {v1, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    aput-object p1, v0, v1

    .line 84
    iput-object v0, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 78
    monitor-exit v2

    .line 86
    return-void

    .line 78
    .end local v0    # "nc":[Lorg/apache/poi/hslf/record/Record;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private findChildLocation(Lorg/apache/poi/hslf/record/Record;)I
    .locals 3
    .param p1, "child"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 63
    iget-object v2, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v2

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 63
    monitor-exit v2

    .line 70
    const/4 v0, -0x1

    .end local v0    # "i":I
    :goto_1
    return v0

    .line 65
    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    monitor-exit v2

    goto :goto_1

    .line 63
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 64
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static handleParentAwareRecords(Lorg/apache/poi/hslf/record/RecordContainer;)V
    .locals 5
    .param p0, "br"    # Lorg/apache/poi/hslf/record/RecordContainer;

    .prologue
    .line 350
    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 360
    return-void

    .line 350
    :cond_0
    aget-object v0, v3, v2

    .line 352
    .local v0, "record":Lorg/apache/poi/hslf/record/Record;
    instance-of v1, v0, Lorg/apache/poi/hslf/record/ParentAwareRecord;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 353
    check-cast v1, Lorg/apache/poi/hslf/record/ParentAwareRecord;

    invoke-interface {v1, p0}, Lorg/apache/poi/hslf/record/ParentAwareRecord;->setParentRecord(Lorg/apache/poi/hslf/record/RecordContainer;)V

    .line 356
    :cond_1
    instance-of v1, v0, Lorg/apache/poi/hslf/record/RecordContainer;

    if-eqz v1, :cond_2

    .line 357
    check-cast v0, Lorg/apache/poi/hslf/record/RecordContainer;

    .end local v0    # "record":Lorg/apache/poi/hslf/record/Record;
    invoke-static {v0}, Lorg/apache/poi/hslf/record/RecordContainer;->handleParentAwareRecords(Lorg/apache/poi/hslf/record/RecordContainer;)V

    .line 350
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method private moveChildRecords(III)V
    .locals 2
    .param p1, "oldLoc"    # I
    .param p2, "newLoc"    # I
    .param p3, "number"    # I

    .prologue
    .line 112
    if-ne p1, p2, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    if-eqz p3, :cond_0

    .line 116
    add-int v0, p1, p3

    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Asked to move more records than there are!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    invoke-static {v0, p1, p2, p3}, Lorg/apache/poi/util/ArrayUtil;->arrayMoveWithin([Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V
    .locals 4
    .param p1, "newChild"    # Lorg/apache/poi/hslf/record/Record;
    .param p2, "after"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 176
    iget-object v2, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v2

    .line 178
    :try_start_0
    invoke-direct {p0, p2}, Lorg/apache/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/poi/hslf/record/Record;)I

    move-result v0

    .line 179
    .local v0, "loc":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 180
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Asked to add a new child after another record, but that record wasn\'t one of our children!"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 176
    .end local v0    # "loc":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 184
    .restart local v0    # "loc":I
    :cond_0
    add-int/lit8 v1, v0, 0x1

    :try_start_1
    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hslf/record/RecordContainer;->addChildAt(Lorg/apache/poi/hslf/record/Record;I)V

    .line 176
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    return-void
.end method

.method public addChildBefore(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V
    .locals 4
    .param p1, "newChild"    # Lorg/apache/poi/hslf/record/Record;
    .param p2, "before"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 194
    iget-object v2, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v2

    .line 196
    :try_start_0
    invoke-direct {p0, p2}, Lorg/apache/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/poi/hslf/record/Record;)I

    move-result v0

    .line 197
    .local v0, "loc":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 198
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Asked to add a new child before another record, but that record wasn\'t one of our children!"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 194
    .end local v0    # "loc":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 202
    .restart local v0    # "loc":I
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hslf/record/RecordContainer;->addChildAt(Lorg/apache/poi/hslf/record/Record;I)V

    .line 194
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    return-void
.end method

.method public appendChildRecord(Lorg/apache/poi/hslf/record/Record;)V
    .locals 2
    .param p1, "newChild"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 165
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v1

    .line 166
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/record/RecordContainer;->appendChild(Lorg/apache/poi/hslf/record/Record;)V

    .line 165
    monitor-exit v1

    .line 168
    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;
    .locals 5
    .param p1, "type"    # J

    .prologue
    .line 131
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 136
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 132
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_1

    .line 133
    iget-object v1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v1, v1, v0

    goto :goto_1

    .line 131
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getChildRecords()[Lorg/apache/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    return-object v0
.end method

.method public isAnAtom()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public moveChildBefore(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V
    .locals 1
    .param p1, "child"    # Lorg/apache/poi/hslf/record/Record;
    .param p2, "before"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 210
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/poi/hslf/record/RecordContainer;->moveChildrenBefore(Lorg/apache/poi/hslf/record/Record;ILorg/apache/poi/hslf/record/Record;)V

    .line 211
    return-void
.end method

.method public moveChildrenAfter(Lorg/apache/poi/hslf/record/Record;ILorg/apache/poi/hslf/record/Record;)V
    .locals 5
    .param p1, "firstChild"    # Lorg/apache/poi/hslf/record/Record;
    .param p2, "number"    # I
    .param p3, "after"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    const/4 v4, -0x1

    .line 241
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 261
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v3

    .line 245
    :try_start_0
    invoke-direct {p0, p3}, Lorg/apache/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/poi/hslf/record/Record;)I

    move-result v0

    .line 246
    .local v0, "newLoc":I
    if-ne v0, v4, :cond_1

    .line 247
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move children before another record, but that record wasn\'t one of our children!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 243
    .end local v0    # "newLoc":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 250
    .restart local v0    # "newLoc":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 253
    :try_start_1
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/poi/hslf/record/Record;)I

    move-result v1

    .line 254
    .local v1, "oldLoc":I
    if-ne v1, v4, :cond_2

    .line 255
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move a record that wasn\'t a child!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 259
    :cond_2
    invoke-direct {p0, v1, v0, p2}, Lorg/apache/poi/hslf/record/RecordContainer;->moveChildRecords(III)V

    .line 243
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public moveChildrenBefore(Lorg/apache/poi/hslf/record/Record;ILorg/apache/poi/hslf/record/Record;)V
    .locals 5
    .param p1, "firstChild"    # Lorg/apache/poi/hslf/record/Record;
    .param p2, "number"    # I
    .param p3, "before"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    const/4 v4, -0x1

    .line 217
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 235
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Boolean;

    monitor-enter v3

    .line 221
    :try_start_0
    invoke-direct {p0, p3}, Lorg/apache/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/poi/hslf/record/Record;)I

    move-result v0

    .line 222
    .local v0, "newLoc":I
    if-ne v0, v4, :cond_1

    .line 223
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move children before another record, but that record wasn\'t one of our children!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 219
    .end local v0    # "newLoc":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 227
    .restart local v0    # "newLoc":I
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lorg/apache/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/poi/hslf/record/Record;)I

    move-result v1

    .line 228
    .local v1, "oldLoc":I
    if-ne v1, v4, :cond_2

    .line 229
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move a record that wasn\'t a child!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 233
    :cond_2
    invoke-direct {p0, v1, v0, p2}, Lorg/apache/poi/hslf/record/RecordContainer;->moveChildRecords(III)V

    .line 219
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public removeChild(Lorg/apache/poi/hslf/record/Record;)Lorg/apache/poi/hslf/record/Record;
    .locals 6
    .param p1, "ch"    # Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 146
    const/4 v2, 0x0

    .line 147
    .local v2, "rm":Lorg/apache/poi/hslf/record/Record;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v0, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hslf/record/Record;>;"
    iget-object v4, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 152
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/hslf/record/Record;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/poi/hslf/record/Record;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 153
    return-object v2

    .line 148
    :cond_0
    aget-object v1, v4, v3

    .line 149
    .local v1, "r":Lorg/apache/poi/hslf/record/Record;
    if-eq v1, p1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 150
    :cond_1
    move-object v2, v1

    goto :goto_1
.end method

.method public setChildRecord([Lorg/apache/poi/hslf/record/Record;)V
    .locals 0
    .param p1, "records"    # [Lorg/apache/poi/hslf/record/Record;

    .prologue
    .line 269
    iput-object p1, p0, Lorg/apache/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 270
    return-void
.end method

.method public writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V
    .locals 11
    .param p1, "headerA"    # B
    .param p2, "headerB"    # B
    .param p3, "type"    # J
    .param p5, "children"    # [Lorg/apache/poi/hslf/record/Record;
    .param p6, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    move-object/from16 v0, p6

    instance-of v9, v0, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;

    if-eqz v9, :cond_1

    move-object/from16 v4, p6

    .line 289
    check-cast v4, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;

    .line 292
    .local v4, "mout":Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;->getBytesWritten()I

    move-result v5

    .line 295
    .local v5, "oldSize":I
    const/4 v9, 0x2

    new-array v9, v9, [B

    const/4 v10, 0x0

    aput-byte p1, v9, v10

    const/4 v10, 0x1

    aput-byte p2, v9, v10

    invoke-virtual {v4, v9}, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;->write([B)V

    .line 296
    const/4 v9, 0x2

    new-array v8, v9, [B

    .line 297
    .local v8, "typeB":[B
    long-to-int v9, p3

    int-to-short v9, v9

    invoke-static {v8, v9}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 298
    invoke-virtual {v4, v8}, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;->write([B)V

    .line 299
    const/4 v9, 0x4

    new-array v9, v9, [B

    invoke-virtual {v4, v9}, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;->write([B)V

    .line 302
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move-object/from16 v0, p5

    array-length v9, v0

    if-lt v2, v9, :cond_0

    .line 309
    invoke-virtual {v4}, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;->getBytesWritten()I

    move-result v9

    sub-int/2addr v9, v5

    add-int/lit8 v3, v9, -0x8

    .line 310
    .local v3, "length":I
    const/4 v9, 0x4

    new-array v6, v9, [B

    .line 311
    .local v6, "size":[B
    const/4 v9, 0x0

    invoke-static {v6, v9, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 312
    add-int/lit8 v9, v5, 0x4

    invoke-virtual {v4, v6, v9}, Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;->overwrite([BI)V

    .line 343
    .end local v3    # "length":I
    .end local v4    # "mout":Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;
    .end local v5    # "oldSize":I
    .end local v6    # "size":[B
    :goto_1
    return-void

    .line 303
    .restart local v4    # "mout":Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;
    .restart local v5    # "oldSize":I
    :cond_0
    aget-object v9, p5, v2

    invoke-virtual {v9, v4}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 302
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 318
    .end local v2    # "i":I
    .end local v4    # "mout":Lorg/apache/poi/hslf/util/MutableByteArrayOutputStream;
    .end local v5    # "oldSize":I
    .end local v8    # "typeB":[B
    :cond_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 321
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v9, 0x2

    new-array v9, v9, [B

    const/4 v10, 0x0

    aput-byte p1, v9, v10

    const/4 v10, 0x1

    aput-byte p2, v9, v10

    invoke-virtual {v1, v9}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 322
    const/4 v9, 0x2

    new-array v8, v9, [B

    .line 323
    .restart local v8    # "typeB":[B
    long-to-int v9, p3

    int-to-short v9, v9

    invoke-static {v8, v9}, Lorg/apache/poi/util/LittleEndian;->putShort([BS)V

    .line 324
    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 325
    const/4 v9, 0x4

    new-array v9, v9, [B

    invoke-virtual {v1, v9}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 328
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    move-object/from16 v0, p5

    array-length v9, v0

    if-lt v2, v9, :cond_2

    .line 333
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    .line 338
    .local v7, "toWrite":[B
    const/4 v9, 0x4

    array-length v10, v7

    add-int/lit8 v10, v10, -0x8

    invoke-static {v7, v9, v10}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 341
    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    .line 329
    .end local v7    # "toWrite":[B
    :cond_2
    aget-object v9, p5, v2

    invoke-virtual {v9, v1}, Lorg/apache/poi/hslf/record/Record;->writeOut(Ljava/io/OutputStream;)V

    .line 328
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method
