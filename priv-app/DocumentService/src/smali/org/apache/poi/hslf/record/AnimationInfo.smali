.class public final Lorg/apache/poi/hslf/record/AnimationInfo;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "AnimationInfo.java"


# instance fields
.field private _header:[B

.field private animationAtom:Lorg/apache/poi/hslf/record/AnimationInfoAtom;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 70
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    .line 71
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    const/16 v1, 0xf

    aput-byte v1, v0, v4

    .line 72
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfo;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 75
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    new-instance v1, Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->animationAtom:Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    aput-object v1, v0, v4

    .line 76
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 40
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 42
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    .line 43
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 47
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/AnimationInfo;->findInterestingChildren()V

    .line 48
    return-void
.end method

.method private findInterestingChildren()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 58
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v4

    instance-of v0, v0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v4

    check-cast v0, Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->animationAtom:Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    sget-object v0, Lorg/apache/poi/hslf/record/AnimationInfo;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "First child record wasn\'t a AnimationInfoAtom, was of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getAnimationInfoAtom()Lorg/apache/poi/hslf/record/AnimationInfoAtom;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->animationAtom:Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->AnimationInfo:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/AnimationInfo;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/AnimationInfo;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/AnimationInfo;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 89
    return-void
.end method
