.class public Lorg/apache/poi/hslf/record/ExEmbed;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "ExEmbed.java"


# instance fields
.field private _header:[B

.field private clipboardName:Lorg/apache/poi/hslf/record/CString;

.field protected embedAtom:Lorg/apache/poi/hslf/record/RecordAtom;

.field private menuName:Lorg/apache/poi/hslf/record/CString;

.field private oleObjAtom:Lorg/apache/poi/hslf/record/ExOleObjAtom;

.field private progId:Lorg/apache/poi/hslf/record/CString;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 65
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 66
    const/16 v3, 0x8

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    .line 67
    const/4 v3, 0x5

    new-array v3, v3, [Lorg/apache/poi/hslf/record/Record;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 70
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    const/16 v4, 0xf

    aput-byte v4, v3, v6

    .line 71
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExEmbed;->getRecordType()J

    move-result-wide v4

    long-to-int v4, v4

    int-to-short v4, v4

    invoke-static {v3, v7, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 74
    new-instance v0, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 75
    .local v0, "cs1":Lorg/apache/poi/hslf/record/CString;
    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 76
    new-instance v1, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 77
    .local v1, "cs2":Lorg/apache/poi/hslf/record/CString;
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 78
    new-instance v2, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    .line 79
    .local v2, "cs3":Lorg/apache/poi/hslf/record/CString;
    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Lorg/apache/poi/hslf/record/CString;->setOptions(I)V

    .line 80
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    new-instance v4, Lorg/apache/poi/hslf/record/ExEmbedAtom;

    invoke-direct {v4}, Lorg/apache/poi/hslf/record/ExEmbedAtom;-><init>()V

    aput-object v4, v3, v6

    .line 81
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v4, 0x1

    new-instance v5, Lorg/apache/poi/hslf/record/ExOleObjAtom;

    invoke-direct {v5}, Lorg/apache/poi/hslf/record/ExOleObjAtom;-><init>()V

    aput-object v5, v3, v4

    .line 82
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aput-object v0, v3, v7

    .line 83
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v4, 0x3

    aput-object v1, v3, v4

    .line 84
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v4, 0x4

    aput-object v2, v3, v4

    .line 85
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExEmbed;->findInterestingChildren()V

    .line 86
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 52
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 54
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    .line 55
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 59
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExEmbed;->findInterestingChildren()V

    .line 60
    return-void
.end method

.method private findInterestingChildren()V
    .locals 10

    .prologue
    const/4 v9, 0x7

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 95
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v6

    instance-of v3, v3, Lorg/apache/poi/hslf/record/ExEmbedAtom;

    if-eqz v3, :cond_0

    .line 96
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v6

    check-cast v3, Lorg/apache/poi/hslf/record/ExEmbedAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->embedAtom:Lorg/apache/poi/hslf/record/RecordAtom;

    .line 102
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v8

    instance-of v3, v3, Lorg/apache/poi/hslf/record/ExOleObjAtom;

    if-eqz v3, :cond_1

    .line 103
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v8

    check-cast v3, Lorg/apache/poi/hslf/record/ExOleObjAtom;

    iput-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->oleObjAtom:Lorg/apache/poi/hslf/record/ExOleObjAtom;

    .line 108
    :goto_1
    const/4 v1, 0x2

    .local v1, "i":I
    :goto_2
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    array-length v3, v3

    if-lt v1, v3, :cond_2

    .line 119
    return-void

    .line 98
    .end local v1    # "i":I
    :cond_0
    sget-object v3, Lorg/apache/poi/hslf/record/ExEmbed;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "First child record wasn\'t a ExEmbedAtom, was of type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 105
    :cond_1
    sget-object v3, Lorg/apache/poi/hslf/record/ExEmbed;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Second child record wasn\'t a ExOleObjAtom, was of type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 109
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v3, v3, v1

    instance-of v3, v3, Lorg/apache/poi/hslf/record/CString;

    if-eqz v3, :cond_3

    .line 110
    iget-object v3, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v3, v1

    check-cast v0, Lorg/apache/poi/hslf/record/CString;

    .line 111
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getOptions()I

    move-result v3

    shr-int/lit8 v2, v3, 0x4

    .line 112
    .local v2, "opts":I
    packed-switch v2, :pswitch_data_0

    .line 108
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    .end local v2    # "opts":I
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 113
    .restart local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    .restart local v2    # "opts":I
    :pswitch_0
    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->menuName:Lorg/apache/poi/hslf/record/CString;

    goto :goto_3

    .line 114
    :pswitch_1
    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->progId:Lorg/apache/poi/hslf/record/CString;

    goto :goto_3

    .line 115
    :pswitch_2
    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->clipboardName:Lorg/apache/poi/hslf/record/CString;

    goto :goto_3

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getClipboardName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->clipboardName:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->clipboardName:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getExEmbedAtom()Lorg/apache/poi/hslf/record/ExEmbedAtom;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->embedAtom:Lorg/apache/poi/hslf/record/RecordAtom;

    check-cast v0, Lorg/apache/poi/hslf/record/ExEmbedAtom;

    return-object v0
.end method

.method public getExOleObjAtom()Lorg/apache/poi/hslf/record/ExOleObjAtom;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->oleObjAtom:Lorg/apache/poi/hslf/record/ExOleObjAtom;

    return-object v0
.end method

.method public getMenuName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->menuName:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->menuName:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProgId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->progId:Lorg/apache/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->progId:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 191
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExEmbed:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setClipboardName(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->clipboardName:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->clipboardName:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 183
    :cond_0
    return-void
.end method

.method public setMenuName(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->menuName:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->menuName:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 154
    :cond_0
    return-void
.end method

.method public setProgId(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->progId:Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->progId:Lorg/apache/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 169
    :cond_0
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExEmbed;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/ExEmbed;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/ExEmbed;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 203
    return-void
.end method
