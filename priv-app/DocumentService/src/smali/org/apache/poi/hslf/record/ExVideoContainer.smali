.class public final Lorg/apache/poi/hslf/record/ExVideoContainer;
.super Lorg/apache/poi/hslf/record/RecordContainer;
.source "ExVideoContainer.java"


# instance fields
.field private _header:[B

.field private mediaAtom:Lorg/apache/poi/hslf/record/ExMediaAtom;

.field private pathAtom:Lorg/apache/poi/hslf/record/CString;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 75
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 77
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    .line 78
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    const/16 v1, 0xf

    aput-byte v1, v0, v4

    .line 79
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getRecordType()J

    move-result-wide v2

    long-to-int v1, v2

    int-to-short v1, v1

    invoke-static {v0, v5, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 81
    new-array v0, v5, [Lorg/apache/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 82
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    new-instance v1, Lorg/apache/poi/hslf/record/ExMediaAtom;

    invoke-direct {v1}, Lorg/apache/poi/hslf/record/ExMediaAtom;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->mediaAtom:Lorg/apache/poi/hslf/record/ExMediaAtom;

    aput-object v1, v0, v4

    .line 83
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/poi/hslf/record/CString;

    invoke-direct {v2}, Lorg/apache/poi/hslf/record/CString;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->pathAtom:Lorg/apache/poi/hslf/record/CString;

    aput-object v2, v0, v1

    .line 84
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 42
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordContainer;-><init>()V

    .line 44
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    .line 45
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    .line 49
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->findInterestingChildren()V

    .line 50
    return-void
.end method

.method private findInterestingChildren()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v3

    instance-of v0, v0, Lorg/apache/poi/hslf/record/ExMediaAtom;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v3

    check-cast v0, Lorg/apache/poi/hslf/record/ExMediaAtom;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->mediaAtom:Lorg/apache/poi/hslf/record/ExMediaAtom;

    .line 65
    :goto_0
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v4

    instance-of v0, v0, Lorg/apache/poi/hslf/record/CString;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v0, v0, v4

    check-cast v0, Lorg/apache/poi/hslf/record/CString;

    iput-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->pathAtom:Lorg/apache/poi/hslf/record/CString;

    .line 70
    :goto_1
    return-void

    .line 63
    :cond_0
    sget-object v0, Lorg/apache/poi/hslf/record/ExVideoContainer;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "First child record wasn\'t a ExMediaAtom, was of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 68
    :cond_1
    sget-object v0, Lorg/apache/poi/hslf/record/ExVideoContainer;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Second child record wasn\'t a CString, was of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public getExMediaAtom()Lorg/apache/poi/hslf/record/ExMediaAtom;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->mediaAtom:Lorg/apache/poi/hslf/record/ExMediaAtom;

    return-object v0
.end method

.method public getPathAtom()Lorg/apache/poi/hslf/record/CString;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->pathAtom:Lorg/apache/poi/hslf/record/CString;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lorg/apache/poi/hslf/record/RecordTypes;->ExVideoContainer:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iget-object v0, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_header:[B

    const/4 v1, 0x1

    aget-byte v3, v0, v1

    invoke-virtual {p0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getRecordType()J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/poi/hslf/record/ExVideoContainer;->_children:[Lorg/apache/poi/hslf/record/Record;

    move-object v1, p0

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/poi/hslf/record/ExVideoContainer;->writeOut(BBJ[Lorg/apache/poi/hslf/record/Record;Ljava/io/OutputStream;)V

    .line 97
    return-void
.end method
