.class public final Lorg/apache/poi/hslf/blip/WMF;
.super Lorg/apache/poi/hslf/blip/Metafile;
.source "WMF.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hslf/blip/WMF$AldusHeader;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hslf/blip/Metafile;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hslf/blip/WMF;)Lorg/apache/poi/util/POILogger;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/poi/hslf/blip/WMF;->logger:Lorg/apache/poi/util/POILogger;

    return-object v0
.end method


# virtual methods
.method public getData()[B
    .locals 12

    .prologue
    .line 41
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/blip/WMF;->getRawData()[B

    move-result-object v8

    .line 43
    .local v8, "rawdata":[B
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 44
    .local v7, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 45
    .local v6, "is":Ljava/io/InputStream;
    new-instance v4, Lorg/apache/poi/hslf/blip/Metafile$Header;

    invoke-direct {v4}, Lorg/apache/poi/hslf/blip/Metafile$Header;-><init>()V

    .line 46
    .local v4, "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    const/16 v9, 0x10

    invoke-virtual {v4, v8, v9}, Lorg/apache/poi/hslf/blip/Metafile$Header;->read([BI)V

    .line 47
    invoke-virtual {v4}, Lorg/apache/poi/hslf/blip/Metafile$Header;->getSize()I

    move-result v9

    add-int/lit8 v9, v9, 0x10

    int-to-long v10, v9

    invoke-virtual {v6, v10, v11}, Ljava/io/InputStream;->skip(J)J

    .line 49
    new-instance v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;

    invoke-direct {v0, p0}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;-><init>(Lorg/apache/poi/hslf/blip/WMF;)V

    .line 50
    .local v0, "aldus":Lorg/apache/poi/hslf/blip/WMF$AldusHeader;
    iget-object v9, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v9, v9, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iput v9, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->left:I

    .line 51
    iget-object v9, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v9, v9, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iput v9, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->top:I

    .line 52
    iget-object v9, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v9, v9, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget-object v10, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v10, v10, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v9, v10

    iput v9, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->right:I

    .line 53
    iget-object v9, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v9, v9, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget-object v10, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v10, v10, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v9, v10

    iput v9, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->bottom:I

    .line 54
    invoke-virtual {v0, v7}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->write(Ljava/io/OutputStream;)V

    .line 56
    new-instance v5, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v5, v6}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 57
    .local v5, "inflater":Ljava/util/zip/InflaterInputStream;
    const/16 v9, 0x1000

    new-array v1, v9, [B

    .line 59
    .local v1, "chunk":[B
    :goto_0
    invoke-virtual {v5, v1}, Ljava/util/zip/InflaterInputStream;->read([B)I

    move-result v2

    .local v2, "count":I
    if-gez v2, :cond_0

    .line 62
    invoke-virtual {v5}, Ljava/util/zip/InflaterInputStream;->close()V

    .line 63
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    return-object v9

    .line 60
    :cond_0
    const/4 v9, 0x0

    invoke-virtual {v7, v1, v9, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 64
    .end local v0    # "aldus":Lorg/apache/poi/hslf/blip/WMF$AldusHeader;
    .end local v1    # "chunk":[B
    .end local v2    # "count":I
    .end local v4    # "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    .end local v5    # "inflater":Ljava/util/zip/InflaterInputStream;
    .end local v6    # "is":Ljava/io/InputStream;
    .end local v7    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "rawdata":[B
    :catch_0
    move-exception v3

    .line 65
    .local v3, "e":Ljava/io/IOException;
    new-instance v9, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v9, v3}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v9
.end method

.method public getSignature()I
    .locals 1

    .prologue
    .line 105
    const/16 v0, 0x2160

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x3

    return v0
.end method

.method public setData([B)V
    .locals 13
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v6, 0x0

    .line 71
    .local v6, "pos":I
    new-instance v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;

    invoke-direct {v0, p0}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;-><init>(Lorg/apache/poi/hslf/blip/WMF;)V

    .line 72
    .local v0, "aldus":Lorg/apache/poi/hslf/blip/WMF$AldusHeader;
    invoke-virtual {v0, p1, v6}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->read([BI)V

    .line 73
    invoke-virtual {v0}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->getSize()I

    move-result v7

    add-int/2addr v6, v7

    .line 75
    array-length v7, p1

    sub-int/2addr v7, v6

    invoke-virtual {p0, p1, v6, v7}, Lorg/apache/poi/hslf/blip/WMF;->compress([BII)[B

    move-result-object v3

    .line 77
    .local v3, "compressed":[B
    new-instance v4, Lorg/apache/poi/hslf/blip/Metafile$Header;

    invoke-direct {v4}, Lorg/apache/poi/hslf/blip/Metafile$Header;-><init>()V

    .line 78
    .local v4, "header":Lorg/apache/poi/hslf/blip/Metafile$Header;
    array-length v7, p1

    invoke-virtual {v0}, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->getSize()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->wmfsize:I

    .line 79
    new-instance v7, Lorg/apache/poi/java/awt/Rectangle;

    iget v8, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->left:I

    int-to-short v8, v8

    iget v9, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->top:I

    int-to-short v9, v9

    iget v10, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->right:I

    int-to-short v10, v10

    iget v11, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->left:I

    int-to-short v11, v11

    sub-int/2addr v10, v11

    iget v11, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->bottom:I

    int-to-short v11, v11

    iget v12, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->top:I

    int-to-short v12, v12

    sub-int/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    iput-object v7, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    .line 81
    const v7, 0x129a80

    iget v8, v0, Lorg/apache/poi/hslf/blip/WMF$AldusHeader;->inch:I

    div-int v2, v7, v8

    .line 82
    .local v2, "coeff":I
    new-instance v7, Lorg/apache/poi/java/awt/Dimension;

    iget-object v8, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v8, v8, Lorg/apache/poi/java/awt/Rectangle;->width:I

    mul-int/2addr v8, v2

    iget-object v9, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->bounds:Lorg/apache/poi/java/awt/Rectangle;

    iget v9, v9, Lorg/apache/poi/java/awt/Rectangle;->height:I

    mul-int/2addr v9, v2

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    iput-object v7, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->size:Lorg/apache/poi/java/awt/Dimension;

    .line 83
    array-length v7, v3

    iput v7, v4, Lorg/apache/poi/hslf/blip/Metafile$Header;->zipsize:I

    .line 85
    invoke-static {p1}, Lorg/apache/poi/hslf/blip/WMF;->getChecksum([B)[B

    move-result-object v1

    .line 86
    .local v1, "checksum":[B
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 87
    .local v5, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v5, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 88
    invoke-virtual {v4, v5}, Lorg/apache/poi/hslf/blip/Metafile$Header;->write(Ljava/io/OutputStream;)V

    .line 89
    invoke-virtual {v5, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 91
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/poi/hslf/blip/WMF;->setRawData([B)V

    .line 92
    return-void
.end method
