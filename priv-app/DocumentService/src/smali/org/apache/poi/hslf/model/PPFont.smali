.class public final Lorg/apache/poi/hslf/model/PPFont;
.super Ljava/lang/Object;
.source "PPFont.java"


# static fields
.field public static final ANSI_CHARSET:B = 0x0t

.field public static final ARIAL:Lorg/apache/poi/hslf/model/PPFont;

.field public static final COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

.field public static final DEFAULT_CHARSET:B = 0x1t

.field public static final DEFAULT_PITCH:B = 0x0t

.field public static final FF_DECORATIVE:B = 0x50t

.field public static final FF_DONTCARE:B = 0x0t

.field public static final FF_MODERN:B = 0x30t

.field public static final FF_ROMAN:B = 0x10t

.field public static final FF_SCRIPT:B = 0x40t

.field public static final FF_SWISS:B = 0x20t

.field public static final FIXED_PITCH:B = 0x1t

.field public static final SYMBOL_CHARSET:B = 0x2t

.field public static final TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

.field public static final VARIABLE_PITCH:B = 0x2t

.field public static final WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;


# instance fields
.field protected charset:I

.field protected flags:I

.field protected name:Ljava/lang/String;

.field protected pitch:I

.field protected type:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 215
    new-instance v0, Lorg/apache/poi/hslf/model/PPFont;

    invoke-direct {v0}, Lorg/apache/poi/hslf/model/PPFont;-><init>()V

    sput-object v0, Lorg/apache/poi/hslf/model/PPFont;->ARIAL:Lorg/apache/poi/hslf/model/PPFont;

    .line 216
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->ARIAL:Lorg/apache/poi/hslf/model/PPFont;

    const-string/jumbo v1, "Arial"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 217
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->ARIAL:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setCharSet(I)V

    .line 218
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->ARIAL:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/PPFont;->setFontType(I)V

    .line 219
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->ARIAL:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setFontFlags(I)V

    .line 220
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->ARIAL:Lorg/apache/poi/hslf/model/PPFont;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 222
    new-instance v0, Lorg/apache/poi/hslf/model/PPFont;

    invoke-direct {v0}, Lorg/apache/poi/hslf/model/PPFont;-><init>()V

    sput-object v0, Lorg/apache/poi/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

    .line 223
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

    const-string/jumbo v1, "Times New Roman"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 224
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setCharSet(I)V

    .line 225
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/PPFont;->setFontType(I)V

    .line 226
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setFontFlags(I)V

    .line 227
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lorg/apache/poi/hslf/model/PPFont;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 229
    new-instance v0, Lorg/apache/poi/hslf/model/PPFont;

    invoke-direct {v0}, Lorg/apache/poi/hslf/model/PPFont;-><init>()V

    sput-object v0, Lorg/apache/poi/hslf/model/PPFont;->COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

    .line 230
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

    const-string/jumbo v1, "Courier New"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 231
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setCharSet(I)V

    .line 232
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/PPFont;->setFontType(I)V

    .line 233
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setFontFlags(I)V

    .line 234
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->COURIER_NEW:Lorg/apache/poi/hslf/model/PPFont;

    const/16 v1, 0x31

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 236
    new-instance v0, Lorg/apache/poi/hslf/model/PPFont;

    invoke-direct {v0}, Lorg/apache/poi/hslf/model/PPFont;-><init>()V

    sput-object v0, Lorg/apache/poi/hslf/model/PPFont;->WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;

    .line 237
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;

    const-string/jumbo v1, "Wingdings"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 238
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/PPFont;->setCharSet(I)V

    .line 239
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/model/PPFont;->setFontType(I)V

    .line 240
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v2}, Lorg/apache/poi/hslf/model/PPFont;->setFontFlags(I)V

    .line 241
    sget-object v0, Lorg/apache/poi/hslf/model/PPFont;->WINGDINGS:Lorg/apache/poi/hslf/model/PPFont;

    invoke-virtual {v0, v4}, Lorg/apache/poi/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 242
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/record/FontEntityAtom;)V
    .locals 1
    .param p1, "fontAtom"    # Lorg/apache/poi/hslf/record/FontEntityAtom;

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getFontName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/PPFont;->name:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getCharSet()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/model/PPFont;->charset:I

    .line 102
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getFontType()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/model/PPFont;->type:I

    .line 103
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getFontFlags()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/model/PPFont;->flags:I

    .line 104
    invoke-virtual {p1}, Lorg/apache/poi/hslf/record/FontEntityAtom;->getPitchAndFamily()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/model/PPFont;->pitch:I

    .line 105
    return-void
.end method


# virtual methods
.method public getCharSet()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lorg/apache/poi/hslf/model/PPFont;->charset:I

    return v0
.end method

.method public getFontFlags()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/poi/hslf/model/PPFont;->flags:I

    return v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/poi/hslf/model/PPFont;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getFontType()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lorg/apache/poi/hslf/model/PPFont;->type:I

    return v0
.end method

.method public getPitchAndFamily()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/poi/hslf/model/PPFont;->pitch:I

    return v0
.end method

.method public setCharSet(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 131
    iput p1, p0, Lorg/apache/poi/hslf/model/PPFont;->charset:I

    .line 132
    return-void
.end method

.method public setFontFlags(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 150
    iput p1, p0, Lorg/apache/poi/hslf/model/PPFont;->flags:I

    .line 151
    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/poi/hslf/model/PPFont;->name:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setFontType(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 174
    iput p1, p0, Lorg/apache/poi/hslf/model/PPFont;->type:I

    .line 175
    return-void
.end method

.method public setPitchAndFamily(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 198
    iput p1, p0, Lorg/apache/poi/hslf/model/PPFont;->pitch:I

    .line 199
    return-void
.end method
