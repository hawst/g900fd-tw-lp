.class public final Lorg/apache/poi/hslf/model/Line;
.super Lorg/apache/poi/hslf/model/SimpleShape;
.source "Line.java"


# static fields
.field public static final LINE_DOUBLE:I = 0x1

.field public static final LINE_SIMPLE:I = 0x0

.field public static final LINE_THICKTHIN:I = 0x2

.field public static final LINE_THINTHICK:I = 0x3

.field public static final LINE_TRIPLE:I = 0x4

.field public static final PEN_DASH:I = 0x7

.field public static final PEN_DASHDOT:I = 0x4

.field public static final PEN_DASHDOTDOT:I = 0x5

.field public static final PEN_DASHDOTGEL:I = 0x9

.field public static final PEN_DOT:I = 0x3

.field public static final PEN_DOTGEL:I = 0x6

.field public static final PEN_LONGDASHDOTDOTGEL:I = 0xb

.field public static final PEN_LONGDASHDOTGEL:I = 0xa

.field public static final PEN_LONGDASHGEL:I = 0x8

.field public static final PEN_PS_DASH:I = 0x2

.field public static final PEN_SOLID:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/model/Line;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 109
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/SimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/Shape;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/model/SimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 104
    instance-of v0, p1, Lorg/apache/poi/hslf/model/ShapeGroup;

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/Line;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/model/Line;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 105
    return-void
.end method


# virtual methods
.method protected createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 5
    .param p1, "isChild"    # Z

    .prologue
    .line 112
    invoke-super {p0, p1}, Lorg/apache/poi/hslf/model/SimpleShape;->createSpContainer(Z)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hslf/model/Line;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 114
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Line;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff6

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 115
    .local v1, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    const/16 v2, 0x142

    .line 116
    .local v2, "type":S
    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 119
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Line;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v4, -0xff5

    invoke-static {v3, v4}, Lorg/apache/poi/hslf/model/Line;->getEscherChild(Lorg/apache/poi/ddf/EscherContainerRecord;I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 122
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v3, 0x144

    const/4 v4, 0x4

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hslf/model/Line;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 123
    const/16 v3, 0x17f

    const/high16 v4, 0x10000

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hslf/model/Line;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 124
    const/16 v3, 0x1bf

    const/high16 v4, 0x100000

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hslf/model/Line;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 125
    const/16 v3, 0x1c0

    const v4, 0x8000001

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hslf/model/Line;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 126
    const/16 v3, 0x1ff

    const v4, 0xa0008

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hslf/model/Line;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 127
    const/16 v3, 0x201

    const v4, 0x8000002

    invoke-static {v0, v3, v4}, Lorg/apache/poi/hslf/model/Line;->setEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;SI)V

    .line 129
    iget-object v3, p0, Lorg/apache/poi/hslf/model/Line;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v3
.end method
