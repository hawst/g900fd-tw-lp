.class public final Lorg/apache/poi/hslf/record/TextHeaderAtom;
.super Lorg/apache/poi/hslf/record/RecordAtom;
.source "TextHeaderAtom.java"

# interfaces
.implements Lorg/apache/poi/hslf/record/ParentAwareRecord;


# static fields
.field public static final BODY_TYPE:I = 0x1

.field public static final CENTER_TITLE_TYPE:I = 0x6

.field public static final CENTRE_BODY_TYPE:I = 0x5

.field public static final HALF_BODY_TYPE:I = 0x7

.field public static final NOTES_TYPE:I = 0x2

.field public static final OTHER_TYPE:I = 0x4

.field public static final QUARTER_BODY_TYPE:I = 0x8

.field public static final TITLE_TYPE:I

.field private static _type:J


# instance fields
.field private _header:[B

.field private parentRecord:Lorg/apache/poi/hslf/record/RecordContainer;

.field private textType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/16 v0, 0xf9f

    sput-wide v0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_type:J

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x4

    .line 81
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 82
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    .line 83
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    invoke-static {v0, v1, v1}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 84
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/LittleEndian;->putUShort([BII)V

    .line 85
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    invoke-static {v0, v4, v4}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 87
    iput v4, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->textType:I

    .line 88
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0xc

    const/16 v2, 0x8

    .line 61
    invoke-direct {p0}, Lorg/apache/poi/hslf/record/RecordAtom;-><init>()V

    .line 63
    if-ge p3, v1, :cond_0

    .line 64
    const/16 p3, 0xc

    .line 65
    array-length v0, p1

    sub-int/2addr v0, p2

    if-ge v0, v1, :cond_0

    .line 66
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Not enough data to form a TextHeaderAtom (always 12 bytes long) - found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    sub-int/2addr v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    .line 72
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->textType:I

    .line 76
    return-void
.end method


# virtual methods
.method public getParentRecord()Lorg/apache/poi/hslf/record/RecordContainer;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->parentRecord:Lorg/apache/poi/hslf/record/RecordContainer;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 93
    sget-wide v0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_type:J

    return-wide v0
.end method

.method public getTextType()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->textType:I

    return v0
.end method

.method public setParentRecord(Lorg/apache/poi/hslf/record/RecordContainer;)V
    .locals 0
    .param p1, "record"    # Lorg/apache/poi/hslf/record/RecordContainer;

    .prologue
    .line 54
    iput-object p1, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->parentRecord:Lorg/apache/poi/hslf/record/RecordContainer;

    return-void
.end method

.method public setTextType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 51
    iput p1, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->textType:I

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 104
    iget v0, p0, Lorg/apache/poi/hslf/record/TextHeaderAtom;->textType:I

    invoke-static {v0, p1}, Lorg/apache/poi/hslf/record/TextHeaderAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 105
    return-void
.end method
