.class public final Lorg/apache/poi/hslf/usermodel/RichTextRun;
.super Ljava/lang/Object;
.source "RichTextRun.java"


# instance fields
.field private _fontname:Ljava/lang/String;

.field private characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

.field private length:I

.field protected logger:Lorg/apache/poi/util/POILogger;

.field private paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

.field private parentRun:Lorg/apache/poi/hslf/model/TextRun;

.field private sharingCharacterStyle:Z

.field private sharingParagraphStyle:Z

.field private slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

.field private startPos:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/model/TextRun;II)V
    .locals 8
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/TextRun;
    .param p2, "startAt"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 72
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, v4

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hslf/usermodel/RichTextRun;-><init>(Lorg/apache/poi/hslf/model/TextRun;IILorg/apache/poi/hslf/model/textproperties/TextPropCollection;Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;ZZ)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/model/TextRun;IILorg/apache/poi/hslf/model/textproperties/TextPropCollection;Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;ZZ)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/poi/hslf/model/TextRun;
    .param p2, "startAt"    # I
    .param p3, "len"    # I
    .param p4, "pStyle"    # Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .param p5, "cStyle"    # Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .param p6, "pShared"    # Z
    .param p7, "cShared"    # Z

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->logger:Lorg/apache/poi/util/POILogger;

    .line 87
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    .line 88
    iput p2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    .line 89
    iput p3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->length:I

    .line 90
    iput-object p4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 91
    iput-object p5, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 92
    iput-boolean p6, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->sharingParagraphStyle:Z

    .line 93
    iput-boolean p7, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->sharingCharacterStyle:Z

    .line 94
    return-void
.end method

.method private fetchOrAddTextProp(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;
    .locals 1
    .param p1, "textPropCol"    # Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .param p2, "textPropName"    # Ljava/lang/String;

    .prologue
    .line 264
    invoke-virtual {p1, p2}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v0

    .line 265
    .local v0, "tp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p1, p2}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->addWithName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v0

    .line 268
    :cond_0
    return-object v0
.end method

.method private getCharTextPropVal(Ljava/lang/String;)I
    .locals 6
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    .line 278
    const/4 v1, 0x0

    .line 279
    .local v1, "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    iget-object v4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-eqz v4, :cond_0

    .line 280
    iget-object v4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    .line 283
    :cond_0
    if-nez v1, :cond_1

    .line 284
    iget-object v4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/TextRun;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v2

    .line 285
    .local v2, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    iget-object v4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/TextRun;->getRunType()I

    move-result v3

    .line 286
    .local v3, "txtype":I
    invoke-virtual {v2}, Lorg/apache/poi/hslf/model/Sheet;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    .line 287
    .local v0, "master":Lorg/apache/poi/hslf/model/MasterSheet;
    if-eqz v0, :cond_1

    .line 288
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v0, v3, v4, p1, v5}, Lorg/apache/poi/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    .line 291
    .end local v0    # "master":Lorg/apache/poi/hslf/model/MasterSheet;
    .end local v2    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    .end local v3    # "txtype":I
    :cond_1
    if-nez v1, :cond_2

    const/high16 v4, -0x1000000

    :goto_0
    return v4

    :cond_2
    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v4

    goto :goto_0
.end method

.method private getFlag(ZI)Z
    .locals 9
    .param p1, "isCharacter"    # Z
    .param p2, "index"    # I

    .prologue
    .line 197
    if-eqz p1, :cond_2

    .line 198
    iget-object v3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 199
    .local v3, "props":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    sget-object v2, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;->NAME:Ljava/lang/String;

    .line 205
    .local v2, "propname":Ljava/lang/String;
    :goto_0
    const/4 v1, 0x0

    .line 206
    .local v1, "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    if-eqz v3, :cond_0

    .line 207
    invoke-virtual {v3, v2}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    .end local v1    # "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    check-cast v1, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    .line 209
    .restart local v1    # "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    :cond_0
    if-nez v1, :cond_1

    .line 210
    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TextRun;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v4

    .line 211
    .local v4, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    if-eqz v4, :cond_3

    .line 212
    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v6}, Lorg/apache/poi/hslf/model/TextRun;->getRunType()I

    move-result v5

    .line 213
    .local v5, "txtype":I
    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Sheet;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v0

    .line 214
    .local v0, "master":Lorg/apache/poi/hslf/model/MasterSheet;
    if-eqz v0, :cond_1

    .line 215
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v6

    invoke-virtual {v0, v5, v6, v2, p1}, Lorg/apache/poi/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    .end local v1    # "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    check-cast v1, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    .line 222
    .end local v0    # "master":Lorg/apache/poi/hslf/model/MasterSheet;
    .end local v4    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    .end local v5    # "txtype":I
    .restart local v1    # "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    :cond_1
    :goto_1
    if-nez v1, :cond_4

    const/4 v6, 0x0

    :goto_2
    return v6

    .line 201
    .end local v1    # "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    .end local v2    # "propname":Ljava/lang/String;
    .end local v3    # "props":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 202
    .restart local v3    # "props":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    sget-object v2, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    .restart local v2    # "propname":Ljava/lang/String;
    goto :goto_0

    .line 218
    .restart local v1    # "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    .restart local v4    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    :cond_3
    iget-object v6, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v7, 0x5

    const-string/jumbo v8, "MasterSheet is not available"

    invoke-virtual {v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 222
    .end local v4    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    :cond_4
    invoke-virtual {v1, p2}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->getSubValue(I)Z

    move-result v6

    goto :goto_2
.end method

.method private getParaTextPropVal(Ljava/lang/String;)I
    .locals 9
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 300
    const/4 v3, 0x0

    .line 301
    .local v3, "prop":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    const/4 v0, 0x0

    .line 302
    .local v0, "hardAttribute":Z
    iget-object v7, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-eqz v7, :cond_0

    .line 303
    iget-object v7, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-virtual {v7, p1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v3

    .line 305
    iget-object v7, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    sget-object v8, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    .line 306
    .local v1, "maskProp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->getValue()I

    move-result v7

    if-nez v7, :cond_2

    const/4 v0, 0x1

    .line 308
    .end local v1    # "maskProp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    :cond_0
    :goto_0
    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 309
    iget-object v7, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/TextRun;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v4

    .line 310
    .local v4, "sheet":Lorg/apache/poi/hslf/model/Sheet;
    iget-object v7, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v7}, Lorg/apache/poi/hslf/model/TextRun;->getRunType()I

    move-result v5

    .line 311
    .local v5, "txtype":I
    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Sheet;->getMasterSheet()Lorg/apache/poi/hslf/model/MasterSheet;

    move-result-object v2

    .line 312
    .local v2, "master":Lorg/apache/poi/hslf/model/MasterSheet;
    if-eqz v2, :cond_1

    .line 313
    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getIndentLevel()I

    move-result v7

    invoke-virtual {v2, v5, v7, p1, v6}, Lorg/apache/poi/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v3

    .line 316
    .end local v2    # "master":Lorg/apache/poi/hslf/model/MasterSheet;
    .end local v4    # "sheet":Lorg/apache/poi/hslf/model/Sheet;
    .end local v5    # "txtype":I
    :cond_1
    if-nez v3, :cond_3

    const/4 v6, -0x1

    :goto_1
    return v6

    .restart local v1    # "maskProp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    :cond_2
    move v0, v6

    .line 306
    goto :goto_0

    .line 316
    .end local v1    # "maskProp":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    :cond_3
    invoke-virtual {v3}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->getValue()I

    move-result v6

    goto :goto_1
.end method

.method private isCharFlagsTextPropVal(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 191
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    move-result v0

    return v0
.end method

.method private setCharFlagsTextPropVal(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # Z

    .prologue
    const/4 v1, 0x1

    .line 230
    invoke-direct {p0, v1, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    move-result v0

    if-eq v0, p2, :cond_0

    invoke-virtual {p0, v1, p1, p2}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setFlag(ZIZ)V

    .line 231
    :cond_0
    return-void
.end method


# virtual methods
.method public _getRawCharacterStyle()Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    return-object v0
.end method

.method public _getRawParagraphStyle()Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    return-object v0
.end method

.method public _isCharacterStyleShared()Z
    .locals 1

    .prologue
    .line 797
    iget-boolean v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->sharingCharacterStyle:Z

    return v0
.end method

.method public _isParagraphStyleShared()Z
    .locals 1

    .prologue
    .line 793
    iget-boolean v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->sharingParagraphStyle:Z

    return v0
.end method

.method public getAlignment()I
    .locals 1

    .prologue
    .line 568
    const-string/jumbo v0, "alignment"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getBulletChar()C
    .locals 1

    .prologue
    .line 620
    const-string/jumbo v0, "bullet.char"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public getBulletColor()Lorg/apache/poi/java/awt/Color;
    .locals 8

    .prologue
    .line 677
    const-string/jumbo v4, "bullet.color"

    invoke-direct {p0, v4}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v2

    .line 678
    .local v2, "rgb":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFontColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    .line 686
    :goto_0
    return-object v4

    .line 680
    :cond_0
    shr-int/lit8 v1, v2, 0x18

    .line 681
    .local v1, "cidx":I
    const/high16 v4, 0x1000000

    rem-int v4, v2, v4

    if-nez v4, :cond_1

    .line 682
    iget-object v4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/TextRun;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Sheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v0

    .line 683
    .local v0, "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    if-ltz v1, :cond_1

    const/4 v4, 0x7

    if-gt v1, v4, :cond_1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->getColor(I)I

    move-result v2

    .line 685
    .end local v0    # "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    :cond_1
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 686
    .local v3, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v4, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    goto :goto_0
.end method

.method public getBulletFont()I
    .locals 1

    .prologue
    .line 701
    const-string/jumbo v0, "bullet.font"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getBulletOffset()I
    .locals 1

    .prologue
    .line 634
    const-string/jumbo v0, "bullet.offset"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x48

    div-int/lit16 v0, v0, 0x240

    return v0
.end method

.method public getBulletSize()I
    .locals 1

    .prologue
    .line 662
    const-string/jumbo v0, "bullet.size"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getEndIndex()I
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    iget v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->length:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getFontColor()Lorg/apache/poi/java/awt/Color;
    .locals 8

    .prologue
    .line 522
    const-string/jumbo v4, "font.color"

    invoke-direct {p0, v4}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    move-result v2

    .line 524
    .local v2, "rgb":I
    shr-int/lit8 v1, v2, 0x18

    .line 525
    .local v1, "cidx":I
    const/high16 v4, 0x1000000

    rem-int v4, v2, v4

    if-nez v4, :cond_0

    .line 526
    iget-object v4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/TextRun;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hslf/model/Sheet;->getColorScheme()Lorg/apache/poi/hslf/record/ColorSchemeAtom;

    move-result-object v0

    .line 527
    .local v0, "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    if-ltz v1, :cond_0

    const/4 v4, 0x7

    if-gt v1, v4, :cond_0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/record/ColorSchemeAtom;->getColor(I)I

    move-result v2

    .line 529
    .end local v0    # "ca":Lorg/apache/poi/hslf/record/ColorSchemeAtom;
    :cond_0
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 530
    .local v3, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v4, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    return-object v4
.end method

.method public getFontIndex()I
    .locals 1

    .prologue
    .line 480
    const-string/jumbo v0, "font.index"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 509
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    if-nez v1, :cond_0

    .line 510
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 514
    :goto_0
    return-object v1

    .line 512
    :cond_0
    const-string/jumbo v1, "font.index"

    invoke-direct {p0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    move-result v0

    .line 513
    .local v0, "fontIdx":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    .line 514
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/poi/hslf/record/FontCollection;->getFontWithId(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getFontSize()I
    .locals 1

    .prologue
    .line 465
    const-string/jumbo v0, "font.size"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getIndentLevel()I
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->getReservedField()S

    move-result v0

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->length:I

    return v0
.end method

.method public getLineSpacing()I
    .locals 2

    .prologue
    .line 725
    const-string/jumbo v1, "linespacing"

    invoke-direct {p0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    .line 726
    .local v0, "val":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "val":I
    :cond_0
    return v0
.end method

.method public getRawText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/TextRun;->getRawText()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    iget v2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    iget v3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->length:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpaceAfter()I
    .locals 2

    .prologue
    .line 775
    const-string/jumbo v1, "spaceafter"

    invoke-direct {p0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    .line 776
    .local v0, "val":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "val":I
    :cond_0
    return v0
.end method

.method public getSpaceBefore()I
    .locals 2

    .prologue
    .line 750
    const-string/jumbo v1, "spacebefore"

    invoke-direct {p0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    .line 751
    .local v0, "val":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "val":I
    :cond_0
    return v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    return v0
.end method

.method public getSuperscript()I
    .locals 2

    .prologue
    .line 448
    const-string/jumbo v1, "superscript"

    invoke-direct {p0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    move-result v0

    .line 449
    .local v0, "val":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "val":I
    :cond_0
    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/model/TextRun;->getText()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    iget v2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    iget v3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->length:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextOffset()I
    .locals 1

    .prologue
    .line 648
    const-string/jumbo v0, "text.offset"

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x48

    div-int/lit16 v0, v0, 0x240

    return v0
.end method

.method public isBold()Z
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    move-result v0

    return v0
.end method

.method public isBullet()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 599
    invoke-direct {p0, v0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    move-result v0

    return v0
.end method

.method public isBulletHard()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 606
    invoke-direct {p0, v0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    move-result v0

    return v0
.end method

.method public isEmbossed()Z
    .locals 1

    .prologue
    .line 418
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    move-result v0

    return v0
.end method

.method public isItalic()Z
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    move-result v0

    return v0
.end method

.method public isShadowed()Z
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    move-result v0

    return v0
.end method

.method public isStrikethrough()Z
    .locals 1

    .prologue
    .line 432
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    move-result v0

    return v0
.end method

.method public isUnderlined()Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    move-result v0

    return v0
.end method

.method public setAlignment(I)V
    .locals 1
    .param p1, "align"    # I

    .prologue
    .line 559
    const-string/jumbo v0, "alignment"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 560
    return-void
.end method

.method public setBold(Z)V
    .locals 1
    .param p1, "bold"    # Z

    .prologue
    .line 369
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 370
    return-void
.end method

.method public setBullet(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    const/4 v0, 0x0

    .line 592
    invoke-virtual {p0, v0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setFlag(ZIZ)V

    .line 593
    return-void
.end method

.method public setBulletChar(C)V
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 613
    const-string/jumbo v0, "bullet.char"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 614
    return-void
.end method

.method public setBulletColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 6
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 669
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    const/16 v5, 0xfe

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v0

    .line 670
    .local v0, "rgb":I
    const-string/jumbo v1, "bullet.color"

    invoke-virtual {p0, v1, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 671
    return-void
.end method

.method public setBulletFont(I)V
    .locals 2
    .param p1, "idx"    # I

    .prologue
    const/4 v1, 0x1

    .line 693
    const-string/jumbo v0, "bullet.font"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 694
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setFlag(ZIZ)V

    .line 695
    return-void
.end method

.method public setBulletOffset(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 627
    const-string/jumbo v0, "bullet.offset"

    mul-int/lit16 v1, p1, 0x240

    div-int/lit8 v1, v1, 0x48

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 628
    return-void
.end method

.method public setBulletSize(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 655
    const-string/jumbo v0, "bullet.size"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 656
    return-void
.end method

.method public setCharTextPropVal(Ljava/lang/String;I)V
    .locals 2
    .param p1, "propName"    # Ljava/lang/String;
    .param p2, "val"    # I

    .prologue
    .line 344
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-nez v1, :cond_0

    .line 345
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 349
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-nez v1, :cond_1

    .line 353
    :goto_0
    return-void

    .line 351
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {p0, v1, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->fetchOrAddTextProp(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v0

    .line 352
    .local v0, "tp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->setValue(I)V

    goto :goto_0
.end method

.method public setEmbossed(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 425
    const/16 v0, 0x9

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 426
    return-void
.end method

.method public setFlag(ZIZ)V
    .locals 4
    .param p1, "isCharacter"    # Z
    .param p2, "index"    # I
    .param p3, "value"    # Z

    .prologue
    .line 236
    if-eqz p1, :cond_1

    .line 237
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 238
    .local v2, "props":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    sget-object v1, Lorg/apache/poi/hslf/model/textproperties/CharFlagsTextProp;->NAME:Ljava/lang/String;

    .line 245
    .local v1, "propname":Ljava/lang/String;
    :goto_0
    if-nez v2, :cond_0

    .line 246
    iget-object v3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 247
    if-eqz p1, :cond_2

    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 250
    :cond_0
    :goto_1
    if-nez v2, :cond_3

    .line 254
    :goto_2
    return-void

    .line 240
    .end local v1    # "propname":Ljava/lang/String;
    .end local v2    # "props":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 241
    .restart local v2    # "props":Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    sget-object v1, Lorg/apache/poi/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    .restart local v1    # "propname":Ljava/lang/String;
    goto :goto_0

    .line 247
    :cond_2
    iget-object v2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    goto :goto_1

    .line 252
    :cond_3
    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->fetchOrAddTextProp(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;

    .line 253
    .local v0, "prop":Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;
    invoke-virtual {v0, p3, p2}, Lorg/apache/poi/hslf/model/textproperties/BitMaskTextProp;->setSubValue(ZI)V

    goto :goto_2
.end method

.method public setFontColor(I)V
    .locals 1
    .param p1, "bgr"    # I

    .prologue
    .line 540
    const-string/jumbo v0, "font.color"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 541
    return-void
.end method

.method public setFontColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 6
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 548
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    const/16 v5, 0xfe

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v0

    .line 549
    .local v0, "rgb":I
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setFontColor(I)V

    .line 550
    return-void
.end method

.method public setFontIndex(I)V
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 487
    const-string/jumbo v0, "font.index"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 488
    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 2
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 495
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    if-nez v1, :cond_0

    .line 497
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 503
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getFontCollection()Lorg/apache/poi/hslf/record/FontCollection;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/poi/hslf/record/FontCollection;->addFont(Ljava/lang/String;)I

    move-result v0

    .line 501
    .local v0, "fontIdx":I
    const-string/jumbo v1, "font.index"

    invoke-virtual {p0, v1, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public setFontSize(I)V
    .locals 1
    .param p1, "fontSize"    # I

    .prologue
    .line 473
    const-string/jumbo v0, "font.size"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 474
    return-void
.end method

.method public setIndentLevel(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 585
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;->setReservedField(S)V

    .line 586
    :cond_0
    return-void
.end method

.method public setItalic(Z)V
    .locals 1
    .param p1, "italic"    # Z

    .prologue
    .line 383
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 384
    return-void
.end method

.method public setLineSpacing(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 712
    const-string/jumbo v0, "linespacing"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 713
    return-void
.end method

.method public setParaTextPropVal(Ljava/lang/String;I)V
    .locals 2
    .param p1, "propName"    # Ljava/lang/String;
    .param p2, "val"    # I

    .prologue
    .line 326
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-nez v1, :cond_0

    .line 327
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 332
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-nez v1, :cond_1

    .line 336
    :goto_0
    return-void

    .line 334
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    invoke-direct {p0, v1, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->fetchOrAddTextProp(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lorg/apache/poi/hslf/model/textproperties/TextProp;

    move-result-object v0

    .line 335
    .local v0, "tp":Lorg/apache/poi/hslf/model/textproperties/TextProp;
    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/model/textproperties/TextProp;->setValue(I)V

    goto :goto_0
.end method

.method public setRawText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->length:I

    .line 171
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v0, p0, p1}, Lorg/apache/poi/hslf/model/TextRun;->changeTextInRichTextRun(Lorg/apache/poi/hslf/usermodel/RichTextRun;Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public setShadowed(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 411
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 412
    return-void
.end method

.method public setSpaceAfter(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 762
    const-string/jumbo v0, "spaceafter"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 763
    return-void
.end method

.method public setSpaceBefore(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 737
    const-string/jumbo v0, "spacebefore"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 738
    return-void
.end method

.method public setStrikethrough(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 439
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 440
    return-void
.end method

.method public setSuperscript(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 458
    const-string/jumbo v0, "superscript"

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 459
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 162
    iget-object v1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->parentRun:Lorg/apache/poi/hslf/model/TextRun;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hslf/model/TextRun;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setRawText(Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public setTextOffset(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 641
    const-string/jumbo v0, "text.offset"

    mul-int/lit16 v1, p1, 0x240

    div-int/lit8 v1, v1, 0x48

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 642
    return-void
.end method

.method public setUnderlined(Z)V
    .locals 1
    .param p1, "underlined"    # Z

    .prologue
    .line 397
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 398
    return-void
.end method

.method public supplySlideShow(Lorg/apache/poi/hslf/usermodel/SlideShow;)V
    .locals 1
    .param p1, "ss"    # Lorg/apache/poi/hslf/usermodel/SlideShow;

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->slideShow:Lorg/apache/poi/hslf/usermodel/SlideShow;

    .line 114
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/usermodel/RichTextRun;->setFontName(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 118
    :cond_0
    return-void
.end method

.method public supplyTextProps(Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;ZZ)V
    .locals 2
    .param p1, "pStyle"    # Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .param p2, "cStyle"    # Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;
    .param p3, "pShared"    # Z
    .param p4, "cShared"    # Z

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    if-eqz v0, :cond_1

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Can\'t call supplyTextProps if run already has some"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_1
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->paragraphStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 105
    iput-object p2, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->characterStyle:Lorg/apache/poi/hslf/model/textproperties/TextPropCollection;

    .line 106
    iput-boolean p3, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->sharingParagraphStyle:Z

    .line 107
    iput-boolean p4, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->sharingCharacterStyle:Z

    .line 108
    return-void
.end method

.method public updateStartPosition(I)V
    .locals 0
    .param p1, "startAt"    # I

    .prologue
    .line 179
    iput p1, p0, Lorg/apache/poi/hslf/usermodel/RichTextRun;->startPos:I

    .line 180
    return-void
.end method
