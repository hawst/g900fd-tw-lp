.class public Lorg/apache/poi/hslf/usermodel/ObjectData;
.super Ljava/lang/Object;
.source "ObjectData.java"


# instance fields
.field private storage:Lorg/apache/poi/hslf/record/ExOleObjStg;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/ExOleObjStg;)V
    .locals 0
    .param p1, "storage"    # Lorg/apache/poi/hslf/record/ExOleObjStg;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/poi/hslf/usermodel/ObjectData;->storage:Lorg/apache/poi/hslf/record/ExOleObjStg;

    .line 42
    return-void
.end method


# virtual methods
.method public getData()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/ObjectData;->storage:Lorg/apache/poi/hslf/record/ExOleObjStg;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExOleObjStg;->getData()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getExOleObjStg()Lorg/apache/poi/hslf/record/ExOleObjStg;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/ObjectData;->storage:Lorg/apache/poi/hslf/record/ExOleObjStg;

    return-object v0
.end method

.method public setData([B)V
    .locals 1
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/poi/hslf/usermodel/ObjectData;->storage:Lorg/apache/poi/hslf/record/ExOleObjStg;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/ExOleObjStg;->setData([B)V

    .line 60
    return-void
.end method
