.class public final Lorg/apache/poi/hslf/model/MovieShape;
.super Lorg/apache/poi/hslf/model/Picture;
.source "MovieShape.java"


# static fields
.field public static final DEFAULT_MOVIE_THUMBNAIL:I = -0x1

.field public static final MOVIE_AVI:I = 0x2

.field public static final MOVIE_MPEG:I = 0x1


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "movieIdx"    # I
    .param p2, "pictureIdx"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lorg/apache/poi/hslf/model/Picture;-><init>(ILorg/apache/poi/hslf/model/Shape;)V

    .line 47
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/model/MovieShape;->setMovieIndex(I)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hslf/model/MovieShape;->setAutoPlay(Z)V

    .line 49
    return-void
.end method

.method public constructor <init>(IILorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "movieIdx"    # I
    .param p2, "idx"    # I
    .param p3, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 58
    invoke-direct {p0, p2, p3}, Lorg/apache/poi/hslf/model/Picture;-><init>(ILorg/apache/poi/hslf/model/Shape;)V

    .line 59
    invoke-virtual {p0, p1}, Lorg/apache/poi/hslf/model/MovieShape;->setMovieIndex(I)V

    .line 60
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V
    .locals 0
    .param p1, "escherRecord"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "parent"    # Lorg/apache/poi/hslf/model/Shape;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hslf/model/Picture;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hslf/model/Shape;)V

    .line 71
    return-void
.end method


# virtual methods
.method protected createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 10
    .param p1, "idx"    # I
    .param p2, "isChild"    # Z

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lorg/apache/poi/hslf/model/Picture;->createSpContainer(IZ)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/poi/hslf/model/MovieShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 81
    const/16 v8, 0x7f

    const v9, 0x1000100

    invoke-virtual {p0, v8, v9}, Lorg/apache/poi/hslf/model/MovieShape;->setEscherProperty(SI)V

    .line 82
    const/16 v8, 0x1bf

    const v9, 0x10001

    invoke-virtual {p0, v8, v9}, Lorg/apache/poi/hslf/model/MovieShape;->setEscherProperty(SI)V

    .line 84
    new-instance v2, Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-direct {v2}, Lorg/apache/poi/ddf/EscherClientDataRecord;-><init>()V

    .line 85
    .local v2, "cldata":Lorg/apache/poi/ddf/EscherClientDataRecord;
    const/16 v8, 0xf

    invoke-virtual {v2, v8}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setOptions(S)V

    .line 86
    iget-object v8, p0, Lorg/apache/poi/hslf/model/MovieShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v8, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 88
    new-instance v6, Lorg/apache/poi/hslf/record/OEShapeAtom;

    invoke-direct {v6}, Lorg/apache/poi/hslf/record/OEShapeAtom;-><init>()V

    .line 89
    .local v6, "oe":Lorg/apache/poi/hslf/record/OEShapeAtom;
    new-instance v4, Lorg/apache/poi/hslf/record/InteractiveInfo;

    invoke-direct {v4}, Lorg/apache/poi/hslf/record/InteractiveInfo;-><init>()V

    .line 90
    .local v4, "info":Lorg/apache/poi/hslf/record/InteractiveInfo;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lorg/apache/poi/hslf/record/InteractiveInfoAtom;

    move-result-object v5

    .line 91
    .local v5, "infoAtom":Lorg/apache/poi/hslf/record/InteractiveInfoAtom;
    const/4 v8, 0x6

    invoke-virtual {v5, v8}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 92
    const/4 v8, -0x1

    invoke-virtual {v5, v8}, Lorg/apache/poi/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 94
    new-instance v0, Lorg/apache/poi/hslf/record/AnimationInfo;

    invoke-direct {v0}, Lorg/apache/poi/hslf/record/AnimationInfo;-><init>()V

    .line 95
    .local v0, "an":Lorg/apache/poi/hslf/record/AnimationInfo;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    move-result-object v1

    .line 96
    .local v1, "anAtom":Lorg/apache/poi/hslf/record/AnimationInfoAtom;
    const/4 v8, 0x4

    const/4 v9, 0x1

    invoke-virtual {v1, v8, v9}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 99
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 101
    .local v7, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v6, v7}, Lorg/apache/poi/hslf/record/OEShapeAtom;->writeOut(Ljava/io/OutputStream;)V

    .line 102
    invoke-virtual {v0, v7}, Lorg/apache/poi/hslf/record/AnimationInfo;->writeOut(Ljava/io/OutputStream;)V

    .line 103
    invoke-virtual {v4, v7}, Lorg/apache/poi/hslf/record/InteractiveInfo;->writeOut(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-virtual {v2, v8}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setRemainingData([B)V

    .line 109
    iget-object v8, p0, Lorg/apache/poi/hslf/model/MovieShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v8

    .line 104
    :catch_0
    move-exception v3

    .line 105
    .local v3, "e":Ljava/lang/Exception;
    new-instance v8, Lorg/apache/poi/hslf/exceptions/HSLFException;

    invoke-direct {v8, v3}, Lorg/apache/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    throw v8
.end method

.method public getPath()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 155
    sget-object v10, Lorg/apache/poi/hslf/record/RecordTypes;->OEShapeAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v10, v10, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v10}, Lorg/apache/poi/hslf/model/MovieShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/hslf/record/OEShapeAtom;

    .line 157
    .local v6, "oe":Lorg/apache/poi/hslf/record/OEShapeAtom;
    const/4 v2, 0x0

    .line 158
    .local v2, "idx":I
    if-eqz v6, :cond_0

    .line 159
    invoke-virtual {v6}, Lorg/apache/poi/hslf/record/OEShapeAtom;->getOptions()I

    move-result v2

    .line 161
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/MovieShape;->getSheet()Lorg/apache/poi/hslf/model/Sheet;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/poi/hslf/model/Sheet;->getSlideShow()Lorg/apache/poi/hslf/usermodel/SlideShow;

    move-result-object v7

    .line 162
    .local v7, "ppt":Lorg/apache/poi/hslf/usermodel/SlideShow;
    invoke-virtual {v7}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v10

    sget-object v11, Lorg/apache/poi/hslf/record/RecordTypes;->ExObjList:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v11, v11, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Lorg/apache/poi/hslf/record/Document;->findFirstOfType(J)Lorg/apache/poi/hslf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hslf/record/ExObjList;

    .line 163
    .local v3, "lst":Lorg/apache/poi/hslf/record/ExObjList;
    if-nez v3, :cond_2

    .line 177
    :cond_1
    :goto_0
    return-object v9

    .line 165
    :cond_2
    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/ExObjList;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v8

    .line 166
    .local v8, "r":[Lorg/apache/poi/hslf/record/Record;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v10, v8

    if-ge v1, v10, :cond_1

    .line 167
    aget-object v10, v8, v1

    instance-of v10, v10, Lorg/apache/poi/hslf/record/ExMCIMovie;

    if-eqz v10, :cond_3

    .line 168
    aget-object v4, v8, v1

    check-cast v4, Lorg/apache/poi/hslf/record/ExMCIMovie;

    .line 169
    .local v4, "mci":Lorg/apache/poi/hslf/record/ExMCIMovie;
    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/ExMCIMovie;->getExVideo()Lorg/apache/poi/hslf/record/ExVideoContainer;

    move-result-object v0

    .line 170
    .local v0, "exVideo":Lorg/apache/poi/hslf/record/ExVideoContainer;
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getExMediaAtom()Lorg/apache/poi/hslf/record/ExMediaAtom;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/poi/hslf/record/ExMediaAtom;->getObjectId()I

    move-result v5

    .line 171
    .local v5, "objectId":I
    if-ne v5, v2, :cond_3

    .line 172
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/ExVideoContainer;->getPathAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 166
    .end local v0    # "exVideo":Lorg/apache/poi/hslf/record/ExVideoContainer;
    .end local v4    # "mci":Lorg/apache/poi/hslf/record/ExMCIMovie;
    .end local v5    # "objectId":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public isAutoPlay()Z
    .locals 3

    .prologue
    .line 144
    sget-object v1, Lorg/apache/poi/hslf/record/RecordTypes;->AnimationInfo:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v1, v1, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/MovieShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/AnimationInfo;

    .line 145
    .local v0, "an":Lorg/apache/poi/hslf/record/AnimationInfo;
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    move-result v1

    .line 148
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAutoPlay(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 136
    sget-object v1, Lorg/apache/poi/hslf/record/RecordTypes;->AnimationInfo:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v1, v1, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/MovieShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hslf/record/AnimationInfo;

    .line 137
    .local v0, "an":Lorg/apache/poi/hslf/record/AnimationInfo;
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 139
    invoke-virtual {p0}, Lorg/apache/poi/hslf/model/MovieShape;->updateClientData()V

    .line 141
    :cond_0
    return-void
.end method

.method public setMovieIndex(I)V
    .locals 5
    .param p1, "idx"    # I

    .prologue
    const/4 v4, 0x1

    .line 119
    sget-object v3, Lorg/apache/poi/hslf/record/RecordTypes;->OEShapeAtom:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v3, v3, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/model/MovieShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hslf/record/OEShapeAtom;

    .line 121
    .local v2, "oe":Lorg/apache/poi/hslf/record/OEShapeAtom;
    if-eqz v2, :cond_0

    .line 122
    invoke-virtual {v2, p1}, Lorg/apache/poi/hslf/record/OEShapeAtom;->setOptions(I)V

    .line 124
    :cond_0
    sget-object v3, Lorg/apache/poi/hslf/record/RecordTypes;->AnimationInfo:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v3, v3, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-virtual {p0, v3}, Lorg/apache/poi/hslf/model/MovieShape;->getClientDataRecord(I)Lorg/apache/poi/hslf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hslf/record/AnimationInfo;

    .line 125
    .local v1, "an":Lorg/apache/poi/hslf/record/AnimationInfo;
    if-eqz v1, :cond_1

    .line 126
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lorg/apache/poi/hslf/record/AnimationInfoAtom;

    move-result-object v0

    .line 127
    .local v0, "ai":Lorg/apache/poi/hslf/record/AnimationInfoAtom;
    const/high16 v3, 0x7000000

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setDimColor(I)V

    .line 128
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v4}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 129
    const/16 v3, 0x100

    invoke-virtual {v0, v3, v4}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 130
    const/16 v3, 0x400

    invoke-virtual {v0, v3, v4}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 131
    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hslf/record/AnimationInfoAtom;->setOrderID(I)V

    .line 133
    .end local v0    # "ai":Lorg/apache/poi/hslf/record/AnimationInfoAtom;
    :cond_1
    return-void
.end method
