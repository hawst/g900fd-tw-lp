.class public final Lorg/apache/poi/hslf/model/HeadersFooters;
.super Ljava/lang/Object;
.source "HeadersFooters.java"


# instance fields
.field private _container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

.field private _newRecord:Z

.field private _ppt:Lorg/apache/poi/hslf/usermodel/SlideShow;

.field private _ppt2007:Z

.field private _sheet:Lorg/apache/poi/hslf/model/Sheet;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hslf/record/HeadersFootersContainer;Lorg/apache/poi/hslf/model/Sheet;ZZ)V
    .locals 0
    .param p1, "rec"    # Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    .param p2, "sheet"    # Lorg/apache/poi/hslf/model/Sheet;
    .param p3, "newRecord"    # Z
    .param p4, "isPpt2007"    # Z

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .line 48
    iput-boolean p3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    .line 49
    iput-object p2, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    .line 50
    iput-boolean p4, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt2007:Z

    .line 51
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hslf/record/HeadersFootersContainer;Lorg/apache/poi/hslf/usermodel/SlideShow;ZZ)V
    .locals 0
    .param p1, "rec"    # Lorg/apache/poi/hslf/record/HeadersFootersContainer;
    .param p2, "ppt"    # Lorg/apache/poi/hslf/usermodel/SlideShow;
    .param p3, "newRecord"    # Z
    .param p4, "isPpt2007"    # Z

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    .line 41
    iput-boolean p3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    .line 42
    iput-object p2, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt:Lorg/apache/poi/hslf/usermodel/SlideShow;

    .line 43
    iput-boolean p4, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt2007:Z

    .line 44
    return-void
.end method

.method private attach()V
    .locals 8

    .prologue
    .line 227
    iget-object v4, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v4}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getDocumentRecord()Lorg/apache/poi/hslf/record/Document;

    move-result-object v1

    .line 228
    .local v1, "doc":Lorg/apache/poi/hslf/record/Document;
    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/Document;->getChildRecords()[Lorg/apache/poi/hslf/record/Record;

    move-result-object v0

    .line 229
    .local v0, "ch":[Lorg/apache/poi/hslf/record/Record;
    const/4 v3, 0x0

    .line 230
    .local v3, "lst":Lorg/apache/poi/hslf/record/Record;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    .line 236
    :goto_1
    iget-object v4, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1, v4, v3}, Lorg/apache/poi/hslf/record/Document;->addChildAfter(Lorg/apache/poi/hslf/record/Record;Lorg/apache/poi/hslf/record/Record;)V

    .line 237
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    .line 238
    return-void

    .line 231
    :cond_0
    aget-object v4, v0, v2

    invoke-virtual {v4}, Lorg/apache/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    sget-object v6, Lorg/apache/poi/hslf/record/RecordTypes;->List:Lorg/apache/poi/hslf/record/RecordTypes$Type;

    iget v6, v6, Lorg/apache/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 232
    aget-object v3, v0, v2

    .line 233
    goto :goto_1

    .line 230
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getPlaceholderText(ILorg/apache/poi/hslf/record/CString;)Ljava/lang/String;
    .locals 5
    .param p1, "placeholderId"    # I
    .param p2, "cs"    # Lorg/apache/poi/hslf/record/CString;

    .prologue
    .line 253
    const/4 v2, 0x0

    .line 254
    .local v2, "text":Ljava/lang/String;
    iget-boolean v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt2007:Z

    if-eqz v3, :cond_3

    .line 255
    iget-object v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    if-eqz v3, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    .line 256
    .local v0, "master":Lorg/apache/poi/hslf/model/Sheet;
    :goto_0
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/model/Sheet;->getPlaceholder(I)Lorg/apache/poi/hslf/model/TextShape;

    move-result-object v1

    .line 257
    .local v1, "placeholder":Lorg/apache/poi/hslf/model/TextShape;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/TextShape;->getText()Ljava/lang/String;

    move-result-object v2

    .line 260
    :cond_0
    const-string/jumbo v3, "*"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    .line 264
    .end local v0    # "master":Lorg/apache/poi/hslf/model/Sheet;
    .end local v1    # "placeholder":Lorg/apache/poi/hslf/model/TextShape;
    :cond_1
    :goto_1
    return-object v2

    .line 255
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v0, v3, v4

    goto :goto_0

    .line 262
    :cond_3
    if-nez p2, :cond_4

    const/4 v2, 0x0

    :goto_2
    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Lorg/apache/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method private isVisible(II)Z
    .locals 4
    .param p1, "flag"    # I
    .param p2, "placeholderId"    # I

    .prologue
    const/4 v2, 0x0

    .line 242
    iget-boolean v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt2007:Z

    if-eqz v3, :cond_2

    .line 243
    iget-object v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    if-eqz v3, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_sheet:Lorg/apache/poi/hslf/model/Sheet;

    .line 244
    .local v0, "master":Lorg/apache/poi/hslf/model/Sheet;
    :goto_0
    invoke-virtual {v0, p2}, Lorg/apache/poi/hslf/model/Sheet;->getPlaceholder(I)Lorg/apache/poi/hslf/model/TextShape;

    move-result-object v1

    .line 245
    .local v1, "placeholder":Lorg/apache/poi/hslf/model/TextShape;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hslf/model/TextShape;->getText()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 249
    .end local v0    # "master":Lorg/apache/poi/hslf/model/Sheet;
    .end local v1    # "placeholder":Lorg/apache/poi/hslf/model/TextShape;
    .local v2, "visible":Z
    :cond_0
    :goto_1
    return v2

    .line 243
    .end local v2    # "visible":Z
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_ppt:Lorg/apache/poi/hslf/usermodel/SlideShow;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/usermodel/SlideShow;->getSlidesMasters()[Lorg/apache/poi/hslf/model/SlideMaster;

    move-result-object v3

    aget-object v0, v3, v2

    goto :goto_0

    .line 247
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v3}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v3

    invoke-virtual {v3, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    .restart local v2    # "visible":Z
    goto :goto_1
.end method


# virtual methods
.method public getDateTimeFormat()I
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->getFormatId()I

    move-result v0

    return v0
.end method

.method public getDateTimeText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 110
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    :goto_0
    const/4 v1, 0x7

    invoke-direct {p0, v1, v0}, Lorg/apache/poi/hslf/model/HeadersFooters;->getPlaceholderText(ILorg/apache/poi/hslf/record/CString;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 109
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getUserDateAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    goto :goto_0
.end method

.method public getFooterText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 85
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    :goto_0
    const/16 v1, 0x9

    invoke-direct {p0, v1, v0}, Lorg/apache/poi/hslf/model/HeadersFooters;->getPlaceholderText(ILorg/apache/poi/hslf/record/CString;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 84
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getFooterAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 60
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    :goto_0
    const/16 v1, 0xa

    invoke-direct {p0, v1, v0}, Lorg/apache/poi/hslf/model/HeadersFooters;->getPlaceholderText(ILorg/apache/poi/hslf/record/CString;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 59
    .end local v0    # "cs":Lorg/apache/poi/hslf/record/CString;
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeaderAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    goto :goto_0
.end method

.method public isDateTimeVisible()Z
    .locals 2

    .prologue
    .line 163
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hslf/model/HeadersFooters;->isVisible(II)Z

    move-result v0

    return v0
.end method

.method public isFooterVisible()Z
    .locals 2

    .prologue
    .line 133
    const/16 v0, 0x20

    const/16 v1, 0x9

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hslf/model/HeadersFooters;->isVisible(II)Z

    move-result v0

    return v0
.end method

.method public isHeaderVisible()Z
    .locals 2

    .prologue
    .line 148
    const/16 v0, 0x10

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hslf/model/HeadersFooters;->isVisible(II)Z

    move-result v0

    return v0
.end method

.method public isSlideNumberVisible()Z
    .locals 1

    .prologue
    const/16 v0, 0x8

    .line 193
    invoke-direct {p0, v0, v0}, Lorg/apache/poi/hslf/model/HeadersFooters;->isVisible(II)Z

    move-result v0

    return v0
.end method

.method public isUserDateVisible()Z
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x4

    const/4 v1, 0x7

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hslf/model/HeadersFooters;->isVisible(II)Z

    move-result v0

    return v0
.end method

.method public setDateTimeFormat(I)V
    .locals 1
    .param p1, "formatId"    # I

    .prologue
    .line 219
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 220
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->setFormatId(I)V

    .line 221
    return-void
.end method

.method public setDateTimeText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 119
    iget-boolean v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 121
    :cond_0
    invoke-virtual {p0, v2}, Lorg/apache/poi/hslf/model/HeadersFooters;->setUserDateVisible(Z)V

    .line 122
    invoke-virtual {p0, v2}, Lorg/apache/poi/hslf/model/HeadersFooters;->setDateTimeVisible(Z)V

    .line 123
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getUserDateAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    .line 124
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    if-nez v0, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->addUserDateAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    .line 126
    :cond_1
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public setDateTimeVisible(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 170
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 171
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->setFlag(IZ)V

    .line 172
    return-void
.end method

.method public setFooterVisible(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 140
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 141
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->setFlag(IZ)V

    .line 142
    return-void
.end method

.method public setFootersText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 94
    iget-boolean v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 96
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/HeadersFooters;->setFooterVisible(Z)V

    .line 97
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getFooterAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    .line 98
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    if-nez v0, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->addFooterAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    .line 100
    :cond_1
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public setHeaderText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-boolean v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 71
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hslf/model/HeadersFooters;->setHeaderVisible(Z)V

    .line 72
    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeaderAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    .line 73
    .local v0, "cs":Lorg/apache/poi/hslf/record/CString;
    if-nez v0, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v1}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->addHeaderAtom()Lorg/apache/poi/hslf/record/CString;

    move-result-object v0

    .line 75
    :cond_1
    invoke-virtual {v0, p1}, Lorg/apache/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public setHeaderVisible(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 155
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 156
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->setFlag(IZ)V

    .line 157
    return-void
.end method

.method public setSlideNumberVisible(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 200
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 201
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->setFlag(IZ)V

    .line 202
    return-void
.end method

.method public setUserDateVisible(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 185
    iget-boolean v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_newRecord:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/hslf/model/HeadersFooters;->attach()V

    .line 186
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hslf/model/HeadersFooters;->_container:Lorg/apache/poi/hslf/record/HeadersFootersContainer;

    invoke-virtual {v0}, Lorg/apache/poi/hslf/record/HeadersFootersContainer;->getHeadersFootersAtom()Lorg/apache/poi/hslf/record/HeadersFootersAtom;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/hslf/record/HeadersFootersAtom;->setFlag(IZ)V

    .line 187
    return-void
.end method
