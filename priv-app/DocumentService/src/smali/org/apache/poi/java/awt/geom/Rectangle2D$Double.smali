.class public Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;
.super Lorg/apache/poi/java/awt/geom/Rectangle2D;
.source "Rectangle2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/java/awt/geom/Rectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# instance fields
.field public height:D

.field public width:D

.field public x:D

.field public y:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 263
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 277
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 278
    invoke-virtual/range {p0 .. p8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->setRect(DDDD)V

    .line 279
    return-void
.end method


# virtual methods
.method public createIntersection(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 1
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 352
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 353
    .local v0, "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->intersect(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 354
    return-object v0
.end method

.method public createUnion(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 1
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 359
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 360
    .local v0, "dest":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->union(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 361
    return-object v0
.end method

.method public getBounds2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 10

    .prologue
    .line 347
    new-instance v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    iget-wide v4, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    iget-wide v6, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    iget-wide v8, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    invoke-direct/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    return-object v1
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 298
    iget-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    return-wide v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 293
    iget-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 288
    iget-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 303
    iget-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public outcode(DD)I
    .locals 9
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 324
    const/4 v0, 0x0

    .line 326
    .local v0, "code":I
    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    cmpg-double v1, v2, v6

    if-gtz v1, :cond_2

    .line 327
    or-int/lit8 v0, v0, 0x5

    .line 334
    :cond_0
    :goto_0
    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    cmpg-double v1, v2, v6

    if-gtz v1, :cond_4

    .line 335
    or-int/lit8 v0, v0, 0xa

    .line 342
    :cond_1
    :goto_1
    return v0

    .line 328
    :cond_2
    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    cmpg-double v1, p1, v2

    if-gez v1, :cond_3

    .line 329
    or-int/lit8 v0, v0, 0x1

    .line 330
    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    iget-wide v4, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    add-double/2addr v2, v4

    cmpl-double v1, p1, v2

    if-lez v1, :cond_0

    .line 331
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 336
    :cond_4
    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    cmpg-double v1, p3, v2

    if-gez v1, :cond_5

    .line 337
    or-int/lit8 v0, v0, 0x2

    .line 338
    goto :goto_1

    :cond_5
    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    iget-wide v4, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    add-double/2addr v2, v4

    cmpl-double v1, p3, v2

    if-lez v1, :cond_1

    .line 339
    or-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public setRect(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 308
    iput-wide p1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    .line 309
    iput-wide p3, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    .line 310
    iput-wide p5, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    .line 311
    iput-wide p7, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    .line 312
    return-void
.end method

.method public setRect(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 316
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    .line 317
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    .line 318
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    .line 319
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    .line 320
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 370
    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->width:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;->height:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 369
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
