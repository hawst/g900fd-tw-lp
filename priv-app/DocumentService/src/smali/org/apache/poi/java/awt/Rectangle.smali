.class public Lorg/apache/poi/java/awt/Rectangle;
.super Lorg/apache/poi/java/awt/geom/Rectangle2D;
.source "Rectangle.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/apache/poi/java/awt/Shape;


# static fields
.field private static final serialVersionUID:J = -0x3c4f95fae535958cL


# instance fields
.field public height:I

.field public width:I

.field public x:I

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 71
    invoke-virtual {p0, v0, v0, v0, v0}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 72
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v0, 0x0

    .line 100
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 101
    invoke-virtual {p0, v0, v0, p1, p2}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 102
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 87
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 88
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 89
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/java/awt/Rectangle;)V
    .locals 4
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 112
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 113
    iget v0, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v1, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v2, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v3, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 114
    return-void
.end method


# virtual methods
.method public add(II)V
    .locals 6
    .param p1, "px"    # I
    .param p2, "py"    # I

    .prologue
    .line 346
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    invoke-static {v4, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 347
    .local v0, "x1":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v5, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 348
    .local v1, "x2":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-static {v4, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 349
    .local v2, "y1":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    invoke-static {v4, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 350
    .local v3, "y2":I
    sub-int v4, v1, v0

    sub-int v5, v3, v2

    invoke-virtual {p0, v0, v2, v4, v5}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 351
    return-void
.end method

.method public add(Lorg/apache/poi/java/awt/Rectangle;)V
    .locals 7
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 361
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 362
    .local v0, "x1":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v5, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 363
    .local v1, "x2":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 364
    .local v2, "y1":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v6, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 365
    .local v3, "y2":I
    sub-int v4, v1, v0

    sub-int v5, v3, v2

    invoke-virtual {p0, v0, v2, v4, v5}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 366
    return-void
.end method

.method public contains(II)Z
    .locals 2
    .param p1, "px"    # I
    .param p2, "py"    # I

    .prologue
    const/4 v0, 0x0

    .line 380
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Rectangle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 388
    :cond_0
    :goto_0
    return v0

    .line 383
    :cond_1
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    if-lt p2, v1, :cond_0

    .line 386
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    sub-int/2addr p1, v1

    .line 387
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    sub-int/2addr p2, v1

    .line 388
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    if-ge p1, v1, :cond_0

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    if-ge p2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public contains(IIII)Z
    .locals 2
    .param p1, "rx"    # I
    .param p2, "ry"    # I
    .param p3, "rw"    # I
    .param p4, "rh"    # I

    .prologue
    .line 407
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/java/awt/Rectangle;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int v0, p1, p3

    add-int/lit8 v0, v0, -0x1

    add-int v1, p2, p4

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/java/awt/Rectangle;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public contains(Lorg/apache/poi/java/awt/Rectangle;)Z
    .locals 4
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 420
    iget v0, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v1, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v2, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v3, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Rectangle;->contains(IIII)Z

    move-result v0

    return v0
.end method

.method public createIntersection(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 452
    instance-of v1, p1, Lorg/apache/poi/java/awt/Rectangle;

    if-eqz v1, :cond_0

    .line 453
    check-cast p1, Lorg/apache/poi/java/awt/Rectangle;

    .end local p1    # "r":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Rectangle;->intersection(Lorg/apache/poi/java/awt/Rectangle;)Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 457
    :goto_0
    return-object v0

    .line 455
    .restart local p1    # "r":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 456
    .local v0, "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->intersect(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    goto :goto_0
.end method

.method public createUnion(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 545
    instance-of v1, p1, Lorg/apache/poi/java/awt/Rectangle;

    if-eqz v1, :cond_0

    .line 546
    check-cast p1, Lorg/apache/poi/java/awt/Rectangle;

    .end local p1    # "r":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Rectangle;->union(Lorg/apache/poi/java/awt/Rectangle;)Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 550
    :goto_0
    return-object v0

    .line 548
    .restart local p1    # "r":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 549
    .local v0, "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-static {p0, p1, v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->union(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 577
    if-ne p1, p0, :cond_1

    .line 584
    :cond_0
    :goto_0
    return v1

    .line 580
    :cond_1
    instance-of v3, p1, Lorg/apache/poi/java/awt/Rectangle;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 581
    check-cast v0, Lorg/apache/poi/java/awt/Rectangle;

    .line 582
    .local v0, "r":Lorg/apache/poi/java/awt/Rectangle;
    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "r":Lorg/apache/poi/java/awt/Rectangle;
    :cond_3
    move v1, v2

    .line 584
    goto :goto_0
.end method

.method public getBounds()Lorg/apache/poi/java/awt/Rectangle;
    .locals 5

    .prologue
    .line 257
    new-instance v0, Lorg/apache/poi/java/awt/Rectangle;

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v2, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v3, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    return-object v0
.end method

.method public getBounds2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Rectangle;->getBounds()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getSize()Lorg/apache/poi/java/awt/Dimension;
    .locals 3

    .prologue
    .line 181
    new-instance v0, Lorg/apache/poi/java/awt/Dimension;

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v2, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    return-object v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 127
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 138
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public grow(II)V
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 315
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 316
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    sub-int/2addr v0, p2

    iput v0, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 317
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int v1, p1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 318
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int v1, p2, p2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 319
    return-void
.end method

.method public inside(II)Z
    .locals 1
    .param p1, "px"    # I
    .param p2, "py"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 437
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/java/awt/Rectangle;->contains(II)Z

    move-result v0

    return v0
.end method

.method public intersection(Lorg/apache/poi/java/awt/Rectangle;)Lorg/apache/poi/java/awt/Rectangle;
    .locals 7
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 470
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 471
    .local v0, "x1":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 472
    .local v2, "y1":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v5, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v6, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 473
    .local v1, "x2":I
    iget v4, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v5, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v4, v5

    iget v5, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v6, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 474
    .local v3, "y2":I
    new-instance v4, Lorg/apache/poi/java/awt/Rectangle;

    sub-int v5, v1, v0

    sub-int v6, v3, v2

    invoke-direct {v4, v0, v2, v5, v6}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    return-object v4
.end method

.method public intersects(Lorg/apache/poi/java/awt/Rectangle;)Z
    .locals 1
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 486
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Rectangle;->intersection(Lorg/apache/poi/java/awt/Rectangle;)Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Rectangle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public outcode(DD)I
    .locals 5
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    .line 514
    const/4 v0, 0x0

    .line 516
    .local v0, "code":I
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    if-gtz v1, :cond_2

    .line 517
    or-int/lit8 v0, v0, 0x5

    .line 524
    :cond_0
    :goto_0
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    if-gtz v1, :cond_4

    .line 525
    or-int/lit8 v0, v0, 0xa

    .line 532
    :cond_1
    :goto_1
    return v0

    .line 518
    :cond_2
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    int-to-double v2, v1

    cmpg-double v1, p1, v2

    if-gez v1, :cond_3

    .line 519
    or-int/lit8 v0, v0, 0x1

    .line 520
    goto :goto_0

    :cond_3
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v2, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    add-int/2addr v1, v2

    int-to-double v2, v1

    cmpl-double v1, p1, v2

    if-lez v1, :cond_0

    .line 521
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 526
    :cond_4
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    int-to-double v2, v1

    cmpg-double v1, p3, v2

    if-gez v1, :cond_5

    .line 527
    or-int/lit8 v0, v0, 0x2

    .line 528
    goto :goto_1

    :cond_5
    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v2, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    add-int/2addr v1, v2

    int-to-double v2, v1

    cmpl-double v1, p3, v2

    if-lez v1, :cond_1

    .line 529
    or-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public setBounds(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 286
    iput p1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 287
    iput p2, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 288
    iput p4, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 289
    iput p3, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 290
    return-void
.end method

.method public setBounds(Lorg/apache/poi/java/awt/Rectangle;)V
    .locals 4
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 300
    iget v0, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iget v1, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iget v2, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iget v3, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 301
    return-void
.end method

.method public setLocation(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 218
    iput p1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 219
    iput p2, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 220
    return-void
.end method

.method public setRect(DDDD)V
    .locals 7
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 241
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 242
    .local v0, "x1":I
    invoke-static {p3, p4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 243
    .local v2, "y1":I
    add-double v4, p1, p5

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 244
    .local v1, "x2":I
    add-double v4, p3, p7

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 245
    .local v3, "y2":I
    sub-int v4, v1, v0

    sub-int v5, v3, v2

    invoke-virtual {p0, v0, v2, v4, v5}, Lorg/apache/poi/java/awt/Rectangle;->setBounds(IIII)V

    .line 246
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 193
    iput p1, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 194
    iput p2, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 195
    return-void
.end method

.method public setSize(Lorg/apache/poi/java/awt/Dimension;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/poi/java/awt/Dimension;

    .prologue
    .line 204
    iget v0, p1, Lorg/apache/poi/java/awt/Dimension;->width:I

    iget v1, p1, Lorg/apache/poi/java/awt/Dimension;->height:I

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/java/awt/Rectangle;->setSize(II)V

    .line 205
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 599
    const-string/jumbo v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 598
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public translate(II)V
    .locals 1
    .param p1, "mx"    # I
    .param p2, "my"    # I

    .prologue
    .line 331
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 332
    iget v0, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 333
    return-void
.end method

.method public union(Lorg/apache/poi/java/awt/Rectangle;)Lorg/apache/poi/java/awt/Rectangle;
    .locals 1
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 561
    new-instance v0, Lorg/apache/poi/java/awt/Rectangle;

    invoke-direct {v0, p0}, Lorg/apache/poi/java/awt/Rectangle;-><init>(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 562
    .local v0, "dst":Lorg/apache/poi/java/awt/Rectangle;
    invoke-virtual {v0, p1}, Lorg/apache/poi/java/awt/Rectangle;->add(Lorg/apache/poi/java/awt/Rectangle;)V

    .line 563
    return-object v0
.end method
