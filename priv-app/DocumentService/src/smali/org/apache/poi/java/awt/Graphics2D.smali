.class public abstract Lorg/apache/poi/java/awt/Graphics2D;
.super Lorg/apache/poi/java/awt/Graphics;
.source "Graphics2D.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/poi/java/awt/Graphics;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public abstract addRenderingHints(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation
.end method

.method public abstract clip(Lorg/apache/poi/java/awt/Shape;)V
.end method

.method public abstract draw(Lorg/apache/poi/java/awt/Shape;)V
.end method

.method public abstract drawString(Ljava/lang/String;FF)V
.end method

.method public abstract drawString(Ljava/lang/String;II)V
.end method

.method public abstract drawString(Ljava/text/AttributedCharacterIterator;FF)V
.end method

.method public abstract drawString(Ljava/text/AttributedCharacterIterator;II)V
.end method

.method public abstract fill(Lorg/apache/poi/java/awt/Shape;)V
.end method

.method public abstract getBackground()Lorg/apache/poi/java/awt/Color;
.end method

.method public abstract hit(Lorg/apache/poi/java/awt/Rectangle;Lorg/apache/poi/java/awt/Shape;Z)Z
.end method

.method public abstract rotate(D)V
.end method

.method public abstract rotate(DDD)V
.end method

.method public abstract scale(DD)V
.end method

.method public abstract setBackground(Lorg/apache/poi/java/awt/Color;)V
.end method

.method public abstract shear(DD)V
.end method

.method public abstract translate(II)V
.end method
