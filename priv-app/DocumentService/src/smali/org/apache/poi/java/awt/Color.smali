.class public Lorg/apache/poi/java/awt/Color;
.super Ljava/lang/Object;
.source "Color.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final BLACK:Lorg/apache/poi/java/awt/Color;

.field public static final BLUE:Lorg/apache/poi/java/awt/Color;

.field public static final CYAN:Lorg/apache/poi/java/awt/Color;

.field public static final DARK_GRAY:Lorg/apache/poi/java/awt/Color;

.field public static final GRAY:Lorg/apache/poi/java/awt/Color;

.field public static final GREEN:Lorg/apache/poi/java/awt/Color;

.field public static final LIGHT_GRAY:Lorg/apache/poi/java/awt/Color;

.field public static final MAGENTA:Lorg/apache/poi/java/awt/Color;

.field private static final MIN_SCALABLE:I = 0x3

.field public static final ORANGE:Lorg/apache/poi/java/awt/Color;

.field public static final PINK:Lorg/apache/poi/java/awt/Color;

.field public static final RED:Lorg/apache/poi/java/awt/Color;

.field private static final SCALE_FACTOR:D = 0.7

.field public static final WHITE:Lorg/apache/poi/java/awt/Color;

.field public static final YELLOW:Lorg/apache/poi/java/awt/Color;

.field public static final black:Lorg/apache/poi/java/awt/Color;

.field public static final blue:Lorg/apache/poi/java/awt/Color;

.field public static final cyan:Lorg/apache/poi/java/awt/Color;

.field public static final darkGray:Lorg/apache/poi/java/awt/Color;

.field public static final gray:Lorg/apache/poi/java/awt/Color;

.field public static final green:Lorg/apache/poi/java/awt/Color;

.field public static final lightGray:Lorg/apache/poi/java/awt/Color;

.field public static final magenta:Lorg/apache/poi/java/awt/Color;

.field public static final orange:Lorg/apache/poi/java/awt/Color;

.field public static final pink:Lorg/apache/poi/java/awt/Color;

.field public static final red:Lorg/apache/poi/java/awt/Color;

.field private static final serialVersionUID:J = 0x1a51783108f3375L

.field public static final white:Lorg/apache/poi/java/awt/Color;

.field public static final yellow:Lorg/apache/poi/java/awt/Color;


# instance fields
.field private falpha:F

.field private frgbvalue:[F

.field private fvalue:[F

.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xc0

    const/16 v2, 0x80

    const/16 v1, 0x40

    const/4 v4, 0x0

    const/16 v3, 0xff

    .line 50
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v3, v3, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->white:Lorg/apache/poi/java/awt/Color;

    .line 55
    sget-object v0, Lorg/apache/poi/java/awt/Color;->white:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    .line 60
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v5, v5, v5}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->lightGray:Lorg/apache/poi/java/awt/Color;

    .line 65
    sget-object v0, Lorg/apache/poi/java/awt/Color;->lightGray:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->LIGHT_GRAY:Lorg/apache/poi/java/awt/Color;

    .line 70
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v2, v2, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->gray:Lorg/apache/poi/java/awt/Color;

    .line 75
    sget-object v0, Lorg/apache/poi/java/awt/Color;->gray:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->GRAY:Lorg/apache/poi/java/awt/Color;

    .line 80
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v1, v1, v1}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->darkGray:Lorg/apache/poi/java/awt/Color;

    .line 85
    sget-object v0, Lorg/apache/poi/java/awt/Color;->darkGray:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->DARK_GRAY:Lorg/apache/poi/java/awt/Color;

    .line 90
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v4, v4, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    .line 95
    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->BLACK:Lorg/apache/poi/java/awt/Color;

    .line 100
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v3, v4, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->red:Lorg/apache/poi/java/awt/Color;

    .line 105
    sget-object v0, Lorg/apache/poi/java/awt/Color;->red:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->RED:Lorg/apache/poi/java/awt/Color;

    .line 110
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/16 v1, 0xaf

    const/16 v2, 0xaf

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->pink:Lorg/apache/poi/java/awt/Color;

    .line 115
    sget-object v0, Lorg/apache/poi/java/awt/Color;->pink:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->PINK:Lorg/apache/poi/java/awt/Color;

    .line 120
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/16 v1, 0xc8

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->orange:Lorg/apache/poi/java/awt/Color;

    .line 125
    sget-object v0, Lorg/apache/poi/java/awt/Color;->orange:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->ORANGE:Lorg/apache/poi/java/awt/Color;

    .line 130
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v3, v3, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->yellow:Lorg/apache/poi/java/awt/Color;

    .line 135
    sget-object v0, Lorg/apache/poi/java/awt/Color;->yellow:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->YELLOW:Lorg/apache/poi/java/awt/Color;

    .line 140
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v4, v3, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->green:Lorg/apache/poi/java/awt/Color;

    .line 145
    sget-object v0, Lorg/apache/poi/java/awt/Color;->green:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->GREEN:Lorg/apache/poi/java/awt/Color;

    .line 150
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v3, v4, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->magenta:Lorg/apache/poi/java/awt/Color;

    .line 155
    sget-object v0, Lorg/apache/poi/java/awt/Color;->magenta:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->MAGENTA:Lorg/apache/poi/java/awt/Color;

    .line 160
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v4, v3, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->cyan:Lorg/apache/poi/java/awt/Color;

    .line 165
    sget-object v0, Lorg/apache/poi/java/awt/Color;->cyan:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->CYAN:Lorg/apache/poi/java/awt/Color;

    .line 170
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v0, v4, v4, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lorg/apache/poi/java/awt/Color;->blue:Lorg/apache/poi/java/awt/Color;

    .line 175
    sget-object v0, Lorg/apache/poi/java/awt/Color;->blue:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lorg/apache/poi/java/awt/Color;->BLUE:Lorg/apache/poi/java/awt/Color;

    .line 215
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 1
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F

    .prologue
    .line 331
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/poi/java/awt/Color;-><init>(FFFF)V

    .line 332
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 8
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 310
    mul-float v0, p1, v4

    float-to-double v0, v0

    add-double/2addr v0, v6

    double-to-int v0, v0

    mul-float v1, p2, v4

    float-to-double v2, v1

    add-double/2addr v2, v6

    double-to-int v1, v2

    mul-float v2, p3, v4

    float-to-double v2, v2

    add-double/2addr v2, v6

    double-to-int v2, v2

    mul-float v3, p4, v4

    float-to-double v4, v3

    add-double/2addr v4, v6

    double-to-int v3, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    .line 311
    iput p4, p0, Lorg/apache/poi/java/awt/Color;->falpha:F

    .line 312
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    .line 313
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 314
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 315
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 316
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    iput-object v0, p0, Lorg/apache/poi/java/awt/Color;->frgbvalue:[F

    .line 317
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "rgb"    # I

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    const/high16 v0, -0x1000000

    or-int/2addr v0, p1

    iput v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    .line 294
    return-void
.end method

.method public constructor <init>(III)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    and-int/lit16 v0, p1, 0xff

    if-ne v0, p1, :cond_0

    and-int/lit16 v0, p2, 0xff

    if-ne v0, p2, :cond_0

    and-int/lit16 v0, p3, 0xff

    if-eq v0, p3, :cond_1

    .line 278
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    .line 281
    :cond_1
    shl-int/lit8 v0, p2, 0x8

    or-int/2addr v0, p3

    shl-int/lit8 v1, p1, 0x10

    or-int/2addr v0, v1

    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    .line 282
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 2
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "a"    # I

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    and-int/lit16 v0, p1, 0xff

    if-ne v0, p1, :cond_0

    and-int/lit16 v0, p2, 0xff

    if-ne v0, p2, :cond_0

    and-int/lit16 v0, p3, 0xff

    if-ne v0, p3, :cond_0

    and-int/lit16 v0, p4, 0xff

    if-eq v0, p4, :cond_1

    .line 258
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    .line 260
    :cond_1
    shl-int/lit8 v0, p2, 0x8

    or-int/2addr v0, p3

    shl-int/lit8 v1, p1, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, p4, 0x18

    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    .line 261
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "rgba"    # I
    .param p2, "hasAlpha"    # Z

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    if-nez p2, :cond_0

    .line 235
    const/high16 v0, -0x1000000

    or-int/2addr v0, p1

    iput v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iput p1, p0, Lorg/apache/poi/java/awt/Color;->value:I

    goto :goto_0
.end method

.method public static HSBtoRGB(FFF)I
    .locals 18
    .param p0, "hue"    # F
    .param p1, "saturation"    # F
    .param p2, "brightness"    # F

    .prologue
    .line 766
    const/4 v14, 0x0

    cmpl-float v14, p1, v14

    if-nez v14, :cond_0

    .line 767
    move/from16 v9, p2

    .local v9, "fb":F
    move/from16 v10, p2

    .local v10, "fg":F
    move/from16 v11, p2

    .line 812
    .local v11, "fr":F
    :goto_0
    float-to-double v14, v11

    const-wide v16, 0x406fe00000000000L    # 255.0

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    add-double v14, v14, v16

    double-to-int v13, v14

    .line 813
    .local v13, "r":I
    float-to-double v14, v10

    const-wide v16, 0x406fe00000000000L    # 255.0

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    add-double v14, v14, v16

    double-to-int v12, v14

    .line 814
    .local v12, "g":I
    float-to-double v14, v9

    const-wide v16, 0x406fe00000000000L    # 255.0

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    add-double v14, v14, v16

    double-to-int v8, v14

    .line 816
    .local v8, "b":I
    shl-int/lit8 v14, v13, 0x10

    shl-int/lit8 v15, v12, 0x8

    or-int/2addr v14, v15

    or-int/2addr v14, v8

    const/high16 v15, -0x1000000

    or-int/2addr v14, v15

    return v14

    .line 769
    .end local v8    # "b":I
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    .end local v12    # "g":I
    .end local v13    # "r":I
    :cond_0
    move/from16 v0, p0

    float-to-double v14, v0

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    double-to-float v14, v14

    sub-float v14, p0, v14

    const/high16 v15, 0x40c00000    # 6.0f

    mul-float v3, v14, v15

    .line 770
    .local v3, "H":F
    float-to-double v14, v3

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    double-to-int v4, v14

    .line 771
    .local v4, "I":I
    int-to-float v14, v4

    sub-float v2, v3, v14

    .line 772
    .local v2, "F":F
    const/high16 v14, 0x3f800000    # 1.0f

    sub-float v14, v14, p1

    mul-float v6, p2, v14

    .line 773
    .local v6, "M":F
    const/high16 v14, 0x3f800000    # 1.0f

    mul-float v15, p1, v2

    sub-float/2addr v14, v15

    mul-float v7, p2, v14

    .line 774
    .local v7, "N":F
    const/high16 v14, 0x3f800000    # 1.0f

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v2

    mul-float v15, v15, p1

    sub-float/2addr v14, v15

    mul-float v5, p2, v14

    .line 776
    .local v5, "K":F
    packed-switch v4, :pswitch_data_0

    .line 808
    const/4 v10, 0x0

    .restart local v10    # "fg":F
    move v9, v10

    .restart local v9    # "fb":F
    move v11, v10

    .restart local v11    # "fr":F
    goto :goto_0

    .line 778
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    :pswitch_0
    move/from16 v11, p2

    .line 779
    .restart local v11    # "fr":F
    move v10, v5

    .line 780
    .restart local v10    # "fg":F
    move v9, v6

    .line 781
    .restart local v9    # "fb":F
    goto :goto_0

    .line 783
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    :pswitch_1
    move v11, v7

    .line 784
    .restart local v11    # "fr":F
    move/from16 v10, p2

    .line 785
    .restart local v10    # "fg":F
    move v9, v6

    .line 786
    .restart local v9    # "fb":F
    goto :goto_0

    .line 788
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    :pswitch_2
    move v11, v6

    .line 789
    .restart local v11    # "fr":F
    move/from16 v10, p2

    .line 790
    .restart local v10    # "fg":F
    move v9, v5

    .line 791
    .restart local v9    # "fb":F
    goto :goto_0

    .line 793
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    :pswitch_3
    move v11, v6

    .line 794
    .restart local v11    # "fr":F
    move v10, v7

    .line 795
    .restart local v10    # "fg":F
    move/from16 v9, p2

    .line 796
    .restart local v9    # "fb":F
    goto :goto_0

    .line 798
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    :pswitch_4
    move v11, v5

    .line 799
    .restart local v11    # "fr":F
    move v10, v6

    .line 800
    .restart local v10    # "fg":F
    move/from16 v9, p2

    .line 801
    .restart local v9    # "fb":F
    goto :goto_0

    .line 803
    .end local v9    # "fb":F
    .end local v10    # "fg":F
    .end local v11    # "fr":F
    :pswitch_5
    move/from16 v11, p2

    .line 804
    .restart local v11    # "fr":F
    move v10, v6

    .line 805
    .restart local v10    # "fg":F
    move v9, v7

    .line 806
    .restart local v9    # "fb":F
    goto/16 :goto_0

    .line 776
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static RGBtoHSB(III[F)[F
    .locals 10
    .param p0, "r"    # I
    .param p1, "g"    # I
    .param p2, "b"    # I
    .param p3, "hsbvals"    # [F

    .prologue
    .line 709
    if-nez p3, :cond_0

    .line 710
    const/4 v8, 0x3

    new-array p3, v8, [F

    .line 713
    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-static {p2, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 714
    .local v6, "V":I
    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {p2, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 718
    .local v7, "temp":I
    int-to-float v8, v6

    const/high16 v9, 0x437f0000    # 255.0f

    div-float v0, v8, v9

    .line 720
    .local v0, "B":F
    if-ne v6, v7, :cond_2

    .line 721
    const/4 v5, 0x0

    .local v5, "S":F
    move v4, v5

    .line 743
    .local v4, "H":F
    :cond_1
    :goto_0
    const/4 v8, 0x0

    aput v4, p3, v8

    .line 744
    const/4 v8, 0x1

    aput v5, p3, v8

    .line 745
    const/4 v8, 0x2

    aput v0, p3, v8

    .line 747
    return-object p3

    .line 723
    .end local v4    # "H":F
    .end local v5    # "S":F
    :cond_2
    sub-int v8, v6, v7

    int-to-float v8, v8

    int-to-float v9, v6

    div-float v5, v8, v9

    .line 725
    .restart local v5    # "S":F
    sub-int v8, v6, p0

    int-to-float v8, v8

    sub-int v9, v6, v7

    int-to-float v9, v9

    div-float v3, v8, v9

    .line 726
    .local v3, "Cr":F
    sub-int v8, v6, p1

    int-to-float v8, v8

    sub-int v9, v6, v7

    int-to-float v9, v9

    div-float v2, v8, v9

    .line 727
    .local v2, "Cg":F
    sub-int v8, v6, p2

    int-to-float v8, v8

    sub-int v9, v6, v7

    int-to-float v9, v9

    div-float v1, v8, v9

    .line 729
    .local v1, "Cb":F
    if-ne p0, v6, :cond_3

    .line 730
    sub-float v4, v1, v2

    .line 737
    .restart local v4    # "H":F
    :goto_1
    const/high16 v8, 0x40c00000    # 6.0f

    div-float/2addr v4, v8

    .line 738
    const/4 v8, 0x0

    cmpg-float v8, v4, v8

    if-gez v8, :cond_1

    .line 739
    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v4, v8

    goto :goto_0

    .line 731
    .end local v4    # "H":F
    :cond_3
    if-ne p1, v6, :cond_4

    .line 732
    const/high16 v8, 0x40000000    # 2.0f

    add-float/2addr v8, v3

    sub-float v4, v8, v1

    .line 733
    .restart local v4    # "H":F
    goto :goto_1

    .line 734
    .end local v4    # "H":F
    :cond_4
    const/high16 v8, 0x40800000    # 4.0f

    add-float/2addr v8, v2

    sub-float v4, v8, v3

    .restart local v4    # "H":F
    goto :goto_1
.end method

.method public static decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p0, "nm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 673
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 674
    .local v0, "integer":Ljava/lang/Integer;
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    return-object v1
.end method

.method public static getColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p0, "nm"    # Ljava/lang/String;

    .prologue
    .line 653
    invoke-static {p0}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 655
    .local v0, "integer":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 656
    const/4 v1, 0x0

    .line 659
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    goto :goto_0
.end method

.method public static getColor(Ljava/lang/String;I)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p0, "nm"    # Ljava/lang/String;
    .param p1, "def"    # I

    .prologue
    .line 636
    invoke-static {p0}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 638
    .local v0, "integer":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 639
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v1, p1}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 642
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    goto :goto_0
.end method

.method public static getColor(Ljava/lang/String;Lorg/apache/poi/java/awt/Color;)Lorg/apache/poi/java/awt/Color;
    .locals 2
    .param p0, "nm"    # Ljava/lang/String;
    .param p1, "def"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 615
    invoke-static {p0}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 617
    .local v0, "integer":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 621
    .end local p1    # "def":Lorg/apache/poi/java/awt/Color;
    :goto_0
    return-object p1

    .restart local p1    # "def":Lorg/apache/poi/java/awt/Color;
    :cond_0
    new-instance p1, Lorg/apache/poi/java/awt/Color;

    .end local p1    # "def":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p1, v1}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    goto :goto_0
.end method

.method public static getHSBColor(FFF)Lorg/apache/poi/java/awt/Color;
    .locals 2
    .param p0, "h"    # F
    .param p1, "s"    # F
    .param p2, "b"    # F

    .prologue
    .line 690
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-static {p0, p1, p2}, Lorg/apache/poi/java/awt/Color;->HSBtoRGB(FFF)I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public brighter()Lorg/apache/poi/java/awt/Color;
    .locals 10

    .prologue
    const-wide v8, 0x3fe6666666666666L    # 0.7

    const/16 v3, 0xff

    const/4 v6, 0x3

    .line 389
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v2

    .line 390
    .local v2, "r":I
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v0

    .line 391
    .local v0, "b":I
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v1

    .line 393
    .local v1, "g":I
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 394
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v6, v6, v6}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 418
    :goto_0
    return-object v3

    .line 397
    :cond_0
    if-ge v2, v6, :cond_1

    if-eqz v2, :cond_1

    .line 398
    const/4 v2, 0x3

    .line 404
    :goto_1
    if-ge v0, v6, :cond_3

    if-eqz v0, :cond_3

    .line 405
    const/4 v0, 0x3

    .line 411
    :goto_2
    if-ge v1, v6, :cond_5

    if-eqz v1, :cond_5

    .line 412
    const/4 v1, 0x3

    .line 418
    :goto_3
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v2, v1, v0}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    goto :goto_0

    .line 400
    :cond_1
    int-to-double v4, v2

    div-double/2addr v4, v8

    double-to-int v2, v4

    .line 401
    if-le v2, v3, :cond_2

    move v2, v3

    :cond_2
    goto :goto_1

    .line 407
    :cond_3
    int-to-double v4, v0

    div-double/2addr v4, v8

    double-to-int v0, v4

    .line 408
    if-le v0, v3, :cond_4

    move v0, v3

    :cond_4
    goto :goto_2

    .line 414
    :cond_5
    int-to-double v4, v1

    div-double/2addr v4, v8

    double-to-int v1, v4

    .line 415
    if-le v1, v3, :cond_6

    move v1, v3

    :cond_6
    goto :goto_3
.end method

.method public darker()Lorg/apache/poi/java/awt/Color;
    .locals 8

    .prologue
    const-wide v6, 0x3fe6666666666666L    # 0.7

    .line 378
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    int-to-double v2, v1

    mul-double/2addr v2, v6

    double-to-int v1, v2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    .line 379
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, v6

    double-to-int v3, v4

    .line 378
    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 363
    instance-of v1, p1, Lorg/apache/poi/java/awt/Color;

    if-eqz v1, :cond_0

    .line 364
    check-cast p1, Lorg/apache/poi/java/awt/Color;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v1, p1, Lorg/apache/poi/java/awt/Color;->value:I

    iget v2, p0, Lorg/apache/poi/java/awt/Color;->value:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 366
    :cond_0
    return v0
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 600
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getBlue()I
    .locals 1

    .prologue
    .line 573
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getColorComponents([F)[F
    .locals 2
    .param p1, "components"    # [F

    .prologue
    .line 514
    iget-object v1, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    if-nez v1, :cond_0

    .line 515
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Color;->getRGBColorComponents([F)[F

    move-result-object v1

    .line 526
    :goto_0
    return-object v1

    .line 518
    :cond_0
    if-nez p1, :cond_1

    .line 519
    iget-object v1, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    array-length v1, v1

    new-array p1, v1, [F

    .line 522
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    array-length v1, v1

    if-lt v0, v1, :cond_2

    move-object v1, p1

    .line 526
    goto :goto_0

    .line 523
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    aget v1, v1, v0

    aput v1, p1, v0

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getComponents([F)[F
    .locals 2
    .param p1, "components"    # [F

    .prologue
    .line 486
    iget-object v1, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    if-nez v1, :cond_0

    .line 487
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Color;->getRGBComponents([F)[F

    move-result-object v1

    .line 500
    :goto_0
    return-object v1

    .line 490
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/java/awt/Color;->fvalue:[F

    array-length v0, v1

    .line 492
    .local v0, "nColorComps":I
    if-nez p1, :cond_1

    .line 493
    add-int/lit8 v1, v0, 0x1

    new-array p1, v1, [F

    .line 496
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Color;->getColorComponents([F)[F

    .line 498
    iget v1, p0, Lorg/apache/poi/java/awt/Color;->falpha:F

    aput v1, p1, v0

    move-object v1, p1

    .line 500
    goto :goto_0
.end method

.method public getFAlpha()F
    .locals 1

    .prologue
    .line 582
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->falpha:F

    return v0
.end method

.method public getGreen()I
    .locals 1

    .prologue
    .line 564
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    shr-int/lit8 v0, v0, 0x8

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getRGB()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    return v0
.end method

.method public getRGBColorComponents([F)[F
    .locals 5
    .param p1, "components"    # [F

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, 0x437f0000    # 255.0f

    .line 458
    if-nez p1, :cond_0

    .line 459
    const/4 v0, 0x3

    new-array p1, v0, [F

    .line 462
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->frgbvalue:[F

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->frgbvalue:[F

    aget v0, v0, v4

    aput v0, p1, v4

    .line 464
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->frgbvalue:[F

    aget v0, v0, v3

    aput v0, p1, v3

    .line 465
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->frgbvalue:[F

    aget v0, v0, v2

    aput v0, p1, v2

    .line 472
    :goto_0
    return-object p1

    .line 467
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    aput v0, p1, v4

    .line 468
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    aput v0, p1, v3

    .line 469
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    aput v0, p1, v2

    goto :goto_0
.end method

.method public getRGBComponents([F)[F
    .locals 3
    .param p1, "components"    # [F

    .prologue
    const/4 v2, 0x3

    .line 432
    if-nez p1, :cond_0

    .line 433
    const/4 v0, 0x4

    new-array p1, v0, [F

    .line 436
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/java/awt/Color;->frgbvalue:[F

    if-eqz v0, :cond_1

    .line 437
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->falpha:F

    aput v0, p1, v2

    .line 442
    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/java/awt/Color;->getRGBColorComponents([F)[F

    .line 444
    return-object p1

    .line 439
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    aput v0, p1, v2

    goto :goto_0
.end method

.method public getRed()I
    .locals 1

    .prologue
    .line 545
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    shr-int/lit8 v0, v0, 0x10

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 536
    iget v0, p0, Lorg/apache/poi/java/awt/Color;->value:I

    return v0
.end method

.method public setFAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 591
    iput p1, p0, Lorg/apache/poi/java/awt/Color;->falpha:F

    .line 592
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "[r="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 348
    const-string/jumbo v1, ",g="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 349
    const-string/jumbo v1, ",b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 350
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
