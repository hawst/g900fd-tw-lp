.class public interface abstract Lorg/apache/poi/java/awt/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# virtual methods
.method public abstract contains(DD)Z
.end method

.method public abstract contains(DDDD)Z
.end method

.method public abstract contains(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Z
.end method

.method public abstract getBounds()Lorg/apache/poi/java/awt/Rectangle;
.end method

.method public abstract getBounds2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
.end method

.method public abstract intersects(DDDD)Z
.end method

.method public abstract intersects(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Z
.end method
