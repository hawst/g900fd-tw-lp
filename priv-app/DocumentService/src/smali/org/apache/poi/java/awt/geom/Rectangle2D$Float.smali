.class public Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
.super Lorg/apache/poi/java/awt/geom/Rectangle2D;
.source "Rectangle2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/java/awt/geom/Rectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# instance fields
.field public height:F

.field public width:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 90
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 104
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;-><init>()V

    .line 105
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->setRect(FFFF)V

    .line 106
    return-void
.end method


# virtual methods
.method public createIntersection(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 199
    instance-of v1, p1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    if-eqz v1, :cond_0

    .line 200
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 204
    .local v0, "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :goto_0
    invoke-static {p0, p1, v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->intersect(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 205
    return-object v0

    .line 202
    .end local v0    # "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>()V

    .restart local v0    # "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    goto :goto_0
.end method

.method public createUnion(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 211
    instance-of v1, p1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    if-eqz v1, :cond_0

    .line 212
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 216
    .local v0, "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :goto_0
    invoke-static {p0, p1, v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->union(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 217
    return-object v0

    .line 214
    .end local v0    # "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_0
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>()V

    .restart local v0    # "dst":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    goto :goto_0
.end method

.method public getBounds2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 5

    .prologue
    .line 193
    new-instance v0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iget v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iget v3, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    iget v4, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    return-object v0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 130
    iget v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public outcode(DD)I
    .locals 5
    .param p1, "px"    # D
    .param p3, "py"    # D

    .prologue
    const/4 v4, 0x0

    .line 170
    const/4 v0, 0x0

    .line 172
    .local v0, "code":I
    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_2

    .line 173
    or-int/lit8 v0, v0, 0x5

    .line 180
    :cond_0
    :goto_0
    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_4

    .line 181
    or-int/lit8 v0, v0, 0xa

    .line 188
    :cond_1
    :goto_1
    return v0

    .line 174
    :cond_2
    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    float-to-double v2, v1

    cmpg-double v1, p1, v2

    if-gez v1, :cond_3

    .line 175
    or-int/lit8 v0, v0, 0x1

    .line 176
    goto :goto_0

    :cond_3
    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iget v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    cmpl-double v1, p1, v2

    if-lez v1, :cond_0

    .line 177
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 182
    :cond_4
    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    float-to-double v2, v1

    cmpg-double v1, p3, v2

    if-gez v1, :cond_5

    .line 183
    or-int/lit8 v0, v0, 0x2

    .line 184
    goto :goto_1

    :cond_5
    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iget v2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    cmpl-double v1, p3, v2

    if-lez v1, :cond_1

    .line 185
    or-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public setRect(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 154
    double-to-float v0, p1

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    .line 155
    double-to-float v0, p3

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    .line 156
    double-to-float v0, p5

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    .line 157
    double-to-float v0, p7

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    .line 158
    return-void
.end method

.method public setRect(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 146
    iput p1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    .line 147
    iput p2, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    .line 148
    iput p3, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    .line 149
    iput p4, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    .line 150
    return-void
.end method

.method public setRect(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 162
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    .line 163
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    .line 164
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    .line 165
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    .line 166
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226
    const-string/jumbo v1, "[x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
