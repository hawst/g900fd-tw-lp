.class public abstract Lorg/apache/poi/java/awt/Graphics;
.super Ljava/lang/Object;
.source "Graphics.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method


# virtual methods
.method public abstract clearRect(IIII)V
.end method

.method public abstract clipRect(IIII)V
.end method

.method public abstract copyArea(IIIIII)V
.end method

.method public abstract create()Lorg/apache/poi/java/awt/Graphics;
.end method

.method public create(IIII)Lorg/apache/poi/java/awt/Graphics;
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Graphics;->create()Lorg/apache/poi/java/awt/Graphics;

    move-result-object v0

    .line 75
    .local v0, "res":Lorg/apache/poi/java/awt/Graphics;
    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/java/awt/Graphics;->translate(II)V

    .line 76
    invoke-virtual {v0, v1, v1, p3, p4}, Lorg/apache/poi/java/awt/Graphics;->clipRect(IIII)V

    .line 77
    return-object v0
.end method

.method public abstract dispose()V
.end method

.method public draw3DRect(IIIIZ)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "raised"    # Z

    .prologue
    const/4 v5, 0x1

    .line 103
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Graphics;->getColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 105
    .local v0, "color":Lorg/apache/poi/java/awt/Color;
    if-eqz p5, :cond_0

    .line 106
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->brighter()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 107
    .local v2, "colorUp":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->darker()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 113
    .local v1, "colorDown":Lorg/apache/poi/java/awt/Color;
    :goto_0
    invoke-virtual {p0, v2}, Lorg/apache/poi/java/awt/Graphics;->setColor(Lorg/apache/poi/java/awt/Color;)V

    .line 114
    invoke-virtual {p0, p1, p2, p3, v5}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 115
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {p0, p1, v3, v5, p4}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 117
    invoke-virtual {p0, v1}, Lorg/apache/poi/java/awt/Graphics;->setColor(Lorg/apache/poi/java/awt/Color;)V

    .line 118
    add-int v3, p1, p3

    invoke-virtual {p0, v3, p2, v5, p4}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 119
    add-int/lit8 v3, p1, 0x1

    add-int v4, p2, p4

    invoke-virtual {p0, v3, v4, p3, v5}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 120
    return-void

    .line 109
    .end local v1    # "colorDown":Lorg/apache/poi/java/awt/Color;
    .end local v2    # "colorUp":Lorg/apache/poi/java/awt/Color;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->darker()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 110
    .restart local v2    # "colorUp":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->brighter()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .restart local v1    # "colorDown":Lorg/apache/poi/java/awt/Color;
    goto :goto_0
.end method

.method public abstract drawArc(IIIIII)V
.end method

.method public drawBytes([BIIII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    .line 138
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {p0, v0, p4, p5}, Lorg/apache/poi/java/awt/Graphics;->drawString(Ljava/lang/String;II)V

    .line 139
    return-void
.end method

.method public drawChars([CIIII)V
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    .line 157
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0, p4, p5}, Lorg/apache/poi/java/awt/Graphics;->drawString(Ljava/lang/String;II)V

    .line 158
    return-void
.end method

.method public abstract drawLine(IIII)V
.end method

.method public abstract drawOval(IIII)V
.end method

.method public abstract drawPolygon([I[II)V
.end method

.method public abstract drawPolyline([I[II)V
.end method

.method public drawRect(IIII)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 174
    new-array v0, v3, [I

    .line 175
    aput p1, v0, v4

    aput p1, v0, v5

    add-int v2, p1, p3

    aput v2, v0, v6

    add-int v2, p1, p3

    aput v2, v0, v7

    .line 177
    .local v0, "xpoints":[I
    new-array v1, v3, [I

    .line 178
    aput p2, v1, v4

    add-int v2, p2, p4

    aput v2, v1, v5

    add-int v2, p2, p4

    aput v2, v1, v6

    aput p2, v1, v7

    .line 181
    .local v1, "ypoints":[I
    invoke-virtual {p0, v0, v1, v3}, Lorg/apache/poi/java/awt/Graphics;->drawPolygon([I[II)V

    .line 182
    return-void
.end method

.method public abstract drawRoundRect(IIIIII)V
.end method

.method public abstract drawString(Ljava/lang/String;II)V
.end method

.method public abstract drawString(Ljava/text/AttributedCharacterIterator;II)V
.end method

.method public fill3DRect(IIIIZ)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "raised"    # Z

    .prologue
    const/4 v7, 0x1

    .line 206
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Graphics;->getColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 208
    .local v0, "color":Lorg/apache/poi/java/awt/Color;
    if-eqz p5, :cond_0

    .line 209
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->brighter()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 210
    .local v2, "colorUp":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->darker()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 211
    .local v1, "colorDown":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {p0, v0}, Lorg/apache/poi/java/awt/Graphics;->setColor(Lorg/apache/poi/java/awt/Color;)V

    .line 218
    :goto_0
    add-int/lit8 p3, p3, -0x1

    .line 219
    add-int/lit8 p4, p4, -0x1

    .line 220
    add-int/lit8 v3, p1, 0x1

    add-int/lit8 v4, p2, 0x1

    add-int/lit8 v5, p3, -0x1

    add-int/lit8 v6, p4, -0x1

    invoke-virtual {p0, v3, v4, v5, v6}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 222
    invoke-virtual {p0, v2}, Lorg/apache/poi/java/awt/Graphics;->setColor(Lorg/apache/poi/java/awt/Color;)V

    .line 223
    invoke-virtual {p0, p1, p2, p3, v7}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 224
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {p0, p1, v3, v7, p4}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 226
    invoke-virtual {p0, v1}, Lorg/apache/poi/java/awt/Graphics;->setColor(Lorg/apache/poi/java/awt/Color;)V

    .line 227
    add-int v3, p1, p3

    invoke-virtual {p0, v3, p2, v7, p4}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 228
    add-int/lit8 v3, p1, 0x1

    add-int v4, p2, p4

    invoke-virtual {p0, v3, v4, p3, v7}, Lorg/apache/poi/java/awt/Graphics;->fillRect(IIII)V

    .line 229
    return-void

    .line 213
    .end local v1    # "colorDown":Lorg/apache/poi/java/awt/Color;
    .end local v2    # "colorUp":Lorg/apache/poi/java/awt/Color;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->darker()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 214
    .restart local v2    # "colorUp":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->brighter()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 215
    .restart local v1    # "colorDown":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {p0, v2}, Lorg/apache/poi/java/awt/Graphics;->setColor(Lorg/apache/poi/java/awt/Color;)V

    goto :goto_0
.end method

.method public abstract fillArc(IIIIII)V
.end method

.method public abstract fillOval(IIII)V
.end method

.method public abstract fillPolygon([I[II)V
.end method

.method public abstract fillRect(IIII)V
.end method

.method public abstract fillRoundRect(IIIIII)V
.end method

.method public abstract getClip()Lorg/apache/poi/java/awt/Shape;
.end method

.method public abstract getClipBounds()Lorg/apache/poi/java/awt/Rectangle;
.end method

.method public getClipBounds(Lorg/apache/poi/java/awt/Rectangle;)Lorg/apache/poi/java/awt/Rectangle;
    .locals 3
    .param p1, "r"    # Lorg/apache/poi/java/awt/Rectangle;

    .prologue
    .line 241
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Graphics;->getClip()Lorg/apache/poi/java/awt/Shape;

    move-result-object v1

    .line 243
    .local v1, "clip":Lorg/apache/poi/java/awt/Shape;
    if-eqz v1, :cond_0

    .line 245
    invoke-interface {v1}, Lorg/apache/poi/java/awt/Shape;->getBounds()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    .line 246
    .local v0, "b":Lorg/apache/poi/java/awt/Rectangle;
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->x:I

    iput v2, p1, Lorg/apache/poi/java/awt/Rectangle;->x:I

    .line 247
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->y:I

    iput v2, p1, Lorg/apache/poi/java/awt/Rectangle;->y:I

    .line 248
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->width:I

    iput v2, p1, Lorg/apache/poi/java/awt/Rectangle;->width:I

    .line 249
    iget v2, v0, Lorg/apache/poi/java/awt/Rectangle;->height:I

    iput v2, p1, Lorg/apache/poi/java/awt/Rectangle;->height:I

    .line 252
    .end local v0    # "b":Lorg/apache/poi/java/awt/Rectangle;
    :cond_0
    return-object p1
.end method

.method public getClipRect()Lorg/apache/poi/java/awt/Rectangle;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Graphics;->getClipBounds()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public abstract getColor()Lorg/apache/poi/java/awt/Color;
.end method

.method public hitClip(IIII)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 284
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/Graphics;->getClipBounds()Lorg/apache/poi/java/awt/Rectangle;

    move-result-object v0

    new-instance v1, Lorg/apache/poi/java/awt/Rectangle;

    invoke-direct {v1, p1, p2, p3, p4}, Lorg/apache/poi/java/awt/Rectangle;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lorg/apache/poi/java/awt/Rectangle;->intersects(Lorg/apache/poi/java/awt/Rectangle;)Z

    move-result v0

    return v0
.end method

.method public abstract setClip(IIII)V
.end method

.method public abstract setClip(Lorg/apache/poi/java/awt/Shape;)V
.end method

.method public abstract setColor(Lorg/apache/poi/java/awt/Color;)V
.end method

.method public abstract setPaintMode()V
.end method

.method public abstract setXORMode(Lorg/apache/poi/java/awt/Color;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    const-string/jumbo v0, "Graphics"

    return-object v0
.end method

.method public abstract translate(II)V
.end method
