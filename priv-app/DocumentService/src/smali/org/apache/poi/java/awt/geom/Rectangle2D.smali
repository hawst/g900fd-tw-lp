.class public abstract Lorg/apache/poi/java/awt/geom/Rectangle2D;
.super Lorg/apache/poi/java/awt/geom/RectangularShape;
.source "Rectangle2D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/java/awt/geom/Rectangle2D$Double;,
        Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    }
.end annotation


# static fields
.field public static final OUT_BOTTOM:I = 0x8

.field public static final OUT_LEFT:I = 0x1

.field public static final OUT_RIGHT:I = 0x4

.field public static final OUT_TOP:I = 0x2


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 377
    invoke-direct {p0}, Lorg/apache/poi/java/awt/geom/RectangularShape;-><init>()V

    .line 378
    return-void
.end method

.method public static intersect(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 14
    .param p0, "src1"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .param p1, "src2"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .param p2, "dst"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 509
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 510
    .local v2, "x1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 511
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    .line 512
    .local v10, "x2":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v12

    .line 513
    .local v12, "y2":D
    sub-double v6, v10, v2

    sub-double v8, v12, v4

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->setFrame(DDDD)V

    .line 514
    return-void
.end method

.method public static union(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 14
    .param p0, "src1"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .param p1, "src2"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .param p2, "dst"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 530
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 531
    .local v2, "x1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 532
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    .line 533
    .local v10, "x2":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 534
    .local v12, "y2":D
    sub-double v6, v10, v2

    sub-double v8, v12, v4

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->setFrame(DDDD)V

    .line 535
    return-void
.end method


# virtual methods
.method public add(DD)V
    .locals 17
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 548
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinX()D

    move-result-wide v2

    move-wide/from16 v0, p1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 549
    .local v4, "x1":D
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMinY()D

    move-result-wide v2

    move-wide/from16 v0, p3

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    .line 550
    .local v6, "y1":D
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v2

    move-wide/from16 v0, p1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 551
    .local v12, "x2":D
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v2

    move-wide/from16 v0, p3

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    .line 552
    .local v14, "y2":D
    sub-double v8, v12, v4

    sub-double v10, v14, v6

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v11}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 553
    return-void
.end method

.method public add(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 0
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 562
    invoke-static {p0, p1, p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->union(Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 563
    return-void
.end method

.method public contains(DD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 457
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 458
    const/4 v8, 0x0

    .line 466
    :goto_0
    return v8

    .line 461
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 462
    .local v0, "x1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    .line 463
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v2, v0, v8

    .line 464
    .local v2, "x2":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v4, v8

    .line 466
    .local v6, "y2":D
    cmpg-double v8, v0, p1

    if-gtz v8, :cond_1

    cmpg-double v8, p1, v2

    if-gez v8, :cond_1

    cmpg-double v8, v4, p3

    if-gtz v8, :cond_1

    cmpg-double v8, p3, v6

    if-gez v8, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public contains(DDDD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 483
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p5, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p7, v8

    if-gtz v8, :cond_1

    .line 484
    :cond_0
    const/4 v8, 0x0

    .line 492
    :goto_0
    return v8

    .line 487
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 488
    .local v0, "x1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    .line 489
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v2, v0, v8

    .line 490
    .local v2, "x2":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v4, v8

    .line 492
    .local v6, "y2":D
    cmpg-double v8, v0, p1

    if-gtz v8, :cond_2

    add-double v8, p1, p5

    cmpg-double v8, v8, v2

    if-gtz v8, :cond_2

    cmpg-double v8, v4, p3

    if-gtz v8, :cond_2

    add-double v8, p3, p7

    cmpg-double v8, v8, v6

    if-gtz v8, :cond_2

    const/4 v8, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public abstract createIntersection(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
.end method

.method public abstract createUnion(Lorg/apache/poi/java/awt/geom/Rectangle2D;)Lorg/apache/poi/java/awt/geom/Rectangle2D;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 612
    if-ne p1, p0, :cond_1

    .line 620
    :cond_0
    :goto_0
    return v1

    .line 615
    :cond_1
    instance-of v3, p1, Lorg/apache/poi/java/awt/geom/Rectangle2D;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 616
    check-cast v0, Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .line 617
    .local v0, "r":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    .line 618
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    .line 617
    goto :goto_0

    .end local v0    # "r":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    :cond_3
    move v1, v2

    .line 620
    goto :goto_0
.end method

.method public getBounds2D()Lorg/apache/poi/java/awt/geom/Rectangle2D;
    .locals 1

    .prologue
    .line 452
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/java/awt/geom/Rectangle2D;

    return-object v0
.end method

.method public intersects(DDDD)Z
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 470
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p5, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x0

    cmpg-double v8, p7, v8

    if-gtz v8, :cond_1

    .line 471
    :cond_0
    const/4 v8, 0x0

    .line 479
    :goto_0
    return v8

    .line 474
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    .line 475
    .local v0, "x1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    .line 476
    .local v4, "y1":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v8

    add-double v2, v0, v8

    .line 477
    .local v2, "x2":D
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    add-double v6, v4, v8

    .line 479
    .local v6, "y2":D
    add-double v8, p1, p5

    cmpl-double v8, v8, v0

    if-lez v8, :cond_2

    cmpg-double v8, p1, v2

    if-gez v8, :cond_2

    add-double v8, p3, p7

    cmpl-double v8, v8, v4

    if-lez v8, :cond_2

    cmpg-double v8, p3, v6

    if-gez v8, :cond_2

    const/4 v8, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public intersectsLine(DDDD)Z
    .locals 15
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "x2"    # D
    .param p7, "y2"    # D

    .prologue
    .line 584
    move-wide/from16 v0, p5

    move-wide/from16 v2, p7

    invoke-virtual {p0, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->outcode(DD)I

    move-result v5

    .local v5, "out2":I
    if-nez v5, :cond_3

    .line 585
    const/4 v10, 0x1

    .line 607
    :goto_0
    return v10

    .line 588
    .local v4, "out1":I
    :cond_0
    and-int v10, v4, v5

    if-eqz v10, :cond_1

    .line 589
    const/4 v10, 0x0

    goto :goto_0

    .line 591
    :cond_1
    and-int/lit8 v10, v4, 0x5

    if-eqz v10, :cond_4

    .line 592
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v6

    .line 593
    .local v6, "x":D
    and-int/lit8 v10, v4, 0x4

    if-eqz v10, :cond_2

    .line 594
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v10

    add-double/2addr v6, v10

    .line 596
    :cond_2
    sub-double v10, v6, p1

    sub-double v12, p7, p3

    mul-double/2addr v10, v12

    sub-double v12, p5, p1

    div-double/2addr v10, v12

    add-double p3, p3, v10

    .line 597
    move-wide/from16 p1, v6

    .line 587
    .end local v4    # "out1":I
    .end local v6    # "x":D
    :cond_3
    :goto_1
    invoke-virtual/range {p0 .. p4}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->outcode(DD)I

    move-result v4

    .restart local v4    # "out1":I
    if-nez v4, :cond_0

    .line 607
    const/4 v10, 0x1

    goto :goto_0

    .line 599
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v8

    .line 600
    .local v8, "y":D
    and-int/lit8 v10, v4, 0x8

    if-eqz v10, :cond_5

    .line 601
    invoke-virtual {p0}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v10

    add-double/2addr v8, v10

    .line 603
    :cond_5
    sub-double v10, v8, p3

    sub-double v12, p5, p1

    mul-double/2addr v10, v12

    sub-double v12, p7, p3

    div-double/2addr v10, v12

    add-double p1, p1, v10

    .line 604
    move-wide/from16 p3, v8

    goto :goto_1
.end method

.method public abstract outcode(DD)I
.end method

.method public setFrame(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D
    .param p7, "height"    # D

    .prologue
    .line 448
    invoke-virtual/range {p0 .. p8}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 449
    return-void
.end method

.method public abstract setRect(DDDD)V
.end method

.method public setRect(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V
    .locals 10
    .param p1, "r"    # Lorg/apache/poi/java/awt/geom/Rectangle2D;

    .prologue
    .line 443
    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v2

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v6

    invoke-virtual {p1}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 444
    return-void
.end method
