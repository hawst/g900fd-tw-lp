.class public Lorg/apache/poi/ss/formula/functions/Address;
.super Ljava/lang/Object;
.source "Address.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/functions/Function;


# static fields
.field public static final REF_ABSOLUTE:I = 0x1

.field public static final REF_RELATIVE:I = 0x4

.field public static final REF_ROW_ABSOLUTE_COLUMN_RELATIVE:I = 0x2

.field public static final REF_ROW_RELATIVE_RELATIVE_ABSOLUTE:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public evaluate([Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 12
    .param p1, "args"    # [Lorg/apache/poi/ss/formula/eval/ValueEval;
    .param p2, "srcRowIndex"    # I
    .param p3, "srcColumnIndex"    # I

    .prologue
    .line 36
    array-length v10, p1

    const/4 v11, 0x2

    if-lt v10, v11, :cond_0

    array-length v10, p1

    const/4 v11, 0x5

    if-le v10, v11, :cond_1

    .line 37
    :cond_0
    sget-object v10, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    .line 101
    :goto_0
    return-object v10

    .line 42
    :cond_1
    const/4 v10, 0x0

    :try_start_0
    aget-object v10, p1, v10

    invoke-static {v10, p2, p3}, Lorg/apache/poi/ss/formula/functions/NumericFunction;->singleOperandEvaluate(Lorg/apache/poi/ss/formula/eval/ValueEval;II)D

    move-result-wide v10

    double-to-int v6, v10

    .line 43
    .local v6, "row":I
    const/4 v10, 0x1

    aget-object v10, p1, v10

    invoke-static {v10, p2, p3}, Lorg/apache/poi/ss/formula/functions/NumericFunction;->singleOperandEvaluate(Lorg/apache/poi/ss/formula/eval/ValueEval;II)D

    move-result-wide v10

    double-to-int v0, v10

    .line 46
    .local v0, "col":I
    array-length v10, p1

    const/4 v11, 0x2

    if-le v10, v11, :cond_2

    const/4 v10, 0x2

    aget-object v10, p1, v10

    sget-object v11, Lorg/apache/poi/ss/formula/eval/MissingArgEval;->instance:Lorg/apache/poi/ss/formula/eval/MissingArgEval;

    if-eq v10, v11, :cond_2

    .line 47
    const/4 v10, 0x2

    aget-object v10, p1, v10

    invoke-static {v10, p2, p3}, Lorg/apache/poi/ss/formula/functions/NumericFunction;->singleOperandEvaluate(Lorg/apache/poi/ss/formula/eval/ValueEval;II)D

    move-result-wide v10

    double-to-int v5, v10

    .line 51
    .local v5, "refType":I
    :goto_1
    packed-switch v5, :pswitch_data_0

    .line 69
    new-instance v10, Lorg/apache/poi/ss/formula/eval/EvaluationException;

    sget-object v11, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/eval/EvaluationException;-><init>(Lorg/apache/poi/ss/formula/eval/ErrorEval;)V

    throw v10
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v0    # "col":I
    .end local v5    # "refType":I
    .end local v6    # "row":I
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v10

    goto :goto_0

    .line 49
    .end local v1    # "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    .restart local v0    # "col":I
    .restart local v6    # "row":I
    :cond_2
    const/4 v5, 0x1

    .restart local v5    # "refType":I
    goto :goto_1

    .line 53
    :pswitch_0
    const/4 v3, 0x1

    .line 54
    .local v3, "pAbsRow":Z
    const/4 v2, 0x1

    .line 74
    .local v2, "pAbsCol":Z
    :goto_2
    :try_start_1
    array-length v10, p1

    const/4 v11, 0x3

    if-le v10, v11, :cond_3

    .line 75
    const/4 v10, 0x3

    aget-object v10, p1, v10

    invoke-static {v10, p2, p3}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->getSingleValue(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    .line 83
    :cond_3
    array-length v10, p1

    const/4 v11, 0x5

    if-ne v10, v11, :cond_6

    .line 84
    const/4 v10, 0x4

    aget-object v10, p1, v10

    invoke-static {v10, p2, p3}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->getSingleValue(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v9

    .line 85
    .local v9, "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    sget-object v10, Lorg/apache/poi/ss/formula/eval/MissingArgEval;->instance:Lorg/apache/poi/ss/formula/eval/MissingArgEval;

    if-ne v9, v10, :cond_5

    const/4 v8, 0x0

    .line 90
    .end local v9    # "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .local v8, "sheetName":Ljava/lang/String;
    :goto_3
    new-instance v4, Lorg/apache/poi/ss/util/CellReference;

    add-int/lit8 v10, v6, -0x1

    add-int/lit8 v11, v0, -0x1

    invoke-direct {v4, v10, v11, v3, v2}, Lorg/apache/poi/ss/util/CellReference;-><init>(IIZZ)V

    .line 91
    .local v4, "ref":Lorg/apache/poi/ss/util/CellReference;
    new-instance v7, Ljava/lang/StringBuffer;

    const/16 v10, 0x20

    invoke-direct {v7, v10}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 92
    .local v7, "sb":Ljava/lang/StringBuffer;
    if-eqz v8, :cond_4

    .line 93
    invoke-static {v7, v8}, Lorg/apache/poi/ss/formula/SheetNameFormatter;->appendFormat(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 94
    const/16 v10, 0x21

    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 96
    :cond_4
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 98
    new-instance v10, Lorg/apache/poi/ss/formula/eval/StringEval;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/eval/StringEval;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    .end local v2    # "pAbsCol":Z
    .end local v3    # "pAbsRow":Z
    .end local v4    # "ref":Lorg/apache/poi/ss/util/CellReference;
    .end local v7    # "sb":Ljava/lang/StringBuffer;
    .end local v8    # "sheetName":Ljava/lang/String;
    :pswitch_1
    const/4 v3, 0x1

    .line 58
    .restart local v3    # "pAbsRow":Z
    const/4 v2, 0x0

    .line 59
    .restart local v2    # "pAbsCol":Z
    goto :goto_2

    .line 61
    .end local v2    # "pAbsCol":Z
    .end local v3    # "pAbsRow":Z
    :pswitch_2
    const/4 v3, 0x0

    .line 62
    .restart local v3    # "pAbsRow":Z
    const/4 v2, 0x1

    .line 63
    .restart local v2    # "pAbsCol":Z
    goto :goto_2

    .line 65
    .end local v2    # "pAbsCol":Z
    .end local v3    # "pAbsRow":Z
    :pswitch_3
    const/4 v3, 0x0

    .line 66
    .restart local v3    # "pAbsRow":Z
    const/4 v2, 0x0

    .line 67
    .restart local v2    # "pAbsCol":Z
    goto :goto_2

    .line 85
    .restart local v9    # "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_5
    invoke-static {v9}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->coerceValueToString(Lorg/apache/poi/ss/formula/eval/ValueEval;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_3

    .line 87
    .end local v9    # "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_6
    const/4 v8, 0x0

    .restart local v8    # "sheetName":Ljava/lang/String;
    goto :goto_3

    .line 51
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
