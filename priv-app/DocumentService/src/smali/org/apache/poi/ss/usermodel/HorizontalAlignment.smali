.class public final enum Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
.super Ljava/lang/Enum;
.source "HorizontalAlignment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/HorizontalAlignment;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CENTER:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum CENTER_SELECTION:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum DISTRIBUTED:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum FILL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum GENERAL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum JUSTIFY:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum LEFT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum RIGHT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "GENERAL"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 31
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->GENERAL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 33
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "LEFT"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 39
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->LEFT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 41
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "CENTER"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 44
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->CENTER:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 46
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "RIGHT"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 50
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->RIGHT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 52
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "FILL"

    invoke-direct {v0, v1, v7}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 67
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->FILL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 69
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "JUSTIFY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 74
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->JUSTIFY:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 76
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "CENTER_SELECTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 82
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->CENTER_SELECTION:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 84
    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string/jumbo v1, "DISTRIBUTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    .line 94
    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->DISTRIBUTED:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    sget-object v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->GENERAL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->LEFT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->CENTER:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->RIGHT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->FILL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->JUSTIFY:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->CENTER_SELECTION:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->DISTRIBUTED:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
