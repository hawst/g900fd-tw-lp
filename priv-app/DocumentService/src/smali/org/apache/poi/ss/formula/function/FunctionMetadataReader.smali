.class final Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;
.super Ljava/lang/Object;
.source "FunctionMetadataReader.java"


# static fields
.field private static final DIGIT_ENDING_FUNCTION_NAMES:[Ljava/lang/String;

.field private static final DIGIT_ENDING_FUNCTION_NAMES_SET:Ljava/util/Set;

.field private static final ELLIPSIS:Ljava/lang/String; = "..."

.field private static final EMPTY_BYTE_ARRAY:[B

.field private static final METADATA_FILE_NAME:Ljava/lang/String; = "functionMetadata.txt"

.field private static final SPACE_DELIM_PATTERN:Ljava/util/regex/Pattern;

.field private static final TAB_DELIM_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    const-string/jumbo v0, "\t"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->TAB_DELIM_PATTERN:Ljava/util/regex/Pattern;

    .line 46
    const-string/jumbo v0, " "

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->SPACE_DELIM_PATTERN:Ljava/util/regex/Pattern;

    .line 47
    new-array v0, v2, [B

    sput-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->EMPTY_BYTE_ARRAY:[B

    .line 49
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    .line 53
    const-string/jumbo v1, "LOG10"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "ATAN2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "DAYS360"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "SUMXMY2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "SUMX2MY2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "SUMX2PY2"

    aput-object v2, v0, v1

    .line 49
    sput-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->DIGIT_ENDING_FUNCTION_NAMES:[Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    .line 55
    sget-object v1, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->DIGIT_ENDING_FUNCTION_NAMES:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 54
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->DIGIT_ENDING_FUNCTION_NAMES_SET:Ljava/util/Set;

    .line 55
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createRegistry()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;
    .locals 10

    .prologue
    const/4 v8, 0x1

    .line 58
    const-class v6, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;

    .line 59
    const-string/jumbo v7, "functionMetadata.txt"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 60
    .local v3, "is":Ljava/io/InputStream;
    if-nez v3, :cond_0

    .line 61
    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "resource \'functionMetadata.txt\' not found"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 67
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    const-string/jumbo v7, "UTF-8"

    invoke-direct {v6, v3, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .local v0, "br":Ljava/io/BufferedReader;
    new-instance v2, Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;

    const/16 v6, 0x190

    invoke-direct {v2, v6}, Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;-><init>(I)V

    .line 75
    .local v2, "fdb":Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;
    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "line":Ljava/lang/String;
    if-nez v4, :cond_3

    .line 88
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    if-eqz v3, :cond_2

    .line 94
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 99
    :cond_2
    :goto_1
    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;->build()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    move-result-object v6

    return-object v6

    .line 68
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "fdb":Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;
    .end local v4    # "line":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 79
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "fdb":Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;
    .restart local v4    # "line":Ljava/lang/String;
    :cond_3
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v8, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x23

    if-eq v6, v7, :cond_1

    .line 82
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 83
    .local v5, "trimLine":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v8, :cond_1

    .line 86
    invoke-static {v2, v4}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->processLine(Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 89
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "trimLine":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 90
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 91
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 93
    if-eqz v3, :cond_4

    .line 94
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 98
    :cond_4
    :goto_2
    throw v6

    .line 95
    :catch_2
    move-exception v1

    .line 96
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v7, "FunctionMetadataReader"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "IOException : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 95
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v4    # "line":Ljava/lang/String;
    :catch_3
    move-exception v1

    .line 96
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v6, "FunctionMetadataReader"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "IOException : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static isDash(Ljava/lang/String;)Z
    .locals 3
    .param p0, "codes"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 154
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 155
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 160
    :pswitch_0
    return v0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_0
    .end packed-switch
.end method

.method private static parseInt(Ljava/lang/String;)I
    .locals 4
    .param p0, "valStr"    # Ljava/lang/String;

    .prologue
    .line 205
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Value \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 208
    const-string/jumbo v3, "\' could not be parsed as an integer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 207
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static parseOperandTypeCode(Ljava/lang/String;)B
    .locals 4
    .param p0, "code"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 164
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 165
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Bad operand type code format \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 166
    const-string/jumbo v2, "\' expected single char"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 176
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unexpected operand type code \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :sswitch_0
    const/16 v0, 0x20

    .line 174
    :goto_0
    :sswitch_1
    return v0

    :sswitch_2
    const/16 v0, 0x40

    goto :goto_0

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_2
        0x52 -> :sswitch_1
        0x56 -> :sswitch_0
    .end sparse-switch
.end method

.method private static parseOperandTypeCodes(Ljava/lang/String;)[B
    .locals 6
    .param p0, "codes"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 133
    sget-object v3, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->EMPTY_BYTE_ARRAY:[B

    .line 150
    :cond_0
    :goto_0
    return-object v3

    .line 135
    :cond_1
    invoke-static {p0}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->isDash(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 137
    sget-object v3, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->EMPTY_BYTE_ARRAY:[B

    goto :goto_0

    .line 139
    :cond_2
    sget-object v4, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->SPACE_DELIM_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "array":[Ljava/lang/String;
    array-length v2, v0

    .line 141
    .local v2, "nItems":I
    const-string/jumbo v4, "..."

    add-int/lit8 v5, v2, -0x1

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 144
    add-int/lit8 v2, v2, -0x1

    .line 146
    :cond_3
    new-array v3, v2, [B

    .line 147
    .local v3, "result":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 148
    aget-object v4, v0, v1

    invoke-static {v4}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseOperandTypeCode(Ljava/lang/String;)B

    move-result v4

    aput-byte v4, v3, v1

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static parseReturnTypeCode(Ljava/lang/String;)B
    .locals 1
    .param p0, "code"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 126
    const/4 v0, 0x0

    .line 128
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseOperandTypeCode(Ljava/lang/String;)B

    move-result v0

    goto :goto_0
.end method

.method private static processLine(Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;Ljava/lang/String;)V
    .locals 11
    .param p0, "fdb"    # Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 104
    sget-object v9, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->TAB_DELIM_PATTERN:Ljava/util/regex/Pattern;

    const/4 v10, -0x2

    invoke-virtual {v9, p1, v10}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v8

    .line 105
    .local v8, "parts":[Ljava/lang/String;
    array-length v9, v8

    const/16 v10, 0x8

    if-eq v9, v10, :cond_0

    .line 106
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Bad line format \'"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 107
    const-string/jumbo v10, "\' - expected 8 data fields"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 106
    invoke-direct {v0, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    aget-object v9, v8, v0

    invoke-static {v9}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 110
    .local v1, "functionIndex":I
    aget-object v2, v8, v7

    .line 111
    .local v2, "functionName":Ljava/lang/String;
    const/4 v9, 0x2

    aget-object v9, v8, v9

    invoke-static {v9}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 112
    .local v3, "minParams":I
    const/4 v9, 0x3

    aget-object v9, v8, v9

    invoke-static {v9}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 113
    .local v4, "maxParams":I
    const/4 v9, 0x4

    aget-object v9, v8, v9

    invoke-static {v9}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseReturnTypeCode(Ljava/lang/String;)B

    move-result v5

    .line 114
    .local v5, "returnClassCode":B
    const/4 v9, 0x5

    aget-object v9, v8, v9

    invoke-static {v9}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->parseOperandTypeCodes(Ljava/lang/String;)[B

    move-result-object v6

    .line 116
    .local v6, "parameterClassCodes":[B
    const/4 v9, 0x7

    aget-object v9, v8, v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_1

    .line 118
    .local v7, "hasNote":Z
    :goto_0
    invoke-static {v2}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->validateFunctionName(Ljava/lang/String;)V

    move-object v0, p0

    .line 120
    invoke-virtual/range {v0 .. v7}, Lorg/apache/poi/ss/formula/function/FunctionDataBuilder;->add(ILjava/lang/String;IIB[BZ)V

    .line 122
    return-void

    .end local v7    # "hasNote":Z
    :cond_1
    move v7, v0

    .line 116
    goto :goto_0
.end method

.method private static validateFunctionName(Ljava/lang/String;)V
    .locals 5
    .param p0, "functionName"    # Ljava/lang/String;

    .prologue
    .line 185
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 186
    .local v1, "len":I
    add-int/lit8 v0, v1, -0x1

    .line 187
    .local v0, "ix":I
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_3

    .line 197
    :cond_0
    return-void

    .line 191
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_2

    .line 196
    :goto_0
    sget-object v2, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->DIGIT_ENDING_FUNCTION_NAMES_SET:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 199
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Invalid function name \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 200
    const-string/jumbo v4, "\' (is footnote number incorrectly appended)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 199
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 194
    :cond_2
    add-int/lit8 v0, v0, -0x1

    .line 190
    :cond_3
    if-gez v0, :cond_1

    goto :goto_0
.end method
