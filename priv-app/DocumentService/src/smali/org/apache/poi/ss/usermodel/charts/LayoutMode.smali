.class public final enum Lorg/apache/poi/ss/usermodel/charts/LayoutMode;
.super Ljava/lang/Enum;
.source "LayoutMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/charts/LayoutMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum EDGE:Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

.field public static final enum FACTOR:Lorg/apache/poi/ss/usermodel/charts/LayoutMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    const-string/jumbo v1, "EDGE"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;-><init>(Ljava/lang/String;I)V

    .line 29
    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;->EDGE:Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    .line 30
    new-instance v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    const-string/jumbo v1, "FACTOR"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;-><init>(Ljava/lang/String;I)V

    .line 34
    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;->FACTOR:Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    sget-object v1, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;->EDGE:Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;->FACTOR:Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/charts/LayoutMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/charts/LayoutMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/charts/LayoutMode;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/charts/LayoutMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
