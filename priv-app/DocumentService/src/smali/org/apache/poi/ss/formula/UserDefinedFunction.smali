.class final Lorg/apache/poi/ss/formula/UserDefinedFunction;
.super Ljava/lang/Object;
.source "UserDefinedFunction.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/functions/FreeRefFunction;


# static fields
.field public static final instance:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lorg/apache/poi/ss/formula/UserDefinedFunction;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/UserDefinedFunction;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/UserDefinedFunction;->instance:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public evaluate([Lorg/apache/poi/ss/formula/eval/ValueEval;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 10
    .param p1, "args"    # [Lorg/apache/poi/ss/formula/eval/ValueEval;
    .param p2, "ec"    # Lorg/apache/poi/ss/formula/OperationEvaluationContext;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 42
    array-length v1, p1

    .line 43
    .local v1, "nIncomingArgs":I
    if-ge v1, v9, :cond_0

    .line 44
    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "function name argument missing"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 47
    :cond_0
    aget-object v3, p1, v8

    .line 49
    .local v3, "nameArg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    instance-of v6, v3, Lorg/apache/poi/ss/formula/eval/NameEval;

    if-eqz v6, :cond_1

    .line 50
    check-cast v3, Lorg/apache/poi/ss/formula/eval/NameEval;

    .end local v3    # "nameArg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/eval/NameEval;->getFunctionName()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "functionName":Ljava/lang/String;
    :goto_0
    invoke-virtual {p2, v0}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->findUserDefinedFunction(Ljava/lang/String;)Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    move-result-object v5

    .line 58
    .local v5, "targetFunc":Lorg/apache/poi/ss/formula/functions/FreeRefFunction;
    if-nez v5, :cond_3

    .line 59
    new-instance v6, Lorg/apache/poi/ss/formula/eval/NotImplementedException;

    invoke-direct {v6, v0}, Lorg/apache/poi/ss/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 51
    .end local v0    # "functionName":Ljava/lang/String;
    .end local v5    # "targetFunc":Lorg/apache/poi/ss/formula/functions/FreeRefFunction;
    .restart local v3    # "nameArg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_1
    instance-of v6, v3, Lorg/apache/poi/ss/formula/eval/NameXEval;

    if-eqz v6, :cond_2

    .line 52
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getWorkbook()Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    move-result-object v6

    check-cast v3, Lorg/apache/poi/ss/formula/eval/NameXEval;

    .end local v3    # "nameArg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/eval/NameXEval;->getPtg()Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->resolveNameXText(Lorg/apache/poi/ss/formula/ptg/NameXPtg;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .restart local v0    # "functionName":Ljava/lang/String;
    goto :goto_0

    .line 54
    .end local v0    # "functionName":Ljava/lang/String;
    .restart local v3    # "nameArg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_2
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "First argument should be a NameEval, but got ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 55
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 54
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 61
    .end local v3    # "nameArg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .restart local v0    # "functionName":Ljava/lang/String;
    .restart local v5    # "targetFunc":Lorg/apache/poi/ss/formula/functions/FreeRefFunction;
    :cond_3
    add-int/lit8 v2, v1, -0x1

    .line 62
    .local v2, "nOutGoingArgs":I
    new-array v4, v2, [Lorg/apache/poi/ss/formula/eval/ValueEval;

    .line 63
    .local v4, "outGoingArgs":[Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-static {p1, v9, v4, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    invoke-interface {v5, v4, p2}, Lorg/apache/poi/ss/formula/functions/FreeRefFunction;->evaluate([Lorg/apache/poi/ss/formula/eval/ValueEval;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v6

    return-object v6
.end method
