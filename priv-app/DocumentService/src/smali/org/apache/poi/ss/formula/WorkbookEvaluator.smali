.class public final Lorg/apache/poi/ss/formula/WorkbookEvaluator;
.super Ljava/lang/Object;
.source "WorkbookEvaluator.java"


# static fields
.field private static final LOG:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final EVAL_LOG:Lorg/apache/poi/util/POILogger;

.field private _cache:Lorg/apache/poi/ss/formula/EvaluationCache;

.field private _collaboratingWorkbookEnvironment:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

.field private final _evaluationListener:Lorg/apache/poi/ss/formula/IEvaluationListener;

.field private _ignoreMissingWorkbooks:Z

.field private final _sheetIndexesByName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final _sheetIndexesBySheet:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/poi/ss/formula/EvaluationSheet;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final _stabilityClassifier:Lorg/apache/poi/ss/formula/IStabilityClassifier;

.field private final _udfFinder:Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;

.field private final _workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

.field private _workbookIx:I

.field private dbgEvaluationOutputForNextEval:Z

.field private dbgEvaluationOutputIndent:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->LOG:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method constructor <init>(Lorg/apache/poi/ss/formula/EvaluationWorkbook;Lorg/apache/poi/ss/formula/IEvaluationListener;Lorg/apache/poi/ss/formula/IStabilityClassifier;Lorg/apache/poi/ss/formula/udf/UDFFinder;)V
    .locals 3
    .param p1, "workbook"    # Lorg/apache/poi/ss/formula/EvaluationWorkbook;
    .param p2, "evaluationListener"    # Lorg/apache/poi/ss/formula/IEvaluationListener;
    .param p3, "stabilityClassifier"    # Lorg/apache/poi/ss/formula/IStabilityClassifier;
    .param p4, "udfFinder"    # Lorg/apache/poi/ss/formula/udf/UDFFinder;

    .prologue
    const/4 v2, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-boolean v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_ignoreMissingWorkbooks:Z

    .line 401
    iput-boolean v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputForNextEval:Z

    .line 404
    const-string/jumbo v1, "POI.FormulaEval"

    invoke-static {v1}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/String;)Lorg/apache/poi/util/POILogger;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->EVAL_LOG:Lorg/apache/poi/util/POILogger;

    .line 406
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    .line 105
    iput-object p1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    .line 106
    iput-object p2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_evaluationListener:Lorg/apache/poi/ss/formula/IEvaluationListener;

    .line 107
    new-instance v1, Lorg/apache/poi/ss/formula/EvaluationCache;

    invoke-direct {v1, p2}, Lorg/apache/poi/ss/formula/EvaluationCache;-><init>(Lorg/apache/poi/ss/formula/IEvaluationListener;)V

    iput-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    .line 108
    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesBySheet:Ljava/util/Map;

    .line 109
    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesByName:Ljava/util/Map;

    .line 110
    sget-object v1, Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;->EMPTY:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    iput-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_collaboratingWorkbookEnvironment:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    .line 111
    iput v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbookIx:I

    .line 112
    iput-object p3, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_stabilityClassifier:Lorg/apache/poi/ss/formula/IStabilityClassifier;

    .line 115
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 116
    .local v0, "defaultToolkit":Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;
    :goto_0
    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    .line 117
    invoke-virtual {v0, p4}, Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;->add(Lorg/apache/poi/ss/formula/udf/UDFFinder;)V

    .line 119
    :cond_0
    iput-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_udfFinder:Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;

    .line 120
    return-void

    .line 115
    .end local v0    # "defaultToolkit":Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;
    :cond_1
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getUDFFinder()Lorg/apache/poi/ss/formula/udf/UDFFinder;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;

    move-object v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/poi/ss/formula/EvaluationWorkbook;Lorg/apache/poi/ss/formula/IStabilityClassifier;Lorg/apache/poi/ss/formula/udf/UDFFinder;)V
    .locals 1
    .param p1, "workbook"    # Lorg/apache/poi/ss/formula/EvaluationWorkbook;
    .param p2, "stabilityClassifier"    # Lorg/apache/poi/ss/formula/IStabilityClassifier;
    .param p3, "udfFinder"    # Lorg/apache/poi/ss/formula/udf/UDFFinder;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;-><init>(Lorg/apache/poi/ss/formula/EvaluationWorkbook;Lorg/apache/poi/ss/formula/IEvaluationListener;Lorg/apache/poi/ss/formula/IStabilityClassifier;Lorg/apache/poi/ss/formula/udf/UDFFinder;)V

    .line 102
    return-void
.end method

.method private addExceptionInfo(Lorg/apache/poi/ss/formula/eval/NotImplementedException;III)Lorg/apache/poi/ss/formula/eval/NotImplementedException;
    .locals 8
    .param p1, "inner"    # Lorg/apache/poi/ss/formula/eval/NotImplementedException;
    .param p2, "sheetIndex"    # I
    .param p3, "rowIndex"    # I
    .param p4, "columnIndex"    # I

    .prologue
    .line 362
    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v2, p2}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v1

    .line 363
    .local v1, "sheetName":Ljava/lang/String;
    new-instance v0, Lorg/apache/poi/ss/util/CellReference;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v2, p3

    move v3, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/util/CellReference;-><init>(Ljava/lang/String;IIZZ)V

    .line 364
    .local v0, "cr":Lorg/apache/poi/ss/util/CellReference;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Error evaluating cell "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 365
    .local v7, "msg":Ljava/lang/String;
    new-instance v2, Lorg/apache/poi/ss/formula/eval/NotImplementedException;

    invoke-direct {v2, v7, p1}, Lorg/apache/poi/ss/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/formula/eval/NotImplementedException;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v2

    .line 369
    .end local v0    # "cr":Lorg/apache/poi/ss/util/CellReference;
    .end local v1    # "sheetName":Ljava/lang/String;
    .end local v7    # "msg":Ljava/lang/String;
    .end local p1    # "inner":Lorg/apache/poi/ss/formula/eval/NotImplementedException;
    :goto_0
    return-object p1

    .line 366
    .restart local p1    # "inner":Lorg/apache/poi/ss/formula/eval/NotImplementedException;
    :catch_0
    move-exception v6

    .line 368
    .local v6, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static countTokensToBeSkipped([Lorg/apache/poi/ss/formula/ptg/Ptg;II)I
    .locals 4
    .param p0, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p1, "startIndex"    # I
    .param p2, "distInBytes"    # I

    .prologue
    .line 573
    move v1, p2

    .line 574
    .local v1, "remBytes":I
    move v0, p1

    .line 575
    .local v0, "index":I
    :cond_0
    if-nez v1, :cond_1

    .line 585
    sub-int v2, v0, p1

    return v2

    .line 576
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 577
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/ptg/Ptg;->getSize()I

    move-result v2

    sub-int/2addr v1, v2

    .line 578
    if-gez v1, :cond_2

    .line 579
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Bad skip distance (wrong token size calculation)."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 581
    :cond_2
    array-length v2, p0

    if-lt v0, v2, :cond_0

    .line 582
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Skip distance too far (ran out of formula tokens)."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static dereferenceResult(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 3
    .param p0, "evaluationResult"    # Lorg/apache/poi/ss/formula/eval/ValueEval;
    .param p1, "srcRowNum"    # I
    .param p2, "srcColNum"    # I

    .prologue
    .line 600
    :try_start_0
    invoke-static {p0, p1, p2}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->getSingleValue(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 604
    .local v1, "value":Lorg/apache/poi/ss/formula/eval/ValueEval;
    sget-object v2, Lorg/apache/poi/ss/formula/eval/BlankEval;->instance:Lorg/apache/poi/ss/formula/eval/BlankEval;

    if-ne v1, v2, :cond_0

    .line 606
    sget-object v1, Lorg/apache/poi/ss/formula/eval/NumberEval;->ZERO:Lorg/apache/poi/ss/formula/eval/NumberEval;

    .line 610
    .end local v1    # "value":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_0
    :goto_0
    return-object v1

    .line 601
    :catch_0
    move-exception v0

    .line 602
    .local v0, "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v1

    goto :goto_0
.end method

.method private evaluateAny(Lorg/apache/poi/ss/formula/EvaluationCell;IIILorg/apache/poi/ss/formula/EvaluationTracker;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 27
    .param p1, "srcCell"    # Lorg/apache/poi/ss/formula/EvaluationCell;
    .param p2, "sheetIndex"    # I
    .param p3, "rowIndex"    # I
    .param p4, "columnIndex"    # I
    .param p5, "tracker"    # Lorg/apache/poi/ss/formula/EvaluationTracker;

    .prologue
    .line 270
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_stabilityClassifier:Lorg/apache/poi/ss/formula/IStabilityClassifier;

    if-nez v6, :cond_2

    const/16 v26, 0x1

    .line 272
    .local v26, "shouldCellDependencyBeRecorded":Z
    :goto_0
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getCellType()I

    move-result v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_4

    .line 273
    :cond_0
    invoke-static/range {p1 .. p1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getValueFromNonFormulaCell(Lorg/apache/poi/ss/formula/EvaluationCell;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v11

    .line 274
    .local v11, "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    if-eqz v26, :cond_1

    .line 275
    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbookIx:I

    move-object/from16 v6, p5

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    invoke-virtual/range {v6 .. v11}, Lorg/apache/poi/ss/formula/EvaluationTracker;->acceptPlainValueDependency(IIIILorg/apache/poi/ss/formula/eval/ValueEval;)V

    .line 351
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_1
    :goto_1
    return-object v11

    .line 271
    .end local v26    # "shouldCellDependencyBeRecorded":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_stabilityClassifier:Lorg/apache/poi/ss/formula/IStabilityClassifier;

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-interface {v6, v0, v1, v2}, Lorg/apache/poi/ss/formula/IStabilityClassifier;->isCellFinal(III)Z

    move-result v6

    if-eqz v6, :cond_3

    const/16 v26, 0x0

    goto :goto_0

    :cond_3
    const/16 v26, 0x1

    goto :goto_0

    .line 280
    .restart local v26    # "shouldCellDependencyBeRecorded":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lorg/apache/poi/ss/formula/EvaluationCache;->getOrCreateFormulaCellEntry(Lorg/apache/poi/ss/formula/EvaluationCell;)Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;

    move-result-object v19

    .line 281
    .local v19, "cce":Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;
    if-nez v26, :cond_5

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;->isInputSensitive()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 282
    :cond_5
    move-object/from16 v0, p5

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/EvaluationTracker;->acceptFormulaDependency(Lorg/apache/poi/ss/formula/CellCacheEntry;)V

    .line 284
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_evaluationListener:Lorg/apache/poi/ss/formula/IEvaluationListener;

    move-object/from16 v22, v0

    .line 286
    .local v22, "evalListener":Lorg/apache/poi/ss/formula/IEvaluationListener;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;->getValue()Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v6

    if-nez v6, :cond_a

    .line 287
    move-object/from16 v0, p5

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/EvaluationTracker;->startEvaluate(Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 288
    sget-object v11, Lorg/apache/poi/ss/formula/eval/ErrorEval;->CIRCULAR_REF_ERROR:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto :goto_1

    .line 290
    :cond_7
    new-instance v12, Lorg/apache/poi/ss/formula/OperationEvaluationContext;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    move-object/from16 v13, p0

    move/from16 v15, p2

    move/from16 v16, p3

    move/from16 v17, p4

    move-object/from16 v18, p5

    invoke-direct/range {v12 .. v18}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;-><init>(Lorg/apache/poi/ss/formula/WorkbookEvaluator;Lorg/apache/poi/ss/formula/EvaluationWorkbook;IIILorg/apache/poi/ss/formula/EvaluationTracker;)V

    .line 294
    .local v12, "ec":Lorg/apache/poi/ss/formula/OperationEvaluationContext;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    move-object/from16 v0, p1

    invoke-interface {v6, v0}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getFormulaTokens(Lorg/apache/poi/ss/formula/EvaluationCell;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v23

    .line 295
    .local v23, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    if-nez v22, :cond_8

    .line 296
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v12, v1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateFormula(Lorg/apache/poi/ss/formula/OperationEvaluationContext;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v11

    .line 303
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :goto_2
    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Lorg/apache/poi/ss/formula/EvaluationTracker;->updateCacheResult(Lorg/apache/poi/ss/formula/eval/ValueEval;)V
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/NotImplementedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    move-object/from16 v0, p5

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/EvaluationTracker;->endEvaluate(Lorg/apache/poi/ss/formula/CellCacheEntry;)V

    .line 342
    .end local v23    # "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_3
    invoke-static {}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->isDebugLogEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 343
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetName(I)Ljava/lang/String;

    move-result-object v25

    .line 344
    .local v25, "sheetName":Ljava/lang/String;
    new-instance v20, Lorg/apache/poi/ss/util/CellReference;

    move-object/from16 v0, v20

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/util/CellReference;-><init>(II)V

    .line 345
    .local v20, "cr":Lorg/apache/poi/ss/util/CellReference;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Evaluated "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->logDebug(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 298
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v20    # "cr":Lorg/apache/poi/ss/util/CellReference;
    .end local v25    # "sheetName":Ljava/lang/String;
    .restart local v23    # "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_8
    :try_start_1
    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/poi/ss/formula/IEvaluationListener;->onStartEvaluate(Lorg/apache/poi/ss/formula/EvaluationCell;Lorg/apache/poi/ss/formula/IEvaluationListener$ICacheEntry;)V

    .line 299
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v12, v1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateFormula(Lorg/apache/poi/ss/formula/OperationEvaluationContext;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v11

    .line 300
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v11}, Lorg/apache/poi/ss/formula/IEvaluationListener;->onEndEvaluate(Lorg/apache/poi/ss/formula/IEvaluationListener$ICacheEntry;Lorg/apache/poi/ss/formula/eval/ValueEval;)V
    :try_end_1
    .catch Lorg/apache/poi/ss/formula/eval/NotImplementedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 305
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v23    # "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :catch_0
    move-exception v21

    .line 306
    .local v21, "e":Lorg/apache/poi/ss/formula/eval/NotImplementedException;
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->addExceptionInfo(Lorg/apache/poi/ss/formula/eval/NotImplementedException;III)Lorg/apache/poi/ss/formula/eval/NotImplementedException;

    move-result-object v6

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 333
    .end local v21    # "e":Lorg/apache/poi/ss/formula/eval/NotImplementedException;
    :catchall_0
    move-exception v6

    .line 334
    move-object/from16 v0, p5

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/EvaluationTracker;->endEvaluate(Lorg/apache/poi/ss/formula/CellCacheEntry;)V

    .line 335
    throw v6

    .line 307
    :catch_1
    move-exception v24

    .line 308
    .local v24, "re":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual/range {v24 .. v24}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    instance-of v6, v6, Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_ignoreMissingWorkbooks:Z

    if-eqz v6, :cond_9

    .line 309
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, " - Continuing with cached value!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->logInfo(Ljava/lang/String;)V

    .line 310
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getCachedFormulaResultType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 328
    :pswitch_0
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Unexpected cell type \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getCellType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\' found!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 312
    :pswitch_1
    new-instance v11, Lorg/apache/poi/ss/formula/eval/NumberEval;

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getNumericCellValue()D

    move-result-wide v6

    invoke-direct {v11, v6, v7}, Lorg/apache/poi/ss/formula/eval/NumberEval;-><init>(D)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 334
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :goto_4
    move-object/from16 v0, p5

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/EvaluationTracker;->endEvaluate(Lorg/apache/poi/ss/formula/CellCacheEntry;)V

    goto/16 :goto_3

    .line 315
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :pswitch_2
    :try_start_4
    new-instance v11, Lorg/apache/poi/ss/formula/eval/StringEval;

    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getStringCellValue()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v11, v6}, Lorg/apache/poi/ss/formula/eval/StringEval;-><init>(Ljava/lang/String;)V

    .line 316
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    goto :goto_4

    .line 318
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :pswitch_3
    sget-object v11, Lorg/apache/poi/ss/formula/eval/BlankEval;->instance:Lorg/apache/poi/ss/formula/eval/BlankEval;

    .line 319
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    goto :goto_4

    .line 321
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :pswitch_4
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getBooleanCellValue()Z

    move-result v6

    invoke-static {v6}, Lorg/apache/poi/ss/formula/eval/BoolEval;->valueOf(Z)Lorg/apache/poi/ss/formula/eval/BoolEval;

    move-result-object v11

    .line 322
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    goto :goto_4

    .line 324
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :pswitch_5
    invoke-interface/range {p1 .. p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getErrorCellValue()I

    move-result v6

    invoke-static {v6}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->valueOf(I)Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v11

    .line 325
    .restart local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    goto :goto_4

    .line 331
    .end local v11    # "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_9
    throw v24
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 337
    .end local v12    # "ec":Lorg/apache/poi/ss/formula/OperationEvaluationContext;
    .end local v24    # "re":Ljava/lang/RuntimeException;
    :cond_a
    if-eqz v22, :cond_b

    .line 338
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;->getValue()Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v6

    move-object/from16 v0, v22

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-interface {v0, v1, v2, v3, v6}, Lorg/apache/poi/ss/formula/IEvaluationListener;->onCacheHit(IIILorg/apache/poi/ss/formula/eval/ValueEval;)V

    .line 340
    :cond_b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/ss/formula/FormulaCellCacheEntry;->getValue()Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v11

    goto/16 :goto_1

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getEvalForPtg(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 10
    .param p1, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p2, "ec"    # Lorg/apache/poi/ss/formula/OperationEvaluationContext;

    .prologue
    .line 623
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/NamePtg;

    if-eqz v0, :cond_2

    move-object v7, p1

    .line 625
    check-cast v7, Lorg/apache/poi/ss/formula/ptg/NamePtg;

    .line 626
    .local v7, "namePtg":Lorg/apache/poi/ss/formula/ptg/NamePtg;
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v0, v7}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getName(Lorg/apache/poi/ss/formula/ptg/NamePtg;)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v8

    .line 627
    .local v8, "nameRecord":Lorg/apache/poi/ss/formula/EvaluationName;
    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->isFunctionName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    new-instance v0, Lorg/apache/poi/ss/formula/eval/NameEval;

    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->getNameText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/eval/NameEval;-><init>(Ljava/lang/String;)V

    .line 676
    .end local v7    # "namePtg":Lorg/apache/poi/ss/formula/ptg/NamePtg;
    .end local v8    # "nameRecord":Lorg/apache/poi/ss/formula/EvaluationName;
    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_0
    return-object v0

    .line 630
    .restart local v7    # "namePtg":Lorg/apache/poi/ss/formula/ptg/NamePtg;
    .restart local v8    # "nameRecord":Lorg/apache/poi/ss/formula/EvaluationName;
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_0
    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->hasFormula()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 631
    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateNameFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto :goto_0

    .line 634
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Don\'t now how to evalate name \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->getNameText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 636
    .end local v7    # "namePtg":Lorg/apache/poi/ss/formula/ptg/NamePtg;
    .end local v8    # "nameRecord":Lorg/apache/poi/ss/formula/EvaluationName;
    :cond_2
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    if-eqz v0, :cond_3

    .line 637
    check-cast p1, Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p2, p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getNameXEval(Lorg/apache/poi/ss/formula/ptg/NameXPtg;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto :goto_0

    .line 640
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_3
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    if-eqz v0, :cond_4

    .line 641
    new-instance v0, Lorg/apache/poi/ss/formula/eval/NumberEval;

    check-cast p1, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/IntPtg;->getValue()I

    move-result v1

    int-to-double v2, v1

    invoke-direct {v0, v2, v3}, Lorg/apache/poi/ss/formula/eval/NumberEval;-><init>(D)V

    goto :goto_0

    .line 643
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_4
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    if-eqz v0, :cond_5

    .line 644
    new-instance v0, Lorg/apache/poi/ss/formula/eval/NumberEval;

    check-cast p1, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;->getValue()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lorg/apache/poi/ss/formula/eval/NumberEval;-><init>(D)V

    goto :goto_0

    .line 646
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_5
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/StringPtg;

    if-eqz v0, :cond_6

    .line 647
    new-instance v0, Lorg/apache/poi/ss/formula/eval/StringEval;

    check-cast p1, Lorg/apache/poi/ss/formula/ptg/StringPtg;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/StringPtg;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/eval/StringEval;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 649
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_6
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/BoolPtg;

    if-eqz v0, :cond_7

    .line 650
    check-cast p1, Lorg/apache/poi/ss/formula/ptg/BoolPtg;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/BoolPtg;->getValue()Z

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/ss/formula/eval/BoolEval;->valueOf(Z)Lorg/apache/poi/ss/formula/eval/BoolEval;

    move-result-object v0

    goto :goto_0

    .line 652
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_7
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    if-eqz v0, :cond_8

    .line 653
    check-cast p1, Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->getErrorCode()I

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->valueOf(I)Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v0

    goto/16 :goto_0

    .line 655
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_8
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/MissingArgPtg;

    if-eqz v0, :cond_9

    .line 656
    sget-object v0, Lorg/apache/poi/ss/formula/eval/MissingArgEval;->instance:Lorg/apache/poi/ss/formula/eval/MissingArgEval;

    goto/16 :goto_0

    .line 658
    :cond_9
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;

    if-nez v0, :cond_a

    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/RefErrorPtg;

    if-nez v0, :cond_a

    .line 659
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/DeletedArea3DPtg;

    if-nez v0, :cond_a

    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/DeletedRef3DPtg;

    if-eqz v0, :cond_b

    .line 660
    :cond_a
    sget-object v0, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto/16 :goto_0

    .line 662
    :cond_b
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    if-eqz v0, :cond_c

    move-object v9, p1

    .line 663
    check-cast v9, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    .line 664
    .local v9, "rptg":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getRow()I

    move-result v0

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getColumn()I

    move-result v1

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    move-result v2

    invoke-virtual {p2, v0, v1, v2}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRef3DEval(III)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto/16 :goto_0

    .line 666
    .end local v9    # "rptg":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    :cond_c
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    if-eqz v0, :cond_d

    move-object v6, p1

    .line 667
    check-cast v6, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    .line 668
    .local v6, "aptg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstRow()I

    move-result v1

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstColumn()I

    move-result v2

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastRow()I

    move-result v3

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastColumn()I

    move-result v4

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    move-result v5

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getArea3DEval(IIIII)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto/16 :goto_0

    .line 670
    .end local v6    # "aptg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    :cond_d
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    if-eqz v0, :cond_e

    move-object v9, p1

    .line 671
    check-cast v9, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    .line 672
    .local v9, "rptg":Lorg/apache/poi/ss/formula/ptg/RefPtg;
    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/RefPtg;->getRow()I

    move-result v0

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/RefPtg;->getColumn()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRefEval(II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto/16 :goto_0

    .line 674
    .end local v9    # "rptg":Lorg/apache/poi/ss/formula/ptg/RefPtg;
    :cond_e
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    if-eqz v0, :cond_f

    move-object v6, p1

    .line 675
    check-cast v6, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    .line 676
    .local v6, "aptg":Lorg/apache/poi/ss/formula/ptg/AreaPtg;
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getFirstRow()I

    move-result v0

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getFirstColumn()I

    move-result v1

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getLastRow()I

    move-result v2

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getLastColumn()I

    move-result v3

    invoke-virtual {p2, v0, v1, v2, v3}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getAreaEval(IIII)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto/16 :goto_0

    .line 679
    .end local v6    # "aptg":Lorg/apache/poi/ss/formula/ptg/AreaPtg;
    :cond_f
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/UnknownPtg;

    if-eqz v0, :cond_10

    .line 683
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "UnknownPtg not allowed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 685
    :cond_10
    instance-of v0, p1, Lorg/apache/poi/ss/formula/ptg/ExpPtg;

    if-eqz v0, :cond_11

    .line 688
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "ExpPtg currently not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 691
    :cond_11
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected ptg class ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getNotSupportedFunctionNames()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 755
    .local v0, "lst":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {}, Lorg/apache/poi/ss/formula/eval/FunctionEval;->getNotSupportedFunctionNames()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 756
    invoke-static {}, Lorg/apache/poi/ss/formula/atp/AnalysisToolPak;->getNotSupportedFunctionNames()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 757
    return-object v0
.end method

.method private getSheetIndex(Lorg/apache/poi/ss/formula/EvaluationSheet;)I
    .locals 4
    .param p1, "sheet"    # Lorg/apache/poi/ss/formula/EvaluationSheet;

    .prologue
    .line 224
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesBySheet:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 225
    .local v0, "result":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 226
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v2, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getSheetIndex(Lorg/apache/poi/ss/formula/EvaluationSheet;)I

    move-result v1

    .line 227
    .local v1, "sheetIndex":I
    if-gez v1, :cond_0

    .line 228
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Specified sheet from a different book"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 230
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 231
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesBySheet:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    .end local v1    # "sheetIndex":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    return v2
.end method

.method public static getSupportedFunctionNames()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 742
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 743
    .local v0, "lst":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {}, Lorg/apache/poi/ss/formula/eval/FunctionEval;->getSupportedFunctionNames()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 744
    invoke-static {}, Lorg/apache/poi/ss/formula/atp/AnalysisToolPak;->getSupportedFunctionNames()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 745
    return-object v0
.end method

.method static getValueFromNonFormulaCell(Lorg/apache/poi/ss/formula/EvaluationCell;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 4
    .param p0, "cell"    # Lorg/apache/poi/ss/formula/EvaluationCell;

    .prologue
    .line 378
    if-nez p0, :cond_0

    .line 379
    sget-object v1, Lorg/apache/poi/ss/formula/eval/BlankEval;->instance:Lorg/apache/poi/ss/formula/eval/BlankEval;

    .line 392
    :goto_0
    return-object v1

    .line 381
    :cond_0
    invoke-interface {p0}, Lorg/apache/poi/ss/formula/EvaluationCell;->getCellType()I

    move-result v0

    .line 382
    .local v0, "cellType":I
    packed-switch v0, :pswitch_data_0

    .line 394
    :pswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unexpected cell type ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 384
    :pswitch_1
    new-instance v1, Lorg/apache/poi/ss/formula/eval/NumberEval;

    invoke-interface {p0}, Lorg/apache/poi/ss/formula/EvaluationCell;->getNumericCellValue()D

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/ss/formula/eval/NumberEval;-><init>(D)V

    goto :goto_0

    .line 386
    :pswitch_2
    new-instance v1, Lorg/apache/poi/ss/formula/eval/StringEval;

    invoke-interface {p0}, Lorg/apache/poi/ss/formula/EvaluationCell;->getStringCellValue()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/poi/ss/formula/eval/StringEval;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 388
    :pswitch_3
    invoke-interface {p0}, Lorg/apache/poi/ss/formula/EvaluationCell;->getBooleanCellValue()Z

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/ss/formula/eval/BoolEval;->valueOf(Z)Lorg/apache/poi/ss/formula/eval/BoolEval;

    move-result-object v1

    goto :goto_0

    .line 390
    :pswitch_4
    sget-object v1, Lorg/apache/poi/ss/formula/eval/BlankEval;->instance:Lorg/apache/poi/ss/formula/eval/BlankEval;

    goto :goto_0

    .line 392
    :pswitch_5
    invoke-interface {p0}, Lorg/apache/poi/ss/formula/EvaluationCell;->getErrorCellValue()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->valueOf(I)Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v1

    goto :goto_0

    .line 382
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private static isDebugLogEnabled()Z
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->LOG:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    return v0
.end method

.method private static isInfoLogEnabled()Z
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->LOG:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    return v0
.end method

.method private static logDebug(Ljava/lang/String;)V
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 158
    invoke-static {}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->LOG:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 161
    :cond_0
    return-void
.end method

.method private static logInfo(Ljava/lang/String;)V
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 163
    invoke-static {}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->isInfoLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    sget-object v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->LOG:Lorg/apache/poi/util/POILogger;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 166
    :cond_0
    return-void
.end method

.method public static registerFunction(Ljava/lang/String;Lorg/apache/poi/ss/formula/functions/FreeRefFunction;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "func"    # Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    .prologue
    .line 769
    invoke-static {p0, p1}, Lorg/apache/poi/ss/formula/atp/AnalysisToolPak;->registerFunction(Ljava/lang/String;Lorg/apache/poi/ss/formula/functions/FreeRefFunction;)V

    .line 770
    return-void
.end method

.method public static registerFunction(Ljava/lang/String;Lorg/apache/poi/ss/formula/functions/Function;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "func"    # Lorg/apache/poi/ss/formula/functions/Function;

    .prologue
    .line 781
    invoke-static {p0, p1}, Lorg/apache/poi/ss/formula/eval/FunctionEval;->registerFunction(Ljava/lang/String;Lorg/apache/poi/ss/formula/functions/Function;)V

    .line 782
    return-void
.end method


# virtual methods
.method attachToEnvironment(Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;Lorg/apache/poi/ss/formula/EvaluationCache;I)V
    .locals 0
    .param p1, "collaboratingWorkbooksEnvironment"    # Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;
    .param p2, "cache"    # Lorg/apache/poi/ss/formula/EvaluationCache;
    .param p3, "workbookIx"    # I

    .prologue
    .line 168
    iput-object p1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_collaboratingWorkbookEnvironment:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    .line 169
    iput-object p2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    .line 170
    iput p3, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbookIx:I

    .line 171
    return-void
.end method

.method public clearAllCachedResultValues()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/EvaluationCache;->clear()V

    .line 203
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesBySheet:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 204
    return-void
.end method

.method detachFromEnvironment()V
    .locals 2

    .prologue
    .line 181
    sget-object v0, Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;->EMPTY:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    iput-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_collaboratingWorkbookEnvironment:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    .line 182
    new-instance v0, Lorg/apache/poi/ss/formula/EvaluationCache;

    iget-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_evaluationListener:Lorg/apache/poi/ss/formula/IEvaluationListener;

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/EvaluationCache;-><init>(Lorg/apache/poi/ss/formula/IEvaluationListener;)V

    iput-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    .line 183
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbookIx:I

    .line 184
    return-void
.end method

.method public evaluate(Lorg/apache/poi/ss/formula/EvaluationCell;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 6
    .param p1, "srcCell"    # Lorg/apache/poi/ss/formula/EvaluationCell;

    .prologue
    .line 237
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getSheet()Lorg/apache/poi/ss/formula/EvaluationSheet;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndex(Lorg/apache/poi/ss/formula/EvaluationSheet;)I

    move-result v2

    .line 238
    .local v2, "sheetIndex":I
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getRowIndex()I

    move-result v3

    invoke-interface {p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getColumnIndex()I

    move-result v4

    new-instance v5, Lorg/apache/poi/ss/formula/EvaluationTracker;

    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    invoke-direct {v5, v0}, Lorg/apache/poi/ss/formula/EvaluationTracker;-><init>(Lorg/apache/poi/ss/formula/EvaluationCache;)V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateAny(Lorg/apache/poi/ss/formula/EvaluationCell;IIILorg/apache/poi/ss/formula/EvaluationTracker;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    return-object v0
.end method

.method evaluateFormula(Lorg/apache/poi/ss/formula/OperationEvaluationContext;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 31
    .param p1, "ec"    # Lorg/apache/poi/ss/formula/OperationEvaluationContext;
    .param p2, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 411
    const-string/jumbo v5, ""

    .line 412
    .local v5, "dbgIndentStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputForNextEval:Z

    move/from16 v25, v0

    if-eqz v25, :cond_0

    .line 414
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    .line 415
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputForNextEval:Z

    .line 417
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    if-lez v25, :cond_1

    .line 420
    const-string/jumbo v5, "                                                                                                    "

    .line 421
    const/16 v25, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v27, v0

    mul-int/lit8 v27, v27, 0x2

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->min(II)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->EVAL_LOG:Lorg/apache/poi/util/POILogger;

    move-object/from16 v25, v0

    const/16 v26, 0x5

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 423
    const-string/jumbo v28, "- evaluateFormula(\'"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRefEvaluatorForCurrentSheet()Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/ss/formula/SheetRefEvaluator;->getSheetName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    .line 424
    const-string/jumbo v28, "\'/"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    new-instance v28, Lorg/apache/poi/ss/util/CellReference;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRowIndex()I

    move-result v29

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getColumnIndex()I

    move-result v30

    invoke-direct/range {v28 .. v30}, Lorg/apache/poi/ss/util/CellReference;-><init>(II)V

    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    .line 425
    const-string/jumbo v28, "): "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    const-string/jumbo v29, "\\Qorg.apache.poi.ss.formula.ptg.\\E"

    const-string/jumbo v30, ""

    invoke-virtual/range {v28 .. v30}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 422
    invoke-virtual/range {v25 .. v27}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 426
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    .line 429
    :cond_1
    new-instance v22, Ljava/util/Stack;

    invoke-direct/range {v22 .. v22}, Ljava/util/Stack;-><init>()V

    .line 430
    .local v22, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Lorg/apache/poi/ss/formula/eval/ValueEval;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    move-object/from16 v0, p2

    array-length v10, v0

    .local v10, "iSize":I
    :goto_0
    if-lt v9, v10, :cond_2

    .line 547
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/poi/ss/formula/eval/ValueEval;

    .line 548
    .local v24, "value":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_e

    .line 549
    new-instance v25, Ljava/lang/IllegalStateException;

    const-string/jumbo v26, "evaluation stack not empty"

    invoke-direct/range {v25 .. v26}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 433
    .end local v24    # "value":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_2
    aget-object v20, p2, v9

    .line 434
    .local v20, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    if-lez v25, :cond_3

    .line 435
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->EVAL_LOG:Lorg/apache/poi/util/POILogger;

    move-object/from16 v25, v0

    const/16 v26, 0x3

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v28, "  * ptg "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string/jumbo v28, ": "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 437
    :cond_3
    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    move/from16 v25, v0

    if-eqz v25, :cond_a

    move-object/from16 v4, v20

    .line 438
    check-cast v4, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    .line 439
    .local v4, "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isSum()Z

    move-result v25

    if-eqz v25, :cond_4

    .line 442
    sget-object v20, Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;->SUM:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    .line 444
    :cond_4
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isOptimizedChoose()Z

    move-result v25

    if-eqz v25, :cond_8

    .line 445
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/formula/eval/ValueEval;

    .line 446
    .local v3, "arg0":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getJumpTable()[I

    move-result-object v12

    .line 448
    .local v12, "jumpTable":[I
    array-length v13, v12

    .line 450
    .local v13, "nChoices":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRowIndex()I

    move-result v25

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getColumnIndex()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v3, v0, v1}, Lorg/apache/poi/ss/formula/functions/Choose;->evaluateFirstArg(Lorg/apache/poi/ss/formula/eval/ValueEval;II)I

    move-result v23

    .line 451
    .local v23, "switchIndex":I
    const/16 v25, 0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-lt v0, v1, :cond_5

    move/from16 v0, v23

    if-le v0, v13, :cond_7

    .line 452
    :cond_5
    sget-object v25, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getChooseFuncOffset()I
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v25

    add-int/lit8 v6, v25, 0x4

    .line 463
    .end local v23    # "switchIndex":I
    .local v6, "dist":I
    :goto_1
    mul-int/lit8 v25, v13, 0x2

    add-int/lit8 v25, v25, 0x2

    sub-int v6, v6, v25

    .line 464
    move-object/from16 v0, p2

    invoke-static {v0, v9, v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->countTokensToBeSkipped([Lorg/apache/poi/ss/formula/ptg/Ptg;II)I

    move-result v25

    add-int v9, v9, v25

    .line 430
    .end local v3    # "arg0":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v4    # "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    .end local v6    # "dist":I
    .end local v12    # "jumpTable":[I
    .end local v13    # "nChoices":I
    :cond_6
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 455
    .restart local v3    # "arg0":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .restart local v4    # "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    .restart local v12    # "jumpTable":[I
    .restart local v13    # "nChoices":I
    .restart local v23    # "switchIndex":I
    :cond_7
    add-int/lit8 v25, v23, -0x1

    :try_start_1
    aget v6, v12, v25
    :try_end_1
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v6    # "dist":I
    goto :goto_1

    .line 457
    .end local v6    # "dist":I
    .end local v23    # "switchIndex":I
    :catch_0
    move-exception v7

    .line 458
    .local v7, "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v7}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getChooseFuncOffset()I

    move-result v25

    add-int/lit8 v6, v25, 0x4

    .restart local v6    # "dist":I
    goto :goto_1

    .line 467
    .end local v3    # "arg0":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v6    # "dist":I
    .end local v7    # "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    .end local v12    # "jumpTable":[I
    .end local v13    # "nChoices":I
    :cond_8
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isOptimizedIf()Z

    move-result v25

    if-eqz v25, :cond_9

    .line 468
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/formula/eval/ValueEval;

    .line 471
    .restart local v3    # "arg0":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRowIndex()I

    move-result v25

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getColumnIndex()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v3, v0, v1}, Lorg/apache/poi/ss/formula/functions/IfFunc;->evaluateFirstArg(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Z
    :try_end_2
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v8

    .line 481
    .local v8, "evaluatedPredicate":Z
    if-nez v8, :cond_6

    .line 484
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getData()S

    move-result v6

    .line 485
    .restart local v6    # "dist":I
    move-object/from16 v0, p2

    invoke-static {v0, v9, v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->countTokensToBeSkipped([Lorg/apache/poi/ss/formula/ptg/Ptg;II)I

    move-result v25

    add-int v9, v9, v25

    .line 486
    add-int/lit8 v25, v9, 0x1

    aget-object v14, p2, v25

    .line 487
    .local v14, "nextPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    aget-object v25, p2, v9

    move-object/from16 v0, v25

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    move/from16 v25, v0

    if-eqz v25, :cond_6

    instance-of v0, v14, Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;

    move/from16 v25, v0

    if-eqz v25, :cond_6

    .line 489
    add-int/lit8 v9, v9, 0x1

    .line 490
    sget-object v25, Lorg/apache/poi/ss/formula/eval/BoolEval;->FALSE:Lorg/apache/poi/ss/formula/eval/BoolEval;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 472
    .end local v6    # "dist":I
    .end local v8    # "evaluatedPredicate":Z
    .end local v14    # "nextPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :catch_1
    move-exception v7

    .line 473
    .restart local v7    # "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v7}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getData()S

    move-result v6

    .line 475
    .restart local v6    # "dist":I
    move-object/from16 v0, p2

    invoke-static {v0, v9, v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->countTokensToBeSkipped([Lorg/apache/poi/ss/formula/ptg/Ptg;II)I

    move-result v25

    add-int v9, v9, v25

    .line 476
    aget-object v4, p2, v9

    .end local v4    # "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    check-cast v4, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    .line 477
    .restart local v4    # "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getData()S

    move-result v25

    add-int/lit8 v6, v25, 0x1

    .line 478
    move-object/from16 v0, p2

    invoke-static {v0, v9, v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->countTokensToBeSkipped([Lorg/apache/poi/ss/formula/ptg/Ptg;II)I

    move-result v25

    add-int v9, v9, v25

    .line 479
    goto/16 :goto_2

    .line 495
    .end local v3    # "arg0":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v6    # "dist":I
    .end local v7    # "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    :cond_9
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isSkip()Z

    move-result v25

    if-eqz v25, :cond_a

    .line 496
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getData()S

    move-result v25

    add-int/lit8 v6, v25, 0x1

    .line 497
    .restart local v6    # "dist":I
    move-object/from16 v0, p2

    invoke-static {v0, v9, v6}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->countTokensToBeSkipped([Lorg/apache/poi/ss/formula/ptg/Ptg;II)I

    move-result v25

    add-int v9, v9, v25

    .line 498
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v25

    sget-object v26, Lorg/apache/poi/ss/formula/eval/MissingArgEval;->instance:Lorg/apache/poi/ss/formula/eval/MissingArgEval;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_6

    .line 499
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 500
    sget-object v25, Lorg/apache/poi/ss/formula/eval/BlankEval;->instance:Lorg/apache/poi/ss/formula/eval/BlankEval;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 505
    .end local v4    # "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    .end local v6    # "dist":I
    :cond_a
    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/ControlPtg;

    move/from16 v25, v0

    if-nez v25, :cond_6

    .line 509
    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;

    move/from16 v25, v0

    if-nez v25, :cond_6

    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/MemAreaPtg;

    move/from16 v25, v0

    if-nez v25, :cond_6

    .line 513
    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/MemErrPtg;

    move/from16 v25, v0

    if-nez v25, :cond_6

    .line 518
    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    move/from16 v25, v0

    if-eqz v25, :cond_c

    move-object/from16 v18, v20

    .line 519
    check-cast v18, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    .line 521
    .local v18, "optg":Lorg/apache/poi/ss/formula/ptg/OperationPtg;
    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/poi/ss/formula/ptg/UnionPtg;

    move/from16 v25, v0

    if-nez v25, :cond_6

    .line 524
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/ss/formula/ptg/OperationPtg;->getNumberOfOperands()I

    move-result v15

    .line 525
    .local v15, "numops":I
    new-array v0, v15, [Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-object/from16 v17, v0

    .line 528
    .local v17, "ops":[Lorg/apache/poi/ss/formula/eval/ValueEval;
    add-int/lit8 v11, v15, -0x1

    .local v11, "j":I
    :goto_3
    if-gez v11, :cond_b

    .line 533
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/formula/OperationEvaluatorFactory;->evaluate(Lorg/apache/poi/ss/formula/ptg/OperationPtg;[Lorg/apache/poi/ss/formula/eval/ValueEval;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v16

    .line 537
    .end local v11    # "j":I
    .end local v15    # "numops":I
    .end local v17    # "ops":[Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v18    # "optg":Lorg/apache/poi/ss/formula/ptg/OperationPtg;
    .local v16, "opResult":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :goto_4
    if-nez v16, :cond_d

    .line 538
    new-instance v25, Ljava/lang/RuntimeException;

    const-string/jumbo v26, "Evaluation result must not be null"

    invoke-direct/range {v25 .. v26}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 529
    .end local v16    # "opResult":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .restart local v11    # "j":I
    .restart local v15    # "numops":I
    .restart local v17    # "ops":[Lorg/apache/poi/ss/formula/eval/ValueEval;
    .restart local v18    # "optg":Lorg/apache/poi/ss/formula/ptg/OperationPtg;
    :cond_b
    invoke-virtual/range {v22 .. v22}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/poi/ss/formula/eval/ValueEval;

    .line 530
    .local v19, "p":Lorg/apache/poi/ss/formula/eval/ValueEval;
    aput-object v19, v17, v11

    .line 528
    add-int/lit8 v11, v11, -0x1

    goto :goto_3

    .line 535
    .end local v11    # "j":I
    .end local v15    # "numops":I
    .end local v17    # "ops":[Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v18    # "optg":Lorg/apache/poi/ss/formula/ptg/OperationPtg;
    .end local v19    # "p":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getEvalForPtg(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v16

    .restart local v16    # "opResult":Lorg/apache/poi/ss/formula/eval/ValueEval;
    goto :goto_4

    .line 541
    :cond_d
    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    if-lez v25, :cond_6

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->EVAL_LOG:Lorg/apache/poi/util/POILogger;

    move-object/from16 v25, v0

    const/16 v26, 0x3

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v28, "    = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto/16 :goto_2

    .line 551
    .end local v16    # "opResult":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v20    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .restart local v24    # "value":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRowIndex()I

    move-result v25

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getColumnIndex()I

    move-result v26

    invoke-static/range {v24 .. v26}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dereferenceResult(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v21

    .line 552
    .local v21, "result":Lorg/apache/poi/ss/formula/eval/ValueEval;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    if-lez v25, :cond_f

    .line 553
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->EVAL_LOG:Lorg/apache/poi/util/POILogger;

    move-object/from16 v25, v0

    const/16 v26, 0x3

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v28, "finshed eval of "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    .line 554
    new-instance v28, Lorg/apache/poi/ss/util/CellReference;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRowIndex()I

    move-result v29

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getColumnIndex()I

    move-result v30

    invoke-direct/range {v28 .. v30}, Lorg/apache/poi/ss/util/CellReference;-><init>(II)V

    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    .line 555
    const-string/jumbo v28, ": "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 553
    invoke-virtual/range {v25 .. v27}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 556
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    .line 557
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_f

    .line 559
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputIndent:I

    .line 562
    :cond_f
    return-object v21
.end method

.method evaluateNameFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 2
    .param p1, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p2, "ec"    # Lorg/apache/poi/ss/formula/OperationEvaluationContext;

    .prologue
    .line 697
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 698
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getEvalForPtg(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    .line 700
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2, p1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateFormula(Lorg/apache/poi/ss/formula/OperationEvaluationContext;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    goto :goto_0
.end method

.method evaluateReference(Lorg/apache/poi/ss/formula/EvaluationSheet;IIILorg/apache/poi/ss/formula/EvaluationTracker;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 6
    .param p1, "sheet"    # Lorg/apache/poi/ss/formula/EvaluationSheet;
    .param p2, "sheetIndex"    # I
    .param p3, "rowIndex"    # I
    .param p4, "columnIndex"    # I
    .param p5, "tracker"    # Lorg/apache/poi/ss/formula/EvaluationTracker;

    .prologue
    .line 709
    invoke-interface {p1, p3, p4}, Lorg/apache/poi/ss/formula/EvaluationSheet;->getCell(II)Lorg/apache/poi/ss/formula/EvaluationCell;

    move-result-object v1

    .local v1, "cell":Lorg/apache/poi/ss/formula/EvaluationCell;
    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 710
    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateAny(Lorg/apache/poi/ss/formula/EvaluationCell;IIILorg/apache/poi/ss/formula/EvaluationTracker;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    return-object v0
.end method

.method public findUserDefinedFunction(Ljava/lang/String;)Lorg/apache/poi/ss/formula/functions/FreeRefFunction;
    .locals 1
    .param p1, "functionName"    # Ljava/lang/String;

    .prologue
    .line 713
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_udfFinder:Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;->findFunction(Ljava/lang/String;)Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    move-result-object v0

    return-object v0
.end method

.method getEnvironment()Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_collaboratingWorkbookEnvironment:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    return-object v0
.end method

.method getEvaluationListener()Lorg/apache/poi/ss/formula/IEvaluationListener;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_evaluationListener:Lorg/apache/poi/ss/formula/IEvaluationListener;

    return-object v0
.end method

.method getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sheetIndex"    # I

    .prologue
    .line 139
    const/4 v0, 0x0

    .line 140
    .local v0, "namePtg":Lorg/apache/poi/ss/formula/ptg/NamePtg;
    iget-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v1, p1, p2}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v1, p1, p2}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/poi/ss/formula/EvaluationName;->createPtg()Lorg/apache/poi/ss/formula/ptg/NamePtg;

    move-result-object v0

    .line 144
    :cond_0
    if-nez v0, :cond_1

    .line 145
    const/4 v1, 0x0

    .line 147
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v1, v0}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getName(Lorg/apache/poi/ss/formula/ptg/NamePtg;)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v1

    goto :goto_0
.end method

.method getOtherWorkbookEvaluator(Ljava/lang/String;)Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    .locals 1
    .param p1, "workbookName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_collaboratingWorkbookEnvironment:Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment;->getWorkbookEvaluator(Ljava/lang/String;)Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    move-result-object v0

    return-object v0
.end method

.method getSheet(I)Lorg/apache/poi/ss/formula/EvaluationSheet;
    .locals 1
    .param p1, "sheetIndex"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v0, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getSheet(I)Lorg/apache/poi/ss/formula/EvaluationSheet;

    move-result-object v0

    return-object v0
.end method

.method getSheetIndex(Ljava/lang/String;)I
    .locals 3
    .param p1, "sheetName"    # Ljava/lang/String;

    .prologue
    .line 246
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesByName:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 247
    .local v0, "result":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 248
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v2, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result v1

    .line 249
    .local v1, "sheetIndex":I
    if-gez v1, :cond_0

    .line 250
    const/4 v2, -0x1

    .line 255
    .end local v1    # "sheetIndex":I
    :goto_0
    return v2

    .line 252
    .restart local v1    # "sheetIndex":I
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 253
    iget-object v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_sheetIndexesByName:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    .end local v1    # "sheetIndex":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method getSheetIndexByExternIndex(I)I
    .locals 1
    .param p1, "externSheetIndex"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v0, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->convertFromExternSheetIndex(I)I

    move-result v0

    return v0
.end method

.method getSheetName(I)Ljava/lang/String;
    .locals 1
    .param p1, "sheetIndex"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v0, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getWorkbook()Lorg/apache/poi/ss/formula/EvaluationWorkbook;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    return-object v0
.end method

.method public notifyDeleteCell(Lorg/apache/poi/ss/formula/EvaluationCell;)V
    .locals 3
    .param p1, "cell"    # Lorg/apache/poi/ss/formula/EvaluationCell;

    .prologue
    .line 219
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getSheet()Lorg/apache/poi/ss/formula/EvaluationSheet;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndex(Lorg/apache/poi/ss/formula/EvaluationSheet;)I

    move-result v0

    .line 220
    .local v0, "sheetIndex":I
    iget-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    iget v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbookIx:I

    invoke-virtual {v1, v2, v0, p1}, Lorg/apache/poi/ss/formula/EvaluationCache;->notifyDeleteCell(IILorg/apache/poi/ss/formula/EvaluationCell;)V

    .line 221
    return-void
.end method

.method public notifyUpdateCell(Lorg/apache/poi/ss/formula/EvaluationCell;)V
    .locals 3
    .param p1, "cell"    # Lorg/apache/poi/ss/formula/EvaluationCell;

    .prologue
    .line 211
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/EvaluationCell;->getSheet()Lorg/apache/poi/ss/formula/EvaluationSheet;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndex(Lorg/apache/poi/ss/formula/EvaluationSheet;)I

    move-result v0

    .line 212
    .local v0, "sheetIndex":I
    iget-object v1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_cache:Lorg/apache/poi/ss/formula/EvaluationCache;

    iget v2, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_workbookIx:I

    invoke-virtual {v1, v2, v0, p1}, Lorg/apache/poi/ss/formula/EvaluationCache;->notifyUpdateCell(IILorg/apache/poi/ss/formula/EvaluationCell;)V

    .line 213
    return-void
.end method

.method public setDebugEvaluationOutputForNextEval(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 785
    iput-boolean p1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->dbgEvaluationOutputForNextEval:Z

    .line 786
    return-void
.end method

.method public setIgnoreMissingWorkbooks(Z)V
    .locals 0
    .param p1, "ignore"    # Z

    .prologue
    .line 733
    iput-boolean p1, p0, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->_ignoreMissingWorkbooks:Z

    .line 734
    return-void
.end method
