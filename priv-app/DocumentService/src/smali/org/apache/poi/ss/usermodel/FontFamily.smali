.class public final enum Lorg/apache/poi/ss/usermodel/FontFamily;
.super Ljava/lang/Enum;
.source "FontFamily.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/FontFamily;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DECORATIVE:Lorg/apache/poi/ss/usermodel/FontFamily;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontFamily;

.field public static final enum MODERN:Lorg/apache/poi/ss/usermodel/FontFamily;

.field public static final enum NOT_APPLICABLE:Lorg/apache/poi/ss/usermodel/FontFamily;

.field public static final enum ROMAN:Lorg/apache/poi/ss/usermodel/FontFamily;

.field public static final enum SCRIPT:Lorg/apache/poi/ss/usermodel/FontFamily;

.field public static final enum SWISS:Lorg/apache/poi/ss/usermodel/FontFamily;

.field private static _table:[Lorg/apache/poi/ss/usermodel/FontFamily;


# instance fields
.field private family:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 29
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontFamily;

    const-string/jumbo v3, "NOT_APPLICABLE"

    invoke-direct {v2, v3, v1, v1}, Lorg/apache/poi/ss/usermodel/FontFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->NOT_APPLICABLE:Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 30
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontFamily;

    const-string/jumbo v3, "ROMAN"

    invoke-direct {v2, v3, v6, v6}, Lorg/apache/poi/ss/usermodel/FontFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->ROMAN:Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 31
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontFamily;

    const-string/jumbo v3, "SWISS"

    invoke-direct {v2, v3, v7, v7}, Lorg/apache/poi/ss/usermodel/FontFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->SWISS:Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 32
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontFamily;

    const-string/jumbo v3, "MODERN"

    invoke-direct {v2, v3, v8, v8}, Lorg/apache/poi/ss/usermodel/FontFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->MODERN:Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 33
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontFamily;

    const-string/jumbo v3, "SCRIPT"

    invoke-direct {v2, v3, v9, v9}, Lorg/apache/poi/ss/usermodel/FontFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->SCRIPT:Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 34
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontFamily;

    const-string/jumbo v3, "DECORATIVE"

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->DECORATIVE:Lorg/apache/poi/ss/usermodel/FontFamily;

    const/4 v2, 0x6

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/FontFamily;

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontFamily;->NOT_APPLICABLE:Lorg/apache/poi/ss/usermodel/FontFamily;

    aput-object v3, v2, v1

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontFamily;->ROMAN:Lorg/apache/poi/ss/usermodel/FontFamily;

    aput-object v3, v2, v6

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontFamily;->SWISS:Lorg/apache/poi/ss/usermodel/FontFamily;

    aput-object v3, v2, v7

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontFamily;->MODERN:Lorg/apache/poi/ss/usermodel/FontFamily;

    aput-object v3, v2, v8

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontFamily;->SCRIPT:Lorg/apache/poi/ss/usermodel/FontFamily;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontFamily;->DECORATIVE:Lorg/apache/poi/ss/usermodel/FontFamily;

    aput-object v4, v2, v3

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 51
    const/4 v2, 0x6

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/FontFamily;

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontFamily;->_table:[Lorg/apache/poi/ss/usermodel/FontFamily;

    .line 54
    invoke-static {}, Lorg/apache/poi/ss/usermodel/FontFamily;->values()[Lorg/apache/poi/ss/usermodel/FontFamily;

    move-result-object v2

    array-length v3, v2

    .local v0, "c":Lorg/apache/poi/ss/usermodel/FontFamily;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 57
    return-void

    .line 54
    :cond_0
    aget-object v0, v2, v1

    .line 55
    sget-object v4, Lorg/apache/poi/ss/usermodel/FontFamily;->_table:[Lorg/apache/poi/ss/usermodel/FontFamily;

    invoke-virtual {v0}, Lorg/apache/poi/ss/usermodel/FontFamily;->getValue()I

    move-result v5

    aput-object v0, v4, v5

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lorg/apache/poi/ss/usermodel/FontFamily;->family:I

    .line 40
    return-void
.end method

.method public static valueOf(I)Lorg/apache/poi/ss/usermodel/FontFamily;
    .locals 1
    .param p0, "family"    # I

    .prologue
    .line 60
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontFamily;->_table:[Lorg/apache/poi/ss/usermodel/FontFamily;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/FontFamily;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/FontFamily;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/FontFamily;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/FontFamily;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontFamily;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontFamily;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/FontFamily;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/poi/ss/usermodel/FontFamily;->family:I

    return v0
.end method
