.class final Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;
.super Ljava/lang/Object;
.source "LookupUtils.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/functions/LookupUtils$ValueVector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/functions/LookupUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ColumnVector"
.end annotation


# instance fields
.field private final _columnIndex:I

.field private final _size:I

.field private final _tableArray:Lorg/apache/poi/ss/formula/TwoDEval;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ss/formula/TwoDEval;I)V
    .locals 4
    .param p1, "tableArray"    # Lorg/apache/poi/ss/formula/TwoDEval;
    .param p2, "columnIndex"    # I

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput p2, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_columnIndex:I

    .line 89
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/TwoDEval;->getWidth()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 90
    .local v0, "lastColIx":I
    if-ltz p2, :cond_0

    if-le p2, v0, :cond_1

    .line 91
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Specified column index ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 92
    const-string/jumbo v3, ") is outside the allowed range (0.."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 94
    :cond_1
    iput-object p1, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_tableArray:Lorg/apache/poi/ss/formula/TwoDEval;

    .line 95
    iget-object v1, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_tableArray:Lorg/apache/poi/ss/formula/TwoDEval;

    invoke-interface {v1}, Lorg/apache/poi/ss/formula/TwoDEval;->getHeight()I

    move-result v1

    iput v1, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_size:I

    .line 96
    return-void
.end method


# virtual methods
.method public getItem(I)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 99
    iget v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_size:I

    if-le p1, v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Specified index ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 101
    const-string/jumbo v2, ") is outside the allowed range (0.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_size:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 100
    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_tableArray:Lorg/apache/poi/ss/formula/TwoDEval;

    iget v1, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_columnIndex:I

    invoke-interface {v0, p1, v1}, Lorg/apache/poi/ss/formula/TwoDEval;->getValue(II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$ColumnVector;->_size:I

    return v0
.end method
