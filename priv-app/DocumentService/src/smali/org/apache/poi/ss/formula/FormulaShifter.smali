.class public final Lorg/apache/poi/ss/formula/FormulaShifter;
.super Ljava/lang/Object;
.source "FormulaShifter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$poi$ss$formula$FormulaShifter$ShiftMode:[I


# instance fields
.field private final _amountToMove:I

.field private final _dstSheetIndex:I

.field private final _externSheetIndex:I

.field private final _firstMovedIndex:I

.field private final _lastMovedIndex:I

.field private final _mode:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

.field private final _srcSheetIndex:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$poi$ss$formula$FormulaShifter$ShiftMode()[I
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaShifter;->$SWITCH_TABLE$org$apache$poi$ss$formula$FormulaShifter$ShiftMode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->values()[Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Row:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Sheet:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lorg/apache/poi/ss/formula/FormulaShifter;->$SWITCH_TABLE$org$apache$poi$ss$formula$FormulaShifter$ShiftMode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>(II)V
    .locals 1
    .param p1, "srcSheetIndex"    # I
    .param p2, "dstSheetIndex"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_externSheetIndex:I

    .line 75
    iput p1, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_srcSheetIndex:I

    .line 76
    iput p2, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_dstSheetIndex:I

    .line 77
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Sheet:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    iput-object v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_mode:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    .line 78
    return-void
.end method

.method private constructor <init>(IIII)V
    .locals 2
    .param p1, "externSheetIndex"    # I
    .param p2, "firstMovedIndex"    # I
    .param p3, "lastMovedIndex"    # I
    .param p4, "amountToMove"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    if-nez p4, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "amountToMove must not be zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    if-le p2, p3, :cond_1

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "firstMovedIndex, lastMovedIndex out of order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    iput p1, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_externSheetIndex:I

    .line 59
    iput p2, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    .line 60
    iput p3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    .line 61
    iput p4, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    .line 62
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Row:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    iput-object v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_mode:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_dstSheetIndex:I

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_srcSheetIndex:I

    .line 65
    return-void
.end method

.method private adjustPtg(Lorg/apache/poi/ss/formula/ptg/Ptg;I)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 3
    .param p1, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p2, "currentExternSheetIx"    # I

    .prologue
    .line 117
    invoke-static {}, Lorg/apache/poi/ss/formula/FormulaShifter;->$SWITCH_TABLE$org$apache$poi$ss$formula$FormulaShifter$ShiftMode()[I

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_mode:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 123
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unsupported shift mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_mode:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustPtgDueToRowMove(Lorg/apache/poi/ss/formula/ptg/Ptg;I)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    :pswitch_1
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustPtgDueToShiftMove(Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private adjustPtgDueToRowMove(Lorg/apache/poi/ss/formula/ptg/Ptg;I)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 5
    .param p1, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p2, "currentExternSheetIx"    # I

    .prologue
    const/4 v2, 0x0

    .line 130
    instance-of v3, p1, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    if-eqz v3, :cond_2

    .line 131
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_externSheetIndex:I

    if-eq p2, v3, :cond_1

    move-object p1, v2

    .line 163
    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_1
    move-object v1, p1

    .line 135
    check-cast v1, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    .line 136
    .local v1, "rptg":Lorg/apache/poi/ss/formula/ptg/RefPtg;
    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaShifter;->rowMoveRefPtg(Lorg/apache/poi/ss/formula/ptg/RefPtgBase;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object p1

    goto :goto_0

    .line 138
    .end local v1    # "rptg":Lorg/apache/poi/ss/formula/ptg/RefPtg;
    :cond_2
    instance-of v3, p1, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    if-eqz v3, :cond_4

    move-object v1, p1

    .line 139
    check-cast v1, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    .line 140
    .local v1, "rptg":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_externSheetIndex:I

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    move-result v4

    if-eq v3, v4, :cond_3

    move-object p1, v2

    .line 143
    goto :goto_0

    .line 145
    :cond_3
    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaShifter;->rowMoveRefPtg(Lorg/apache/poi/ss/formula/ptg/RefPtgBase;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object p1

    goto :goto_0

    .line 147
    .end local v1    # "rptg":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    :cond_4
    instance-of v3, p1, Lorg/apache/poi/ss/formula/ptg/Area2DPtgBase;

    if-eqz v3, :cond_5

    .line 148
    iget v2, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_externSheetIndex:I

    if-ne p2, v2, :cond_0

    .line 152
    check-cast p1, Lorg/apache/poi/ss/formula/ptg/Area2DPtgBase;

    .end local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/formula/FormulaShifter;->rowMoveAreaPtg(Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object p1

    goto :goto_0

    .line 154
    .restart local p1    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_5
    instance-of v3, p1, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    if-eqz v3, :cond_7

    move-object v0, p1

    .line 155
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    .line 156
    .local v0, "aptg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_externSheetIndex:I

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    move-result v4

    if-eq v3, v4, :cond_6

    move-object p1, v2

    .line 159
    goto :goto_0

    .line 161
    :cond_6
    invoke-direct {p0, v0}, Lorg/apache/poi/ss/formula/FormulaShifter;->rowMoveAreaPtg(Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object p1

    goto :goto_0

    .end local v0    # "aptg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    :cond_7
    move-object p1, v2

    .line 163
    goto :goto_0
.end method

.method private adjustPtgDueToShiftMove(Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 4
    .param p1, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 167
    const/4 v1, 0x0

    .line 168
    .local v1, "updatedPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v2, p1, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 169
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    .line 170
    .local v0, "ref":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    move-result v2

    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_srcSheetIndex:I

    if-ne v2, v3, :cond_1

    .line 171
    iget v2, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_dstSheetIndex:I

    invoke-virtual {v0, v2}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->setExternSheetIndex(I)V

    .line 172
    move-object v1, v0

    .line 178
    .end local v0    # "ref":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    :cond_0
    :goto_0
    return-object v1

    .line 173
    .restart local v0    # "ref":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    move-result v2

    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_dstSheetIndex:I

    if-ne v2, v3, :cond_0

    .line 174
    iget v2, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_srcSheetIndex:I

    invoke-virtual {v0, v2}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->setExternSheetIndex(I)V

    .line 175
    move-object v1, v0

    goto :goto_0
.end method

.method private static createDeletedRef(Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 5
    .param p0, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 336
    instance-of v2, p0, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    if-eqz v2, :cond_0

    .line 337
    new-instance v2, Lorg/apache/poi/ss/formula/ptg/RefErrorPtg;

    invoke-direct {v2}, Lorg/apache/poi/ss/formula/ptg/RefErrorPtg;-><init>()V

    .line 348
    :goto_0
    return-object v2

    .line 339
    :cond_0
    instance-of v2, p0, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    if-eqz v2, :cond_1

    move-object v1, p0

    .line 340
    check-cast v1, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    .line 341
    .local v1, "rptg":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    new-instance v2, Lorg/apache/poi/ss/formula/ptg/DeletedRef3DPtg;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    move-result v3

    invoke-direct {v2, v3}, Lorg/apache/poi/ss/formula/ptg/DeletedRef3DPtg;-><init>(I)V

    goto :goto_0

    .line 343
    .end local v1    # "rptg":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    :cond_1
    instance-of v2, p0, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    if-eqz v2, :cond_2

    .line 344
    new-instance v2, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;

    invoke-direct {v2}, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;-><init>()V

    goto :goto_0

    .line 346
    :cond_2
    instance-of v2, p0, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    if-eqz v2, :cond_3

    move-object v0, p0

    .line 347
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    .line 348
    .local v0, "area3DPtg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    new-instance v2, Lorg/apache/poi/ss/formula/ptg/DeletedArea3DPtg;

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    move-result v3

    invoke-direct {v2, v3}, Lorg/apache/poi/ss/formula/ptg/DeletedArea3DPtg;-><init>(I)V

    goto :goto_0

    .line 351
    .end local v0    # "area3DPtg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unexpected ref ptg class ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static createForRowShift(IIII)Lorg/apache/poi/ss/formula/FormulaShifter;
    .locals 1
    .param p0, "externSheetIndex"    # I
    .param p1, "firstMovedRowIndex"    # I
    .param p2, "lastMovedRowIndex"    # I
    .param p3, "numberOfRowsToMove"    # I

    .prologue
    .line 81
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaShifter;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/poi/ss/formula/FormulaShifter;-><init>(IIII)V

    return-object v0
.end method

.method public static createForSheetShift(II)Lorg/apache/poi/ss/formula/FormulaShifter;
    .locals 1
    .param p0, "srcSheetIndex"    # I
    .param p1, "dstSheetIndex"    # I

    .prologue
    .line 85
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaShifter;

    invoke-direct {v0, p0, p1}, Lorg/apache/poi/ss/formula/FormulaShifter;-><init>(II)V

    return-object v0
.end method

.method private rowMoveAreaPtg(Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 11
    .param p1, "aptg"    # Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    .prologue
    const/4 v8, 0x0

    .line 211
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result v0

    .line 212
    .local v0, "aFirstRow":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getLastRow()I

    move-result v1

    .line 213
    .local v1, "aLastRow":I
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    if-gt v9, v0, :cond_0

    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    if-gt v1, v9, :cond_0

    .line 216
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int/2addr v8, v0

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 217
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int/2addr v8, v1

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    .line 329
    .end local p1    # "aptg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    :goto_0
    return-object p1

    .line 222
    .restart local p1    # "aptg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    :cond_0
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    iget v10, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int v4, v9, v10

    .line 223
    .local v4, "destFirstRowIndex":I
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    iget v10, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int v5, v9, v10

    .line 225
    .local v5, "destLastRowIndex":I
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    if-ge v0, v9, :cond_3

    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    if-ge v9, v1, :cond_3

    .line 230
    if-ge v4, v0, :cond_1

    if-gt v0, v5, :cond_1

    .line 232
    add-int/lit8 v8, v5, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    goto :goto_0

    .line 234
    :cond_1
    if-gt v4, v1, :cond_2

    if-ge v1, v5, :cond_2

    .line 236
    add-int/lit8 v8, v4, -0x1

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    goto :goto_0

    :cond_2
    move-object p1, v8

    .line 241
    goto :goto_0

    .line 243
    :cond_3
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    if-gt v9, v0, :cond_8

    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    if-gt v0, v9, :cond_8

    .line 246
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    if-gez v9, :cond_4

    .line 248
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int/2addr v8, v0

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    goto :goto_0

    .line 251
    :cond_4
    if-le v4, v1, :cond_5

    move-object p1, v8

    .line 253
    goto :goto_0

    .line 255
    :cond_5
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int v6, v0, v8

    .line 256
    .local v6, "newFirstRowIx":I
    if-ge v5, v1, :cond_6

    .line 259
    invoke-virtual {p1, v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    goto :goto_0

    .line 263
    :cond_6
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    add-int/lit8 v3, v8, 0x1

    .line 264
    .local v3, "areaRemainingTopRowIx":I
    if-le v4, v3, :cond_7

    .line 266
    move v6, v3

    .line 268
    :cond_7
    invoke-virtual {p1, v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 269
    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    goto :goto_0

    .line 272
    .end local v3    # "areaRemainingTopRowIx":I
    .end local v6    # "newFirstRowIx":I
    :cond_8
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    if-gt v9, v1, :cond_d

    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    if-gt v1, v9, :cond_d

    .line 275
    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    if-lez v9, :cond_9

    .line 277
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int/2addr v8, v1

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    goto :goto_0

    .line 280
    :cond_9
    if-ge v5, v0, :cond_a

    move-object p1, v8

    .line 282
    goto :goto_0

    .line 284
    :cond_a
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int v7, v1, v8

    .line 285
    .local v7, "newLastRowIx":I
    if-le v4, v0, :cond_b

    .line 288
    invoke-virtual {p1, v7}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    goto :goto_0

    .line 292
    :cond_b
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    add-int/lit8 v2, v8, -0x1

    .line 293
    .local v2, "areaRemainingBottomRowIx":I
    if-ge v5, v2, :cond_c

    .line 295
    move v7, v2

    .line 297
    :cond_c
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 298
    invoke-virtual {p1, v7}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    goto/16 :goto_0

    .line 304
    .end local v2    # "areaRemainingBottomRowIx":I
    .end local v7    # "newLastRowIx":I
    :cond_d
    if-lt v5, v0, :cond_e

    if-ge v1, v4, :cond_f

    :cond_e
    move-object p1, v8

    .line 306
    goto/16 :goto_0

    .line 309
    :cond_f
    if-gt v4, v0, :cond_10

    if-gt v1, v5, :cond_10

    .line 311
    invoke-static {p1}, Lorg/apache/poi/ss/formula/FormulaShifter;->createDeletedRef(Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object p1

    goto/16 :goto_0

    .line 314
    :cond_10
    if-gt v0, v4, :cond_11

    if-gt v5, v1, :cond_11

    move-object p1, v8

    .line 316
    goto/16 :goto_0

    .line 319
    :cond_11
    if-ge v4, v0, :cond_12

    if-gt v0, v5, :cond_12

    .line 322
    add-int/lit8 v8, v5, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    goto/16 :goto_0

    .line 325
    :cond_12
    if-ge v4, v1, :cond_13

    if-gt v1, v5, :cond_13

    .line 328
    add-int/lit8 v8, v4, -0x1

    invoke-virtual {p1, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    goto/16 :goto_0

    .line 331
    :cond_13
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Situation not covered: ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 332
    iget v10, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 331
    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method private rowMoveRefPtg(Lorg/apache/poi/ss/formula/ptg/RefPtgBase;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 6
    .param p1, "rptg"    # Lorg/apache/poi/ss/formula/ptg/RefPtgBase;

    .prologue
    .line 182
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/ptg/RefPtgBase;->getRow()I

    move-result v2

    .line 183
    .local v2, "refRow":I
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    if-gt v3, v2, :cond_0

    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    if-gt v2, v3, :cond_0

    .line 186
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int/2addr v3, v2

    invoke-virtual {p1, v3}, Lorg/apache/poi/ss/formula/ptg/RefPtgBase;->setRow(I)V

    .line 204
    .end local p1    # "rptg":Lorg/apache/poi/ss/formula/ptg/RefPtgBase;
    :goto_0
    return-object p1

    .line 191
    .restart local p1    # "rptg":Lorg/apache/poi/ss/formula/ptg/RefPtgBase;
    :cond_0
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    iget v4, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int v0, v3, v4

    .line 192
    .local v0, "destFirstRowIndex":I
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    iget v4, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    add-int v1, v3, v4

    .line 197
    .local v1, "destLastRowIndex":I
    if-lt v1, v2, :cond_1

    if-ge v2, v0, :cond_2

    .line 199
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 202
    :cond_2
    if-gt v0, v2, :cond_3

    if-gt v2, v1, :cond_3

    .line 204
    invoke-static {p1}, Lorg/apache/poi/ss/formula/FormulaShifter;->createDeletedRef(Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object p1

    goto :goto_0

    .line 206
    :cond_3
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Situation not covered: ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 207
    iget v5, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 206
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public adjustFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;I)Z
    .locals 4
    .param p1, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p2, "currentExternSheetIx"    # I

    .prologue
    .line 105
    const/4 v2, 0x0

    .line 106
    .local v2, "refsWereChanged":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-lt v0, v3, :cond_0

    .line 113
    return v2

    .line 107
    :cond_0
    aget-object v3, p1, v0

    invoke-direct {p0, v3, p2}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustPtg(Lorg/apache/poi/ss/formula/ptg/Ptg;I)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    .line 108
    .local v1, "newPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    if-eqz v1, :cond_1

    .line 109
    const/4 v2, 0x1

    .line 110
    aput-object v1, p1, v0

    .line 106
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 91
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    const-string/jumbo v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_firstMovedIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 94
    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_lastMovedIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 95
    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaShifter;->_amountToMove:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
