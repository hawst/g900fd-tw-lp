.class public final Lorg/apache/poi/ss/formula/eval/BoolEval;
.super Ljava/lang/Object;
.source "BoolEval.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/eval/NumericValueEval;
.implements Lorg/apache/poi/ss/formula/eval/StringValueEval;


# static fields
.field public static final FALSE:Lorg/apache/poi/ss/formula/eval/BoolEval;

.field public static final TRUE:Lorg/apache/poi/ss/formula/eval/BoolEval;


# instance fields
.field private _value:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lorg/apache/poi/ss/formula/eval/BoolEval;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/eval/BoolEval;-><init>(Z)V

    sput-object v0, Lorg/apache/poi/ss/formula/eval/BoolEval;->FALSE:Lorg/apache/poi/ss/formula/eval/BoolEval;

    .line 29
    new-instance v0, Lorg/apache/poi/ss/formula/eval/BoolEval;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/eval/BoolEval;-><init>(Z)V

    sput-object v0, Lorg/apache/poi/ss/formula/eval/BoolEval;->TRUE:Lorg/apache/poi/ss/formula/eval/BoolEval;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean p1, p0, Lorg/apache/poi/ss/formula/eval/BoolEval;->_value:Z

    .line 43
    return-void
.end method

.method public static final valueOf(Z)Lorg/apache/poi/ss/formula/eval/BoolEval;
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 38
    if-eqz p0, :cond_0

    sget-object v0, Lorg/apache/poi/ss/formula/eval/BoolEval;->TRUE:Lorg/apache/poi/ss/formula/eval/BoolEval;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/poi/ss/formula/eval/BoolEval;->FALSE:Lorg/apache/poi/ss/formula/eval/BoolEval;

    goto :goto_0
.end method


# virtual methods
.method public getBooleanValue()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/eval/BoolEval;->_value:Z

    return v0
.end method

.method public getNumberValue()D
    .locals 2

    .prologue
    .line 50
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/eval/BoolEval;->_value:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-double v0, v0

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/eval/BoolEval;->_value:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "TRUE"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "FALSE"

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 59
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/eval/BoolEval;->getStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
