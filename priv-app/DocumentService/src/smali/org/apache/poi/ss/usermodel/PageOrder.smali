.class public final enum Lorg/apache/poi/ss/usermodel/PageOrder;
.super Ljava/lang/Enum;
.source "PageOrder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/PageOrder;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOWN_THEN_OVER:Lorg/apache/poi/ss/usermodel/PageOrder;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PageOrder;

.field public static final enum OVER_THEN_DOWN:Lorg/apache/poi/ss/usermodel/PageOrder;

.field private static _table:[Lorg/apache/poi/ss/usermodel/PageOrder;


# instance fields
.field private order:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 27
    new-instance v2, Lorg/apache/poi/ss/usermodel/PageOrder;

    const-string/jumbo v3, "DOWN_THEN_OVER"

    .line 30
    invoke-direct {v2, v3, v1, v4}, Lorg/apache/poi/ss/usermodel/PageOrder;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PageOrder;->DOWN_THEN_OVER:Lorg/apache/poi/ss/usermodel/PageOrder;

    .line 31
    new-instance v2, Lorg/apache/poi/ss/usermodel/PageOrder;

    const-string/jumbo v3, "OVER_THEN_DOWN"

    .line 34
    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/PageOrder;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PageOrder;->OVER_THEN_DOWN:Lorg/apache/poi/ss/usermodel/PageOrder;

    new-array v2, v5, [Lorg/apache/poi/ss/usermodel/PageOrder;

    sget-object v3, Lorg/apache/poi/ss/usermodel/PageOrder;->DOWN_THEN_OVER:Lorg/apache/poi/ss/usermodel/PageOrder;

    aput-object v3, v2, v1

    sget-object v3, Lorg/apache/poi/ss/usermodel/PageOrder;->OVER_THEN_DOWN:Lorg/apache/poi/ss/usermodel/PageOrder;

    aput-object v3, v2, v4

    sput-object v2, Lorg/apache/poi/ss/usermodel/PageOrder;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PageOrder;

    .line 49
    const/4 v2, 0x3

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/PageOrder;

    sput-object v2, Lorg/apache/poi/ss/usermodel/PageOrder;->_table:[Lorg/apache/poi/ss/usermodel/PageOrder;

    .line 51
    invoke-static {}, Lorg/apache/poi/ss/usermodel/PageOrder;->values()[Lorg/apache/poi/ss/usermodel/PageOrder;

    move-result-object v2

    array-length v3, v2

    .local v0, "c":Lorg/apache/poi/ss/usermodel/PageOrder;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 54
    return-void

    .line 51
    :cond_0
    aget-object v0, v2, v1

    .line 52
    sget-object v4, Lorg/apache/poi/ss/usermodel/PageOrder;->_table:[Lorg/apache/poi/ss/usermodel/PageOrder;

    invoke-virtual {v0}, Lorg/apache/poi/ss/usermodel/PageOrder;->getValue()I

    move-result v5

    aput-object v0, v4, v5

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "order"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lorg/apache/poi/ss/usermodel/PageOrder;->order:I

    .line 42
    return-void
.end method

.method public static valueOf(I)Lorg/apache/poi/ss/usermodel/PageOrder;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 57
    sget-object v0, Lorg/apache/poi/ss/usermodel/PageOrder;->_table:[Lorg/apache/poi/ss/usermodel/PageOrder;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/PageOrder;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/PageOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/PageOrder;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/PageOrder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/PageOrder;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PageOrder;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/PageOrder;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lorg/apache/poi/ss/usermodel/PageOrder;->order:I

    return v0
.end method
