.class Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;
.super Ljava/lang/Object;
.source "FractionFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/usermodel/FractionFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleFraction"
.end annotation


# instance fields
.field private final denom:I

.field private final num:I

.field final synthetic this$0:Lorg/apache/poi/ss/usermodel/FractionFormat;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ss/usermodel/FractionFormat;II)V
    .locals 0
    .param p2, "num"    # I
    .param p3, "denom"    # I

    .prologue
    .line 259
    iput-object p1, p0, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->this$0:Lorg/apache/poi/ss/usermodel/FractionFormat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput p2, p0, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->num:I

    .line 261
    iput p3, p0, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->denom:I

    .line 262
    return-void
.end method


# virtual methods
.method public getDenominator()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->denom:I

    return v0
.end method

.method public getNumerator()I
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->num:I

    return v0
.end method
