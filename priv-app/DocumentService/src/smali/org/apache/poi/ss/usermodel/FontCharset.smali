.class public final enum Lorg/apache/poi/ss/usermodel/FontCharset;
.super Ljava/lang/Enum;
.source "FontCharset.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/FontCharset;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ANSI:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum ARABIC:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum BALTIC:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum CHINESEBIG5:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum DEFAULT:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum EASTEUROPE:Lorg/apache/poi/ss/usermodel/FontCharset;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum GB2312:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum GREEK:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum HANGEUL:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum HEBREW:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum JOHAB:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum MAC:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum OEM:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum RUSSIAN:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum SHIFTJIS:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum SYMBOL:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum THAI:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum TURKISH:Lorg/apache/poi/ss/usermodel/FontCharset;

.field public static final enum VIETNAMESE:Lorg/apache/poi/ss/usermodel/FontCharset;

.field private static _table:[Lorg/apache/poi/ss/usermodel/FontCharset;


# instance fields
.field private charset:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 29
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "ANSI"

    invoke-direct {v2, v3, v1, v1}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->ANSI:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 30
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "DEFAULT"

    invoke-direct {v2, v3, v6, v6}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->DEFAULT:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 31
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "SYMBOL"

    invoke-direct {v2, v3, v7, v7}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->SYMBOL:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 32
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "MAC"

    const/16 v4, 0x4d

    invoke-direct {v2, v3, v8, v4}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->MAC:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 33
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "SHIFTJIS"

    const/16 v4, 0x80

    invoke-direct {v2, v3, v9, v4}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->SHIFTJIS:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 34
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "HANGEUL"

    const/4 v4, 0x5

    const/16 v5, 0x81

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->HANGEUL:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 35
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "JOHAB"

    const/4 v4, 0x6

    const/16 v5, 0x82

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->JOHAB:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 36
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "GB2312"

    const/4 v4, 0x7

    const/16 v5, 0x86

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->GB2312:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 37
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "CHINESEBIG5"

    const/16 v4, 0x8

    const/16 v5, 0x88

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->CHINESEBIG5:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 38
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "GREEK"

    const/16 v4, 0x9

    const/16 v5, 0xa1

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->GREEK:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 39
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "TURKISH"

    const/16 v4, 0xa

    const/16 v5, 0xa2

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->TURKISH:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 40
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "VIETNAMESE"

    const/16 v4, 0xb

    const/16 v5, 0xa3

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->VIETNAMESE:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 41
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "HEBREW"

    const/16 v4, 0xc

    const/16 v5, 0xb1

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->HEBREW:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 42
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "ARABIC"

    const/16 v4, 0xd

    const/16 v5, 0xb2

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->ARABIC:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 43
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "BALTIC"

    const/16 v4, 0xe

    const/16 v5, 0xba

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->BALTIC:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 44
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "RUSSIAN"

    const/16 v4, 0xf

    const/16 v5, 0xcc

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->RUSSIAN:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 45
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "THAI"

    const/16 v4, 0x10

    const/16 v5, 0xde

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->THAI:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 46
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "EASTEUROPE"

    const/16 v4, 0x11

    const/16 v5, 0xee

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->EASTEUROPE:Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 47
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontCharset;

    const-string/jumbo v3, "OEM"

    const/16 v4, 0x12

    const/16 v5, 0xff

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/FontCharset;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->OEM:Lorg/apache/poi/ss/usermodel/FontCharset;

    const/16 v2, 0x13

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/FontCharset;

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontCharset;->ANSI:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v3, v2, v1

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontCharset;->DEFAULT:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v3, v2, v6

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontCharset;->SYMBOL:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v3, v2, v7

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontCharset;->MAC:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v3, v2, v8

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontCharset;->SHIFTJIS:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->HANGEUL:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->JOHAB:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->GB2312:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->CHINESEBIG5:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->GREEK:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->TURKISH:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->VIETNAMESE:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->HEBREW:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->ARABIC:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->BALTIC:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->RUSSIAN:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->THAI:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->EASTEUROPE:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->OEM:Lorg/apache/poi/ss/usermodel/FontCharset;

    aput-object v4, v2, v3

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 65
    const/16 v2, 0x100

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/FontCharset;

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontCharset;->_table:[Lorg/apache/poi/ss/usermodel/FontCharset;

    .line 67
    invoke-static {}, Lorg/apache/poi/ss/usermodel/FontCharset;->values()[Lorg/apache/poi/ss/usermodel/FontCharset;

    move-result-object v2

    array-length v3, v2

    .local v0, "c":Lorg/apache/poi/ss/usermodel/FontCharset;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 70
    return-void

    .line 67
    :cond_0
    aget-object v0, v2, v1

    .line 68
    sget-object v4, Lorg/apache/poi/ss/usermodel/FontCharset;->_table:[Lorg/apache/poi/ss/usermodel/FontCharset;

    invoke-virtual {v0}, Lorg/apache/poi/ss/usermodel/FontCharset;->getValue()I

    move-result v5

    aput-object v0, v4, v5

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lorg/apache/poi/ss/usermodel/FontCharset;->charset:I

    .line 54
    return-void
.end method

.method public static valueOf(I)Lorg/apache/poi/ss/usermodel/FontCharset;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 73
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontCharset;->_table:[Lorg/apache/poi/ss/usermodel/FontCharset;

    array-length v0, v0

    if-lt p0, v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontCharset;->_table:[Lorg/apache/poi/ss/usermodel/FontCharset;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/FontCharset;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/FontCharset;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/FontCharset;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/FontCharset;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontCharset;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontCharset;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/FontCharset;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/poi/ss/usermodel/FontCharset;->charset:I

    return v0
.end method
