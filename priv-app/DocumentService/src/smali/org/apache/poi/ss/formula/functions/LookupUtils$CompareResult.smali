.class public final Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;
.super Ljava/lang/Object;
.source "LookupUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/functions/LookupUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CompareResult"
.end annotation


# static fields
.field public static final EQUAL:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

.field public static final GREATER_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

.field public static final LESS_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

.field public static final TYPE_MISMATCH:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;


# instance fields
.field private final _isEqual:Z

.field private final _isGreaterThan:Z

.field private final _isLessThan:Z

.field private final _isTypeMismatch:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 159
    new-instance v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    invoke-direct {v0, v3, v2}, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;-><init>(ZI)V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->TYPE_MISMATCH:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    .line 160
    new-instance v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    const/4 v1, -0x1

    invoke-direct {v0, v2, v1}, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;-><init>(ZI)V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->LESS_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    .line 161
    new-instance v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    invoke-direct {v0, v2, v2}, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;-><init>(ZI)V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->EQUAL:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    .line 162
    new-instance v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    invoke-direct {v0, v2, v3}, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;-><init>(ZI)V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->GREATER_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    return-void
.end method

.method private constructor <init>(ZI)V
    .locals 3
    .param p1, "isTypeMismatch"    # Z
    .param p2, "simpleCompareResult"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    if-eqz p1, :cond_0

    .line 148
    iput-boolean v1, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isTypeMismatch:Z

    .line 149
    iput-boolean v2, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isLessThan:Z

    .line 150
    iput-boolean v2, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isEqual:Z

    .line 151
    iput-boolean v2, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isGreaterThan:Z

    .line 158
    :goto_0
    return-void

    .line 153
    :cond_0
    iput-boolean v2, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isTypeMismatch:Z

    .line 154
    if-gez p2, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isLessThan:Z

    .line 155
    if-nez p2, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isEqual:Z

    .line 156
    if-lez p2, :cond_3

    :goto_3
    iput-boolean v1, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isGreaterThan:Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 154
    goto :goto_1

    :cond_2
    move v0, v2

    .line 155
    goto :goto_2

    :cond_3
    move v1, v2

    .line 156
    goto :goto_3
.end method

.method private formatAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isTypeMismatch:Z

    if-eqz v0, :cond_0

    .line 204
    const-string/jumbo v0, "TYPE_MISMATCH"

    .line 216
    :goto_0
    return-object v0

    .line 206
    :cond_0
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isLessThan:Z

    if-eqz v0, :cond_1

    .line 207
    const-string/jumbo v0, "LESS_THAN"

    goto :goto_0

    .line 209
    :cond_1
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isEqual:Z

    if-eqz v0, :cond_2

    .line 210
    const-string/jumbo v0, "EQUAL"

    goto :goto_0

    .line 212
    :cond_2
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isGreaterThan:Z

    if-eqz v0, :cond_3

    .line 213
    const-string/jumbo v0, "GREATER_THAN"

    goto :goto_0

    .line 216
    :cond_3
    const-string/jumbo v0, "??error??"

    goto :goto_0
.end method

.method public static final valueOf(I)Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;
    .locals 1
    .param p0, "simpleCompareResult"    # I

    .prologue
    .line 165
    if-gez p0, :cond_0

    .line 166
    sget-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->LESS_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    .line 171
    :goto_0
    return-object v0

    .line 168
    :cond_0
    if-lez p0, :cond_1

    .line 169
    sget-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->GREATER_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    goto :goto_0

    .line 171
    :cond_1
    sget-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->EQUAL:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    goto :goto_0
.end method

.method public static final valueOf(Z)Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;
    .locals 1
    .param p0, "matches"    # Z

    .prologue
    .line 175
    if-eqz p0, :cond_0

    .line 176
    sget-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->EQUAL:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    .line 178
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->LESS_THAN:Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;

    goto :goto_0
.end method


# virtual methods
.method public isEqual()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isEqual:Z

    return v0
.end method

.method public isGreaterThan()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isGreaterThan:Z

    return v0
.end method

.method public isLessThan()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isLessThan:Z

    return v0
.end method

.method public isTypeMismatch()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->_isTypeMismatch:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 195
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 196
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 197
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/functions/LookupUtils$CompareResult;->formatAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 198
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
