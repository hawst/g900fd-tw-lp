.class final Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;
.super Ljava/lang/Object;
.source "ForkedEvaluationSheet.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RowColKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final _columnIndex:I

.field private final _rowIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    const-class v0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "rowIndex"    # I
    .param p2, "columnIndex"    # I

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput p1, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    .line 110
    iput p2, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    .line 111
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;

    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->compareTo(Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;)I
    .locals 3
    .param p1, "o"    # Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;

    .prologue
    .line 123
    iget v1, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    iget v2, p1, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    sub-int v0, v1, v2

    .line 124
    .local v0, "cmp":I
    if-eqz v0, :cond_0

    .line 127
    .end local v0    # "cmp":I
    :goto_0
    return v0

    .restart local v0    # "cmp":I
    :cond_0
    iget v1, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    iget v2, p1, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    sub-int v0, v1, v2

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 114
    sget-boolean v1, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    const-string/jumbo v2, "these private cache key instances are only compared to themselves"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_0
    move-object v0, p1

    .line 115
    check-cast v0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;

    .line 116
    .local v0, "other":Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;
    iget v1, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    iget v2, v0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    iget v2, v0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getColumnIndex()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    return v0
.end method

.method public getRowIndex()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_rowIndex:I

    iget v1, p0, Lorg/apache/poi/ss/formula/eval/forked/ForkedEvaluationSheet$RowColKey;->_columnIndex:I

    xor-int/2addr v0, v1

    return v0
.end method
