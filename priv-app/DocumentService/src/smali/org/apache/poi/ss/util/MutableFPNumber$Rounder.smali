.class final Lorg/apache/poi/ss/util/MutableFPNumber$Rounder;
.super Ljava/lang/Object;
.source "MutableFPNumber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/util/MutableFPNumber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Rounder"
.end annotation


# static fields
.field private static final HALF_BITS:[Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 136
    const/16 v4, 0x21

    new-array v2, v4, [Ljava/math/BigInteger;

    .line 137
    .local v2, "bis":[Ljava/math/BigInteger;
    const-wide/16 v0, 0x1

    .line 138
    .local v0, "acc":J
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-lt v3, v4, :cond_0

    .line 142
    sput-object v2, Lorg/apache/poi/ss/util/MutableFPNumber$Rounder;->HALF_BITS:[Ljava/math/BigInteger;

    .line 143
    return-void

    .line 139
    :cond_0
    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    aput-object v4, v2, v3

    .line 140
    const/4 v4, 0x1

    shl-long/2addr v0, v4

    .line 138
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static round(Ljava/math/BigInteger;I)Ljava/math/BigInteger;
    .locals 1
    .param p0, "bi"    # Ljava/math/BigInteger;
    .param p1, "nBits"    # I

    .prologue
    .line 148
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 151
    .end local p0    # "bi":Ljava/math/BigInteger;
    :goto_0
    return-object p0

    .restart local p0    # "bi":Ljava/math/BigInteger;
    :cond_0
    sget-object v0, Lorg/apache/poi/ss/util/MutableFPNumber$Rounder;->HALF_BITS:[Ljava/math/BigInteger;

    aget-object v0, v0, p1

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p0

    goto :goto_0
.end method
