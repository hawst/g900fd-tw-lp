.class public Lorg/apache/poi/ss/formula/FormulaRenderer;
.super Ljava/lang/Object;
.source "FormulaRenderer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getOperands(Ljava/util/Stack;I)[Ljava/lang/String;
    .locals 5
    .param p1, "nOperands"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;I)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    new-array v2, p1, [Ljava/lang/String;

    .line 121
    .local v2, "operands":[Ljava/lang/String;
    add-int/lit8 v0, p1, -0x1

    .local v0, "j":I
    :goto_0
    if-gez v0, :cond_0

    .line 129
    return-object v2

    .line 122
    :cond_0
    invoke-virtual {p0}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Too few arguments supplied to operation. Expected ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 124
    const-string/jumbo v4, ") operands but got ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-int v4, p1, v0

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 123
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    .local v1, "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 127
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v2, v0

    .line 121
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static toFormulaString(Lorg/apache/poi/ss/formula/FormulaRenderingWorkbook;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Ljava/lang/String;
    .locals 12
    .param p0, "book"    # Lorg/apache/poi/ss/formula/FormulaRenderingWorkbook;
    .param p1, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 47
    if-eqz p1, :cond_0

    array-length v9, p1

    if-nez v9, :cond_1

    .line 48
    :cond_0
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "ptgs must not be null"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 50
    :cond_1
    new-instance v8, Ljava/util/Stack;

    invoke-direct {v8}, Ljava/util/Stack;-><init>()V

    .line 52
    .local v8, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, p1

    if-lt v2, v9, :cond_2

    .line 104
    invoke-virtual {v8}, Ljava/util/Stack;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 107
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "Stack underflow"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 53
    :cond_2
    aget-object v6, p1, v2

    .line 55
    .local v6, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v9, v6, Lorg/apache/poi/ss/formula/ptg/MemAreaPtg;

    if-nez v9, :cond_3

    instance-of v9, v6, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;

    if-nez v9, :cond_3

    instance-of v9, v6, Lorg/apache/poi/ss/formula/ptg/MemErrPtg;

    if-eqz v9, :cond_4

    .line 52
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 61
    :cond_4
    instance-of v9, v6, Lorg/apache/poi/ss/formula/ptg/ParenthesisPtg;

    if-eqz v9, :cond_5

    .line 62
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 63
    .local v1, "contents":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 66
    .end local v1    # "contents":Ljava/lang/String;
    :cond_5
    instance-of v9, v6, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    if-eqz v9, :cond_7

    move-object v0, v6

    .line 67
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    .line 68
    .local v0, "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isOptimizedIf()Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isOptimizedChoose()Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isSkip()Z

    move-result v9

    if-nez v9, :cond_3

    .line 71
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isSpace()Z

    move-result v9

    if-nez v9, :cond_3

    .line 78
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isSemiVolatile()Z

    move-result v9

    if-nez v9, :cond_3

    .line 82
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->isSum()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 83
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getNumberOfOperands()I

    move-result v9

    invoke-static {v8, v9}, Lorg/apache/poi/ss/formula/FormulaRenderer;->getOperands(Ljava/util/Stack;I)[Ljava/lang/String;

    move-result-object v4

    .line 84
    .local v4, "operands":[Ljava/lang/String;
    invoke-virtual {v0, v4}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->toFormulaString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 87
    .end local v4    # "operands":[Ljava/lang/String;
    :cond_6
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Unexpected tAttr: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 90
    .end local v0    # "attrPtg":Lorg/apache/poi/ss/formula/ptg/AttrPtg;
    :cond_7
    instance-of v9, v6, Lorg/apache/poi/ss/formula/WorkbookDependentFormula;

    if-eqz v9, :cond_8

    move-object v5, v6

    .line 91
    check-cast v5, Lorg/apache/poi/ss/formula/WorkbookDependentFormula;

    .line 92
    .local v5, "optg":Lorg/apache/poi/ss/formula/WorkbookDependentFormula;
    invoke-interface {v5, p0}, Lorg/apache/poi/ss/formula/WorkbookDependentFormula;->toFormulaString(Lorg/apache/poi/ss/formula/FormulaRenderingWorkbook;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 95
    .end local v5    # "optg":Lorg/apache/poi/ss/formula/WorkbookDependentFormula;
    :cond_8
    instance-of v9, v6, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    if-nez v9, :cond_9

    .line 96
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/Ptg;->toFormulaString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_9
    move-object v3, v6

    .line 100
    check-cast v3, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    .line 101
    .local v3, "o":Lorg/apache/poi/ss/formula/ptg/OperationPtg;
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/ptg/OperationPtg;->getNumberOfOperands()I

    move-result v9

    invoke-static {v8, v9}, Lorg/apache/poi/ss/formula/FormulaRenderer;->getOperands(Ljava/util/Stack;I)[Ljava/lang/String;

    move-result-object v4

    .line 102
    .restart local v4    # "operands":[Ljava/lang/String;
    invoke-virtual {v3, v4}, Lorg/apache/poi/ss/formula/ptg/OperationPtg;->toFormulaString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 109
    .end local v3    # "o":Lorg/apache/poi/ss/formula/ptg/OperationPtg;
    .end local v4    # "operands":[Ljava/lang/String;
    .end local v6    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_a
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 110
    .local v7, "result":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/Stack;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_b

    .line 113
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "too much stuff left on the stack"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 115
    :cond_b
    return-object v7
.end method
