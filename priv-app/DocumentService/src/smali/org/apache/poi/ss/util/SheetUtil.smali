.class public Lorg/apache/poi/ss/util/SheetUtil;
.super Ljava/lang/Object;
.source "SheetUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static containsCell(Lorg/apache/poi/ss/util/CellRangeAddress;II)Z
    .locals 1
    .param p0, "cr"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p1, "rowIx"    # I
    .param p2, "colIx"    # I

    .prologue
    .line 142
    invoke-virtual {p0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v0

    if-gt v0, p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 143
    invoke-virtual {p0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v0

    if-gt v0, p2, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v0

    if-lt v0, p2, :cond_0

    .line 145
    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCellWidth(Lorg/apache/poi/ss/usermodel/Cell;ILorg/apache/poi/ss/usermodel/DataFormatter;Z)D
    .locals 2
    .param p0, "cell"    # Lorg/apache/poi/ss/usermodel/Cell;
    .param p1, "defaultCharWidth"    # I
    .param p2, "formatter"    # Lorg/apache/poi/ss/usermodel/DataFormatter;
    .param p3, "useMergedCells"    # Z

    .prologue
    .line 85
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 87
    .local v0, "width":D
    return-wide v0
.end method

.method public static getColumnWidth(Lorg/apache/poi/ss/usermodel/Sheet;IZ)D
    .locals 2
    .param p0, "sheet"    # Lorg/apache/poi/ss/usermodel/Sheet;
    .param p1, "column"    # I
    .param p2, "useMergedCells"    # Z

    .prologue
    .line 100
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 102
    .local v0, "width":D
    return-wide v0
.end method

.method public static getColumnWidth(Lorg/apache/poi/ss/usermodel/Sheet;IZII)D
    .locals 2
    .param p0, "sheet"    # Lorg/apache/poi/ss/usermodel/Sheet;
    .param p1, "column"    # I
    .param p2, "useMergedCells"    # Z
    .param p3, "firstRow"    # I
    .param p4, "lastRow"    # I

    .prologue
    .line 117
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 119
    .local v0, "width":D
    return-wide v0
.end method
