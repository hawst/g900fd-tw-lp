.class public final enum Lorg/apache/poi/ss/util/CellReference$NameType;
.super Ljava/lang/Enum;
.source "CellReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/util/CellReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NameType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/util/CellReference$NameType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BAD_CELL_OR_NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

.field public static final enum CELL:Lorg/apache/poi/ss/util/CellReference$NameType;

.field public static final enum COLUMN:Lorg/apache/poi/ss/util/CellReference$NameType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/util/CellReference$NameType;

.field public static final enum NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

.field public static final enum ROW:Lorg/apache/poi/ss/util/CellReference$NameType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    const-string/jumbo v1, "CELL"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/util/CellReference$NameType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->CELL:Lorg/apache/poi/ss/util/CellReference$NameType;

    .line 39
    new-instance v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    const-string/jumbo v1, "NAMED_RANGE"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/util/CellReference$NameType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    .line 40
    new-instance v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    const-string/jumbo v1, "COLUMN"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/ss/util/CellReference$NameType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->COLUMN:Lorg/apache/poi/ss/util/CellReference$NameType;

    .line 41
    new-instance v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    const-string/jumbo v1, "ROW"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/ss/util/CellReference$NameType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->ROW:Lorg/apache/poi/ss/util/CellReference$NameType;

    .line 42
    new-instance v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    const-string/jumbo v1, "BAD_CELL_OR_NAMED_RANGE"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/ss/util/CellReference$NameType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->BAD_CELL_OR_NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/poi/ss/util/CellReference$NameType;

    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->CELL:Lorg/apache/poi/ss/util/CellReference$NameType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->COLUMN:Lorg/apache/poi/ss/util/CellReference$NameType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->ROW:Lorg/apache/poi/ss/util/CellReference$NameType;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->BAD_CELL_OR_NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->ENUM$VALUES:[Lorg/apache/poi/ss/util/CellReference$NameType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/util/CellReference$NameType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/util/CellReference$NameType;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/util/CellReference$NameType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/util/CellReference$NameType;->ENUM$VALUES:[Lorg/apache/poi/ss/util/CellReference$NameType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
