.class public final Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;
.super Ljava/lang/Object;
.source "FunctionMetadataRegistry.java"


# static fields
.field public static final FUNCTION_INDEX_CHOOSE:I = 0x64

.field public static final FUNCTION_INDEX_EXTERNAL:S = 0xffs

.field public static final FUNCTION_INDEX_IF:I = 0x1

.field public static final FUNCTION_INDEX_INDIRECT:S = 0x94s

.field public static final FUNCTION_INDEX_SUM:S = 0x4s

.field public static final FUNCTION_NAME_IF:Ljava/lang/String; = "IF"

.field private static _instance:Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;


# instance fields
.field private final _functionDataByIndex:[Lorg/apache/poi/ss/formula/function/FunctionMetadata;

.field private final _functionDataByName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/ss/formula/function/FunctionMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>([Lorg/apache/poi/ss/formula/function/FunctionMetadata;Ljava/util/Map;)V
    .locals 0
    .param p1, "functionDataByIndex"    # [Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/poi/ss/formula/function/FunctionMetadata;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/ss/formula/function/FunctionMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "functionDataByName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/poi/ss/formula/function/FunctionMetadata;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_functionDataByIndex:[Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    .line 53
    iput-object p2, p0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_functionDataByName:Ljava/util/Map;

    .line 54
    return-void
.end method

.method public static getFunctionByIndex(I)Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 62
    invoke-static {}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getInstance()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    move-result-object v0

    invoke-direct {v0, p0}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getFunctionByIndexInternal(I)Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    move-result-object v0

    return-object v0
.end method

.method private getFunctionByIndexInternal(I)Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_functionDataByIndex:[Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public static getFunctionByName(Ljava/lang/String;)Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-static {}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getInstance()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    move-result-object v0

    invoke-direct {v0, p0}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getFunctionByNameInternal(Ljava/lang/String;)Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    move-result-object v0

    return-object v0
.end method

.method private getFunctionByNameInternal(Ljava/lang/String;)Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_functionDataByName:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    return-object v0
.end method

.method private static getInstance()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_instance:Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lorg/apache/poi/ss/formula/function/FunctionMetadataReader;->createRegistry()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_instance:Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    .line 48
    :cond_0
    sget-object v0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_instance:Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    return-object v0
.end method

.method public static lookupIndexByName(Ljava/lang/String;)S
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getInstance()Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;

    move-result-object v1

    invoke-direct {v1, p0}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getFunctionByNameInternal(Ljava/lang/String;)Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    move-result-object v0

    .line 76
    .local v0, "fd":Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    if-nez v0, :cond_0

    .line 77
    const/4 v1, -0x1

    .line 79
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getIndex()I

    move-result v1

    int-to-short v1, v1

    goto :goto_0
.end method


# virtual methods
.method getAllFunctionNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->_functionDataByName:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
