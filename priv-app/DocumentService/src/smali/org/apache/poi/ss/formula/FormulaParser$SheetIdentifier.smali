.class final Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;
.super Ljava/lang/Object;
.source "FormulaParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/FormulaParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SheetIdentifier"
.end annotation


# instance fields
.field private final _bookName:Ljava/lang/String;

.field private final _sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/poi/ss/formula/FormulaParser$Identifier;)V
    .locals 0
    .param p1, "bookName"    # Ljava/lang/String;
    .param p2, "sheetIdentifier"    # Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_bookName:Ljava/lang/String;

    .line 93
    iput-object p2, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    .line 94
    return-void
.end method


# virtual methods
.method public getBookName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_bookName:Ljava/lang/String;

    return-object v0
.end method

.method public getSheetIdentifier()Lorg/apache/poi/ss/formula/FormulaParser$Identifier;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 103
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    const-string/jumbo v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_bookName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 106
    const-string/jumbo v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 108
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;->isQuoted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    :goto_0
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 111
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->_sheetIdentifier:Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
