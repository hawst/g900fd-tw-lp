.class public Lorg/apache/poi/ss/formula/functions/EDate;
.super Ljava/lang/Object;
.source "EDate.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/functions/FreeRefFunction;


# static fields
.field public static final instance:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lorg/apache/poi/ss/formula/functions/EDate;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/functions/EDate;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/EDate;->instance:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getValue(Lorg/apache/poi/ss/formula/eval/ValueEval;)D
    .locals 4
    .param p1, "arg"    # Lorg/apache/poi/ss/formula/eval/ValueEval;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/ss/formula/eval/EvaluationException;
        }
    .end annotation

    .prologue
    .line 53
    instance-of v1, p1, Lorg/apache/poi/ss/formula/eval/NumberEval;

    if-eqz v1, :cond_0

    .line 54
    check-cast p1, Lorg/apache/poi/ss/formula/eval/NumberEval;

    .end local p1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/eval/NumberEval;->getNumberValue()D

    move-result-wide v2

    .line 58
    :goto_0
    return-wide v2

    .line 56
    .restart local p1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_0
    instance-of v1, p1, Lorg/apache/poi/ss/formula/eval/RefEval;

    if-eqz v1, :cond_1

    .line 57
    check-cast p1, Lorg/apache/poi/ss/formula/eval/RefEval;

    .end local p1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/eval/RefEval;->getInnerValueEval()Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    .line 58
    .local v0, "innerValueEval":Lorg/apache/poi/ss/formula/eval/ValueEval;
    check-cast v0, Lorg/apache/poi/ss/formula/eval/NumberEval;

    .end local v0    # "innerValueEval":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/eval/NumberEval;->getNumberValue()D

    move-result-wide v2

    goto :goto_0

    .line 60
    .restart local p1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_1
    new-instance v1, Lorg/apache/poi/ss/formula/eval/EvaluationException;

    sget-object v2, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    invoke-direct {v1, v2}, Lorg/apache/poi/ss/formula/eval/EvaluationException;-><init>(Lorg/apache/poi/ss/formula/eval/ErrorEval;)V

    throw v1
.end method


# virtual methods
.method public evaluate([Lorg/apache/poi/ss/formula/eval/ValueEval;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 10
    .param p1, "args"    # [Lorg/apache/poi/ss/formula/eval/ValueEval;
    .param p2, "ec"    # Lorg/apache/poi/ss/formula/OperationEvaluationContext;

    .prologue
    const/4 v8, 0x2

    .line 34
    array-length v5, p1

    if-eq v5, v8, :cond_0

    .line 35
    sget-object v5, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    .line 48
    :goto_0
    return-object v5

    .line 38
    :cond_0
    const/4 v5, 0x0

    :try_start_0
    aget-object v5, p1, v5

    invoke-direct {p0, v5}, Lorg/apache/poi/ss/formula/functions/EDate;->getValue(Lorg/apache/poi/ss/formula/eval/ValueEval;)D

    move-result-wide v6

    .line 39
    .local v6, "startDateAsNumber":D
    const/4 v5, 0x1

    aget-object v3, p1, v5

    check-cast v3, Lorg/apache/poi/ss/formula/eval/NumberEval;

    .line 40
    .local v3, "offsetInYearsValue":Lorg/apache/poi/ss/formula/eval/NumberEval;
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/eval/NumberEval;->getNumberValue()D

    move-result-wide v8

    double-to-int v2, v8

    .line 42
    .local v2, "offsetInMonthAsNumber":I
    invoke-static {v6, v7}, Lorg/apache/poi/ss/usermodel/DateUtil;->getJavaDate(D)Ljava/util/Date;

    move-result-object v4

    .line 43
    .local v4, "startDate":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 44
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 45
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v2}, Ljava/util/Calendar;->add(II)V

    .line 46
    new-instance v5, Lorg/apache/poi/ss/formula/eval/NumberEval;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/poi/ss/usermodel/DateUtil;->getExcelDate(Ljava/util/Date;)D

    move-result-wide v8

    invoke-direct {v5, v8, v9}, Lorg/apache/poi/ss/formula/eval/NumberEval;-><init>(D)V
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 47
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v2    # "offsetInMonthAsNumber":I
    .end local v3    # "offsetInYearsValue":Lorg/apache/poi/ss/formula/eval/NumberEval;
    .end local v4    # "startDate":Ljava/util/Date;
    .end local v6    # "startDateAsNumber":D
    :catch_0
    move-exception v1

    .line 48
    .local v1, "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v5

    goto :goto_0
.end method
