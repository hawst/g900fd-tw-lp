.class public final enum Lorg/apache/poi/ss/usermodel/PrintCellComments;
.super Ljava/lang/Enum;
.source "PrintCellComments.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/PrintCellComments;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AS_DISPLAYED:Lorg/apache/poi/ss/usermodel/PrintCellComments;

.field public static final enum AT_END:Lorg/apache/poi/ss/usermodel/PrintCellComments;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PrintCellComments;

.field public static final enum NONE:Lorg/apache/poi/ss/usermodel/PrintCellComments;

.field private static _table:[Lorg/apache/poi/ss/usermodel/PrintCellComments;


# instance fields
.field private comments:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 27
    new-instance v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;

    const-string/jumbo v3, "NONE"

    .line 30
    invoke-direct {v2, v3, v1, v4}, Lorg/apache/poi/ss/usermodel/PrintCellComments;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;->NONE:Lorg/apache/poi/ss/usermodel/PrintCellComments;

    .line 31
    new-instance v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;

    const-string/jumbo v3, "AS_DISPLAYED"

    .line 34
    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/PrintCellComments;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;->AS_DISPLAYED:Lorg/apache/poi/ss/usermodel/PrintCellComments;

    .line 35
    new-instance v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;

    const-string/jumbo v3, "AT_END"

    .line 38
    invoke-direct {v2, v3, v5, v6}, Lorg/apache/poi/ss/usermodel/PrintCellComments;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;->AT_END:Lorg/apache/poi/ss/usermodel/PrintCellComments;

    new-array v2, v6, [Lorg/apache/poi/ss/usermodel/PrintCellComments;

    sget-object v3, Lorg/apache/poi/ss/usermodel/PrintCellComments;->NONE:Lorg/apache/poi/ss/usermodel/PrintCellComments;

    aput-object v3, v2, v1

    sget-object v3, Lorg/apache/poi/ss/usermodel/PrintCellComments;->AS_DISPLAYED:Lorg/apache/poi/ss/usermodel/PrintCellComments;

    aput-object v3, v2, v4

    sget-object v3, Lorg/apache/poi/ss/usermodel/PrintCellComments;->AT_END:Lorg/apache/poi/ss/usermodel/PrintCellComments;

    aput-object v3, v2, v5

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PrintCellComments;

    .line 51
    const/4 v2, 0x4

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/PrintCellComments;

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintCellComments;->_table:[Lorg/apache/poi/ss/usermodel/PrintCellComments;

    .line 53
    invoke-static {}, Lorg/apache/poi/ss/usermodel/PrintCellComments;->values()[Lorg/apache/poi/ss/usermodel/PrintCellComments;

    move-result-object v2

    array-length v3, v2

    .local v0, "c":Lorg/apache/poi/ss/usermodel/PrintCellComments;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 56
    return-void

    .line 53
    :cond_0
    aget-object v0, v2, v1

    .line 54
    sget-object v4, Lorg/apache/poi/ss/usermodel/PrintCellComments;->_table:[Lorg/apache/poi/ss/usermodel/PrintCellComments;

    invoke-virtual {v0}, Lorg/apache/poi/ss/usermodel/PrintCellComments;->getValue()I

    move-result v5

    aput-object v0, v4, v5

    .line 53
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "comments"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, Lorg/apache/poi/ss/usermodel/PrintCellComments;->comments:I

    .line 45
    return-void
.end method

.method public static valueOf(I)Lorg/apache/poi/ss/usermodel/PrintCellComments;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 59
    sget-object v0, Lorg/apache/poi/ss/usermodel/PrintCellComments;->_table:[Lorg/apache/poi/ss/usermodel/PrintCellComments;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/PrintCellComments;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/PrintCellComments;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/PrintCellComments;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/PrintCellComments;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/PrintCellComments;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PrintCellComments;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/PrintCellComments;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/poi/ss/usermodel/PrintCellComments;->comments:I

    return v0
.end method
