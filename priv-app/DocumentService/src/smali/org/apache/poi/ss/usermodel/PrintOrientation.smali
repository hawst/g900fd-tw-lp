.class public final enum Lorg/apache/poi/ss/usermodel/PrintOrientation;
.super Ljava/lang/Enum;
.source "PrintOrientation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/PrintOrientation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEFAULT:Lorg/apache/poi/ss/usermodel/PrintOrientation;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PrintOrientation;

.field public static final enum LANDSCAPE:Lorg/apache/poi/ss/usermodel/PrintOrientation;

.field public static final enum PORTRAIT:Lorg/apache/poi/ss/usermodel/PrintOrientation;

.field private static _table:[Lorg/apache/poi/ss/usermodel/PrintOrientation;


# instance fields
.field private orientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 27
    new-instance v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;

    const-string/jumbo v3, "DEFAULT"

    .line 30
    invoke-direct {v2, v3, v1, v4}, Lorg/apache/poi/ss/usermodel/PrintOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;->DEFAULT:Lorg/apache/poi/ss/usermodel/PrintOrientation;

    .line 31
    new-instance v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;

    const-string/jumbo v3, "PORTRAIT"

    .line 34
    invoke-direct {v2, v3, v4, v5}, Lorg/apache/poi/ss/usermodel/PrintOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;->PORTRAIT:Lorg/apache/poi/ss/usermodel/PrintOrientation;

    .line 35
    new-instance v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;

    const-string/jumbo v3, "LANDSCAPE"

    .line 38
    invoke-direct {v2, v3, v5, v6}, Lorg/apache/poi/ss/usermodel/PrintOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;->LANDSCAPE:Lorg/apache/poi/ss/usermodel/PrintOrientation;

    new-array v2, v6, [Lorg/apache/poi/ss/usermodel/PrintOrientation;

    sget-object v3, Lorg/apache/poi/ss/usermodel/PrintOrientation;->DEFAULT:Lorg/apache/poi/ss/usermodel/PrintOrientation;

    aput-object v3, v2, v1

    sget-object v3, Lorg/apache/poi/ss/usermodel/PrintOrientation;->PORTRAIT:Lorg/apache/poi/ss/usermodel/PrintOrientation;

    aput-object v3, v2, v4

    sget-object v3, Lorg/apache/poi/ss/usermodel/PrintOrientation;->LANDSCAPE:Lorg/apache/poi/ss/usermodel/PrintOrientation;

    aput-object v3, v2, v5

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PrintOrientation;

    .line 53
    const/4 v2, 0x4

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/PrintOrientation;

    sput-object v2, Lorg/apache/poi/ss/usermodel/PrintOrientation;->_table:[Lorg/apache/poi/ss/usermodel/PrintOrientation;

    .line 55
    invoke-static {}, Lorg/apache/poi/ss/usermodel/PrintOrientation;->values()[Lorg/apache/poi/ss/usermodel/PrintOrientation;

    move-result-object v2

    array-length v3, v2

    .local v0, "c":Lorg/apache/poi/ss/usermodel/PrintOrientation;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 58
    return-void

    .line 55
    :cond_0
    aget-object v0, v2, v1

    .line 56
    sget-object v4, Lorg/apache/poi/ss/usermodel/PrintOrientation;->_table:[Lorg/apache/poi/ss/usermodel/PrintOrientation;

    invoke-virtual {v0}, Lorg/apache/poi/ss/usermodel/PrintOrientation;->getValue()I

    move-result v5

    aput-object v0, v4, v5

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "orientation"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, Lorg/apache/poi/ss/usermodel/PrintOrientation;->orientation:I

    .line 45
    return-void
.end method

.method public static valueOf(I)Lorg/apache/poi/ss/usermodel/PrintOrientation;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 61
    sget-object v0, Lorg/apache/poi/ss/usermodel/PrintOrientation;->_table:[Lorg/apache/poi/ss/usermodel/PrintOrientation;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/PrintOrientation;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/PrintOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/PrintOrientation;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/PrintOrientation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/PrintOrientation;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/PrintOrientation;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/PrintOrientation;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/poi/ss/usermodel/PrintOrientation;->orientation:I

    return v0
.end method
