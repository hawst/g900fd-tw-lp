.class public final Lorg/apache/poi/ss/formula/functions/T;
.super Lorg/apache/poi/ss/formula/functions/Fixed1ArgFunction;
.source "T.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/functions/Fixed1ArgFunction;-><init>()V

    return-void
.end method


# virtual methods
.method public evaluate(IILorg/apache/poi/ss/formula/eval/ValueEval;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 3
    .param p1, "srcRowIndex"    # I
    .param p2, "srcColumnIndex"    # I
    .param p3, "arg0"    # Lorg/apache/poi/ss/formula/eval/ValueEval;

    .prologue
    const/4 v2, 0x0

    .line 36
    move-object v0, p3

    .line 37
    .local v0, "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    instance-of v1, v0, Lorg/apache/poi/ss/formula/eval/RefEval;

    if-eqz v1, :cond_2

    .line 38
    check-cast v0, Lorg/apache/poi/ss/formula/eval/RefEval;

    .end local v0    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-interface {v0}, Lorg/apache/poi/ss/formula/eval/RefEval;->getInnerValueEval()Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    .line 44
    .restart local v0    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_0
    :goto_0
    instance-of v1, v0, Lorg/apache/poi/ss/formula/eval/StringEval;

    if-eqz v1, :cond_3

    .line 54
    .end local v0    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_1
    :goto_1
    return-object v0

    .line 39
    .restart local v0    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_2
    instance-of v1, v0, Lorg/apache/poi/ss/formula/eval/AreaEval;

    if-eqz v1, :cond_0

    .line 41
    check-cast v0, Lorg/apache/poi/ss/formula/eval/AreaEval;

    .end local v0    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-interface {v0, v2, v2}, Lorg/apache/poi/ss/formula/eval/AreaEval;->getRelativeValue(II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v0

    .restart local v0    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    goto :goto_0

    .line 49
    :cond_3
    instance-of v1, v0, Lorg/apache/poi/ss/formula/eval/ErrorEval;

    if-nez v1, :cond_1

    .line 54
    sget-object v0, Lorg/apache/poi/ss/formula/eval/StringEval;->EMPTY_INSTANCE:Lorg/apache/poi/ss/formula/eval/StringEval;

    goto :goto_1
.end method
