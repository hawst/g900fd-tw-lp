.class Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
.super Ljava/lang/Object;
.source "CellNumberFormatter.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/format/CellNumberFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StringMod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;",
        ">;"
    }
.end annotation


# static fields
.field public static final AFTER:I = 0x2

.field public static final BEFORE:I = 0x1

.field public static final REPLACE:I = 0x3


# instance fields
.field end:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field endInclusive:Z

.field final op:I

.field final special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field startInclusive:Z

.field toAdd:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)V
    .locals 0
    .param p1, "special"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p2, "toAdd"    # Ljava/lang/CharSequence;
    .param p3, "op"    # I

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 141
    iput-object p2, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    .line 142
    iput p3, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    .line 143
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;ILorg/apache/poi/ss/format/CellNumberFormatter$StringMod;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)V
    .locals 1
    .param p1, "start"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p2, "startInclusive"    # Z
    .param p3, "end"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p4, "endInclusive"    # Z

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 154
    iput-boolean p2, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->startInclusive:Z

    .line 155
    iput-object p3, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->end:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 156
    iput-boolean p4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->endInclusive:Z

    .line 157
    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    .line 158
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    .line 159
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;ZC)V
    .locals 2
    .param p1, "start"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p2, "startInclusive"    # Z
    .param p3, "end"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p4, "endInclusive"    # Z
    .param p5, "toAdd"    # C

    .prologue
    .line 147
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)V

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    .line 149
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->compareTo(Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;)I
    .locals 3
    .param p1, "that"    # Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    .prologue
    .line 162
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iget v1, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    iget-object v2, p1, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iget v2, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    sub-int v0, v1, v2

    .line 163
    .local v0, "diff":I
    if-eqz v0, :cond_0

    .line 166
    .end local v0    # "diff":I
    :goto_0
    return v0

    .restart local v0    # "diff":I
    :cond_0
    iget v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    iget v2, p1, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    sub-int v0, v1, v2

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 172
    :try_start_0
    check-cast p1, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    .end local p1    # "that":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->compareTo(Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 175
    :cond_0
    :goto_0
    return v1

    .line 173
    :catch_0
    move-exception v0

    .line 175
    .local v0, "ignored":Ljava/lang/RuntimeException;
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    add-int/2addr v0, v1

    return v0
.end method
