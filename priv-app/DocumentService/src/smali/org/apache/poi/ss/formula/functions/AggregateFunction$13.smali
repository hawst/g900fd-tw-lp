.class Lorg/apache/poi/ss/formula/functions/AggregateFunction$13;
.super Lorg/apache/poi/ss/formula/functions/AggregateFunction;
.source "AggregateFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/ss/formula/functions/AggregateFunction;->subtotalInstance(Lorg/apache/poi/ss/formula/functions/Function;)Lorg/apache/poi/ss/formula/functions/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$arg:Lorg/apache/poi/ss/formula/functions/AggregateFunction;


# direct methods
.method constructor <init>(Lorg/apache/poi/ss/formula/functions/AggregateFunction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/poi/ss/formula/functions/AggregateFunction$13;->val$arg:Lorg/apache/poi/ss/formula/functions/AggregateFunction;

    .line 100
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/functions/AggregateFunction;-><init>()V

    return-void
.end method


# virtual methods
.method protected evaluate([D)D
    .locals 2
    .param p1, "values"    # [D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/ss/formula/eval/EvaluationException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/poi/ss/formula/functions/AggregateFunction$13;->val$arg:Lorg/apache/poi/ss/formula/functions/AggregateFunction;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/formula/functions/AggregateFunction;->evaluate([D)D

    move-result-wide v0

    return-wide v0
.end method

.method public isSubtotalCounted()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method
