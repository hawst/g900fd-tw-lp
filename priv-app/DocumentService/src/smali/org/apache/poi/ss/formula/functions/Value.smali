.class public final Lorg/apache/poi/ss/formula/functions/Value;
.super Lorg/apache/poi/ss/formula/functions/Fixed1ArgFunction;
.source "Value.java"


# static fields
.field private static final MIN_DISTANCE_BETWEEN_THOUSANDS_SEPARATOR:I = 0x4

.field private static final ZERO:Ljava/lang/Double;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/formula/functions/Value;->ZERO:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/functions/Fixed1ArgFunction;-><init>()V

    return-void
.end method

.method private static convertTextToNumber(Ljava/lang/String;)Ljava/lang/Double;
    .locals 18
    .param p0, "strText"    # Ljava/lang/String;

    .prologue
    .line 65
    const/4 v7, 0x0

    .line 66
    .local v7, "foundCurrency":Z
    const/4 v10, 0x0

    .line 67
    .local v10, "foundUnaryPlus":Z
    const/4 v9, 0x0

    .line 69
    .local v9, "foundUnaryMinus":Z
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v13

    .line 71
    .local v13, "len":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-lt v11, v13, :cond_2

    .line 104
    :cond_0
    if-lt v11, v13, :cond_9

    .line 106
    if-nez v7, :cond_1

    if-nez v9, :cond_1

    if-eqz v10, :cond_8

    .line 107
    :cond_1
    const/16 v16, 0x0

    .line 182
    :goto_1
    return-object v16

    .line 72
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 73
    .local v2, "ch":C
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v16

    if-nez v16, :cond_0

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-eq v2, v0, :cond_0

    .line 76
    sparse-switch v2, :sswitch_data_0

    .line 101
    const/16 v16, 0x0

    goto :goto_1

    .line 81
    :sswitch_0
    if-eqz v7, :cond_3

    .line 83
    const/16 v16, 0x0

    goto :goto_1

    .line 85
    :cond_3
    const/4 v7, 0x1

    .line 71
    :goto_2
    :sswitch_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 88
    :sswitch_2
    if-nez v9, :cond_4

    if-eqz v10, :cond_5

    .line 89
    :cond_4
    const/16 v16, 0x0

    goto :goto_1

    .line 91
    :cond_5
    const/4 v10, 0x1

    .line 92
    goto :goto_2

    .line 94
    :sswitch_3
    if-nez v9, :cond_6

    if-eqz v10, :cond_7

    .line 95
    :cond_6
    const/16 v16, 0x0

    goto :goto_1

    .line 97
    :cond_7
    const/4 v9, 0x1

    .line 98
    goto :goto_2

    .line 109
    .end local v2    # "ch":C
    :cond_8
    sget-object v16, Lorg/apache/poi/ss/formula/functions/Value;->ZERO:Ljava/lang/Double;

    goto :goto_1

    .line 114
    :cond_9
    const/4 v8, 0x0

    .line 115
    .local v8, "foundDecimalPoint":Z
    const/16 v12, -0x8000

    .line 117
    .local v12, "lastThousandsSeparatorIndex":I
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15, v13}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 118
    .local v15, "sb":Ljava/lang/StringBuffer;
    :goto_3
    if-lt v11, v13, :cond_a

    .line 170
    if-nez v8, :cond_12

    .line 171
    sub-int v16, v11, v12

    const/16 v17, 0x4

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_12

    .line 172
    const/16 v16, 0x0

    goto :goto_1

    .line 119
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 120
    .restart local v2    # "ch":C
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 121
    invoke-virtual {v15, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 118
    :cond_b
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 124
    :cond_c
    sparse-switch v2, :sswitch_data_1

    .line 167
    const/16 v16, 0x0

    goto :goto_1

    .line 126
    :sswitch_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 127
    .local v14, "remainingText":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_b

    .line 129
    const/16 v16, 0x0

    goto :goto_1

    .line 133
    .end local v14    # "remainingText":Ljava/lang/String;
    :sswitch_5
    if-eqz v8, :cond_d

    .line 134
    const/16 v16, 0x0

    goto :goto_1

    .line 136
    :cond_d
    sub-int v16, v11, v12

    const/16 v17, 0x4

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_e

    .line 137
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 139
    :cond_e
    const/4 v8, 0x1

    .line 140
    const/16 v16, 0x2e

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 143
    :sswitch_6
    if-eqz v8, :cond_f

    .line 145
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 147
    :cond_f
    sub-int v3, v11, v12

    .line 149
    .local v3, "distanceBetweenThousandsSeparators":I
    const/16 v16, 0x4

    move/from16 v0, v16

    if-ge v3, v0, :cond_10

    .line 150
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 152
    :cond_10
    move v12, v11

    .line 154
    goto :goto_4

    .line 158
    .end local v3    # "distanceBetweenThousandsSeparators":I
    :sswitch_7
    sub-int v16, v11, v12

    const/16 v17, 0x4

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_11

    .line 159
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 162
    :cond_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    move v11, v13

    .line 164
    goto :goto_4

    .line 177
    .end local v2    # "ch":C
    :cond_12
    :try_start_0
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 182
    .local v4, "d":D
    if-eqz v9, :cond_13

    neg-double v4, v4

    .end local v4    # "d":D
    :cond_13
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    goto/16 :goto_1

    .line 178
    :catch_0
    move-exception v6

    .line 180
    .local v6, "e":Ljava/lang/NumberFormatException;
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x24 -> :sswitch_0
        0x2b -> :sswitch_2
        0x2d -> :sswitch_3
    .end sparse-switch

    .line 124
    :sswitch_data_1
    .sparse-switch
        0x20 -> :sswitch_4
        0x2c -> :sswitch_6
        0x2e -> :sswitch_5
        0x45 -> :sswitch_7
        0x65 -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method public evaluate(IILorg/apache/poi/ss/formula/eval/ValueEval;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 8
    .param p1, "srcRowIndex"    # I
    .param p2, "srcColumnIndex"    # I
    .param p3, "arg0"    # Lorg/apache/poi/ss/formula/eval/ValueEval;

    .prologue
    .line 47
    :try_start_0
    invoke-static {p3, p1, p2}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->getSingleValue(Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 51
    .local v3, "veText":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-static {v3}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->coerceValueToString(Lorg/apache/poi/ss/formula/eval/ValueEval;)Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "strText":Ljava/lang/String;
    invoke-static {v2}, Lorg/apache/poi/ss/formula/functions/Value;->convertTextToNumber(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    .line 53
    .local v1, "result":Ljava/lang/Double;
    if-nez v1, :cond_0

    .line 54
    sget-object v4, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    .line 56
    .end local v1    # "result":Ljava/lang/Double;
    .end local v2    # "strText":Ljava/lang/String;
    .end local v3    # "veText":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :goto_0
    return-object v4

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v4

    goto :goto_0

    .line 56
    .end local v0    # "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    .restart local v1    # "result":Ljava/lang/Double;
    .restart local v2    # "strText":Ljava/lang/String;
    .restart local v3    # "veText":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_0
    new-instance v4, Lorg/apache/poi/ss/formula/eval/NumberEval;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Lorg/apache/poi/ss/formula/eval/NumberEval;-><init>(D)V

    goto :goto_0
.end method
