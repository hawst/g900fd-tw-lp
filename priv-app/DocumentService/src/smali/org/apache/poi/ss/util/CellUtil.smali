.class public final Lorg/apache/poi/ss/util/CellUtil;
.super Ljava/lang/Object;
.source "CellUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;
    }
.end annotation


# static fields
.field public static final ALIGNMENT:Ljava/lang/String; = "alignment"

.field public static final BORDER_BOTTOM:Ljava/lang/String; = "borderBottom"

.field public static final BORDER_LEFT:Ljava/lang/String; = "borderLeft"

.field public static final BORDER_RIGHT:Ljava/lang/String; = "borderRight"

.field public static final BORDER_TOP:Ljava/lang/String; = "borderTop"

.field public static final BOTTOM_BORDER_COLOR:Ljava/lang/String; = "bottomBorderColor"

.field public static final DATA_FORMAT:Ljava/lang/String; = "dataFormat"

.field public static final FILL_BACKGROUND_COLOR:Ljava/lang/String; = "fillBackgroundColor"

.field public static final FILL_FOREGROUND_COLOR:Ljava/lang/String; = "fillForegroundColor"

.field public static final FILL_PATTERN:Ljava/lang/String; = "fillPattern"

.field public static final FONT:Ljava/lang/String; = "font"

.field public static final HIDDEN:Ljava/lang/String; = "hidden"

.field public static final INDENTION:Ljava/lang/String; = "indention"

.field public static final LEFT_BORDER_COLOR:Ljava/lang/String; = "leftBorderColor"

.field public static final LOCKED:Ljava/lang/String; = "locked"

.field public static final RIGHT_BORDER_COLOR:Ljava/lang/String; = "rightBorderColor"

.field public static final ROTATION:Ljava/lang/String; = "rotation"

.field public static final TOP_BORDER_COLOR:Ljava/lang/String; = "topBorderColor"

.field public static final VERTICAL_ALIGNMENT:Ljava/lang/String; = "verticalAlignment"

.field public static final WRAP_TEXT:Ljava/lang/String; = "wrapText"

.field private static unicodeMappings:[Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 358
    const/16 v0, 0xf

    new-array v0, v0, [Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    const/4 v1, 0x0

    .line 359
    const-string/jumbo v2, "alpha"

    const-string/jumbo v3, "\u03b1"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 360
    const-string/jumbo v2, "beta"

    const-string/jumbo v3, "\u03b2"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 361
    const-string/jumbo v2, "gamma"

    const-string/jumbo v3, "\u03b3"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 362
    const-string/jumbo v2, "delta"

    const-string/jumbo v3, "\u03b4"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 363
    const-string/jumbo v2, "epsilon"

    const-string/jumbo v3, "\u03b5"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 364
    const-string/jumbo v2, "zeta"

    const-string/jumbo v3, "\u03b6"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 365
    const-string/jumbo v2, "eta"

    const-string/jumbo v3, "\u03b7"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 366
    const-string/jumbo v2, "theta"

    const-string/jumbo v3, "\u03b8"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 367
    const-string/jumbo v2, "iota"

    const-string/jumbo v3, "\u03b9"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 368
    const-string/jumbo v2, "kappa"

    const-string/jumbo v3, "\u03ba"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 369
    const-string/jumbo v2, "lambda"

    const-string/jumbo v3, "\u03bb"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 370
    const-string/jumbo v2, "mu"

    const-string/jumbo v3, "\u03bc"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 371
    const-string/jumbo v2, "nu"

    const-string/jumbo v3, "\u03bd"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 372
    const-string/jumbo v2, "xi"

    const-string/jumbo v3, "\u03be"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 373
    const-string/jumbo v2, "omicron"

    const-string/jumbo v3, "\u03bf"

    invoke-static {v2, v3}, Lorg/apache/poi/ss/util/CellUtil;->um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    move-result-object v2

    aput-object v2, v0, v1

    .line 358
    sput-object v0, Lorg/apache/poi/ss/util/CellUtil;->unicodeMappings:[Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    .line 375
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    return-void
.end method

.method public static createCell(Lorg/apache/poi/ss/usermodel/Row;ILjava/lang/String;)Lorg/apache/poi/ss/usermodel/Cell;
    .locals 1
    .param p0, "row"    # Lorg/apache/poi/ss/usermodel/Row;
    .param p1, "column"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->createCell(Lorg/apache/poi/ss/usermodel/Row;ILjava/lang/String;Lorg/apache/poi/ss/usermodel/CellStyle;)Lorg/apache/poi/ss/usermodel/Cell;

    move-result-object v0

    return-object v0
.end method

.method public static createCell(Lorg/apache/poi/ss/usermodel/Row;ILjava/lang/String;Lorg/apache/poi/ss/usermodel/CellStyle;)Lorg/apache/poi/ss/usermodel/Cell;
    .locals 2
    .param p0, "row"    # Lorg/apache/poi/ss/usermodel/Row;
    .param p1, "column"    # I
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "style"    # Lorg/apache/poi/ss/usermodel/CellStyle;

    .prologue
    .line 124
    invoke-static {p0, p1}, Lorg/apache/poi/ss/util/CellUtil;->getCell(Lorg/apache/poi/ss/usermodel/Row;I)Lorg/apache/poi/ss/usermodel/Cell;

    move-result-object v0

    .line 126
    .local v0, "cell":Lorg/apache/poi/ss/usermodel/Cell;
    invoke-interface {v0}, Lorg/apache/poi/ss/usermodel/Cell;->getRow()Lorg/apache/poi/ss/usermodel/Row;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/poi/ss/usermodel/Row;->getSheet()Lorg/apache/poi/ss/usermodel/Sheet;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/poi/ss/usermodel/Sheet;->getWorkbook()Lorg/apache/poi/ss/usermodel/Workbook;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/poi/ss/usermodel/Workbook;->getCreationHelper()Lorg/apache/poi/ss/usermodel/CreationHelper;

    move-result-object v1

    .line 127
    invoke-interface {v1, p2}, Lorg/apache/poi/ss/usermodel/CreationHelper;->createRichTextString(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/RichTextString;

    move-result-object v1

    .line 126
    invoke-interface {v0, v1}, Lorg/apache/poi/ss/usermodel/Cell;->setCellValue(Lorg/apache/poi/ss/usermodel/RichTextString;)V

    .line 128
    if-eqz p3, :cond_0

    .line 129
    invoke-interface {v0, p3}, Lorg/apache/poi/ss/usermodel/Cell;->setCellStyle(Lorg/apache/poi/ss/usermodel/CellStyle;)V

    .line 131
    :cond_0
    return-object v0
.end method

.method private static getBoolean(Ljava/util/Map;Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 300
    .local p0, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 301
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 302
    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 304
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getCell(Lorg/apache/poi/ss/usermodel/Row;I)Lorg/apache/poi/ss/usermodel/Cell;
    .locals 1
    .param p0, "row"    # Lorg/apache/poi/ss/usermodel/Row;
    .param p1, "columnIndex"    # I

    .prologue
    .line 105
    invoke-interface {p0, p1}, Lorg/apache/poi/ss/usermodel/Row;->getCell(I)Lorg/apache/poi/ss/usermodel/Cell;

    move-result-object v0

    .line 107
    .local v0, "cell":Lorg/apache/poi/ss/usermodel/Cell;
    if-nez v0, :cond_0

    .line 108
    invoke-interface {p0, p1}, Lorg/apache/poi/ss/usermodel/Row;->createCell(I)Lorg/apache/poi/ss/usermodel/Cell;

    move-result-object v0

    .line 110
    :cond_0
    return-object v0
.end method

.method private static getFormatProperties(Lorg/apache/poi/ss/usermodel/CellStyle;)Ljava/util/Map;
    .locals 3
    .param p0, "style"    # Lorg/apache/poi/ss/usermodel/CellStyle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/ss/usermodel/CellStyle;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 221
    .local v0, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "alignment"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getAlignment()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 222
    const-string/jumbo v1, "borderBottom"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderBottom()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 223
    const-string/jumbo v1, "borderLeft"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderLeft()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 224
    const-string/jumbo v1, "borderRight"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderRight()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 225
    const-string/jumbo v1, "borderTop"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderTop()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 226
    const-string/jumbo v1, "bottomBorderColor"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBottomBorderColor()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 227
    const-string/jumbo v1, "dataFormat"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getDataFormat()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 228
    const-string/jumbo v1, "fillBackgroundColor"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getFillBackgroundColor()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 229
    const-string/jumbo v1, "fillForegroundColor"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getFillForegroundColor()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 230
    const-string/jumbo v1, "fillPattern"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getFillPattern()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 231
    const-string/jumbo v1, "font"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getFontIndex()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 232
    const-string/jumbo v1, "hidden"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getHidden()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putBoolean(Ljava/util/Map;Ljava/lang/String;Z)V

    .line 233
    const-string/jumbo v1, "indention"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getIndention()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 234
    const-string/jumbo v1, "leftBorderColor"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getLeftBorderColor()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 235
    const-string/jumbo v1, "locked"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getLocked()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putBoolean(Ljava/util/Map;Ljava/lang/String;Z)V

    .line 236
    const-string/jumbo v1, "rightBorderColor"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getRightBorderColor()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 237
    const-string/jumbo v1, "rotation"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getRotation()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 238
    const-string/jumbo v1, "topBorderColor"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getTopBorderColor()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 239
    const-string/jumbo v1, "verticalAlignment"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getVerticalAlignment()S

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putShort(Ljava/util/Map;Ljava/lang/String;S)V

    .line 240
    const-string/jumbo v1, "wrapText"

    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/CellStyle;->getWrapText()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ss/util/CellUtil;->putBoolean(Ljava/util/Map;Ljava/lang/String;Z)V

    .line 241
    return-object v0
.end method

.method public static getRow(ILorg/apache/poi/ss/usermodel/Sheet;)Lorg/apache/poi/ss/usermodel/Row;
    .locals 1
    .param p0, "rowIndex"    # I
    .param p1, "sheet"    # Lorg/apache/poi/ss/usermodel/Sheet;

    .prologue
    .line 89
    invoke-interface {p1, p0}, Lorg/apache/poi/ss/usermodel/Sheet;->getRow(I)Lorg/apache/poi/ss/usermodel/Row;

    move-result-object v0

    .line 90
    .local v0, "row":Lorg/apache/poi/ss/usermodel/Row;
    if-nez v0, :cond_0

    .line 91
    invoke-interface {p1, p0}, Lorg/apache/poi/ss/usermodel/Sheet;->createRow(I)Lorg/apache/poi/ss/usermodel/Row;

    move-result-object v0

    .line 93
    :cond_0
    return-object v0
.end method

.method private static getShort(Ljava/util/Map;Ljava/lang/String;)S
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")S"
        }
    .end annotation

    .prologue
    .line 284
    .local p0, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 285
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Short;

    if-eqz v1, :cond_0

    .line 286
    check-cast v0, Ljava/lang/Short;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v1

    .line 288
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static putBoolean(Ljava/util/Map;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p0, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    return-void
.end method

.method private static putShort(Ljava/util/Map;Ljava/lang/String;S)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "S)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p0, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    return-void
.end method

.method public static setAlignment(Lorg/apache/poi/ss/usermodel/Cell;Lorg/apache/poi/ss/usermodel/Workbook;S)V
    .locals 2
    .param p0, "cell"    # Lorg/apache/poi/ss/usermodel/Cell;
    .param p1, "workbook"    # Lorg/apache/poi/ss/usermodel/Workbook;
    .param p2, "align"    # S

    .prologue
    .line 158
    const-string/jumbo v0, "alignment"

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lorg/apache/poi/ss/util/CellUtil;->setCellStyleProperty(Lorg/apache/poi/ss/usermodel/Cell;Lorg/apache/poi/ss/usermodel/Workbook;Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    return-void
.end method

.method public static setCellStyleProperty(Lorg/apache/poi/ss/usermodel/Cell;Lorg/apache/poi/ss/usermodel/Workbook;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .param p0, "cell"    # Lorg/apache/poi/ss/usermodel/Cell;
    .param p1, "workbook"    # Lorg/apache/poi/ss/usermodel/Workbook;
    .param p2, "propertyName"    # Ljava/lang/String;
    .param p3, "propertyValue"    # Ljava/lang/Object;

    .prologue
    .line 185
    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/Cell;->getCellStyle()Lorg/apache/poi/ss/usermodel/CellStyle;

    move-result-object v3

    .line 186
    .local v3, "originalStyle":Lorg/apache/poi/ss/usermodel/CellStyle;
    const/4 v1, 0x0

    .line 187
    .local v1, "newStyle":Lorg/apache/poi/ss/usermodel/CellStyle;
    invoke-static {v3}, Lorg/apache/poi/ss/util/CellUtil;->getFormatProperties(Lorg/apache/poi/ss/usermodel/CellStyle;)Ljava/util/Map;

    move-result-object v4

    .line 188
    .local v4, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v4, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Workbook;->getNumCellStyles()S

    move-result v2

    .line 194
    .local v2, "numberCellStyles":S
    const/4 v0, 0x0

    .local v0, "i":S
    :goto_0
    if-lt v0, v2, :cond_1

    .line 204
    :goto_1
    if-nez v1, :cond_0

    .line 205
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Workbook;->createCellStyle()Lorg/apache/poi/ss/usermodel/CellStyle;

    move-result-object v1

    .line 206
    invoke-static {v1, p1, v4}, Lorg/apache/poi/ss/util/CellUtil;->setFormatProperties(Lorg/apache/poi/ss/usermodel/CellStyle;Lorg/apache/poi/ss/usermodel/Workbook;Ljava/util/Map;)V

    .line 209
    :cond_0
    invoke-interface {p0, v1}, Lorg/apache/poi/ss/usermodel/Cell;->setCellStyle(Lorg/apache/poi/ss/usermodel/CellStyle;)V

    .line 210
    return-void

    .line 195
    :cond_1
    invoke-interface {p1, v0}, Lorg/apache/poi/ss/usermodel/Workbook;->getCellStyleAt(S)Lorg/apache/poi/ss/usermodel/CellStyle;

    move-result-object v5

    .line 196
    .local v5, "wbStyle":Lorg/apache/poi/ss/usermodel/CellStyle;
    invoke-static {v5}, Lorg/apache/poi/ss/util/CellUtil;->getFormatProperties(Lorg/apache/poi/ss/usermodel/CellStyle;)Ljava/util/Map;

    move-result-object v6

    .line 198
    .local v6, "wbStyleMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v6, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 199
    move-object v1, v5

    .line 200
    goto :goto_1

    .line 194
    :cond_2
    add-int/lit8 v7, v0, 0x1

    int-to-short v0, v7

    goto :goto_0
.end method

.method public static setFont(Lorg/apache/poi/ss/usermodel/Cell;Lorg/apache/poi/ss/usermodel/Workbook;Lorg/apache/poi/ss/usermodel/Font;)V
    .locals 2
    .param p0, "cell"    # Lorg/apache/poi/ss/usermodel/Cell;
    .param p1, "workbook"    # Lorg/apache/poi/ss/usermodel/Workbook;
    .param p2, "font"    # Lorg/apache/poi/ss/usermodel/Font;

    .prologue
    .line 169
    const-string/jumbo v0, "font"

    invoke-interface {p2}, Lorg/apache/poi/ss/usermodel/Font;->getIndex()S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lorg/apache/poi/ss/util/CellUtil;->setCellStyleProperty(Lorg/apache/poi/ss/usermodel/Cell;Lorg/apache/poi/ss/usermodel/Workbook;Ljava/lang/String;Ljava/lang/Object;)V

    .line 170
    return-void
.end method

.method private static setFormatProperties(Lorg/apache/poi/ss/usermodel/CellStyle;Lorg/apache/poi/ss/usermodel/Workbook;Ljava/util/Map;)V
    .locals 1
    .param p0, "style"    # Lorg/apache/poi/ss/usermodel/CellStyle;
    .param p1, "workbook"    # Lorg/apache/poi/ss/usermodel/Workbook;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/ss/usermodel/CellStyle;",
            "Lorg/apache/poi/ss/usermodel/Workbook;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p2, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v0, "alignment"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setAlignment(S)V

    .line 254
    const-string/jumbo v0, "borderBottom"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setBorderBottom(S)V

    .line 255
    const-string/jumbo v0, "borderLeft"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setBorderLeft(S)V

    .line 256
    const-string/jumbo v0, "borderRight"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setBorderRight(S)V

    .line 257
    const-string/jumbo v0, "borderTop"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setBorderTop(S)V

    .line 258
    const-string/jumbo v0, "bottomBorderColor"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setBottomBorderColor(S)V

    .line 259
    const-string/jumbo v0, "dataFormat"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setDataFormat(S)V

    .line 260
    const-string/jumbo v0, "fillBackgroundColor"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setFillBackgroundColor(S)V

    .line 261
    const-string/jumbo v0, "fillForegroundColor"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setFillForegroundColor(S)V

    .line 262
    const-string/jumbo v0, "fillPattern"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setFillPattern(S)V

    .line 263
    const-string/jumbo v0, "font"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/ss/usermodel/Workbook;->getFontAt(S)Lorg/apache/poi/ss/usermodel/Font;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setFont(Lorg/apache/poi/ss/usermodel/Font;)V

    .line 264
    const-string/jumbo v0, "hidden"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getBoolean(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setHidden(Z)V

    .line 265
    const-string/jumbo v0, "indention"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setIndention(S)V

    .line 266
    const-string/jumbo v0, "leftBorderColor"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setLeftBorderColor(S)V

    .line 267
    const-string/jumbo v0, "locked"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getBoolean(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setLocked(Z)V

    .line 268
    const-string/jumbo v0, "rightBorderColor"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setRightBorderColor(S)V

    .line 269
    const-string/jumbo v0, "rotation"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setRotation(S)V

    .line 270
    const-string/jumbo v0, "topBorderColor"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setTopBorderColor(S)V

    .line 271
    const-string/jumbo v0, "verticalAlignment"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getShort(Ljava/util/Map;Ljava/lang/String;)S

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setVerticalAlignment(S)V

    .line 272
    const-string/jumbo v0, "wrapText"

    invoke-static {p2, v0}, Lorg/apache/poi/ss/util/CellUtil;->getBoolean(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    invoke-interface {p0, v0}, Lorg/apache/poi/ss/usermodel/CellStyle;->setWrapText(Z)V

    .line 273
    return-void
.end method

.method public static translateUnicodeValues(Lorg/apache/poi/ss/usermodel/Cell;)Lorg/apache/poi/ss/usermodel/Cell;
    .locals 8
    .param p0, "cell"    # Lorg/apache/poi/ss/usermodel/Cell;

    .prologue
    .line 338
    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/Cell;->getRichStringCellValue()Lorg/apache/poi/ss/usermodel/RichTextString;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/poi/ss/usermodel/RichTextString;->getString()Ljava/lang/String;

    move-result-object v5

    .line 339
    .local v5, "s":Ljava/lang/String;
    const/4 v1, 0x0

    .line 340
    .local v1, "foundUnicode":Z
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 342
    .local v4, "lowerCaseStr":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v6, Lorg/apache/poi/ss/util/CellUtil;->unicodeMappings:[Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    array-length v6, v6

    if-lt v2, v6, :cond_1

    .line 350
    if-eqz v1, :cond_0

    .line 351
    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/Cell;->getRow()Lorg/apache/poi/ss/usermodel/Row;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/poi/ss/usermodel/Row;->getSheet()Lorg/apache/poi/ss/usermodel/Sheet;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/poi/ss/usermodel/Sheet;->getWorkbook()Lorg/apache/poi/ss/usermodel/Workbook;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/poi/ss/usermodel/Workbook;->getCreationHelper()Lorg/apache/poi/ss/usermodel/CreationHelper;

    move-result-object v6

    .line 352
    invoke-interface {v6, v5}, Lorg/apache/poi/ss/usermodel/CreationHelper;->createRichTextString(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/RichTextString;

    move-result-object v6

    .line 351
    invoke-interface {p0, v6}, Lorg/apache/poi/ss/usermodel/Cell;->setCellValue(Lorg/apache/poi/ss/usermodel/RichTextString;)V

    .line 354
    :cond_0
    return-object p0

    .line 343
    :cond_1
    sget-object v6, Lorg/apache/poi/ss/util/CellUtil;->unicodeMappings:[Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    aget-object v0, v6, v2

    .line 344
    .local v0, "entry":Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;
    iget-object v3, v0, Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;->entityName:Ljava/lang/String;

    .line 345
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 346
    iget-object v6, v0, Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;->resolvedValue:Ljava/lang/String;

    invoke-virtual {v5, v3, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 347
    const/4 v1, 0x1

    .line 342
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static um(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;
    .locals 1
    .param p0, "entityName"    # Ljava/lang/String;
    .param p1, "resolvedValue"    # Ljava/lang/String;

    .prologue
    .line 378
    new-instance v0, Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;

    invoke-direct {v0, p0, p1}, Lorg/apache/poi/ss/util/CellUtil$UnicodeMapping;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
