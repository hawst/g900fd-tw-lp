.class public final Lorg/apache/poi/ss/formula/OperationEvaluationContext;
.super Ljava/lang/Object;
.source "OperationEvaluationContext.java"


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType:[I

.field public static final UDF:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;


# instance fields
.field private final _bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

.field private final _columnIndex:I

.field private final _rowIndex:I

.field private final _sheetIndex:I

.field private final _tracker:Lorg/apache/poi/ss/formula/EvaluationTracker;

.field private final _workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType()[I
    .locals 3

    .prologue
    .line 41
    sget-object v0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->$SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/ss/util/CellReference$NameType;->values()[Lorg/apache/poi/ss/util/CellReference$NameType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->BAD_CELL_OR_NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-virtual {v1}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->CELL:Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-virtual {v1}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->COLUMN:Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-virtual {v1}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-virtual {v1}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->ROW:Lorg/apache/poi/ss/util/CellReference$NameType;

    invoke-virtual {v1}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->$SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lorg/apache/poi/ss/formula/UserDefinedFunction;->instance:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    sput-object v0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->UDF:Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ss/formula/WorkbookEvaluator;Lorg/apache/poi/ss/formula/EvaluationWorkbook;IIILorg/apache/poi/ss/formula/EvaluationTracker;)V
    .locals 0
    .param p1, "bookEvaluator"    # Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    .param p2, "workbook"    # Lorg/apache/poi/ss/formula/EvaluationWorkbook;
    .param p3, "sheetIndex"    # I
    .param p4, "srcRowNum"    # I
    .param p5, "srcColNum"    # I
    .param p6, "tracker"    # Lorg/apache/poi/ss/formula/EvaluationTracker;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    .line 53
    iput-object p2, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    .line 54
    iput p3, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_sheetIndex:I

    .line 55
    iput p4, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_rowIndex:I

    .line 56
    iput p5, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_columnIndex:I

    .line 57
    iput-object p6, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_tracker:Lorg/apache/poi/ss/formula/EvaluationTracker;

    .line 58
    return-void
.end method

.method private static classifyCellReference(Ljava/lang/String;Lorg/apache/poi/ss/SpreadsheetVersion;)Lorg/apache/poi/ss/util/CellReference$NameType;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "ssVersion"    # Lorg/apache/poi/ss/SpreadsheetVersion;

    .prologue
    .line 233
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 234
    .local v0, "len":I
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 235
    sget-object v1, Lorg/apache/poi/ss/util/CellReference$NameType;->BAD_CELL_OR_NAMED_RANGE:Lorg/apache/poi/ss/util/CellReference$NameType;

    .line 237
    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0, p1}, Lorg/apache/poi/ss/util/CellReference;->classifyCellReference(Ljava/lang/String;Lorg/apache/poi/ss/SpreadsheetVersion;)Lorg/apache/poi/ss/util/CellReference$NameType;

    move-result-object v1

    goto :goto_0
.end method

.method private createExternSheetRefEvaluator(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .locals 5
    .param p1, "workbookName"    # Ljava/lang/String;
    .param p2, "sheetName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 105
    if-nez p1, :cond_0

    .line 106
    iget-object v2, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    .line 117
    .local v2, "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :goto_0
    if-nez p2, :cond_2

    iget v1, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_sheetIndex:I

    .line 118
    .local v1, "otherSheetIndex":I
    :goto_1
    if-gez v1, :cond_3

    .line 121
    .end local v1    # "otherSheetIndex":I
    .end local v2    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :goto_2
    return-object v3

    .line 108
    :cond_0
    if-nez p2, :cond_1

    .line 109
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "sheetName must not be null if workbookName is provided"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 112
    :cond_1
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    invoke-virtual {v4, p1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getOtherWorkbookEvaluator(Ljava/lang/String;)Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .restart local v2    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    goto :goto_0

    .line 113
    .end local v2    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;
    goto :goto_2

    .line 117
    .end local v0    # "e":Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;
    .restart local v2    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :cond_2
    invoke-virtual {v2, p2}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndex(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 121
    .restart local v1    # "otherSheetIndex":I
    :cond_3
    new-instance v3, Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    iget-object v4, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_tracker:Lorg/apache/poi/ss/formula/EvaluationTracker;

    invoke-direct {v3, v2, v4, v1}, Lorg/apache/poi/ss/formula/SheetRefEvaluator;-><init>(Lorg/apache/poi/ss/formula/WorkbookEvaluator;Lorg/apache/poi/ss/formula/EvaluationTracker;I)V

    goto :goto_2
.end method

.method private static parseColRef(Ljava/lang/String;)I
    .locals 1
    .param p0, "refStrPart"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private static parseRowRef(Ljava/lang/String;)I
    .locals 1
    .param p0, "refStrPart"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-static {p0}, Lorg/apache/poi/ss/util/CellReference;->convertColStringToIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method createExternSheetRefEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .locals 8
    .param p1, "externSheetIndex"    # I

    .prologue
    .line 76
    iget-object v5, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v5, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getExternalSheet(I)Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;

    move-result-object v1

    .line 79
    .local v1, "externalSheet":Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;
    if-nez v1, :cond_1

    .line 81
    iget-object v5, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-interface {v5, p1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->convertFromExternSheetIndex(I)I

    move-result v2

    .line 82
    .local v2, "otherSheetIndex":I
    iget-object v3, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    .line 97
    .local v3, "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :cond_0
    new-instance v5, Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    iget-object v6, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_tracker:Lorg/apache/poi/ss/formula/EvaluationTracker;

    invoke-direct {v5, v3, v6, v2}, Lorg/apache/poi/ss/formula/SheetRefEvaluator;-><init>(Lorg/apache/poi/ss/formula/WorkbookEvaluator;Lorg/apache/poi/ss/formula/EvaluationTracker;I)V

    return-object v5

    .line 85
    .end local v2    # "otherSheetIndex":I
    .end local v3    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;->getWorkbookName()Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "workbookName":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    invoke-virtual {v5, v4}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getOtherWorkbookEvaluator(Ljava/lang/String;)Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 91
    .restart local v3    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndex(Ljava/lang/String;)I

    move-result v2

    .line 92
    .restart local v2    # "otherSheetIndex":I
    if-gez v2, :cond_0

    .line 93
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Invalid sheet name \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 94
    const-string/jumbo v7, "\' in bool \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 93
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 88
    .end local v2    # "otherSheetIndex":I
    .end local v3    # "targetEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method createExternSheetRefEvaluator(Lorg/apache/poi/ss/formula/ExternSheetReferenceToken;)Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .locals 1
    .param p1, "ptg"    # Lorg/apache/poi/ss/formula/ExternSheetReferenceToken;

    .prologue
    .line 73
    invoke-interface {p1}, Lorg/apache/poi/ss/formula/ExternSheetReferenceToken;->getExternSheetIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->createExternSheetRefEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v0

    return-object v0
.end method

.method public findUserDefinedFunction(Ljava/lang/String;)Lorg/apache/poi/ss/formula/functions/FreeRefFunction;
    .locals 1
    .param p1, "functionName"    # Ljava/lang/String;

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->findUserDefinedFunction(Ljava/lang/String;)Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    move-result-object v0

    return-object v0
.end method

.method public getArea3DEval(IIIII)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 6
    .param p1, "firstRowIndex"    # I
    .param p2, "firstColumnIndex"    # I
    .param p3, "lastRowIndex"    # I
    .param p4, "lastColumnIndex"    # I
    .param p5, "extSheetIndex"    # I

    .prologue
    .line 259
    invoke-virtual {p0, p5}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->createExternSheetRefEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v5

    .line 260
    .local v5, "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    new-instance v0, Lorg/apache/poi/ss/formula/LazyAreaEval;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/formula/LazyAreaEval;-><init>(IIIILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    return-object v0
.end method

.method public getAreaEval(IIII)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 6
    .param p1, "firstRowIndex"    # I
    .param p2, "firstColumnIndex"    # I
    .param p3, "lastRowIndex"    # I
    .param p4, "lastColumnIndex"    # I

    .prologue
    .line 254
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRefEvaluatorForCurrentSheet()Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v5

    .line 255
    .local v5, "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    new-instance v0, Lorg/apache/poi/ss/formula/LazyAreaEval;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/formula/LazyAreaEval;-><init>(IIIILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    return-object v0
.end method

.method public getColumnIndex()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_columnIndex:I

    return v0
.end method

.method public getDynamicReference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 14
    .param p1, "workbookName"    # Ljava/lang/String;
    .param p2, "sheetName"    # Ljava/lang/String;
    .param p3, "refStrPart1"    # Ljava/lang/String;
    .param p4, "refStrPart2"    # Ljava/lang/String;
    .param p5, "isA1Style"    # Z

    .prologue
    .line 149
    if-nez p5, :cond_0

    .line 150
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v12, "R1C1 style not supported yet"

    invoke-direct {v1, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 152
    :cond_0
    invoke-direct/range {p0 .. p2}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->createExternSheetRefEvaluator(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v6

    .line 153
    .local v6, "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    if-nez v6, :cond_1

    .line 154
    sget-object v1, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    .line 221
    :goto_0
    return-object v1

    .line 157
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    check-cast v1, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    invoke-interface {v1}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getSpreadsheetVersion()Lorg/apache/poi/ss/SpreadsheetVersion;

    move-result-object v11

    .line 159
    .local v11, "ssVersion":Lorg/apache/poi/ss/SpreadsheetVersion;
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->classifyCellReference(Ljava/lang/String;Lorg/apache/poi/ss/SpreadsheetVersion;)Lorg/apache/poi/ss/util/CellReference$NameType;

    move-result-object v9

    .line 160
    .local v9, "part1refType":Lorg/apache/poi/ss/util/CellReference$NameType;
    invoke-static {}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->$SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType()[I

    move-result-object v1

    invoke-virtual {v9}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v12

    aget v1, v1, v12

    packed-switch v1, :pswitch_data_0

    .line 170
    :pswitch_0
    if-nez p4, :cond_3

    .line 172
    invoke-static {}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->$SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType()[I

    move-result-object v1

    invoke-virtual {v9}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v12

    aget v1, v1, v12

    packed-switch v1, :pswitch_data_1

    .line 180
    :pswitch_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Unexpected reference classification of \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 162
    :pswitch_2
    sget-object v1, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto :goto_0

    .line 164
    :pswitch_3
    iget-object v1, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    check-cast v1, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    iget v12, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_sheetIndex:I

    move-object/from16 v0, p3

    invoke-interface {v1, v0, v12}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v8

    .line 165
    .local v8, "nm":Lorg/apache/poi/ss/formula/EvaluationName;
    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->isRange()Z

    move-result v1

    if-nez v1, :cond_2

    .line 166
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Specified name \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\' is not a range as expected."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 168
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    invoke-interface {v8}, Lorg/apache/poi/ss/formula/EvaluationName;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v12

    invoke-virtual {v1, v12, p0}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->evaluateNameFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/OperationEvaluationContext;)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v1

    goto/16 :goto_0

    .line 175
    .end local v8    # "nm":Lorg/apache/poi/ss/formula/EvaluationName;
    :pswitch_4
    sget-object v1, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto/16 :goto_0

    .line 177
    :pswitch_5
    new-instance v7, Lorg/apache/poi/ss/util/CellReference;

    move-object/from16 v0, p3

    invoke-direct {v7, v0}, Lorg/apache/poi/ss/util/CellReference;-><init>(Ljava/lang/String;)V

    .line 178
    .local v7, "cr":Lorg/apache/poi/ss/util/CellReference;
    new-instance v1, Lorg/apache/poi/ss/formula/LazyRefEval;

    invoke-virtual {v7}, Lorg/apache/poi/ss/util/CellReference;->getRow()I

    move-result v12

    invoke-virtual {v7}, Lorg/apache/poi/ss/util/CellReference;->getCol()S

    move-result v13

    invoke-direct {v1, v12, v13, v6}, Lorg/apache/poi/ss/formula/LazyRefEval;-><init>(IILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    goto/16 :goto_0

    .line 182
    .end local v7    # "cr":Lorg/apache/poi/ss/util/CellReference;
    :cond_3
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->classifyCellReference(Ljava/lang/String;Lorg/apache/poi/ss/SpreadsheetVersion;)Lorg/apache/poi/ss/util/CellReference$NameType;

    move-result-object v10

    .line 183
    .local v10, "part2refType":Lorg/apache/poi/ss/util/CellReference$NameType;
    invoke-static {}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->$SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType()[I

    move-result-object v1

    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v12

    aget v1, v1, v12

    packed-switch v1, :pswitch_data_2

    .line 191
    :pswitch_6
    if-eq v10, v9, :cond_4

    .line 193
    sget-object v1, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto/16 :goto_0

    .line 185
    :pswitch_7
    sget-object v1, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto/16 :goto_0

    .line 187
    :pswitch_8
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Cannot evaluate \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 188
    const-string/jumbo v13, "\'. Indirect evaluation of defined names not supported yet"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 187
    invoke-direct {v1, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 196
    :cond_4
    invoke-static {}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->$SWITCH_TABLE$org$apache$poi$ss$util$CellReference$NameType()[I

    move-result-object v1

    invoke-virtual {v9}, Lorg/apache/poi/ss/util/CellReference$NameType;->ordinal()I

    move-result v12

    aget v1, v1, v12

    packed-switch v1, :pswitch_data_3

    .line 219
    :pswitch_9
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Unexpected reference classification of \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :pswitch_a
    const/4 v2, 0x0

    .line 199
    .local v2, "firstRow":I
    invoke-virtual {v11}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v4

    .line 200
    .local v4, "lastRow":I
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->parseColRef(Ljava/lang/String;)I

    move-result v3

    .line 201
    .local v3, "firstCol":I
    invoke-static/range {p4 .. p4}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->parseColRef(Ljava/lang/String;)I

    move-result v5

    .line 221
    .local v5, "lastCol":I
    :goto_1
    new-instance v1, Lorg/apache/poi/ss/formula/LazyAreaEval;

    invoke-direct/range {v1 .. v6}, Lorg/apache/poi/ss/formula/LazyAreaEval;-><init>(IIIILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    goto/16 :goto_0

    .line 204
    .end local v2    # "firstRow":I
    .end local v3    # "firstCol":I
    .end local v4    # "lastRow":I
    .end local v5    # "lastCol":I
    :pswitch_b
    const/4 v3, 0x0

    .line 205
    .restart local v3    # "firstCol":I
    invoke-virtual {v11}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastColumnIndex()I

    move-result v5

    .line 206
    .restart local v5    # "lastCol":I
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->parseRowRef(Ljava/lang/String;)I

    move-result v2

    .line 207
    .restart local v2    # "firstRow":I
    invoke-static/range {p4 .. p4}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->parseRowRef(Ljava/lang/String;)I

    move-result v4

    .line 208
    .restart local v4    # "lastRow":I
    goto :goto_1

    .line 211
    .end local v2    # "firstRow":I
    .end local v3    # "firstCol":I
    .end local v4    # "lastRow":I
    .end local v5    # "lastCol":I
    :pswitch_c
    new-instance v7, Lorg/apache/poi/ss/util/CellReference;

    move-object/from16 v0, p3

    invoke-direct {v7, v0}, Lorg/apache/poi/ss/util/CellReference;-><init>(Ljava/lang/String;)V

    .line 212
    .restart local v7    # "cr":Lorg/apache/poi/ss/util/CellReference;
    invoke-virtual {v7}, Lorg/apache/poi/ss/util/CellReference;->getRow()I

    move-result v2

    .line 213
    .restart local v2    # "firstRow":I
    invoke-virtual {v7}, Lorg/apache/poi/ss/util/CellReference;->getCol()S

    move-result v3

    .line 214
    .restart local v3    # "firstCol":I
    new-instance v7, Lorg/apache/poi/ss/util/CellReference;

    .end local v7    # "cr":Lorg/apache/poi/ss/util/CellReference;
    move-object/from16 v0, p4

    invoke-direct {v7, v0}, Lorg/apache/poi/ss/util/CellReference;-><init>(Ljava/lang/String;)V

    .line 215
    .restart local v7    # "cr":Lorg/apache/poi/ss/util/CellReference;
    invoke-virtual {v7}, Lorg/apache/poi/ss/util/CellReference;->getRow()I

    move-result v4

    .line 216
    .restart local v4    # "lastRow":I
    invoke-virtual {v7}, Lorg/apache/poi/ss/util/CellReference;->getCol()S

    move-result v5

    .line 217
    .restart local v5    # "lastCol":I
    goto :goto_1

    .line 160
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 172
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 183
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_8
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 196
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public getNameXEval(Lorg/apache/poi/ss/formula/ptg/NameXPtg;)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 20
    .param p1, "nameXPtg"    # Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    .prologue
    .line 263
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;->getSheetRefIndex()I

    move-result v4

    invoke-interface {v3, v4}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getExternalSheet(I)Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;

    move-result-object v12

    .line 264
    .local v12, "externSheet":Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;
    if-nez v12, :cond_0

    .line 265
    new-instance v3, Lorg/apache/poi/ss/formula/eval/NameXEval;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lorg/apache/poi/ss/formula/eval/NameXEval;-><init>(Lorg/apache/poi/ss/formula/ptg/NameXPtg;)V

    .line 295
    :goto_0
    return-object v3

    .line 266
    :cond_0
    invoke-virtual {v12}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;->getWorkbookName()Ljava/lang/String;

    move-result-object v19

    .line 267
    .local v19, "workbookName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    .line 268
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;->getSheetRefIndex()I

    move-result v4

    .line 269
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;->getNameIndex()I

    move-result v5

    .line 267
    invoke-interface {v3, v4, v5}, Lorg/apache/poi/ss/formula/EvaluationWorkbook;->getExternalName(II)Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;

    move-result-object v11

    .line 272
    .local v11, "externName":Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getOtherWorkbookEvaluator(Ljava/lang/String;)Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    move-result-object v15

    .line 273
    .local v15, "refWorkbookEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    invoke-virtual {v11}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;->getIx()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v15, v3, v4}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v10

    .line 274
    .local v10, "evaluationName":Lorg/apache/poi/ss/formula/EvaluationName;
    if-eqz v10, :cond_3

    invoke-interface {v10}, Lorg/apache/poi/ss/formula/EvaluationName;->hasFormula()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 275
    invoke-interface {v10}, Lorg/apache/poi/ss/formula/EvaluationName;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 276
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Complex name formulas not supported yet"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    .end local v10    # "evaluationName":Lorg/apache/poi/ss/formula/EvaluationName;
    .end local v15    # "refWorkbookEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :catch_0
    move-exception v18

    .line 295
    .local v18, "wnfe":Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;
    sget-object v3, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    goto :goto_0

    .line 278
    .end local v18    # "wnfe":Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException;
    .restart local v10    # "evaluationName":Lorg/apache/poi/ss/formula/EvaluationName;
    .restart local v15    # "refWorkbookEvaluator":Lorg/apache/poi/ss/formula/WorkbookEvaluator;
    :cond_1
    :try_start_1
    invoke-interface {v10}, Lorg/apache/poi/ss/formula/EvaluationName;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v13, v3, v4

    .line 279
    .local v13, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v3, v13, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    if-eqz v3, :cond_2

    .line 280
    move-object v0, v13

    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    move-object v14, v0

    .line 281
    .local v14, "ref3D":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    invoke-virtual {v14}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    move-result v3

    invoke-virtual {v15, v3}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndexByExternIndex(I)I

    move-result v16

    .line 282
    .local v16, "sheetIndex":I
    invoke-virtual/range {v15 .. v16}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetName(I)Ljava/lang/String;

    move-result-object v17

    .line 283
    .local v17, "sheetName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->createExternSheetRefEvaluator(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v8

    .line 284
    .local v8, "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    new-instance v3, Lorg/apache/poi/ss/formula/LazyRefEval;

    invoke-virtual {v14}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getRow()I

    move-result v4

    invoke-virtual {v14}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->getColumn()I

    move-result v5

    invoke-direct {v3, v4, v5, v8}, Lorg/apache/poi/ss/formula/LazyRefEval;-><init>(IILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    goto :goto_0

    .line 285
    .end local v8    # "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .end local v14    # "ref3D":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    .end local v16    # "sheetIndex":I
    .end local v17    # "sheetName":Ljava/lang/String;
    :cond_2
    instance-of v3, v13, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    if-eqz v3, :cond_3

    .line 286
    move-object v0, v13

    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    move-object v9, v0

    .line 287
    .local v9, "area3D":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    move-result v3

    invoke-virtual {v15, v3}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetIndexByExternIndex(I)I

    move-result v16

    .line 288
    .restart local v16    # "sheetIndex":I
    invoke-virtual/range {v15 .. v16}, Lorg/apache/poi/ss/formula/WorkbookEvaluator;->getSheetName(I)Ljava/lang/String;

    move-result-object v17

    .line 289
    .restart local v17    # "sheetName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->createExternSheetRefEvaluator(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v8

    .line 290
    .restart local v8    # "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    new-instance v3, Lorg/apache/poi/ss/formula/LazyAreaEval;

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstRow()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstColumn()I

    move-result v5

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastRow()I

    move-result v6

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastColumn()I

    move-result v7

    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/ss/formula/LazyAreaEval;-><init>(IIIILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    goto/16 :goto_0

    .line 293
    .end local v8    # "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .end local v9    # "area3D":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    .end local v13    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .end local v16    # "sheetIndex":I
    .end local v17    # "sheetName":Ljava/lang/String;
    :cond_3
    sget-object v3, Lorg/apache/poi/ss/formula/eval/ErrorEval;->REF_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;
    :try_end_1
    .catch Lorg/apache/poi/ss/formula/CollaboratingWorkbooksEnvironment$WorkbookNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public getRef3DEval(III)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 2
    .param p1, "rowIndex"    # I
    .param p2, "columnIndex"    # I
    .param p3, "extSheetIndex"    # I

    .prologue
    .line 249
    invoke-virtual {p0, p3}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->createExternSheetRefEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v0

    .line 250
    .local v0, "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    new-instance v1, Lorg/apache/poi/ss/formula/LazyRefEval;

    invoke-direct {v1, p1, p2, v0}, Lorg/apache/poi/ss/formula/LazyRefEval;-><init>(IILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    return-object v1
.end method

.method public getRefEval(II)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 2
    .param p1, "rowIndex"    # I
    .param p2, "columnIndex"    # I

    .prologue
    .line 245
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->getRefEvaluatorForCurrentSheet()Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object v0

    .line 246
    .local v0, "sre":Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    new-instance v1, Lorg/apache/poi/ss/formula/LazyRefEval;

    invoke-direct {v1, p1, p2, v0}, Lorg/apache/poi/ss/formula/LazyRefEval;-><init>(IILorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    return-object v1
.end method

.method public getRefEvaluatorForCurrentSheet()Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .locals 4

    .prologue
    .line 125
    new-instance v0, Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    iget-object v1, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_bookEvaluator:Lorg/apache/poi/ss/formula/WorkbookEvaluator;

    iget-object v2, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_tracker:Lorg/apache/poi/ss/formula/EvaluationTracker;

    iget v3, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_sheetIndex:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/ss/formula/SheetRefEvaluator;-><init>(Lorg/apache/poi/ss/formula/WorkbookEvaluator;Lorg/apache/poi/ss/formula/EvaluationTracker;I)V

    return-object v0
.end method

.method public getRowIndex()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_rowIndex:I

    return v0
.end method

.method public getWorkbook()Lorg/apache/poi/ss/formula/EvaluationWorkbook;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/poi/ss/formula/OperationEvaluationContext;->_workbook:Lorg/apache/poi/ss/formula/EvaluationWorkbook;

    return-object v0
.end method
