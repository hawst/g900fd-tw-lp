.class public final enum Lorg/apache/poi/ss/SpreadsheetVersion;
.super Ljava/lang/Enum;
.source "SpreadsheetVersion.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/SpreadsheetVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/SpreadsheetVersion;

.field public static final enum EXCEL2007:Lorg/apache/poi/ss/SpreadsheetVersion;

.field public static final enum EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;


# instance fields
.field private final _maxColumns:I

.field private final _maxCondFormats:I

.field private final _maxFunctionArgs:I

.field private final _maxRows:I

.field private final _maxTextLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/16 v7, 0x7fff

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lorg/apache/poi/ss/SpreadsheetVersion;

    const-string/jumbo v1, "EXCEL97"

    .line 42
    const/high16 v3, 0x10000

    const/16 v4, 0x100

    const/16 v5, 0x1e

    const/4 v6, 0x3

    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/ss/SpreadsheetVersion;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    .line 44
    new-instance v8, Lorg/apache/poi/ss/SpreadsheetVersion;

    const-string/jumbo v9, "EXCEL2007"

    .line 56
    const/high16 v11, 0x100000

    const/16 v12, 0x4000

    const/16 v13, 0xff

    const v14, 0x7fffffff

    move v15, v7

    invoke-direct/range {v8 .. v15}, Lorg/apache/poi/ss/SpreadsheetVersion;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v8, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL2007:Lorg/apache/poi/ss/SpreadsheetVersion;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/poi/ss/SpreadsheetVersion;

    sget-object v1, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL2007:Lorg/apache/poi/ss/SpreadsheetVersion;

    aput-object v1, v0, v10

    sput-object v0, Lorg/apache/poi/ss/SpreadsheetVersion;->ENUM$VALUES:[Lorg/apache/poi/ss/SpreadsheetVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p3, "maxRows"    # I
    .param p4, "maxColumns"    # I
    .param p5, "maxFunctionArgs"    # I
    .param p6, "maxCondFormats"    # I
    .param p7, "maxText"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxRows:I

    .line 66
    iput p4, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxColumns:I

    .line 67
    iput p5, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxFunctionArgs:I

    .line 68
    iput p6, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxCondFormats:I

    .line 69
    iput p7, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxTextLength:I

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/SpreadsheetVersion;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/SpreadsheetVersion;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/SpreadsheetVersion;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/SpreadsheetVersion;->ENUM$VALUES:[Lorg/apache/poi/ss/SpreadsheetVersion;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getLastColumnIndex()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxColumns:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getLastColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastColumnIndex()I

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/ss/util/CellReference;->convertNumToColString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastRowIndex()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxRows:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getMaxColumns()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxColumns:I

    return v0
.end method

.method public getMaxConditionalFormats()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxCondFormats:I

    return v0
.end method

.method public getMaxFunctionArgs()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxFunctionArgs:I

    return v0
.end method

.method public getMaxRows()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxRows:I

    return v0
.end method

.method public getMaxTextLength()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/poi/ss/SpreadsheetVersion;->_maxTextLength:I

    return v0
.end method
