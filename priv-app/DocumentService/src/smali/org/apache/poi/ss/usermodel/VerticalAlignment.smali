.class public final enum Lorg/apache/poi/ss/usermodel/VerticalAlignment;
.super Ljava/lang/Enum;
.source "VerticalAlignment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/VerticalAlignment;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOTTOM:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

.field public static final enum CENTER:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

.field public static final enum DISTRIBUTED:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/VerticalAlignment;

.field public static final enum JUSTIFY:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

.field public static final enum TOP:Lorg/apache/poi/ss/usermodel/VerticalAlignment;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    const-string/jumbo v1, "TOP"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/VerticalAlignment;-><init>(Ljava/lang/String;I)V

    .line 28
    sput-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->TOP:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    .line 30
    new-instance v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    const-string/jumbo v1, "CENTER"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/usermodel/VerticalAlignment;-><init>(Ljava/lang/String;I)V

    .line 33
    sput-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->CENTER:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    .line 35
    new-instance v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    const-string/jumbo v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/ss/usermodel/VerticalAlignment;-><init>(Ljava/lang/String;I)V

    .line 38
    sput-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->BOTTOM:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    .line 40
    new-instance v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    const-string/jumbo v1, "JUSTIFY"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/ss/usermodel/VerticalAlignment;-><init>(Ljava/lang/String;I)V

    .line 53
    sput-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->JUSTIFY:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    .line 55
    new-instance v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    const-string/jumbo v1, "DISTRIBUTED"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/ss/usermodel/VerticalAlignment;-><init>(Ljava/lang/String;I)V

    .line 68
    sput-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->DISTRIBUTED:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    .line 24
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    sget-object v1, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->TOP:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->CENTER:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->BOTTOM:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->JUSTIFY:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->DISTRIBUTED:Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/VerticalAlignment;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/VerticalAlignment;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/VerticalAlignment;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/VerticalAlignment;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
