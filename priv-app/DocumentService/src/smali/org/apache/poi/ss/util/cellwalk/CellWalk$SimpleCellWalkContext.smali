.class Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;
.super Ljava/lang/Object;
.source "CellWalk.java"

# interfaces
.implements Lorg/apache/poi/ss/util/cellwalk/CellWalkContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/util/cellwalk/CellWalk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleCellWalkContext"
.end annotation


# instance fields
.field public colNumber:I

.field public ordinalNumber:J

.field public rowNumber:I

.field final synthetic this$0:Lorg/apache/poi/ss/util/cellwalk/CellWalk;


# direct methods
.method private constructor <init>(Lorg/apache/poi/ss/util/cellwalk/CellWalk;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    iput-object p1, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->this$0:Lorg/apache/poi/ss/util/cellwalk/CellWalk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->ordinalNumber:J

    .line 113
    iput v2, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->rowNumber:I

    .line 114
    iput v2, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->colNumber:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/ss/util/cellwalk/CellWalk;Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;-><init>(Lorg/apache/poi/ss/util/cellwalk/CellWalk;)V

    return-void
.end method


# virtual methods
.method public getColumnNumber()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->colNumber:I

    return v0
.end method

.method public getOrdinalNumber()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->ordinalNumber:J

    return-wide v0
.end method

.method public getRowNumber()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lorg/apache/poi/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;->rowNumber:I

    return v0
.end method
