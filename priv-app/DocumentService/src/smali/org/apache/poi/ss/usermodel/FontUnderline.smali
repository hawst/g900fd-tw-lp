.class public final enum Lorg/apache/poi/ss/usermodel/FontUnderline;
.super Ljava/lang/Enum;
.source "FontUnderline.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/FontUnderline;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$poi$ss$usermodel$FontUnderline:[I

.field public static final enum DOUBLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

.field public static final enum DOUBLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontUnderline;

.field public static final enum NONE:Lorg/apache/poi/ss/usermodel/FontUnderline;

.field public static final enum SINGLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

.field public static final enum SINGLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

.field private static _table:[Lorg/apache/poi/ss/usermodel/FontUnderline;


# instance fields
.field private value:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$poi$ss$usermodel$FontUnderline()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->$SWITCH_TABLE$org$apache$poi$ss$usermodel$FontUnderline:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/poi/ss/usermodel/FontUnderline;->values()[Lorg/apache/poi/ss/usermodel/FontUnderline;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-virtual {v1}, Lorg/apache/poi/ss/usermodel/FontUnderline;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-virtual {v1}, Lorg/apache/poi/ss/usermodel/FontUnderline;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/poi/ss/usermodel/FontUnderline;->NONE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-virtual {v1}, Lorg/apache/poi/ss/usermodel/FontUnderline;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-virtual {v1}, Lorg/apache/poi/ss/usermodel/FontUnderline;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-virtual {v1}, Lorg/apache/poi/ss/usermodel/FontUnderline;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->$SWITCH_TABLE$org$apache$poi$ss$usermodel$FontUnderline:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 27
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontUnderline;

    const-string/jumbo v3, "SINGLE"

    .line 32
    invoke-direct {v2, v3, v1, v5}, Lorg/apache/poi/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 34
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontUnderline;

    const-string/jumbo v3, "DOUBLE"

    .line 39
    invoke-direct {v2, v3, v5, v6}, Lorg/apache/poi/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 41
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontUnderline;

    const-string/jumbo v3, "SINGLE_ACCOUNTING"

    .line 46
    invoke-direct {v2, v3, v6, v7}, Lorg/apache/poi/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 48
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontUnderline;

    const-string/jumbo v3, "DOUBLE_ACCOUNTING"

    .line 53
    invoke-direct {v2, v3, v7, v8}, Lorg/apache/poi/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 55
    new-instance v2, Lorg/apache/poi/ss/usermodel/FontUnderline;

    const-string/jumbo v3, "NONE"

    .line 58
    const/4 v4, 0x5

    invoke-direct {v2, v3, v8, v4}, Lorg/apache/poi/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->NONE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    const/4 v2, 0x5

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/FontUnderline;

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    aput-object v3, v2, v1

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    aput-object v3, v2, v5

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    aput-object v3, v2, v6

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    aput-object v3, v2, v7

    sget-object v3, Lorg/apache/poi/ss/usermodel/FontUnderline;->NONE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    aput-object v3, v2, v8

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 88
    const/4 v2, 0x6

    new-array v2, v2, [Lorg/apache/poi/ss/usermodel/FontUnderline;

    sput-object v2, Lorg/apache/poi/ss/usermodel/FontUnderline;->_table:[Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 90
    invoke-static {}, Lorg/apache/poi/ss/usermodel/FontUnderline;->values()[Lorg/apache/poi/ss/usermodel/FontUnderline;

    move-result-object v2

    array-length v3, v2

    .local v0, "c":Lorg/apache/poi/ss/usermodel/FontUnderline;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 93
    return-void

    .line 90
    :cond_0
    aget-object v0, v2, v1

    .line 91
    sget-object v4, Lorg/apache/poi/ss/usermodel/FontUnderline;->_table:[Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-virtual {v0}, Lorg/apache/poi/ss/usermodel/FontUnderline;->getValue()I

    move-result v5

    aput-object v0, v4, v5

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, Lorg/apache/poi/ss/usermodel/FontUnderline;->value:I

    .line 65
    return-void
.end method

.method public static valueOf(B)Lorg/apache/poi/ss/usermodel/FontUnderline;
    .locals 1
    .param p0, "value"    # B

    .prologue
    .line 101
    sparse-switch p0, :sswitch_data_0

    .line 115
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->NONE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 118
    .local v0, "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    :goto_0
    return-object v0

    .line 103
    .end local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    :sswitch_0
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 104
    .restart local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    goto :goto_0

    .line 106
    .end local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    :sswitch_1
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->DOUBLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 107
    .restart local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    goto :goto_0

    .line 109
    .end local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    :sswitch_2
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE_ACCOUNTING:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 110
    .restart local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    goto :goto_0

    .line 112
    .end local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    :sswitch_3
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->SINGLE:Lorg/apache/poi/ss/usermodel/FontUnderline;

    .line 113
    .restart local v0    # "val":Lorg/apache/poi/ss/usermodel/FontUnderline;
    goto :goto_0

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_0
        0x21 -> :sswitch_2
        0x22 -> :sswitch_1
    .end sparse-switch
.end method

.method public static valueOf(I)Lorg/apache/poi/ss/usermodel/FontUnderline;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 96
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->_table:[Lorg/apache/poi/ss/usermodel/FontUnderline;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/FontUnderline;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/FontUnderline;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/FontUnderline;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/FontUnderline;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/FontUnderline;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/FontUnderline;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getByteValue()B
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 72
    invoke-static {}, Lorg/apache/poi/ss/usermodel/FontUnderline;->$SWITCH_TABLE$org$apache$poi$ss$usermodel$FontUnderline()[I

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/ss/usermodel/FontUnderline;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 84
    :goto_0
    :pswitch_0
    return v0

    .line 74
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 76
    :pswitch_2
    const/16 v0, 0x22

    goto :goto_0

    .line 78
    :pswitch_3
    const/16 v0, 0x21

    goto :goto_0

    .line 80
    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lorg/apache/poi/ss/usermodel/FontUnderline;->value:I

    return v0
.end method
