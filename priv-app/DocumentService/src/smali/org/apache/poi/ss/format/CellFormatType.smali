.class public abstract enum Lorg/apache/poi/ss/format/CellFormatType;
.super Ljava/lang/Enum;
.source "CellFormatType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/format/CellFormatType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DATE:Lorg/apache/poi/ss/format/CellFormatType;

.field public static final enum ELAPSED:Lorg/apache/poi/ss/format/CellFormatType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/format/CellFormatType;

.field public static final enum GENERAL:Lorg/apache/poi/ss/format/CellFormatType;

.field public static final enum NUMBER:Lorg/apache/poi/ss/format/CellFormatType;

.field public static final enum TEXT:Lorg/apache/poi/ss/format/CellFormatType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lorg/apache/poi/ss/format/CellFormatType$1;

    const-string/jumbo v1, "GENERAL"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/format/CellFormatType$1;-><init>(Ljava/lang/String;I)V

    .line 28
    sput-object v0, Lorg/apache/poi/ss/format/CellFormatType;->GENERAL:Lorg/apache/poi/ss/format/CellFormatType;

    .line 36
    new-instance v0, Lorg/apache/poi/ss/format/CellFormatType$2;

    const-string/jumbo v1, "NUMBER"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/format/CellFormatType$2;-><init>(Ljava/lang/String;I)V

    .line 37
    sput-object v0, Lorg/apache/poi/ss/format/CellFormatType;->NUMBER:Lorg/apache/poi/ss/format/CellFormatType;

    .line 45
    new-instance v0, Lorg/apache/poi/ss/format/CellFormatType$3;

    const-string/jumbo v1, "DATE"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/ss/format/CellFormatType$3;-><init>(Ljava/lang/String;I)V

    .line 46
    sput-object v0, Lorg/apache/poi/ss/format/CellFormatType;->DATE:Lorg/apache/poi/ss/format/CellFormatType;

    .line 54
    new-instance v0, Lorg/apache/poi/ss/format/CellFormatType$4;

    const-string/jumbo v1, "ELAPSED"

    invoke-direct {v0, v1, v5}, Lorg/apache/poi/ss/format/CellFormatType$4;-><init>(Ljava/lang/String;I)V

    .line 55
    sput-object v0, Lorg/apache/poi/ss/format/CellFormatType;->ELAPSED:Lorg/apache/poi/ss/format/CellFormatType;

    .line 63
    new-instance v0, Lorg/apache/poi/ss/format/CellFormatType$5;

    const-string/jumbo v1, "TEXT"

    invoke-direct {v0, v1, v6}, Lorg/apache/poi/ss/format/CellFormatType$5;-><init>(Ljava/lang/String;I)V

    .line 64
    sput-object v0, Lorg/apache/poi/ss/format/CellFormatType;->TEXT:Lorg/apache/poi/ss/format/CellFormatType;

    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/poi/ss/format/CellFormatType;

    sget-object v1, Lorg/apache/poi/ss/format/CellFormatType;->GENERAL:Lorg/apache/poi/ss/format/CellFormatType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/format/CellFormatType;->NUMBER:Lorg/apache/poi/ss/format/CellFormatType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/ss/format/CellFormatType;->DATE:Lorg/apache/poi/ss/format/CellFormatType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/poi/ss/format/CellFormatType;->ELAPSED:Lorg/apache/poi/ss/format/CellFormatType;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/poi/ss/format/CellFormatType;->TEXT:Lorg/apache/poi/ss/format/CellFormatType;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/poi/ss/format/CellFormatType;->ENUM$VALUES:[Lorg/apache/poi/ss/format/CellFormatType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/poi/ss/format/CellFormatType;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/ss/format/CellFormatType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/format/CellFormatType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/format/CellFormatType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/format/CellFormatType;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/format/CellFormatType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/format/CellFormatType;->ENUM$VALUES:[Lorg/apache/poi/ss/format/CellFormatType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/format/CellFormatType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method abstract formatter(Ljava/lang/String;)Lorg/apache/poi/ss/format/CellFormatter;
.end method

.method abstract isSpecial(C)Z
.end method
