.class public final enum Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;
.super Ljava/lang/Enum;
.source "AxisOrientation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

.field public static final enum MAX_MIN:Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

.field public static final enum MIN_MAX:Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    const-string/jumbo v1, "MAX_MIN"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;-><init>(Ljava/lang/String;I)V

    .line 30
    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;->MAX_MIN:Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    .line 31
    new-instance v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    const-string/jumbo v1, "MIN_MAX"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;-><init>(Ljava/lang/String;I)V

    .line 35
    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;->MIN_MAX:Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    sget-object v1, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;->MAX_MIN:Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;->MIN_MAX:Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;->ENUM$VALUES:[Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/usermodel/charts/AxisOrientation;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
