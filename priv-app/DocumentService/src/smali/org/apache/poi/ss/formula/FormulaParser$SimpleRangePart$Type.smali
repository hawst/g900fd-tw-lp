.class final enum Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;
.super Ljava/lang/Enum;
.source "FormulaParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CELL:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

.field public static final enum COLUMN:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

.field public static final enum ROW:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    const-string/jumbo v1, "CELL"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->CELL:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    new-instance v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    const-string/jumbo v1, "ROW"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->ROW:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    new-instance v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    const-string/jumbo v1, "COLUMN"

    invoke-direct {v0, v1, v4}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->COLUMN:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    .line 723
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    sget-object v1, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->CELL:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->ROW:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->COLUMN:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->ENUM$VALUES:[Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 723
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static get(ZZ)Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;
    .locals 2
    .param p0, "hasLetters"    # Z
    .param p1, "hasDigits"    # Z

    .prologue
    .line 727
    if-eqz p0, :cond_1

    .line 728
    if-eqz p1, :cond_0

    sget-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->CELL:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    .line 733
    :goto_0
    return-object v0

    .line 728
    :cond_0
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->COLUMN:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    goto :goto_0

    .line 730
    :cond_1
    if-nez p1, :cond_2

    .line 731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "must have either letters or numbers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 733
    :cond_2
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->ROW:Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;->ENUM$VALUES:[Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart$Type;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
