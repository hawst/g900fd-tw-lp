.class public final Lorg/apache/poi/ss/formula/eval/NameXEval;
.super Ljava/lang/Object;
.source "NameXEval.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/eval/ValueEval;


# instance fields
.field private final _ptg:Lorg/apache/poi/ss/formula/ptg/NameXPtg;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ss/formula/ptg/NameXPtg;)V
    .locals 0
    .param p1, "ptg"    # Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lorg/apache/poi/ss/formula/eval/NameXEval;->_ptg:Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    .line 31
    return-void
.end method


# virtual methods
.method public getPtg()Lorg/apache/poi/ss/formula/ptg/NameXPtg;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/poi/ss/formula/eval/NameXEval;->_ptg:Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 39
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 40
    iget-object v1, p0, Lorg/apache/poi/ss/formula/eval/NameXEval;->_ptg:Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;->getSheetRefIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/ss/formula/eval/NameXEval;->_ptg:Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;->getNameIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 41
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
