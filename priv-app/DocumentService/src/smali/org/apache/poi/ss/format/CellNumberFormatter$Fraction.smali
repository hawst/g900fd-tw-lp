.class Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;
.super Ljava/lang/Object;
.source "CellNumberFormatter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/format/CellNumberFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Fraction"
.end annotation


# instance fields
.field private final denominator:I

.field private final numerator:I


# direct methods
.method private constructor <init>(DDII)V
    .locals 35
    .param p1, "value"    # D
    .param p3, "epsilon"    # D
    .param p5, "maxDenominator"    # I
    .param p6, "maxIterations"    # I

    .prologue
    .line 1002
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 1004
    const-wide/32 v12, 0x7fffffff

    .line 1005
    .local v12, "overflow":J
    move-wide/from16 v26, p1

    .line 1006
    .local v26, "r0":D
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->floor(D)D

    move-result-wide v30

    move-wide/from16 v0, v30

    double-to-long v4, v0

    .line 1007
    .local v4, "a0":J
    cmp-long v30, v4, v12

    if-lez v30, :cond_0

    .line 1008
    new-instance v30, Ljava/lang/IllegalArgumentException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string/jumbo v32, "Overflow trying to convert "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, " to fraction ("

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, "/"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-wide/16 v32, 0x1

    invoke-virtual/range {v31 .. v33}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, ")"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 1013
    :cond_0
    long-to-double v0, v4

    move-wide/from16 v30, v0

    sub-double v30, v30, p1

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->abs(D)D

    move-result-wide v30

    cmpg-double v30, v30, p3

    if-gez v30, :cond_1

    .line 1014
    long-to-int v0, v4

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->numerator:I

    .line 1015
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->denominator:I

    .line 1064
    :goto_0
    return-void

    .line 1019
    :cond_1
    const-wide/16 v14, 0x1

    .line 1020
    .local v14, "p0":J
    const-wide/16 v20, 0x0

    .line 1021
    .local v20, "q0":J
    move-wide/from16 v16, v4

    .line 1022
    .local v16, "p1":J
    const-wide/16 v22, 0x1

    .line 1027
    .local v22, "q1":J
    const/4 v10, 0x0

    .line 1028
    .local v10, "n":I
    const/4 v11, 0x0

    .line 1030
    .local v11, "stop":Z
    :cond_2
    add-int/lit8 v10, v10, 0x1

    .line 1031
    const-wide/high16 v30, 0x3ff0000000000000L    # 1.0

    long-to-double v0, v4

    move-wide/from16 v32, v0

    sub-double v32, v26, v32

    div-double v28, v30, v32

    .line 1032
    .local v28, "r1":D
    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->floor(D)D

    move-result-wide v30

    move-wide/from16 v0, v30

    double-to-long v6, v0

    .line 1033
    .local v6, "a1":J
    mul-long v30, v6, v16

    add-long v18, v30, v14

    .line 1034
    .local v18, "p2":J
    mul-long v30, v6, v22

    add-long v24, v30, v20

    .line 1035
    .local v24, "q2":J
    cmp-long v30, v18, v12

    if-gtz v30, :cond_3

    cmp-long v30, v24, v12

    if-lez v30, :cond_4

    .line 1036
    :cond_3
    new-instance v30, Ljava/lang/RuntimeException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string/jumbo v32, "Overflow trying to convert "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, " to fraction ("

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, "/"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, ")"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 1039
    :cond_4
    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v30, v0

    move-wide/from16 v0, v24

    long-to-double v0, v0

    move-wide/from16 v32, v0

    div-double v8, v30, v32

    .line 1040
    .local v8, "convergent":D
    move/from16 v0, p6

    if-ge v10, v0, :cond_5

    sub-double v30, v8, p1

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->abs(D)D

    move-result-wide v30

    cmpl-double v30, v30, p3

    if-lez v30, :cond_5

    move/from16 v0, p5

    int-to-long v0, v0

    move-wide/from16 v30, v0

    cmp-long v30, v24, v30

    if-gez v30, :cond_5

    .line 1041
    move-wide/from16 v14, v16

    .line 1042
    move-wide/from16 v16, v18

    .line 1043
    move-wide/from16 v20, v22

    .line 1044
    move-wide/from16 v22, v24

    .line 1045
    move-wide v4, v6

    .line 1046
    move-wide/from16 v26, v28

    .line 1050
    :goto_1
    if-eqz v11, :cond_2

    .line 1052
    move/from16 v0, p6

    if-lt v10, v0, :cond_6

    .line 1053
    new-instance v30, Ljava/lang/RuntimeException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string/jumbo v32, "Unable to convert "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, " to fraction after "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string/jumbo v32, " iterations"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 1048
    :cond_5
    const/4 v11, 0x1

    goto :goto_1

    .line 1056
    :cond_6
    move/from16 v0, p5

    int-to-long v0, v0

    move-wide/from16 v30, v0

    cmp-long v30, v24, v30

    if-gez v30, :cond_7

    .line 1057
    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->numerator:I

    .line 1058
    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->denominator:I

    goto/16 :goto_0

    .line 1060
    :cond_7
    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->numerator:I

    .line 1061
    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->denominator:I

    goto/16 :goto_0
.end method

.method public constructor <init>(DI)V
    .locals 9
    .param p1, "value"    # D
    .param p3, "maxDenominator"    # I

    .prologue
    .line 1082
    const-wide/16 v4, 0x0

    const/16 v7, 0x64

    move-object v1, p0

    move-wide v2, p1

    move v6, p3

    invoke-direct/range {v1 .. v7}, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;-><init>(DDII)V

    .line 1083
    return-void
.end method


# virtual methods
.method public getDenominator()I
    .locals 1

    .prologue
    .line 1090
    iget v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->denominator:I

    return v0
.end method

.method public getNumerator()I
    .locals 1

    .prologue
    .line 1098
    iget v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->numerator:I

    return v0
.end method
