.class final enum Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;
.super Ljava/lang/Enum;
.source "FormulaShifter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/FormulaShifter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ShiftMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

.field public static final enum Row:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

.field public static final enum Sheet:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    const-string/jumbo v1, "Row"

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Row:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    .line 30
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    const-string/jumbo v1, "Sheet"

    invoke-direct {v0, v1, v3}, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Sheet:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    sget-object v1, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Row:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->Sheet:Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->ENUM$VALUES:[Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    return-object v0
.end method

.method public static values()[Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;->ENUM$VALUES:[Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/poi/ss/formula/FormulaShifter$ShiftMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
