.class public Lorg/apache/poi/ss/usermodel/FractionFormat;
.super Ljava/text/Format;
.source "FractionFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;,
        Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;
    }
.end annotation


# static fields
.field private static final DENOM_FORMAT_PATTERN:Ljava/util/regex/Pattern;

.field private static final MAX_DENOM_POW:I = 0x4


# instance fields
.field private final exactDenom:I

.field private final maxDenom:I

.field private final wholePartFormatString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string/jumbo v0, "(?:(#+)|(\\d+))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->DENOM_FORMAT_PATTERN:Ljava/util/regex/Pattern;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "wholePartFormatString"    # Ljava/lang/String;
    .param p2, "denomFormatString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 66
    invoke-direct {p0}, Ljava/text/Format;-><init>()V

    .line 67
    iput-object p1, p0, Lorg/apache/poi/ss/usermodel/FractionFormat;->wholePartFormatString:Ljava/lang/String;

    .line 69
    sget-object v5, Lorg/apache/poi/ss/usermodel/FractionFormat;->DENOM_FORMAT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v5, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 70
    .local v1, "m":Ljava/util/regex/Matcher;
    const/4 v2, -0x1

    .line 71
    .local v2, "tmpExact":I
    const/4 v3, -0x1

    .line 72
    .local v3, "tmpMax":I
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 73
    invoke-virtual {v1, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 75
    const/4 v4, 0x2

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 78
    if-nez v2, :cond_0

    .line 79
    const/4 v2, -0x1

    .line 92
    :cond_0
    :goto_0
    if-gtz v2, :cond_1

    if-gtz v3, :cond_1

    .line 94
    const/16 v2, 0x64

    .line 96
    :cond_1
    iput v2, p0, Lorg/apache/poi/ss/usermodel/FractionFormat;->exactDenom:I

    .line 97
    iput v3, p0, Lorg/apache/poi/ss/usermodel/FractionFormat;->maxDenom:I

    .line 98
    return-void

    .line 84
    :cond_2
    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 85
    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    .line 86
    .local v0, "len":I
    if-le v0, v4, :cond_3

    move v0, v4

    .line 87
    :cond_3
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    int-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v3, v4

    .line 88
    goto :goto_0

    .line 89
    .end local v0    # "len":I
    :cond_4
    const/16 v2, 0x64

    goto :goto_0

    .line 81
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private calcFractionExactDenom(DI)Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;
    .locals 5
    .param p1, "val"    # D
    .param p3, "exactDenom"    # I

    .prologue
    .line 251
    int-to-double v2, p3

    mul-double/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    .line 252
    .local v0, "num":I
    new-instance v1, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;

    invoke-direct {v1, p0, v0, p3}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;-><init>(Lorg/apache/poi/ss/usermodel/FractionFormat;II)V

    return-object v1
.end method

.method private calcFractionMaxDenom(DI)Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;
    .locals 39
    .param p1, "value"    # D
    .param p3, "maxDenominator"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;
        }
    .end annotation

    .prologue
    .line 189
    const-wide v10, 0x3d71979980000000L    # 9.999999960041972E-13

    .line 190
    .local v10, "epsilon":D
    const/16 v12, 0x64

    .line 191
    .local v12, "maxIterations":I
    const-wide/32 v14, 0x7fffffff

    .line 192
    .local v14, "overflow":J
    move-wide/from16 v28, p1

    .line 193
    .local v28, "r0":D
    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->floor(D)D

    move-result-wide v34

    move-wide/from16 v0, v34

    double-to-long v4, v0

    .line 194
    .local v4, "a0":J
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v34

    cmp-long v33, v34, v14

    if-lez v33, :cond_0

    .line 195
    new-instance v33, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;

    .line 196
    const-string/jumbo v34, "value > Integer.MAX_VALUE: %d."

    const/16 v35, 0x1

    move/from16 v0, v35

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    aput-object v37, v35, v36

    invoke-static/range {v34 .. v35}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    .line 195
    const/16 v35, 0x0

    invoke-direct/range {v33 .. v35}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;)V

    throw v33

    .line 201
    :cond_0
    long-to-double v0, v4

    move-wide/from16 v34, v0

    sub-double v34, v34, p1

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->abs(D)D

    move-result-wide v34

    cmpg-double v33, v34, v10

    if-gez v33, :cond_1

    .line 202
    new-instance v33, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;

    long-to-int v0, v4

    move/from16 v34, v0

    const/16 v35, 0x1

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    move/from16 v2, v34

    move/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;-><init>(Lorg/apache/poi/ss/usermodel/FractionFormat;II)V

    .line 246
    :goto_0
    return-object v33

    .line 205
    :cond_1
    const-wide/16 v16, 0x1

    .line 206
    .local v16, "p0":J
    const-wide/16 v22, 0x0

    .line 207
    .local v22, "q0":J
    move-wide/from16 v18, v4

    .line 208
    .local v18, "p1":J
    const-wide/16 v24, 0x1

    .line 210
    .local v24, "q1":J
    const-wide/16 v20, 0x0

    .line 211
    .local v20, "p2":J
    const-wide/16 v26, 0x1

    .line 213
    .local v26, "q2":J
    const/4 v13, 0x0

    .line 214
    .local v13, "n":I
    const/16 v32, 0x0

    .line 216
    .local v32, "stop":Z
    :cond_2
    add-int/lit8 v13, v13, 0x1

    .line 217
    const-wide/high16 v34, 0x3ff0000000000000L    # 1.0

    long-to-double v0, v4

    move-wide/from16 v36, v0

    sub-double v36, v28, v36

    div-double v30, v34, v36

    .line 218
    .local v30, "r1":D
    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->floor(D)D

    move-result-wide v34

    move-wide/from16 v0, v34

    double-to-long v6, v0

    .line 219
    .local v6, "a1":J
    mul-long v34, v6, v18

    add-long v20, v34, v16

    .line 220
    mul-long v34, v6, v24

    add-long v26, v34, v22

    .line 221
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(J)J

    move-result-wide v34

    cmp-long v33, v34, v14

    if-gtz v33, :cond_3

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->abs(J)J

    move-result-wide v34

    cmp-long v33, v34, v14

    if-lez v33, :cond_4

    .line 222
    :cond_3
    new-instance v33, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;

    .line 223
    const-string/jumbo v34, "Greater than overflow in loop %f, %d, %d"

    const/16 v35, 0x3

    move/from16 v0, v35

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v37

    aput-object v37, v35, v36

    const/16 v36, 0x1

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    aput-object v37, v35, v36

    const/16 v36, 0x2

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    aput-object v37, v35, v36

    invoke-static/range {v34 .. v35}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    .line 222
    const/16 v35, 0x0

    invoke-direct/range {v33 .. v35}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;)V

    throw v33

    .line 226
    :cond_4
    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v34, v0

    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v36, v0

    div-double v8, v34, v36

    .line 227
    .local v8, "convergent":D
    if-ge v13, v12, :cond_5

    sub-double v34, v8, p1

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->abs(D)D

    move-result-wide v34

    cmpl-double v33, v34, v10

    if-lez v33, :cond_5

    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v34, v0

    cmp-long v33, v26, v34

    if-gez v33, :cond_5

    .line 228
    move-wide/from16 v16, v18

    .line 229
    move-wide/from16 v18, v20

    .line 230
    move-wide/from16 v22, v24

    .line 231
    move-wide/from16 v24, v26

    .line 232
    move-wide v4, v6

    .line 233
    move-wide/from16 v28, v30

    .line 237
    :goto_1
    if-eqz v32, :cond_2

    .line 239
    if-lt v13, v12, :cond_6

    .line 240
    new-instance v33, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;

    new-instance v34, Ljava/lang/StringBuilder;

    const-string/jumbo v35, "n greater than max iterations "

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string/jumbo v35, " : "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x0

    invoke-direct/range {v33 .. v35}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;)V

    throw v33

    .line 235
    :cond_5
    const/16 v32, 0x1

    goto :goto_1

    .line 243
    :cond_6
    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v34, v0

    cmp-long v33, v26, v34

    if-gez v33, :cond_7

    .line 244
    new-instance v33, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v34, v0

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v35, v0

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    move/from16 v2, v34

    move/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;-><init>(Lorg/apache/poi/ss/usermodel/FractionFormat;II)V

    goto/16 :goto_0

    .line 246
    :cond_7
    new-instance v33, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v34, v0

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v35, v0

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    move/from16 v2, v34

    move/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;-><init>(Lorg/apache/poi/ss/usermodel/FractionFormat;II)V

    goto/16 :goto_0
.end method


# virtual methods
.method public format(Ljava/lang/Number;)Ljava/lang/String;
    .locals 20
    .param p1, "num"    # Ljava/lang/Number;

    .prologue
    .line 102
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    .line 104
    .local v6, "doubleValue":D
    const-wide/16 v16, 0x0

    cmpg-double v13, v6, v16

    if-gez v13, :cond_0

    const/4 v10, 0x1

    .line 105
    .local v10, "isNeg":Z
    :goto_0
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 107
    .local v2, "absDoubleValue":D
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    .line 108
    .local v14, "wholePart":D
    sub-double v4, v2, v14

    .line 109
    .local v4, "decPart":D
    add-double v16, v14, v4

    const-wide/16 v18, 0x0

    cmpl-double v13, v16, v18

    if-nez v13, :cond_1

    .line 110
    const-string/jumbo v13, "0"

    .line 173
    :goto_1
    return-object v13

    .line 104
    .end local v2    # "absDoubleValue":D
    .end local v4    # "decPart":D
    .end local v10    # "isNeg":Z
    .end local v14    # "wholePart":D
    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    .line 115
    .restart local v2    # "absDoubleValue":D
    .restart local v4    # "decPart":D
    .restart local v10    # "isNeg":Z
    .restart local v14    # "wholePart":D
    :cond_1
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->exactDenom:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->maxDenom:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v13, v0}, Ljava/lang/Math;->max(II)I

    move-result v13

    int-to-double v0, v13

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    cmpg-double v13, v2, v16

    if-gez v13, :cond_2

    .line 116
    const-string/jumbo v13, "0"

    goto :goto_1

    .line 122
    :cond_2
    double-to-int v13, v4

    int-to-double v0, v13

    move-wide/from16 v16, v0

    add-double v16, v16, v14

    add-double v18, v14, v4

    invoke-static/range {v16 .. v19}, Ljava/lang/Double;->compare(DD)I

    move-result v13

    if-nez v13, :cond_4

    .line 124
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v11, "sb":Ljava/lang/StringBuilder;
    if-eqz v10, :cond_3

    .line 126
    const-string/jumbo v13, "-"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_3
    double-to-int v13, v14

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    .line 132
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    const/4 v9, 0x0

    .line 135
    .local v9, "fract":Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;
    :try_start_0
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->exactDenom:I

    if-lez v13, :cond_6

    .line 136
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->exactDenom:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v13}, Lorg/apache/poi/ss/usermodel/FractionFormat;->calcFractionExactDenom(DI)Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;
    :try_end_0
    .catch Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 145
    :goto_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    if-eqz v10, :cond_5

    .line 149
    const-string/jumbo v13, "-"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->wholePartFormatString:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_7

    .line 154
    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getDenominator()I

    move-result v13

    double-to-int v0, v14

    move/from16 v16, v0

    mul-int v13, v13, v16

    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getNumerator()I

    move-result v16

    add-int v12, v13, v16

    .line 155
    .local v12, "trueNum":I
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getDenominator()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 138
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    .end local v12    # "trueNum":I
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/poi/ss/usermodel/FractionFormat;->maxDenom:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v13}, Lorg/apache/poi/ss/usermodel/FractionFormat;->calcFractionMaxDenom(DI)Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;
    :try_end_1
    .catch Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v9

    goto :goto_2

    .line 140
    :catch_0
    move-exception v8

    .line 141
    .local v8, "e":Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;
    const-string/jumbo v13, "DocumentService"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Exception: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 161
    .end local v8    # "e":Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFractionException;
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    :cond_7
    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getNumerator()I

    move-result v13

    if-nez v13, :cond_8

    .line 162
    double-to-int v13, v14

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 164
    :cond_8
    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getNumerator()I

    move-result v13

    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getDenominator()I

    move-result v16

    move/from16 v0, v16

    if-ne v13, v0, :cond_9

    .line 165
    double-to-int v13, v14

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 169
    :cond_9
    const-wide/16 v16, 0x0

    cmpl-double v13, v14, v16

    if-lez v13, :cond_a

    .line 170
    double-to-int v13, v14

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_a
    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getNumerator()I

    move-result v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v9}, Lorg/apache/poi/ss/usermodel/FractionFormat$SimpleFraction;->getDenominator()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1
.end method

.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 177
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/usermodel/FractionFormat;->format(Ljava/lang/Number;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 2
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 181
    new-instance v0, Lorg/apache/poi/ss/formula/eval/NotImplementedException;

    const-string/jumbo v1, "Reverse parsing not supported"

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
