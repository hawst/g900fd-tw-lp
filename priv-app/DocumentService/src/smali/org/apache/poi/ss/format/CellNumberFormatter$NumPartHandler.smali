.class Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;
.super Ljava/lang/Object;
.source "CellNumberFormatter.java"

# interfaces
.implements Lorg/apache/poi/ss/format/CellFormatPart$PartHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/format/CellNumberFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NumPartHandler"
.end annotation


# instance fields
.field private insertSignForExponent:C

.field final synthetic this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;


# direct methods
.method private constructor <init>(Lorg/apache/poi/ss/format/CellNumberFormatter;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter;)V

    return-void
.end method


# virtual methods
.method public handlePart(Ljava/util/regex/Matcher;Ljava/lang/String;Lorg/apache/poi/ss/format/CellFormatType;Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 10
    .param p1, "m"    # Ljava/util/regex/Matcher;
    .param p2, "part"    # Ljava/lang/String;
    .param p3, "type"    # Lorg/apache/poi/ss/format/CellFormatType;
    .param p4, "desc"    # Ljava/lang/StringBuffer;

    .prologue
    const/16 v9, 0x2e

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 190
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 191
    .local v3, "pos":I
    invoke-virtual {p2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 192
    .local v1, "firstCh":C
    sparse-switch v1, :sswitch_data_0

    .line 243
    const/4 p2, 0x0

    .line 245
    .end local p2    # "part":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 198
    .restart local p2    # "part":Ljava/lang/String;
    :sswitch_0
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$2(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 199
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    new-instance v6, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-direct {v6, v9, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;-><init>(CI)V

    invoke-static {v5, v6}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$4(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-virtual {p2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iput-char v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->insertSignForExponent:C

    .line 201
    invoke-virtual {p2, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 208
    :sswitch_1
    iget-char v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->insertSignForExponent:C

    if-eqz v4, :cond_1

    .line 209
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iget-char v6, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->insertSignForExponent:C

    invoke-direct {v5, v6, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;-><init>(CI)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-char v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->insertSignForExponent:C

    invoke-virtual {p4, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 211
    iput-char v7, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->insertSignForExponent:C

    .line 212
    add-int/lit8 v3, v3, 0x1

    .line 214
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 215
    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 216
    .local v0, "ch":C
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    add-int v6, v3, v2

    invoke-direct {v5, v0, v6}, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;-><init>(CI)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 221
    .end local v0    # "ch":C
    .end local v2    # "i":I
    :sswitch_2
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$5(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 222
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    new-instance v6, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-direct {v6, v9, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;-><init>(CI)V

    invoke-static {v5, v6}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$6(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 227
    :sswitch_3
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$7(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 228
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # invokes: Lorg/apache/poi/ss/format/CellNumberFormatter;->previousNumber()Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$8(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$9(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V

    .line 231
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$10(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v5

    # invokes: Lorg/apache/poi/ss/format/CellNumberFormatter;->firstDigit(Ljava/util/List;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$11(Ljava/util/List;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v5

    if-ne v4, v5, :cond_2

    .line 232
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    invoke-static {v4, v8}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$12(Lorg/apache/poi/ss/format/CellNumberFormatter;Z)V

    .line 233
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    new-instance v6, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-direct {v6, v9, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;-><init>(CI)V

    invoke-static {v5, v6}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$13(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 239
    :sswitch_4
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;->this$0:Lorg/apache/poi/ss/format/CellNumberFormatter;

    # getter for: Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D
    invoke-static {v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$14(Lorg/apache/poi/ss/format/CellNumberFormatter;)D

    move-result-wide v6

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v8

    invoke-static {v4, v6, v7}, Lorg/apache/poi/ss/format/CellNumberFormatter;->access$15(Lorg/apache/poi/ss/format/CellNumberFormatter;D)V

    goto/16 :goto_0

    .line 192
    :sswitch_data_0
    .sparse-switch
        0x23 -> :sswitch_1
        0x25 -> :sswitch_4
        0x2e -> :sswitch_2
        0x2f -> :sswitch_3
        0x30 -> :sswitch_1
        0x3f -> :sswitch_1
        0x45 -> :sswitch_0
        0x65 -> :sswitch_0
    .end sparse-switch
.end method
