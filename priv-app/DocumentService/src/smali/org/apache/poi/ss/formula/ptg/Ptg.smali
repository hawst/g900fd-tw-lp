.class public abstract Lorg/apache/poi/ss/formula/ptg/Ptg;
.super Ljava/lang/Object;
.source "Ptg.java"


# static fields
.field public static final CLASS_ARRAY:B = 0x40t

.field public static final CLASS_REF:B = 0x0t

.field public static final CLASS_VALUE:B = 0x20t

.field public static final EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;


# instance fields
.field private ptgClass:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    sput-object v0, Lorg/apache/poi/ss/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;

    .line 254
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/4 v0, 0x0

    iput-byte v0, p0, Lorg/apache/poi/ss/formula/ptg/Ptg;->ptgClass:B

    .line 43
    return-void
.end method

.method private static createBasePtg(BLorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 3
    .param p0, "id"    # B
    .param p1, "in"    # Lorg/apache/poi/util/LittleEndianInput;

    .prologue
    .line 127
    packed-switch p0, :pswitch_data_0

    .line 159
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected base token id ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :pswitch_1
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/UnknownPtg;

    invoke-direct {v0, p0}, Lorg/apache/poi/ss/formula/ptg/UnknownPtg;-><init>(I)V

    .line 157
    :goto_0
    return-object v0

    .line 129
    :pswitch_2
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/ExpPtg;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/ptg/ExpPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 130
    :pswitch_3
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/TblPtg;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/ptg/TblPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 131
    :pswitch_4
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/AddPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 132
    :pswitch_5
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/SubtractPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 133
    :pswitch_6
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/MultiplyPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 134
    :pswitch_7
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/DividePtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 135
    :pswitch_8
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/PowerPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 136
    :pswitch_9
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/ConcatPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 137
    :pswitch_a
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/LessThanPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 138
    :pswitch_b
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/LessEqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 139
    :pswitch_c
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/EqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 140
    :pswitch_d
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/GreaterEqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 141
    :pswitch_e
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/GreaterThanPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 142
    :pswitch_f
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/NotEqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 143
    :pswitch_10
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/IntersectionPtg;->instance:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    goto :goto_0

    .line 144
    :pswitch_11
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/UnionPtg;->instance:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    goto :goto_0

    .line 145
    :pswitch_12
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/RangePtg;->instance:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    goto :goto_0

    .line 146
    :pswitch_13
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/UnaryPlusPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 147
    :pswitch_14
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/UnaryMinusPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 148
    :pswitch_15
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/PercentPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 149
    :pswitch_16
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/ParenthesisPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ControlPtg;

    goto :goto_0

    .line 150
    :pswitch_17
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/MissingArgPtg;->instance:Lorg/apache/poi/ss/formula/ptg/Ptg;

    goto :goto_0

    .line 152
    :pswitch_18
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/StringPtg;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/ptg/StringPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 153
    :pswitch_19
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 154
    :pswitch_1a
    invoke-static {p1}, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->read(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_1b
    invoke-static {p1}, Lorg/apache/poi/ss/formula/ptg/BoolPtg;->read(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/BoolPtg;

    move-result-object v0

    goto :goto_0

    .line 156
    :pswitch_1c
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/ptg/IntPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 157
    :pswitch_1d
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method private static createClassifiedPtg(BLorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 4
    .param p0, "id"    # B
    .param p1, "in"    # Lorg/apache/poi/util/LittleEndianInput;

    .prologue
    .line 99
    and-int/lit8 v1, p0, 0x1f

    or-int/lit8 v0, v1, 0x20

    .line 101
    .local v0, "baseId":I
    packed-switch v0, :pswitch_data_0

    .line 122
    :pswitch_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, " Unknown Ptg in Formula: 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 123
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 122
    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :pswitch_1
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/ArrayPtg$Initial;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/ArrayPtg$Initial;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    .line 120
    :goto_0
    return-object v1

    .line 103
    :pswitch_2
    invoke-static {p1}, Lorg/apache/poi/ss/formula/ptg/FuncPtg;->create(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/FuncPtg;

    move-result-object v1

    goto :goto_0

    .line 104
    :pswitch_3
    invoke-static {p1}, Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;->create(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;

    move-result-object v1

    goto :goto_0

    .line 105
    :pswitch_4
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/NamePtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/NamePtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 106
    :pswitch_5
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/RefPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 107
    :pswitch_6
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 108
    :pswitch_7
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/MemAreaPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/MemAreaPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 109
    :pswitch_8
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/MemErrPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/MemErrPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 110
    :pswitch_9
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 111
    :pswitch_a
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/RefErrorPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/RefErrorPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 112
    :pswitch_b
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 113
    :pswitch_c
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/RefNPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/RefNPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 114
    :pswitch_d
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/AreaNPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/AreaNPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 116
    :pswitch_e
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 117
    :pswitch_f
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 118
    :pswitch_10
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 119
    :pswitch_11
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/DeletedRef3DPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/DeletedRef3DPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 120
    :pswitch_12
    new-instance v1, Lorg/apache/poi/ss/formula/ptg/DeletedArea3DPtg;

    invoke-direct {v1, p1}, Lorg/apache/poi/ss/formula/ptg/DeletedArea3DPtg;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static createPtg(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 5
    .param p0, "in"    # Lorg/apache/poi/util/LittleEndianInput;

    .prologue
    const/16 v4, 0x40

    const/16 v3, 0x20

    .line 79
    invoke-interface {p0}, Lorg/apache/poi/util/LittleEndianInput;->readByte()B

    move-result v0

    .line 81
    .local v0, "id":B
    if-ge v0, v3, :cond_0

    .line 82
    invoke-static {v0, p0}, Lorg/apache/poi/ss/formula/ptg/Ptg;->createBasePtg(BLorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    .line 94
    :goto_0
    return-object v1

    .line 85
    :cond_0
    invoke-static {v0, p0}, Lorg/apache/poi/ss/formula/ptg/Ptg;->createClassifiedPtg(BLorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    .line 87
    .local v1, "retval":Lorg/apache/poi/ss/formula/ptg/Ptg;
    const/16 v2, 0x60

    if-lt v0, v2, :cond_1

    .line 88
    invoke-virtual {v1, v4}, Lorg/apache/poi/ss/formula/ptg/Ptg;->setClass(B)V

    goto :goto_0

    .line 89
    :cond_1
    if-lt v0, v4, :cond_2

    .line 90
    invoke-virtual {v1, v3}, Lorg/apache/poi/ss/formula/ptg/Ptg;->setClass(B)V

    goto :goto_0

    .line 92
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/poi/ss/formula/ptg/Ptg;->setClass(B)V

    goto :goto_0
.end method

.method public static doesFormulaReferToDeletedCell([Lorg/apache/poi/ss/formula/ptg/Ptg;)Z
    .locals 2
    .param p0, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 296
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_0

    .line 301
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 297
    :cond_0
    aget-object v1, p0, v0

    invoke-static {v1}, Lorg/apache/poi/ss/formula/ptg/Ptg;->isDeletedCellRef(Lorg/apache/poi/ss/formula/ptg/Ptg;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 298
    const/4 v1, 0x1

    goto :goto_1

    .line 296
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getEncodedSize([Lorg/apache/poi/ss/formula/ptg/Ptg;)I
    .locals 3
    .param p0, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 176
    const/4 v1, 0x0

    .line 177
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-lt v0, v2, :cond_0

    .line 180
    return v1

    .line 178
    :cond_0
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/ptg/Ptg;->getSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getEncodedSizeWithoutArrayData([Lorg/apache/poi/ss/formula/ptg/Ptg;)I
    .locals 4
    .param p0, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 187
    const/4 v2, 0x0

    .line 188
    .local v2, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p0

    if-lt v0, v3, :cond_0

    .line 196
    return v2

    .line 189
    :cond_0
    aget-object v1, p0, v0

    .line 190
    .local v1, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v3, v1, Lorg/apache/poi/ss/formula/ptg/ArrayPtg;

    if-eqz v3, :cond_1

    .line 191
    add-int/lit8 v2, v2, 0x8

    .line 188
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/ptg/Ptg;->getSize()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1
.end method

.method private static isDeletedCellRef(Lorg/apache/poi/ss/formula/ptg/Ptg;)Z
    .locals 2
    .param p0, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    const/4 v0, 0x1

    .line 304
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->REF_INVALID:Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    if-ne p0, v1, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v0

    .line 307
    :cond_1
    instance-of v1, p0, Lorg/apache/poi/ss/formula/ptg/DeletedArea3DPtg;

    if-nez v1, :cond_0

    .line 310
    instance-of v1, p0, Lorg/apache/poi/ss/formula/ptg/DeletedRef3DPtg;

    if-nez v1, :cond_0

    .line 313
    instance-of v1, p0, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;

    if-nez v1, :cond_0

    .line 316
    instance-of v1, p0, Lorg/apache/poi/ss/formula/ptg/RefErrorPtg;

    if-nez v1, :cond_0

    .line 319
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static readTokens(ILorg/apache/poi/util/LittleEndianInput;)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 8
    .param p0, "size"    # I
    .param p1, "in"    # Lorg/apache/poi/util/LittleEndianInput;

    .prologue
    .line 52
    new-instance v5, Ljava/util/ArrayList;

    div-int/lit8 v6, p0, 0x2

    add-int/lit8 v6, v6, 0x4

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    .local v5, "temp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    const/4 v2, 0x0

    .line 54
    .local v2, "pos":I
    const/4 v0, 0x0

    .line 55
    .local v0, "hasArrayPtgs":Z
    :goto_0
    if-lt v2, p0, :cond_0

    .line 63
    if-eq v2, p0, :cond_2

    .line 64
    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "Ptg array size mismatch"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 56
    :cond_0
    invoke-static {p1}, Lorg/apache/poi/ss/formula/ptg/Ptg;->createPtg(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v3

    .line 57
    .local v3, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v6, v3, Lorg/apache/poi/ss/formula/ptg/ArrayPtg$Initial;

    if-eqz v6, :cond_1

    .line 58
    const/4 v0, 0x1

    .line 60
    :cond_1
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/ptg/Ptg;->getSize()I

    move-result v6

    add-int/2addr v2, v6

    .line 61
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    .end local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_2
    if-eqz v0, :cond_5

    .line 67
    invoke-static {v5}, Lorg/apache/poi/ss/formula/ptg/Ptg;->toPtgArray(Ljava/util/List;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v4

    .line 68
    .local v4, "result":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v6, v4

    if-lt v1, v6, :cond_3

    .line 75
    .end local v1    # "i":I
    .end local v4    # "result":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_2
    return-object v4

    .line 69
    .restart local v1    # "i":I
    .restart local v4    # "result":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_3
    aget-object v6, v4, v1

    instance-of v6, v6, Lorg/apache/poi/ss/formula/ptg/ArrayPtg$Initial;

    if-eqz v6, :cond_4

    .line 70
    aget-object v6, v4, v1

    check-cast v6, Lorg/apache/poi/ss/formula/ptg/ArrayPtg$Initial;

    invoke-virtual {v6, p1}, Lorg/apache/poi/ss/formula/ptg/ArrayPtg$Initial;->finishReading(Lorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/ptg/ArrayPtg;

    move-result-object v6

    aput-object v6, v4, v1

    .line 68
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 75
    .end local v1    # "i":I
    .end local v4    # "result":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_5
    invoke-static {v5}, Lorg/apache/poi/ss/formula/ptg/Ptg;->toPtgArray(Ljava/util/List;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v4

    goto :goto_2
.end method

.method public static serializePtgs([Lorg/apache/poi/ss/formula/ptg/Ptg;[BI)I
    .locals 8
    .param p0, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p1, "array"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 206
    array-length v3, p0

    .line 208
    .local v3, "nTokens":I
    new-instance v4, Lorg/apache/poi/util/LittleEndianByteArrayOutputStream;

    invoke-direct {v4, p1, p2}, Lorg/apache/poi/util/LittleEndianByteArrayOutputStream;-><init>([BI)V

    .line 210
    .local v4, "out":Lorg/apache/poi/util/LittleEndianByteArrayOutputStream;
    const/4 v0, 0x0

    .line 212
    .local v0, "arrayPtgs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 223
    if-eqz v0, :cond_0

    .line 224
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-lt v1, v7, :cond_4

    .line 229
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {v4}, Lorg/apache/poi/util/LittleEndianByteArrayOutputStream;->getWriteIndex()I

    move-result v7

    sub-int/2addr v7, p2

    return v7

    .line 213
    :cond_1
    aget-object v6, p0, v2

    .line 215
    .local v6, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {v6, v4}, Lorg/apache/poi/ss/formula/ptg/Ptg;->write(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 216
    instance-of v7, v6, Lorg/apache/poi/ss/formula/ptg/ArrayPtg;

    if-eqz v7, :cond_3

    .line 217
    if-nez v0, :cond_2

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "arrayPtgs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    const/4 v7, 0x5

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 220
    .restart local v0    # "arrayPtgs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    :cond_2
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 225
    .end local v6    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .restart local v1    # "i":I
    :cond_4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ss/formula/ptg/ArrayPtg;

    .line 226
    .local v5, "p":Lorg/apache/poi/ss/formula/ptg/ArrayPtg;
    invoke-virtual {v5, v4}, Lorg/apache/poi/ss/formula/ptg/ArrayPtg;->writeTokenValueBytes(Lorg/apache/poi/util/LittleEndianOutput;)I

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static toPtgArray(Ljava/util/List;)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/formula/ptg/Ptg;",
            ">;)[",
            "Lorg/apache/poi/ss/formula/ptg/Ptg;"
        }
    .end annotation

    .prologue
    .line 163
    .local p0, "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;

    .line 168
    :goto_0
    return-object v0

    .line 166
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    new-array v0, v1, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .line 167
    .local v0, "result":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public abstract getDefaultOperandClass()B
.end method

.method public final getPtgClass()B
    .locals 1

    .prologue
    .line 269
    iget-byte v0, p0, Lorg/apache/poi/ss/formula/ptg/Ptg;->ptgClass:B

    return v0
.end method

.method public final getRVAType()C
    .locals 3

    .prologue
    .line 277
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ptg/Ptg;->isBaseToken()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    const/16 v0, 0x2e

    .line 283
    :goto_0
    return v0

    .line 280
    :cond_0
    iget-byte v0, p0, Lorg/apache/poi/ss/formula/ptg/Ptg;->ptgClass:B

    sparse-switch v0, :sswitch_data_0

    .line 285
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown operand class ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p0, Lorg/apache/poi/ss/formula/ptg/Ptg;->ptgClass:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :sswitch_0
    const/16 v0, 0x52

    goto :goto_0

    .line 282
    :sswitch_1
    const/16 v0, 0x56

    goto :goto_0

    .line 283
    :sswitch_2
    const/16 v0, 0x41

    goto :goto_0

    .line 280
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x20 -> :sswitch_1
        0x40 -> :sswitch_2
    .end sparse-switch
.end method

.method public abstract getSize()I
.end method

.method public abstract isBaseToken()Z
.end method

.method public final setClass(B)V
    .locals 2
    .param p1, "thePtgClass"    # B

    .prologue
    .line 259
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ptg/Ptg;->isBaseToken()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "setClass should not be called on a base token"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_0
    iput-byte p1, p0, Lorg/apache/poi/ss/formula/ptg/Ptg;->ptgClass:B

    .line 263
    return-void
.end method

.method public abstract toFormulaString()Ljava/lang/String;
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract write(Lorg/apache/poi/util/LittleEndianOutput;)V
.end method
