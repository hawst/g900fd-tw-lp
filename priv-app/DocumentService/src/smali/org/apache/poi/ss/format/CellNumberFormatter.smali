.class public Lorg/apache/poi/ss/format/CellNumberFormatter;
.super Lorg/apache/poi/ss/format/CellFormatter;
.source "CellNumberFormatter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;,
        Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;,
        Lorg/apache/poi/ss/format/CellNumberFormatter$Special;,
        Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    }
.end annotation


# static fields
.field private static final SIMPLE_FLOAT:Lorg/apache/poi/ss/format/CellFormatter;

.field private static final SIMPLE_INT:Lorg/apache/poi/ss/format/CellFormatter;

.field static final SIMPLE_NUMBER:Lorg/apache/poi/ss/format/CellFormatter;


# instance fields
.field private afterFractional:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field private afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field private decimalFmt:Ljava/text/DecimalFormat;

.field private decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field private denominatorFmt:Ljava/lang/String;

.field private denominatorSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private final desc:Ljava/lang/String;

.field private exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field private exponentDigitSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private exponentSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private fractionalSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private improperFraction:Z

.field private integerCommas:Z

.field private integerSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private maxDenominator:I

.field private numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field private numeratorFmt:Ljava/lang/String;

.field private numeratorSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private printfFmt:Ljava/lang/String;

.field private scale:D

.field private slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

.field private final specials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lorg/apache/poi/ss/format/CellNumberFormatter$1;

    const-string/jumbo v1, "General"

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/format/CellNumberFormatter$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->SIMPLE_NUMBER:Lorg/apache/poi/ss/format/CellFormatter;

    .line 94
    new-instance v0, Lorg/apache/poi/ss/format/CellNumberFormatter;

    .line 95
    const-string/jumbo v1, "#"

    .line 94
    invoke-direct {v0, v1}, Lorg/apache/poi/ss/format/CellNumberFormatter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->SIMPLE_INT:Lorg/apache/poi/ss/format/CellFormatter;

    .line 96
    new-instance v0, Lorg/apache/poi/ss/format/CellNumberFormatter;

    .line 97
    const-string/jumbo v1, "#.#"

    .line 96
    invoke-direct {v0, v1}, Lorg/apache/poi/ss/format/CellNumberFormatter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->SIMPLE_FLOAT:Lorg/apache/poi/ss/format/CellFormatter;

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 14
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/format/CellFormatter;-><init>(Ljava/lang/String;)V

    .line 257
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    iput-wide v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    .line 259
    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 261
    new-instance v6, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;

    const/4 v11, 0x0

    invoke-direct {v6, p0, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;)V

    .line 263
    .local v6, "partHandler":Lorg/apache/poi/ss/format/CellNumberFormatter$NumPartHandler;
    sget-object v11, Lorg/apache/poi/ss/format/CellFormatType;->NUMBER:Lorg/apache/poi/ss/format/CellFormatType;

    .line 262
    invoke-static {p1, v11, v6}, Lorg/apache/poi/ss/format/CellFormatPart;->parseFormat(Ljava/lang/String;Lorg/apache/poi/ss/format/CellFormatType;Lorg/apache/poi/ss/format/CellFormatPart$PartHandler;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 266
    .local v0, "descBuf":Ljava/lang/StringBuffer;
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v11, :cond_0

    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v11, :cond_1

    :cond_0
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v11, :cond_1

    .line 267
    const/4 v11, 0x0

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 268
    const/4 v11, 0x0

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 271
    :cond_1
    invoke-direct {p0, v0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->interpretCommas(Ljava/lang/StringBuffer;)V

    .line 274
    const/4 v4, 0x0

    .line 275
    .local v4, "fractionPartWidth":I
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v11, :cond_4

    .line 276
    const/4 v7, 0x0

    .line 288
    .local v7, "precision":I
    :cond_2
    :goto_0
    if-nez v7, :cond_5

    .line 289
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 293
    :goto_1
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v11, :cond_6

    .line 294
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    .line 301
    :goto_2
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v11, :cond_7

    .line 302
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 303
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 321
    :goto_3
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    const/4 v12, 0x0

    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerEnd()I

    move-result v13

    invoke-interface {v11, v12, v13}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 323
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v11, :cond_a

    .line 324
    new-instance v3, Ljava/lang/StringBuffer;

    const-string/jumbo v11, "%"

    invoke-direct {v3, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 326
    .local v3, "fmtBuf":Ljava/lang/StringBuffer;
    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->calculateIntegerPartWidth()I

    move-result v5

    .line 327
    .local v5, "integerPartWidth":I
    add-int v10, v5, v4

    .line 329
    .local v10, "totalWidth":I
    const/16 v11, 0x30

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v11

    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 331
    const-string/jumbo v11, "f"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 332
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->printfFmt:Ljava/lang/String;

    .line 364
    .end local v5    # "integerPartWidth":I
    .end local v10    # "totalWidth":I
    :goto_4
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v11, :cond_3

    .line 366
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    .line 365
    iput-wide v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    .line 368
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->desc:Ljava/lang/String;

    .line 369
    return-void

    .line 278
    .end local v3    # "fmtBuf":Ljava/lang/StringBuffer;
    .end local v7    # "precision":I
    :cond_4
    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->interpretPrecision()I

    move-result v7

    .line 279
    .restart local v7    # "precision":I
    add-int/lit8 v4, v7, 0x1

    .line 280
    if-nez v7, :cond_2

    .line 283
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-interface {v11, v12}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 284
    const/4 v11, 0x0

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    goto/16 :goto_0

    .line 291
    :cond_5
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 292
    iget-object v13, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 291
    invoke-interface {v12, v13}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v12

    .line 292
    add-int/lit8 v12, v12, 0x1

    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalEnd()I

    move-result v13

    .line 291
    invoke-interface {v11, v12, v13}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    goto/16 :goto_1

    .line 296
    :cond_6
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-interface {v11, v12}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 297
    .local v1, "exponentPos":I
    const/4 v11, 0x2

    invoke-direct {p0, v1, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->specialsFor(II)Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    .line 298
    add-int/lit8 v11, v1, 0x2

    invoke-direct {p0, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->specialsFor(I)Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentDigitSpecials:Ljava/util/List;

    goto/16 :goto_2

    .line 305
    .end local v1    # "exponentPos":I
    :cond_7
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v11, :cond_8

    .line 306
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 310
    :goto_5
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-interface {v11, v12}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-direct {p0, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->specialsFor(I)Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 311
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 313
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    goto/16 :goto_3

    .line 308
    :cond_8
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v12, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    invoke-interface {v11, v12}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v11

    invoke-direct {p0, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->specialsFor(I)Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    goto :goto_5

    .line 315
    :cond_9
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    invoke-static {v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->maxValue(Ljava/util/List;)I

    move-result v11

    iput v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->maxDenominator:I

    .line 316
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    invoke-static {v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->singleNumberFormat(Ljava/util/List;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorFmt:Ljava/lang/String;

    .line 317
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    invoke-static {v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->singleNumberFormat(Ljava/util/List;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorFmt:Ljava/lang/String;

    goto/16 :goto_3

    .line 334
    :cond_a
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 335
    .restart local v3    # "fmtBuf":Ljava/lang/StringBuffer;
    const/4 v2, 0x1

    .line 336
    .local v2, "first":Z
    iget-object v9, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 337
    .local v9, "specialList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_e

    .line 339
    const-string/jumbo v11, "0"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 340
    const/4 v2, 0x0

    .line 348
    :cond_b
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_d

    .line 349
    const/16 v11, 0x2e

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 350
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_c
    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_11

    .line 358
    :cond_d
    const/16 v11, 0x45

    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 359
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    const/4 v12, 0x2

    .line 360
    iget-object v13, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    .line 359
    invoke-interface {v11, v12, v13}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    invoke-static {v3, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->placeZeros(Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 361
    new-instance v11, Ljava/text/DecimalFormat;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalFmt:Ljava/text/DecimalFormat;

    goto/16 :goto_4

    .line 342
    :cond_e
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_f
    :goto_7
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 343
    .local v8, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v8}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 344
    if-eqz v2, :cond_10

    const/16 v11, 0x23

    :goto_8
    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 345
    const/4 v2, 0x0

    goto :goto_7

    .line 344
    :cond_10
    const/16 v11, 0x30

    goto :goto_8

    .line 350
    .end local v8    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_11
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 351
    .restart local v8    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v8}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 352
    if-nez v2, :cond_12

    .line 353
    const/16 v12, 0x30

    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 354
    :cond_12
    const/4 v2, 0x0

    goto :goto_6
.end method

.method static synthetic access$0()Lorg/apache/poi/ss/format/CellFormatter;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->SIMPLE_INT:Lorg/apache/poi/ss/format/CellFormatter;

    return-object v0
.end method

.method static synthetic access$1()Lorg/apache/poi/ss/format/CellFormatter;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->SIMPLE_FLOAT:Lorg/apache/poi/ss/format/CellFormatter;

    return-object v0
.end method

.method static synthetic access$10(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-object v0
.end method

.method static synthetic access$11(Ljava/util/List;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 1

    .prologue
    .line 378
    invoke-static {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->firstDigit(Ljava/util/List;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lorg/apache/poi/ss/format/CellNumberFormatter;Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->improperFraction:Z

    return-void
.end method

.method static synthetic access$13(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-void
.end method

.method static synthetic access$14(Lorg/apache/poi/ss/format/CellNumberFormatter;)D
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    return-wide v0
.end method

.method static synthetic access$15(Lorg/apache/poi/ss/format/CellNumberFormatter;D)V
    .locals 1

    .prologue
    .line 46
    iput-wide p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    return-void
.end method

.method static synthetic access$2(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/poi/ss/format/CellNumberFormatter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-void
.end method

.method static synthetic access$5(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-object v0
.end method

.method static synthetic access$6(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-void
.end method

.method static synthetic access$7(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-object v0
.end method

.method static synthetic access$8(Lorg/apache/poi/ss/format/CellNumberFormatter;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 1

    .prologue
    .line 435
    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->previousNumber()Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lorg/apache/poi/ss/format/CellNumberFormatter;Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    return-void
.end method

.method private calculateIntegerPartWidth()I
    .locals 4

    .prologue
    .line 459
    iget-object v3, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 460
    .local v1, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    const/4 v0, 0x0

    .line 461
    .local v0, "digitCount":I
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 469
    :cond_1
    return v0

    .line 462
    :cond_2
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 464
    .local v2, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget-object v3, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eq v2, v3, :cond_1

    .line 466
    invoke-static {v2}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static deleteMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    .locals 1
    .param p0, "start"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p1, "startInclusive"    # Z
    .param p2, "end"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p3, "endInclusive"    # Z

    .prologue
    .line 393
    new-instance v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)V

    return-object v0
.end method

.method private static firstDigit(Ljava/util/List;)Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;)",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;"
        }
    .end annotation

    .prologue
    .line 379
    .local p0, "specials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 383
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 379
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 380
    .local v0, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private fractionalEnd()I
    .locals 3

    .prologue
    .line 552
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v1, :cond_0

    .line 553
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iput-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterFractional:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 558
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterFractional:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 560
    .local v0, "end":I
    :goto_1
    return v0

    .line 554
    .end local v0    # "end":I
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v1, :cond_1

    .line 555
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iput-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    goto :goto_0

    .line 557
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterFractional:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    goto :goto_0

    .line 558
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 559
    iget-object v2, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterFractional:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 558
    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method private static varargs hasChar(C[Ljava/util/List;)Z
    .locals 7
    .param p0, "ch"    # C
    .param p1, "numSpecials"    # [Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C[",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 848
    array-length v4, p1

    move v3, v2

    :goto_0
    if-lt v3, v4, :cond_0

    .line 855
    :goto_1
    return v2

    .line 848
    :cond_0
    aget-object v1, p1, v3

    .line 849
    .local v1, "specials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 848
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 849
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 850
    .local v0, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget-char v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    if-ne v6, p0, :cond_1

    .line 851
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private static varargs hasOnly(C[Ljava/util/List;)Z
    .locals 7
    .param p0, "ch"    # C
    .param p1, "numSpecials"    # [Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C[",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 859
    array-length v4, p1

    move v3, v2

    :goto_0
    if-lt v3, v4, :cond_0

    .line 866
    const/4 v2, 0x1

    :goto_1
    return v2

    .line 859
    :cond_0
    aget-object v1, p1, v3

    .line 860
    .local v1, "specials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 859
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 860
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 861
    .local v0, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget-char v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    if-eq v6, p0, :cond_1

    goto :goto_1
.end method

.method static insertMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    .locals 2
    .param p0, "special"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p1, "toAdd"    # Ljava/lang/CharSequence;
    .param p2, "where"    # I

    .prologue
    .line 387
    new-instance v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;ILorg/apache/poi/ss/format/CellNumberFormatter$StringMod;)V

    return-object v0
.end method

.method private integerEnd()I
    .locals 2

    .prologue
    .line 538
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 546
    :goto_0
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    return v0

    .line 540
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v0, :cond_1

    .line 541
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    goto :goto_0

    .line 542
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v0, :cond_2

    .line 543
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    goto :goto_0

    .line 545
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    goto :goto_0

    .line 546
    :cond_3
    iget-object v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 547
    iget-object v1, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->afterInteger:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 546
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method private interpretCommas(Ljava/lang/StringBuffer;)V
    .locals 10
    .param p1, "sb"    # Ljava/lang/StringBuffer;

    .prologue
    const-wide v8, 0x408f400000000000L    # 1000.0

    const/16 v6, 0x2c

    .line 494
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerEnd()I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 496
    .local v0, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    const/4 v3, 0x1

    .line 497
    .local v3, "stillScaling":Z
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerCommas:Z

    .line 498
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v4

    if-nez v4, :cond_2

    .line 511
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v4, :cond_0

    .line 512
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-direct {p0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalEnd()I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 513
    :goto_1
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v4

    if-nez v4, :cond_5

    .line 524
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 525
    const/4 v1, 0x0

    .line 526
    .local v1, "removed":I
    :cond_1
    :goto_2
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_6

    .line 535
    return-void

    .line 499
    .end local v1    # "removed":I
    :cond_2
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 500
    .local v2, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget-char v4, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    if-eq v4, v6, :cond_3

    .line 501
    const/4 v3, 0x0

    .line 502
    goto :goto_0

    .line 503
    :cond_3
    if-eqz v3, :cond_4

    .line 504
    iget-wide v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    div-double/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    goto :goto_0

    .line 506
    :cond_4
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerCommas:Z

    goto :goto_0

    .line 514
    .end local v2    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_5
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 515
    .restart local v2    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget-char v4, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    if-ne v4, v6, :cond_0

    .line 518
    iget-wide v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    div-double/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    goto :goto_1

    .line 527
    .end local v2    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .restart local v1    # "removed":I
    :cond_6
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 528
    .restart local v2    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget v4, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    sub-int/2addr v4, v1

    iput v4, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    .line 529
    iget-char v4, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    if-ne v4, v6, :cond_1

    .line 530
    add-int/lit8 v1, v1, 0x1

    .line 531
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    .line 532
    iget v4, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_2
.end method

.method private interpretPrecision()I
    .locals 6

    .prologue
    .line 473
    iget-object v3, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v3, :cond_1

    .line 474
    const/4 v1, -0x1

    .line 488
    :cond_0
    return v1

    .line 476
    :cond_1
    const/4 v1, 0x0

    .line 477
    .local v1, "precision":I
    iget-object v3, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 478
    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalPoint:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 477
    invoke-interface {v4, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 479
    .local v0, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 480
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 481
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 482
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 483
    .local v2, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v2}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 484
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z
    .locals 2
    .param p0, "s"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .prologue
    .line 432
    iget-char v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v1, 0x30

    if-eq v0, v1, :cond_0

    iget-char v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_0

    iget-char v0, p0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v1, 0x23

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static maxValue(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 408
    .local p0, "s":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method private static placeZeros(Ljava/lang/StringBuffer;Ljava/util/List;)V
    .locals 3
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 372
    .local p1, "specials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 376
    return-void

    .line 372
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 373
    .local v0, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 374
    const/16 v2, 0x30

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private previousNumber()Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .locals 6

    .prologue
    .line 436
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 437
    .local v0, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v4

    if-nez v4, :cond_2

    .line 455
    const/4 v2, 0x0

    :cond_1
    return-object v2

    .line 438
    :cond_2
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 439
    .local v3, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 440
    move-object v2, v3

    .line 441
    .local v2, "numStart":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    move-object v1, v3

    .line 442
    .local v1, "last":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 443
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    check-cast v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 444
    .restart local v3    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget v4, v1, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    iget v5, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    if-gt v4, v5, :cond_1

    .line 446
    invoke-static {v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 447
    move-object v2, v3

    .line 450
    move-object v1, v3

    goto :goto_0
.end method

.method static replaceMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;ZC)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    .locals 6
    .param p0, "start"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p1, "startInclusive"    # Z
    .param p2, "end"    # Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .param p3, "endInclusive"    # Z
    .param p4, "withChar"    # C

    .prologue
    .line 399
    new-instance v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;-><init>(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;ZC)V

    return-object v0
.end method

.method private static singleNumberFormat(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 404
    .local p0, "numSpecials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "%0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private specialsFor(I)Ljava/util/List;
    .locals 1
    .param p1, "pos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation

    .prologue
    .line 428
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/ss/format/CellNumberFormatter;->specialsFor(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private specialsFor(II)Ljava/util/List;
    .locals 6
    .param p1, "pos"    # I
    .param p2, "takeFirst"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt p1, v4, :cond_0

    .line 413
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 424
    :goto_0
    return-object v4

    .line 414
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    add-int v5, p1, p2

    invoke-interface {v4, v5}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    .line 415
    .local v1, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 416
    .local v2, "last":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    add-int v0, p1, p2

    .line 417
    .local v0, "end":I
    :goto_1
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 424
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    add-int/lit8 v5, v0, 0x1

    invoke-interface {v4, p1, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    goto :goto_0

    .line 418
    :cond_2
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 419
    .local v3, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-static {v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->isDigitFmt(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    iget v5, v2, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    if-gt v4, v5, :cond_1

    .line 421
    add-int/lit8 v0, v0, 0x1

    .line 422
    move-object v2, v3

    goto :goto_1
.end method

.method private writeFraction(DLjava/lang/StringBuffer;DLjava/lang/StringBuffer;Ljava/util/Set;)V
    .locals 20
    .param p1, "value"    # D
    .param p3, "result"    # Ljava/lang/StringBuffer;
    .param p4, "fractional"    # D
    .param p6, "output"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/lang/StringBuffer;",
            "D",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 776
    .local p7, "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->improperFraction:Z

    if-nez v2, :cond_4

    .line 779
    const-wide/16 v2, 0x0

    cmpl-double v2, p4, v2

    if-nez v2, :cond_1

    const/16 v2, 0x30

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 780
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p6

    move-object/from16 v6, p7

    invoke-direct/range {v2 .. v7}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 782
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 783
    .local v18, "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 784
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 783
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 785
    .local v13, "end":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    const/16 v2, 0x3f

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    .line 786
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    .line 785
    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    move-result v2

    .line 786
    if-eqz v2, :cond_0

    .line 788
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v5, 0x20

    move-object/from16 v0, v18

    invoke-static {v0, v2, v13, v3, v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->replaceMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;ZC)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 845
    .end local v13    # "end":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .end local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :goto_0
    return-void

    .line 791
    .restart local v13    # "end":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .restart local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-static {v0, v2, v13, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->deleteMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 798
    .end local v13    # "end":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .end local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_1
    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-nez v2, :cond_7

    const-wide/16 v2, 0x0

    cmpl-double v2, p4, v2

    if-nez v2, :cond_7

    const/4 v11, 0x1

    .line 799
    .local v11, "allZero":Z
    :goto_1
    const-wide/16 v2, 0x0

    cmpl-double v2, p4, v2

    if-nez v2, :cond_8

    const/16 v2, 0x30

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    .line 800
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    .line 799
    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    move-result v2

    .line 800
    if-nez v2, :cond_8

    .line 799
    const/16 v19, 0x0

    .line 801
    .local v19, "willShowFraction":Z
    :goto_2
    if-eqz v11, :cond_9

    const/16 v2, 0x23

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    .line 802
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    .line 801
    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasOnly(C[Ljava/util/List;)Z

    move-result v2

    .line 802
    if-nez v2, :cond_2

    const/16 v2, 0x30

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 801
    :cond_2
    const/16 v17, 0x1

    .line 804
    .local v17, "removeBecauseZero":Z
    :goto_3
    if-nez v11, :cond_a

    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-nez v2, :cond_a

    if-eqz v19, :cond_a

    .line 805
    const/16 v2, 0x30

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    .line 804
    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    move-result v2

    .line 805
    if-nez v2, :cond_a

    .line 803
    const/16 v16, 0x1

    .line 806
    .local v16, "removeBecauseFraction":Z
    :goto_4
    if-nez v17, :cond_3

    if-eqz v16, :cond_c

    .line 807
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 808
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 807
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 809
    .restart local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    const/16 v2, 0x3f

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/List;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lorg/apache/poi/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 810
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    const/4 v5, 0x0

    .line 811
    const/16 v6, 0x20

    .line 810
    move-object/from16 v0, v18

    invoke-static {v0, v2, v3, v5, v6}, Lorg/apache/poi/ss/format/CellNumberFormatter;->replaceMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;ZC)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 827
    .end local v11    # "allZero":Z
    .end local v16    # "removeBecauseFraction":Z
    .end local v17    # "removeBecauseZero":Z
    .end local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .end local v19    # "willShowFraction":Z
    :cond_4
    :goto_5
    const-wide/16 v2, 0x0

    cmpl-double v2, p4, v2

    if-eqz v2, :cond_5

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->improperFraction:Z

    if-eqz v2, :cond_d

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    rem-double v2, p4, v2

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-nez v2, :cond_d

    .line 829
    :cond_5
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v4, v2

    .line 830
    .local v4, "n":I
    const/4 v12, 0x1

    .line 836
    .local v12, "d":I
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->improperFraction:Z

    if-eqz v2, :cond_6

    .line 837
    int-to-long v2, v4

    int-to-double v6, v12

    mul-double v6, v6, p1

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    add-long/2addr v2, v6

    long-to-int v4, v2

    .line 838
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorFmt:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    move-object/from16 v2, p0

    move-object/from16 v5, p6

    move-object/from16 v7, p7

    invoke-direct/range {v2 .. v7}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeSingleInteger(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;)V

    .line 840
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorFmt:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    move-object/from16 v5, p0

    move v7, v12

    move-object/from16 v8, p6

    move-object/from16 v10, p7

    invoke-direct/range {v5 .. v10}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeSingleInteger(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 842
    .end local v4    # "n":I
    .end local v12    # "d":I
    :catch_0
    move-exception v15

    .line 843
    .local v15, "ignored":Ljava/lang/RuntimeException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 798
    .end local v15    # "ignored":Ljava/lang/RuntimeException;
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 799
    .restart local v11    # "allZero":Z
    :cond_8
    const/16 v19, 0x1

    goto/16 :goto_2

    .line 801
    .restart local v19    # "willShowFraction":Z
    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_3

    .line 803
    .restart local v17    # "removeBecauseZero":Z
    :cond_a
    const/16 v16, 0x0

    goto/16 :goto_4

    .line 813
    .restart local v16    # "removeBecauseFraction":Z
    .restart local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_b
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-static {v0, v2, v3, v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->deleteMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 817
    .end local v18    # "start":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p6

    move-object/from16 v6, p7

    invoke-direct/range {v2 .. v7}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    goto/16 :goto_5

    .line 832
    .end local v11    # "allZero":Z
    .end local v16    # "removeBecauseFraction":Z
    .end local v17    # "removeBecauseZero":Z
    .end local v19    # "willShowFraction":Z
    :cond_d
    :try_start_1
    new-instance v14, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->maxDenominator:I

    move-wide/from16 v0, p4

    invoke-direct {v14, v0, v1, v2}, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;-><init>(DI)V

    .line 833
    .local v14, "frac":Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;
    invoke-virtual {v14}, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->getNumerator()I

    move-result v4

    .line 834
    .restart local v4    # "n":I
    invoke-virtual {v14}, Lorg/apache/poi/ss/format/CellNumberFormatter$Fraction;->getDenominator()I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v12

    .restart local v12    # "d":I
    goto/16 :goto_6
.end method

.method private writeFractional(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "output"    # Ljava/lang/StringBuffer;

    .prologue
    const/16 v7, 0x30

    .line 946
    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 947
    const-string/jumbo v5, "."

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v0, v5, 0x1

    .line 948
    .local v0, "digit":I
    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v5, :cond_2

    .line 949
    const-string/jumbo v5, "e"

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    .line 952
    .local v4, "strip":I
    :goto_0
    if-le v4, v0, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    if-eq v5, v7, :cond_3

    .line 954
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 955
    .local v1, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 967
    .end local v0    # "digit":I
    .end local v1    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    .end local v4    # "strip":I
    :cond_1
    return-void

    .line 951
    .restart local v0    # "digit":I
    :cond_2
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    .line 952
    .restart local v4    # "strip":I
    goto :goto_0

    .line 953
    :cond_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 956
    .restart local v1    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    :cond_4
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 957
    .local v3, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    .line 958
    .local v2, "resultCh":C
    if-ne v2, v7, :cond_5

    iget-char v5, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    if-eq v5, v7, :cond_5

    if-ge v0, v4, :cond_7

    .line 959
    :cond_5
    iget v5, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    invoke-virtual {p2, v5, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 964
    :cond_6
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 960
    :cond_7
    iget-char v5, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v6, 0x3f

    if-ne v5, v6, :cond_6

    .line 962
    iget v5, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    const/16 v6, 0x20

    invoke-virtual {p2, v5, v6}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    goto :goto_2
.end method

.method private writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V
    .locals 13
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "output"    # Ljava/lang/StringBuffer;
    .param p5, "showCommas"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 882
    .local p3, "numSpecials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    .local p4, "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    const-string/jumbo v11, "."

    invoke-virtual {p1, v11}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v6, v11, -0x1

    .line 883
    .local v6, "pos":I
    if-gez v6, :cond_0

    .line 884
    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    move-object/from16 v0, p3

    if-ne v0, v11, :cond_4

    .line 885
    const-string/jumbo v11, "E"

    invoke-virtual {p1, v11}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v6, v11, -0x1

    .line 891
    :cond_0
    :goto_0
    const/4 v9, 0x0

    .local v9, "strip":I
    :goto_1
    if-lt v9, v6, :cond_5

    .line 897
    :cond_1
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    .line 898
    .local v4, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    const/4 v3, 0x0

    .line 899
    .local v3, "followWithComma":Z
    const/4 v5, 0x0

    .line 900
    .local v5, "lastOutputIntegerDigit":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    const/4 v1, 0x0

    .line 901
    .local v1, "digit":I
    :goto_2
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v11

    if-nez v11, :cond_7

    .line 924
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 925
    .local v2, "extraLeadingDigits":Ljava/lang/StringBuffer;
    if-ltz v6, :cond_3

    .line 927
    add-int/lit8 v6, v6, 0x1

    .line 928
    new-instance v2, Ljava/lang/StringBuffer;

    .end local v2    # "extraLeadingDigits":Ljava/lang/StringBuffer;
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v6}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 929
    .restart local v2    # "extraLeadingDigits":Ljava/lang/StringBuffer;
    if-eqz p5, :cond_2

    .line 930
    :goto_3
    if-gtz v6, :cond_10

    .line 938
    :cond_2
    const/4 v11, 0x1

    .line 937
    invoke-static {v5, v2, v11}, Lorg/apache/poi/ss/format/CellNumberFormatter;->insertMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 940
    :cond_3
    return-void

    .line 887
    .end local v1    # "digit":I
    .end local v2    # "extraLeadingDigits":Ljava/lang/StringBuffer;
    .end local v3    # "followWithComma":Z
    .end local v4    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    .end local v5    # "lastOutputIntegerDigit":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .end local v9    # "strip":I
    :cond_4
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    add-int/lit8 v6, v11, -0x1

    goto :goto_0

    .line 892
    .restart local v9    # "strip":I
    :cond_5
    invoke-virtual {p1, v9}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    .line 893
    .local v7, "resultCh":C
    const/16 v11, 0x30

    if-eq v7, v11, :cond_6

    const/16 v11, 0x2c

    if-ne v7, v11, :cond_1

    .line 891
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 903
    .end local v7    # "resultCh":C
    .restart local v1    # "digit":I
    .restart local v3    # "followWithComma":Z
    .restart local v4    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    .restart local v5    # "lastOutputIntegerDigit":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_7
    if-ltz v6, :cond_c

    .line 904
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    .line 909
    .restart local v7    # "resultCh":C
    :goto_4
    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 910
    .local v8, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    if-eqz p5, :cond_d

    if-lez v1, :cond_d

    rem-int/lit8 v11, v1, 0x3

    if-nez v11, :cond_d

    const/4 v3, 0x1

    .line 911
    :goto_5
    const/4 v10, 0x0

    .line 912
    .local v10, "zeroStrip":Z
    const/16 v11, 0x30

    if-ne v7, v11, :cond_8

    iget-char v11, v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v12, 0x30

    if-eq v11, v12, :cond_8

    iget-char v11, v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v12, 0x3f

    if-eq v11, v12, :cond_8

    if-lt v6, v9, :cond_a

    .line 913
    :cond_8
    iget-char v11, v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    const/16 v12, 0x3f

    if-ne v11, v12, :cond_e

    if-ge v6, v9, :cond_e

    const/4 v10, 0x1

    .line 914
    :goto_6
    iget v11, v8, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    if-eqz v10, :cond_9

    const/16 v7, 0x20

    .end local v7    # "resultCh":C
    :cond_9
    invoke-virtual {p2, v11, v7}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 915
    move-object v5, v8

    .line 917
    :cond_a
    if-eqz v3, :cond_b

    .line 918
    if-eqz v10, :cond_f

    const-string/jumbo v11, " "

    :goto_7
    const/4 v12, 0x2

    invoke-static {v8, v11, v12}, Lorg/apache/poi/ss/format/CellNumberFormatter;->insertMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 919
    const/4 v3, 0x0

    .line 921
    :cond_b
    add-int/lit8 v1, v1, 0x1

    .line 922
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_2

    .line 907
    .end local v8    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .end local v10    # "zeroStrip":Z
    :cond_c
    const/16 v7, 0x30

    .restart local v7    # "resultCh":C
    goto :goto_4

    .line 910
    .restart local v8    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    :cond_d
    const/4 v3, 0x0

    goto :goto_5

    .line 913
    .restart local v10    # "zeroStrip":Z
    :cond_e
    const/4 v10, 0x0

    goto :goto_6

    .line 918
    .end local v7    # "resultCh":C
    :cond_f
    const-string/jumbo v11, ","

    goto :goto_7

    .line 931
    .end local v8    # "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    .end local v10    # "zeroStrip":Z
    .restart local v2    # "extraLeadingDigits":Ljava/lang/StringBuffer;
    :cond_10
    if-lez v1, :cond_11

    rem-int/lit8 v11, v1, 0x3

    if-nez v11, :cond_11

    .line 932
    const/16 v11, 0x2c

    invoke-virtual {v2, v6, v11}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 933
    :cond_11
    add-int/lit8 v1, v1, 0x1

    .line 934
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_3
.end method

.method private writeScientific(DLjava/lang/StringBuffer;Ljava/util/Set;)V
    .locals 17
    .param p1, "value"    # D
    .param p3, "output"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 699
    .local p4, "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 700
    .local v3, "result":Ljava/lang/StringBuffer;
    new-instance v14, Ljava/text/FieldPosition;

    .line 701
    const/4 v2, 0x1

    .line 700
    invoke-direct {v14, v2}, Ljava/text/FieldPosition;-><init>(I)V

    .line 702
    .local v14, "fractionPos":Ljava/text/FieldPosition;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->decimalFmt:Ljava/text/DecimalFormat;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1, v3, v14}, Ljava/text/DecimalFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 703
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerCommas:Z

    move-object/from16 v2, p0

    move-object/from16 v4, p3

    move-object/from16 v6, p4

    invoke-direct/range {v2 .. v7}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 704
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeFractional(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 743
    invoke-virtual {v14}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v10

    .line 744
    .local v10, "ePos":I
    add-int/lit8 v16, v10, 0x1

    .line 745
    .local v16, "signPos":I
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    .line 746
    .local v13, "expSignRes":C
    const/16 v2, 0x2d

    if-eq v13, v2, :cond_0

    .line 748
    const/16 v13, 0x2b

    .line 751
    const/16 v2, 0x2b

    move/from16 v0, v16

    invoke-virtual {v3, v0, v2}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 755
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v15

    .line 756
    .local v15, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {v15}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 757
    .local v11, "expSign":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    iget-char v12, v11, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->ch:C

    .line 761
    .local v12, "expSignFmt":C
    const/16 v2, 0x2d

    if-eq v13, v2, :cond_1

    const/16 v2, 0x2b

    if-ne v12, v2, :cond_2

    .line 762
    :cond_1
    const/4 v2, 0x1

    const/4 v4, 0x1

    invoke-static {v11, v2, v11, v4, v13}, Lorg/apache/poi/ss/format/CellNumberFormatter;->replaceMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;ZC)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 766
    :goto_0
    new-instance v5, Ljava/lang/StringBuffer;

    .line 767
    add-int/lit8 v2, v16, 0x1

    .line 766
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 768
    .local v5, "exponentNum":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponentDigitSpecials:Ljava/util/List;

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-object/from16 v6, p3

    move-object/from16 v8, p4

    invoke-direct/range {v4 .. v9}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 769
    return-void

    .line 764
    .end local v5    # "exponentNum":Ljava/lang/StringBuffer;
    :cond_2
    const/4 v2, 0x1

    const/4 v4, 0x1

    invoke-static {v11, v2, v11, v4}, Lorg/apache/poi/ss/format/CellNumberFormatter;->deleteMod(Lorg/apache/poi/ss/format/CellNumberFormatter$Special;ZLorg/apache/poi/ss/format/CellNumberFormatter$Special;Z)Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private writeSingleInteger(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;)V
    .locals 7
    .param p1, "fmt"    # Ljava/lang/String;
    .param p2, "num"    # I
    .param p3, "output"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$Special;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "numSpecials":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    .local p5, "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    const/4 v5, 0x0

    .line 872
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 873
    .local v1, "sb":Ljava/lang/StringBuffer;
    new-instance v6, Ljava/util/Formatter;

    invoke-direct {v6, v1}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 874
    .local v6, "formatter":Ljava/util/Formatter;
    sget-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->LOCALE:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v6, v0, p1, v2}, Ljava/util/Formatter;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    .line 875
    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 876
    return-void
.end method


# virtual methods
.method public formatValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 34
    .param p1, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p2, "valueObject"    # Ljava/lang/Object;

    .prologue
    .line 565
    check-cast p2, Ljava/lang/Number;

    .end local p2    # "valueObject":Ljava/lang/Object;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    .line 566
    .local v4, "value":D
    move-object/from16 v0, p0

    iget-wide v13, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->scale:D

    mul-double/2addr v4, v13

    .line 575
    const-wide/16 v13, 0x0

    cmpg-double v3, v4, v13

    if-gez v3, :cond_4

    const/16 v30, 0x1

    .line 576
    .local v30, "negative":Z
    :goto_0
    if-eqz v30, :cond_0

    .line 577
    neg-double v4, v4

    .line 580
    :cond_0
    const-wide/16 v7, 0x0

    .line 581
    .local v7, "fractional":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->slash:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v3, :cond_1

    .line 582
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->improperFraction:Z

    if-eqz v3, :cond_5

    .line 583
    move-wide v7, v4

    .line 584
    const-wide/16 v4, 0x0

    .line 592
    :cond_1
    :goto_1
    new-instance v10, Ljava/util/TreeSet;

    invoke-direct {v10}, Ljava/util/TreeSet;-><init>()V

    .line 593
    .local v10, "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    new-instance v9, Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->desc:Ljava/lang/String;

    invoke-direct {v9, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 595
    .local v9, "output":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->exponent:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-eqz v3, :cond_6

    .line 596
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v9, v10}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeScientific(DLjava/lang/StringBuffer;Ljava/util/Set;)V

    .line 614
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v26

    .line 615
    .local v26, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 616
    .local v19, "changes":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    move-object/from16 v31, v3

    .line 617
    .local v31, "nextChange":Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    :goto_3
    const/16 v17, 0x0

    .line 618
    .local v17, "adjust":I
    new-instance v22, Ljava/util/BitSet;

    invoke-direct/range {v22 .. v22}, Ljava/util/BitSet;-><init>()V

    .line 619
    .local v22, "deletedChars":Ljava/util/BitSet;
    :cond_2
    invoke-interface/range {v26 .. v26}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_a

    .line 691
    if-eqz v30, :cond_3

    .line 692
    const/16 v3, 0x2d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 693
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 694
    return-void

    .line 575
    .end local v7    # "fractional":D
    .end local v9    # "output":Ljava/lang/StringBuffer;
    .end local v10    # "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    .end local v17    # "adjust":I
    .end local v19    # "changes":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    .end local v22    # "deletedChars":Ljava/util/BitSet;
    .end local v26    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    .end local v30    # "negative":Z
    .end local v31    # "nextChange":Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    :cond_4
    const/16 v30, 0x0

    goto :goto_0

    .line 586
    .restart local v7    # "fractional":D
    .restart local v30    # "negative":Z
    :cond_5
    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    rem-double v7, v4, v13

    .line 588
    double-to-long v13, v4

    long-to-double v4, v13

    goto :goto_1

    .line 597
    .restart local v9    # "output":Ljava/lang/StringBuffer;
    .restart local v10    # "mods":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->improperFraction:Z

    if-eqz v3, :cond_7

    .line 598
    const/4 v6, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeFraction(DLjava/lang/StringBuffer;DLjava/lang/StringBuffer;Ljava/util/Set;)V

    goto :goto_2

    .line 600
    :cond_7
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 601
    .local v12, "result":Ljava/lang/StringBuffer;
    new-instance v23, Ljava/util/Formatter;

    move-object/from16 v0, v23

    invoke-direct {v0, v12}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 602
    .local v23, "f":Ljava/util/Formatter;
    sget-object v3, Lorg/apache/poi/ss/format/CellNumberFormatter;->LOCALE:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->printfFmt:Ljava/lang/String;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v11, v13

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v6, v11}, Ljava/util/Formatter;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 604
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->numerator:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    if-nez v3, :cond_8

    .line 605
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v9}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeFractional(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 606
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 607
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->integerCommas:Z

    move/from16 v16, v0

    move-object/from16 v11, p0

    move-object v13, v9

    move-object v15, v10

    .line 606
    invoke-direct/range {v11 .. v16}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v3, p0

    move-object v6, v12

    .line 609
    invoke-direct/range {v3 .. v10}, Lorg/apache/poi/ss/format/CellNumberFormatter;->writeFraction(DLjava/lang/StringBuffer;DLjava/lang/StringBuffer;Ljava/util/Set;)V

    goto/16 :goto_2

    .line 616
    .end local v12    # "result":Ljava/lang/StringBuffer;
    .end local v23    # "f":Ljava/util/Formatter;
    .restart local v19    # "changes":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;>;"
    .restart local v26    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/ss/format/CellNumberFormatter$Special;>;"
    :cond_9
    const/16 v31, 0x0

    goto :goto_3

    .line 620
    .restart local v17    # "adjust":I
    .restart local v22    # "deletedChars":Ljava/util/BitSet;
    .restart local v31    # "nextChange":Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    :cond_a
    invoke-interface/range {v26 .. v26}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    .line 621
    .local v33, "s":Lorg/apache/poi/ss/format/CellNumberFormatter$Special;
    move-object/from16 v0, v33

    iget v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    add-int v18, v3, v17

    .line 622
    .local v18, "adjustedPos":I
    move-object/from16 v0, v33

    iget v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-nez v3, :cond_b

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    const/16 v6, 0x23

    if-ne v3, v6, :cond_b

    .line 623
    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 624
    add-int/lit8 v17, v17, -0x1

    .line 625
    move-object/from16 v0, v33

    iget v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    .line 627
    :cond_b
    :goto_4
    if-eqz v31, :cond_2

    move-object/from16 v0, v31

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->special:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    move-object/from16 v0, v33

    if-ne v0, v3, :cond_2

    .line 628
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v27

    .line 629
    .local v27, "lenBefore":I
    move-object/from16 v0, v33

    iget v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    add-int v29, v3, v17

    .line 630
    .local v29, "modPos":I
    const/16 v32, 0x0

    .line 631
    .local v32, "posTweak":I
    move-object/from16 v0, v31

    iget v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    packed-switch v3, :pswitch_data_0

    .line 678
    new-instance v3, Ljava/lang/IllegalStateException;

    .line 679
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Unknown op: "

    invoke-direct {v6, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    iget v11, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->op:I

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 678
    invoke-direct {v3, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 634
    :pswitch_0
    move-object/from16 v0, v31

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    const-string/jumbo v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    move-object/from16 v0, v33

    iget v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 681
    :cond_c
    :goto_5
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    sub-int v3, v3, v27

    add-int v17, v17, v3

    .line 683
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 684
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    .end local v31    # "nextChange":Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    check-cast v31, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;

    .restart local v31    # "nextChange":Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;
    goto :goto_4

    .line 636
    :cond_d
    const/16 v32, 0x1

    .line 639
    :pswitch_1
    add-int v3, v29, v32

    move-object/from16 v0, v31

    iget-object v6, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    invoke-virtual {v9, v3, v6}, Ljava/lang/StringBuffer;->insert(ILjava/lang/CharSequence;)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 644
    :pswitch_2
    move-object/from16 v0, v33

    iget v0, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    move/from16 v21, v0

    .line 645
    .local v21, "delPos":I
    move-object/from16 v0, v31

    iget-boolean v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->startInclusive:Z

    if-nez v3, :cond_e

    .line 646
    add-int/lit8 v21, v21, 0x1

    .line 647
    add-int/lit8 v29, v29, 0x1

    .line 651
    :cond_e
    :goto_6
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-nez v3, :cond_11

    .line 657
    move-object/from16 v0, v31

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->end:Lorg/apache/poi/ss/format/CellNumberFormatter$Special;

    iget v0, v3, Lorg/apache/poi/ss/format/CellNumberFormatter$Special;->pos:I

    move/from16 v20, v0

    .line 658
    .local v20, "delEndPos":I
    move-object/from16 v0, v31

    iget-boolean v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->endInclusive:Z

    if-eqz v3, :cond_f

    .line 659
    add-int/lit8 v20, v20, 0x1

    .line 662
    :cond_f
    add-int v28, v20, v17

    .line 664
    .local v28, "modEndPos":I
    move/from16 v0, v29

    move/from16 v1, v28

    if-ge v0, v1, :cond_c

    .line 666
    move-object/from16 v0, v31

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_12

    .line 667
    move/from16 v0, v29

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 673
    :cond_10
    move-object/from16 v0, v22

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->set(II)V

    goto :goto_5

    .line 652
    .end local v20    # "delEndPos":I
    .end local v28    # "modEndPos":I
    :cond_11
    add-int/lit8 v21, v21, 0x1

    .line 653
    add-int/lit8 v29, v29, 0x1

    goto :goto_6

    .line 669
    .restart local v20    # "delEndPos":I
    .restart local v28    # "modEndPos":I
    :cond_12
    move-object/from16 v0, v31

    iget-object v3, v0, Lorg/apache/poi/ss/format/CellNumberFormatter$StringMod;->toAdd:Ljava/lang/CharSequence;

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v24

    .line 670
    .local v24, "fillCh":C
    move/from16 v25, v29

    .local v25, "i":I
    :goto_7
    move/from16 v0, v25

    move/from16 v1, v28

    if-ge v0, v1, :cond_10

    .line 671
    move/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 670
    add-int/lit8 v25, v25, 0x1

    goto :goto_7

    .line 686
    .end local v20    # "delEndPos":I
    .end local v21    # "delPos":I
    .end local v24    # "fillCh":C
    .end local v25    # "i":I
    .end local v28    # "modEndPos":I
    :cond_13
    const/16 v31, 0x0

    goto/16 :goto_4

    .line 631
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public simpleValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 1
    .param p1, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 976
    sget-object v0, Lorg/apache/poi/ss/format/CellNumberFormatter;->SIMPLE_NUMBER:Lorg/apache/poi/ss/format/CellFormatter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/ss/format/CellFormatter;->formatValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 977
    return-void
.end method
