.class public abstract Lorg/apache/poi/ss/formula/eval/RefEvalBase;
.super Ljava/lang/Object;
.source "RefEvalBase.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/eval/RefEval;


# instance fields
.field private final _columnIndex:I

.field private final _rowIndex:I


# direct methods
.method protected constructor <init>(II)V
    .locals 0
    .param p1, "rowIndex"    # I
    .param p2, "columnIndex"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lorg/apache/poi/ss/formula/eval/RefEvalBase;->_rowIndex:I

    .line 32
    iput p2, p0, Lorg/apache/poi/ss/formula/eval/RefEvalBase;->_columnIndex:I

    .line 33
    return-void
.end method


# virtual methods
.method public final getColumn()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lorg/apache/poi/ss/formula/eval/RefEvalBase;->_columnIndex:I

    return v0
.end method

.method public final getRow()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lorg/apache/poi/ss/formula/eval/RefEvalBase;->_rowIndex:I

    return v0
.end method
