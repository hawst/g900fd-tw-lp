.class Lorg/apache/poi/ss/formula/functions/NumericFunction$12;
.super Lorg/apache/poi/ss/formula/functions/NumericFunction$OneArg;
.source "NumericFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/formula/functions/NumericFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/functions/NumericFunction$OneArg;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method protected evaluate(D)D
    .locals 3
    .param p1, "d"    # D

    .prologue
    .line 199
    const-wide v0, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method
