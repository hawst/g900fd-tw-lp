.class public final Lorg/apache/poi/ss/formula/FormulaParser;
.super Ljava/lang/Object;
.source "FormulaParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/formula/FormulaParser$Identifier;,
        Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;,
        Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    }
.end annotation


# static fields
.field private static final CELL_REF_PATTERN:Ljava/util/regex/Pattern;

.field private static final CR:C = '\r'

.field private static final LF:C = '\n'

.field private static final TAB:C = '\t'


# instance fields
.field private _book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

.field private final _formulaLength:I

.field private final _formulaString:Ljava/lang/String;

.field private _pointer:I

.field private _rootNode:Lorg/apache/poi/ss/formula/ParseNode;

.field private _sheetIndex:I

.field private _ssVersion:Lorg/apache/poi/ss/SpreadsheetVersion;

.field private look:C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 658
    const-string/jumbo v0, "(\\$?[A-Za-z]+)?(\\$?[0-9]+)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/formula/FormulaParser;->CELL_REF_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;I)V
    .locals 1
    .param p1, "formula"    # Ljava/lang/String;
    .param p2, "book"    # Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;
    .param p3, "sheetIndex"    # I

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 156
    iput-object p2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    .line 157
    if-nez p2, :cond_0

    sget-object v0, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    :goto_0
    iput-object v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_ssVersion:Lorg/apache/poi/ss/SpreadsheetVersion;

    .line 158
    iget-object v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaLength:I

    .line 159
    iput p3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_sheetIndex:I

    .line 160
    return-void

    .line 157
    :cond_0
    invoke-interface {p2}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getSpreadsheetVersion()Lorg/apache/poi/ss/SpreadsheetVersion;

    move-result-object v0

    goto :goto_0
.end method

.method private Arguments()[Lorg/apache/poi/ss/formula/ParseNode;
    .locals 6

    .prologue
    const/16 v5, 0x29

    .line 1032
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1033
    .local v2, "temp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ParseNode;>;"
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1034
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v3, v5, :cond_0

    .line 1035
    sget-object v1, Lorg/apache/poi/ss/formula/ParseNode;->EMPTY_ARRAY:[Lorg/apache/poi/ss/formula/ParseNode;

    .line 1064
    :goto_0
    return-object v1

    .line 1038
    :cond_0
    const/4 v0, 0x1

    .line 1041
    .local v0, "missedPrevArg":Z
    :cond_1
    :goto_1
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1042
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v3}, Lorg/apache/poi/ss/formula/FormulaParser;->isArgumentDelimiter(C)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1043
    if-eqz v0, :cond_2

    .line 1044
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    sget-object v4, Lorg/apache/poi/ss/formula/ptg/MissingArgPtg;->instance:Lorg/apache/poi/ss/formula/ptg/Ptg;

    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047
    :cond_2
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v3, v5, :cond_3

    .line 1062
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v1, v3, [Lorg/apache/poi/ss/formula/ParseNode;

    .line 1063
    .local v1, "result":[Lorg/apache/poi/ss/formula/ParseNode;
    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0

    .line 1050
    .end local v1    # "result":[Lorg/apache/poi/ss/formula/ParseNode;
    :cond_3
    const/16 v3, 0x2c

    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1051
    const/4 v0, 0x1

    .line 1052
    goto :goto_1

    .line 1054
    :cond_4
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->comparisonExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1056
    const/4 v0, 0x0

    .line 1057
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1058
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v3}, Lorg/apache/poi/ss/formula/FormulaParser;->isArgumentDelimiter(C)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1059
    const-string/jumbo v3, "\',\' or \')\'"

    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v3

    throw v3
.end method

.method private GetChar()V
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaLength:I

    if-le v0, v1, :cond_0

    .line 185
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "too far"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    iget v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaLength:I

    if-ge v0, v1, :cond_1

    .line 188
    iget-object v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    .line 194
    :goto_0
    iget v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 196
    return-void

    .line 192
    :cond_1
    const/4 v0, 0x0

    iput-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    goto :goto_0
.end method

.method private GetNum()Ljava/lang/String;
    .locals 2

    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 261
    .local v0, "value":Ljava/lang/StringBuffer;
    :goto_0
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v1}, Lorg/apache/poi/ss/formula/FormulaParser;->IsDigit(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 265
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 262
    :cond_0
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 263
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_0

    .line 265
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private static IsAlpha(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 225
    invoke-static {p0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x24

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5f

    if-eq p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static IsDigit(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 230
    invoke-static {p0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    return v0
.end method

.method private static IsWhite(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 235
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Match(C)V
    .locals 2
    .param p1, "x"    # C

    .prologue
    .line 251
    iget-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v0, p1, :cond_0

    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 254
    :cond_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 255
    return-void
.end method

.method private SkipWhite()V
    .locals 1

    .prologue
    .line 240
    :goto_0
    iget-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v0}, Lorg/apache/poi/ss/formula/FormulaParser;->IsWhite(C)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    return-void

    .line 241
    :cond_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_0
.end method

.method private Term()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 5

    .prologue
    .line 1432
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->powerFactor()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v2

    .line 1434
    .local v2, "result":Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1436
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    sparse-switch v4, :sswitch_data_0

    .line 1446
    return-object v2

    .line 1438
    :sswitch_0
    const/16 v4, 0x2a

    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1439
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/MultiplyPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    .line 1448
    .local v0, "operator":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_1
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->powerFactor()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1449
    .local v1, "other":Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v3, v0, v2, v1}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v3, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v2, v3

    .line 1433
    .end local v3    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0

    .line 1442
    .end local v0    # "operator":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .end local v1    # "other":Lorg/apache/poi/ss/formula/ParseNode;
    :sswitch_1
    const/16 v4, 0x2f

    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1443
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/DividePtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    .line 1444
    .restart local v0    # "operator":Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_1

    .line 1436
    nop

    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_0
        0x2f -> :sswitch_1
    .end sparse-switch
.end method

.method private additiveExpression()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 5

    .prologue
    .line 1532
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->Term()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v2

    .line 1534
    .local v2, "result":Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1536
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    packed-switch v4, :pswitch_data_0

    .line 1546
    :pswitch_0
    return-object v2

    .line 1538
    :pswitch_1
    const/16 v4, 0x2b

    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1539
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/AddPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    .line 1548
    .local v0, "operator":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_1
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->Term()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1549
    .local v1, "other":Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v3, v0, v2, v1}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v3, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v2, v3

    .line 1533
    .end local v3    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0

    .line 1542
    .end local v0    # "operator":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .end local v1    # "other":Lorg/apache/poi/ss/formula/ParseNode;
    :pswitch_2
    const/16 v4, 0x2d

    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1543
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/SubtractPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    .line 1544
    .restart local v0    # "operator":Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_1

    .line 1536
    nop

    :pswitch_data_0
    .packed-switch 0x2b
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static augmentWithMemPtg(Lorg/apache/poi/ss/formula/ParseNode;)Lorg/apache/poi/ss/formula/ParseNode;
    .locals 2
    .param p0, "root"    # Lorg/apache/poi/ss/formula/ParseNode;

    .prologue
    .line 295
    invoke-static {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->needsMemFunc(Lorg/apache/poi/ss/formula/ParseNode;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;

    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ParseNode;->getEncodedSize()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;-><init>(I)V

    .line 300
    .local v0, "memPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_0
    new-instance v1, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v1, v0, p0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;)V

    return-object v1

    .line 298
    .end local v0    # "memPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_0
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/MemAreaPtg;

    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ParseNode;->getEncodedSize()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/ptg/MemAreaPtg;-><init>(I)V

    .restart local v0    # "memPtg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_0
.end method

.method private checkRowLengths([[Ljava/lang/Object;I)V
    .locals 5
    .param p1, "values2d"    # [[Ljava/lang/Object;
    .param p2, "nColumns"    # I

    .prologue
    .line 1182
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_0

    .line 1189
    return-void

    .line 1183
    :cond_0
    aget-object v2, p1, v0

    array-length v1, v2

    .line 1184
    .local v1, "rowLen":I
    if-eq v1, p2, :cond_1

    .line 1185
    new-instance v2, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Array row "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " has length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1186
    const-string/jumbo v4, " but row 0 has length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1185
    invoke-direct {v2, v3}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1182
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static checkValidRangeOperand(Ljava/lang/String;ILorg/apache/poi/ss/formula/ParseNode;)V
    .locals 3
    .param p0, "sideName"    # Ljava/lang/String;
    .param p1, "currentParsePosition"    # I
    .param p2, "pn"    # Lorg/apache/poi/ss/formula/ParseNode;

    .prologue
    .line 343
    invoke-static {p2}, Lorg/apache/poi/ss/formula/FormulaParser;->isValidRangeOperand(Lorg/apache/poi/ss/formula/ParseNode;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 345
    const-string/jumbo v2, " of the range operator \':\' at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 346
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not a proper reference."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 344
    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_0
    return-void
.end method

.method private comparisonExpression()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 5

    .prologue
    .line 1473
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->concatExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v2

    .line 1475
    .local v2, "result":Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1476
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    packed-switch v4, :pswitch_data_0

    .line 1485
    return-object v2

    .line 1480
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->getComparisonToken()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    .line 1481
    .local v0, "comparisonToken":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->concatExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1482
    .local v1, "other":Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v3, v0, v2, v1}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v3, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v2, v3

    .line 1483
    .end local v3    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0

    .line 1476
    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private concatExpression()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 5

    .prologue
    const/16 v4, 0x26

    .line 1516
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->additiveExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1518
    .local v1, "result":Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1519
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v3, v4, :cond_0

    .line 1526
    return-object v1

    .line 1522
    :cond_0
    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1523
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->additiveExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 1524
    .local v0, "other":Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v2, Lorg/apache/poi/ss/formula/ParseNode;

    sget-object v3, Lorg/apache/poi/ss/formula/ptg/ConcatPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v1    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v2, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v1, v2

    .line 1517
    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v1    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0
.end method

.method private static convertArrayNumber(Lorg/apache/poi/ss/formula/ptg/Ptg;Z)Ljava/lang/Double;
    .locals 5
    .param p0, "ptg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p1, "isPositive"    # Z

    .prologue
    .line 1245
    instance-of v2, p0, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    if-eqz v2, :cond_1

    .line 1246
    check-cast p0, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    .end local p0    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ptg/IntPtg;->getValue()I

    move-result v2

    int-to-double v0, v2

    .line 1252
    .local v0, "value":D
    :goto_0
    if-nez p1, :cond_0

    .line 1253
    neg-double v0, v0

    .line 1255
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    return-object v2

    .line 1247
    .end local v0    # "value":D
    .restart local p0    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_1
    instance-of v2, p0, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    if-eqz v2, :cond_2

    .line 1248
    check-cast p0, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    .end local p0    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;->getValue()D

    move-result-wide v0

    .line 1249
    .restart local v0    # "value":D
    goto :goto_0

    .line 1250
    .end local v0    # "value":D
    .restart local p0    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_2
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unexpected ptg ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static createAreaRef(Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/util/AreaReference;
    .locals 3
    .param p0, "part1"    # Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .param p1, "part2"    # Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;

    .prologue
    .line 640
    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isCompatibleForArea(Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 641
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "has incompatible parts: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 642
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' and \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 641
    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 644
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 645
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/poi/ss/util/AreaReference;->getWholeRow(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/AreaReference;

    move-result-object v0

    .line 650
    :goto_0
    return-object v0

    .line 647
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isColumn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 648
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/poi/ss/util/AreaReference;->getWholeColumn(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/util/AreaReference;

    move-result-object v0

    goto :goto_0

    .line 650
    :cond_2
    new-instance v0, Lorg/apache/poi/ss/util/AreaReference;

    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getCellReference()Lorg/apache/poi/ss/util/CellReference;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getCellReference()Lorg/apache/poi/ss/util/CellReference;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/util/AreaReference;-><init>(Lorg/apache/poi/ss/util/CellReference;Lorg/apache/poi/ss/util/CellReference;)V

    goto :goto_0
.end method

.method private createAreaRefParseNode(Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/formula/ParseNode;
    .locals 7
    .param p1, "sheetIden"    # Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;
    .param p2, "part1"    # Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .param p3, "part2"    # Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/ss/formula/FormulaParseException;
        }
    .end annotation

    .prologue
    .line 609
    if-nez p1, :cond_0

    .line 610
    const/high16 v2, -0x80000000

    .line 620
    .local v2, "extIx":I
    :goto_0
    if-nez p3, :cond_3

    .line 621
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getCellReference()Lorg/apache/poi/ss/util/CellReference;

    move-result-object v1

    .line 622
    .local v1, "cr":Lorg/apache/poi/ss/util/CellReference;
    if-nez p1, :cond_2

    .line 623
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/RefPtg;

    invoke-direct {v3, v1}, Lorg/apache/poi/ss/formula/ptg/RefPtg;-><init>(Lorg/apache/poi/ss/util/CellReference;)V

    .line 636
    .end local v1    # "cr":Lorg/apache/poi/ss/util/CellReference;
    .local v3, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_1
    new-instance v5, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v5, v3}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    return-object v5

    .line 612
    .end local v2    # "extIx":I
    .end local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->getSheetIdentifier()Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;->getName()Ljava/lang/String;

    move-result-object v4

    .line 613
    .local v4, "sName":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->getBookName()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 614
    iget-object v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    invoke-interface {v5, v4}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getExternalSheetIndex(Ljava/lang/String;)I

    move-result v2

    .line 615
    .restart local v2    # "extIx":I
    goto :goto_0

    .line 616
    .end local v2    # "extIx":I
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->getBookName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .restart local v2    # "extIx":I
    goto :goto_0

    .line 625
    .end local v4    # "sName":Ljava/lang/String;
    .restart local v1    # "cr":Lorg/apache/poi/ss/util/CellReference;
    :cond_2
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    invoke-direct {v3, v1, v2}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;-><init>(Lorg/apache/poi/ss/util/CellReference;I)V

    .line 627
    .restart local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_1

    .line 628
    .end local v1    # "cr":Lorg/apache/poi/ss/util/CellReference;
    .end local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_3
    invoke-static {p2, p3}, Lorg/apache/poi/ss/formula/FormulaParser;->createAreaRef(Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/util/AreaReference;

    move-result-object v0

    .line 630
    .local v0, "areaRef":Lorg/apache/poi/ss/util/AreaReference;
    if-nez p1, :cond_4

    .line 631
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    invoke-direct {v3, v0}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;-><init>(Lorg/apache/poi/ss/util/AreaReference;)V

    .line 632
    .restart local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_1

    .line 633
    .end local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_4
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    invoke-direct {v3, v0, v2}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;-><init>(Lorg/apache/poi/ss/util/AreaReference;I)V

    .restart local v3    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_1
.end method

.method private expected(Ljava/lang/String;)Ljava/lang/RuntimeException;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v2, 0x3d

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The specified formula \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 214
    const-string/jumbo v2, "\' starts with an equals sign which is not allowed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 213
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "msg":Ljava/lang/String;
    :goto_0
    new-instance v1, Lorg/apache/poi/ss/formula/FormulaParseException;

    invoke-direct {v1, v0}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 216
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Parse error near char "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 217
    const-string/jumbo v2, " in specified formula \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'. Expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 218
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 216
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "msg":Ljava/lang/String;
    goto :goto_0
.end method

.method private function(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ParseNode;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 908
    const/4 v2, 0x0

    .line 909
    .local v2, "nameToken":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-static {p1}, Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;->isBuiltInFunctionName(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 913
    iget-object v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    if-nez v3, :cond_0

    .line 915
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Need book to evaluate name \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 917
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    iget v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_sheetIndex:I

    invoke-interface {v3, p1, v4}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v1

    .line 918
    .local v1, "hName":Lorg/apache/poi/ss/formula/EvaluationName;
    if-nez v1, :cond_1

    .line 920
    iget-object v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    invoke-interface {v3, p1}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getNameXPtg(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    move-result-object v2

    .line 921
    if-nez v2, :cond_3

    .line 922
    new-instance v3, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Name \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 923
    const-string/jumbo v5, "\' is completely unknown in the current workbook"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 922
    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 926
    :cond_1
    invoke-interface {v1}, Lorg/apache/poi/ss/formula/EvaluationName;->isFunctionName()Z

    move-result v3

    if-nez v3, :cond_2

    .line 927
    new-instance v3, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Attempt to use name \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 928
    const-string/jumbo v5, "\' as a function, but defined name in workbook does not refer to a function"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 927
    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 933
    :cond_2
    invoke-interface {v1}, Lorg/apache/poi/ss/formula/EvaluationName;->createPtg()Lorg/apache/poi/ss/formula/ptg/NamePtg;

    move-result-object v2

    .line 937
    .end local v1    # "hName":Lorg/apache/poi/ss/formula/EvaluationName;
    :cond_3
    const/16 v3, 0x28

    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 938
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->Arguments()[Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 939
    .local v0, "args":[Lorg/apache/poi/ss/formula/ParseNode;
    const/16 v3, 0x29

    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 941
    invoke-direct {p0, p1, v2, v0}, Lorg/apache/poi/ss/formula/FormulaParser;->getFunction(Ljava/lang/String;Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ParseNode;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v3

    return-object v3
.end method

.method private getComparisonToken()Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 4

    .prologue
    const/16 v3, 0x3e

    const/16 v2, 0x3d

    .line 1490
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v1, v2, :cond_0

    .line 1491
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1492
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/EqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    .line 1511
    :goto_0
    return-object v1

    .line 1494
    :cond_0
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v1, v3, :cond_1

    const/4 v0, 0x1

    .line 1495
    .local v0, "isGreater":Z
    :goto_1
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1496
    if-eqz v0, :cond_3

    .line 1497
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v1, v2, :cond_2

    .line 1498
    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1499
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/GreaterEqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 1494
    .end local v0    # "isGreater":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1501
    .restart local v0    # "isGreater":Z
    :cond_2
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/GreaterThanPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 1503
    :cond_3
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    packed-switch v1, :pswitch_data_0

    .line 1511
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/LessThanPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 1505
    :pswitch_0
    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1506
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/LessEqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 1508
    :pswitch_1
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1509
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/NotEqualPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_0

    .line 1503
    nop

    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getFunction(Ljava/lang/String;Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ParseNode;)Lorg/apache/poi/ss/formula/ParseNode;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "namePtg"    # Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p3, "args"    # [Lorg/apache/poi/ss/formula/ParseNode;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 953
    .line 954
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 953
    invoke-static {v7}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getFunctionByName(Ljava/lang/String;)Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    move-result-object v1

    .line 955
    .local v1, "fm":Lorg/apache/poi/ss/formula/function/FunctionMetadata;
    array-length v4, p3

    .line 956
    .local v4, "numArgs":I
    if-nez v1, :cond_1

    .line 957
    if-nez p2, :cond_0

    .line 958
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "NamePtg must be supplied for external functions"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 961
    :cond_0
    add-int/lit8 v7, v4, 0x1

    new-array v0, v7, [Lorg/apache/poi/ss/formula/ParseNode;

    .line 962
    .local v0, "allArgs":[Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v7, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v7, p2}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    aput-object v7, v0, v3

    .line 963
    invoke-static {p3, v3, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 964
    new-instance v6, Lorg/apache/poi/ss/formula/ParseNode;

    add-int/lit8 v7, v4, 0x1

    invoke-static {p1, v7}, Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;->create(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ParseNode;)V

    .line 986
    .end local v0    # "allArgs":[Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    return-object v6

    .line 967
    :cond_1
    if-eqz p2, :cond_2

    .line 968
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "NamePtg no applicable to internal functions"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 970
    :cond_2
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->hasFixedArgsLength()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 971
    .local v3, "isVarArgs":Z
    :goto_1
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getIndex()I

    move-result v2

    .line 972
    .local v2, "funcIx":I
    const/4 v7, 0x4

    if-ne v2, v7, :cond_4

    array-length v7, p3

    if-ne v7, v6, :cond_4

    .line 975
    new-instance v6, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-static {}, Lorg/apache/poi/ss/formula/ptg/AttrPtg;->getSumSingle()Lorg/apache/poi/ss/formula/ptg/AttrPtg;

    move-result-object v7

    invoke-direct {v6, v7, p3}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ParseNode;)V

    goto :goto_0

    .end local v2    # "funcIx":I
    .end local v3    # "isVarArgs":Z
    :cond_3
    move v3, v6

    .line 970
    goto :goto_1

    .line 978
    .restart local v2    # "funcIx":I
    .restart local v3    # "isVarArgs":Z
    :cond_4
    array-length v6, p3

    invoke-direct {p0, v6, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->validateNumArgs(ILorg/apache/poi/ss/formula/function/FunctionMetadata;)V

    .line 981
    if-eqz v3, :cond_5

    .line 982
    invoke-static {p1, v4}, Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;->create(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/ptg/FuncVarPtg;

    move-result-object v5

    .line 986
    .local v5, "retval":Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;
    :goto_2
    new-instance v6, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {v6, v5, p3}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ParseNode;)V

    goto :goto_0

    .line 984
    .end local v5    # "retval":Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;
    :cond_5
    invoke-static {v2}, Lorg/apache/poi/ss/formula/ptg/FuncPtg;->create(I)Lorg/apache/poi/ss/formula/ptg/FuncPtg;

    move-result-object v5

    .restart local v5    # "retval":Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;
    goto :goto_2
.end method

.method private static getNumberPtgFromString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 6
    .param p0, "number1"    # Ljava/lang/String;
    .param p1, "number2"    # Ljava/lang/String;
    .param p2, "exponent"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x45

    .line 1374
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1376
    .local v2, "number":Ljava/lang/StringBuffer;
    if-nez p1, :cond_2

    .line 1377
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1379
    if-eqz p2, :cond_0

    .line 1380
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1381
    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1384
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1387
    .local v3, "numberStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1391
    .local v1, "intVal":I
    invoke-static {v1}, Lorg/apache/poi/ss/formula/ptg/IntPtg;->isInRange(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1392
    new-instance v4, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    invoke-direct {v4, v1}, Lorg/apache/poi/ss/formula/ptg/IntPtg;-><init>(I)V

    .line 1409
    .end local v1    # "intVal":I
    .end local v3    # "numberStr":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 1388
    .restart local v3    # "numberStr":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1389
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v4, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    invoke-direct {v4, v3}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1394
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "intVal":I
    :cond_1
    new-instance v4, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    invoke-direct {v4, v3}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1397
    .end local v1    # "intVal":I
    .end local v3    # "numberStr":Ljava/lang/String;
    :cond_2
    if-eqz p0, :cond_3

    .line 1398
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1401
    :cond_3
    const/16 v4, 0x2e

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1402
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1404
    if-eqz p2, :cond_4

    .line 1405
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1406
    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1409
    :cond_4
    new-instance v4, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getRPNPtg(I)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 2
    .param p1, "formulaType"    # I

    .prologue
    .line 1584
    new-instance v0, Lorg/apache/poi/ss/formula/OperandClassTransformer;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/formula/OperandClassTransformer;-><init>(I)V

    .line 1586
    .local v0, "oct":Lorg/apache/poi/ss/formula/OperandClassTransformer;
    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_rootNode:Lorg/apache/poi/ss/formula/ParseNode;

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/formula/OperandClassTransformer;->transformFormula(Lorg/apache/poi/ss/formula/ParseNode;)V

    .line 1587
    iget-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_rootNode:Lorg/apache/poi/ss/formula/ParseNode;

    invoke-static {v1}, Lorg/apache/poi/ss/formula/ParseNode;->toTokenArray(Lorg/apache/poi/ss/formula/ParseNode;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    return-object v1
.end method

.method private static isArgumentDelimiter(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 1026
    const/16 v0, 0x2c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isUnquotedSheetNameChar(C)Z
    .locals 2
    .param p0, "ch"    # C

    .prologue
    const/4 v0, 0x1

    .line 858
    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 866
    :goto_0
    :sswitch_0
    return v0

    .line 861
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 866
    const/4 v0, 0x0

    goto :goto_0

    .line 861
    nop

    :sswitch_data_0
    .sparse-switch
        0x2e -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch
.end method

.method private isValidCellReference(Ljava/lang/String;)Z
    .locals 7
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 874
    iget-object v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_ssVersion:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-static {p1, v5}, Lorg/apache/poi/ss/util/CellReference;->classifyCellReference(Ljava/lang/String;Lorg/apache/poi/ss/SpreadsheetVersion;)Lorg/apache/poi/ss/util/CellReference$NameType;

    move-result-object v5

    sget-object v6, Lorg/apache/poi/ss/util/CellReference$NameType;->CELL:Lorg/apache/poi/ss/util/CellReference$NameType;

    if-ne v5, v6, :cond_1

    move v1, v3

    .line 876
    .local v1, "result":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 885
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 884
    invoke-static {v5}, Lorg/apache/poi/ss/formula/function/FunctionMetadataRegistry;->getFunctionByName(Ljava/lang/String;)Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    move-result-object v5

    if-eqz v5, :cond_2

    move v0, v3

    .line 886
    .local v0, "isFunc":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 887
    iget v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 888
    .local v2, "savePointer":I
    iget v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {p0, v5}, Lorg/apache/poi/ss/formula/FormulaParser;->resetPointer(I)V

    .line 889
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 892
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x28

    if-eq v5, v6, :cond_3

    move v1, v3

    .line 893
    :goto_2
    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->resetPointer(I)V

    .line 896
    .end local v0    # "isFunc":Z
    .end local v2    # "savePointer":I
    :cond_0
    return v1

    .end local v1    # "result":Z
    :cond_1
    move v1, v4

    .line 874
    goto :goto_0

    .restart local v1    # "result":Z
    :cond_2
    move v0, v4

    .line 884
    goto :goto_1

    .restart local v0    # "isFunc":Z
    .restart local v2    # "savePointer":I
    :cond_3
    move v1, v4

    .line 892
    goto :goto_2
.end method

.method private static isValidDefinedNameChar(C)Z
    .locals 2
    .param p0, "ch"    # C

    .prologue
    const/4 v0, 0x1

    .line 586
    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596
    :goto_0
    :sswitch_0
    return v0

    .line 589
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 596
    const/4 v0, 0x0

    goto :goto_0

    .line 589
    nop

    :sswitch_data_0
    .sparse-switch
        0x2e -> :sswitch_0
        0x3f -> :sswitch_0
        0x5c -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch
.end method

.method private static isValidRangeOperand(Lorg/apache/poi/ss/formula/ParseNode;)Z
    .locals 6
    .param p0, "a"    # Lorg/apache/poi/ss/formula/ParseNode;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 355
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ParseNode;->getToken()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v2

    .line 357
    .local v2, "tkn":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v5, v2, Lorg/apache/poi/ss/formula/ptg/OperandPtg;

    if-eqz v5, :cond_1

    .line 387
    :cond_0
    :goto_0
    return v3

    .line 363
    :cond_1
    instance-of v5, v2, Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;

    if-eqz v5, :cond_2

    move-object v0, v2

    .line 364
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;

    .line 365
    .local v0, "afp":Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;->getDefaultOperandClass()B

    move-result v1

    .line 366
    .local v1, "returnClass":B
    if-eqz v1, :cond_0

    move v3, v4

    goto :goto_0

    .line 368
    .end local v0    # "afp":Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;
    .end local v1    # "returnClass":B
    :cond_2
    instance-of v5, v2, Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    if-eqz v5, :cond_3

    move v3, v4

    .line 369
    goto :goto_0

    .line 371
    :cond_3
    instance-of v5, v2, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    if-nez v5, :cond_0

    .line 376
    instance-of v5, v2, Lorg/apache/poi/ss/formula/ptg/ParenthesisPtg;

    if-eqz v5, :cond_4

    .line 378
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ParseNode;->getChildren()[Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-static {v3}, Lorg/apache/poi/ss/formula/FormulaParser;->isValidRangeOperand(Lorg/apache/poi/ss/formula/ParseNode;)Z

    move-result v3

    goto :goto_0

    .line 382
    :cond_4
    sget-object v5, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->REF_INVALID:Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    if-eq v2, v5, :cond_0

    move v3, v4

    .line 387
    goto :goto_0
.end method

.method private static needsMemFunc(Lorg/apache/poi/ss/formula/ParseNode;)Z
    .locals 8
    .param p0, "root"    # Lorg/apache/poi/ss/formula/ParseNode;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 309
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ParseNode;->getToken()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    .line 310
    .local v1, "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v4, v1, Lorg/apache/poi/ss/formula/ptg/AbstractFunctionPtg;

    if-eqz v4, :cond_1

    .line 336
    :cond_0
    :goto_0
    return v2

    .line 313
    :cond_1
    instance-of v4, v1, Lorg/apache/poi/ss/formula/ExternSheetReferenceToken;

    if-nez v4, :cond_0

    .line 316
    instance-of v4, v1, Lorg/apache/poi/ss/formula/ptg/NamePtg;

    if-nez v4, :cond_0

    instance-of v4, v1, Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    if-nez v4, :cond_0

    .line 320
    instance-of v4, v1, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    if-nez v4, :cond_2

    instance-of v4, v1, Lorg/apache/poi/ss/formula/ptg/ParenthesisPtg;

    if-eqz v4, :cond_4

    .line 322
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/ParseNode;->getChildren()[Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_1
    if-lt v4, v6, :cond_3

    move v2, v3

    .line 327
    goto :goto_0

    .line 322
    :cond_3
    aget-object v0, v5, v4

    .line 323
    .local v0, "child":Lorg/apache/poi/ss/formula/ParseNode;
    invoke-static {v0}, Lorg/apache/poi/ss/formula/FormulaParser;->needsMemFunc(Lorg/apache/poi/ss/formula/ParseNode;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 322
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 329
    .end local v0    # "child":Lorg/apache/poi/ss/formula/ParseNode;
    :cond_4
    instance-of v2, v1, Lorg/apache/poi/ss/formula/ptg/OperandPtg;

    if-eqz v2, :cond_5

    move v2, v3

    .line 330
    goto :goto_0

    :cond_5
    move v2, v3

    .line 336
    goto :goto_0
.end method

.method private parse()V
    .locals 4

    .prologue
    .line 1572
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 1573
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1574
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->unionExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_rootNode:Lorg/apache/poi/ss/formula/ParseNode;

    .line 1576
    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    iget v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaLength:I

    if-gt v1, v2, :cond_0

    .line 1577
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unused input ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1578
    const-string/jumbo v2, "] after attempting to parse the formula ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1577
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1579
    .local v0, "msg":Ljava/lang/String;
    new-instance v1, Lorg/apache/poi/ss/formula/FormulaParseException;

    invoke-direct {v1, v0}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1581
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static parse(Ljava/lang/String;Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;II)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 2
    .param p0, "formula"    # Ljava/lang/String;
    .param p1, "workbook"    # Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;
    .param p2, "formulaType"    # I
    .param p3, "sheetIndex"    # I

    .prologue
    .line 176
    new-instance v0, Lorg/apache/poi/ss/formula/FormulaParser;

    invoke-direct {v0, p0, p1, p3}, Lorg/apache/poi/ss/formula/FormulaParser;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;I)V

    .line 177
    .local v0, "fp":Lorg/apache/poi/ss/formula/FormulaParser;
    invoke-direct {v0}, Lorg/apache/poi/ss/formula/FormulaParser;->parse()V

    .line 178
    invoke-direct {v0, p2}, Lorg/apache/poi/ss/formula/FormulaParser;->getRPNPtg(I)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    return-object v1
.end method

.method private parseArray()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 8

    .prologue
    const/16 v7, 0x3b

    .line 1161
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1163
    .local v2, "rowsData":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/Object;>;"
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseArrayRow()[Ljava/lang/Object;

    move-result-object v3

    .line 1164
    .local v3, "singleRowData":[Ljava/lang/Object;
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x7d

    if-ne v5, v6, :cond_0

    .line 1173
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 1174
    .local v1, "nRows":I
    new-array v4, v1, [[Ljava/lang/Object;

    .line 1175
    .local v4, "values2d":[[Ljava/lang/Object;
    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1176
    const/4 v5, 0x0

    aget-object v5, v4, v5

    array-length v0, v5

    .line 1177
    .local v0, "nColumns":I
    invoke-direct {p0, v4, v0}, Lorg/apache/poi/ss/formula/FormulaParser;->checkRowLengths([[Ljava/lang/Object;I)V

    .line 1179
    new-instance v5, Lorg/apache/poi/ss/formula/ParseNode;

    new-instance v6, Lorg/apache/poi/ss/formula/ptg/ArrayPtg;

    invoke-direct {v6, v4}, Lorg/apache/poi/ss/formula/ptg/ArrayPtg;-><init>([[Ljava/lang/Object;)V

    invoke-direct {v5, v6}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    return-object v5

    .line 1168
    .end local v0    # "nColumns":I
    .end local v1    # "nRows":I
    .end local v4    # "values2d":[[Ljava/lang/Object;
    :cond_0
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v5, v7, :cond_1

    .line 1169
    const-string/jumbo v5, "\'}\' or \';\'"

    invoke-direct {p0, v5}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v5

    throw v5

    .line 1171
    :cond_1
    invoke-direct {p0, v7}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    goto :goto_0
.end method

.method private parseArrayItem()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1216
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1217
    iget-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    sparse-switch v0, :sswitch_data_0

    .line 1229
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNumber()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->convertArrayNumber(Lorg/apache/poi/ss/formula/ptg/Ptg;Z)Ljava/lang/Double;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1218
    :sswitch_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseStringLiteral()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1219
    :sswitch_1
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseErrorLiteral()I

    move-result v0

    invoke-static {v0}, Lorg/apache/poi/ss/formula/constant/ErrorConstant;->valueOf(I)Lorg/apache/poi/ss/formula/constant/ErrorConstant;

    move-result-object v0

    goto :goto_0

    .line 1222
    :sswitch_2
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseBooleanLiteral()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1224
    :sswitch_3
    const/16 v0, 0x2d

    invoke-direct {p0, v0}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1225
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1226
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNumber()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->convertArrayNumber(Lorg/apache/poi/ss/formula/ptg/Ptg;Z)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 1217
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x23 -> :sswitch_1
        0x2d -> :sswitch_3
        0x46 -> :sswitch_2
        0x54 -> :sswitch_2
        0x66 -> :sswitch_2
        0x74 -> :sswitch_2
    .end sparse-switch
.end method

.method private parseArrayRow()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1194
    .local v1, "temp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseArrayItem()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1195
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1196
    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    sparse-switch v2, :sswitch_data_0

    .line 1204
    const-string/jumbo v2, "\'}\' or \',\'"

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2

    .line 1201
    :sswitch_0
    const/16 v2, 0x2c

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    goto :goto_0

    .line 1210
    :sswitch_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/Object;

    .line 1211
    .local v0, "result":[Ljava/lang/Object;
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1212
    return-object v0

    .line 1196
    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private parseBooleanLiteral()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 1233
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseUnquotedIdentifier()Ljava/lang/String;

    move-result-object v0

    .line 1234
    .local v0, "iden":Ljava/lang/String;
    const-string/jumbo v1, "TRUE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1235
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 1238
    :goto_0
    return-object v1

    .line 1237
    :cond_0
    const-string/jumbo v1, "FALSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1238
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1240
    :cond_1
    const-string/jumbo v1, "\'TRUE\' or \'FALSE\'"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method private parseErrorLiteral()I
    .locals 5

    .prologue
    const/16 v4, 0x2f

    const/4 v1, 0x0

    const/16 v3, 0x21

    .line 1295
    const/16 v2, 0x23

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1297
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseUnquotedIdentifier()Ljava/lang/String;

    move-result-object v0

    .line 1298
    .local v0, "part1":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1299
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 1300
    :cond_0
    if-nez v0, :cond_1

    .line 1301
    const-string/jumbo v1, "remainder of error constant literal"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1304
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1350
    const-string/jumbo v1, "#VALUE!, #REF!, #DIV/0!, #NAME?, #NUM!, #NULL! or #N/A"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1306
    :sswitch_0
    const-string/jumbo v1, "VALUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1307
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1308
    const/16 v1, 0xf

    .line 1345
    :goto_0
    return v1

    .line 1310
    :cond_2
    const-string/jumbo v1, "#VALUE!"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1312
    :sswitch_1
    const-string/jumbo v1, "REF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1313
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1314
    const/16 v1, 0x17

    goto :goto_0

    .line 1316
    :cond_3
    const-string/jumbo v1, "#REF!"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1318
    :sswitch_2
    const-string/jumbo v1, "DIV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1319
    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1320
    const/16 v1, 0x30

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1321
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1322
    const/4 v1, 0x7

    goto :goto_0

    .line 1324
    :cond_4
    const-string/jumbo v1, "#DIV/0!"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1326
    :sswitch_3
    const-string/jumbo v2, "NAME"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1327
    const/16 v1, 0x3f

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1328
    const/16 v1, 0x1d

    goto :goto_0

    .line 1330
    :cond_5
    const-string/jumbo v2, "NUM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1331
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1332
    const/16 v1, 0x24

    goto :goto_0

    .line 1334
    :cond_6
    const-string/jumbo v2, "NULL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1335
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    goto :goto_0

    .line 1338
    :cond_7
    const-string/jumbo v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1339
    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1340
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v2, 0x41

    if-eq v1, v2, :cond_8

    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v2, 0x61

    if-eq v1, v2, :cond_8

    .line 1341
    const-string/jumbo v1, "#N/A"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1343
    :cond_8
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1345
    const/16 v1, 0x2a

    goto/16 :goto_0

    .line 1347
    :cond_9
    const-string/jumbo v1, "#NAME?, #NUM!, #NULL! or #N/A"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1304
    nop

    :sswitch_data_0
    .sparse-switch
        0x44 -> :sswitch_2
        0x4e -> :sswitch_3
        0x52 -> :sswitch_1
        0x56 -> :sswitch_0
    .end sparse-switch
.end method

.method private parseNonRange(I)Lorg/apache/poi/ss/formula/ParseNode;
    .locals 6
    .param p1, "savePointer"    # I

    .prologue
    .line 536
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/formula/FormulaParser;->resetPointer(I)V

    .line 538
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 539
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNumber()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 574
    :goto_0
    return-object v3

    .line 541
    :cond_0
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v4, 0x22

    if-ne v3, v4, :cond_1

    .line 542
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    new-instance v4, Lorg/apache/poi/ss/formula/ptg/StringPtg;

    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseStringLiteral()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/ss/formula/ptg/StringPtg;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_0

    .line 546
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-nez v3, :cond_3

    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v4, 0x5f

    if-eq v3, v4, :cond_3

    .line 550
    const-string/jumbo v3, "number, string, or defined name"

    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v3

    throw v3

    .line 553
    :cond_2
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 554
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 552
    :cond_3
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v3}, Lorg/apache/poi/ss/formula/FormulaParser;->isValidDefinedNameChar(C)Z

    move-result v3

    if-nez v3, :cond_2

    .line 556
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 557
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 558
    .local v1, "name":Ljava/lang/String;
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v4, 0x28

    if-ne v3, v4, :cond_4

    .line 559
    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->function(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v3

    goto :goto_0

    .line 561
    :cond_4
    const-string/jumbo v3, "TRUE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "FALSE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 562
    :cond_5
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    const-string/jumbo v4, "TRUE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Lorg/apache/poi/ss/formula/ptg/BoolPtg;->valueOf(Z)Lorg/apache/poi/ss/formula/ptg/BoolPtg;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_0

    .line 564
    :cond_6
    iget-object v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    if-nez v3, :cond_7

    .line 566
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Need book to evaluate name \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 568
    :cond_7
    iget-object v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    iget v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_sheetIndex:I

    invoke-interface {v3, v1, v4}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getName(Ljava/lang/String;I)Lorg/apache/poi/ss/formula/EvaluationName;

    move-result-object v0

    .line 569
    .local v0, "evalName":Lorg/apache/poi/ss/formula/EvaluationName;
    if-nez v0, :cond_8

    .line 570
    new-instance v3, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Specified named range \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 571
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\' does not exist in the current workbook."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 570
    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 573
    :cond_8
    invoke-interface {v0}, Lorg/apache/poi/ss/formula/EvaluationName;->isRange()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 574
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-interface {v0}, Lorg/apache/poi/ss/formula/EvaluationName;->createPtg()Lorg/apache/poi/ss/formula/ptg/NamePtg;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto/16 :goto_0

    .line 577
    :cond_9
    new-instance v3, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Specified name \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 578
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\' is not a range as expected."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 577
    invoke-direct {v3, v4}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private parseNumber()Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 7

    .prologue
    .line 1259
    const/4 v3, 0x0

    .line 1260
    .local v3, "number2":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1261
    .local v0, "exponent":Ljava/lang/String;
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetNum()Ljava/lang/String;

    move-result-object v2

    .line 1263
    .local v2, "number1":Ljava/lang/String;
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x2e

    if-ne v5, v6, :cond_0

    .line 1264
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1265
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetNum()Ljava/lang/String;

    move-result-object v3

    .line 1268
    :cond_0
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x45

    if-ne v5, v6, :cond_4

    .line 1269
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1271
    const-string/jumbo v4, ""

    .line 1272
    .local v4, "sign":Ljava/lang/String;
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x2b

    if-ne v5, v6, :cond_2

    .line 1273
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1279
    :cond_1
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetNum()Ljava/lang/String;

    move-result-object v1

    .line 1280
    .local v1, "number":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 1281
    const-string/jumbo v5, "Integer"

    invoke-direct {p0, v5}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v5

    throw v5

    .line 1274
    .end local v1    # "number":Ljava/lang/String;
    :cond_2
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x2d

    if-ne v5, v6, :cond_1

    .line 1275
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1276
    const-string/jumbo v4, "-"

    goto :goto_0

    .line 1283
    .restart local v1    # "number":Ljava/lang/String;
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1286
    .end local v1    # "number":Ljava/lang/String;
    .end local v4    # "sign":Ljava/lang/String;
    :cond_4
    if-nez v2, :cond_5

    if-nez v3, :cond_5

    .line 1287
    const-string/jumbo v5, "Integer"

    invoke-direct {p0, v5}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v5

    throw v5

    .line 1290
    :cond_5
    invoke-static {v2, v3, v0}, Lorg/apache/poi/ss/formula/FormulaParser;->getNumberPtgFromString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v5

    return-object v5
.end method

.method private parseRangeExpression()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 7

    .prologue
    .line 269
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseRangeable()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v4

    .line 270
    .local v4, "result":Lorg/apache/poi/ss/formula/ParseNode;
    const/4 v1, 0x0

    .line 271
    .local v1, "hasRange":Z
    :goto_0
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x3a

    if-eq v5, v6, :cond_1

    .line 287
    if-eqz v1, :cond_0

    .line 288
    invoke-static {v4}, Lorg/apache/poi/ss/formula/FormulaParser;->augmentWithMemPtg(Lorg/apache/poi/ss/formula/ParseNode;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v4

    .line 290
    .end local v4    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    :cond_0
    return-object v4

    .line 272
    .restart local v4    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    :cond_1
    iget v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 273
    .local v3, "pos":I
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 274
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseRangeable()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v2

    .line 280
    .local v2, "nextPart":Lorg/apache/poi/ss/formula/ParseNode;
    const-string/jumbo v5, "LHS"

    invoke-static {v5, v3, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->checkValidRangeOperand(Ljava/lang/String;ILorg/apache/poi/ss/formula/ParseNode;)V

    .line 281
    const-string/jumbo v5, "RHS"

    invoke-static {v5, v3, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->checkValidRangeOperand(Ljava/lang/String;ILorg/apache/poi/ss/formula/ParseNode;)V

    .line 283
    const/4 v5, 0x2

    new-array v0, v5, [Lorg/apache/poi/ss/formula/ParseNode;

    const/4 v5, 0x0

    aput-object v4, v0, v5

    const/4 v5, 0x1

    aput-object v2, v0, v5

    .line 284
    .local v0, "children":[Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v4, Lorg/apache/poi/ss/formula/ParseNode;

    .end local v4    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    sget-object v5, Lorg/apache/poi/ss/formula/ptg/RangePtg;->instance:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    invoke-direct {v4, v5, v0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ParseNode;)V

    .line 285
    .restart local v4    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private parseRangeable()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 13

    .prologue
    const/16 v12, 0x2e

    .line 410
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 411
    iget v6, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 412
    .local v6, "savePointer":I
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseSheetName()Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;

    move-result-object v7

    .line 413
    .local v7, "sheetIden":Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;
    if-nez v7, :cond_0

    .line 414
    invoke-direct {p0, v6}, Lorg/apache/poi/ss/formula/FormulaParser;->resetPointer(I)V

    .line 420
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseSimpleRangePart()Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;

    move-result-object v2

    .line 421
    .local v2, "part1":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    if-nez v2, :cond_3

    .line 422
    if-eqz v7, :cond_2

    .line 423
    iget-char v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v11, 0x23

    if-ne v10, v11, :cond_1

    .line 424
    new-instance v10, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseErrorLiteral()I

    move-result v11

    invoke-static {v11}, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->valueOf(I)Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 518
    :goto_1
    return-object v10

    .line 416
    .end local v2    # "part1":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    :cond_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 417
    iget v6, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    goto :goto_0

    .line 426
    .restart local v2    # "part1":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    :cond_1
    new-instance v10, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Cell reference expected after sheet name at index "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 427
    iget v12, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 426
    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 430
    :cond_2
    invoke-direct {p0, v6}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNonRange(I)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto :goto_1

    .line 432
    :cond_3
    iget-char v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v10}, Lorg/apache/poi/ss/formula/FormulaParser;->IsWhite(C)Z

    move-result v8

    .line 433
    .local v8, "whiteAfterPart1":Z
    if-eqz v8, :cond_4

    .line 434
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 437
    :cond_4
    iget-char v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v11, 0x3a

    if-ne v10, v11, :cond_9

    .line 438
    iget v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 439
    .local v0, "colonPos":I
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 440
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 441
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseSimpleRangePart()Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;

    move-result-object v4

    .line 442
    .local v4, "part2":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    if-eqz v4, :cond_5

    invoke-virtual {v2, v4}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isCompatibleForArea(Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 446
    const/4 v4, 0x0

    .line 448
    :cond_5
    if-nez v4, :cond_8

    .line 451
    invoke-direct {p0, v0}, Lorg/apache/poi/ss/formula/FormulaParser;->resetPointer(I)V

    .line 452
    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isCell()Z

    move-result v10

    if-nez v10, :cond_7

    .line 454
    if-nez v7, :cond_6

    .line 455
    const-string/jumbo v5, ""

    .line 459
    .local v5, "prefix":Ljava/lang/String;
    :goto_2
    new-instance v10, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\' is not a proper reference."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 457
    .end local v5    # "prefix":Ljava/lang/String;
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "\'"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;->getSheetIdentifier()Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x21

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "prefix":Ljava/lang/String;
    goto :goto_2

    .line 461
    .end local v5    # "prefix":Ljava/lang/String;
    :cond_7
    invoke-direct {p0, v7, v2, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->createAreaRefParseNode(Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 463
    :cond_8
    invoke-direct {p0, v7, v2, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->createAreaRefParseNode(Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 466
    .end local v0    # "colonPos":I
    .end local v4    # "part2":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    :cond_9
    iget-char v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v10, v12, :cond_14

    .line 467
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 468
    const/4 v1, 0x1

    .line 469
    .local v1, "dotCount":I
    :goto_3
    iget-char v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v10, v12, :cond_a

    .line 473
    iget-char v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v10}, Lorg/apache/poi/ss/formula/FormulaParser;->IsWhite(C)Z

    move-result v9

    .line 475
    .local v9, "whiteBeforePart2":Z
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 476
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseSimpleRangePart()Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;

    move-result-object v4

    .line 477
    .restart local v4    # "part2":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    iget-object v10, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    add-int/lit8 v11, v6, -0x1

    iget v12, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 478
    .local v3, "part1And2":Ljava/lang/String;
    if-nez v4, :cond_c

    .line 479
    if-eqz v7, :cond_b

    .line 480
    new-instance v10, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Complete area reference expected after sheet name at index "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 481
    iget v12, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 480
    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 470
    .end local v3    # "part1And2":Ljava/lang/String;
    .end local v4    # "part2":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .end local v9    # "whiteBeforePart2":Z
    :cond_a
    add-int/lit8 v1, v1, 0x1

    .line 471
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_3

    .line 483
    .restart local v3    # "part1And2":Ljava/lang/String;
    .restart local v4    # "part2":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .restart local v9    # "whiteBeforePart2":Z
    :cond_b
    invoke-direct {p0, v6}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNonRange(I)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 487
    :cond_c
    if-nez v8, :cond_d

    if-eqz v9, :cond_10

    .line 488
    :cond_d
    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRowOrColumn()Z

    move-result v10

    if-nez v10, :cond_e

    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRowOrColumn()Z

    move-result v10

    if-eqz v10, :cond_f

    .line 491
    :cond_e
    new-instance v10, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Dotted range (full row or column) expression \'"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 492
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\' must not contain whitespace."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 491
    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 494
    :cond_f
    invoke-direct {p0, v7, v2, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->createAreaRefParseNode(Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 497
    :cond_10
    const/4 v10, 0x1

    if-ne v1, v10, :cond_11

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRow()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRow()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 499
    invoke-direct {p0, v6}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNonRange(I)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 502
    :cond_11
    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRowOrColumn()Z

    move-result v10

    if-nez v10, :cond_12

    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isRowOrColumn()Z

    move-result v10

    if-eqz v10, :cond_13

    .line 503
    :cond_12
    const/4 v10, 0x2

    if-eq v1, v10, :cond_13

    .line 504
    new-instance v10, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Dotted range (full row or column) expression \'"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 505
    const-string/jumbo v12, "\' must have exactly 2 dots."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 504
    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 508
    :cond_13
    invoke-direct {p0, v7, v2, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->createAreaRefParseNode(Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 510
    .end local v1    # "dotCount":I
    .end local v3    # "part1And2":Ljava/lang/String;
    .end local v4    # "part2":Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .end local v9    # "whiteBeforePart2":Z
    :cond_14
    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->isCell()Z

    move-result v10

    if-eqz v10, :cond_15

    invoke-virtual {v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;->getRep()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lorg/apache/poi/ss/formula/FormulaParser;->isValidCellReference(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 511
    const/4 v10, 0x0

    invoke-direct {p0, v7, v2, v10}, Lorg/apache/poi/ss/formula/FormulaParser;->createAreaRefParseNode(Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1

    .line 513
    :cond_15
    if-eqz v7, :cond_16

    .line 514
    new-instance v10, Lorg/apache/poi/ss/formula/FormulaParseException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Second part of cell reference expected after sheet name at index "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 515
    iget v12, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 514
    invoke-direct {v10, v11}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 518
    :cond_16
    invoke-direct {p0, v6}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNonRange(I)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v10

    goto/16 :goto_1
.end method

.method private parseSheetName()Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v9, 0x27

    .line 798
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v8, 0x5b

    if-ne v7, v8, :cond_2

    .line 799
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 800
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 801
    :goto_0
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v8, 0x5d

    if-ne v7, v8, :cond_1

    .line 805
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 806
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 811
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    .local v0, "bookName":Ljava/lang/String;
    :goto_1
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v7, v9, :cond_7

    .line 812
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 814
    .local v3, "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0, v9}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 815
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v7, v9, :cond_3

    move v1, v4

    .line 816
    .local v1, "done":Z
    :cond_0
    :goto_2
    if-eqz v1, :cond_4

    .line 826
    new-instance v2, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5, v4}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;-><init>(Ljava/lang/String;Z)V

    .line 828
    .local v2, "iden":Lorg/apache/poi/ss/formula/FormulaParser$Identifier;
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 829
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v4, v10, :cond_6

    .line 830
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 831
    new-instance v4, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;

    invoke-direct {v4, v0, v2}, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/formula/FormulaParser$Identifier;)V

    .line 851
    .end local v1    # "done":Z
    .end local v2    # "iden":Lorg/apache/poi/ss/formula/FormulaParser$Identifier;
    .end local v3    # "sb":Ljava/lang/StringBuffer;
    :goto_3
    return-object v4

    .line 802
    .end local v0    # "bookName":Ljava/lang/String;
    .local v3, "sb":Ljava/lang/StringBuilder;
    :cond_1
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 803
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_0

    .line 808
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "bookName":Ljava/lang/String;
    goto :goto_1

    .local v3, "sb":Ljava/lang/StringBuffer;
    :cond_3
    move v1, v5

    .line 815
    goto :goto_2

    .line 817
    .restart local v1    # "done":Z
    :cond_4
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 818
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 819
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v7, v9, :cond_0

    .line 821
    invoke-direct {p0, v9}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 822
    iget-char v7, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v7, v9, :cond_5

    move v1, v4

    :goto_4
    goto :goto_2

    :cond_5
    move v1, v5

    goto :goto_4

    .restart local v2    # "iden":Lorg/apache/poi/ss/formula/FormulaParser$Identifier;
    :cond_6
    move-object v4, v6

    .line 833
    goto :goto_3

    .line 837
    .end local v1    # "done":Z
    .end local v2    # "iden":Lorg/apache/poi/ss/formula/FormulaParser$Identifier;
    .end local v3    # "sb":Ljava/lang/StringBuffer;
    :cond_7
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v7, 0x5f

    if-eq v4, v7, :cond_8

    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 838
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 840
    .local v3, "sb":Ljava/lang/StringBuilder;
    :goto_5
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v4}, Lorg/apache/poi/ss/formula/FormulaParser;->isUnquotedSheetNameChar(C)Z

    move-result v4

    if-nez v4, :cond_9

    .line 844
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 845
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v4, v10, :cond_a

    .line 846
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 847
    new-instance v4, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;

    new-instance v6, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Lorg/apache/poi/ss/formula/FormulaParser$Identifier;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v4, v0, v6}, Lorg/apache/poi/ss/formula/FormulaParser$SheetIdentifier;-><init>(Ljava/lang/String;Lorg/apache/poi/ss/formula/FormulaParser$Identifier;)V

    goto :goto_3

    .line 841
    :cond_9
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 842
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_5

    :cond_a
    move-object v4, v6

    .line 849
    goto :goto_3

    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_b
    move-object v4, v6

    .line 851
    goto :goto_3
.end method

.method private parseSimpleFactor()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 4

    .prologue
    .line 1098
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1099
    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    sparse-switch v2, :sswitch_data_0

    .line 1121
    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v2}, Lorg/apache/poi/ss/formula/FormulaParser;->IsAlpha(C)Z

    move-result v2

    if-nez v2, :cond_0

    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_0

    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v3, 0x27

    if-eq v2, v3, :cond_0

    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v3, 0x5b

    if-ne v2, v3, :cond_1

    .line 1122
    :cond_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseRangeExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 1125
    :goto_0
    return-object v0

    .line 1101
    :sswitch_0
    new-instance v0, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseErrorLiteral()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->valueOf(I)Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_0

    .line 1103
    :sswitch_1
    const/16 v2, 0x2d

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1104
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->parseUnary(Z)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    goto :goto_0

    .line 1106
    :sswitch_2
    const/16 v2, 0x2b

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1107
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->parseUnary(Z)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    goto :goto_0

    .line 1109
    :sswitch_3
    const/16 v2, 0x28

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1110
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->comparisonExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1111
    .local v1, "inside":Lorg/apache/poi/ss/formula/ParseNode;
    const/16 v2, 0x29

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1112
    new-instance v0, Lorg/apache/poi/ss/formula/ParseNode;

    sget-object v2, Lorg/apache/poi/ss/formula/ptg/ParenthesisPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ControlPtg;

    invoke-direct {v0, v2, v1}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;)V

    goto :goto_0

    .line 1114
    .end local v1    # "inside":Lorg/apache/poi/ss/formula/ParseNode;
    :sswitch_4
    new-instance v0, Lorg/apache/poi/ss/formula/ParseNode;

    new-instance v2, Lorg/apache/poi/ss/formula/ptg/StringPtg;

    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseStringLiteral()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/poi/ss/formula/ptg/StringPtg;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_0

    .line 1116
    :sswitch_5
    const/16 v2, 0x7b

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1117
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseArray()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 1118
    .local v0, "arrayNode":Lorg/apache/poi/ss/formula/ParseNode;
    const/16 v2, 0x7d

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    goto :goto_0

    .line 1124
    .end local v0    # "arrayNode":Lorg/apache/poi/ss/formula/ParseNode;
    :cond_1
    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_2

    .line 1125
    new-instance v0, Lorg/apache/poi/ss/formula/ParseNode;

    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseNumber()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_0

    .line 1127
    :cond_2
    const-string/jumbo v2, "cell ref or constant literal"

    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2

    .line 1099
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_4
        0x23 -> :sswitch_0
        0x28 -> :sswitch_3
        0x2b -> :sswitch_2
        0x2d -> :sswitch_1
        0x7b -> :sswitch_5
    .end sparse-switch
.end method

.method private parseSimpleRangePart()Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 666
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v5, v8, -0x1

    .line 667
    .local v5, "ptr":I
    const/4 v2, 0x0

    .line 668
    .local v2, "hasDigits":Z
    const/4 v3, 0x0

    .line 669
    .local v3, "hasLetters":Z
    :goto_0
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaLength:I

    if-lt v5, v8, :cond_1

    .line 682
    :goto_1
    iget v8, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v8, v8, -0x1

    if-gt v5, v8, :cond_5

    .line 715
    :cond_0
    :goto_2
    return-object v7

    .line 670
    :cond_1
    iget-object v8, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 671
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 672
    const/4 v2, 0x1

    .line 680
    :cond_2
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 673
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 674
    const/4 v3, 0x1

    .line 675
    goto :goto_3

    :cond_4
    const/16 v8, 0x24

    if-eq v0, v8, :cond_2

    const/16 v8, 0x5f

    if-eq v0, v8, :cond_2

    goto :goto_1

    .line 685
    .end local v0    # "ch":C
    :cond_5
    iget-object v8, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    iget v9, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 686
    .local v6, "rep":Ljava/lang/String;
    sget-object v8, Lorg/apache/poi/ss/formula/FormulaParser;->CELL_REF_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 690
    if-eqz v3, :cond_7

    if-eqz v2, :cond_7

    .line 691
    invoke-direct {p0, v6}, Lorg/apache/poi/ss/formula/FormulaParser;->isValidCellReference(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 714
    :cond_6
    add-int/lit8 v7, v5, 0x1

    invoke-direct {p0, v7}, Lorg/apache/poi/ss/formula/FormulaParser;->resetPointer(I)V

    .line 715
    new-instance v7, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;

    invoke-direct {v7, v6, v3, v2}, Lorg/apache/poi/ss/formula/FormulaParser$SimpleRangePart;-><init>(Ljava/lang/String;ZZ)V

    goto :goto_2

    .line 694
    :cond_7
    if-eqz v3, :cond_8

    .line 695
    const-string/jumbo v8, "$"

    const-string/jumbo v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_ssVersion:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-static {v8, v9}, Lorg/apache/poi/ss/util/CellReference;->isColumnWithnRange(Ljava/lang/String;Lorg/apache/poi/ss/SpreadsheetVersion;)Z

    move-result v8

    if-nez v8, :cond_6

    goto :goto_2

    .line 698
    :cond_8
    if-eqz v2, :cond_0

    .line 701
    :try_start_0
    const-string/jumbo v8, "$"

    const-string/jumbo v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 705
    .local v4, "i":I
    const/4 v8, 0x1

    if-lt v4, v8, :cond_0

    const/high16 v8, 0x10000

    if-le v4, v8, :cond_6

    goto :goto_2

    .line 702
    .end local v4    # "i":I
    :catch_0
    move-exception v1

    .line 703
    .local v1, "e":Ljava/lang/NumberFormatException;
    goto :goto_2
.end method

.method private parseStringLiteral()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x22

    .line 1414
    invoke-direct {p0, v2}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1416
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1418
    .local v0, "token":Ljava/lang/StringBuffer;
    :goto_0
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-ne v1, v2, :cond_0

    .line 1419
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1420
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v1, v2, :cond_0

    .line 1427
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1424
    :cond_0
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1425
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_0
.end method

.method private parseUnary(Z)Lorg/apache/poi/ss/formula/ParseNode;
    .locals 8
    .param p1, "isPlus"    # Z

    .prologue
    .line 1133
    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v5}, Lorg/apache/poi/ss/formula/FormulaParser;->IsDigit(C)Z

    move-result v5

    if-nez v5, :cond_1

    iget-char v5, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v6, 0x2e

    if-eq v5, v6, :cond_1

    const/4 v2, 0x0

    .line 1134
    .local v2, "numberFollows":Z
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->powerFactor()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 1136
    .local v0, "factor":Lorg/apache/poi/ss/formula/ParseNode;
    if-eqz v2, :cond_4

    .line 1139
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ParseNode;->getToken()Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v3

    .line 1140
    .local v3, "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v5, v3, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    if-eqz v5, :cond_3

    .line 1141
    if-eqz p1, :cond_2

    .line 1157
    .end local v0    # "factor":Lorg/apache/poi/ss/formula/ParseNode;
    .end local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_0
    :goto_1
    return-object v0

    .line 1133
    .end local v2    # "numberFollows":Z
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 1144
    .restart local v0    # "factor":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v2    # "numberFollows":Z
    .restart local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_2
    new-instance v4, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    check-cast v3, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    .end local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;->getValue()D

    move-result-wide v6

    neg-double v6, v6

    invoke-direct {v4, v6, v7}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(D)V

    .line 1145
    .local v4, "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    new-instance v0, Lorg/apache/poi/ss/formula/ParseNode;

    .end local v0    # "factor":Lorg/apache/poi/ss/formula/ParseNode;
    invoke-direct {v0, v4}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_1

    .line 1147
    .end local v4    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .restart local v0    # "factor":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_3
    instance-of v5, v3, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    if-eqz v5, :cond_4

    .line 1148
    if-nez p1, :cond_0

    .line 1151
    check-cast v3, Lorg/apache/poi/ss/formula/ptg/IntPtg;

    .end local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {v3}, Lorg/apache/poi/ss/formula/ptg/IntPtg;->getValue()I

    move-result v1

    .line 1153
    .local v1, "intVal":I
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    neg-int v5, v1

    int-to-double v6, v5

    invoke-direct {v3, v6, v7}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(D)V

    .line 1154
    .restart local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    new-instance v0, Lorg/apache/poi/ss/formula/ParseNode;

    .end local v0    # "factor":Lorg/apache/poi/ss/formula/ParseNode;
    invoke-direct {v0, v3}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    goto :goto_1

    .line 1157
    .end local v1    # "intVal":I
    .end local v3    # "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    .restart local v0    # "factor":Lorg/apache/poi/ss/formula/ParseNode;
    :cond_4
    new-instance v6, Lorg/apache/poi/ss/formula/ParseNode;

    if-eqz p1, :cond_5

    sget-object v5, Lorg/apache/poi/ss/formula/ptg/UnaryPlusPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    :goto_2
    invoke-direct {v6, v5, v0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;)V

    move-object v0, v6

    goto :goto_1

    :cond_5
    sget-object v5, Lorg/apache/poi/ss/formula/ptg/UnaryMinusPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    goto :goto_2
.end method

.method private parseUnquotedIdentifier()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1354
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    .line 1355
    const-string/jumbo v1, "unquoted identifier"

    invoke-direct {p0, v1}, Lorg/apache/poi/ss/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1357
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1358
    .local v0, "sb":Ljava/lang/StringBuilder;
    :goto_0
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-nez v1, :cond_1

    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    const/16 v2, 0x2e

    if-eq v1, v2, :cond_1

    .line 1362
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_2

    .line 1363
    const/4 v1, 0x0

    .line 1366
    :goto_1
    return-object v1

    .line 1359
    :cond_1
    iget-char v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1360
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    goto :goto_0

    .line 1366
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private percentFactor()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 4

    .prologue
    const/16 v3, 0x25

    .line 1082
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->parseSimpleFactor()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 1084
    .local v0, "result":Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1085
    iget-char v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v2, v3, :cond_0

    .line 1086
    return-object v0

    .line 1088
    :cond_0
    invoke-direct {p0, v3}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1089
    new-instance v1, Lorg/apache/poi/ss/formula/ParseNode;

    sget-object v2, Lorg/apache/poi/ss/formula/ptg/PercentPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    invoke-direct {v1, v2, v0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v0    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v1, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v0, v1

    .line 1083
    .end local v1    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v0    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0
.end method

.method private powerFactor()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 5

    .prologue
    const/16 v4, 0x5e

    .line 1069
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->percentFactor()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1071
    .local v1, "result":Lorg/apache/poi/ss/formula/ParseNode;
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1072
    iget-char v3, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    if-eq v3, v4, :cond_0

    .line 1073
    return-object v1

    .line 1075
    :cond_0
    invoke-direct {p0, v4}, Lorg/apache/poi/ss/formula/FormulaParser;->Match(C)V

    .line 1076
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->percentFactor()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v0

    .line 1077
    .local v0, "other":Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v2, Lorg/apache/poi/ss/formula/ParseNode;

    sget-object v3, Lorg/apache/poi/ss/formula/ptg/PowerPtg;->instance:Lorg/apache/poi/ss/formula/ptg/ValueOperatorPtg;

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v1    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v2, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v1, v2

    .line 1070
    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v1    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0
.end method

.method private resetPointer(I)V
    .locals 2
    .param p1, "ptr"    # I

    .prologue
    .line 198
    iput p1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    .line 199
    iget v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaLength:I

    if-gt v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    iget v1, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_pointer:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    .line 206
    :goto_0
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    iput-char v0, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    goto :goto_0
.end method

.method private unionExpression()Lorg/apache/poi/ss/formula/ParseNode;
    .locals 5

    .prologue
    .line 1453
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->comparisonExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v2

    .line 1454
    .local v2, "result":Lorg/apache/poi/ss/formula/ParseNode;
    const/4 v0, 0x0

    .line 1456
    .local v0, "hasUnions":Z
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->SkipWhite()V

    .line 1457
    iget-char v4, p0, Lorg/apache/poi/ss/formula/FormulaParser;->look:C

    packed-switch v4, :pswitch_data_0

    .line 1465
    if-eqz v0, :cond_0

    .line 1466
    invoke-static {v2}, Lorg/apache/poi/ss/formula/FormulaParser;->augmentWithMemPtg(Lorg/apache/poi/ss/formula/ParseNode;)Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v2

    .line 1468
    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    :cond_0
    return-object v2

    .line 1459
    .restart local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->GetChar()V

    .line 1460
    const/4 v0, 0x1

    .line 1461
    invoke-direct {p0}, Lorg/apache/poi/ss/formula/FormulaParser;->comparisonExpression()Lorg/apache/poi/ss/formula/ParseNode;

    move-result-object v1

    .line 1462
    .local v1, "other":Lorg/apache/poi/ss/formula/ParseNode;
    new-instance v3, Lorg/apache/poi/ss/formula/ParseNode;

    sget-object v4, Lorg/apache/poi/ss/formula/ptg/UnionPtg;->instance:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    invoke-direct {v3, v4, v2, v1}, Lorg/apache/poi/ss/formula/ParseNode;-><init>(Lorg/apache/poi/ss/formula/ptg/Ptg;Lorg/apache/poi/ss/formula/ParseNode;Lorg/apache/poi/ss/formula/ParseNode;)V

    .end local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .local v3, "result":Lorg/apache/poi/ss/formula/ParseNode;
    move-object v2, v3

    .line 1463
    .end local v3    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    .restart local v2    # "result":Lorg/apache/poi/ss/formula/ParseNode;
    goto :goto_0

    .line 1457
    nop

    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_0
    .end packed-switch
.end method

.method private validateNumArgs(ILorg/apache/poi/ss/formula/function/FunctionMetadata;)V
    .locals 4
    .param p1, "numArgs"    # I
    .param p2, "fm"    # Lorg/apache/poi/ss/formula/function/FunctionMetadata;

    .prologue
    .line 990
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getMinParams()I

    move-result v2

    if-ge p1, v2, :cond_1

    .line 991
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Too few arguments to function \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 992
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->hasFixedArgsLength()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 993
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "Expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getMinParams()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 997
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, " but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 998
    new-instance v2, Lorg/apache/poi/ss/formula/FormulaParseException;

    invoke-direct {v2, v1}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 995
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "At least "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getMinParams()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " were expected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1002
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->hasUnlimitedVarags()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1003
    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    if-eqz v2, :cond_2

    .line 1004
    iget-object v2, p0, Lorg/apache/poi/ss/formula/FormulaParser;->_book:Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;

    invoke-interface {v2}, Lorg/apache/poi/ss/formula/FormulaParsingWorkbook;->getSpreadsheetVersion()Lorg/apache/poi/ss/SpreadsheetVersion;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/ss/SpreadsheetVersion;->getMaxFunctionArgs()I

    move-result v0

    .line 1013
    .local v0, "maxArgs":I
    :goto_1
    if-le p1, v0, :cond_5

    .line 1014
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Too many arguments to function \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1015
    .restart local v1    # "msg":Ljava/lang/String;
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->hasFixedArgsLength()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1016
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "Expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1020
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, " but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1021
    new-instance v2, Lorg/apache/poi/ss/formula/FormulaParseException;

    invoke-direct {v2, v1}, Lorg/apache/poi/ss/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1007
    .end local v0    # "maxArgs":I
    .end local v1    # "msg":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getMaxParams()I

    move-result v0

    .line 1009
    .restart local v0    # "maxArgs":I
    goto :goto_1

    .line 1010
    .end local v0    # "maxArgs":I
    :cond_3
    invoke-virtual {p2}, Lorg/apache/poi/ss/formula/function/FunctionMetadata;->getMaxParams()I

    move-result v0

    .restart local v0    # "maxArgs":I
    goto :goto_1

    .line 1018
    .restart local v1    # "msg":Ljava/lang/String;
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "At most "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " were expected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1023
    .end local v1    # "msg":Ljava/lang/String;
    :cond_5
    return-void
.end method
