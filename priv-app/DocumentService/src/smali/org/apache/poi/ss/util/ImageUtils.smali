.class public Lorg/apache/poi/ss/util/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# static fields
.field public static final PIXEL_DPI:I = 0x60

.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/poi/ss/util/ImageUtils;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/util/ImageUtils;->logger:Lorg/apache/poi/util/POILogger;

    .line 32
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getImageDimension(Ljava/io/InputStream;I)Lorg/apache/poi/java/awt/Dimension;
    .locals 4
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "type"    # I

    .prologue
    .line 46
    new-instance v0, Lorg/apache/poi/java/awt/Dimension;

    invoke-direct {v0}, Lorg/apache/poi/java/awt/Dimension;-><init>()V

    .line 48
    .local v0, "size":Lorg/apache/poi/java/awt/Dimension;
    packed-switch p1, :pswitch_data_0

    .line 82
    sget-object v1, Lorg/apache/poi/ss/util/ImageUtils;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x5

    const-string/jumbo v3, "Only JPEG, PNG and DIB pictures can be automatically sized"

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 84
    :pswitch_0
    return-object v0

    .line 48
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
