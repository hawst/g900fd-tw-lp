.class public abstract Lorg/apache/poi/ss/formula/functions/BooleanFunction;
.super Ljava/lang/Object;
.source "BooleanFunction.java"

# interfaces
.implements Lorg/apache/poi/ss/formula/functions/Function;


# static fields
.field public static final AND:Lorg/apache/poi/ss/formula/functions/Function;

.field public static final FALSE:Lorg/apache/poi/ss/formula/functions/Function;

.field public static final NOT:Lorg/apache/poi/ss/formula/functions/Function;

.field public static final OR:Lorg/apache/poi/ss/formula/functions/Function;

.field public static final TRUE:Lorg/apache/poi/ss/formula/functions/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction$1;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/functions/BooleanFunction$1;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->AND:Lorg/apache/poi/ss/formula/functions/Function;

    .line 117
    new-instance v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction$2;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/functions/BooleanFunction$2;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->OR:Lorg/apache/poi/ss/formula/functions/Function;

    .line 125
    new-instance v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction$3;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/functions/BooleanFunction$3;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->FALSE:Lorg/apache/poi/ss/formula/functions/Function;

    .line 130
    new-instance v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction$4;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/functions/BooleanFunction$4;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->TRUE:Lorg/apache/poi/ss/formula/functions/Function;

    .line 135
    new-instance v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction$5;

    invoke-direct {v0}, Lorg/apache/poi/ss/formula/functions/BooleanFunction$5;-><init>()V

    sput-object v0, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->NOT:Lorg/apache/poi/ss/formula/functions/Function;

    .line 148
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private calculate([Lorg/apache/poi/ss/formula/eval/ValueEval;)Z
    .locals 14
    .param p1, "args"    # [Lorg/apache/poi/ss/formula/eval/ValueEval;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/ss/formula/eval/EvaluationException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    .line 57
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->getInitialResultValue()Z

    move-result v7

    .line 58
    .local v7, "result":Z
    const/4 v2, 0x0

    .line 63
    .local v2, "atleastOneNonBlank":Z
    const/4 v4, 0x0

    .local v4, "i":I
    array-length v5, p1

    .local v5, "iSize":I
    :goto_0
    if-lt v4, v5, :cond_0

    .line 98
    if-nez v2, :cond_8

    .line 99
    new-instance v12, Lorg/apache/poi/ss/formula/eval/EvaluationException;

    sget-object v13, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    invoke-direct {v12, v13}, Lorg/apache/poi/ss/formula/eval/EvaluationException;-><init>(Lorg/apache/poi/ss/formula/eval/ErrorEval;)V

    throw v12

    .line 64
    :cond_0
    aget-object v1, p1, v4

    .line 65
    .local v1, "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    instance-of v12, v1, Lorg/apache/poi/ss/formula/TwoDEval;

    if-eqz v12, :cond_5

    move-object v0, v1

    .line 66
    check-cast v0, Lorg/apache/poi/ss/formula/TwoDEval;

    .line 67
    .local v0, "ae":Lorg/apache/poi/ss/formula/TwoDEval;
    invoke-interface {v0}, Lorg/apache/poi/ss/formula/TwoDEval;->getHeight()I

    move-result v3

    .line 68
    .local v3, "height":I
    invoke-interface {v0}, Lorg/apache/poi/ss/formula/TwoDEval;->getWidth()I

    move-result v11

    .line 69
    .local v11, "width":I
    const/4 v8, 0x0

    .local v8, "rrIx":I
    :goto_1
    if-lt v8, v3, :cond_2

    .line 63
    .end local v0    # "ae":Lorg/apache/poi/ss/formula/TwoDEval;
    .end local v1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v3    # "height":I
    .end local v8    # "rrIx":I
    .end local v11    # "width":I
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 70
    .restart local v0    # "ae":Lorg/apache/poi/ss/formula/TwoDEval;
    .restart local v1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .restart local v3    # "height":I
    .restart local v8    # "rrIx":I
    .restart local v11    # "width":I
    :cond_2
    const/4 v6, 0x0

    .local v6, "rcIx":I
    :goto_3
    if-lt v6, v11, :cond_3

    .line 69
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 71
    :cond_3
    invoke-interface {v0, v8, v6}, Lorg/apache/poi/ss/formula/TwoDEval;->getValue(II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v10

    .line 72
    .local v10, "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-static {v10, v13}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->coerceValueToBoolean(Lorg/apache/poi/ss/formula/eval/ValueEval;Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 73
    .local v9, "tempVe":Ljava/lang/Boolean;
    if-eqz v9, :cond_4

    .line 74
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-virtual {p0, v7, v12}, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->partialEvaluate(ZZ)Z

    move-result v7

    .line 75
    const/4 v2, 0x1

    .line 70
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 82
    .end local v0    # "ae":Lorg/apache/poi/ss/formula/TwoDEval;
    .end local v3    # "height":I
    .end local v6    # "rcIx":I
    .end local v8    # "rrIx":I
    .end local v9    # "tempVe":Ljava/lang/Boolean;
    .end local v10    # "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v11    # "width":I
    :cond_5
    instance-of v12, v1, Lorg/apache/poi/ss/formula/eval/RefEval;

    if-eqz v12, :cond_6

    .line 83
    check-cast v1, Lorg/apache/poi/ss/formula/eval/RefEval;

    .end local v1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-interface {v1}, Lorg/apache/poi/ss/formula/eval/RefEval;->getInnerValueEval()Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object v10

    .line 84
    .restart local v10    # "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    invoke-static {v10, v13}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->coerceValueToBoolean(Lorg/apache/poi/ss/formula/eval/ValueEval;Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 92
    .end local v10    # "ve":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .restart local v9    # "tempVe":Ljava/lang/Boolean;
    :goto_4
    if-eqz v9, :cond_1

    .line 93
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-virtual {p0, v7, v12}, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->partialEvaluate(ZZ)Z

    move-result v7

    .line 94
    const/4 v2, 0x1

    goto :goto_2

    .line 85
    .end local v9    # "tempVe":Ljava/lang/Boolean;
    .restart local v1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    :cond_6
    sget-object v12, Lorg/apache/poi/ss/formula/eval/MissingArgEval;->instance:Lorg/apache/poi/ss/formula/eval/MissingArgEval;

    if-ne v1, v12, :cond_7

    .line 86
    const/4 v9, 0x0

    .line 87
    .restart local v9    # "tempVe":Ljava/lang/Boolean;
    goto :goto_4

    .line 88
    .end local v9    # "tempVe":Ljava/lang/Boolean;
    :cond_7
    const/4 v12, 0x0

    invoke-static {v1, v12}, Lorg/apache/poi/ss/formula/eval/OperandResolver;->coerceValueToBoolean(Lorg/apache/poi/ss/formula/eval/ValueEval;Z)Ljava/lang/Boolean;

    move-result-object v9

    .restart local v9    # "tempVe":Ljava/lang/Boolean;
    goto :goto_4

    .line 101
    .end local v1    # "arg":Lorg/apache/poi/ss/formula/eval/ValueEval;
    .end local v9    # "tempVe":Ljava/lang/Boolean;
    :cond_8
    return v7
.end method


# virtual methods
.method public final evaluate([Lorg/apache/poi/ss/formula/eval/ValueEval;II)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 4
    .param p1, "args"    # [Lorg/apache/poi/ss/formula/eval/ValueEval;
    .param p2, "srcRow"    # I
    .param p3, "srcCol"    # I

    .prologue
    .line 43
    array-length v2, p1

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    .line 44
    sget-object v2, Lorg/apache/poi/ss/formula/eval/ErrorEval;->VALUE_INVALID:Lorg/apache/poi/ss/formula/eval/ErrorEval;

    .line 52
    :goto_0
    return-object v2

    .line 48
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/formula/functions/BooleanFunction;->calculate([Lorg/apache/poi/ss/formula/eval/ValueEval;)Z
    :try_end_0
    .catch Lorg/apache/poi/ss/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 52
    .local v0, "boolResult":Z
    invoke-static {v0}, Lorg/apache/poi/ss/formula/eval/BoolEval;->valueOf(Z)Lorg/apache/poi/ss/formula/eval/BoolEval;

    move-result-object v2

    goto :goto_0

    .line 49
    .end local v0    # "boolResult":Z
    :catch_0
    move-exception v1

    .line 50
    .local v1, "e":Lorg/apache/poi/ss/formula/eval/EvaluationException;
    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/eval/EvaluationException;->getErrorEval()Lorg/apache/poi/ss/formula/eval/ErrorEval;

    move-result-object v2

    goto :goto_0
.end method

.method protected abstract getInitialResultValue()Z
.end method

.method protected abstract partialEvaluate(ZZ)Z
.end method
