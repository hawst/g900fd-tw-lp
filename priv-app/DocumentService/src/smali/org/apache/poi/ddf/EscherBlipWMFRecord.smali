.class public Lorg/apache/poi/ddf/EscherBlipWMFRecord;
.super Lorg/apache/poi/ddf/EscherBlipRecord;
.source "EscherBlipWMFRecord.java"


# static fields
.field private static final HEADER_SIZE:I = 0x8

.field public static final RECORD_DESCRIPTION:Ljava/lang/String; = "msofbtBlip"


# instance fields
.field private field_10_compressionFlag:B

.field private field_11_filter:B

.field private field_12_data:[B

.field private field_1_secondaryUID:[B

.field private field_2_cacheOfSize:I

.field private field_3_boundaryTop:I

.field private field_4_boundaryLeft:I

.field private field_5_boundaryWidth:I

.field private field_6_boundaryHeight:I

.field private field_7_width:I

.field private field_8_height:I

.field private field_9_cacheOfSavedSize:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/poi/ddf/EscherBlipRecord;-><init>()V

    return-void
.end method

.method public static compress([B)[B
    .locals 6
    .param p0, "data"    # [B

    .prologue
    .line 392
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 393
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v0, v3}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 396
    .local v0, "deflaterOutputStream":Ljava/util/zip/DeflaterOutputStream;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    array-length v4, p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v2, v4, :cond_0

    .line 404
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4

    .line 397
    :cond_0
    :try_start_1
    aget-byte v4, p0, v2

    invoke-virtual {v0, v4}, Ljava/util/zip/DeflaterOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 396
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 399
    :catch_0
    move-exception v1

    .line 401
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Lorg/apache/poi/util/RecordFormatException;

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/util/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static decompress([BII)[B
    .locals 8
    .param p0, "data"    # [B
    .param p1, "pos"    # I
    .param p2, "length"    # I

    .prologue
    .line 418
    new-array v1, p2, [B

    .line 419
    .local v1, "compressedData":[B
    add-int/lit8 v6, p1, 0x32

    const/4 v7, 0x0

    invoke-static {p0, v6, v1, v7, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 420
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 421
    .local v2, "compressedInputStream":Ljava/io/InputStream;
    new-instance v4, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v4, v2}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 422
    .local v4, "inflaterInputStream":Ljava/util/zip/InflaterInputStream;
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 426
    .local v5, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    :try_start_0
    invoke-virtual {v4}, Ljava/util/zip/InflaterInputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .local v0, "c":I
    const/4 v6, -0x1

    if-ne v0, v6, :cond_0

    .line 433
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .line 427
    :cond_0
    :try_start_1
    invoke-virtual {v5, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 429
    .end local v0    # "c":I
    :catch_0
    move-exception v3

    .line 431
    .local v3, "e":Ljava/io/IOException;
    new-instance v6, Lorg/apache/poi/util/RecordFormatException;

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/util/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v6
.end method


# virtual methods
.method public fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I
    .locals 8
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "recordFactory"    # Lorg/apache/poi/ddf/EscherRecordFactory;

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x0

    .line 62
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->readHeader([BI)I

    move-result v0

    .line 63
    .local v0, "bytesAfterHeader":I
    add-int/lit8 v2, p2, 0x8

    .line 65
    .local v2, "pos":I
    const/4 v3, 0x0

    .line 66
    .local v3, "size":I
    new-array v4, v7, [B

    iput-object v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    .line 67
    add-int v4, v2, v3

    iget-object v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    invoke-static {p1, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v3, 0x10

    .line 68
    add-int/lit8 v4, v2, 0x10

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_2_cacheOfSize:I

    add-int/lit8 v3, v3, 0x4

    .line 69
    add-int/lit8 v4, v2, 0x14

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_3_boundaryTop:I

    add-int/lit8 v3, v3, 0x4

    .line 70
    add-int/lit8 v4, v2, 0x18

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_4_boundaryLeft:I

    add-int/lit8 v3, v3, 0x4

    .line 71
    add-int/lit8 v4, v2, 0x1c

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_5_boundaryWidth:I

    add-int/lit8 v3, v3, 0x4

    .line 72
    add-int/lit8 v4, v2, 0x20

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_6_boundaryHeight:I

    add-int/lit8 v3, v3, 0x4

    .line 73
    add-int/lit8 v4, v2, 0x24

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_7_width:I

    add-int/lit8 v3, v3, 0x4

    .line 74
    add-int/lit8 v4, v2, 0x28

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_8_height:I

    add-int/lit8 v3, v3, 0x4

    .line 75
    add-int/lit8 v4, v2, 0x2c

    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_9_cacheOfSavedSize:I

    add-int/lit8 v3, v3, 0x4

    .line 76
    add-int/lit8 v4, v2, 0x30

    aget-byte v4, p1, v4

    iput-byte v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_10_compressionFlag:B

    add-int/lit8 v3, v3, 0x1

    .line 77
    add-int/lit8 v4, v2, 0x31

    aget-byte v4, p1, v4

    iput-byte v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_11_filter:B

    add-int/lit8 v3, v3, 0x1

    .line 79
    add-int/lit8 v1, v0, -0x32

    .line 80
    .local v1, "bytesRemaining":I
    new-array v4, v1, [B

    iput-object v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    .line 81
    add-int/lit8 v4, v2, 0x32

    iget-object v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    invoke-static {p1, v4, v5, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    add-int/lit8 v3, v1, 0x32

    .line 84
    add-int/lit8 v4, v3, 0x8

    return v4
.end method

.method public getBoundaryHeight()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_6_boundaryHeight:I

    return v0
.end method

.method public getBoundaryLeft()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_4_boundaryLeft:I

    return v0
.end method

.method public getBoundaryTop()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_3_boundaryTop:I

    return v0
.end method

.method public getBoundaryWidth()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_5_boundaryWidth:I

    return v0
.end method

.method public getCacheOfSavedSize()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_9_cacheOfSavedSize:I

    return v0
.end method

.method public getCacheOfSize()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_2_cacheOfSize:I

    return v0
.end method

.method public getCompressionFlag()B
    .locals 1

    .prologue
    .line 272
    iget-byte v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_10_compressionFlag:B

    return v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    return-object v0
.end method

.method public getFilter()B
    .locals 1

    .prologue
    .line 288
    iget-byte v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_11_filter:B

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_8_height:I

    return v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string/jumbo v0, "Blip"

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3a

    return v0
.end method

.method public getSecondaryUID()[B
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_7_width:I

    return v0
.end method

.method public serialize(I[BLorg/apache/poi/ddf/EscherSerializationListener;)I
    .locals 6
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/poi/ddf/EscherSerializationListener;

    .prologue
    const/4 v5, 0x0

    .line 89
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getRecordId()S

    move-result v3

    invoke-interface {p3, p1, v3, p0}, Lorg/apache/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/poi/ddf/EscherRecord;)V

    .line 91
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getOptions()S

    move-result v3

    invoke-static {p2, p1, v3}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 92
    add-int/lit8 v3, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getRecordId()S

    move-result v4

    invoke-static {p2, v3, v4}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 93
    iget-object v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    array-length v3, v3

    add-int/lit8 v2, v3, 0x24

    .line 94
    .local v2, "remainingBytes":I
    add-int/lit8 v3, p1, 0x4

    invoke-static {p2, v3, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 96
    add-int/lit8 v0, p1, 0x8

    .line 97
    .local v0, "pos":I
    iget-object v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    const/16 v4, 0x10

    invoke-static {v3, v5, p2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v0, 0x10

    .line 98
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_2_cacheOfSize:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 99
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_3_boundaryTop:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 100
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_4_boundaryLeft:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 101
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_5_boundaryWidth:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 102
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_6_boundaryHeight:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 103
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_7_width:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 104
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_8_height:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 105
    iget v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_9_cacheOfSavedSize:I

    invoke-static {p2, v0, v3}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    add-int/lit8 v0, v0, 0x4

    .line 106
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "pos":I
    .local v1, "pos":I
    iget-byte v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_10_compressionFlag:B

    aput-byte v3, p2, v0

    .line 107
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "pos":I
    .restart local v0    # "pos":I
    iget-byte v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_11_filter:B

    aput-byte v3, p2, v1

    .line 108
    iget-object v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    iget-object v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    array-length v4, v4

    invoke-static {v3, v5, p2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 110
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getRecordId()S

    move-result v3

    sub-int v4, v0, p1

    invoke-interface {p3, v0, v3, v4, p0}, Lorg/apache/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/poi/ddf/EscherRecord;)V

    .line 111
    sub-int v3, v0, p1

    return v3
.end method

.method public setBoundaryHeight(I)V
    .locals 0
    .param p1, "field_6_boundaryHeight"    # I

    .prologue
    .line 216
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_6_boundaryHeight:I

    .line 217
    return-void
.end method

.method public setBoundaryLeft(I)V
    .locals 0
    .param p1, "field_4_boundaryLeft"    # I

    .prologue
    .line 184
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_4_boundaryLeft:I

    .line 185
    return-void
.end method

.method public setBoundaryTop(I)V
    .locals 0
    .param p1, "field_3_boundaryTop"    # I

    .prologue
    .line 168
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_3_boundaryTop:I

    .line 169
    return-void
.end method

.method public setBoundaryWidth(I)V
    .locals 0
    .param p1, "field_5_boundaryWidth"    # I

    .prologue
    .line 200
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_5_boundaryWidth:I

    .line 201
    return-void
.end method

.method public setCacheOfSavedSize(I)V
    .locals 0
    .param p1, "field_9_cacheOfSavedSize"    # I

    .prologue
    .line 264
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_9_cacheOfSavedSize:I

    .line 265
    return-void
.end method

.method public setCacheOfSize(I)V
    .locals 0
    .param p1, "field_2_cacheOfSize"    # I

    .prologue
    .line 152
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_2_cacheOfSize:I

    .line 153
    return-void
.end method

.method public setCompressionFlag(B)V
    .locals 0
    .param p1, "field_10_compressionFlag"    # B

    .prologue
    .line 280
    iput-byte p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_10_compressionFlag:B

    .line 281
    return-void
.end method

.method public setData([B)V
    .locals 0
    .param p1, "field_12_data"    # [B

    .prologue
    .line 312
    iput-object p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    .line 313
    return-void
.end method

.method public setFilter(B)V
    .locals 0
    .param p1, "field_11_filter"    # B

    .prologue
    .line 296
    iput-byte p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_11_filter:B

    .line 297
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 248
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_8_height:I

    .line 249
    return-void
.end method

.method public setSecondaryUID([B)V
    .locals 0
    .param p1, "field_1_secondaryUID"    # [B

    .prologue
    .line 136
    iput-object p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    .line 137
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 232
    iput p1, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_7_width:I

    .line 233
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 322
    const-string/jumbo v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 325
    .local v3, "nl":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 328
    .local v0, "b":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v6, v7, v0, v5}, Lorg/apache/poi/util/HexDump;->dump([BJLjava/io/OutputStream;I)V

    .line 329
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 335
    .local v2, "extraData":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 336
    const-string/jumbo v5, "  RecordId: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getRecordId()S

    move-result v5

    invoke-static {v5}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 337
    const-string/jumbo v5, "  Version: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getVersion()S

    move-result v5

    invoke-static {v5}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 338
    const-string/jumbo v5, "  Instance: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getInstance()S

    move-result v5

    invoke-static {v5}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 339
    const-string/jumbo v5, "  Secondary UID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    invoke-static {v5}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 340
    const-string/jumbo v5, "  CacheOfSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_2_cacheOfSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 341
    const-string/jumbo v5, "  BoundaryTop: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_3_boundaryTop:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 342
    const-string/jumbo v5, "  BoundaryLeft: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_4_boundaryLeft:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 343
    const-string/jumbo v5, "  BoundaryWidth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_5_boundaryWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 344
    const-string/jumbo v5, "  BoundaryHeight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_6_boundaryHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 345
    const-string/jumbo v5, "  X: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_7_width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 346
    const-string/jumbo v5, "  Y: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_8_height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 347
    const-string/jumbo v5, "  CacheOfSavedSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_9_cacheOfSavedSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 348
    const-string/jumbo v5, "  CompressionFlag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-byte v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_10_compressionFlag:B

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 349
    const-string/jumbo v5, "  Filter: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-byte v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_11_filter:B

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 350
    const-string/jumbo v5, "  Data:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 335
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 331
    .end local v2    # "extraData":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 333
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "extraData":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public toXml(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "tab"    # Ljava/lang/String;

    .prologue
    .line 356
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 359
    .local v0, "b":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_12_data:[B

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v6, v7, v0, v5}, Lorg/apache/poi/util/HexDump;->dump([BJLjava/io/OutputStream;I)V

    .line 360
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 366
    .local v3, "extraData":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getRecordId()S

    move-result v6

    invoke-static {v6}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getVersion()S

    move-result v7

    invoke-static {v7}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->getInstance()S

    move-result v8

    invoke-static {v8}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v6, v7, v8}, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->formatXmlRecordHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 368
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<SecondaryUID>0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_1_secondaryUID:[B

    invoke-static {v5}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</SecondaryUID>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 369
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<CacheOfSize>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_2_cacheOfSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</CacheOfSize>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 370
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<BoundaryTop>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_3_boundaryTop:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</BoundaryTop>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 371
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<BoundaryLeft>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_4_boundaryLeft:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</BoundaryLeft>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 372
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<BoundaryWidth>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_5_boundaryWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</BoundaryWidth>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 373
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<BoundaryHeight>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_6_boundaryHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</BoundaryHeight>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 374
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<X>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_7_width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</X>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 375
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<Y>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_8_height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</Y>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 376
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<CacheOfSavedSize>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_9_cacheOfSavedSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</CacheOfSavedSize>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 377
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<CompressionFlag>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-byte v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_10_compressionFlag:B

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</CompressionFlag>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 378
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<Filter>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-byte v5, p0, Lorg/apache/poi/ddf/EscherBlipWMFRecord;->field_11_filter:B

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</Filter>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 379
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "<Data>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</Data>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "</"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ">\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 362
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v3    # "extraData":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 364
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "extraData":Ljava/lang/String;
    goto/16 :goto_0
.end method
