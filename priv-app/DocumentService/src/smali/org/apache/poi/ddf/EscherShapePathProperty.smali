.class public Lorg/apache/poi/ddf/EscherShapePathProperty;
.super Lorg/apache/poi/ddf/EscherSimpleProperty;
.source "EscherShapePathProperty.java"


# static fields
.field public static final CLOSED_CURVES:I = 0x3

.field public static final CLOSED_POLYGON:I = 0x1

.field public static final COMPLEX:I = 0x4

.field public static final CURVES:I = 0x2

.field public static final LINE_OF_STRAIGHT_SEGMENTS:I


# direct methods
.method public constructor <init>(SI)V
    .locals 1
    .param p1, "propertyNumber"    # S
    .param p2, "shapePath"    # I

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, v0, v0, p2}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    .line 36
    return-void
.end method
