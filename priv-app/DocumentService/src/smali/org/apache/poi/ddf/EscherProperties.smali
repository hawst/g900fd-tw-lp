.class public final Lorg/apache/poi/ddf/EscherProperties;
.super Ljava/lang/Object;
.source "EscherProperties.java"


# static fields
.field public static final BLIP__BLIPFILENAME:S = 0x105s

.field public static final BLIP__BLIPFLAGS:S = 0x106s

.field public static final BLIP__BLIPTODISPLAY:S = 0x104s

.field public static final BLIP__BRIGHTNESSSETTING:S = 0x109s

.field public static final BLIP__CONTRASTSETTING:S = 0x108s

.field public static final BLIP__CROPFROMBOTTOM:S = 0x101s

.field public static final BLIP__CROPFROMLEFT:S = 0x102s

.field public static final BLIP__CROPFROMRIGHT:S = 0x103s

.field public static final BLIP__CROPFROMTOP:S = 0x100s

.field public static final BLIP__DOUBLEMOD:S = 0x10cs

.field public static final BLIP__GAMMA:S = 0x10as

.field public static final BLIP__NOHITTESTPICTURE:S = 0x13cs

.field public static final BLIP__PICTUREACTIVE:S = 0x13fs

.field public static final BLIP__PICTUREBILEVEL:S = 0x13es

.field public static final BLIP__PICTUREFILLMOD:S = 0x10ds

.field public static final BLIP__PICTUREGRAY:S = 0x13ds

.field public static final BLIP__PICTUREID:S = 0x10bs

.field public static final BLIP__PICTURELINE:S = 0x10es

.field public static final BLIP__PRINTBLIP:S = 0x10fs

.field public static final BLIP__PRINTBLIPFILENAME:S = 0x110s

.field public static final BLIP__PRINTFLAGS:S = 0x111s

.field public static final BLIP__TRANSPARENTCOLOR:S = 0x107s

.field public static final CALLOUT__CALLOUTACCENTBAR:S = 0x37as

.field public static final CALLOUT__CALLOUTANGLE:S = 0x342s

.field public static final CALLOUT__CALLOUTDROPSPECIFIED:S = 0x344s

.field public static final CALLOUT__CALLOUTDROPTYPE:S = 0x343s

.field public static final CALLOUT__CALLOUTLENGTHSPECIFIED:S = 0x345s

.field public static final CALLOUT__CALLOUTMINUSX:S = 0x37cs

.field public static final CALLOUT__CALLOUTMINUSY:S = 0x37ds

.field public static final CALLOUT__CALLOUTTEXTBORDER:S = 0x37bs

.field public static final CALLOUT__CALLOUTTYPE:S = 0x340s

.field public static final CALLOUT__DROPAUTO:S = 0x37es

.field public static final CALLOUT__ISCALLOUT:S = 0x379s

.field public static final CALLOUT__LENGTHSPECIFIED:S = 0x37fs

.field public static final CALLOUT__XYCALLOUTGAP:S = 0x341s

.field public static final FILL__ANGLE:S = 0x18bs

.field public static final FILL__BACKOPACITY:S = 0x184s

.field public static final FILL__BLIPFILENAME:S = 0x187s

.field public static final FILL__BLIPFLAGS:S = 0x188s

.field public static final FILL__CRMOD:S = 0x185s

.field public static final FILL__DZTYPE:S = 0x195s

.field public static final FILL__FILLBACKCOLOR:S = 0x183s

.field public static final FILL__FILLCOLOR:S = 0x181s

.field public static final FILL__FILLED:S = 0x1bbs

.field public static final FILL__FILLOPACITY:S = 0x182s

.field public static final FILL__FILLTYPE:S = 0x180s

.field public static final FILL__FOCUS:S = 0x18cs

.field public static final FILL__HEIGHT:S = 0x18as

.field public static final FILL__HITTESTFILL:S = 0x1bcs

.field public static final FILL__NOFILLHITTEST:S = 0x1bfs

.field public static final FILL__ORIGINX:S = 0x198s

.field public static final FILL__ORIGINY:S = 0x199s

.field public static final FILL__PATTERNTEXTURE:S = 0x186s

.field public static final FILL__RECTBOTTOM:S = 0x194s

.field public static final FILL__RECTLEFT:S = 0x191s

.field public static final FILL__RECTRIGHT:S = 0x193s

.field public static final FILL__RECTTOP:S = 0x192s

.field public static final FILL__SHADECOLORS:S = 0x197s

.field public static final FILL__SHADEPRESET:S = 0x196s

.field public static final FILL__SHADETYPE:S = 0x19cs

.field public static final FILL__SHAPE:S = 0x1bds

.field public static final FILL__SHAPEORIGINX:S = 0x19as

.field public static final FILL__SHAPEORIGINY:S = 0x19bs

.field public static final FILL__TOBOTTOM:S = 0x190s

.field public static final FILL__TOLEFT:S = 0x18ds

.field public static final FILL__TORIGHT:S = 0x18fs

.field public static final FILL__TOTOP:S = 0x18es

.field public static final FILL__USERECT:S = 0x1bes

.field public static final FILL__WIDTH:S = 0x189s

.field public static final GEOMETRY__3DOK:S = 0x17bs

.field public static final GEOMETRY__ADJUST10VALUE:S = 0x150s

.field public static final GEOMETRY__ADJUST2VALUE:S = 0x148s

.field public static final GEOMETRY__ADJUST3VALUE:S = 0x149s

.field public static final GEOMETRY__ADJUST4VALUE:S = 0x14as

.field public static final GEOMETRY__ADJUST5VALUE:S = 0x14bs

.field public static final GEOMETRY__ADJUST6VALUE:S = 0x14cs

.field public static final GEOMETRY__ADJUST7VALUE:S = 0x14ds

.field public static final GEOMETRY__ADJUST8VALUE:S = 0x14es

.field public static final GEOMETRY__ADJUST9VALUE:S = 0x14fs

.field public static final GEOMETRY__ADJUSTVALUE:S = 0x147s

.field public static final GEOMETRY__BOTTOM:S = 0x143s

.field public static final GEOMETRY__FILLOK:S = 0x17fs

.field public static final GEOMETRY__FILLSHADESHAPEOK:S = 0x17es

.field public static final GEOMETRY__GEOTEXTOK:S = 0x17ds

.field public static final GEOMETRY__LEFT:S = 0x140s

.field public static final GEOMETRY__LINEOK:S = 0x17cs

.field public static final GEOMETRY__RIGHT:S = 0x142s

.field public static final GEOMETRY__SEGMENTINFO:S = 0x146s

.field public static final GEOMETRY__SHADOWok:S = 0x17as

.field public static final GEOMETRY__SHAPEPATH:S = 0x144s

.field public static final GEOMETRY__TOP:S = 0x141s

.field public static final GEOMETRY__VERTICES:S = 0x145s

.field public static final GEOTEXT__ALIGNMENTONCURVE:S = 0xc2s

.field public static final GEOTEXT__BOLDFONT:S = 0xfas

.field public static final GEOTEXT__CHARBOUNDINGBOX:S = 0xf6s

.field public static final GEOTEXT__DEFAULTPOINTSIZE:S = 0xc3s

.field public static final GEOTEXT__FONTFAMILYNAME:S = 0xc5s

.field public static final GEOTEXT__HASTEXTEFFECT:S = 0xf1s

.field public static final GEOTEXT__ITALICFONT:S = 0xfbs

.field public static final GEOTEXT__KERNCHARACTERS:S = 0xf3s

.field public static final GEOTEXT__NOMEASUREALONGPATH:S = 0xf9s

.field public static final GEOTEXT__REVERSEROWORDER:S = 0xf0s

.field public static final GEOTEXT__ROTATECHARACTERS:S = 0xf2s

.field public static final GEOTEXT__RTFTEXT:S = 0xc1s

.field public static final GEOTEXT__SCALETEXTONPATH:S = 0xf7s

.field public static final GEOTEXT__SHADOWFONT:S = 0xfds

.field public static final GEOTEXT__SMALLCAPSFONT:S = 0xfes

.field public static final GEOTEXT__STRETCHCHARHEIGHT:S = 0xf8s

.field public static final GEOTEXT__STRETCHTOFITSHAPE:S = 0xf5s

.field public static final GEOTEXT__STRIKETHROUGHFONT:S = 0xffs

.field public static final GEOTEXT__TEXTSPACING:S = 0xc4s

.field public static final GEOTEXT__TIGHTORTRACK:S = 0xf4s

.field public static final GEOTEXT__UNDERLINEFONT:S = 0xfcs

.field public static final GEOTEXT__UNICODE:S = 0xc0s

.field public static final GROUPSHAPE__1DADJUSTMENT:S = 0x3bds

.field public static final GROUPSHAPE__BEHINDDOCUMENT:S = 0x3bas

.field public static final GROUPSHAPE__BORDERBOTTOMCOLOR:S = 0x39ds

.field public static final GROUPSHAPE__BORDERLEFTCOLOR:S = 0x39cs

.field public static final GROUPSHAPE__BORDERRIGHTCOLOR:S = 0x39es

.field public static final GROUPSHAPE__BORDERTOPCOLOR:S = 0x39bs

.field public static final GROUPSHAPE__DESCRIPTION:S = 0x381s

.field public static final GROUPSHAPE__EDITEDWRAP:S = 0x3b9s

.field public static final GROUPSHAPE__FLAGS:S = 0x3bfs

.field public static final GROUPSHAPE__HIDDEN:S = 0x3bes

.field public static final GROUPSHAPE__HR_ALIGN:S = 0x394s

.field public static final GROUPSHAPE__HR_HEIGHT:S = 0x395s

.field public static final GROUPSHAPE__HR_PCT:S = 0x393s

.field public static final GROUPSHAPE__HR_WIDTH:S = 0x396s

.field public static final GROUPSHAPE__HYPERLINK:S = 0x382s

.field public static final GROUPSHAPE__ISBUTTON:S = 0x3bcs

.field public static final GROUPSHAPE__METROBLOB:S = 0x3a9s

.field public static final GROUPSHAPE__ONDBLCLICKNOTIFY:S = 0x3bbs

.field public static final GROUPSHAPE__POSH:S = 0x38fs

.field public static final GROUPSHAPE__POSRELH:S = 0x390s

.field public static final GROUPSHAPE__POSRELV:S = 0x392s

.field public static final GROUPSHAPE__POSV:S = 0x391s

.field public static final GROUPSHAPE__PRINT:S = 0x3bfs

.field public static final GROUPSHAPE__REGROUPID:S = 0x388s

.field public static final GROUPSHAPE__SCRIPT:S = 0x38es

.field public static final GROUPSHAPE__SCRIPTEXT:S = 0x397s

.field public static final GROUPSHAPE__SCRIPTLANG:S = 0x398s

.field public static final GROUPSHAPE__SHAPENAME:S = 0x380s

.field public static final GROUPSHAPE__TABLEPROPERTIES:S = 0x39fs

.field public static final GROUPSHAPE__TABLEROWPROPERTIES:S = 0x3a0s

.field public static final GROUPSHAPE__TOOLTIP:S = 0x38ds

.field public static final GROUPSHAPE__UNUSED906:S = 0x38as

.field public static final GROUPSHAPE__WEBBOT:S = 0x3a5s

.field public static final GROUPSHAPE__WRAPDISTBOTTOM:S = 0x387s

.field public static final GROUPSHAPE__WRAPDISTLEFT:S = 0x384s

.field public static final GROUPSHAPE__WRAPDISTRIGHT:S = 0x386s

.field public static final GROUPSHAPE__WRAPDISTTOP:S = 0x385s

.field public static final GROUPSHAPE__WRAPPOLYGONVERTICES:S = 0x383s

.field public static final GROUPSHAPE__ZORDER:S = 0x3aas

.field public static final LINESTYLE__ANYLINE:S = 0x1fcs

.field public static final LINESTYLE__ARROWHEADSOK:S = 0x1fbs

.field public static final LINESTYLE__BACKCOLOR:S = 0x1c2s

.field public static final LINESTYLE__COLOR:S = 0x1c0s

.field public static final LINESTYLE__CRMOD:S = 0x1c3s

.field public static final LINESTYLE__FILLBLIP:S = 0x1c5s

.field public static final LINESTYLE__FILLBLIPFLAGS:S = 0x1c7s

.field public static final LINESTYLE__FILLBLIPNAME:S = 0x1c6s

.field public static final LINESTYLE__FILLDZTYPE:S = 0x1cas

.field public static final LINESTYLE__FILLHEIGHT:S = 0x1c9s

.field public static final LINESTYLE__FILLWIDTH:S = 0x1c8s

.field public static final LINESTYLE__HITLINETEST:S = 0x1fds

.field public static final LINESTYLE__LINEDASHING:S = 0x1ces

.field public static final LINESTYLE__LINEDASHSTYLE:S = 0x1cfs

.field public static final LINESTYLE__LINEENDARROWHEAD:S = 0x1d1s

.field public static final LINESTYLE__LINEENDARROWLENGTH:S = 0x1d5s

.field public static final LINESTYLE__LINEENDARROWWIDTH:S = 0x1d4s

.field public static final LINESTYLE__LINEENDCAPSTYLE:S = 0x1d7s

.field public static final LINESTYLE__LINEESTARTARROWLENGTH:S = 0x1d3s

.field public static final LINESTYLE__LINEFILLSHAPE:S = 0x1fes

.field public static final LINESTYLE__LINEJOINSTYLE:S = 0x1d6s

.field public static final LINESTYLE__LINEMITERLIMIT:S = 0x1ccs

.field public static final LINESTYLE__LINESTARTARROWHEAD:S = 0x1d0s

.field public static final LINESTYLE__LINESTARTARROWWIDTH:S = 0x1d2s

.field public static final LINESTYLE__LINESTYLE:S = 0x1cds

.field public static final LINESTYLE__LINETYPE:S = 0x1c4s

.field public static final LINESTYLE__LINEWIDTH:S = 0x1cbs

.field public static final LINESTYLE__NOLINEDRAWDASH:S = 0x1ffs

.field public static final LINESTYLE__OPACITY:S = 0x1c1s

.field public static final PERSPECTIVE__OFFSETX:S = 0x241s

.field public static final PERSPECTIVE__OFFSETY:S = 0x242s

.field public static final PERSPECTIVE__ORIGINX:S = 0x24as

.field public static final PERSPECTIVE__ORIGINY:S = 0x24bs

.field public static final PERSPECTIVE__PERSPECTIVEON:S = 0x27fs

.field public static final PERSPECTIVE__PERSPECTIVEX:S = 0x247s

.field public static final PERSPECTIVE__PERSPECTIVEY:S = 0x248s

.field public static final PERSPECTIVE__SCALEXTOX:S = 0x243s

.field public static final PERSPECTIVE__SCALEXTOY:S = 0x245s

.field public static final PERSPECTIVE__SCALEYTOX:S = 0x244s

.field public static final PERSPECTIVE__SCALEYTOY:S = 0x246s

.field public static final PERSPECTIVE__TYPE:S = 0x240s

.field public static final PERSPECTIVE__WEIGHT:S = 0x249s

.field public static final PROTECTION__LOCKADJUSTHANDLES:S = 0x7es

.field public static final PROTECTION__LOCKAGAINSTGROUPING:S = 0x7fs

.field public static final PROTECTION__LOCKAGAINSTSELECT:S = 0x7as

.field public static final PROTECTION__LOCKASPECTRATIO:S = 0x78s

.field public static final PROTECTION__LOCKCROPPING:S = 0x7bs

.field public static final PROTECTION__LOCKPOSITION:S = 0x79s

.field public static final PROTECTION__LOCKROTATION:S = 0x77s

.field public static final PROTECTION__LOCKTEXT:S = 0x7ds

.field public static final PROTECTION__LOCKVERTICES:S = 0x7cs

.field public static final SHADOWSTYLE__COLOR:S = 0x201s

.field public static final SHADOWSTYLE__CRMOD:S = 0x203s

.field public static final SHADOWSTYLE__HIGHLIGHT:S = 0x202s

.field public static final SHADOWSTYLE__OFFSETX:S = 0x205s

.field public static final SHADOWSTYLE__OFFSETY:S = 0x206s

.field public static final SHADOWSTYLE__OPACITY:S = 0x204s

.field public static final SHADOWSTYLE__ORIGINX:S = 0x210s

.field public static final SHADOWSTYLE__ORIGINY:S = 0x211s

.field public static final SHADOWSTYLE__PERSPECTIVEX:S = 0x20ds

.field public static final SHADOWSTYLE__PERSPECTIVEY:S = 0x20es

.field public static final SHADOWSTYLE__SCALEXTOX:S = 0x209s

.field public static final SHADOWSTYLE__SCALEXTOY:S = 0x20bs

.field public static final SHADOWSTYLE__SCALEYTOX:S = 0x20as

.field public static final SHADOWSTYLE__SCALEYTOY:S = 0x20cs

.field public static final SHADOWSTYLE__SECONDOFFSETX:S = 0x207s

.field public static final SHADOWSTYLE__SECONDOFFSETY:S = 0x208s

.field public static final SHADOWSTYLE__SHADOW:S = 0x23es

.field public static final SHADOWSTYLE__SHADOWOBSURED:S = 0x23fs

.field public static final SHADOWSTYLE__TYPE:S = 0x200s

.field public static final SHADOWSTYLE__WEIGHT:S = 0x20fs

.field public static final SHAPE__BACKGROUNDSHAPE:S = 0x33fs

.field public static final SHAPE__BLACKANDWHITESETTINGS:S = 0x304s

.field public static final SHAPE__CONNECTORSTYLE:S = 0x303s

.field public static final SHAPE__DELETEATTACHEDOBJECT:S = 0x33es

.field public static final SHAPE__LOCKSHAPETYPE:S = 0x33cs

.field public static final SHAPE__MASTER:S = 0x301s

.field public static final SHAPE__OLEICON:S = 0x33as

.field public static final SHAPE__PREFERRELATIVERESIZE:S = 0x33bs

.field public static final SHAPE__WMODEBW:S = 0x306s

.field public static final SHAPE__WMODEPUREBW:S = 0x305s

.field public static final TEXT__ANCHORTEXT:S = 0x87s

.field public static final TEXT__BIDIR:S = 0x8bs

.field public static final TEXT__FONTROTATION:S = 0x89s

.field public static final TEXT__IDOFNEXTSHAPE:S = 0x8as

.field public static final TEXT__ROTATETEXTWITHSHAPE:S = 0xbds

.field public static final TEXT__SCALETEXT:S = 0x86s

.field public static final TEXT__SINGLECLICKSELECTS:S = 0xbbs

.field public static final TEXT__SIZESHAPETOFITTEXT:S = 0xbes

.field public static final TEXT__SIZE_TEXT_TO_FIT_SHAPE:S = 0xbfs

.field public static final TEXT__TEXTBOTTOM:S = 0x84s

.field public static final TEXT__TEXTFLOW:S = 0x88s

.field public static final TEXT__TEXTID:S = 0x80s

.field public static final TEXT__TEXTLEFT:S = 0x81s

.field public static final TEXT__TEXTRIGHT:S = 0x83s

.field public static final TEXT__TEXTTOP:S = 0x82s

.field public static final TEXT__USEHOSTMARGINS:S = 0xbcs

.field public static final TEXT__WRAPTEXT:S = 0x85s

.field public static final THREEDSTYLE__AMBIENTINTENSITY:S = 0x2d2s

.field public static final THREEDSTYLE__CONSTRAINROTATION:S = 0x2fbs

.field public static final THREEDSTYLE__FILLHARSH:S = 0x2ffs

.field public static final THREEDSTYLE__FILLINTENSITY:S = 0x2das

.field public static final THREEDSTYLE__FILLX:S = 0x2d7s

.field public static final THREEDSTYLE__FILLY:S = 0x2d8s

.field public static final THREEDSTYLE__FILLZ:S = 0x2d9s

.field public static final THREEDSTYLE__KEYHARSH:S = 0x2fes

.field public static final THREEDSTYLE__KEYINTENSITY:S = 0x2d6s

.field public static final THREEDSTYLE__KEYX:S = 0x2d3s

.field public static final THREEDSTYLE__KEYY:S = 0x2d4s

.field public static final THREEDSTYLE__KEYZ:S = 0x2d5s

.field public static final THREEDSTYLE__ORIGINX:S = 0x2ces

.field public static final THREEDSTYLE__ORIGINY:S = 0x2cfs

.field public static final THREEDSTYLE__PARALLEL:S = 0x2fds

.field public static final THREEDSTYLE__RENDERMODE:S = 0x2c9s

.field public static final THREEDSTYLE__ROTATIONANGLE:S = 0x2c5s

.field public static final THREEDSTYLE__ROTATIONAXISX:S = 0x2c2s

.field public static final THREEDSTYLE__ROTATIONAXISY:S = 0x2c3s

.field public static final THREEDSTYLE__ROTATIONAXISZ:S = 0x2c4s

.field public static final THREEDSTYLE__ROTATIONCENTERAUTO:S = 0x2fcs

.field public static final THREEDSTYLE__ROTATIONCENTERX:S = 0x2c6s

.field public static final THREEDSTYLE__ROTATIONCENTERY:S = 0x2c7s

.field public static final THREEDSTYLE__ROTATIONCENTERZ:S = 0x2c8s

.field public static final THREEDSTYLE__SKEWAMOUNT:S = 0x2d1s

.field public static final THREEDSTYLE__SKEWANGLE:S = 0x2d0s

.field public static final THREEDSTYLE__TOLERANCE:S = 0x2cas

.field public static final THREEDSTYLE__XROTATIONANGLE:S = 0x2c1s

.field public static final THREEDSTYLE__XVIEWPOINT:S = 0x2cbs

.field public static final THREEDSTYLE__YROTATIONANGLE:S = 0x2c0s

.field public static final THREEDSTYLE__YVIEWPOINT:S = 0x2ccs

.field public static final THREEDSTYLE__ZVIEWPOINT:S = 0x2cds

.field public static final THREED__3DEFFECT:S = 0x2bcs

.field public static final THREED__CRMOD:S = 0x288s

.field public static final THREED__DIFFUSEAMOUNT:S = 0x295s

.field public static final THREED__EDGETHICKNESS:S = 0x297s

.field public static final THREED__EXTRUDEBACKWARD:S = 0x299s

.field public static final THREED__EXTRUDEFORWARD:S = 0x298s

.field public static final THREED__EXTRUDEPLANE:S = 0x29as

.field public static final THREED__EXTRUSIONCOLOR:S = 0x29bs

.field public static final THREED__LIGHTFACE:S = 0x2bfs

.field public static final THREED__METALLIC:S = 0x2bds

.field public static final THREED__SHININESS:S = 0x296s

.field public static final THREED__SPECULARAMOUNT:S = 0x280s

.field public static final THREED__USEEXTRUSIONCOLOR:S = 0x2bes

.field public static final TRANSFORM__ROTATION:S = 0x4s

.field private static final properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Lorg/apache/poi/ddf/EscherPropertyMetaData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 330
    invoke-static {}, Lorg/apache/poi/ddf/EscherProperties;->initProps()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ddf/EscherProperties;->properties:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addProp(Ljava/util/Map;ILjava/lang/String;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "propName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Lorg/apache/poi/ddf/EscherPropertyMetaData;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 636
    .local p0, "m":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Short;Lorg/apache/poi/ddf/EscherPropertyMetaData;>;"
    int-to-short v0, p1

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    new-instance v1, Lorg/apache/poi/ddf/EscherPropertyMetaData;

    invoke-direct {v1, p2}, Lorg/apache/poi/ddf/EscherPropertyMetaData;-><init>(Ljava/lang/String;)V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 637
    return-void
.end method

.method private static addProp(Ljava/util/Map;ILjava/lang/String;B)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "propName"    # Ljava/lang/String;
    .param p3, "type"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Lorg/apache/poi/ddf/EscherPropertyMetaData;",
            ">;I",
            "Ljava/lang/String;",
            "B)V"
        }
    .end annotation

    .prologue
    .line 640
    .local p0, "m":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Short;Lorg/apache/poi/ddf/EscherPropertyMetaData;>;"
    int-to-short v0, p1

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    new-instance v1, Lorg/apache/poi/ddf/EscherPropertyMetaData;

    invoke-direct {v1, p2, p3}, Lorg/apache/poi/ddf/EscherPropertyMetaData;-><init>(Ljava/lang/String;B)V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    return-void
.end method

.method public static getPropertyName(S)Ljava/lang/String;
    .locals 3
    .param p0, "propertyId"    # S

    .prologue
    .line 644
    sget-object v1, Lorg/apache/poi/ddf/EscherProperties;->properties:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherPropertyMetaData;

    .line 645
    .local v0, "o":Lorg/apache/poi/ddf/EscherPropertyMetaData;
    if-nez v0, :cond_0

    const-string/jumbo v1, "unknown"

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherPropertyMetaData;->getDescription()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getPropertyType(S)B
    .locals 3
    .param p0, "propertyId"    # S

    .prologue
    .line 649
    sget-object v1, Lorg/apache/poi/ddf/EscherProperties;->properties:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherPropertyMetaData;

    .line 650
    .local v0, "escherPropertyMetaData":Lorg/apache/poi/ddf/EscherPropertyMetaData;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherPropertyMetaData;->getType()B

    move-result v1

    goto :goto_0
.end method

.method private static initProps()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Lorg/apache/poi/ddf/EscherPropertyMetaData;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0x3bf

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 333
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 334
    .local v0, "m":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Short;Lorg/apache/poi/ddf/EscherPropertyMetaData;>;"
    const/4 v1, 0x4

    const-string/jumbo v2, "transform.rotation"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 335
    const/16 v1, 0x77

    const-string/jumbo v2, "protection.lockrotation"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 336
    const/16 v1, 0x78

    const-string/jumbo v2, "protection.lockaspectratio"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 337
    const/16 v1, 0x79

    const-string/jumbo v2, "protection.lockposition"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 338
    const/16 v1, 0x7a

    const-string/jumbo v2, "protection.lockagainstselect"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 339
    const/16 v1, 0x7b

    const-string/jumbo v2, "protection.lockcropping"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 340
    const/16 v1, 0x7c

    const-string/jumbo v2, "protection.lockvertices"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 341
    const/16 v1, 0x7d

    const-string/jumbo v2, "protection.locktext"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 342
    const/16 v1, 0x7e

    const-string/jumbo v2, "protection.lockadjusthandles"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 343
    const/16 v1, 0x7f

    const-string/jumbo v2, "protection.lockagainstgrouping"

    invoke-static {v0, v1, v2, v5}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 344
    const/16 v1, 0x80

    const-string/jumbo v2, "text.textid"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 345
    const/16 v1, 0x81

    const-string/jumbo v2, "text.textleft"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 346
    const/16 v1, 0x82

    const-string/jumbo v2, "text.texttop"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 347
    const/16 v1, 0x83

    const-string/jumbo v2, "text.textright"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 348
    const/16 v1, 0x84

    const-string/jumbo v2, "text.textbottom"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 349
    const/16 v1, 0x85

    const-string/jumbo v2, "text.wraptext"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 350
    const/16 v1, 0x86

    const-string/jumbo v2, "text.scaletext"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 351
    const/16 v1, 0x87

    const-string/jumbo v2, "text.anchortext"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 352
    const/16 v1, 0x88

    const-string/jumbo v2, "text.textflow"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 353
    const/16 v1, 0x89

    const-string/jumbo v2, "text.fontrotation"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 354
    const/16 v1, 0x8a

    const-string/jumbo v2, "text.idofnextshape"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 355
    const/16 v1, 0x8b

    const-string/jumbo v2, "text.bidir"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 356
    const/16 v1, 0xbb

    const-string/jumbo v2, "text.singleclickselects"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 357
    const/16 v1, 0xbc

    const-string/jumbo v2, "text.usehostmargins"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 358
    const/16 v1, 0xbd

    const-string/jumbo v2, "text.rotatetextwithshape"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 359
    const/16 v1, 0xbe

    const-string/jumbo v2, "text.sizeshapetofittext"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 360
    const/16 v1, 0xbf

    const-string/jumbo v2, "text.sizetexttofitshape"

    invoke-static {v0, v1, v2, v5}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 361
    const/16 v1, 0xc0

    const-string/jumbo v2, "geotext.unicode"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 362
    const/16 v1, 0xc1

    const-string/jumbo v2, "geotext.rtftext"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 363
    const/16 v1, 0xc2

    const-string/jumbo v2, "geotext.alignmentoncurve"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 364
    const/16 v1, 0xc3

    const-string/jumbo v2, "geotext.defaultpointsize"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 365
    const/16 v1, 0xc4

    const-string/jumbo v2, "geotext.textspacing"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 366
    const/16 v1, 0xc5

    const-string/jumbo v2, "geotext.fontfamilyname"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 367
    const/16 v1, 0xf0

    const-string/jumbo v2, "geotext.reverseroworder"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 368
    const/16 v1, 0xf1

    const-string/jumbo v2, "geotext.hastexteffect"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 369
    const/16 v1, 0xf2

    const-string/jumbo v2, "geotext.rotatecharacters"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 370
    const/16 v1, 0xf3

    const-string/jumbo v2, "geotext.kerncharacters"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 371
    const/16 v1, 0xf4

    const-string/jumbo v2, "geotext.tightortrack"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 372
    const/16 v1, 0xf5

    const-string/jumbo v2, "geotext.stretchtofitshape"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 373
    const/16 v1, 0xf6

    const-string/jumbo v2, "geotext.charboundingbox"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 374
    const/16 v1, 0xf7

    const-string/jumbo v2, "geotext.scaletextonpath"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 375
    const/16 v1, 0xf8

    const-string/jumbo v2, "geotext.stretchcharheight"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 376
    const/16 v1, 0xf9

    const-string/jumbo v2, "geotext.nomeasurealongpath"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 377
    const/16 v1, 0xfa

    const-string/jumbo v2, "geotext.boldfont"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 378
    const/16 v1, 0xfb

    const-string/jumbo v2, "geotext.italicfont"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 379
    const/16 v1, 0xfc

    const-string/jumbo v2, "geotext.underlinefont"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 380
    const/16 v1, 0xfd

    const-string/jumbo v2, "geotext.shadowfont"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 381
    const/16 v1, 0xfe

    const-string/jumbo v2, "geotext.smallcapsfont"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 382
    const/16 v1, 0xff

    const-string/jumbo v2, "geotext.strikethroughfont"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 383
    const/16 v1, 0x100

    const-string/jumbo v2, "blip.cropfromtop"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 384
    const/16 v1, 0x101

    const-string/jumbo v2, "blip.cropfrombottom"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 385
    const/16 v1, 0x102

    const-string/jumbo v2, "blip.cropfromleft"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 386
    const/16 v1, 0x103

    const-string/jumbo v2, "blip.cropfromright"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 387
    const/16 v1, 0x104

    const-string/jumbo v2, "blip.bliptodisplay"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 388
    const/16 v1, 0x105

    const-string/jumbo v2, "blip.blipfilename"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 389
    const/16 v1, 0x106

    const-string/jumbo v2, "blip.blipflags"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 390
    const/16 v1, 0x107

    const-string/jumbo v2, "blip.transparentcolor"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 391
    const/16 v1, 0x108

    const-string/jumbo v2, "blip.contrastsetting"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 392
    const/16 v1, 0x109

    const-string/jumbo v2, "blip.brightnesssetting"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 393
    const/16 v1, 0x10a

    const-string/jumbo v2, "blip.gamma"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 394
    const/16 v1, 0x10b

    const-string/jumbo v2, "blip.pictureid"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 395
    const/16 v1, 0x10c

    const-string/jumbo v2, "blip.doublemod"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 396
    const/16 v1, 0x10d

    const-string/jumbo v2, "blip.picturefillmod"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 397
    const/16 v1, 0x10e

    const-string/jumbo v2, "blip.pictureline"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 398
    const/16 v1, 0x10f

    const-string/jumbo v2, "blip.printblip"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 399
    const/16 v1, 0x110

    const-string/jumbo v2, "blip.printblipfilename"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 400
    const/16 v1, 0x111

    const-string/jumbo v2, "blip.printflags"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 401
    const/16 v1, 0x13c

    const-string/jumbo v2, "blip.nohittestpicture"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 402
    const/16 v1, 0x13d

    const-string/jumbo v2, "blip.picturegray"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 403
    const/16 v1, 0x13e

    const-string/jumbo v2, "blip.picturebilevel"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 404
    const/16 v1, 0x13f

    const-string/jumbo v2, "blip.pictureactive"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 405
    const/16 v1, 0x140

    const-string/jumbo v2, "geometry.left"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 406
    const/16 v1, 0x141

    const-string/jumbo v2, "geometry.top"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 407
    const/16 v1, 0x142

    const-string/jumbo v2, "geometry.right"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 408
    const/16 v1, 0x143

    const-string/jumbo v2, "geometry.bottom"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 409
    const/16 v1, 0x144

    const-string/jumbo v2, "geometry.shapepath"

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 410
    const/16 v1, 0x145

    const-string/jumbo v2, "geometry.vertices"

    invoke-static {v0, v1, v2, v6}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 411
    const/16 v1, 0x146

    const-string/jumbo v2, "geometry.segmentinfo"

    invoke-static {v0, v1, v2, v6}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 412
    const/16 v1, 0x147

    const-string/jumbo v2, "geometry.adjustvalue"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 413
    const/16 v1, 0x148

    const-string/jumbo v2, "geometry.adjust2value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 414
    const/16 v1, 0x149

    const-string/jumbo v2, "geometry.adjust3value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 415
    const/16 v1, 0x14a

    const-string/jumbo v2, "geometry.adjust4value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 416
    const/16 v1, 0x14b

    const-string/jumbo v2, "geometry.adjust5value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 417
    const/16 v1, 0x14c

    const-string/jumbo v2, "geometry.adjust6value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 418
    const/16 v1, 0x14d

    const-string/jumbo v2, "geometry.adjust7value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 419
    const/16 v1, 0x14e

    const-string/jumbo v2, "geometry.adjust8value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 420
    const/16 v1, 0x14f

    const-string/jumbo v2, "geometry.adjust9value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 421
    const/16 v1, 0x150

    const-string/jumbo v2, "geometry.adjust10value"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 422
    const/16 v1, 0x17a

    const-string/jumbo v2, "geometry.shadowOK"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 423
    const/16 v1, 0x17b

    const-string/jumbo v2, "geometry.3dok"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 424
    const/16 v1, 0x17c

    const-string/jumbo v2, "geometry.lineok"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 425
    const/16 v1, 0x17d

    const-string/jumbo v2, "geometry.geotextok"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 426
    const/16 v1, 0x17e

    const-string/jumbo v2, "geometry.fillshadeshapeok"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 427
    const/16 v1, 0x17f

    const-string/jumbo v2, "geometry.fillok"

    invoke-static {v0, v1, v2, v5}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 428
    const/16 v1, 0x180

    const-string/jumbo v2, "fill.filltype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 429
    const/16 v1, 0x181

    const-string/jumbo v2, "fill.fillcolor"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 430
    const/16 v1, 0x182

    const-string/jumbo v2, "fill.fillopacity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 431
    const/16 v1, 0x183

    const-string/jumbo v2, "fill.fillbackcolor"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 432
    const/16 v1, 0x184

    const-string/jumbo v2, "fill.backopacity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 433
    const/16 v1, 0x185

    const-string/jumbo v2, "fill.crmod"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 434
    const/16 v1, 0x186

    const-string/jumbo v2, "fill.patterntexture"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 435
    const/16 v1, 0x187

    const-string/jumbo v2, "fill.blipfilename"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 436
    const/16 v1, 0x188

    const-string/jumbo v2, "fill.blipflags"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 437
    const/16 v1, 0x189

    const-string/jumbo v2, "fill.width"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 438
    const/16 v1, 0x18a

    const-string/jumbo v2, "fill.height"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 439
    const/16 v1, 0x18b

    const-string/jumbo v2, "fill.angle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 440
    const/16 v1, 0x18c

    const-string/jumbo v2, "fill.focus"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 441
    const/16 v1, 0x18d

    const-string/jumbo v2, "fill.toleft"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 442
    const/16 v1, 0x18e

    const-string/jumbo v2, "fill.totop"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 443
    const/16 v1, 0x18f

    const-string/jumbo v2, "fill.toright"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 444
    const/16 v1, 0x190

    const-string/jumbo v2, "fill.tobottom"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 445
    const/16 v1, 0x191

    const-string/jumbo v2, "fill.rectleft"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 446
    const/16 v1, 0x192

    const-string/jumbo v2, "fill.recttop"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 447
    const/16 v1, 0x193

    const-string/jumbo v2, "fill.rectright"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 448
    const/16 v1, 0x194

    const-string/jumbo v2, "fill.rectbottom"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 449
    const/16 v1, 0x195

    const-string/jumbo v2, "fill.dztype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 450
    const/16 v1, 0x196

    const-string/jumbo v2, "fill.shadepreset"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 451
    const/16 v1, 0x197

    const-string/jumbo v2, "fill.shadecolors"

    invoke-static {v0, v1, v2, v6}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 452
    const/16 v1, 0x198

    const-string/jumbo v2, "fill.originx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 453
    const/16 v1, 0x199

    const-string/jumbo v2, "fill.originy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 454
    const/16 v1, 0x19a

    const-string/jumbo v2, "fill.shapeoriginx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 455
    const/16 v1, 0x19b

    const-string/jumbo v2, "fill.shapeoriginy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 456
    const/16 v1, 0x19c

    const-string/jumbo v2, "fill.shadetype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 457
    const/16 v1, 0x1bb

    const-string/jumbo v2, "fill.filled"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 458
    const/16 v1, 0x1bc

    const-string/jumbo v2, "fill.hittestfill"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 459
    const/16 v1, 0x1bd

    const-string/jumbo v2, "fill.shape"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 460
    const/16 v1, 0x1be

    const-string/jumbo v2, "fill.userect"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 461
    const/16 v1, 0x1bf

    const-string/jumbo v2, "fill.nofillhittest"

    invoke-static {v0, v1, v2, v5}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 462
    const/16 v1, 0x1c0

    const-string/jumbo v2, "linestyle.color"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 463
    const/16 v1, 0x1c1

    const-string/jumbo v2, "linestyle.opacity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 464
    const/16 v1, 0x1c2

    const-string/jumbo v2, "linestyle.backcolor"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 465
    const/16 v1, 0x1c3

    const-string/jumbo v2, "linestyle.crmod"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 466
    const/16 v1, 0x1c4

    const-string/jumbo v2, "linestyle.linetype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 467
    const/16 v1, 0x1c5

    const-string/jumbo v2, "linestyle.fillblip"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 468
    const/16 v1, 0x1c6

    const-string/jumbo v2, "linestyle.fillblipname"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 469
    const/16 v1, 0x1c7

    const-string/jumbo v2, "linestyle.fillblipflags"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 470
    const/16 v1, 0x1c8

    const-string/jumbo v2, "linestyle.fillwidth"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 471
    const/16 v1, 0x1c9

    const-string/jumbo v2, "linestyle.fillheight"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 472
    const/16 v1, 0x1ca

    const-string/jumbo v2, "linestyle.filldztype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 473
    const/16 v1, 0x1cb

    const-string/jumbo v2, "linestyle.linewidth"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 474
    const/16 v1, 0x1cc

    const-string/jumbo v2, "linestyle.linemiterlimit"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 475
    const/16 v1, 0x1cd

    const-string/jumbo v2, "linestyle.linestyle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 476
    const/16 v1, 0x1ce

    const-string/jumbo v2, "linestyle.linedashing"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 477
    const/16 v1, 0x1cf

    const-string/jumbo v2, "linestyle.linedashstyle"

    invoke-static {v0, v1, v2, v6}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 478
    const/16 v1, 0x1d0

    const-string/jumbo v2, "linestyle.linestartarrowhead"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 479
    const/16 v1, 0x1d1

    const-string/jumbo v2, "linestyle.lineendarrowhead"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 480
    const/16 v1, 0x1d2

    const-string/jumbo v2, "linestyle.linestartarrowwidth"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 481
    const/16 v1, 0x1d3

    const-string/jumbo v2, "linestyle.lineestartarrowlength"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 482
    const/16 v1, 0x1d4

    const-string/jumbo v2, "linestyle.lineendarrowwidth"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 483
    const/16 v1, 0x1d5

    const-string/jumbo v2, "linestyle.lineendarrowlength"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 484
    const/16 v1, 0x1d6

    const-string/jumbo v2, "linestyle.linejoinstyle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 485
    const/16 v1, 0x1d7

    const-string/jumbo v2, "linestyle.lineendcapstyle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 486
    const/16 v1, 0x1fb

    const-string/jumbo v2, "linestyle.arrowheadsok"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 487
    const/16 v1, 0x1fc

    const-string/jumbo v2, "linestyle.anyline"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 488
    const/16 v1, 0x1fd

    const-string/jumbo v2, "linestyle.hitlinetest"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 489
    const/16 v1, 0x1fe

    const-string/jumbo v2, "linestyle.linefillshape"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 490
    const/16 v1, 0x1ff

    const-string/jumbo v2, "linestyle.nolinedrawdash"

    invoke-static {v0, v1, v2, v5}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 491
    const/16 v1, 0x200

    const-string/jumbo v2, "shadowstyle.type"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 492
    const/16 v1, 0x201

    const-string/jumbo v2, "shadowstyle.color"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 493
    const/16 v1, 0x202

    const-string/jumbo v2, "shadowstyle.highlight"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 494
    const/16 v1, 0x203

    const-string/jumbo v2, "shadowstyle.crmod"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 495
    const/16 v1, 0x204

    const-string/jumbo v2, "shadowstyle.opacity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 496
    const/16 v1, 0x205

    const-string/jumbo v2, "shadowstyle.offsetx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 497
    const/16 v1, 0x206

    const-string/jumbo v2, "shadowstyle.offsety"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 498
    const/16 v1, 0x207

    const-string/jumbo v2, "shadowstyle.secondoffsetx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 499
    const/16 v1, 0x208

    const-string/jumbo v2, "shadowstyle.secondoffsety"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 500
    const/16 v1, 0x209

    const-string/jumbo v2, "shadowstyle.scalextox"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 501
    const/16 v1, 0x20a

    const-string/jumbo v2, "shadowstyle.scaleytox"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 502
    const/16 v1, 0x20b

    const-string/jumbo v2, "shadowstyle.scalextoy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 503
    const/16 v1, 0x20c

    const-string/jumbo v2, "shadowstyle.scaleytoy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 504
    const/16 v1, 0x20d

    const-string/jumbo v2, "shadowstyle.perspectivex"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 505
    const/16 v1, 0x20e

    const-string/jumbo v2, "shadowstyle.perspectivey"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 506
    const/16 v1, 0x20f

    const-string/jumbo v2, "shadowstyle.weight"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 507
    const/16 v1, 0x210

    const-string/jumbo v2, "shadowstyle.originx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 508
    const/16 v1, 0x211

    const-string/jumbo v2, "shadowstyle.originy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 509
    const/16 v1, 0x23e

    const-string/jumbo v2, "shadowstyle.shadow"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 510
    const/16 v1, 0x23f

    const-string/jumbo v2, "shadowstyle.shadowobsured"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 511
    const/16 v1, 0x240

    const-string/jumbo v2, "perspective.type"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 512
    const/16 v1, 0x241

    const-string/jumbo v2, "perspective.offsetx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 513
    const/16 v1, 0x242

    const-string/jumbo v2, "perspective.offsety"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 514
    const/16 v1, 0x243

    const-string/jumbo v2, "perspective.scalextox"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 515
    const/16 v1, 0x244

    const-string/jumbo v2, "perspective.scaleytox"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 516
    const/16 v1, 0x245

    const-string/jumbo v2, "perspective.scalextoy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 517
    const/16 v1, 0x246

    const-string/jumbo v2, "perspective.scaleytoy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 518
    const/16 v1, 0x247

    const-string/jumbo v2, "perspective.perspectivex"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 519
    const/16 v1, 0x248

    const-string/jumbo v2, "perspective.perspectivey"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 520
    const/16 v1, 0x249

    const-string/jumbo v2, "perspective.weight"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 521
    const/16 v1, 0x24a

    const-string/jumbo v2, "perspective.originx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 522
    const/16 v1, 0x24b

    const-string/jumbo v2, "perspective.originy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 523
    const/16 v1, 0x27f

    const-string/jumbo v2, "perspective.perspectiveon"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 524
    const/16 v1, 0x280

    const-string/jumbo v2, "3d.specularamount"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 525
    const/16 v1, 0x295

    const-string/jumbo v2, "3d.diffuseamount"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 526
    const/16 v1, 0x296

    const-string/jumbo v2, "3d.shininess"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 527
    const/16 v1, 0x297

    const-string/jumbo v2, "3d.edgethickness"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 528
    const/16 v1, 0x298

    const-string/jumbo v2, "3d.extrudeforward"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 529
    const/16 v1, 0x299

    const-string/jumbo v2, "3d.extrudebackward"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 530
    const/16 v1, 0x29a

    const-string/jumbo v2, "3d.extrudeplane"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 531
    const/16 v1, 0x29b

    const-string/jumbo v2, "3d.extrusioncolor"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 532
    const/16 v1, 0x288

    const-string/jumbo v2, "3d.crmod"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 533
    const/16 v1, 0x2bc

    const-string/jumbo v2, "3d.3deffect"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 534
    const/16 v1, 0x2bd

    const-string/jumbo v2, "3d.metallic"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 535
    const/16 v1, 0x2be

    const-string/jumbo v2, "3d.useextrusioncolor"

    invoke-static {v0, v1, v2, v4}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 536
    const/16 v1, 0x2bf

    const-string/jumbo v2, "3d.lightface"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 537
    const/16 v1, 0x2c0

    const-string/jumbo v2, "3dstyle.yrotationangle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 538
    const/16 v1, 0x2c1

    const-string/jumbo v2, "3dstyle.xrotationangle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 539
    const/16 v1, 0x2c2

    const-string/jumbo v2, "3dstyle.rotationaxisx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 540
    const/16 v1, 0x2c3

    const-string/jumbo v2, "3dstyle.rotationaxisy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 541
    const/16 v1, 0x2c4

    const-string/jumbo v2, "3dstyle.rotationaxisz"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 542
    const/16 v1, 0x2c5

    const-string/jumbo v2, "3dstyle.rotationangle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 543
    const/16 v1, 0x2c6

    const-string/jumbo v2, "3dstyle.rotationcenterx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 544
    const/16 v1, 0x2c7

    const-string/jumbo v2, "3dstyle.rotationcentery"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 545
    const/16 v1, 0x2c8

    const-string/jumbo v2, "3dstyle.rotationcenterz"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 546
    const/16 v1, 0x2c9

    const-string/jumbo v2, "3dstyle.rendermode"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 547
    const/16 v1, 0x2ca

    const-string/jumbo v2, "3dstyle.tolerance"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 548
    const/16 v1, 0x2cb

    const-string/jumbo v2, "3dstyle.xviewpoint"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 549
    const/16 v1, 0x2cc

    const-string/jumbo v2, "3dstyle.yviewpoint"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 550
    const/16 v1, 0x2cd

    const-string/jumbo v2, "3dstyle.zviewpoint"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 551
    const/16 v1, 0x2ce

    const-string/jumbo v2, "3dstyle.originx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 552
    const/16 v1, 0x2cf

    const-string/jumbo v2, "3dstyle.originy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 553
    const/16 v1, 0x2d0

    const-string/jumbo v2, "3dstyle.skewangle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 554
    const/16 v1, 0x2d1

    const-string/jumbo v2, "3dstyle.skewamount"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 555
    const/16 v1, 0x2d2

    const-string/jumbo v2, "3dstyle.ambientintensity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 556
    const/16 v1, 0x2d3

    const-string/jumbo v2, "3dstyle.keyx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 557
    const/16 v1, 0x2d4

    const-string/jumbo v2, "3dstyle.keyy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 558
    const/16 v1, 0x2d5

    const-string/jumbo v2, "3dstyle.keyz"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 559
    const/16 v1, 0x2d6

    const-string/jumbo v2, "3dstyle.keyintensity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 560
    const/16 v1, 0x2d7

    const-string/jumbo v2, "3dstyle.fillx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 561
    const/16 v1, 0x2d8

    const-string/jumbo v2, "3dstyle.filly"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 562
    const/16 v1, 0x2d9

    const-string/jumbo v2, "3dstyle.fillz"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 563
    const/16 v1, 0x2da

    const-string/jumbo v2, "3dstyle.fillintensity"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 564
    const/16 v1, 0x2fb

    const-string/jumbo v2, "3dstyle.constrainrotation"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 565
    const/16 v1, 0x2fc

    const-string/jumbo v2, "3dstyle.rotationcenterauto"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 566
    const/16 v1, 0x2fd

    const-string/jumbo v2, "3dstyle.parallel"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 567
    const/16 v1, 0x2fe

    const-string/jumbo v2, "3dstyle.keyharsh"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 568
    const/16 v1, 0x2ff

    const-string/jumbo v2, "3dstyle.fillharsh"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 569
    const/16 v1, 0x301

    const-string/jumbo v2, "shape.master"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 570
    const/16 v1, 0x303

    const-string/jumbo v2, "shape.connectorstyle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 571
    const/16 v1, 0x304

    const-string/jumbo v2, "shape.blackandwhitesettings"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 572
    const/16 v1, 0x305

    const-string/jumbo v2, "shape.wmodepurebw"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 573
    const/16 v1, 0x306

    const-string/jumbo v2, "shape.wmodebw"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 574
    const/16 v1, 0x33a

    const-string/jumbo v2, "shape.oleicon"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 575
    const/16 v1, 0x33b

    const-string/jumbo v2, "shape.preferrelativeresize"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 576
    const/16 v1, 0x33c

    const-string/jumbo v2, "shape.lockshapetype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 577
    const/16 v1, 0x33e

    const-string/jumbo v2, "shape.deleteattachedobject"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 578
    const/16 v1, 0x33f

    const-string/jumbo v2, "shape.backgroundshape"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 579
    const/16 v1, 0x340

    const-string/jumbo v2, "callout.callouttype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 580
    const/16 v1, 0x341

    const-string/jumbo v2, "callout.xycalloutgap"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 581
    const/16 v1, 0x342

    const-string/jumbo v2, "callout.calloutangle"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 582
    const/16 v1, 0x343

    const-string/jumbo v2, "callout.calloutdroptype"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 583
    const/16 v1, 0x344

    const-string/jumbo v2, "callout.calloutdropspecified"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 584
    const/16 v1, 0x345

    const-string/jumbo v2, "callout.calloutlengthspecified"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 585
    const/16 v1, 0x379

    const-string/jumbo v2, "callout.iscallout"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 586
    const/16 v1, 0x37a

    const-string/jumbo v2, "callout.calloutaccentbar"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 587
    const/16 v1, 0x37b

    const-string/jumbo v2, "callout.callouttextborder"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 588
    const/16 v1, 0x37c

    const-string/jumbo v2, "callout.calloutminusx"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 589
    const/16 v1, 0x37d

    const-string/jumbo v2, "callout.calloutminusy"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 590
    const/16 v1, 0x37e

    const-string/jumbo v2, "callout.dropauto"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 591
    const/16 v1, 0x37f

    const-string/jumbo v2, "callout.lengthspecified"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 592
    const/16 v1, 0x380

    const-string/jumbo v2, "groupshape.shapename"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 593
    const/16 v1, 0x381

    const-string/jumbo v2, "groupshape.description"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 594
    const/16 v1, 0x382

    const-string/jumbo v2, "groupshape.hyperlink"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 595
    const/16 v1, 0x383

    const-string/jumbo v2, "groupshape.wrappolygonvertices"

    invoke-static {v0, v1, v2, v6}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 596
    const/16 v1, 0x384

    const-string/jumbo v2, "groupshape.wrapdistleft"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 597
    const/16 v1, 0x385

    const-string/jumbo v2, "groupshape.wrapdisttop"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 598
    const/16 v1, 0x386

    const-string/jumbo v2, "groupshape.wrapdistright"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 599
    const/16 v1, 0x387

    const-string/jumbo v2, "groupshape.wrapdistbottom"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 600
    const/16 v1, 0x388

    const-string/jumbo v2, "groupshape.regroupid"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 601
    const/16 v1, 0x38a

    const-string/jumbo v2, "unused906"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 602
    const/16 v1, 0x38d

    const-string/jumbo v2, "groupshape.wzTooltip"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 603
    const/16 v1, 0x38e

    const-string/jumbo v2, "groupshape.wzScript"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 604
    const/16 v1, 0x38f

    const-string/jumbo v2, "groupshape.posh"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 605
    const/16 v1, 0x390

    const-string/jumbo v2, "groupshape.posrelh"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 606
    const/16 v1, 0x391

    const-string/jumbo v2, "groupshape.posv"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 607
    const/16 v1, 0x392

    const-string/jumbo v2, "groupshape.posrelv"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 608
    const/16 v1, 0x393

    const-string/jumbo v2, "groupshape.pctHR"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 609
    const/16 v1, 0x394

    const-string/jumbo v2, "groupshape.alignHR"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 610
    const/16 v1, 0x395

    const-string/jumbo v2, "groupshape.dxHeightHR"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 611
    const/16 v1, 0x396

    const-string/jumbo v2, "groupshape.dxWidthHR"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 612
    const/16 v1, 0x397

    const-string/jumbo v2, "groupshape.wzScriptExtAttr"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 613
    const/16 v1, 0x398

    const-string/jumbo v2, "groupshape.scriptLang"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 614
    const/16 v1, 0x39b

    const-string/jumbo v2, "groupshape.borderTopColor"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 615
    const/16 v1, 0x39c

    const-string/jumbo v2, "groupshape.borderLeftColor"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 616
    const/16 v1, 0x39d

    const-string/jumbo v2, "groupshape.borderBottomColor"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 617
    const/16 v1, 0x39e

    const-string/jumbo v2, "groupshape.borderRightColor"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 618
    const/16 v1, 0x39f

    const-string/jumbo v2, "groupshape.tableProperties"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 619
    const/16 v1, 0x3a0

    const-string/jumbo v2, "groupshape.tableRowProperties"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 620
    const/16 v1, 0x3a5

    const-string/jumbo v2, "groupshape.wzWebBot"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 621
    const/16 v1, 0x3a9

    const-string/jumbo v2, "groupshape.metroBlob"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 622
    const/16 v1, 0x3aa

    const-string/jumbo v2, "groupshape.dhgt"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 623
    const-string/jumbo v1, "groupshape.GroupShapeBooleanProperties"

    invoke-static {v0, v7, v1}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 625
    const/16 v1, 0x3b9

    const-string/jumbo v2, "groupshape.editedwrap"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 626
    const/16 v1, 0x3ba

    const-string/jumbo v2, "groupshape.behinddocument"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 627
    const/16 v1, 0x3bb

    const-string/jumbo v2, "groupshape.ondblclicknotify"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 628
    const/16 v1, 0x3bc

    const-string/jumbo v2, "groupshape.isbutton"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 629
    const/16 v1, 0x3bd

    const-string/jumbo v2, "groupshape.1dadjustment"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 630
    const/16 v1, 0x3be

    const-string/jumbo v2, "groupshape.hidden"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 631
    const-string/jumbo v1, "groupshape.print"

    invoke-static {v0, v7, v1, v5}, Lorg/apache/poi/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 632
    return-object v0
.end method
