.class public final Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;
.super Ljava/lang/Object;
.source "Biff8DecryptingStream.java"

# interfaces
.implements Lorg/apache/poi/hssf/record/BiffHeaderInput;
.implements Lorg/apache/poi/util/LittleEndianInput;


# instance fields
.field private final _le:Lorg/apache/poi/util/LittleEndianInput;

.field private final _rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;ILorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "initialOffset"    # I
    .param p3, "key"    # Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    invoke-direct {v0, p2, p3}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;-><init>(ILorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    .line 38
    instance-of v0, p1, Lorg/apache/poi/util/LittleEndianInput;

    if-eqz v0, :cond_0

    .line 40
    check-cast p1, Lorg/apache/poi/util/LittleEndianInput;

    .end local p1    # "in":Ljava/io/InputStream;
    iput-object p1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    .line 45
    :goto_0
    return-void

    .line 43
    .restart local p1    # "in":Ljava/io/InputStream;
    :cond_0
    new-instance v0, Lorg/apache/poi/util/LittleEndianInputStream;

    invoke-direct {v0, p1}, Lorg/apache/poi/util/LittleEndianInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/poi/util/LittleEndianInput;->available()I

    move-result v0

    return v0
.end method

.method public readByte()B
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readUByte()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xorByte(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public readDataSize()I
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    .line 66
    .local v0, "dataSize":I
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->skipTwoBytes()V

    .line 67
    return v0
.end method

.method public readDouble()D
    .locals 6

    .prologue
    .line 71
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->readLong()J

    move-result-wide v2

    .line 72
    .local v2, "valueLongBits":J
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 73
    .local v0, "result":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 74
    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "Did not expect to read NaN"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 76
    :cond_0
    return-wide v0
.end method

.method public readFully([B)V
    .locals 2
    .param p1, "buf"    # [B

    .prologue
    .line 80
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->readFully([BII)V

    .line 81
    return-void
.end method

.method public readFully([BII)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/poi/util/LittleEndianInput;->readFully([BII)V

    .line 85
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xor([BII)V

    .line 86
    return-void
.end method

.method public readInt()I
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xorInt(I)I

    move-result v0

    return v0
.end method

.method public readLong()J
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readLong()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xorLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public readRecordSID()I
    .locals 2

    .prologue
    .line 55
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    .line 56
    .local v0, "sid":I
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->skipTwoBytes()V

    .line 57
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->startRecord(I)V

    .line 58
    return v0
.end method

.method public readShort()S
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xorShort(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public readUByte()I
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readUByte()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xorByte(I)I

    move-result v0

    return v0
.end method

.method public readUShort()I
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lorg/apache/poi/hssf/record/crypto/Biff8RC4;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;->_le:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->xorShort(I)I

    move-result v0

    return v0
.end method
