.class public final Lorg/apache/poi/hssf/util/HSSFColor$ORCHID;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ORCHID"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "6666:0:6666"

.field public static final index:S = 0x1cs

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x66

    .line 1558
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 1559
    aput-short v2, v0, v1

    const/4 v1, 0x2

    aput-short v2, v0, v1

    .line 1557
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$ORCHID;->triplet:[S

    .line 1561
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1553
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1575
    const-string/jumbo v0, "6666:0:6666"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1565
    const/16 v0, 0x1c

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1570
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$ORCHID;->triplet:[S

    return-object v0
.end method
