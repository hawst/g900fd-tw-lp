.class public Lorg/apache/poi/hssf/util/Region;
.super Lorg/apache/poi/ss/util/Region;
.source "Region.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/poi/ss/util/Region;-><init>()V

    .line 38
    return-void
.end method

.method public constructor <init>(ISIS)V
    .locals 0
    .param p1, "rowFrom"    # I
    .param p2, "colFrom"    # S
    .param p3, "rowTo"    # I
    .param p4, "colTo"    # S

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/ss/util/Region;-><init>(ISIS)V

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "ref"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/poi/ss/util/Region;-><init>(Ljava/lang/String;)V

    .line 47
    return-void
.end method
