.class public final Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;
.super Ljava/lang/Object;
.source "HSSFConditionalFormattingRule.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/ConditionalFormattingRule;


# static fields
.field private static final CELL_COMPARISON:B = 0x1t


# instance fields
.field private final cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

.field private final workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;


# direct methods
.method constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/record/CFRuleRecord;)V
    .locals 2
    .param p1, "pWorkbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "pRuleRecord"    # Lorg/apache/poi/hssf/record/CFRuleRecord;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    if-nez p1, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "pWorkbook must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    if-nez p2, :cond_1

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "pRuleRecord must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_1
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .line 52
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    .line 53
    return-void
.end method

.method private getBorderFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;
    .locals 3
    .param p1, "create"    # Z

    .prologue
    .line 99
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getBorderFormatting()Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    move-result-object v0

    .line 100
    .local v0, "borderFormatting":Lorg/apache/poi/hssf/record/cf/BorderFormatting;
    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setBorderFormatting(Lorg/apache/poi/hssf/record/cf/BorderFormatting;)V

    .line 103
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;-><init>(Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    .line 113
    :goto_0
    return-object v1

    .line 105
    :cond_0
    if-eqz p1, :cond_1

    .line 107
    new-instance v0, Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .end local v0    # "borderFormatting":Lorg/apache/poi/hssf/record/cf/BorderFormatting;
    invoke-direct {v0}, Lorg/apache/poi/hssf/record/cf/BorderFormatting;-><init>()V

    .line 108
    .restart local v0    # "borderFormatting":Lorg/apache/poi/hssf/record/cf/BorderFormatting;
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setBorderFormatting(Lorg/apache/poi/hssf/record/cf/BorderFormatting;)V

    .line 109
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;-><init>(Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    goto :goto_0

    .line 113
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getFontFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;
    .locals 3
    .param p1, "create"    # Z

    .prologue
    .line 62
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getFontFormatting()Lorg/apache/poi/hssf/record/cf/FontFormatting;

    move-result-object v0

    .line 63
    .local v0, "fontFormatting":Lorg/apache/poi/hssf/record/cf/FontFormatting;
    if-eqz v0, :cond_0

    .line 65
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setFontFormatting(Lorg/apache/poi/hssf/record/cf/FontFormatting;)V

    .line 66
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;-><init>(Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    .line 76
    :goto_0
    return-object v1

    .line 68
    :cond_0
    if-eqz p1, :cond_1

    .line 70
    new-instance v0, Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .end local v0    # "fontFormatting":Lorg/apache/poi/hssf/record/cf/FontFormatting;
    invoke-direct {v0}, Lorg/apache/poi/hssf/record/cf/FontFormatting;-><init>()V

    .line 71
    .restart local v0    # "fontFormatting":Lorg/apache/poi/hssf/record/cf/FontFormatting;
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setFontFormatting(Lorg/apache/poi/hssf/record/cf/FontFormatting;)V

    .line 72
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;-><init>(Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    goto :goto_0

    .line 76
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getPatternFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;
    .locals 3
    .param p1, "create"    # Z

    .prologue
    .line 135
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getPatternFormatting()Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    move-result-object v0

    .line 136
    .local v0, "patternFormatting":Lorg/apache/poi/hssf/record/cf/PatternFormatting;
    if-eqz v0, :cond_0

    .line 138
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setPatternFormatting(Lorg/apache/poi/hssf/record/cf/PatternFormatting;)V

    .line 139
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;-><init>(Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    .line 149
    :goto_0
    return-object v1

    .line 141
    :cond_0
    if-eqz p1, :cond_1

    .line 143
    new-instance v0, Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .end local v0    # "patternFormatting":Lorg/apache/poi/hssf/record/cf/PatternFormatting;
    invoke-direct {v0}, Lorg/apache/poi/hssf/record/cf/PatternFormatting;-><init>()V

    .line 144
    .restart local v0    # "patternFormatting":Lorg/apache/poi/hssf/record/cf/PatternFormatting;
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setPatternFormatting(Lorg/apache/poi/hssf/record/cf/PatternFormatting;)V

    .line 145
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;-><init>(Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    goto :goto_0

    .line 149
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private toFormulaString([Lorg/apache/poi/ss/formula/ptg/Ptg;)Ljava/lang/String;
    .locals 1
    .param p1, "parsedExpression"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 206
    if-nez p1, :cond_0

    .line 207
    const/4 v0, 0x0

    .line 209
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-static {v0, p1}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->toFormulaString(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public createBorderFormatting()Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getBorderFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createBorderFormatting()Lorg/apache/poi/ss/usermodel/BorderFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->createBorderFormatting()Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method public createFontFormatting()Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getFontFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFontFormatting()Lorg/apache/poi/ss/usermodel/FontFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->createFontFormatting()Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public createPatternFormatting()Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getPatternFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createPatternFormatting()Lorg/apache/poi/ss/usermodel/PatternFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->createPatternFormatting()Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

.method public getBorderFormatting()Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getBorderFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBorderFormatting()Lorg/apache/poi/ss/usermodel/BorderFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getBorderFormatting()Lorg/apache/poi/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method getCfRuleRecord()Lorg/apache/poi/hssf/record/CFRuleRecord;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    return-object v0
.end method

.method public getComparisonOperation()B
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getComparisonOperation()B

    move-result v0

    return v0
.end method

.method public getConditionType()B
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getConditionType()B

    move-result v0

    return v0
.end method

.method public getFontFormatting()Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getFontFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getFontFormatting()Lorg/apache/poi/ss/usermodel/FontFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getFontFormatting()Lorg/apache/poi/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public getFormula1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getParsedExpression1()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->toFormulaString([Lorg/apache/poi/ss/formula/ptg/Ptg;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormula2()Ljava/lang/String;
    .locals 3

    .prologue
    .line 191
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getConditionType()B

    move-result v1

    .line 192
    .local v1, "conditionType":B
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 193
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getComparisonOperation()B

    move-result v0

    .line 194
    .local v0, "comparisonOperation":B
    packed-switch v0, :pswitch_data_0

    .line 201
    .end local v0    # "comparisonOperation":B
    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 198
    .restart local v0    # "comparisonOperation":B
    :pswitch_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lorg/apache/poi/hssf/record/CFRuleRecord;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getParsedExpression2()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->toFormulaString([Lorg/apache/poi/ss/formula/ptg/Ptg;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getPatternFormatting()Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getPatternFormatting(Z)Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPatternFormatting()Lorg/apache/poi/ss/usermodel/PatternFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFConditionalFormattingRule;->getPatternFormatting()Lorg/apache/poi/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method
