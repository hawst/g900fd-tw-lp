.class public final Lorg/apache/poi/hssf/util/HSSFColor$GREEN;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GREEN"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:8080:0"

.field public static final index:S = 0x11s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 564
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x1

    .line 565
    const/16 v2, 0x80

    aput-short v2, v0, v1

    .line 563
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$GREEN;->triplet:[S

    .line 567
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 581
    const-string/jumbo v0, "0:8080:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 571
    const/16 v0, 0x11

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 576
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$GREEN;->triplet:[S

    return-object v0
.end method
