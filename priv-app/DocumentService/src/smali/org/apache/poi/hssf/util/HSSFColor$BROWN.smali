.class public final Lorg/apache/poi/hssf/util/HSSFColor$BROWN;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BROWN"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "9999:3333:0"

.field public static final index:S = 0x3cs

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 252
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 253
    const/16 v2, 0x99

    aput-short v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x33

    aput-short v2, v0, v1

    .line 251
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BROWN;->triplet:[S

    .line 255
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    const-string/jumbo v0, "9999:3333:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 259
    const/16 v0, 0x3c

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 264
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BROWN;->triplet:[S

    return-object v0
.end method
