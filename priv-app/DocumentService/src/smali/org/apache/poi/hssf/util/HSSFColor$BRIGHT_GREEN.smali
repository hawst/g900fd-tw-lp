.class public final Lorg/apache/poi/hssf/util/HSSFColor$BRIGHT_GREEN;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BRIGHT_GREEN"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:FFFF:0"

.field public static final index:S = 0xbs

.field public static final index2:S = 0x23s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1066
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x1

    .line 1067
    const/16 v2, 0xff

    aput-short v2, v0, v1

    .line 1065
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BRIGHT_GREEN;->triplet:[S

    .line 1069
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1060
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1078
    const-string/jumbo v0, "0:FFFF:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1073
    const/16 v0, 0xb

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1083
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BRIGHT_GREEN;->triplet:[S

    return-object v0
.end method
