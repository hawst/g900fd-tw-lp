.class public Lorg/apache/poi/hssf/usermodel/HSSFDataValidationHelper;
.super Ljava/lang/Object;
.source "HSSFDataValidationHelper.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/DataValidationHelper;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V
    .locals 0
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public createCustomConstraint(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "formula"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createCustomFormulaConstraint(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createDateConstraint(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "operatorType"    # I
    .param p2, "formula1"    # Ljava/lang/String;
    .param p3, "formula2"    # Ljava/lang/String;
    .param p4, "dateFormat"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p1, p2, p3, p4}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createDateConstraint(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createDecimalConstraint(ILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "operatorType"    # I
    .param p2, "formula1"    # Ljava/lang/String;
    .param p3, "formula2"    # Ljava/lang/String;

    .prologue
    .line 91
    const/4 v0, 0x2

    invoke-static {v0, p1, p2, p3}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createExplicitListConstraint([Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "listOfValues"    # [Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createExplicitListConstraint([Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createFormulaListConstraint(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "listFormula"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createFormulaListConstraint(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createIntegerConstraint(ILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "operatorType"    # I
    .param p2, "formula1"    # Ljava/lang/String;
    .param p3, "formula2"    # Ljava/lang/String;

    .prologue
    .line 80
    const/4 v0, 0x1

    invoke-static {v0, p1, p2, p3}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "validationType"    # I
    .param p2, "operatorType"    # I
    .param p3, "formula1"    # Ljava/lang/String;
    .param p4, "formula2"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-static {p1, p2, p3, p4}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createTextLengthConstraint(ILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "operatorType"    # I
    .param p2, "formula1"    # Ljava/lang/String;
    .param p3, "formula2"    # Ljava/lang/String;

    .prologue
    .line 102
    const/4 v0, 0x6

    invoke-static {v0, p1, p2, p3}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createTimeConstraint(ILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .locals 1
    .param p1, "operatorType"    # I
    .param p2, "formula1"    # Ljava/lang/String;
    .param p3, "formula2"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-static {p1, p2, p3}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createTimeConstraint(ILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move-result-object v0

    return-object v0
.end method

.method public createValidation(Lorg/apache/poi/ss/usermodel/DataValidationConstraint;Lorg/apache/poi/ss/util/CellRangeAddressList;)Lorg/apache/poi/ss/usermodel/DataValidation;
    .locals 1
    .param p1, "constraint"    # Lorg/apache/poi/ss/usermodel/DataValidationConstraint;
    .param p2, "cellRangeAddressList"    # Lorg/apache/poi/ss/util/CellRangeAddressList;

    .prologue
    .line 131
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFDataValidation;

    invoke-direct {v0, p2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFDataValidation;-><init>(Lorg/apache/poi/ss/util/CellRangeAddressList;Lorg/apache/poi/ss/usermodel/DataValidationConstraint;)V

    return-object v0
.end method
