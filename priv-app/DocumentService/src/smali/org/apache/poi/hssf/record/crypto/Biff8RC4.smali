.class final Lorg/apache/poi/hssf/record/crypto/Biff8RC4;
.super Ljava/lang/Object;
.source "Biff8RC4.java"


# static fields
.field private static final RC4_REKEYING_INTERVAL:I = 0x400


# instance fields
.field private _currentKeyIndex:I

.field private final _key:Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

.field private _nextRC4BlockStart:I

.field private _rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

.field private _shouldSkipEncryptionOnCurrentRecord:Z

.field private _streamPos:I


# direct methods
.method public constructor <init>(ILorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;)V
    .locals 5
    .param p1, "initialOffset"    # I
    .param p2, "key"    # Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    .prologue
    const/16 v4, 0x400

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    if-lt p1, v4, :cond_0

    .line 49
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "initialOffset ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 50
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " not supported yet"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 52
    :cond_0
    iput-object p2, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_key:Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    .line 53
    iput v2, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    .line 54
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->rekeyForNextBlock()V

    .line 55
    iput p1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    .line 56
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-gtz v0, :cond_1

    .line 59
    iput-boolean v2, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_shouldSkipEncryptionOnCurrentRecord:Z

    .line 60
    return-void

    .line 57
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/crypto/RC4;->output()B

    .line 56
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private getNextRC4Byte()I
    .locals 3

    .prologue
    .line 69
    iget v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    iget v2, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_nextRC4BlockStart:I

    if-lt v1, v2, :cond_0

    .line 70
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->rekeyForNextBlock()V

    .line 72
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/crypto/RC4;->output()B

    move-result v0

    .line 73
    .local v0, "mask":B
    iget v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    .line 74
    iget-boolean v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_shouldSkipEncryptionOnCurrentRecord:Z

    if-eqz v1, :cond_1

    .line 75
    const/4 v1, 0x0

    .line 77
    :goto_0
    return v1

    :cond_1
    and-int/lit16 v1, v0, 0xff

    goto :goto_0
.end method

.method private static isNeverEncryptedRecord(I)Z
    .locals 1
    .param p0, "sid"    # I

    .prologue
    .line 90
    sparse-switch p0, :sswitch_data_0

    .line 108
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 106
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x2f -> :sswitch_0
        0xe1 -> :sswitch_0
        0x809 -> :sswitch_0
    .end sparse-switch
.end method

.method private rekeyForNextBlock()V
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    div-int/lit16 v0, v0, 0x400

    iput v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_currentKeyIndex:I

    .line 64
    iget-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_key:Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    iget v1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_currentKeyIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->createRC4(I)Lorg/apache/poi/hssf/record/crypto/RC4;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    .line 65
    iget v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_currentKeyIndex:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit16 v0, v0, 0x400

    iput v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_nextRC4BlockStart:I

    .line 66
    return-void
.end method


# virtual methods
.method public skipTwoBytes()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    .line 117
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    .line 118
    return-void
.end method

.method public startRecord(I)V
    .locals 1
    .param p1, "currentSid"    # I

    .prologue
    .line 81
    invoke-static {p1}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->isNeverEncryptedRecord(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_shouldSkipEncryptionOnCurrentRecord:Z

    .line 82
    return-void
.end method

.method public xor([BII)V
    .locals 6
    .param p1, "buf"    # [B
    .param p2, "pOffset"    # I
    .param p3, "pLen"    # I

    .prologue
    const/16 v5, 0x400

    .line 122
    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_nextRC4BlockStart:I

    iget v4, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    sub-int v1, v3, v4

    .line 123
    .local v1, "nLeftInBlock":I
    if-gt p3, v1, :cond_0

    .line 125
    iget-object v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-virtual {v3, p1, p2, p3}, Lorg/apache/poi/hssf/record/crypto/RC4;->encrypt([BII)V

    .line 126
    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    .line 154
    :goto_0
    return-void

    .line 130
    :cond_0
    move v2, p2

    .line 131
    .local v2, "offset":I
    move v0, p3

    .line 134
    .local v0, "len":I
    if-le v0, v1, :cond_2

    .line 135
    if-lez v1, :cond_1

    .line 136
    iget-object v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-virtual {v3, p1, v2, v1}, Lorg/apache/poi/hssf/record/crypto/RC4;->encrypt([BII)V

    .line 137
    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    .line 138
    add-int/2addr v2, v1

    .line 139
    sub-int/2addr v0, v1

    .line 141
    :cond_1
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->rekeyForNextBlock()V

    .line 144
    :cond_2
    :goto_1
    if-gt v0, v5, :cond_3

    .line 152
    iget-object v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-virtual {v3, p1, v2, v0}, Lorg/apache/poi/hssf/record/crypto/RC4;->encrypt([BII)V

    .line 153
    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    add-int/2addr v3, v0

    iput v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    goto :goto_0

    .line 145
    :cond_3
    iget-object v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_rc4:Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-virtual {v3, p1, v2, v5}, Lorg/apache/poi/hssf/record/crypto/RC4;->encrypt([BII)V

    .line 146
    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    add-int/lit16 v3, v3, 0x400

    iput v3, p0, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->_streamPos:I

    .line 147
    add-int/lit16 v2, v2, 0x400

    .line 148
    add-int/lit16 v0, v0, -0x400

    .line 149
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->rekeyForNextBlock()V

    goto :goto_1
.end method

.method public xorByte(I)I
    .locals 2
    .param p1, "rawVal"    # I

    .prologue
    .line 157
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v0

    .line 158
    .local v0, "mask":I
    xor-int v1, p1, v0

    int-to-byte v1, v1

    return v1
.end method

.method public xorInt(I)I
    .locals 7
    .param p1, "rawVal"    # I

    .prologue
    .line 169
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v0

    .line 170
    .local v0, "b0":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v1

    .line 171
    .local v1, "b1":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v2

    .line 172
    .local v2, "b2":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v3

    .line 173
    .local v3, "b3":I
    shl-int/lit8 v5, v3, 0x18

    shl-int/lit8 v6, v2, 0x10

    add-int/2addr v5, v6

    shl-int/lit8 v6, v1, 0x8

    add-int/2addr v5, v6

    shl-int/lit8 v6, v0, 0x0

    add-int v4, v5, v6

    .line 174
    .local v4, "mask":I
    xor-int v5, p1, v4

    return v5
.end method

.method public xorLong(J)J
    .locals 15
    .param p1, "rawVal"    # J

    .prologue
    .line 178
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v0

    .line 179
    .local v0, "b0":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v1

    .line 180
    .local v1, "b1":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v2

    .line 181
    .local v2, "b2":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v3

    .line 182
    .local v3, "b3":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v4

    .line 183
    .local v4, "b4":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v5

    .line 184
    .local v5, "b5":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v6

    .line 185
    .local v6, "b6":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v7

    .line 187
    .local v7, "b7":I
    int-to-long v10, v7

    const/16 v12, 0x38

    shl-long/2addr v10, v12

    .line 188
    int-to-long v12, v6

    const/16 v14, 0x30

    shl-long/2addr v12, v14

    .line 187
    add-long/2addr v10, v12

    .line 189
    int-to-long v12, v5

    const/16 v14, 0x28

    shl-long/2addr v12, v14

    .line 187
    add-long/2addr v10, v12

    .line 190
    int-to-long v12, v4

    const/16 v14, 0x20

    shl-long/2addr v12, v14

    .line 187
    add-long/2addr v10, v12

    .line 191
    int-to-long v12, v3

    const/16 v14, 0x18

    shl-long/2addr v12, v14

    .line 187
    add-long/2addr v10, v12

    .line 192
    shl-int/lit8 v12, v2, 0x10

    int-to-long v12, v12

    .line 187
    add-long/2addr v10, v12

    .line 193
    shl-int/lit8 v12, v1, 0x8

    int-to-long v12, v12

    .line 187
    add-long/2addr v10, v12

    .line 194
    shl-int/lit8 v12, v0, 0x0

    int-to-long v12, v12

    .line 187
    add-long v8, v10, v12

    .line 195
    .local v8, "mask":J
    xor-long v10, p1, v8

    return-wide v10
.end method

.method public xorShort(I)I
    .locals 5
    .param p1, "rawVal"    # I

    .prologue
    .line 162
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v0

    .line 163
    .local v0, "b0":I
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/crypto/Biff8RC4;->getNextRC4Byte()I

    move-result v1

    .line 164
    .local v1, "b1":I
    shl-int/lit8 v3, v1, 0x8

    shl-int/lit8 v4, v0, 0x0

    add-int v2, v3, v4

    .line 165
    .local v2, "mask":I
    xor-int v3, p1, v2

    return v3
.end method
