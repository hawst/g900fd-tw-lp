.class public Lorg/apache/poi/hssf/util/CellRangeAddressList;
.super Lorg/apache/poi/ss/util/CellRangeAddressList;
.source "CellRangeAddressList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/poi/ss/util/CellRangeAddressList;-><init>()V

    .line 43
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "firstRow"    # I
    .param p2, "lastRow"    # I
    .param p3, "firstCol"    # I
    .param p4, "lastCol"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/ss/util/CellRangeAddressList;-><init>(IIII)V

    .line 40
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/poi/ss/util/CellRangeAddressList;-><init>()V

    .line 50
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUShort()I

    move-result v1

    .line 52
    .local v1, "nItems":I
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 55
    return-void

    .line 53
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/util/CellRangeAddressList;->_list:Ljava/util/List;

    new-instance v3, Lorg/apache/poi/hssf/util/CellRangeAddress;

    invoke-direct {v3, p1}, Lorg/apache/poi/hssf/util/CellRangeAddress;-><init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
