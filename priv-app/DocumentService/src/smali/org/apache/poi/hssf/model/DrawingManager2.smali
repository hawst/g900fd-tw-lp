.class public Lorg/apache/poi/hssf/model/DrawingManager2;
.super Ljava/lang/Object;
.source "DrawingManager2.java"


# instance fields
.field dgg:Lorg/apache/poi/ddf/EscherDggRecord;

.field drawingGroups:Ljava/util/List;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ddf/EscherDggRecord;)V
    .locals 1
    .param p1, "dgg"    # Lorg/apache/poi/ddf/EscherDggRecord;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->drawingGroups:Ljava/util/List;

    .line 40
    iput-object p1, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    .line 41
    return-void
.end method


# virtual methods
.method public allocateShapeId(S)I
    .locals 2
    .param p1, "drawingGroupId"    # S

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/model/DrawingManager2;->getDrawingGroup(I)Lorg/apache/poi/ddf/EscherDgRecord;

    move-result-object v0

    .line 72
    .local v0, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hssf/model/DrawingManager2;->allocateShapeId(SLorg/apache/poi/ddf/EscherDgRecord;)I

    move-result v1

    return v1
.end method

.method public allocateShapeId(SLorg/apache/poi/ddf/EscherDgRecord;)I
    .locals 5
    .param p1, "drawingGroupId"    # S
    .param p2, "dg"    # Lorg/apache/poi/ddf/EscherDgRecord;

    .prologue
    .line 82
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    iget-object v4, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherDggRecord;->getNumShapesSaved()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherDggRecord;->setNumShapesSaved(I)V

    .line 85
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v3

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 101
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/ddf/EscherDggRecord;->addCluster(II)V

    .line 102
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v4}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 103
    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherDgRecord;->getNumShapes()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p2, v3}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 104
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v3

    array-length v3, v3

    mul-int/lit16 v2, v3, 0x400

    .line 105
    .local v2, "result":I
    invoke-virtual {p2, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 106
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 107
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherDggRecord;->setShapeIdMax(I)V

    .line 108
    :cond_0
    :goto_1
    return v2

    .line 87
    .end local v2    # "result":I
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v3

    aget-object v0, v3, v1

    .line 88
    .local v0, "c":Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    move-result v3

    if-ne v3, p1, :cond_2

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    move-result v3

    const/16 v4, 0x400

    if-eq v3, v4, :cond_2

    .line 90
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    move-result v3

    add-int/lit8 v4, v1, 0x1

    mul-int/lit16 v4, v4, 0x400

    add-int v2, v3, v4

    .line 91
    .restart local v2    # "result":I
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 92
    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherDgRecord;->getNumShapes()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p2, v3}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 93
    invoke-virtual {p2, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 94
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 95
    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherDggRecord;->setShapeIdMax(I)V

    goto :goto_1

    .line 85
    .end local v2    # "result":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public clearDrawingGroups()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->drawingGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 48
    return-void
.end method

.method public createDgRecord()Lorg/apache/poi/ddf/EscherDgRecord;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 52
    new-instance v0, Lorg/apache/poi/ddf/EscherDgRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherDgRecord;-><init>()V

    .line 53
    .local v0, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    const/16 v2, -0xff8

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setRecordId(S)V

    .line 54
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/DrawingManager2;->findNewDrawingGroupId()S

    move-result v1

    .line 55
    .local v1, "dgId":S
    shl-int/lit8 v2, v1, 0x4

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    .line 56
    invoke-virtual {v0, v3}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 57
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 58
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->drawingGroups:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v2, v1, v3}, Lorg/apache/poi/ddf/EscherDggRecord;->addCluster(II)V

    .line 60
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getDrawingsSaved()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 61
    return-object v0
.end method

.method drawingGroupExists(S)Z
    .locals 2
    .param p1, "dgId"    # S

    .prologue
    .line 130
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 135
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 132
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 133
    const/4 v1, 0x1

    goto :goto_1

    .line 130
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method findFreeSPIDBlock()I
    .locals 3

    .prologue
    .line 140
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v0

    .line 141
    .local v0, "max":I
    div-int/lit16 v2, v0, 0x400

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v1, v2, 0x400

    .line 142
    .local v1, "next":I
    return v1
.end method

.method public findNewDrawingGroupId()S
    .locals 2

    .prologue
    .line 117
    const/4 v0, 0x1

    .line 118
    .local v0, "dgId":S
    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/DrawingManager2;->drawingGroupExists(S)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    return v0

    .line 119
    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0
.end method

.method public getDgg()Lorg/apache/poi/ddf/EscherDggRecord;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    return-object v0
.end method

.method getDrawingGroup(I)Lorg/apache/poi/ddf/EscherDgRecord;
    .locals 2
    .param p1, "drawingGroupId"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->drawingGroups:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherDgRecord;

    return-object v0
.end method

.method public incrementDrawingsSaved()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/DrawingManager2;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord;->getDrawingsSaved()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 152
    return-void
.end method
