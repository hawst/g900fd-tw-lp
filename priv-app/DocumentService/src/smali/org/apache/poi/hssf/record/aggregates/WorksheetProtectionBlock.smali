.class public final Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;
.super Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;
.source "WorksheetProtectionBlock.java"


# instance fields
.field private _objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

.field private _passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

.field private _protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

.field private _scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 50
    return-void
.end method

.method private checkNotPresent(Lorg/apache/poi/hssf/record/Record;)V
    .locals 3
    .param p1, "rec"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    new-instance v0, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Duplicate PageSettingsBlock record (sid=0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    return-void
.end method

.method private static createObjectProtect()Lorg/apache/poi/hssf/record/ObjectProtectRecord;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/ObjectProtectRecord;-><init>()V

    .line 219
    .local v0, "retval":Lorg/apache/poi/hssf/record/ObjectProtectRecord;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ObjectProtectRecord;->setProtect(Z)V

    .line 220
    return-object v0
.end method

.method private static createPassword()Lorg/apache/poi/hssf/record/PasswordRecord;
    .locals 2

    .prologue
    .line 236
    new-instance v0, Lorg/apache/poi/hssf/record/PasswordRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/PasswordRecord;-><init>(I)V

    return-object v0
.end method

.method private static createScenarioProtect()Lorg/apache/poi/hssf/record/ScenarioProtectRecord;
    .locals 2

    .prologue
    .line 227
    new-instance v0, Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/ScenarioProtectRecord;-><init>()V

    .line 228
    .local v0, "retval":Lorg/apache/poi/hssf/record/ScenarioProtectRecord;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ScenarioProtectRecord;->setProtect(Z)V

    .line 229
    return-object v0
.end method

.method private getPassword()Lorg/apache/poi/hssf/record/PasswordRecord;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    if-nez v0, :cond_0

    .line 163
    invoke-static {}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->createPassword()Lorg/apache/poi/hssf/record/PasswordRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    .line 165
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    return-object v0
.end method

.method private getProtect()Lorg/apache/poi/hssf/record/ProtectRecord;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Lorg/apache/poi/hssf/record/ProtectRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/ProtectRecord;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    .line 154
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    return-object v0
.end method

.method public static isComponentRecord(I)Z
    .locals 1
    .param p0, "sid"    # I

    .prologue
    .line 57
    sparse-switch p0, :sswitch_data_0

    .line 64
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 62
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_0
        0x13 -> :sswitch_0
        0x63 -> :sswitch_0
        0xdd -> :sswitch_0
    .end sparse-switch
.end method

.method private readARecord(Lorg/apache/poi/hssf/model/RecordStream;)Z
    .locals 1
    .param p1, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;

    .prologue
    .line 68
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextSid()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    .line 70
    :sswitch_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->checkNotPresent(Lorg/apache/poi/hssf/record/Record;)V

    .line 71
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/ProtectRecord;

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    .line 89
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 74
    :sswitch_1
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->checkNotPresent(Lorg/apache/poi/hssf/record/Record;)V

    .line 75
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    goto :goto_1

    .line 78
    :sswitch_2
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->checkNotPresent(Lorg/apache/poi/hssf/record/Record;)V

    .line 79
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    goto :goto_1

    .line 82
    :sswitch_3
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->checkNotPresent(Lorg/apache/poi/hssf/record/Record;)V

    .line 83
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/PasswordRecord;

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    goto :goto_1

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_0
        0x13 -> :sswitch_3
        0x63 -> :sswitch_1
        0xdd -> :sswitch_2
    .end sparse-switch
.end method

.method private static visitIfPresent(Lorg/apache/poi/hssf/record/Record;Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 0
    .param p0, "r"    # Lorg/apache/poi/hssf/record/Record;
    .param p1, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    .line 109
    if-eqz p0, :cond_0

    .line 110
    invoke-interface {p1, p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 112
    :cond_0
    return-void
.end method


# virtual methods
.method public addRecords(Lorg/apache/poi/hssf/model/RecordStream;)V
    .locals 1
    .param p1, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;

    .prologue
    .line 140
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->readARecord(Lorg/apache/poi/hssf/model/RecordStream;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    return-void
.end method

.method public getHCenter()Lorg/apache/poi/hssf/record/ScenarioProtectRecord;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    return-object v0
.end method

.method public getPasswordHash()I
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/PasswordRecord;->getPassword()I

    move-result v0

    goto :goto_0
.end method

.method public getPasswordRecord()Lorg/apache/poi/hssf/record/PasswordRecord;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    return-object v0
.end method

.method public isObjectProtected()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ObjectProtectRecord;->getProtect()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScenarioProtected()Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ScenarioProtectRecord;->getProtect()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSheetProtected()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ProtectRecord;->getProtect()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public protectSheet(Ljava/lang/String;ZZ)V
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "shouldProtectObjects"    # Z
    .param p3, "shouldProtectScenarios"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 178
    if-nez p1, :cond_1

    .line 179
    iput-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    .line 180
    iput-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    .line 181
    iput-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    .line 182
    iput-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->getProtect()Lorg/apache/poi/hssf/record/ProtectRecord;

    move-result-object v1

    .line 187
    .local v1, "prec":Lorg/apache/poi/hssf/record/ProtectRecord;
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->getPassword()Lorg/apache/poi/hssf/record/PasswordRecord;

    move-result-object v0

    .line 188
    .local v0, "pass":Lorg/apache/poi/hssf/record/PasswordRecord;
    invoke-virtual {v1, v5}, Lorg/apache/poi/hssf/record/ProtectRecord;->setProtect(Z)V

    .line 189
    invoke-static {p1}, Lorg/apache/poi/hssf/record/PasswordRecord;->hashPassword(Ljava/lang/String;)S

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/PasswordRecord;->setPassword(I)V

    .line 190
    iget-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    if-nez v4, :cond_2

    if-eqz p2, :cond_2

    .line 191
    invoke-static {}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->createObjectProtect()Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    move-result-object v2

    .line 192
    .local v2, "rec":Lorg/apache/poi/hssf/record/ObjectProtectRecord;
    invoke-virtual {v2, v5}, Lorg/apache/poi/hssf/record/ObjectProtectRecord;->setProtect(Z)V

    .line 193
    iput-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    .line 195
    .end local v2    # "rec":Lorg/apache/poi/hssf/record/ObjectProtectRecord;
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    if-nez v4, :cond_0

    if-eqz p3, :cond_0

    .line 196
    invoke-static {}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->createScenarioProtect()Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    move-result-object v3

    .line 197
    .local v3, "srec":Lorg/apache/poi/hssf/record/ScenarioProtectRecord;
    invoke-virtual {v3, v5}, Lorg/apache/poi/hssf/record/ScenarioProtectRecord;->setProtect(Z)V

    .line 198
    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    goto :goto_0
.end method

.method public visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 1
    .param p1, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_protectRecord:Lorg/apache/poi/hssf/record/ProtectRecord;

    invoke-static {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->visitIfPresent(Lorg/apache/poi/hssf/record/Record;Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 103
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_objectProtectRecord:Lorg/apache/poi/hssf/record/ObjectProtectRecord;

    invoke-static {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->visitIfPresent(Lorg/apache/poi/hssf/record/Record;Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 104
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_scenarioProtectRecord:Lorg/apache/poi/hssf/record/ScenarioProtectRecord;

    invoke-static {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->visitIfPresent(Lorg/apache/poi/hssf/record/Record;Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 105
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->_passwordRecord:Lorg/apache/poi/hssf/record/PasswordRecord;

    invoke-static {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->visitIfPresent(Lorg/apache/poi/hssf/record/Record;Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 106
    return-void
.end method
