.class public final Lorg/apache/poi/hssf/record/WSBoolRecord;
.super Lorg/apache/poi/hssf/record/StandardRecord;
.source "WSBoolRecord.java"


# static fields
.field private static final alternateexpression:Lorg/apache/poi/util/BitField;

.field private static final alternateformula:Lorg/apache/poi/util/BitField;

.field private static final autobreaks:Lorg/apache/poi/util/BitField;

.field private static final dialog:Lorg/apache/poi/util/BitField;

.field private static final displayguts:Lorg/apache/poi/util/BitField;

.field private static final fittopage:Lorg/apache/poi/util/BitField;

.field private static final rowsumsbelow:Lorg/apache/poi/util/BitField;

.field private static final rowsumsright:Lorg/apache/poi/util/BitField;

.field public static final sid:S = 0x81s


# instance fields
.field private field_1_wsbool:B

.field private field_2_wsbool:B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x80

    const/16 v2, 0x40

    const/4 v1, 0x1

    .line 39
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->autobreaks:Lorg/apache/poi/util/BitField;

    .line 42
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->dialog:Lorg/apache/poi/util/BitField;

    .line 46
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->rowsumsbelow:Lorg/apache/poi/util/BitField;

    .line 47
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->rowsumsright:Lorg/apache/poi/util/BitField;

    .line 48
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->fittopage:Lorg/apache/poi/util/BitField;

    .line 51
    const/4 v0, 0x6

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->displayguts:Lorg/apache/poi/util/BitField;

    .line 54
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->alternateexpression:Lorg/apache/poi/util/BitField;

    .line 55
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->alternateformula:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 63
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readRemainder()[B

    move-result-object v0

    .line 65
    .local v0, "data":[B
    const/4 v1, 0x1

    aget-byte v1, v0, v1

    .line 64
    iput-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 67
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    .line 66
    iput-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 68
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 335
    new-instance v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;-><init>()V

    .line 336
    .local v0, "rec":Lorg/apache/poi/hssf/record/WSBoolRecord;
    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    iput-byte v1, v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 337
    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    iput-byte v1, v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 338
    return-object v0
.end method

.method public getAlternateExpression()Z
    .locals 2

    .prologue
    .line 277
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->alternateexpression:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getAlternateFormula()Z
    .locals 2

    .prologue
    .line 287
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->alternateformula:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getAutobreaks()Z
    .locals 2

    .prologue
    .line 203
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->autobreaks:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method protected getDataSize()I
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x2

    return v0
.end method

.method public getDialog()Z
    .locals 2

    .prologue
    .line 213
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->dialog:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getDisplayGuts()Z
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->displayguts:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getFitToPage()Z
    .locals 2

    .prologue
    .line 256
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->fittopage:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getRowSumsBelow()Z
    .locals 2

    .prologue
    .line 223
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->rowsumsbelow:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getRowSumsRight()Z
    .locals 2

    .prologue
    .line 233
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->rowsumsright:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 331
    const/16 v0, 0x81

    return v0
.end method

.method public getWSBool1()B
    .locals 1

    .prologue
    .line 191
    iget-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    return v0
.end method

.method public getWSBool2()B
    .locals 1

    .prologue
    .line 244
    iget-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    return v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 321
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getWSBool2()B

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 322
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getWSBool1()B

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 323
    return-void
.end method

.method public setAlternateExpression(Z)V
    .locals 2
    .param p1, "altexp"    # Z

    .prologue
    .line 168
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->alternateexpression:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 170
    return-void
.end method

.method public setAlternateFormula(Z)V
    .locals 2
    .param p1, "formula"    # Z

    .prologue
    .line 179
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->alternateformula:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 181
    return-void
.end method

.method public setAutobreaks(Z)V
    .locals 2
    .param p1, "ab"    # Z

    .prologue
    .line 94
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->autobreaks:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 95
    return-void
.end method

.method public setDialog(Z)V
    .locals 2
    .param p1, "isDialog"    # Z

    .prologue
    .line 104
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->dialog:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 105
    return-void
.end method

.method public setDisplayGuts(Z)V
    .locals 2
    .param p1, "guts"    # Z

    .prologue
    .line 158
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->displayguts:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 159
    return-void
.end method

.method public setFitToPage(Z)V
    .locals 2
    .param p1, "fit2page"    # Z

    .prologue
    .line 147
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->fittopage:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 148
    return-void
.end method

.method public setRowSumsBelow(Z)V
    .locals 2
    .param p1, "below"    # Z

    .prologue
    .line 114
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->rowsumsbelow:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 115
    return-void
.end method

.method public setRowSumsRight(Z)V
    .locals 2
    .param p1, "right"    # Z

    .prologue
    .line 124
    sget-object v0, Lorg/apache/poi/hssf/record/WSBoolRecord;->rowsumsright:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setByteBoolean(BZ)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 125
    return-void
.end method

.method public setWSBool1(B)V
    .locals 0
    .param p1, "bool1"    # B

    .prologue
    .line 82
    iput-byte p1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_1_wsbool:B

    .line 83
    return-void
.end method

.method public setWSBool2(B)V
    .locals 0
    .param p1, "bool2"    # B

    .prologue
    .line 135
    iput-byte p1, p0, Lorg/apache/poi/hssf/record/WSBoolRecord;->field_2_wsbool:B

    .line 136
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 295
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[WSBOOL]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 296
    const-string/jumbo v1, "    .wsbool1        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 297
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getWSBool1()B

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 298
    const-string/jumbo v1, "        .autobreaks = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getAutobreaks()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 299
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 300
    const-string/jumbo v1, "        .dialog     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getDialog()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 301
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    const-string/jumbo v1, "        .rowsumsbelw= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getRowSumsBelow()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 303
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 304
    const-string/jumbo v1, "        .rowsumsrigt= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getRowSumsRight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 305
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 306
    const-string/jumbo v1, "    .wsbool2        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 307
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getWSBool2()B

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 308
    const-string/jumbo v1, "        .fittopage  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getFitToPage()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 309
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 310
    const-string/jumbo v1, "        .displayguts= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getDisplayGuts()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 311
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 312
    const-string/jumbo v1, "        .alternateex= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 313
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getAlternateExpression()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 314
    const-string/jumbo v1, "        .alternatefo= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getAlternateFormula()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 315
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 316
    const-string/jumbo v1, "[/WSBOOL]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 317
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
