.class public final Lorg/apache/poi/hssf/util/HSSFColor$DARK_BLUE;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DARK_BLUE"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:0:8080"

.field public static final index:S = 0x12s

.field public static final index2:S = 0x20s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 377
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x2

    .line 378
    const/16 v2, 0x80

    aput-short v2, v0, v1

    .line 376
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$DARK_BLUE;->triplet:[S

    .line 380
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 394
    const-string/jumbo v0, "0:0:8080"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 384
    const/16 v0, 0x12

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 389
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$DARK_BLUE;->triplet:[S

    return-object v0
.end method
