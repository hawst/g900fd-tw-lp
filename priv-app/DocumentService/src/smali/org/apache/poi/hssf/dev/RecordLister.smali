.class public Lorg/apache/poi/hssf/dev/RecordLister;
.super Ljava/lang/Object;
.source "RecordLister.java"


# instance fields
.field file:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 182
    array-length v2, p0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    aget-object v2, p0, v4

    const-string/jumbo v3, "--help"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    :try_start_0
    new-instance v1, Lorg/apache/poi/hssf/dev/RecordLister;

    invoke-direct {v1}, Lorg/apache/poi/hssf/dev/RecordLister;-><init>()V

    .line 188
    .local v1, "viewer":Lorg/apache/poi/hssf/dev/RecordLister;
    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/dev/RecordLister;->setFile(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v1}, Lorg/apache/poi/hssf/dev/RecordLister;->run()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    .end local v1    # "viewer":Lorg/apache/poi/hssf/dev/RecordLister;
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v7, p0, Lorg/apache/poi/hssf/dev/RecordLister;->file:Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 58
    .local v3, "fin":Ljava/io/FileInputStream;
    new-instance v4, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v4, v3}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 59
    .local v4, "poifs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    const-string/jumbo v7, "Workbook"

    invoke-virtual {v4, v7}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v2

    .line 60
    .local v2, "din":Ljava/io/InputStream;
    new-instance v5, Lorg/apache/poi/hssf/record/RecordInputStream;

    invoke-direct {v5, v2}, Lorg/apache/poi/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;)V

    .line 62
    .local v5, "rinp":Lorg/apache/poi/hssf/record/RecordInputStream;
    :goto_0
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v7

    if-nez v7, :cond_0

    .line 87
    return-void

    .line 63
    :cond_0
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/RecordInputStream;->getNextSid()I

    move-result v6

    .line 64
    .local v6, "sid":I
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 67
    invoke-static {v6}, Lorg/apache/poi/hssf/record/RecordFactory;->getRecordClass(I)Ljava/lang/Class;

    move-result-object v0

    .line 81
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/hssf/record/Record;>;"
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/RecordInputStream;->readRemainder()[B

    move-result-object v1

    .line 82
    .local v1, "data":[B
    array-length v7, v1

    goto :goto_0
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lorg/apache/poi/hssf/dev/RecordLister;->file:Ljava/lang/String;

    .line 178
    return-void
.end method
