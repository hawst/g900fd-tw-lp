.class public final Lorg/apache/poi/hssf/util/HSSFColor$ROSE;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ROSE"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:9999:CCCC"

.field public static final index:S = 0x2ds

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1223
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1222
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$ROSE;->triplet:[S

    .line 1226
    return-void

    .line 1223
    nop

    :array_0
    .array-data 2
        0xffs
        0x99s
        0xccs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1218
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1240
    const-string/jumbo v0, "FFFF:9999:CCCC"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1230
    const/16 v0, 0x2d

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1235
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$ROSE;->triplet:[S

    return-object v0
.end method
