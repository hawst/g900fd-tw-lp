.class public final Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
.super Lorg/apache/poi/POIDocument;
.source "HSSFWorkbook.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/Workbook;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    }
.end annotation


# static fields
.field private static final COMMA_PATTERN:Ljava/util/regex/Pattern;

.field private static final DEBUG:I = 0x1

.field public static final INITIAL_CAPACITY:I = 0x3

.field private static final MAX_STYLES:I = 0xfbe

.field private static final VIEWFIRSTSHEET:Z = true

.field private static final WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

.field private static log:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected _sheets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFSheet;",
            ">;"
        }
    .end annotation
.end field

.field private _udfFinder:Lorg/apache/poi/ss/formula/udf/UDFFinder;

.field private fonts:Ljava/util/Hashtable;

.field private formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;

.field private missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

.field private names:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFName;",
            ">;"
        }
    .end annotation
.end field

.field private preserveNodes:Z

.field private workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const-string/jumbo v0, ","

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    .line 148
    const-class v0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    .line 200
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 201
    const-string/jumbo v2, "Workbook"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 202
    const-string/jumbo v2, "WORKBOOK"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 203
    const-string/jumbo v2, "BOOK"

    aput-object v2, v0, v1

    .line 200
    sput-object v0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 204
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/hssf/model/InternalWorkbook;)V

    .line 165
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 317
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Ljava/io/InputStream;Z)V

    .line 318
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 1
    .param p1, "s"    # Ljava/io/InputStream;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v0, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 337
    return-void
.end method

.method private constructor <init>(Lorg/apache/poi/hssf/model/InternalWorkbook;)V
    .locals 5
    .param p1, "book"    # Lorg/apache/poi/hssf/model/InternalWorkbook;

    .prologue
    const/4 v4, 0x3

    .line 168
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/POIDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 146
    sget-object v0, Lorg/apache/poi/hssf/usermodel/HSSFRow;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 154
    new-instance v0, Lorg/apache/poi/ss/formula/udf/IndexedUDFFinder;

    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/poi/ss/formula/udf/UDFFinder;

    const/4 v2, 0x0

    sget-object v3, Lorg/apache/poi/ss/formula/udf/UDFFinder;->DEFAULT:Lorg/apache/poi/ss/formula/udf/UDFFinder;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/apache/poi/ss/formula/udf/IndexedUDFFinder;-><init>([Lorg/apache/poi/ss/formula/udf/UDFFinder;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lorg/apache/poi/ss/formula/udf/UDFFinder;

    .line 169
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 172
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .param p3, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-direct {p0, p1, p3}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Z)V

    .line 251
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Z)V
    .locals 13
    .param p1, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lorg/apache/poi/POIDocument;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 146
    sget-object v9, Lorg/apache/poi/hssf/usermodel/HSSFRow;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    iput-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 154
    new-instance v9, Lorg/apache/poi/ss/formula/udf/IndexedUDFFinder;

    const/4 v10, 0x1

    new-array v10, v10, [Lorg/apache/poi/ss/formula/udf/UDFFinder;

    const/4 v11, 0x0

    sget-object v12, Lorg/apache/poi/ss/formula/udf/UDFFinder;->DEFAULT:Lorg/apache/poi/ss/formula/udf/UDFFinder;

    aput-object v12, v10, v11

    invoke-direct {v9, v10}, Lorg/apache/poi/ss/formula/udf/IndexedUDFFinder;-><init>([Lorg/apache/poi/ss/formula/udf/UDFFinder;)V

    iput-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lorg/apache/poi/ss/formula/udf/UDFFinder;

    .line 270
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbookDirEntryName(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/lang/String;

    move-result-object v8

    .line 272
    .local v8, "workbookName":Ljava/lang/String;
    iput-boolean p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->preserveNodes:Z

    .line 276
    if-nez p2, :cond_0

    .line 277
    const/4 v9, 0x0

    iput-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .line 280
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    const/4 v10, 0x3

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 281
    new-instance v9, Ljava/util/ArrayList;

    const/4 v10, 0x3

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 285
    invoke-virtual {p1, v8}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v7

    .line 287
    .local v7, "stream":Ljava/io/InputStream;
    invoke-static {v7}, Lorg/apache/poi/hssf/record/RecordFactory;->createRecords(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v4

    .line 289
    .local v4, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    invoke-static {v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWorkbook(Ljava/util/List;)Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v9

    iput-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    .line 290
    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-direct {p0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->setPropertiesFromWorkbook(Lorg/apache/poi/hssf/model/InternalWorkbook;)V

    .line 291
    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumRecords()I

    move-result v3

    .line 295
    .local v3, "recOffset":I
    invoke-direct {p0, v4, v3}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->convertLabelRecords(Ljava/util/List;I)V

    .line 296
    new-instance v5, Lorg/apache/poi/hssf/model/RecordStream;

    invoke-direct {v5, v4, v3}, Lorg/apache/poi/hssf/model/RecordStream;-><init>(Ljava/util/List;I)V

    .line 299
    .local v5, "rs":Lorg/apache/poi/hssf/model/RecordStream;
    invoke-static {v5}, Lorg/apache/poi/hssf/model/InternalSheet;->createSheet(Lorg/apache/poi/hssf/model/RecordStream;)Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v6

    .line 300
    .local v6, "sheet":Lorg/apache/poi/hssf/model/InternalSheet;
    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    new-instance v10, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    invoke-direct {v10, p0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/model/InternalSheet;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumNames()I

    move-result v9

    if-lt v0, v9, :cond_1

    .line 314
    return-void

    .line 310
    :cond_1
    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v9, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v2

    .line 311
    .local v2, "nameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFName;

    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v9, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameCommentRecord(Lorg/apache/poi/hssf/record/NameRecord;)Lorg/apache/poi/hssf/record/NameCommentRecord;

    move-result-object v9

    invoke-direct {v1, p0, v2, v9}, Lorg/apache/poi/hssf/usermodel/HSSFName;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/record/NameRecord;Lorg/apache/poi/hssf/record/NameCommentRecord;)V

    .line 312
    .local v1, "name":Lorg/apache/poi/hssf/usermodel/HSSFName;
    iget-object v9, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 176
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 194
    return-void
.end method

.method private convertLabelRecords(Ljava/util/List;I)V
    .locals 9
    .param p1, "records"    # Ljava/util/List;
    .param p2, "offset"    # I

    .prologue
    const/4 v8, 0x1

    .line 369
    sget-object v5, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v8}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 370
    sget-object v5, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v6, "convertLabelRecords called"

    invoke-virtual {v5, v8, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 371
    :cond_0
    move v0, p2

    .local v0, "k":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-lt v0, v5, :cond_2

    .line 391
    sget-object v5, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v8}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 392
    sget-object v5, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v6, "convertLabelRecords exit"

    invoke-virtual {v5, v8, v6}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 393
    :cond_1
    return-void

    .line 373
    :cond_2
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/Record;

    .line 375
    .local v3, "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v5

    const/16 v6, 0x204

    if-ne v5, v6, :cond_3

    move-object v2, v3

    .line 377
    check-cast v2, Lorg/apache/poi/hssf/record/LabelRecord;

    .line 379
    .local v2, "oldrec":Lorg/apache/poi/hssf/record/LabelRecord;
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 380
    new-instance v1, Lorg/apache/poi/hssf/record/LabelSSTRecord;

    invoke-direct {v1}, Lorg/apache/poi/hssf/record/LabelSSTRecord;-><init>()V

    .line 382
    .local v1, "newrec":Lorg/apache/poi/hssf/record/LabelSSTRecord;
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    new-instance v6, Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/LabelRecord;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lorg/apache/poi/hssf/model/InternalWorkbook;->addSSTString(Lorg/apache/poi/hssf/record/common/UnicodeString;)I

    move-result v4

    .line 384
    .local v4, "stringid":I
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/LabelRecord;->getRow()I

    move-result v5

    invoke-virtual {v1, v5}, Lorg/apache/poi/hssf/record/LabelSSTRecord;->setRow(I)V

    .line 385
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/LabelRecord;->getColumn()S

    move-result v5

    invoke-virtual {v1, v5}, Lorg/apache/poi/hssf/record/LabelSSTRecord;->setColumn(S)V

    .line 386
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/LabelRecord;->getXFIndex()S

    move-result v5

    invoke-virtual {v1, v5}, Lorg/apache/poi/hssf/record/LabelSSTRecord;->setXFIndex(S)V

    .line 387
    invoke-virtual {v1, v4}, Lorg/apache/poi/hssf/record/LabelSSTRecord;->setSSTIndex(I)V

    .line 388
    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 371
    .end local v1    # "newrec":Lorg/apache/poi/hssf/record/LabelSSTRecord;
    .end local v2    # "oldrec":Lorg/apache/poi/hssf/record/LabelRecord;
    .end local v4    # "stringid":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static create(Lorg/apache/poi/hssf/model/InternalWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .locals 1
    .param p0, "book"    # Lorg/apache/poi/hssf/model/InternalWorkbook;

    .prologue
    .line 157
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v0, p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/hssf/model/InternalWorkbook;)V

    return-object v0
.end method

.method private getAllEmbeddedObjects(Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;Ljava/util/List;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFObjectData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1756
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFObjectData;>;"
    invoke-interface {p1}, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;->getChildren()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1763
    return-void

    .line 1756
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFShape;

    .line 1757
    .local v0, "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    instance-of v2, v0, Lorg/apache/poi/hssf/usermodel/HSSFObjectData;

    if-eqz v2, :cond_2

    .line 1758
    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFObjectData;

    .end local v0    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1759
    .restart local v0    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :cond_2
    instance-of v2, v0, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;

    if-eqz v2, :cond_0

    .line 1760
    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;

    .end local v0    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getAllEmbeddedObjects(Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;Ljava/util/List;)V

    goto :goto_0
.end method

.method private getAllEmbeddedObjects(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Ljava/util/List;)V
    .locals 1
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hssf/usermodel/HSSFSheet;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFObjectData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1742
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFObjectData;>;"
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v0

    .line 1743
    .local v0, "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    if-nez v0, :cond_0

    .line 1747
    :goto_0
    return-void

    .line 1746
    :cond_0
    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getAllEmbeddedObjects(Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;Ljava/util/List;)V

    goto :goto_0
.end method

.method private getSheets()[Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 2

    .prologue
    .line 830
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v0, v1, [Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .line 831
    .local v0, "result":[Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 832
    return-object v0
.end method

.method private getUniqueSheetName(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "srcName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 723
    const/4 v5, 0x2

    .line 724
    .local v5, "uniqueIndex":I
    move-object v0, p1

    .line 725
    .local v0, "baseName":Ljava/lang/String;
    const/16 v7, 0x28

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 726
    .local v1, "bracketPos":I
    if-lez v1, :cond_0

    const-string/jumbo v7, ")"

    invoke-virtual {p1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 727
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const-string/jumbo v9, ")"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 729
    .local v4, "suffix":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 730
    add-int/lit8 v5, v5, 0x1

    .line 731
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 738
    .end local v4    # "suffix":Ljava/lang/String;
    :cond_0
    :goto_0
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "uniqueIndex":I
    .local v6, "uniqueIndex":I
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 740
    .local v2, "index":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x2

    const/16 v8, 0x1f

    if-ge v7, v8, :cond_1

    .line 741
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 747
    .local v3, "name":Ljava/lang/String;
    :goto_1
    iget-object v7, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v7, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_2

    .line 748
    return-object v3

    .line 743
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    rsub-int/lit8 v8, v8, 0x1f

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v0, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "name":Ljava/lang/String;
    goto :goto_1

    .line 732
    .end local v2    # "index":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    .end local v6    # "uniqueIndex":I
    .restart local v4    # "suffix":Ljava/lang/String;
    .restart local v5    # "uniqueIndex":I
    :catch_0
    move-exception v7

    goto :goto_0

    .end local v4    # "suffix":Ljava/lang/String;
    .end local v5    # "uniqueIndex":I
    .restart local v2    # "index":Ljava/lang/String;
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v6    # "uniqueIndex":I
    :cond_2
    move v5, v6

    .end local v6    # "uniqueIndex":I
    .restart local v5    # "uniqueIndex":I
    goto :goto_0
.end method

.method private static getWorkbookDirEntryName(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/lang/String;
    .locals 5
    .param p0, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .prologue
    .line 209
    sget-object v1, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 210
    .local v1, "potentialNames":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-lt v0, v3, :cond_0

    .line 222
    :try_start_0
    const-string/jumbo v3, "Book"

    invoke-virtual {p0, v3}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    .line 223
    new-instance v3, Lorg/apache/poi/hssf/OldExcelFormatException;

    const-string/jumbo v4, "The supplied spreadsheet seems to be Excel 5.0/7.0 (BIFF5) format. POI only supports BIFF8 format (from Excel versions 97/2000/XP/2003)"

    invoke-direct {v3, v4}, Lorg/apache/poi/hssf/OldExcelFormatException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :catch_0
    move-exception v3

    .line 229
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "The supplied POIFSFileSystem does not contain a BIFF8 \'Workbook\' entry. Is it really an excel file?"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 211
    :cond_0
    aget-object v2, v1, v0

    .line 213
    .local v2, "wbName":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, v2}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 214
    return-object v2

    .line 215
    :catch_1
    move-exception v3

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private searchForPictures(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFPictureData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1675
    .local p1, "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .local p2, "pictures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFPictureData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1694
    return-void

    .line 1675
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 1677
    .local v1, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v3, v1, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v3, :cond_1

    move-object v3, v1

    .line 1679
    check-cast v3, Lorg/apache/poi/ddf/EscherBSERecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v0

    .line 1680
    .local v0, "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    if-eqz v0, :cond_1

    .line 1683
    new-instance v2, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;

    invoke-direct {v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;-><init>(Lorg/apache/poi/ddf/EscherBlipRecord;)V

    .line 1684
    .local v2, "picture":Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1691
    .end local v0    # "blip":Lorg/apache/poi/ddf/EscherBlipRecord;
    .end local v2    # "picture":Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->searchForPictures(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private setPropertiesFromWorkbook(Lorg/apache/poi/hssf/model/InternalWorkbook;)V
    .locals 0
    .param p1, "book"    # Lorg/apache/poi/hssf/model/InternalWorkbook;

    .prologue
    .line 345
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    .line 348
    return-void
.end method

.method private validateSheetIndex(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 441
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 442
    .local v0, "lastSheetIx":I
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_2

    .line 443
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "(0.."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 444
    .local v1, "range":Ljava/lang/String;
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 445
    const-string/jumbo v1, "(no sheets)"

    .line 447
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Sheet index ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 448
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") is out of range "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 447
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 450
    .end local v1    # "range":Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public addPicture([BI)I
    .locals 4
    .param p1, "pictureData"    # [B
    .param p2, "format"    # I

    .prologue
    const/4 v3, 0x0

    .line 1598
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->initDrawings()V

    .line 1601
    new-instance v0, Lorg/apache/poi/ddf/EscherBitmapBlip;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherBitmapBlip;-><init>()V

    .line 1602
    .local v0, "blipRecord":Lorg/apache/poi/ddf/EscherBitmapBlip;
    add-int/lit16 v2, p2, -0xfe8

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setRecordId(S)V

    .line 1603
    packed-switch p2, :pswitch_data_0

    .line 1626
    :goto_0
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setMarker(B)V

    .line 1627
    invoke-virtual {v0, p1}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setPictureData([B)V

    .line 1629
    new-instance v1, Lorg/apache/poi/ddf/EscherBSERecord;

    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherBSERecord;-><init>()V

    .line 1630
    .local v1, "r":Lorg/apache/poi/ddf/EscherBSERecord;
    const/16 v2, -0xff9

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setRecordId(S)V

    .line 1631
    shl-int/lit8 v2, p2, 0x4

    or-int/lit8 v2, v2, 0x2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setOptions(S)V

    .line 1632
    int-to-byte v2, p2

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeMacOS(B)V

    .line 1633
    int-to-byte v2, p2

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipTypeWin32(B)V

    .line 1635
    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setTag(S)V

    .line 1636
    array-length v2, p1

    add-int/lit8 v2, v2, 0x19

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setSize(I)V

    .line 1637
    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 1638
    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherBSERecord;->setOffset(I)V

    .line 1639
    invoke-virtual {v1, v0}, Lorg/apache/poi/ddf/EscherBSERecord;->setBlipRecord(Lorg/apache/poi/ddf/EscherBlipRecord;)V

    .line 1641
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2, v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->addBSERecord(Lorg/apache/poi/ddf/EscherBSERecord;)I

    move-result v2

    return v2

    .line 1606
    .end local v1    # "r":Lorg/apache/poi/ddf/EscherBSERecord;
    :pswitch_0
    const/16 v2, 0x3d40

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setOptions(S)V

    goto :goto_0

    .line 1609
    :pswitch_1
    const/16 v2, 0x2160

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setOptions(S)V

    goto :goto_0

    .line 1612
    :pswitch_2
    const/16 v2, 0x5420

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setOptions(S)V

    goto :goto_0

    .line 1615
    :pswitch_3
    const/16 v2, 0x6e00

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setOptions(S)V

    goto :goto_0

    .line 1618
    :pswitch_4
    const/16 v2, 0x46a0

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setOptions(S)V

    goto :goto_0

    .line 1621
    :pswitch_5
    const/16 v2, 0x7a80

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBitmapBlip;->setOptions(S)V

    goto :goto_0

    .line 1603
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public addSSTString(Ljava/lang/String;)I
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1311
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    new-instance v1, Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-direct {v1, p1}, Lorg/apache/poi/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->addSSTString(Lorg/apache/poi/hssf/record/common/UnicodeString;)I

    move-result v0

    return v0
.end method

.method public addToolPack(Lorg/apache/poi/ss/formula/udf/UDFFinder;)V
    .locals 1
    .param p1, "toopack"    # Lorg/apache/poi/ss/formula/udf/UDFFinder;

    .prologue
    .line 1785
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lorg/apache/poi/ss/formula/udf/UDFFinder;

    check-cast v0, Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;

    .line 1786
    .local v0, "udfs":Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;
    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/formula/udf/AggregatingUDFFinder;->add(Lorg/apache/poi/ss/formula/udf/UDFFinder;)V

    .line 1787
    return-void
.end method

.method public changeExternalReference(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "oldUrl"    # Ljava/lang/String;
    .param p2, "newUrl"    # Ljava/lang/String;

    .prologue
    .line 1833
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->changeExternalReference(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public cloneSheet(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 10
    .param p1, "sheetIndex"    # I

    .prologue
    const/4 v9, 0x0

    .line 697
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 698
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .line 699
    .local v7, "srcSheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v8, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v6

    .line 700
    .local v6, "srcName":Ljava/lang/String;
    invoke-virtual {v7, p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->cloneSheet(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v0

    .line 701
    .local v0, "clonedSheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 702
    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 704
    invoke-direct {p0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getUniqueSheetName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 705
    .local v2, "name":Ljava/lang/String;
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    .line 706
    .local v5, "newSheetIndex":I
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 707
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v8, v5, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 710
    const/16 v8, 0xd

    invoke-virtual {p0, p1, v8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->findExistingBuiltinNameRecordIdx(IB)I

    move-result v1

    .line 711
    .local v1, "filterDbNameIndex":I
    const/4 v8, -0x1

    if-eq v1, v8, :cond_0

    .line 712
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v8, v1, v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->cloneFilter(II)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v4

    .line 713
    .local v4, "newNameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    new-instance v3, Lorg/apache/poi/hssf/usermodel/HSSFName;

    invoke-direct {v3, p0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFName;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/record/NameRecord;)V

    .line 714
    .local v3, "newName":Lorg/apache/poi/hssf/usermodel/HSSFName;
    iget-object v8, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 719
    .end local v3    # "newName":Lorg/apache/poi/hssf/usermodel/HSSFName;
    .end local v4    # "newNameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    :cond_0
    return-object v0
.end method

.method public bridge synthetic cloneSheet(I)Lorg/apache/poi/ss/usermodel/Sheet;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->cloneSheet(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v0

    return-object v0
.end method

.method createBuiltInName(BI)Lorg/apache/poi/hssf/usermodel/HSSFName;
    .locals 4
    .param p1, "builtinCode"    # B
    .param p2, "sheetIndex"    # I

    .prologue
    .line 1016
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, p1, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    .line 1017
    .local v0, "nameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFName;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFName;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/record/NameRecord;Lorg/apache/poi/hssf/record/NameCommentRecord;)V

    .line 1018
    .local v1, "newName":Lorg/apache/poi/hssf/usermodel/HSSFName;
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1019
    return-object v1
.end method

.method public createCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .locals 5

    .prologue
    .line 1138
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumExFormats()I

    move-result v3

    const/16 v4, 0xfbe

    if-ne v3, v4, :cond_0

    .line 1139
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "The maximum number of cell styles was exceeded. You can define up to 4000 styles in a .xls workbook"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1142
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createCellXF()Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v2

    .line 1143
    .local v2, "xfr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumCellStyles()S

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-short v0, v3

    .line 1144
    .local v0, "index":S
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    invoke-direct {v1, v0, v2, p0}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;-><init>(SLorg/apache/poi/hssf/record/ExtendedFormatRecord;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 1146
    .local v1, "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    return-object v1
.end method

.method public bridge synthetic createCellStyle()Lorg/apache/poi/ss/usermodel/CellStyle;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v0

    return-object v0
.end method

.method public createDataFormat()Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;
    .locals 2

    .prologue
    .line 1497
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;

    if-nez v0, :cond_0

    .line 1498
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;-><init>(Lorg/apache/poi/hssf/model/InternalWorkbook;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;

    .line 1499
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;

    return-object v0
.end method

.method public bridge synthetic createDataFormat()Lorg/apache/poi/ss/usermodel/DataFormat;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createDataFormat()Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;

    move-result-object v0

    return-object v0
.end method

.method public createFont()Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .locals 3

    .prologue
    .line 1042
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfFonts()S

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-short v0, v1

    .line 1044
    .local v0, "fontindex":S
    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 1046
    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    .line 1048
    :cond_0
    const/16 v1, 0x7fff

    if-ne v0, v1, :cond_1

    .line 1049
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Maximum number of fonts was exceeded"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1054
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getFontAt(S)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic createFont()Lorg/apache/poi/ss/usermodel/Font;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createFont()Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v0

    return-object v0
.end method

.method public createName()Lorg/apache/poi/hssf/usermodel/HSSFName;
    .locals 3

    .prologue
    .line 1444
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createName()Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    .line 1446
    .local v0, "nameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFName;

    invoke-direct {v1, p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFName;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/record/NameRecord;)V

    .line 1448
    .local v1, "newName":Lorg/apache/poi/hssf/usermodel/HSSFName;
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1450
    return-object v1
.end method

.method public bridge synthetic createName()Lorg/apache/poi/ss/usermodel/Name;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createName()Lorg/apache/poi/hssf/usermodel/HSSFName;

    move-result-object v0

    return-object v0
.end method

.method public createSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 680
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    invoke-direct {v1, p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 682
    .local v1, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 683
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Sheet"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 684
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 685
    .local v0, "isOnlySheet":Z
    :goto_0
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 686
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 687
    return-object v1

    .line 684
    .end local v0    # "isOnlySheet":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createSheet(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 4
    .param p1, "sheetname"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 798
    if-nez p1, :cond_0

    .line 799
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "sheetName must not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 802
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->doesContainsSheetName(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 803
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "The workbook already contains a sheet of this name"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 805
    :cond_1
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    invoke-direct {v1, p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 807
    .local v1, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 808
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 809
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 810
    .local v0, "isOnlySheet":Z
    :goto_0
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 811
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 812
    return-object v1

    .line 809
    .end local v0    # "isOnlySheet":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createSheet()Lorg/apache/poi/ss/usermodel/Sheet;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createSheet(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/Sheet;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createSheet(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v0

    return-object v0
.end method

.method public dumpDrawingGroupRecords(Z)V
    .locals 7
    .param p1, "fat"    # Z

    .prologue
    .line 1558
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    const/16 v6, 0xeb

    invoke-virtual {v5, v6}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    .line 1560
    .local v3, "r":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    if-eqz v3, :cond_1

    .line 1561
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->decode()V

    .line 1562
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->getEscherRecords()Ljava/util/List;

    move-result-object v1

    .line 1563
    .local v1, "escherRecords":Ljava/util/List;
    new-instance v4, Ljava/io/PrintWriter;

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v4, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 1564
    .local v4, "w":Ljava/io/PrintWriter;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1565
    .local v2, "iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1573
    invoke-virtual {v4}, Ljava/io/PrintWriter;->flush()V

    .line 1575
    .end local v1    # "escherRecords":Ljava/util/List;
    .end local v2    # "iterator":Ljava/util/Iterator;
    .end local v4    # "w":Ljava/io/PrintWriter;
    :cond_1
    return-void

    .line 1566
    .restart local v1    # "escherRecords":Ljava/util/List;
    .restart local v2    # "iterator":Ljava/util/Iterator;
    .restart local v4    # "w":Ljava/io/PrintWriter;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 1567
    .local v0, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    if-nez p1, :cond_0

    .line 1571
    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lorg/apache/poi/ddf/EscherRecord;->display(Ljava/io/PrintWriter;I)V

    goto :goto_0
.end method

.method findExistingBuiltinNameRecordIdx(IB)I
    .locals 4
    .param p1, "sheetIndex"    # I
    .param p2, "builtinCode"    # B

    .prologue
    .line 998
    const/4 v0, 0x0

    .local v0, "defNameIndex":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1010
    const/4 v0, -0x1

    .end local v0    # "defNameIndex":I
    :goto_1
    return v0

    .line 999
    .restart local v0    # "defNameIndex":I
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v1

    .line 1000
    .local v1, "r":Lorg/apache/poi/hssf/record/NameRecord;
    if-nez v1, :cond_1

    .line 1001
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Unable to find all defined names to iterate over"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1003
    :cond_1
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->isBuiltInName()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getBuiltInName()B

    move-result v2

    if-eq v2, p2, :cond_3

    .line 998
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1006
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v2, p1, :cond_2

    goto :goto_1
.end method

.method public findFont(SSSLjava/lang/String;ZZSB)Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .locals 3
    .param p1, "boldWeight"    # S
    .param p2, "color"    # S
    .param p3, "fontHeight"    # S
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "italic"    # Z
    .param p6, "strikeout"    # Z
    .param p7, "typeOffset"    # S
    .param p8, "underline"    # B

    .prologue
    .line 1064
    const/4 v1, 0x0

    .local v1, "i":S
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfFonts()S

    move-result v2

    if-le v1, v2, :cond_0

    .line 1082
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1066
    :cond_0
    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 1064
    :cond_1
    add-int/lit8 v2, v1, 0x1

    int-to-short v1, v2

    goto :goto_0

    .line 1068
    :cond_2
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getFontAt(S)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v0

    .line 1069
    .local v0, "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getBoldweight()S

    move-result v2

    if-ne v2, p1, :cond_1

    .line 1070
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getColor()S

    move-result v2

    if-ne v2, p2, :cond_1

    .line 1071
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeight()S

    move-result v2

    if-ne v2, p3, :cond_1

    .line 1072
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1073
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getItalic()Z

    move-result v2

    if-ne v2, p5, :cond_1

    .line 1074
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getStrikeout()Z

    move-result v2

    if-ne v2, p6, :cond_1

    .line 1075
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getTypeOffset()S

    move-result v2

    if-ne v2, p7, :cond_1

    .line 1076
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getUnderline()B

    move-result v2

    if-ne v2, p8, :cond_1

    goto :goto_1
.end method

.method public bridge synthetic findFont(SSSLjava/lang/String;ZZSB)Lorg/apache/poi/ss/usermodel/Font;
    .locals 1

    .prologue
    .line 1
    invoke-virtual/range {p0 .. p8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->findFont(SSSLjava/lang/String;ZZSB)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v0

    return-object v0
.end method

.method public findSheetNameFromExternSheet(I)Ljava/lang/String;
    .locals 1
    .param p1, "externSheetIndex"    # I

    .prologue
    .line 653
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findSheetNameFromExternSheet(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getActiveSheetIndex()I
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowOneRecord;->getActiveSheetIndex()I

    move-result v0

    return v0
.end method

.method public getAllEmbeddedObjects()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFObjectData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1726
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1727
    .local v1, "objects":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFObjectData;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1731
    return-object v1

    .line 1729
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getAllEmbeddedObjects(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Ljava/util/List;)V

    .line 1727
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getAllPictures()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFPictureData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1652
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1653
    .local v1, "pictures":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFPictureData;>;"
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1654
    .local v3, "recordIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/Record;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1664
    return-object v1

    .line 1656
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/Record;

    .line 1657
    .local v2, "r":Lorg/apache/poi/hssf/record/Record;
    instance-of v4, v2, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;

    if-eqz v4, :cond_0

    move-object v4, v2

    .line 1659
    check-cast v4, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;->decode()V

    .line 1660
    check-cast v2, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;

    .end local v2    # "r":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    move-result-object v0

    .line 1661
    .local v0, "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->searchForPictures(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method public getBackupFlag()Z
    .locals 2

    .prologue
    .line 942
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBackupRecord()Lorg/apache/poi/hssf/record/BackupRecord;

    move-result-object v0

    .line 944
    .local v0, "backupRecord":Lorg/apache/poi/hssf/record/BackupRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/BackupRecord;->getBackup()S

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    .line 945
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method getBuiltInName(BI)Lorg/apache/poi/hssf/usermodel/HSSFName;
    .locals 2
    .param p1, "builtinCode"    # B
    .param p2, "sheetIndex"    # I

    .prologue
    .line 1024
    invoke-virtual {p0, p2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->findExistingBuiltinNameRecordIdx(IB)I

    move-result v0

    .line 1025
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 1026
    const/4 v1, 0x0

    .line 1028
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFName;

    goto :goto_0
.end method

.method public getBytes()[B
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 1258
    sget-object v10, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v10, v12}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1259
    sget-object v10, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v11, "HSSFWorkbook.getBytes()"

    invoke-virtual {v10, v12, v11}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 1262
    :cond_0
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheets()[Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v6

    .line 1263
    .local v6, "sheets":[Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    array-length v2, v6

    .line 1268
    .local v2, "nSheets":I
    iget-object v10, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v10}, Lorg/apache/poi/hssf/model/InternalWorkbook;->preSerialize()V

    .line 1269
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 1274
    iget-object v10, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v10}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSize()I

    move-result v9

    .line 1277
    .local v9, "totalsize":I
    new-array v7, v2, [Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;

    .line 1278
    .local v7, "srCollectors":[Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_1
    if-lt v1, v2, :cond_2

    .line 1286
    new-array v4, v9, [B

    .line 1287
    .local v4, "retval":[B
    iget-object v10, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->serialize(I[B)I

    move-result v3

    .line 1289
    .local v3, "pos":I
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v2, :cond_3

    .line 1303
    return-object v4

    .line 1270
    .end local v1    # "k":I
    .end local v3    # "pos":I
    .end local v4    # "retval":[B
    .end local v7    # "srCollectors":[Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    .end local v9    # "totalsize":I
    :cond_1
    aget-object v10, v6, v0

    invoke-virtual {v10}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/poi/hssf/model/InternalSheet;->preSerialize()V

    .line 1271
    aget-object v10, v6, v0

    invoke-virtual {v10}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->preSerialize()V

    .line 1269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1279
    .restart local v1    # "k":I
    .restart local v7    # "srCollectors":[Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    .restart local v9    # "totalsize":I
    :cond_2
    iget-object v10, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v10, v1, v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetBof(II)V

    .line 1280
    new-instance v8, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;

    invoke-direct {v8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;-><init>()V

    .line 1281
    .local v8, "src":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    aget-object v10, v6, v1

    invoke-virtual {v10}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Lorg/apache/poi/hssf/model/InternalSheet;->visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V

    .line 1282
    invoke-virtual {v8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->getTotalSize()I

    move-result v10

    add-int/2addr v9, v10

    .line 1283
    aput-object v8, v7, v1

    .line 1278
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1290
    .end local v8    # "src":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    .restart local v3    # "pos":I
    .restart local v4    # "retval":[B
    :cond_3
    aget-object v8, v7, v1

    .line 1291
    .restart local v8    # "src":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    invoke-virtual {v8, v3, v4}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->serialize(I[B)I

    move-result v5

    .line 1292
    .local v5, "serializedSize":I
    invoke-virtual {v8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->getTotalSize()I

    move-result v10

    if-eq v5, v10, :cond_4

    .line 1296
    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Actual serialized sheet size ("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1297
    const-string/jumbo v12, ") differs from pre-calculated size ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->getTotalSize()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1298
    const-string/jumbo v12, ") for sheet ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1296
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1301
    :cond_4
    add-int/2addr v3, v5

    .line 1289
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public getCellStyleAt(S)Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .locals 3
    .param p1, "idx"    # S

    .prologue
    .line 1166
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getExFormatAt(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v1

    .line 1167
    .local v1, "xfr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    invoke-direct {v0, p1, v1, p0}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;-><init>(SLorg/apache/poi/hssf/record/ExtendedFormatRecord;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 1169
    .local v0, "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    return-object v0
.end method

.method public bridge synthetic getCellStyleAt(S)Lorg/apache/poi/ss/usermodel/CellStyle;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCellStyleAt(S)Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v0

    return-object v0
.end method

.method public getCreationHelper()Lorg/apache/poi/hssf/usermodel/HSSFCreationHelper;
    .locals 1

    .prologue
    .line 1765
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFCreationHelper;

    invoke-direct {v0, p0}, Lorg/apache/poi/hssf/usermodel/HSSFCreationHelper;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    return-object v0
.end method

.method public bridge synthetic getCreationHelper()Lorg/apache/poi/ss/usermodel/CreationHelper;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCreationHelper()Lorg/apache/poi/hssf/usermodel/HSSFCreationHelper;

    move-result-object v0

    return-object v0
.end method

.method public getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .locals 2

    .prologue
    .line 1522
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/record/PaletteRecord;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;-><init>(Lorg/apache/poi/hssf/record/PaletteRecord;)V

    return-object v0
.end method

.method public getDisplayedTab()S
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getFirstVisibleTab()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getExternalSheetIndex(I)I
    .locals 1
    .param p1, "internalSheetIndex"    # I

    .prologue
    .line 645
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v0

    return v0
.end method

.method public getFirstVisibleTab()I
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowOneRecord;->getFirstVisibleTab()I

    move-result v0

    return v0
.end method

.method public getFontAt(S)Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .locals 4
    .param p1, "idx"    # S

    .prologue
    .line 1101
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    iput-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 1106
    :cond_0
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    .line 1107
    .local v2, "sIdx":Ljava/lang/Short;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1108
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/usermodel/HSSFFont;

    .line 1115
    :goto_0
    return-object v3

    .line 1111
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getFontRecordAt(I)Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v0

    .line 1112
    .local v0, "font":Lorg/apache/poi/hssf/record/FontRecord;
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFFont;

    invoke-direct {v1, p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;-><init>(SLorg/apache/poi/hssf/record/FontRecord;)V

    .line 1113
    .local v1, "retval":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    invoke-virtual {v3, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v1

    .line 1115
    goto :goto_0
.end method

.method public bridge synthetic getFontAt(S)Lorg/apache/poi/ss/usermodel/Font;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getFontAt(S)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v0

    return-object v0
.end method

.method public getForceFormulaRecalculation()Z
    .locals 3

    .prologue
    .line 1818
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    .line 1819
    .local v0, "iwb":Lorg/apache/poi/hssf/model/InternalWorkbook;
    const/16 v2, 0x1c1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/RecalcIdRecord;

    .line 1820
    .local v1, "recalc":Lorg/apache/poi/hssf/record/RecalcIdRecord;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/RecalcIdRecord;->getEngineId()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMissingCellPolicy()Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    return-object v0
.end method

.method public getName(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/HSSFName;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1332
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameIndex(Ljava/lang/String;)I

    move-result v0

    .line 1333
    .local v0, "nameIndex":I
    if-gez v0, :cond_0

    .line 1334
    const/4 v1, 0x0

    .line 1336
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFName;

    goto :goto_0
.end method

.method public bridge synthetic getName(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/Name;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getName(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/HSSFName;

    move-result-object v0

    return-object v0
.end method

.method public getNameAt(I)Lorg/apache/poi/hssf/usermodel/HSSFName;
    .locals 4
    .param p1, "nameIndex"    # I

    .prologue
    .line 1340
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1341
    .local v0, "nNames":I
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 1342
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "There are no defined names in this workbook"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1344
    :cond_0
    if-ltz p1, :cond_1

    if-le p1, v0, :cond_2

    .line 1345
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Specified name index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1346
    const-string/jumbo v3, " is outside the allowable range (0.."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1345
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1348
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFName;

    return-object v1
.end method

.method public bridge synthetic getNameAt(I)Lorg/apache/poi/ss/usermodel/Name;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameAt(I)Lorg/apache/poi/hssf/usermodel/HSSFName;

    move-result-object v0

    return-object v0
.end method

.method public getNameIndex(Ljava/lang/String;)I
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1455
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 1462
    const/4 v0, -0x1

    .end local v0    # "k":I
    :cond_0
    return v0

    .line 1456
    .restart local v0    # "k":I
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameName(I)Ljava/lang/String;

    move-result-object v1

    .line 1458
    .local v1, "nameName":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1455
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method getNameIndex(Lorg/apache/poi/hssf/usermodel/HSSFName;)I
    .locals 2
    .param p1, "name"    # Lorg/apache/poi/hssf/usermodel/HSSFName;

    .prologue
    .line 1476
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1481
    const/4 v0, -0x1

    .end local v0    # "k":I
    :cond_0
    return v0

    .line 1477
    .restart local v0    # "k":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eq p1, v1, :cond_0

    .line 1476
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getNameName(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1360
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameAt(I)Lorg/apache/poi/hssf/usermodel/HSSFName;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFName;->getNameName()Ljava/lang/String;

    move-result-object v0

    .line 1362
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 1
    .param p1, "nameIndex"    # I

    .prologue
    .line 1352
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    return-object v0
.end method

.method public getNumCellStyles()S
    .locals 1

    .prologue
    .line 1156
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumExFormats()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getNumberOfFonts()S
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumberOfFontRecords()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getNumberOfNames()I
    .locals 2

    .prologue
    .line 1327
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1328
    .local v0, "result":I
    return v0
.end method

.method public getNumberOfSheets()I
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPrintArea(I)Ljava/lang/String;
    .locals 4
    .param p1, "sheetIndex"    # I

    .prologue
    .line 1423
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    const/4 v2, 0x6

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    .line 1425
    .local v0, "name":Lorg/apache/poi/hssf/record/NameRecord;
    if-nez v0, :cond_0

    .line 1426
    const/4 v1, 0x0

    .line 1429
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/NameRecord;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->toFormulaString(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;[Lorg/apache/poi/ss/formula/ptg/Ptg;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getRootDirectory()Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .locals 1

    .prologue
    .line 1837
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    return-object v0
.end method

.method public getSSTString(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1319
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSSTString(I)Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedTab()S
    .locals 1

    .prologue
    .line 521
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getActiveSheetIndex()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getSheet(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 855
    const/4 v1, 0x0

    .line 857
    .local v1, "retval":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 866
    return-object v1

    .line 859
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v2

    .line 861
    .local v2, "sheetname":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 863
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "retval":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .line 857
    .restart local v1    # "retval":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic getSheet(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/Sheet;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheet(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v0

    return-object v0
.end method

.method public getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 843
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 844
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    return-object v0
.end method

.method public bridge synthetic getSheetAt(I)Lorg/apache/poi/ss/usermodel/Sheet;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v0

    return-object v0
.end method

.method public getSheetIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I
    .locals 2
    .param p1, "sheet"    # Lorg/apache/poi/ss/usermodel/Sheet;

    .prologue
    .line 627
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 632
    const/4 v0, -0x1

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 628
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 627
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getSheetIndexFromExternSheetIndex(I)I
    .locals 1
    .param p1, "externSheetNumber"    # I

    .prologue
    .line 826
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    move-result v0

    return v0
.end method

.method public getSheetName(I)Ljava/lang/String;
    .locals 1
    .param p1, "sheetIndex"    # I

    .prologue
    .line 580
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 581
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getUDFFinder()Lorg/apache/poi/ss/formula/udf/UDFFinder;
    .locals 1

    .prologue
    .line 1776
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lorg/apache/poi/ss/formula/udf/UDFFinder;

    return-object v0
.end method

.method getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;
    .locals 1

    .prologue
    .line 1323
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    return-object v0
.end method

.method initDrawings()V
    .locals 3

    .prologue
    .line 1578
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findDrawingGroup()Lorg/apache/poi/hssf/model/DrawingManager2;

    move-result-object v1

    .line 1579
    .local v1, "mgr":Lorg/apache/poi/hssf/model/DrawingManager2;
    if-eqz v1, :cond_1

    .line 1580
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1586
    .end local v0    # "i":I
    :goto_1
    return-void

    .line 1581
    .restart local v0    # "i":I
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 1580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1584
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createDrawingGroup()V

    goto :goto_1
.end method

.method public insertChartRecord()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x10

    const/16 v7, -0x10

    const/4 v6, 0x1

    const/16 v5, 0x8

    .line 1528
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    const/16 v4, 0xfc

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v1

    .line 1529
    .local v1, "loc":I
    const/16 v3, 0x5a

    new-array v0, v3, [B

    const/4 v3, 0x0

    .line 1530
    const/16 v4, 0xf

    aput-byte v4, v0, v3

    const/4 v3, 0x3

    aput-byte v7, v0, v3

    const/4 v3, 0x4

    const/16 v4, 0x52

    aput-byte v4, v0, v3

    const/16 v3, 0xa

    .line 1532
    const/4 v4, 0x6

    aput-byte v4, v0, v3

    const/16 v3, 0xb

    aput-byte v7, v0, v3

    const/16 v3, 0xc

    const/16 v4, 0x18

    aput-byte v4, v0, v3

    .line 1533
    aput-byte v6, v0, v8

    const/16 v3, 0x11

    aput-byte v5, v0, v3

    const/16 v3, 0x14

    .line 1534
    aput-byte v9, v0, v3

    const/16 v3, 0x18

    aput-byte v9, v0, v3

    const/16 v3, 0x1c

    .line 1535
    aput-byte v6, v0, v3

    const/16 v3, 0x20

    .line 1536
    aput-byte v6, v0, v3

    const/16 v3, 0x24

    .line 1537
    const/4 v4, 0x3

    aput-byte v4, v0, v3

    const/16 v3, 0x28

    .line 1538
    const/16 v4, 0x33

    aput-byte v4, v0, v3

    const/16 v3, 0x2a

    const/16 v4, 0xb

    aput-byte v4, v0, v3

    const/16 v3, 0x2b

    aput-byte v7, v0, v3

    const/16 v3, 0x2c

    const/16 v4, 0x12

    aput-byte v4, v0, v3

    const/16 v3, 0x30

    .line 1539
    const/16 v4, -0x41

    aput-byte v4, v0, v3

    const/16 v3, 0x32

    .line 1540
    aput-byte v5, v0, v3

    const/16 v3, 0x34

    aput-byte v5, v0, v3

    const/16 v3, 0x36

    const/16 v4, -0x7f

    aput-byte v4, v0, v3

    const/16 v3, 0x37

    .line 1541
    aput-byte v6, v0, v3

    const/16 v3, 0x38

    const/16 v4, 0x9

    aput-byte v4, v0, v3

    const/16 v3, 0x3b

    aput-byte v5, v0, v3

    const/16 v3, 0x3c

    .line 1542
    const/16 v4, -0x40

    aput-byte v4, v0, v3

    const/16 v3, 0x3d

    aput-byte v6, v0, v3

    const/16 v3, 0x3e

    const/16 v4, 0x40

    aput-byte v4, v0, v3

    const/16 v3, 0x41

    .line 1543
    aput-byte v5, v0, v3

    const/16 v3, 0x42

    const/16 v4, 0x40

    aput-byte v4, v0, v3

    const/16 v3, 0x44

    const/16 v4, 0x1e

    aput-byte v4, v0, v3

    const/16 v3, 0x45

    const/16 v4, -0xf

    aput-byte v4, v0, v3

    const/16 v3, 0x46

    .line 1544
    aput-byte v8, v0, v3

    const/16 v3, 0x4a

    const/16 v4, 0xd

    aput-byte v4, v0, v3

    const/16 v3, 0x4d

    .line 1545
    aput-byte v5, v0, v3

    const/16 v3, 0x4e

    const/16 v4, 0xc

    aput-byte v4, v0, v3

    const/16 v3, 0x51

    .line 1546
    aput-byte v5, v0, v3

    const/16 v3, 0x52

    const/16 v4, 0x17

    aput-byte v4, v0, v3

    const/16 v3, 0x55

    .line 1547
    aput-byte v5, v0, v3

    const/16 v3, 0x56

    const/16 v4, -0x9

    aput-byte v4, v0, v3

    const/16 v3, 0x59

    aput-byte v8, v0, v3

    .line 1549
    .local v0, "data":[B
    new-instance v2, Lorg/apache/poi/hssf/record/UnknownRecord;

    const/16 v3, 0xeb

    invoke-direct {v2, v3, v0}, Lorg/apache/poi/hssf/record/UnknownRecord;-><init>(I[B)V

    .line 1550
    .local v2, "r":Lorg/apache/poi/hssf/record/UnknownRecord;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1551
    return-void
.end method

.method public isHidden()Z
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowOneRecord;->getHidden()Z

    move-result v0

    return v0
.end method

.method public isSheetHidden(I)Z
    .locals 1
    .param p1, "sheetIx"    # I

    .prologue
    .line 593
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 594
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->isSheetHidden(I)Z

    move-result v0

    return v0
.end method

.method public isSheetVeryHidden(I)Z
    .locals 1
    .param p1, "sheetIx"    # I

    .prologue
    .line 598
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 599
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->isSheetVeryHidden(I)Z

    move-result v0

    return v0
.end method

.method public isWriteProtected()Z
    .locals 1

    .prologue
    .line 1700
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->isWriteProtected()Z

    move-result v0

    return v0
.end method

.method public removeName(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1486
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1487
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->removeName(I)V

    .line 1488
    return-void
.end method

.method public removeName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1504
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameIndex(Ljava/lang/String;)I

    move-result v0

    .line 1505
    .local v0, "index":I
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->removeName(I)V

    .line 1506
    return-void
.end method

.method removeName(Lorg/apache/poi/hssf/usermodel/HSSFName;)V
    .locals 1
    .param p1, "name"    # Lorg/apache/poi/hssf/usermodel/HSSFName;

    .prologue
    .line 1516
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameIndex(Lorg/apache/poi/hssf/usermodel/HSSFName;)I

    move-result v0

    .line 1517
    .local v0, "index":I
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->removeName(I)V

    .line 1518
    return-void
.end method

.method public removePrintArea(I)V
    .locals 3
    .param p1, "sheetIndex"    # I

    .prologue
    .line 1437
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    const/4 v1, 0x6

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->removeBuiltinRecord(BI)V

    .line 1438
    return-void
.end method

.method public removeSheetAt(I)V
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 884
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 885
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isActive()Z

    move-result v4

    .line 886
    .local v4, "wasActive":Z
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isSelected()Z

    move-result v5

    .line 888
    .local v5, "wasSelected":Z
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 889
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v6, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->removeSheet(I)V

    .line 892
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    .line 893
    .local v1, "nSheets":I
    const/4 v6, 0x1

    if-ge v1, v6, :cond_1

    .line 918
    :cond_0
    :goto_0
    return-void

    .line 898
    :cond_1
    move v2, p1

    .line 899
    .local v2, "newSheetIndex":I
    if-lt v2, v1, :cond_2

    .line 900
    add-int/lit8 v2, v1, -0x1

    .line 902
    :cond_2
    if-eqz v4, :cond_3

    .line 903
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->setActiveSheet(I)V

    .line 906
    :cond_3
    if-eqz v5, :cond_0

    .line 907
    const/4 v3, 0x0

    .line 908
    .local v3, "someOtherSheetIsStillSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_4

    .line 914
    :goto_2
    if-nez v3, :cond_0

    .line 915
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->setSelectedTab(I)V

    goto :goto_0

    .line 909
    :cond_4
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isSelected()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 910
    const/4 v3, 0x1

    .line 911
    goto :goto_2

    .line 908
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected resetFontCache()V
    .locals 1

    .prologue
    .line 1125
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 1126
    return-void
.end method

.method public resolveNameXText(II)Ljava/lang/String;
    .locals 1
    .param p1, "refIndex"    # I
    .param p2, "definedNameIndex"    # I

    .prologue
    .line 665
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->resolveNameXText(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setActiveSheet(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 498
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 499
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 500
    .local v1, "nSheets":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 503
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setActiveSheetIndex(I)V

    .line 504
    return-void

    .line 501
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v3

    if-ne v0, p1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 501
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public setBackupFlag(Z)V
    .locals 2
    .param p1, "backupValue"    # Z

    .prologue
    .line 928
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBackupRecord()Lorg/apache/poi/hssf/record/BackupRecord;

    move-result-object v0

    .line 930
    .local v0, "backupRecord":Lorg/apache/poi/hssf/record/BackupRecord;
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BackupRecord;->setBackup(S)V

    .line 932
    return-void

    .line 931
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDisplayedTab(S)V
    .locals 0
    .param p1, "index"    # S

    .prologue
    .line 538
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->setFirstVisibleTab(I)V

    .line 539
    return-void
.end method

.method public setFirstVisibleTab(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 531
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setFirstVisibleTab(I)V

    .line 532
    return-void
.end method

.method public setForceFormulaRecalculation(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 1807
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    .line 1808
    .local v0, "iwb":Lorg/apache/poi/hssf/model/InternalWorkbook;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getRecalcId()Lorg/apache/poi/hssf/record/RecalcIdRecord;

    move-result-object v1

    .line 1809
    .local v1, "recalc":Lorg/apache/poi/hssf/record/RecalcIdRecord;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/RecalcIdRecord;->setEngineId(I)V

    .line 1810
    return-void
.end method

.method public setHidden(Z)V
    .locals 1
    .param p1, "hiddenFlag"    # Z

    .prologue
    .line 589
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setHidden(Z)V

    .line 590
    return-void
.end method

.method public setMissingCellPolicy(Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;)V
    .locals 0
    .param p1, "missingCellPolicy"    # Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .prologue
    .line 416
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    .line 417
    return-void
.end method

.method public setPrintArea(IIIII)V
    .locals 4
    .param p1, "sheetIndex"    # I
    .param p2, "startColumn"    # I
    .param p3, "endColumn"    # I
    .param p4, "startRow"    # I
    .param p5, "endRow"    # I

    .prologue
    const/4 v2, 0x1

    .line 1407
    new-instance v0, Lorg/apache/poi/hssf/util/CellReference;

    invoke-direct {v0, p4, p2, v2, v2}, Lorg/apache/poi/hssf/util/CellReference;-><init>(IIZZ)V

    .line 1408
    .local v0, "cell":Lorg/apache/poi/hssf/util/CellReference;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v1

    .line 1410
    .local v1, "reference":Ljava/lang/String;
    new-instance v0, Lorg/apache/poi/hssf/util/CellReference;

    .end local v0    # "cell":Lorg/apache/poi/hssf/util/CellReference;
    invoke-direct {v0, p5, p3, v2, v2}, Lorg/apache/poi/hssf/util/CellReference;-><init>(IIZZ)V

    .line 1411
    .restart local v0    # "cell":Lorg/apache/poi/hssf/util/CellReference;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/poi/hssf/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1413
    invoke-virtual {p0, p1, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->setPrintArea(ILjava/lang/String;)V

    .line 1414
    return-void
.end method

.method public setPrintArea(ILjava/lang/String;)V
    .locals 7
    .param p1, "sheetIndex"    # I
    .param p2, "reference"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x6

    .line 1374
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v6, v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v1

    .line 1377
    .local v1, "name":Lorg/apache/poi/hssf/record/NameRecord;
    if-nez v1, :cond_0

    .line 1378
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v6, v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v1

    .line 1381
    :cond_0
    sget-object v4, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    .line 1382
    .local v2, "parts":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1383
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_1

    .line 1391
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v4, p0, v5, p1}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;II)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/apache/poi/hssf/record/NameRecord;->setNameDefinition([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 1392
    return-void

    .line 1384
    :cond_1
    if-lez v0, :cond_2

    .line 1385
    const-string/jumbo v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1387
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/apache/poi/ss/formula/SheetNameFormatter;->appendFormat(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 1388
    const-string/jumbo v4, "!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1389
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1383
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setRepeatingRowsAndColumns(IIIII)V
    .locals 4
    .param p1, "sheetIndex"    # I
    .param p2, "startColumn"    # I
    .param p3, "endColumn"    # I
    .param p4, "startRow"    # I
    .param p5, "endRow"    # I

    .prologue
    const/4 v3, -0x1

    .line 980
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    .line 982
    .local v2, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    const/4 v1, 0x0

    .line 983
    .local v1, "rows":Lorg/apache/poi/ss/util/CellRangeAddress;
    const/4 v0, 0x0

    .line 985
    .local v0, "cols":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-eq p4, v3, :cond_0

    .line 986
    new-instance v1, Lorg/apache/poi/ss/util/CellRangeAddress;

    .end local v1    # "rows":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-direct {v1, p4, p5, v3, v3}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    .line 988
    .restart local v1    # "rows":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_0
    if-eq p2, v3, :cond_1

    .line 989
    new-instance v0, Lorg/apache/poi/ss/util/CellRangeAddress;

    .end local v0    # "cols":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-direct {v0, v3, v3, p2, p3}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    .line 992
    .restart local v0    # "cols":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_1
    invoke-virtual {v2, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setRepeatingRows(Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 993
    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setRepeatingColumns(Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 994
    return-void
.end method

.method public setSelectedTab(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x1

    .line 458
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 459
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 460
    .local v1, "nSheets":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 463
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v2

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setNumSelectedTabs(S)V

    .line 464
    return-void

    .line 461
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v4

    if-ne v0, p1, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 460
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public setSelectedTab(S)V
    .locals 0
    .param p1, "index"    # S

    .prologue
    .line 470
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->setSelectedTab(I)V

    .line 471
    return-void
.end method

.method public setSelectedTabs([I)V
    .locals 6
    .param p1, "indexes"    # [I

    .prologue
    .line 474
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-lt v1, v4, :cond_0

    .line 477
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 478
    .local v3, "nSheets":I
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v3, :cond_1

    .line 489
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v4

    array-length v5, p1

    int-to-short v5, v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setNumSelectedTabs(S)V

    .line 490
    return-void

    .line 475
    .end local v3    # "nSheets":I
    :cond_0
    aget v4, p1, v1

    invoke-direct {p0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 474
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 479
    .restart local v3    # "nSheets":I
    :cond_1
    const/4 v0, 0x0

    .line 480
    .local v0, "bSelect":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    array-length v4, p1

    if-lt v2, v4, :cond_2

    .line 487
    :goto_3
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v4

    invoke-virtual {v4, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 478
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 481
    :cond_2
    aget v4, p1, v2

    if-ne v4, v1, :cond_3

    .line 482
    const/4 v0, 0x1

    .line 483
    goto :goto_3

    .line 480
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public setSheetHidden(II)V
    .locals 1
    .param p1, "sheetIx"    # I
    .param p2, "hidden"    # I

    .prologue
    .line 609
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 610
    invoke-static {p2}, Lorg/apache/poi/ss/util/WorkbookUtil;->validateSheetState(I)V

    .line 611
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetHidden(II)V

    .line 612
    return-void
.end method

.method public setSheetHidden(IZ)V
    .locals 1
    .param p1, "sheetIx"    # I
    .param p2, "hidden"    # Z

    .prologue
    .line 604
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 605
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetHidden(IZ)V

    .line 606
    return-void
.end method

.method public setSheetName(ILjava/lang/String;)V
    .locals 2
    .param p1, "sheetIx"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 565
    if-nez p2, :cond_0

    .line 566
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sheetName must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p2, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->doesContainsSheetName(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The workbook already contains a sheet with this name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 573
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 574
    return-void
.end method

.method public setSheetOrder(Ljava/lang/String;I)V
    .locals 6
    .param p1, "sheetname"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 427
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result v0

    .line 428
    .local v0, "oldSheetIndex":I
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    invoke-interface {v4, p2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 429
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->setSheetOrder(Ljava/lang/String;I)V

    .line 431
    invoke-static {v0, p2}, Lorg/apache/poi/ss/formula/FormulaShifter;->createForSheetShift(II)Lorg/apache/poi/ss/formula/FormulaShifter;

    move-result-object v2

    .line 432
    .local v2, "shifter":Lorg/apache/poi/ss/formula/FormulaShifter;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 436
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->updateNamesAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;)V

    .line 438
    return-void

    .line 432
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .line 433
    .local v1, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v2, v5}, Lorg/apache/poi/hssf/model/InternalSheet;->updateFormulasAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;I)V

    goto :goto_0
.end method

.method public unwriteProtectWorkbook()V
    .locals 1

    .prologue
    .line 1716
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->unwriteProtectWorkbook()V

    .line 1717
    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 7
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1186
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getBytes()[B

    move-result-object v0

    .line 1187
    .local v0, "bytes":[B
    new-instance v2, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v2}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>()V

    .line 1191
    .local v2, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v1, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1194
    .local v1, "excepts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string/jumbo v5, "Workbook"

    invoke-virtual {v2, v4, v5}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 1197
    invoke-virtual {p0, v2, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->writeProperties(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V

    .line 1199
    iget-boolean v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->preserveNodes:Z

    if-eqz v4, :cond_0

    .line 1201
    const-string/jumbo v4, "Workbook"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1204
    sget-object v5, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_1

    .line 1209
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    invoke-virtual {v2}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v5

    invoke-virtual {p0, v4, v5, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->copyNodes(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Lorg/apache/poi/poifs/filesystem/DirectoryNode;Ljava/util/List;)V

    .line 1213
    invoke-virtual {v2}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    invoke-virtual {v5}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getStorageClsid()Lorg/apache/poi/hpsf/ClassID;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->setStorageClsid(Lorg/apache/poi/hpsf/ClassID;)V

    .line 1215
    :cond_0
    invoke-virtual {v2, p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->writeFilesystem(Ljava/io/OutputStream;)V

    .line 1216
    return-void

    .line 1204
    :cond_1
    aget-object v3, v5, v4

    .line 1205
    .local v3, "wrongName":Ljava/lang/String;
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1204
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public writeProtectWorkbook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "username"    # Ljava/lang/String;

    .prologue
    .line 1709
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->workbook:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtectWorkbook(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    return-void
.end method
