.class public final Lorg/apache/poi/hssf/model/WorkbookRecordList;
.super Ljava/lang/Object;
.source "WorkbookRecordList.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/poi/hssf/record/Record;",
        ">;"
    }
.end annotation


# instance fields
.field private backuppos:I

.field private bspos:I

.field private externsheetPos:I

.field private fontpos:I

.field private namepos:I

.field private palettepos:I

.field private protpos:I

.field private records:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private supbookpos:I

.field private tabpos:I

.field private xfpos:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    .line 29
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->protpos:I

    .line 30
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->bspos:I

    .line 31
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->tabpos:I

    .line 32
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->fontpos:I

    .line 33
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->xfpos:I

    .line 34
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->backuppos:I

    .line 35
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->namepos:I

    .line 36
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->supbookpos:I

    .line 37
    iput v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->externsheetPos:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->palettepos:I

    .line 26
    return-void
.end method


# virtual methods
.method public add(ILorg/apache/poi/hssf/record/Record;)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "r"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 55
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getProtpos()I

    move-result v0

    if-lt v0, p1, :cond_0

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->protpos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setProtpos(I)V

    .line 56
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBspos()I

    move-result v0

    if-lt v0, p1, :cond_1

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->bspos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBspos(I)V

    .line 57
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getTabpos()I

    move-result v0

    if-lt v0, p1, :cond_2

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->tabpos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setTabpos(I)V

    .line 58
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getFontpos()I

    move-result v0

    if-lt v0, p1, :cond_3

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->fontpos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setFontpos(I)V

    .line 59
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v0

    if-lt v0, p1, :cond_4

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->xfpos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setXfpos(I)V

    .line 60
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBackuppos()I

    move-result v0

    if-lt v0, p1, :cond_5

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->backuppos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBackuppos(I)V

    .line 61
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getNamepos()I

    move-result v0

    if-lt v0, p1, :cond_6

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->namepos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setNamepos(I)V

    .line 62
    :cond_6
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getSupbookpos()I

    move-result v0

    if-lt v0, p1, :cond_7

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->supbookpos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setSupbookpos(I)V

    .line 63
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getPalettepos()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getPalettepos()I

    move-result v0

    if-lt v0, p1, :cond_8

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->palettepos:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setPalettepos(I)V

    .line 64
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getExternsheetPos()I

    move-result v0

    if-lt v0, p1, :cond_9

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getExternsheetPos()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setExternsheetPos(I)V

    .line 65
    :cond_9
    return-void
.end method

.method public get(I)Lorg/apache/poi/hssf/record/Record;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/Record;

    return-object v0
.end method

.method public getBackuppos()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->backuppos:I

    return v0
.end method

.method public getBspos()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->bspos:I

    return v0
.end method

.method public getExternsheetPos()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->externsheetPos:I

    return v0
.end method

.method public getFontpos()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->fontpos:I

    return v0
.end method

.method public getNamepos()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->namepos:I

    return v0
.end method

.method public getPalettepos()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->palettepos:I

    return v0
.end method

.method public getProtpos()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->protpos:I

    return v0
.end method

.method public getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    return-object v0
.end method

.method public getSupbookpos()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->supbookpos:I

    return v0
.end method

.method public getTabpos()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->tabpos:I

    return v0
.end method

.method public getXfpos()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->xfpos:I

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 83
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getProtpos()I

    move-result v0

    if-lt v0, p1, :cond_0

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->protpos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setProtpos(I)V

    .line 84
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBspos()I

    move-result v0

    if-lt v0, p1, :cond_1

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->bspos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBspos(I)V

    .line 85
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getTabpos()I

    move-result v0

    if-lt v0, p1, :cond_2

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->tabpos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setTabpos(I)V

    .line 86
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getFontpos()I

    move-result v0

    if-lt v0, p1, :cond_3

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->fontpos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setFontpos(I)V

    .line 87
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v0

    if-lt v0, p1, :cond_4

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->xfpos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setXfpos(I)V

    .line 88
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBackuppos()I

    move-result v0

    if-lt v0, p1, :cond_5

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->backuppos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBackuppos(I)V

    .line 89
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getNamepos()I

    move-result v0

    if-lt v0, p1, :cond_6

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getNamepos()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setNamepos(I)V

    .line 90
    :cond_6
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getSupbookpos()I

    move-result v0

    if-lt v0, p1, :cond_7

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getSupbookpos()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setSupbookpos(I)V

    .line 91
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getPalettepos()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getPalettepos()I

    move-result v0

    if-lt v0, p1, :cond_8

    iget v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->palettepos:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setPalettepos(I)V

    .line 92
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getExternsheetPos()I

    move-result v0

    if-lt v0, p1, :cond_9

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getExternsheetPos()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setExternsheetPos(I)V

    .line 93
    :cond_9
    return-void
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 2
    .param p1, "record"    # Ljava/lang/Object;

    .prologue
    .line 76
    iget-object v1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 77
    .local v0, "i":I
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(I)V

    .line 78
    return-void
.end method

.method public setBackuppos(I)V
    .locals 0
    .param p1, "backuppos"    # I

    .prologue
    .line 140
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->backuppos:I

    .line 141
    return-void
.end method

.method public setBspos(I)V
    .locals 0
    .param p1, "bspos"    # I

    .prologue
    .line 108
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->bspos:I

    .line 109
    return-void
.end method

.method public setExternsheetPos(I)V
    .locals 0
    .param p1, "externsheetPos"    # I

    .prologue
    .line 197
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->externsheetPos:I

    .line 198
    return-void
.end method

.method public setFontpos(I)V
    .locals 0
    .param p1, "fontpos"    # I

    .prologue
    .line 124
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->fontpos:I

    .line 125
    return-void
.end method

.method public setNamepos(I)V
    .locals 0
    .param p1, "namepos"    # I

    .prologue
    .line 173
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->namepos:I

    .line 174
    return-void
.end method

.method public setPalettepos(I)V
    .locals 0
    .param p1, "palettepos"    # I

    .prologue
    .line 148
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->palettepos:I

    .line 149
    return-void
.end method

.method public setProtpos(I)V
    .locals 0
    .param p1, "protpos"    # I

    .prologue
    .line 100
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->protpos:I

    .line 101
    return-void
.end method

.method public setRecords(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    iput-object p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    .line 43
    return-void
.end method

.method public setSupbookpos(I)V
    .locals 0
    .param p1, "supbookpos"    # I

    .prologue
    .line 181
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->supbookpos:I

    .line 182
    return-void
.end method

.method public setTabpos(I)V
    .locals 0
    .param p1, "tabpos"    # I

    .prologue
    .line 116
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->tabpos:I

    .line 117
    return-void
.end method

.method public setXfpos(I)V
    .locals 0
    .param p1, "xfpos"    # I

    .prologue
    .line 132
    iput p1, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->xfpos:I

    .line 133
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/poi/hssf/model/WorkbookRecordList;->records:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
