.class public Lorg/apache/poi/hssf/converter/ExcelToFoConverter;
.super Lorg/apache/poi/hssf/converter/AbstractExcelConverter;
.source "ExcelToFoConverter.java"


# static fields
.field private static final CM_PER_INCH:F = 2.54f

.field private static final DPI:F = 72.0f

.field private static final PAPER_A4_HEIGHT_INCHES:F = 11.574803f

.field private static final PAPER_A4_WIDTH_INCHES:F = 8.267716f

.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

.field private pageMarginInches:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 69
    sput-object v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/converter/FoDocumentFacade;)V
    .locals 1
    .param p1, "foDocumentFacade"    # Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .prologue
    .line 155
    invoke-direct {p0}, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;-><init>()V

    .line 148
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->pageMarginInches:F

    .line 157
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 158
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 1
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 150
    invoke-direct {p0}, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;-><init>()V

    .line 148
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->pageMarginInches:F

    .line 152
    new-instance v0, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;-><init>(Lorg/w3c/dom/Document;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 153
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 12
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 87
    array-length v8, p0

    const/4 v9, 0x2

    if-ge v8, v9, :cond_1

    .line 89
    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 90
    const-string/jumbo v9, "Usage: ExcelToFoConverter <inputFile.xls> <saveTo.xml>"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    const/4 v3, 0x0

    .line 100
    .local v3, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v8, Ljava/io/File;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->process(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 102
    .local v0, "doc":Lorg/w3c/dom/Document;
    new-instance v4, Ljava/io/FileWriter;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v4, v8}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    .end local v3    # "out":Ljava/io/FileWriter;
    .local v4, "out":Ljava/io/FileWriter;
    :try_start_1
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v0}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 104
    .local v1, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v6, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v6, v4}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 106
    .local v6, "streamResult":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v7

    .line 107
    .local v7, "tf":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v7}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v5

    .line 109
    .local v5, "serializer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v8, "encoding"

    const-string/jumbo v9, "UTF-8"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string/jumbo v8, "indent"

    const-string/jumbo v9, "no"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string/jumbo v8, "method"

    const-string/jumbo v9, "xml"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v5, v1, v6}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    .line 113
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 120
    if-eqz v4, :cond_3

    .line 122
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 123
    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 115
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_0
    move-exception v2

    .line 117
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 120
    if-eqz v3, :cond_0

    .line 122
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 123
    :catch_1
    move-exception v2

    .line 124
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 119
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 120
    :goto_2
    if-eqz v3, :cond_2

    .line 122
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 126
    :cond_2
    :goto_3
    throw v8

    .line 123
    :catch_2
    move-exception v2

    .line 124
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 123
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .restart local v4    # "out":Ljava/io/FileWriter;
    .restart local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .restart local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .restart local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_3
    move-exception v2

    .line 124
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_0

    .line 119
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v3    # "out":Ljava/io/FileWriter;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .line 115
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_1
.end method

.method public static process(Ljava/io/File;)Lorg/w3c/dom/Document;
    .locals 3
    .param p0, "xlsFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->loadXls(Ljava/io/File;)Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    .line 139
    .local v1, "workbook":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    new-instance v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;

    .line 140
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 141
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    .line 139
    invoke-direct {v0, v2}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;-><init>(Lorg/w3c/dom/Document;)V

    .line 142
    .local v0, "excelToHtmlConverter":Lorg/apache/poi/hssf/converter/ExcelToFoConverter;
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processWorkbook(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 143
    invoke-virtual {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    return-object v2
.end method

.method private setBlockProperties(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)V
    .locals 2
    .param p1, "textBlock"    # Lorg/w3c/dom/Element;
    .param p2, "triplet"    # Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    .prologue
    .line 833
    iget-boolean v0, p2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    if-eqz v0, :cond_0

    .line 834
    const-string/jumbo v0, "font-weight"

    const-string/jumbo v1, "bold"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    :cond_0
    iget-boolean v0, p2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    if-eqz v0, :cond_1

    .line 837
    const-string/jumbo v0, "font-style"

    const-string/jumbo v1, "italic"

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    :cond_1
    iget-object v0, p2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 840
    const-string/jumbo v0, "font-family"

    iget-object v1, p2, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    :cond_2
    return-void
.end method


# virtual methods
.method protected createPageMaster(FLjava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "tableWidthIn"    # F
    .param p2, "pageMasterName"    # Ljava/lang/String;

    .prologue
    .line 165
    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getPageMarginInches()F

    move-result v10

    mul-float/2addr v9, v10

    add-float v6, p1, v9

    .line 167
    .local v6, "requiredWidthIn":F
    const v9, 0x41044891

    cmpg-float v9, v6, v9

    if-gez v9, :cond_0

    .line 170
    const v4, 0x41044891

    .line 171
    .local v4, "paperWidthIn":F
    const v3, 0x41393265

    .line 182
    .local v3, "paperHeightIn":F
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getPageMarginInches()F

    move-result v1

    .line 183
    .local v1, "leftMargin":F
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getPageMarginInches()F

    move-result v7

    .line 184
    .local v7, "rightMargin":F
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getPageMarginInches()F

    move-result v8

    .line 185
    .local v8, "topMargin":F
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getPageMarginInches()F

    move-result v0

    .line 187
    .local v0, "bottomMargin":F
    iget-object v9, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 188
    invoke-virtual {v9, p2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addSimplePageMaster(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 189
    .local v2, "pageMaster":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "page-height"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "in"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v2, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string/jumbo v9, "page-width"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "in"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v2, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v9, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v9, v2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addRegionBody(Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 193
    .local v5, "regionBody":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "margin"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 194
    const-string/jumbo v11, "in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "in"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 193
    invoke-interface {v5, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-object p2

    .line 176
    .end local v0    # "bottomMargin":F
    .end local v1    # "leftMargin":F
    .end local v2    # "pageMaster":Lorg/w3c/dom/Element;
    .end local v3    # "paperHeightIn":F
    .end local v4    # "paperWidthIn":F
    .end local v5    # "regionBody":Lorg/w3c/dom/Element;
    .end local v7    # "rightMargin":F
    .end local v8    # "topMargin":F
    :cond_0
    move v4, v6

    .line 178
    .restart local v4    # "paperWidthIn":F
    const v9, 0x3f36db6d

    .line 177
    mul-float v3, v4, v9

    .restart local v3    # "paperHeightIn":F
    goto/16 :goto_0
.end method

.method protected getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method public getPageMarginInches()F
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->pageMarginInches:F

    return v0
.end method

.method protected isEmptyStyle(Lorg/apache/poi/ss/usermodel/CellStyle;)Z
    .locals 1
    .param p1, "cellStyle"    # Lorg/apache/poi/ss/usermodel/CellStyle;

    .prologue
    .line 219
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/CellStyle;->getFillPattern()S

    move-result v0

    if-nez v0, :cond_0

    .line 220
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderTop()S

    move-result v0

    if-nez v0, :cond_0

    .line 221
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderRight()S

    move-result v0

    if-nez v0, :cond_0

    .line 222
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderBottom()S

    move-result v0

    if-nez v0, :cond_0

    .line 223
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/CellStyle;->getBorderLeft()S

    move-result v0

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected processCell(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCell;Lorg/w3c/dom/Element;IIF)Z
    .locals 20
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "cell"    # Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .param p3, "tableCellElement"    # Lorg/w3c/dom/Element;
    .param p4, "normalWidthPx"    # I
    .param p5, "maxSpannedWidthPx"    # I
    .param p6, "normalHeightPt"    # F

    .prologue
    .line 230
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v7

    .line 233
    .local v7, "cellStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v15

    packed-switch v15, :pswitch_data_0

    .line 294
    sget-object v15, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/16 v16, 0x5

    .line 295
    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "Unexpected cell type ("

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 294
    invoke-virtual/range {v15 .. v17}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 296
    const/4 v15, 0x1

    .line 360
    :goto_0
    return v15

    .line 237
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v15

    invoke-virtual {v15}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v13

    .line 299
    .local v13, "value":Ljava/lang/String;
    :goto_1
    invoke-static {v13}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v9

    .line 300
    .local v9, "noText":Z
    if-nez v9, :cond_8

    invoke-virtual {v7}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getWrapText()Z

    move-result v15

    if-nez v15, :cond_8

    const/4 v14, 0x1

    .line 302
    .local v14, "wrapInDivs":Z
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isEmptyStyle(Lorg/apache/poi/ss/usermodel/CellStyle;)Z

    move-result v8

    .line 303
    .local v8, "emptyStyle":Z
    if-nez v8, :cond_0

    .line 305
    if-eqz v9, :cond_0

    .line 312
    const-string/jumbo v13, "\u00a0"

    .line 316
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputLeadingSpacesAsNonBreaking()Z

    move-result v15

    if-eqz v15, :cond_3

    const-string/jumbo v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 318
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v5, "builder":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .local v6, "c":I
    :goto_3
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    if-lt v6, v15, :cond_9

    .line 326
    :cond_1
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v16

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    .line 327
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v15

    invoke-virtual {v13, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 332
    .end local v5    # "builder":Ljava/lang/StringBuilder;
    .end local v6    # "c":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v15, v13}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v12

    .line 333
    .local v12, "text":Lorg/w3c/dom/Text;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v15}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 335
    .local v4, "block":Lorg/w3c/dom/Element;
    if-eqz v14, :cond_5

    .line 337
    const-string/jumbo v15, "absolute-position"

    const-string/jumbo v16, "fixed"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const-string/jumbo v15, "left"

    const-string/jumbo v16, "0px"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string/jumbo v15, "top"

    const-string/jumbo v16, "0px"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string/jumbo v15, "bottom"

    const-string/jumbo v16, "0px"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const-string/jumbo v15, "min-width"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, "px"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const v15, 0x7fffffff

    move/from16 v0, p5

    if-eq v0, v15, :cond_4

    .line 345
    const-string/jumbo v15, "max-width"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, "px"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_4
    const-string/jumbo v15, "overflow"

    const-string/jumbo v16, "hidden"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const-string/jumbo v15, "height"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, "pt"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string/jumbo v15, "keep-together.within-line"

    const-string/jumbo v16, "always"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string/jumbo v15, "wrap-option"

    const-string/jumbo v16, "no-wrap"

    move-object/from16 v0, v16

    invoke-interface {v4, v15, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v15, v2, v4}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCellStyle(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;Lorg/w3c/dom/Element;Lorg/w3c/dom/Element;)V

    .line 357
    invoke-interface {v4, v12}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 358
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 360
    invoke-static {v13}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    if-eqz v8, :cond_a

    const/4 v15, 0x1

    goto/16 :goto_0

    .line 240
    .end local v4    # "block":Lorg/w3c/dom/Element;
    .end local v8    # "emptyStyle":Z
    .end local v9    # "noText":Z
    .end local v12    # "text":Lorg/w3c/dom/Text;
    .end local v13    # "value":Ljava/lang/String;
    .end local v14    # "wrapInDivs":Z
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCachedFormulaResultType()I

    move-result v15

    packed-switch v15, :pswitch_data_1

    .line 273
    :pswitch_2
    sget-object v15, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 274
    const/16 v16, 0x5

    .line 275
    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "Unexpected cell cachedFormulaResultType ("

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 276
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCachedFormulaResultType()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 275
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 273
    invoke-virtual/range {v15 .. v17}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 277
    const-string/jumbo v13, ""

    .line 280
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 243
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v10

    .line 244
    .local v10, "str":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    if-eqz v10, :cond_6

    invoke-virtual {v10}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v15

    if-lez v15, :cond_6

    .line 246
    invoke-virtual {v10}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->toString()Ljava/lang/String;

    move-result-object v13

    .line 247
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 250
    .end local v13    # "value":Ljava/lang/String;
    :cond_6
    const-string/jumbo v13, ""

    .line 252
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 254
    .end local v10    # "str":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_4
    move-object v11, v7

    .line 255
    .local v11, "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    if-nez v11, :cond_7

    .line 257
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getNumericCellValue()D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v13

    .line 258
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 261
    .end local v13    # "value":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    .line 262
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getNumericCellValue()D

    move-result-wide v16

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormat()S

    move-result v18

    .line 263
    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormatString()Ljava/lang/String;

    move-result-object v19

    .line 261
    invoke-virtual/range {v15 .. v19}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatRawCellContents(DILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 265
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 267
    .end local v11    # "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_5
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v13

    .line 268
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 270
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_6
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getErrorCellValue()B

    move-result v15

    invoke-static {v15}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->getText(I)Ljava/lang/String;

    move-result-object v13

    .line 271
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 282
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_7
    const-string/jumbo v13, ""

    .line 283
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 285
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatCellValue(Lorg/apache/poi/ss/usermodel/Cell;)Ljava/lang/String;

    move-result-object v13

    .line 286
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 288
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_9
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v13

    .line 289
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 291
    .end local v13    # "value":Ljava/lang/String;
    :pswitch_a
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getErrorCellValue()B

    move-result v15

    invoke-static {v15}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->getText(I)Ljava/lang/String;

    move-result-object v13

    .line 292
    .restart local v13    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 300
    .restart local v9    # "noText":Z
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 321
    .restart local v5    # "builder":Ljava/lang/StringBuilder;
    .restart local v6    # "c":I
    .restart local v8    # "emptyStyle":Z
    .restart local v14    # "wrapInDivs":Z
    :cond_9
    invoke-virtual {v13, v6}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_1

    .line 323
    const/16 v15, 0xa0

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 319
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    .line 360
    .end local v5    # "builder":Ljava/lang/StringBuilder;
    .end local v6    # "c":I
    .restart local v4    # "block":Lorg/w3c/dom/Element;
    .restart local v12    # "text":Lorg/w3c/dom/Text;
    :cond_a
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 240
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected processCellStyle(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;Lorg/w3c/dom/Element;Lorg/w3c/dom/Element;)V
    .locals 10
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "cellStyle"    # Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .param p3, "cellTarget"    # Lorg/w3c/dom/Element;
    .param p4, "blockTarget"    # Lorg/w3c/dom/Element;

    .prologue
    .line 366
    const-string/jumbo v0, "white-space-collapse"

    const-string/jumbo v1, "false"

    invoke-interface {p4, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getAlignment()S

    move-result v0

    .line 368
    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->getAlign(S)Ljava/lang/String;

    move-result-object v9

    .line 370
    .local v9, "textAlign":Ljava/lang/String;
    invoke-static {v9}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    const-string/jumbo v0, "text-align"

    invoke-interface {p4, v0, v9}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillPattern()S

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillPattern()S

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 381
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillForegroundColorColor()Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v8

    .line 382
    .local v8, "foregroundColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v8, :cond_1

    .line 383
    const-string/jumbo v0, "background-color"

    .line 384
    invoke-static {v8}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v1

    .line 383
    invoke-interface {p3, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    .end local v8    # "foregroundColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :cond_1
    :goto_0
    const-string/jumbo v3, "top"

    .line 396
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderTop()S

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getTopBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    .line 395
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCellStyleBorder(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Ljava/lang/String;SS)V

    .line 397
    const-string/jumbo v3, "right"

    .line 398
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderRight()S

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getRightBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    .line 397
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCellStyleBorder(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Ljava/lang/String;SS)V

    .line 399
    const-string/jumbo v3, "bottom"

    .line 400
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderBottom()S

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBottomBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    .line 399
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCellStyleBorder(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Ljava/lang/String;SS)V

    .line 401
    const-string/jumbo v3, "left"

    .line 402
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderLeft()S

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getLeftBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    .line 401
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCellStyleBorder(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Ljava/lang/String;SS)V

    .line 404
    invoke-virtual {p2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFont(Lorg/apache/poi/ss/usermodel/Workbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v7

    .line 405
    .local v7, "font":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    invoke-virtual {p0, p1, p4, v7}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCellStyleFont(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Lorg/apache/poi/hssf/usermodel/HSSFFont;)V

    .line 407
    return-void

    .line 389
    .end local v7    # "font":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    :cond_2
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillBackgroundColorColor()Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v6

    .line 390
    .local v6, "backgroundColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v6, :cond_1

    .line 391
    const-string/jumbo v0, "background-color"

    .line 392
    invoke-static {v6}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v1

    .line 391
    invoke-interface {p3, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected processCellStyleBorder(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Ljava/lang/String;SS)V
    .locals 4
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "cellTarget"    # Lorg/w3c/dom/Element;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "xlsBorder"    # S
    .param p5, "borderColor"    # S

    .prologue
    const/16 v3, 0x20

    .line 412
    if-nez p4, :cond_0

    .line 430
    :goto_0
    return-void

    .line 415
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 416
    .local v0, "borderStyle":Ljava/lang/StringBuilder;
    invoke-static {p4}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getBorderWidth(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v2

    invoke-virtual {v2, p5}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v1

    .line 420
    .local v1, "color":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v1, :cond_1

    .line 422
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 423
    invoke-static {v1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 426
    invoke-static {p4}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getBorderStyle(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "border-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v2, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected processCellStyleFont(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/w3c/dom/Element;Lorg/apache/poi/hssf/usermodel/HSSFFont;)V
    .locals 5
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "blockTarget"    # Lorg/w3c/dom/Element;
    .param p3, "font"    # Lorg/apache/poi/hssf/usermodel/HSSFFont;

    .prologue
    const/4 v3, 0x1

    .line 435
    new-instance v1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    invoke-direct {v1}, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;-><init>()V

    .line 436
    .local v1, "triplet":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 438
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getBoldweight()S

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 448
    :goto_0
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getItalic()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    iput-boolean v3, v1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 453
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getFontReplacer()Lorg/apache/poi/hwpf/converter/FontReplacer;

    move-result-object v2

    invoke-interface {v2, v1}, Lorg/apache/poi/hwpf/converter/FontReplacer;->update(Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    .line 454
    invoke-direct {p0, p2, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->setBlockProperties(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)V

    .line 456
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v2

    .line 457
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getColor()S

    move-result v3

    .line 456
    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v0

    .line 458
    .local v0, "fontColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v0, :cond_1

    .line 459
    const-string/jumbo v2, "color"

    .line 460
    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v3

    .line 459
    invoke-interface {p2, v2, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_1
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeightInPoints()S

    move-result v2

    if-eqz v2, :cond_2

    .line 463
    const-string/jumbo v2, "font-size"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeightInPoints()S

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 464
    const-string/jumbo v4, "pt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 463
    invoke-interface {p2, v2, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :cond_2
    return-void

    .line 441
    .end local v0    # "fontColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :sswitch_0
    iput-boolean v3, v1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    goto :goto_0

    .line 444
    :sswitch_1
    const/4 v2, 0x0

    iput-boolean v2, v1, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    goto :goto_0

    .line 438
    nop

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_1
        0x2bc -> :sswitch_0
    .end sparse-switch
.end method

.method protected processColumnHeaders(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)V
    .locals 9
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "maxSheetColumns"    # I
    .param p3, "table"    # Lorg/w3c/dom/Element;

    .prologue
    .line 471
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableHeader()Lorg/w3c/dom/Element;

    move-result-object v5

    .line 472
    .local v5, "tableHeader":Lorg/w3c/dom/Element;
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 474
    .local v3, "row":Lorg/w3c/dom/Element;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputRowNumbers()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 477
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 478
    .local v4, "tableCellElement":Lorg/w3c/dom/Element;
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v7

    invoke-interface {v4, v7}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 479
    invoke-interface {v3, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 482
    .end local v4    # "tableCellElement":Lorg/w3c/dom/Element;
    :cond_0
    const/4 v1, 0x0

    .local v1, "c":I
    :goto_0
    if-lt v1, p2, :cond_1

    .line 499
    invoke-interface {v5, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 500
    invoke-interface {p3, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 501
    return-void

    .line 484
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputHiddenColumns()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 482
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 487
    :cond_2
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 488
    .local v2, "cell":Lorg/w3c/dom/Element;
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 489
    .local v0, "block":Lorg/w3c/dom/Element;
    const-string/jumbo v7, "text-align"

    const-string/jumbo v8, "center"

    invoke-interface {v0, v7, v8}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const-string/jumbo v7, "font-weight"

    const-string/jumbo v8, "bold"

    invoke-interface {v0, v7, v8}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getColumnName(I)Ljava/lang/String;

    move-result-object v6

    .line 493
    .local v6, "text":Ljava/lang/String;
    iget-object v7, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v7, v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v7

    invoke-interface {v0, v7}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 495
    invoke-interface {v2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 496
    invoke-interface {v3, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_1
.end method

.method protected processColumnWidths(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)F
    .locals 9
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "maxSheetColumns"    # I
    .param p3, "table"    # Lorg/w3c/dom/Element;

    .prologue
    const/high16 v8, 0x42900000    # 72.0f

    .line 512
    const/4 v4, 0x0

    .line 514
    .local v4, "tableWidth":F
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputRowNumbers()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 516
    invoke-static {p1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getDefaultColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)I

    move-result v5

    int-to-float v5, v5

    div-float v2, v5, v8

    .line 518
    .local v2, "columnWidthIn":F
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 519
    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableColumn()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 520
    .local v3, "rowNumberColumn":Lorg/w3c/dom/Element;
    const-string/jumbo v5, "column-width"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "in"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    invoke-interface {p3, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 523
    add-float/2addr v4, v2

    .line 526
    .end local v2    # "columnWidthIn":F
    .end local v3    # "rowNumberColumn":Lorg/w3c/dom/Element;
    :cond_0
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    if-lt v0, p2, :cond_1

    .line 540
    const-string/jumbo v5, "width"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "in"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p3, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    return v4

    .line 528
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputHiddenColumns()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 526
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 531
    :cond_2
    invoke-static {p1, v0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v5

    int-to-float v5, v5

    div-float v2, v5, v8

    .line 533
    .restart local v2    # "columnWidthIn":F
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableColumn()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 534
    .local v1, "col":Lorg/w3c/dom/Element;
    const-string/jumbo v5, "column-width"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "in"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-interface {p3, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 537
    add-float/2addr v4, v2

    goto :goto_1
.end method

.method protected processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
    .locals 2
    .param p1, "summaryInformation"    # Lorg/apache/poi/hpsf/SummaryInformation;

    .prologue
    .line 547
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setTitle(Ljava/lang/String;)V

    .line 550
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 551
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setCreator(Ljava/lang/String;)V

    .line 553
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 554
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setKeywords(Ljava/lang/String;)V

    .line 556
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToFoUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 557
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->setDescription(Ljava/lang/String;)V

    .line 558
    :cond_3
    return-void
.end method

.method protected processRow(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;[[Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFRow;Lorg/w3c/dom/Element;)I
    .locals 20
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "mergedRanges"    # [[Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p3, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .param p4, "tableRowElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 567
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v18

    .line 568
    .local v18, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getLastCellNum()S

    move-result v14

    .line 569
    .local v14, "maxColIx":S
    if-gtz v14, :cond_0

    .line 571
    const/4 v2, 0x0

    .line 670
    :goto_0
    return v2

    .line 574
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 576
    .local v12, "emptyCells":Ljava/util/List;, "Ljava/util/List<Lorg/w3c/dom/Element;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputRowNumbers()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 578
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processRowNumber(Lorg/apache/poi/hssf/usermodel/HSSFRow;)Lorg/w3c/dom/Element;

    move-result-object v19

    .line 579
    .local v19, "tableRowNumberCellElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 582
    .end local v19    # "tableRowNumberCellElement":Lorg/w3c/dom/Element;
    :cond_1
    const/4 v15, 0x0

    .line 583
    .local v15, "maxRenderedColumn":I
    const/4 v9, 0x0

    .local v9, "colIx":I
    :goto_1
    if-lt v9, v14, :cond_2

    .line 670
    add-int/lit8 v2, v15, 0x1

    goto :goto_0

    .line 585
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputHiddenColumns()Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 583
    :cond_3
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 589
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v2

    .line 588
    move-object/from16 v0, p2

    invoke-static {v0, v2, v9}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getMergedRange([[Lorg/apache/poi/ss/util/CellRangeAddress;II)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v17

    .line 591
    .local v17, "range":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-eqz v17, :cond_5

    .line 592
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v2

    if-ne v2, v9, :cond_3

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v2

    .line 593
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 596
    :cond_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v4

    .line 599
    .local v4, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    const/4 v7, 0x0

    .line 601
    .local v7, "divWidthPx":I
    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v7

    .line 603
    const/4 v13, 0x0

    .line 604
    .local v13, "hasBreaks":Z
    add-int/lit8 v16, v9, 0x1

    .local v16, "nextColumnIndex":I
    :goto_3
    move/from16 v0, v16

    if-lt v0, v14, :cond_9

    .line 620
    :goto_4
    if-nez v13, :cond_6

    .line 621
    const v7, 0x7fffffff

    .line 624
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v5

    .line 626
    .local v5, "tableCellElement":Lorg/w3c/dom/Element;
    if-eqz v17, :cond_8

    .line 628
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v2

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v3

    if-eq v2, v3, :cond_7

    .line 630
    const-string/jumbo v2, "number-columns-spanned"

    .line 631
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v3

    .line 632
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v6

    .line 631
    sub-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 629
    invoke-interface {v5, v2, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v2

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v3

    if-eq v2, v3, :cond_8

    .line 635
    const-string/jumbo v2, "number-rows-spanned"

    .line 636
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v3

    .line 637
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v6

    .line 636
    sub-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 634
    invoke-interface {v5, v2, v3}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :cond_8
    if-eqz v4, :cond_c

    .line 644
    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v6

    .line 645
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x41a00000    # 20.0f

    div-float v8, v2, v3

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 643
    invoke-virtual/range {v2 .. v8}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processCell(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCell;Lorg/w3c/dom/Element;IIF)Z

    move-result v10

    .line 653
    .local v10, "emptyCell":Z
    :goto_5
    if-eqz v10, :cond_d

    .line 655
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 606
    .end local v5    # "tableCellElement":Lorg/w3c/dom/Element;
    .end local v10    # "emptyCell":Z
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputHiddenColumns()Z

    move-result v2

    if-nez v2, :cond_a

    .line 607
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 604
    :goto_6
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    .line 610
    :cond_a
    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 611
    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isTextEmpty(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 613
    const/4 v13, 0x1

    .line 614
    goto/16 :goto_4

    .line 617
    :cond_b
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-static {v0, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v2

    add-int/2addr v7, v2

    goto :goto_6

    .line 649
    .restart local v5    # "tableCellElement":Lorg/w3c/dom/Element;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v2

    invoke-interface {v5, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 650
    const/4 v10, 0x1

    .restart local v10    # "emptyCell":Z
    goto :goto_5

    .line 659
    :cond_d
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_e

    .line 663
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 665
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 666
    move v15, v9

    goto/16 :goto_2

    .line 659
    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/Element;

    .line 661
    .local v11, "emptyCellElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p4

    invoke-interface {v0, v11}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_7
.end method

.method protected processRowNumber(Lorg/apache/poi/hssf/usermodel/HSSFRow;)Lorg/w3c/dom/Element;
    .locals 5
    .param p1, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .prologue
    .line 675
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 677
    .local v1, "tableRowNumberCellElement":Lorg/w3c/dom/Element;
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 678
    .local v0, "block":Lorg/w3c/dom/Element;
    const-string/jumbo v3, "text-align"

    const-string/jumbo v4, "right"

    invoke-interface {v0, v3, v4}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    const-string/jumbo v3, "font-weight"

    const-string/jumbo v4, "bold"

    invoke-interface {v0, v3, v4}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getRowName(Lorg/apache/poi/hssf/usermodel/HSSFRow;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v2

    .line 682
    .local v2, "text":Lorg/w3c/dom/Text;
    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 684
    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 685
    return-object v1
.end method

.method protected processSheet(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/w3c/dom/Element;)F
    .locals 20
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "flow"    # Lorg/w3c/dom/Element;

    .prologue
    .line 691
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getPhysicalNumberOfRows()I

    move-result v9

    .line 692
    .local v9, "physicalNumberOfRows":I
    if-gtz v9, :cond_0

    .line 693
    const/4 v15, 0x0

    .line 762
    :goto_0
    return v15

    .line 695
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processSheetName(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/w3c/dom/Element;)V

    .line 697
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTable()Lorg/w3c/dom/Element;

    move-result-object v12

    .line 698
    .local v12, "table":Lorg/w3c/dom/Element;
    const-string/jumbo v16, "table-layout"

    const-string/jumbo v17, "fixed"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v12, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableBody()Lorg/w3c/dom/Element;

    move-result-object v13

    .line 703
    .local v13, "tableBody":Lorg/w3c/dom/Element;
    invoke-static/range {p2 .. p2}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->buildMergedRangesMap(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[[Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v8

    .line 705
    .local v8, "mergedRanges":[[Lorg/apache/poi/ss/util/CellRangeAddress;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 707
    .local v5, "emptyRowElements":Ljava/util/List;, "Ljava/util/List<Lorg/w3c/dom/Element;>;"
    const/4 v7, 0x1

    .line 708
    .local v7, "maxSheetColumns":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getFirstRowNum()I

    move-result v10

    .local v10, "r":I
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v16

    move/from16 v0, v16

    if-le v10, v0, :cond_2

    .line 752
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v7, v12}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processColumnWidths(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)F

    move-result v15

    .line 754
    .local v15, "tableWidthIn":F
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputColumnHeaders()Z

    move-result v16

    if-eqz v16, :cond_1

    .line 756
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v7, v12}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processColumnHeaders(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)V

    .line 759
    :cond_1
    invoke-interface {v12, v13}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 760
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    .line 710
    .end local v15    # "tableWidthIn":F
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v11

    .line 712
    .local v11, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v11, :cond_4

    .line 708
    :cond_3
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 715
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->isOutputHiddenRows()Z

    move-result v16

    if-nez v16, :cond_5

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getZeroHeight()Z

    move-result v16

    if-nez v16, :cond_3

    .line 718
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v14

    .line 719
    .local v14, "tableRowElement":Lorg/w3c/dom/Element;
    const-string/jumbo v16, "height"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x41a00000    # 20.0f

    div-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 720
    const-string/jumbo v18, "pt"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 719
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v14, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8, v11, v14}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processRow(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;[[Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFRow;Lorg/w3c/dom/Element;)I

    move-result v6

    .line 725
    .local v6, "maxRowColumnNumber":I
    invoke-interface {v14}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v16

    if-nez v16, :cond_6

    .line 727
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 728
    .local v3, "emptyCellElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 729
    invoke-interface {v14, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 732
    .end local v3    # "emptyCellElement":Lorg/w3c/dom/Element;
    :cond_6
    if-nez v6, :cond_7

    .line 734
    invoke-interface {v5, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 749
    :goto_3
    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v7

    goto :goto_2

    .line 738
    :cond_7
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_8

    .line 740
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_9

    .line 744
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 747
    :cond_8
    invoke-interface {v13, v14}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_3

    .line 740
    :cond_9
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/w3c/dom/Element;

    .line 742
    .local v4, "emptyRowElement":Lorg/w3c/dom/Element;
    invoke-interface {v13, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_4
.end method

.method protected processSheet(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;I)Z
    .locals 7
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "sheetIndex"    # I

    .prologue
    .line 773
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "sheet-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 775
    .local v1, "pageMasterName":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 776
    invoke-virtual {v5, v1}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createPageSequence(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    .line 777
    .local v2, "pageSequence":Lorg/w3c/dom/Element;
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 778
    const-string/jumbo v6, "xsl-region-body"

    .line 777
    invoke-virtual {v5, v2, v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addFlowToPageSequence(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 780
    .local v0, "flow":Lorg/w3c/dom/Element;
    invoke-virtual {p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v3

    .line 781
    .local v3, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {p0, p1, v3, v0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processSheet(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/w3c/dom/Element;)F

    move-result v4

    .line 783
    .local v4, "tableWidthIn":F
    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-nez v5, :cond_0

    .line 784
    const/4 v5, 0x0

    .line 788
    :goto_0
    return v5

    .line 786
    :cond_0
    invoke-virtual {p0, v4, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->createPageMaster(FLjava/lang/String;)Ljava/lang/String;

    .line 787
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5, v2}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->addPageSequence(Lorg/w3c/dom/Element;)V

    .line 788
    const/4 v5, 0x1

    goto :goto_0
.end method

.method protected processSheetName(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/w3c/dom/Element;)V
    .locals 7
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "flow"    # Lorg/w3c/dom/Element;

    .prologue
    .line 793
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 795
    .local v0, "titleBlock":Lorg/w3c/dom/Element;
    new-instance v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    invoke-direct {v4}, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;-><init>()V

    .line 796
    .local v4, "triplet":Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;
    const/4 v5, 0x1

    iput-boolean v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->bold:Z

    .line 797
    const/4 v5, 0x0

    iput-boolean v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->italic:Z

    .line 798
    const-string/jumbo v5, "Arial"

    iput-object v5, v4, Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;->fontName:Ljava/lang/String;

    .line 799
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->getFontReplacer()Lorg/apache/poi/hwpf/converter/FontReplacer;

    move-result-object v5

    invoke-interface {v5, v4}, Lorg/apache/poi/hwpf/converter/FontReplacer;->update(Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;

    .line 801
    invoke-direct {p0, v0, v4}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->setBlockProperties(Lorg/w3c/dom/Element;Lorg/apache/poi/hwpf/converter/FontReplacer$Triplet;)V

    .line 802
    const-string/jumbo v5, "font-size"

    const-string/jumbo v6, "200%"

    invoke-interface {v0, v5, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 805
    .local v2, "titleInline":Lorg/w3c/dom/Element;
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    .line 806
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheetName()Ljava/lang/String;

    move-result-object v6

    .line 805
    invoke-virtual {v5, v6}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v5

    invoke-interface {v2, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 807
    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 808
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 810
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 811
    .local v1, "titleBlock2":Lorg/w3c/dom/Element;
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->foDocumentFacade:Lorg/apache/poi/hwpf/converter/FoDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/FoDocumentFacade;->createInline()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 812
    .local v3, "titleInline2":Lorg/w3c/dom/Element;
    invoke-interface {v1, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 813
    invoke-interface {p2, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 814
    return-void
.end method

.method public processWorkbook(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 3
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 818
    .line 819
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSummaryInformation()Lorg/apache/poi/hpsf/SummaryInformation;

    move-result-object v1

    .line 820
    .local v1, "summaryInformation":Lorg/apache/poi/hpsf/SummaryInformation;
    if-eqz v1, :cond_0

    .line 822
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V

    .line 825
    :cond_0
    const/4 v0, 0x0

    .local v0, "s":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 829
    return-void

    .line 827
    :cond_1
    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->processSheet(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;I)Z

    .line 825
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setPageMarginInches(F)V
    .locals 0
    .param p1, "pageMarginInches"    # F

    .prologue
    .line 845
    iput p1, p0, Lorg/apache/poi/hssf/converter/ExcelToFoConverter;->pageMarginInches:F

    .line 846
    return-void
.end method
