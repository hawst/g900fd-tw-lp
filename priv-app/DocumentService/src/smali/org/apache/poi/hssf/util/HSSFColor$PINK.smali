.class public final Lorg/apache/poi/hssf/util/HSSFColor$PINK;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PINK"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:0:FFFF"

.field public static final index:S = 0xes

.field public static final index2:S = 0x21s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 971
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 972
    aput-short v2, v0, v1

    const/4 v1, 0x2

    aput-short v2, v0, v1

    .line 970
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$PINK;->triplet:[S

    .line 974
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 965
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 988
    const-string/jumbo v0, "FFFF:0:FFFF"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 978
    const/16 v0, 0xe

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 983
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$PINK;->triplet:[S

    return-object v0
.end method
