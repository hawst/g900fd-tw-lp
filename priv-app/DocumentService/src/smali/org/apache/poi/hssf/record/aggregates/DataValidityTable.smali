.class public final Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;
.super Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;
.source "DataValidityTable.java"


# instance fields
.field private final _headerRec:Lorg/apache/poi/hssf/record/DVALRecord;

.field private final _validationList:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 52
    new-instance v0, Lorg/apache/poi/hssf/record/DVALRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/DVALRecord;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_headerRec:Lorg/apache/poi/hssf/record/DVALRecord;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/model/RecordStream;)V
    .locals 3
    .param p1, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 43
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/DVALRecord;

    iput-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_headerRec:Lorg/apache/poi/hssf/record/DVALRecord;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v0, "temp":Ljava/util/List;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lorg/apache/poi/hssf/record/DVRecord;

    if-eq v1, v2, :cond_0

    .line 48
    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 49
    return-void

    .line 46
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public addDataValidation(Lorg/apache/poi/hssf/record/DVRecord;)V
    .locals 2
    .param p1, "dvRecord"    # Lorg/apache/poi/hssf/record/DVRecord;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_headerRec:Lorg/apache/poi/hssf/record/DVALRecord;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/DVALRecord;->setDVRecNo(I)V

    .line 69
    return-void
.end method

.method public visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 2
    .param p1, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    .line 57
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    return-void

    .line 60
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_headerRec:Lorg/apache/poi/hssf/record/DVALRecord;

    invoke-interface {p1, v1}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 61
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 62
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/Record;

    invoke-interface {p1, v1}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
