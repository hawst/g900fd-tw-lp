.class public final Lorg/apache/poi/hssf/util/HSSFColor$AUTOMATIC;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AUTOMATIC"
.end annotation


# static fields
.field public static final index:S = 0x40s

.field private static instance:Lorg/apache/poi/hssf/util/HSSFColor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1676
    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$AUTOMATIC;

    invoke-direct {v0}, Lorg/apache/poi/hssf/util/HSSFColor$AUTOMATIC;-><init>()V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$AUTOMATIC;->instance:Lorg/apache/poi/hssf/util/HSSFColor;

    .line 1678
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1674
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/apache/poi/hssf/util/HSSFColor;
    .locals 1

    .prologue
    .line 1696
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$AUTOMATIC;->instance:Lorg/apache/poi/hssf/util/HSSFColor;

    return-object v0
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1692
    const-string/jumbo v0, "0:0:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1682
    const/16 v0, 0x40

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1687
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLACK;->triplet:[S

    return-object v0
.end method
