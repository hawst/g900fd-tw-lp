.class public Lorg/apache/poi/hssf/dev/ReSave;
.super Ljava/lang/Object;
.source "ReSave.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 11
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v2, 0x0

    .line 37
    .local v2, "initDrawing":Z
    array-length v8, p0

    const/4 v7, 0x0

    :goto_0
    if-lt v7, v8, :cond_0

    .line 66
    return-void

    .line 37
    :cond_0
    aget-object v0, p0, v7

    .line 38
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v9, "-dg"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 39
    const/4 v2, 0x1

    .line 37
    :cond_1
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 42
    :cond_2
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 43
    .local v3, "is":Ljava/io/FileInputStream;
    new-instance v6, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v6, v3}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Ljava/io/InputStream;)V

    .line 44
    .local v6, "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 47
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v9

    if-lt v1, v9, :cond_3

    .line 54
    const-string/jumbo v9, ".xls"

    const-string/jumbo v10, "-saved.xls"

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "outputFile":Ljava/lang/String;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 58
    .local v4, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {v6, v4}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->write(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    if-eqz v4, :cond_1

    .line 61
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    .line 48
    .end local v4    # "out":Ljava/io/FileOutputStream;
    .end local v5    # "outputFile":Ljava/lang/String;
    :cond_3
    invoke-virtual {v6, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 59
    .restart local v4    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "outputFile":Ljava/lang/String;
    :catchall_0
    move-exception v7

    .line 60
    if-eqz v4, :cond_4

    .line 61
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 62
    :cond_4
    throw v7
.end method
