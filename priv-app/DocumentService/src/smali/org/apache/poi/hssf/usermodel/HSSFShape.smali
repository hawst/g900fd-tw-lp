.class public abstract Lorg/apache/poi/hssf/usermodel/HSSFShape;
.super Ljava/lang/Object;
.source "HSSFShape.java"


# static fields
.field public static final FILL__FILLCOLOR_DEFAULT:I = 0x8000009

.field public static final LINESTYLE_DASHDOTDOTSYS:I = 0x4

.field public static final LINESTYLE_DASHDOTGEL:I = 0x8

.field public static final LINESTYLE_DASHDOTSYS:I = 0x3

.field public static final LINESTYLE_DASHGEL:I = 0x6

.field public static final LINESTYLE_DASHSYS:I = 0x1

.field public static final LINESTYLE_DEFAULT:I = -0x1

.field public static final LINESTYLE_DOTGEL:I = 0x5

.field public static final LINESTYLE_DOTSYS:I = 0x2

.field public static final LINESTYLE_LONGDASHDOTDOTGEL:I = 0xa

.field public static final LINESTYLE_LONGDASHDOTGEL:I = 0x9

.field public static final LINESTYLE_LONGDASHGEL:I = 0x7

.field public static final LINESTYLE_NONE:I = -0x1

.field public static final LINESTYLE_SOLID:I = 0x0

.field public static final LINESTYLE__COLOR_DEFAULT:I = 0x8000040

.field public static final LINEWIDTH_DEFAULT:I = 0x2535

.field public static final LINEWIDTH_ONE_PT:I = 0x319c

.field public static final NO_FILLHITTEST_FALSE:I = 0x10000

.field public static final NO_FILLHITTEST_TRUE:I = 0x110000

.field public static final NO_FILL_DEFAULT:Z = true


# instance fields
.field private final _escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

.field private final _objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

.field private final _optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

.field private _patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

.field anchor:Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

.field private parent:Lorg/apache/poi/hssf/usermodel/HSSFShape;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V
    .locals 1
    .param p1, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "objRecord"    # Lorg/apache/poi/hssf/record/ObjRecord;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 78
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

    .line 79
    const/16 v0, -0xff5

    invoke-virtual {p1, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 80
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/HSSFAnchor;->createAnchorFromEscher(Lorg/apache/poi/ddf/EscherContainerRecord;)Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->anchor:Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V
    .locals 2
    .param p1, "parent"    # Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .param p2, "anchor"    # Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->parent:Lorg/apache/poi/hssf/usermodel/HSSFShape;

    .line 88
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->anchor:Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .line 89
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->createSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 90
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v1, -0xff5

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    .line 91
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->createObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

    .line 93
    return-void
.end method


# virtual methods
.method abstract afterInsert(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V
.end method

.method protected abstract afterRemove(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V
.end method

.method protected abstract cloneShape()Lorg/apache/poi/hssf/usermodel/HSSFShape;
.end method

.method public countOfAllChildren()I
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract createObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;
.end method

.method protected abstract createSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
.end method

.method public getAnchor()Lorg/apache/poi/hssf/usermodel/HSSFAnchor;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->anchor:Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    return-object v0
.end method

.method protected getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getFillColor()I
    .locals 3

    .prologue
    .line 237
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v2, 0x181

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRGBProperty;

    .line 238
    .local v0, "rgbProperty":Lorg/apache/poi/ddf/EscherRGBProperty;
    if-nez v0, :cond_0

    const v1, 0x8000009

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRGBProperty;->getRgbColor()I

    move-result v1

    goto :goto_0
.end method

.method public getLineStyle()I
    .locals 3

    .prologue
    .line 278
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v2, 0x1ce

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 279
    .local v0, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v0, :cond_0

    .line 280
    const/4 v1, -0x1

    .line 282
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    goto :goto_0
.end method

.method public getLineStyleColor()I
    .locals 3

    .prologue
    .line 214
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v2, 0x1c0

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRGBProperty;

    .line 215
    .local v0, "rgbProperty":Lorg/apache/poi/ddf/EscherRGBProperty;
    if-nez v0, :cond_0

    const v1, 0x8000040

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRGBProperty;->getRgbColor()I

    move-result v1

    goto :goto_0
.end method

.method public getLineWidth()I
    .locals 3

    .prologue
    .line 260
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v2, 0x1cb

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 261
    .local v0, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v0, :cond_0

    const/16 v1, 0x2535

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    goto :goto_0
.end method

.method protected getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

    return-object v0
.end method

.method protected getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    return-object v0
.end method

.method public getParent()Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->parent:Lorg/apache/poi/hssf/usermodel/HSSFShape;

    return-object v0
.end method

.method public getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    return-object v0
.end method

.method public getRotationDegree()I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 379
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 380
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 381
    .local v2, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v2, :cond_0

    .line 389
    :goto_0
    return v3

    .line 385
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v4

    invoke-static {v4, v0}, Lorg/apache/poi/util/LittleEndian;->putInt(ILjava/io/OutputStream;)V

    .line 386
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 387
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Exception: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method getShapeId()I
    .locals 2

    .prologue
    const/16 v1, -0xff6

    .line 125
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    if-nez v0, :cond_0

    .line 127
    const/4 v0, -0x1

    .line 128
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 129
    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v0

    goto :goto_0
.end method

.method public isFlipHorizontal()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 368
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 369
    const/16 v3, -0xff6

    .line 368
    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 370
    .local v0, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    if-nez v0, :cond_1

    .line 372
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v2

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isFlipVertical()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 357
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 358
    const/16 v3, -0xff6

    .line 357
    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 359
    .local v0, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    if-nez v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v2

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isNoFill()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 306
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    const/16 v3, 0x1bf

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherBoolProperty;

    .line 307
    .local v0, "property":Lorg/apache/poi/ddf/EscherBoolProperty;
    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBoolProperty;->getPropertyValue()I

    move-result v2

    const/high16 v3, 0x110000

    if-eq v2, v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAnchor(Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V
    .locals 7
    .param p1, "anchor"    # Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .prologue
    const/16 v6, -0xff0

    const/16 v5, -0xff1

    .line 171
    const/4 v1, 0x0

    .line 172
    .local v1, "i":I
    const/4 v2, -0x1

    .line 173
    .local v2, "recordId":I
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->parent:Lorg/apache/poi/hssf/usermodel/HSSFShape;

    if-nez v3, :cond_4

    .line 174
    instance-of v3, p1, Lorg/apache/poi/hssf/usermodel/HSSFChildAnchor;

    if-eqz v3, :cond_0

    .line 175
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Must use client anchors for shapes directly attached to sheet."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 176
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 177
    .local v0, "anch":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    if-eqz v0, :cond_1

    .line 178
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 185
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->removeChildRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    .line 202
    .end local v0    # "anch":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    :cond_1
    :goto_1
    const/4 v3, -0x1

    if-ne v3, v2, :cond_8

    .line 203
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFAnchor;->getEscherAnchor()Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 207
    :goto_2
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->anchor:Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .line 208
    return-void

    .line 179
    .restart local v0    # "anch":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v3

    if-ne v3, v6, :cond_3

    .line 180
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_3

    .line 181
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v2

    .line 178
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "anch":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    :cond_4
    instance-of v3, p1, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    if-eqz v3, :cond_5

    .line 189
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Must use child anchors for shapes attached to groups."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 190
    :cond_5
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherChildAnchorRecord;

    .line 191
    .local v0, "anch":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    if-eqz v0, :cond_1

    .line 192
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_6

    .line 199
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->removeChildRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    goto :goto_1

    .line 193
    :cond_6
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v3

    if-ne v3, v5, :cond_7

    .line 194
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_7

    .line 195
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v2

    .line 192
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 205
    .end local v0    # "anch":Lorg/apache/poi/ddf/EscherChildAnchorRecord;
    :cond_8
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFAnchor;->getEscherAnchor()Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildBefore(Lorg/apache/poi/ddf/EscherRecord;I)V

    goto/16 :goto_2
.end method

.method public setFillColor(I)V
    .locals 2
    .param p1, "fillColor"    # I

    .prologue
    .line 245
    new-instance v0, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v1, 0x181

    invoke-direct {v0, v1, p1}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 246
    return-void
.end method

.method public setFillColor(III)V
    .locals 3
    .param p1, "red"    # I
    .param p2, "green"    # I
    .param p3, "blue"    # I

    .prologue
    .line 252
    shl-int/lit8 v1, p3, 0x10

    shl-int/lit8 v2, p2, 0x8

    or-int/2addr v1, v2

    or-int v0, v1, p1

    .line 253
    .local v0, "fillColor":I
    new-instance v1, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v2, 0x181

    invoke-direct {v1, v2, v0}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 254
    return-void
.end method

.method public setFlipHorizontal(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 341
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 342
    const/16 v2, -0xff6

    .line 341
    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 343
    .local v0, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    if-nez v0, :cond_0

    .line 351
    :goto_0
    return-void

    .line 345
    :cond_0
    if-eqz p1, :cond_1

    .line 346
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x40

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    goto :goto_0

    .line 348
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v1

    .line 349
    const v2, 0x7fffffbf

    and-int/2addr v1, v2

    .line 348
    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    goto :goto_0
.end method

.method public setFlipVertical(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 325
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 326
    const/16 v2, -0xff6

    .line 325
    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 327
    .local v0, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    if-nez v0, :cond_0

    .line 335
    :goto_0
    return-void

    .line 329
    :cond_0
    if-eqz p1, :cond_1

    .line 330
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v1

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    goto :goto_0

    .line 332
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSpRecord;->getFlags()I

    move-result v1

    .line 333
    const v2, 0x7fffff7f

    and-int/2addr v1, v2

    .line 332
    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    goto :goto_0
.end method

.method public setLineStyle(I)V
    .locals 4
    .param p1, "lineStyle"    # I

    .prologue
    const/16 v3, 0x1ff

    .line 291
    new-instance v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v1, 0x1ce

    invoke-direct {v0, v1, p1}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 292
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getLineStyle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    new-instance v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v1, 0x1d7

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 294
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->getLineStyle()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 295
    new-instance v0, Lorg/apache/poi/ddf/EscherBoolProperty;

    const/high16 v1, 0x80000

    invoke-direct {v0, v3, v1}, Lorg/apache/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    new-instance v0, Lorg/apache/poi/ddf/EscherBoolProperty;

    const v1, 0x80008

    invoke-direct {v0, v3, v1}, Lorg/apache/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    goto :goto_0
.end method

.method public setLineStyleColor(I)V
    .locals 2
    .param p1, "lineStyleColor"    # I

    .prologue
    .line 222
    new-instance v0, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v1, 0x1c0

    invoke-direct {v0, v1, p1}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 223
    return-void
.end method

.method public setLineStyleColor(III)V
    .locals 3
    .param p1, "red"    # I
    .param p2, "green"    # I
    .param p3, "blue"    # I

    .prologue
    .line 229
    shl-int/lit8 v1, p3, 0x10

    shl-int/lit8 v2, p2, 0x8

    or-int/2addr v1, v2

    or-int v0, v1, p1

    .line 230
    .local v0, "lineStyleColor":I
    new-instance v1, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v2, 0x1c0

    invoke-direct {v1, v2, v0}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 231
    return-void
.end method

.method public setLineWidth(I)V
    .locals 2
    .param p1, "lineWidth"    # I

    .prologue
    .line 271
    new-instance v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v1, 0x1cb

    invoke-direct {v0, v1, p1}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 272
    return-void
.end method

.method public setNoFill(Z)V
    .locals 3
    .param p1, "noFill"    # Z

    .prologue
    .line 314
    new-instance v1, Lorg/apache/poi/ddf/EscherBoolProperty;

    const/16 v2, 0x1bf

    if-eqz p1, :cond_0

    const/high16 v0, 0x110000

    :goto_0
    invoke-direct {v1, v2, v0}, Lorg/apache/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 315
    return-void

    .line 314
    :cond_0
    const/high16 v0, 0x10000

    goto :goto_0
.end method

.method protected setParent(Lorg/apache/poi/hssf/usermodel/HSSFShape;)V
    .locals 0
    .param p1, "parent"    # Lorg/apache/poi/hssf/usermodel/HSSFShape;

    .prologue
    .line 423
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->parent:Lorg/apache/poi/hssf/usermodel/HSSFShape;

    .line 424
    return-void
.end method

.method protected setPatriarch(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V
    .locals 0
    .param p1, "_patriarch"    # Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .prologue
    .line 415
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 416
    return-void
.end method

.method protected setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V
    .locals 1
    .param p1, "property"    # Lorg/apache/poi/ddf/EscherProperty;

    .prologue
    .line 318
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_optRecord:Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ddf/EscherOptRecord;->setEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 319
    return-void
.end method

.method public setRotationDegree(S)V
    .locals 3
    .param p1, "value"    # S

    .prologue
    .line 402
    new-instance v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/4 v1, 0x4

    shl-int/lit8 v2, p1, 0x10

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFShape;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 403
    return-void
.end method

.method setShapeId(I)V
    .locals 4
    .param p1, "shapeId"    # I

    .prologue
    .line 111
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_escherContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 112
    const/16 v3, -0xff6

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 113
    .local v1, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-nez v1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    .line 116
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFShape;->_objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

    .line 117
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    .line 118
    .local v0, "cod":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    rem-int/lit16 v2, p1, 0x400

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectId(I)V

    goto :goto_0
.end method
