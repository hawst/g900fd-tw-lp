.class final Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
.super Ljava/lang/Object;
.source "RecordFactoryInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/record/RecordFactoryInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StreamEncryptionInfo"
.end annotation


# instance fields
.field private final _filePassRec:Lorg/apache/poi/hssf/record/FilePassRecord;

.field private final _hasBOFRecord:Z

.field private final _initialRecordsSize:I

.field private final _lastRecord:Lorg/apache/poi/hssf/record/Record;


# direct methods
.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;Ljava/util/List;)V
    .locals 5
    .param p1, "rs"    # Lorg/apache/poi/hssf/record/RecordInputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hssf/record/RecordInputStream;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "outputRecs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    const/4 v4, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 54
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v3

    add-int/lit8 v2, v3, 0x4

    .line 55
    .local v2, "recSize":I
    invoke-static {p1}, Lorg/apache/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/poi/hssf/record/RecordInputStream;)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 56
    .local v1, "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    const/4 v0, 0x0

    .line 58
    .local v0, "fpr":Lorg/apache/poi/hssf/record/FilePassRecord;
    instance-of v3, v1, Lorg/apache/poi/hssf/record/BOFRecord;

    if-eqz v3, :cond_3

    .line 59
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_hasBOFRecord:Z

    .line 63
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 64
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 65
    invoke-static {p1}, Lorg/apache/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/poi/hssf/record/RecordInputStream;)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getRecordSize()I

    move-result v3

    add-int/2addr v2, v3

    .line 67
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    instance-of v3, v1, Lorg/apache/poi/hssf/record/WriteProtectRecord;

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 72
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 73
    invoke-static {p1}, Lorg/apache/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/poi/hssf/record/RecordInputStream;)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getRecordSize()I

    move-result v3

    add-int/2addr v2, v3

    .line 75
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_0
    instance-of v3, v1, Lorg/apache/poi/hssf/record/FilePassRecord;

    if-eqz v3, :cond_2

    move-object v0, v1

    .line 81
    check-cast v0, Lorg/apache/poi/hssf/record/FilePassRecord;

    .line 82
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 84
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    check-cast v1, Lorg/apache/poi/hssf/record/Record;

    .line 100
    .restart local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    :cond_1
    :goto_0
    iput v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_initialRecordsSize:I

    .line 101
    iput-object v0, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_filePassRec:Lorg/apache/poi/hssf/record/FilePassRecord;

    .line 102
    iput-object v1, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    .line 103
    return-void

    .line 87
    :cond_2
    instance-of v3, v1, Lorg/apache/poi/hssf/record/EOFRecord;

    if-eqz v3, :cond_1

    .line 90
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "Nothing between BOF and EOF"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 98
    :cond_3
    iput-boolean v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_hasBOFRecord:Z

    goto :goto_0
.end method


# virtual methods
.method public createDecryptingStream(Ljava/io/InputStream;)Lorg/apache/poi/hssf/record/RecordInputStream;
    .locals 6
    .param p1, "original"    # Ljava/io/InputStream;

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_filePassRec:Lorg/apache/poi/hssf/record/FilePassRecord;

    .line 107
    .local v0, "fpr":Lorg/apache/poi/hssf/record/FilePassRecord;
    invoke-static {}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->getCurrentUserPassword()Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "userPassword":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 111
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FilePassRecord;->getDocId()[B

    move-result-object v3

    invoke-static {v3}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->create([B)Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    move-result-object v1

    .line 115
    .local v1, "key":Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
    :goto_0
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FilePassRecord;->getSaltData()[B

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FilePassRecord;->getSaltHash()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->validate([B[B)Z

    move-result v3

    if-nez v3, :cond_2

    .line 116
    new-instance v4, Lorg/apache/poi/EncryptedDocumentException;

    .line 117
    new-instance v5, Ljava/lang/StringBuilder;

    if-nez v2, :cond_1

    const-string/jumbo v3, "Default"

    :goto_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 118
    const-string/jumbo v3, " password is invalid for docId/saltData/saltHash"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 117
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-direct {v4, v3}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 113
    .end local v1    # "key":Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FilePassRecord;->getDocId()[B

    move-result-object v3

    invoke-static {v2, v3}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->create(Ljava/lang/String;[B)Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    move-result-object v1

    .restart local v1    # "key":Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
    goto :goto_0

    .line 117
    :cond_1
    const-string/jumbo v3, "Supplied"

    goto :goto_1

    .line 120
    :cond_2
    new-instance v3, Lorg/apache/poi/hssf/record/RecordInputStream;

    iget v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_initialRecordsSize:I

    invoke-direct {v3, p1, v1, v4}, Lorg/apache/poi/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;I)V

    return-object v3
.end method

.method public getLastRecord()Lorg/apache/poi/hssf/record/Record;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    return-object v0
.end method

.method public hasBOFRecord()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_hasBOFRecord:Z

    return v0
.end method

.method public hasEncryption()Z
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_filePassRec:Lorg/apache/poi/hssf/record/FilePassRecord;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
