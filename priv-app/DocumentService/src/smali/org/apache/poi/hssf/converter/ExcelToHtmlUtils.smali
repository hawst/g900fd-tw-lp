.class public Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;
.super Lorg/apache/poi/hssf/converter/AbstractExcelUtils;
.source "ExcelToHtmlUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/poi/hssf/converter/AbstractExcelUtils;-><init>()V

    return-void
.end method

.method public static appendAlign(Ljava/lang/StringBuilder;S)V
    .locals 2
    .param p0, "style"    # Ljava/lang/StringBuilder;
    .param p1, "alignment"    # S

    .prologue
    .line 30
    invoke-static {p1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getAlign(S)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "cssAlign":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string/jumbo v1, "text-align:"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    const-string/jumbo v1, ";"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static buildMergedRangesMap(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[[Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 13
    .param p0, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    const/4 v12, 0x0

    .line 48
    const/4 v10, 0x1

    new-array v2, v10, [[Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 49
    .local v2, "mergedRanges":[[Lorg/apache/poi/ss/util/CellRangeAddress;
    const/4 v1, 0x0

    .local v1, "m":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getNumMergedRegions()I

    move-result v10

    if-lt v1, v10, :cond_0

    .line 87
    return-object v2

    .line 50
    :cond_0
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getMergedRegion(I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 52
    .local v0, "cellRangeAddress":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-eqz v0, :cond_2

    .line 53
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v10

    add-int/lit8 v6, v10, 0x1

    .line 54
    .local v6, "requiredHeight":I
    array-length v10, v2

    if-ge v10, v6, :cond_1

    .line 55
    new-array v3, v6, [[Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 57
    .local v3, "newArray":[[Lorg/apache/poi/ss/util/CellRangeAddress;
    array-length v10, v2

    .line 56
    invoke-static {v2, v12, v3, v12, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    move-object v2, v3

    .line 61
    .end local v3    # "newArray":[[Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v5

    .line 62
    .local v5, "r":I
    :goto_1
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v10

    .line 61
    if-le v5, v10, :cond_3

    .line 49
    .end local v5    # "r":I
    .end local v6    # "requiredHeight":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    .restart local v5    # "r":I
    .restart local v6    # "requiredHeight":I
    :cond_3
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v10

    add-int/lit8 v7, v10, 0x1

    .line 65
    .local v7, "requiredWidth":I
    aget-object v8, v2, v5

    .line 66
    .local v8, "rowMerged":[Lorg/apache/poi/ss/util/CellRangeAddress;
    if-nez v8, :cond_5

    .line 67
    new-array v8, v7, [Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 68
    aput-object v8, v2, v5

    .line 81
    :cond_4
    :goto_2
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v10

    .line 82
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    .line 81
    invoke-static {v8, v10, v11, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 62
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 70
    :cond_5
    array-length v9, v8

    .line 71
    .local v9, "rowMergedLength":I
    if-ge v9, v7, :cond_4

    .line 72
    new-array v4, v7, [Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 73
    .local v4, "newRow":[Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-static {v8, v12, v4, v12, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 76
    aput-object v4, v2, v5

    .line 77
    move-object v8, v4

    goto :goto_2
.end method
