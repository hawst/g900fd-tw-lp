.class public abstract Lorg/apache/poi/hssf/converter/AbstractExcelConverter;
.super Ljava/lang/Object;
.source "AbstractExcelConverter.java"


# instance fields
.field protected final _formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

.field private fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

.field private outputColumnHeaders:Z

.field private outputHiddenColumns:Z

.field private outputHiddenRows:Z

.field private outputLeadingSpacesAsNonBreaking:Z

.field private outputRowNumbers:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    invoke-direct {v0}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    .line 56
    new-instance v0, Lorg/apache/poi/hwpf/converter/DefaultFontReplacer;

    invoke-direct {v0}, Lorg/apache/poi/hwpf/converter/DefaultFontReplacer;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

    .line 58
    iput-boolean v1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputColumnHeaders:Z

    .line 60
    iput-boolean v2, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputHiddenColumns:Z

    .line 62
    iput-boolean v2, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputHiddenRows:Z

    .line 64
    iput-boolean v1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputLeadingSpacesAsNonBreaking:Z

    .line 66
    iput-boolean v1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputRowNumbers:Z

    .line 40
    return-void
.end method

.method protected static getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I
    .locals 1
    .param p0, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p1, "columnIndex"    # I

    .prologue
    .line 44
    .line 45
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getColumnWidth(I)I

    move-result v0

    .line 44
    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColumnWidthInPx(I)I

    move-result v0

    return v0
.end method

.method protected static getDefaultColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)I
    .locals 1
    .param p0, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 50
    .line 51
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDefaultColumnWidth()I

    move-result v0

    .line 50
    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColumnWidthInPx(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected getColumnName(I)Ljava/lang/String;
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 77
    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lorg/apache/poi/hwpf/converter/NumberFormatter;->getNumber(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getDocument()Lorg/w3c/dom/Document;
.end method

.method public getFontReplacer()Lorg/apache/poi/hwpf/converter/FontReplacer;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

    return-object v0
.end method

.method protected getRowName(Lorg/apache/poi/hssf/usermodel/HSSFRow;)Ljava/lang/String;
    .locals 1
    .param p1, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .prologue
    .line 93
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isOutputColumnHeaders()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputColumnHeaders:Z

    return v0
.end method

.method public isOutputHiddenColumns()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputHiddenColumns:Z

    return v0
.end method

.method public isOutputHiddenRows()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputHiddenRows:Z

    return v0
.end method

.method public isOutputLeadingSpacesAsNonBreaking()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputLeadingSpacesAsNonBreaking:Z

    return v0
.end method

.method public isOutputRowNumbers()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputRowNumbers:Z

    return v0
.end method

.method protected isTextEmpty(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Z
    .locals 8
    .param p1, "cell"    # Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .prologue
    const/4 v3, 0x0

    .line 124
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 175
    const/4 v3, 0x1

    .line 178
    :cond_0
    :goto_0
    return v3

    .line 128
    :pswitch_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, "value":Ljava/lang/String;
    :goto_1
    invoke-static {v2}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 131
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCachedFormulaResultType()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 158
    :pswitch_2
    const-string/jumbo v2, ""

    .line 161
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 134
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v0

    .line 135
    .local v0, "str":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 138
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->toString()Ljava/lang/String;

    move-result-object v2

    .line 139
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 141
    .end local v0    # "str":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v1

    .line 142
    .local v1, "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    if-eqz v1, :cond_0

    .line 147
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    .line 148
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getNumericCellValue()D

    move-result-wide v4

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormat()S

    move-result v6

    .line 149
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormatString()Ljava/lang/String;

    move-result-object v7

    .line 147
    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatRawCellContents(DILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 150
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 152
    .end local v1    # "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_5
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 153
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 155
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_6
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getErrorCellValue()B

    move-result v3

    invoke-static {v3}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->getText(I)Ljava/lang/String;

    move-result-object v2

    .line 156
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 163
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_7
    const-string/jumbo v2, ""

    .line 164
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 166
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_8
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    invoke-virtual {v3, p1}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatCellValue(Lorg/apache/poi/ss/usermodel/Cell;)Ljava/lang/String;

    move-result-object v2

    .line 167
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 169
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_9
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 170
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 172
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_a
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getErrorCellValue()B

    move-result v3

    invoke-static {v3}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->getText(I)Ljava/lang/String;

    move-result-object v2

    .line 173
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 131
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setFontReplacer(Lorg/apache/poi/hwpf/converter/FontReplacer;)V
    .locals 0
    .param p1, "fontReplacer"    # Lorg/apache/poi/hwpf/converter/FontReplacer;

    .prologue
    .line 183
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->fontReplacer:Lorg/apache/poi/hwpf/converter/FontReplacer;

    .line 184
    return-void
.end method

.method public setOutputColumnHeaders(Z)V
    .locals 0
    .param p1, "outputColumnHeaders"    # Z

    .prologue
    .line 188
    iput-boolean p1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputColumnHeaders:Z

    .line 189
    return-void
.end method

.method public setOutputHiddenColumns(Z)V
    .locals 0
    .param p1, "outputZeroWidthColumns"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputHiddenColumns:Z

    .line 194
    return-void
.end method

.method public setOutputHiddenRows(Z)V
    .locals 0
    .param p1, "outputZeroHeightRows"    # Z

    .prologue
    .line 198
    iput-boolean p1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputHiddenRows:Z

    .line 199
    return-void
.end method

.method public setOutputLeadingSpacesAsNonBreaking(Z)V
    .locals 0
    .param p1, "outputPrePostSpacesAsNonBreaking"    # Z

    .prologue
    .line 204
    iput-boolean p1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputLeadingSpacesAsNonBreaking:Z

    .line 205
    return-void
.end method

.method public setOutputRowNumbers(Z)V
    .locals 0
    .param p1, "outputRowNumbers"    # Z

    .prologue
    .line 209
    iput-boolean p1, p0, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;->outputRowNumbers:Z

    .line 210
    return-void
.end method
