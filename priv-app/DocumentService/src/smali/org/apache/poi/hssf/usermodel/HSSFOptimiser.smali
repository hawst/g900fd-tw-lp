.class public Lorg/apache/poi/hssf/usermodel/HSSFOptimiser;
.super Ljava/lang/Object;
.source "HSSFOptimiser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static optimiseCellStyles(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 24
    .param p0, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 177
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumExFormats()I

    move-result v21

    move/from16 v0, v21

    new-array v9, v0, [S

    .line 178
    .local v9, "newPos":[S
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v6, v0, [Z

    .line 179
    .local v6, "isUsed":[Z
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [Z

    move-object/from16 v20, v0

    .line 180
    .local v20, "zapRecords":[Z
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v5, v0, :cond_0

    .line 188
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-object/from16 v19, v0

    .line 189
    .local v19, "xfrs":[Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    const/4 v5, 0x0

    :goto_1
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v5, v0, :cond_1

    .line 198
    const/16 v5, 0x15

    :goto_2
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v5, v0, :cond_2

    .line 218
    const/16 v17, 0x0

    .local v17, "sheetNum":I
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v21

    move/from16 v0, v17

    move/from16 v1, v21

    if-lt v0, v1, :cond_7

    .line 229
    const/16 v5, 0x15

    :goto_4
    array-length v0, v6

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v5, v0, :cond_a

    .line 241
    const/16 v5, 0x15

    :goto_5
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v5, v0, :cond_c

    .line 257
    array-length v8, v9

    .line 258
    .local v8, "max":I
    const/4 v14, 0x0

    .line 259
    .local v14, "removed":I
    const/16 v5, 0x15

    :goto_6
    if-lt v5, v8, :cond_f

    .line 269
    const/16 v17, 0x0

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v21

    move/from16 v0, v17

    move/from16 v1, v21

    if-lt v0, v1, :cond_11

    .line 283
    return-void

    .line 181
    .end local v8    # "max":I
    .end local v14    # "removed":I
    .end local v17    # "sheetNum":I
    .end local v19    # "xfrs":[Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    :cond_0
    const/16 v21, 0x0

    aput-boolean v21, v6, v5

    .line 182
    int-to-short v0, v5

    move/from16 v21, v0

    aput-short v21, v9, v5

    .line 183
    const/16 v21, 0x0

    aput-boolean v21, v20, v5

    .line 180
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 190
    .restart local v19    # "xfrs":[Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getExFormatAt(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v21

    aput-object v21, v19, v5

    .line 189
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 201
    :cond_2
    const/4 v4, -0x1

    .line 202
    .local v4, "earlierDuplicate":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_8
    if-ge v7, v5, :cond_3

    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v4, v0, :cond_5

    .line 210
    :cond_3
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v4, v0, :cond_4

    .line 211
    int-to-short v0, v4

    move/from16 v21, v0

    aput-short v21, v9, v5

    .line 212
    const/16 v21, 0x1

    aput-boolean v21, v20, v5

    .line 198
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 203
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getExFormatAt(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v18

    .line 204
    .local v18, "xfCheck":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    aget-object v21, v19, v5

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 205
    move v4, v7

    .line 202
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 219
    .end local v4    # "earlierDuplicate":I
    .end local v7    # "j":I
    .end local v18    # "xfCheck":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    .restart local v17    # "sheetNum":I
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v16

    .line 220
    .local v16, "s":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_8
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_9

    .line 218
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 220
    :cond_9
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/ss/usermodel/Row;

    .line 221
    .local v15, "row":Lorg/apache/poi/ss/usermodel/Row;
    invoke-interface {v15}, Lorg/apache/poi/ss/usermodel/Row;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_9
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_8

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/usermodel/Cell;

    .local v3, "cellI":Lorg/apache/poi/ss/usermodel/Cell;
    move-object v2, v3

    .line 222
    check-cast v2, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 223
    .local v2, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellValueRecord()Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v12

    .line 224
    .local v12, "oldXf":S
    const/16 v23, 0x1

    aput-boolean v23, v6, v12

    goto :goto_9

    .line 230
    .end local v2    # "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .end local v3    # "cellI":Lorg/apache/poi/ss/usermodel/Cell;
    .end local v12    # "oldXf":S
    .end local v15    # "row":Lorg/apache/poi/ss/usermodel/Row;
    .end local v16    # "s":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    :cond_a
    aget-boolean v21, v6, v5

    if-nez v21, :cond_b

    .line 232
    const/16 v21, 0x1

    aput-boolean v21, v20, v5

    .line 233
    const/16 v21, 0x0

    aput-short v21, v9, v5

    .line 229
    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    .line 244
    :cond_c
    aget-short v13, v9, v5

    .line 245
    .local v13, "preDeletePos":S
    move v10, v13

    .line 246
    .local v10, "newPosition":S
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_a
    if-lt v7, v13, :cond_d

    .line 251
    aput-short v10, v9, v5

    .line 241
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5

    .line 247
    :cond_d
    aget-boolean v21, v20, v7

    if-eqz v21, :cond_e

    add-int/lit8 v21, v10, -0x1

    move/from16 v0, v21

    int-to-short v10, v0

    .line 246
    :cond_e
    add-int/lit8 v7, v7, 0x1

    goto :goto_a

    .line 260
    .end local v7    # "j":I
    .end local v10    # "newPosition":S
    .end local v13    # "preDeletePos":S
    .restart local v8    # "max":I
    .restart local v14    # "removed":I
    :cond_f
    add-int v21, v5, v14

    aget-boolean v21, v20, v21

    if-eqz v21, :cond_10

    .line 261
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->removeExFormatRecord(I)V

    .line 262
    add-int/lit8 v5, v5, -0x1

    .line 263
    add-int/lit8 v8, v8, -0x1

    .line 264
    add-int/lit8 v14, v14, 0x1

    .line 259
    :cond_10
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    .line 270
    :cond_11
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v16

    .line 271
    .restart local v16    # "s":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_12
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_13

    .line 269
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_7

    .line 271
    :cond_13
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/ss/usermodel/Row;

    .line 272
    .restart local v15    # "row":Lorg/apache/poi/ss/usermodel/Row;
    invoke-interface {v15}, Lorg/apache/poi/ss/usermodel/Row;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_b
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_12

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ss/usermodel/Cell;

    .restart local v3    # "cellI":Lorg/apache/poi/ss/usermodel/Cell;
    move-object v2, v3

    .line 273
    check-cast v2, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 274
    .restart local v2    # "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellValueRecord()Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v12

    .line 277
    .restart local v12    # "oldXf":S
    aget-short v23, v9, v12

    .line 276
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCellStyleAt(S)Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v11

    .line 279
    .local v11, "newStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    invoke-virtual {v2, v11}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->setCellStyle(Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;)V

    goto :goto_b
.end method

.method public static optimiseFonts(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 23
    .param p0, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 54
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumberOfFontRecords()I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    new-array v9, v0, [S

    .line 55
    .local v9, "newPos":[S
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v0, v0, [Z

    move-object/from16 v18, v0

    .line 56
    .local v18, "zapRecords":[Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v7, v0, :cond_0

    .line 63
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v6, v0, [Lorg/apache/poi/hssf/record/FontRecord;

    .line 64
    .local v6, "frecs":[Lorg/apache/poi/hssf/record/FontRecord;
    const/4 v7, 0x0

    :goto_1
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v7, v0, :cond_1

    .line 76
    const/4 v7, 0x5

    :goto_2
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v7, v0, :cond_3

    .line 100
    const/4 v7, 0x5

    :goto_3
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v7, v0, :cond_9

    .line 114
    const/4 v7, 0x5

    :goto_4
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v7, v0, :cond_c

    .line 124
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->resetFontCache()V

    .line 128
    const/4 v7, 0x0

    :goto_5
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumExFormats()I

    move-result v19

    move/from16 v0, v19

    if-lt v7, v0, :cond_e

    .line 139
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 140
    .local v3, "doneUnicodeStrings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/poi/hssf/record/common/UnicodeString;>;"
    const/4 v15, 0x0

    .end local v7    # "i":I
    .local v15, "sheetNum":I
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v19

    move/from16 v0, v19

    if-lt v15, v0, :cond_f

    .line 164
    return-void

    .line 57
    .end local v3    # "doneUnicodeStrings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/poi/hssf/record/common/UnicodeString;>;"
    .end local v6    # "frecs":[Lorg/apache/poi/hssf/record/FontRecord;
    .end local v15    # "sheetNum":I
    .restart local v7    # "i":I
    :cond_0
    int-to-short v0, v7

    move/from16 v19, v0

    aput-short v19, v9, v7

    .line 58
    const/16 v19, 0x0

    aput-boolean v19, v18, v7

    .line 56
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 66
    .restart local v6    # "frecs":[Lorg/apache/poi/hssf/record/FontRecord;
    :cond_1
    const/16 v19, 0x4

    move/from16 v0, v19

    if-ne v7, v0, :cond_2

    .line 64
    :goto_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 68
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getFontRecordAt(I)Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v19

    aput-object v19, v6, v7

    goto :goto_7

    .line 79
    :cond_3
    const/4 v4, -0x1

    .line 80
    .local v4, "earlierDuplicate":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_8
    if-ge v8, v7, :cond_4

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v4, v0, :cond_6

    .line 90
    :cond_4
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v4, v0, :cond_5

    .line 91
    int-to-short v0, v4

    move/from16 v19, v0

    aput-short v19, v9, v7

    .line 92
    const/16 v19, 0x1

    aput-boolean v19, v18, v7

    .line 76
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 81
    :cond_6
    const/16 v19, 0x4

    move/from16 v0, v19

    if-ne v8, v0, :cond_8

    .line 80
    :cond_7
    :goto_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 83
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getFontRecordAt(I)Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v5

    .line 84
    .local v5, "frCheck":Lorg/apache/poi/hssf/record/FontRecord;
    aget-object v19, v6, v7

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/record/FontRecord;->sameProperties(Lorg/apache/poi/hssf/record/FontRecord;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 85
    move v4, v8

    goto :goto_9

    .line 103
    .end local v4    # "earlierDuplicate":I
    .end local v5    # "frCheck":Lorg/apache/poi/hssf/record/FontRecord;
    .end local v8    # "j":I
    :cond_9
    aget-short v11, v9, v7

    .line 104
    .local v11, "preDeletePos":S
    move v10, v11

    .line 105
    .local v10, "newPosition":S
    const/4 v8, 0x0

    .restart local v8    # "j":I
    :goto_a
    if-lt v8, v11, :cond_a

    .line 110
    aput-short v10, v9, v7

    .line 100
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 106
    :cond_a
    aget-boolean v19, v18, v8

    if-eqz v19, :cond_b

    add-int/lit8 v19, v10, -0x1

    move/from16 v0, v19

    int-to-short v10, v0

    .line 105
    :cond_b
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 115
    .end local v8    # "j":I
    .end local v10    # "newPosition":S
    .end local v11    # "preDeletePos":S
    :cond_c
    aget-boolean v19, v18, v7

    if-eqz v19, :cond_d

    .line 116
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v19

    .line 117
    aget-object v20, v6, v7

    .line 116
    invoke-virtual/range {v19 .. v20}, Lorg/apache/poi/hssf/model/InternalWorkbook;->removeFontRecord(Lorg/apache/poi/hssf/record/FontRecord;)V

    .line 114
    :cond_d
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4

    .line 129
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getExFormatAt(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v17

    .line 131
    .local v17, "xfr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->getFontIndex()S

    move-result v19

    aget-short v19, v9, v19

    .line 130
    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 128
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_5

    .line 141
    .end local v7    # "i":I
    .end local v17    # "xfr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    .restart local v3    # "doneUnicodeStrings":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/poi/hssf/record/common/UnicodeString;>;"
    .restart local v15    # "sheetNum":I
    :cond_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v14

    .line 142
    .local v14, "s":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {v14}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_10
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_11

    .line 140
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_6

    .line 142
    :cond_11
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/poi/ss/usermodel/Row;

    .line 143
    .local v12, "row":Lorg/apache/poi/ss/usermodel/Row;
    invoke-interface {v12}, Lorg/apache/poi/ss/usermodel/Row;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_12
    :goto_b
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_10

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ss/usermodel/Cell;

    .line 144
    .local v2, "cell":Lorg/apache/poi/ss/usermodel/Cell;
    invoke-interface {v2}, Lorg/apache/poi/ss/usermodel/Cell;->getCellType()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_12

    .line 145
    invoke-interface {v2}, Lorg/apache/poi/ss/usermodel/Cell;->getRichStringCellValue()Lorg/apache/poi/ss/usermodel/RichTextString;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    .line 146
    .local v13, "rtr":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    invoke-virtual {v13}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getRawUnicodeString()Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v16

    .line 149
    .local v16, "u":Lorg/apache/poi/hssf/record/common/UnicodeString;
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_12

    .line 151
    const/4 v7, 0x5

    .local v7, "i":S
    :goto_c
    array-length v0, v9

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v7, v0, :cond_13

    .line 158
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 152
    :cond_13
    aget-short v21, v9, v7

    move/from16 v0, v21

    if-eq v7, v0, :cond_14

    .line 153
    aget-short v21, v9, v7

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->swapFontUse(SS)V

    .line 151
    :cond_14
    add-int/lit8 v21, v7, 0x1

    move/from16 v0, v21

    int-to-short v7, v0

    goto :goto_c
.end method
