.class public Lorg/apache/poi/hssf/usermodel/DVConstraint;
.super Ljava/lang/Object;
.source "DVConstraint.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/DataValidationConstraint;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hssf/usermodel/DVConstraint$FormulaPair;
    }
.end annotation


# static fields
.field private static final VT:Lorg/apache/poi/ss/usermodel/DataValidationConstraint$ValidationType;


# instance fields
.field private _explicitListValues:[Ljava/lang/String;

.field private _formula1:Ljava/lang/String;

.field private _formula2:Ljava/lang/String;

.field private _operator:I

.field private final _validationType:I

.field private _value1:Ljava/lang/Double;

.field private _value2:Ljava/lang/Double;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->VT:Lorg/apache/poi/ss/usermodel/DataValidationConstraint$ValidationType;

    return-void
.end method

.method private constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;[Ljava/lang/String;)V
    .locals 0
    .param p1, "validationType"    # I
    .param p2, "comparisonOperator"    # I
    .param p3, "formulaA"    # Ljava/lang/String;
    .param p4, "formulaB"    # Ljava/lang/String;
    .param p5, "value1"    # Ljava/lang/Double;
    .param p6, "value2"    # Ljava/lang/Double;
    .param p7, "excplicitListValues"    # [Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p1, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_validationType:I

    .line 71
    iput p2, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_operator:I

    .line 72
    iput-object p3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula2:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value1:Ljava/lang/Double;

    .line 75
    iput-object p6, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value2:Ljava/lang/Double;

    .line 76
    iput-object p7, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    .line 77
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8
    .param p1, "listFormula"    # Ljava/lang/String;
    .param p2, "excplicitListValues"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 84
    const/4 v1, 0x3

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    .line 85
    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;[Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method private static convertDate(Ljava/lang/String;Ljava/text/SimpleDateFormat;)Ljava/lang/Double;
    .locals 5
    .param p0, "dateStr"    # Ljava/lang/String;
    .param p1, "dateFormat"    # Ljava/text/SimpleDateFormat;

    .prologue
    .line 248
    if-nez p0, :cond_0

    .line 249
    const/4 v2, 0x0

    .line 262
    :goto_0
    return-object v2

    .line 252
    :cond_0
    if-nez p1, :cond_1

    .line 253
    invoke-static {p0}, Lorg/apache/poi/hssf/usermodel/HSSFDateUtil;->parseYYYYMMDDDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 262
    .local v0, "dateVal":Ljava/util/Date;
    :goto_1
    invoke-static {v0}, Lorg/apache/poi/hssf/usermodel/HSSFDateUtil;->getExcelDate(Ljava/util/Date;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    .line 256
    .end local v0    # "dateVal":Ljava/util/Date;
    :cond_1
    :try_start_0
    invoke-virtual {p1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .restart local v0    # "dateVal":Ljava/util/Date;
    goto :goto_1

    .line 257
    .end local v0    # "dateVal":Ljava/util/Date;
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/text/ParseException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to parse date \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 259
    const-string/jumbo v4, "\' using specified format \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 258
    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static convertDoubleFormula(Ljava/lang/String;Ljava/lang/Double;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 6
    .param p0, "formula"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Double;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    const/4 v3, 0x0

    .line 421
    if-nez p0, :cond_1

    .line 422
    if-nez p1, :cond_0

    .line 423
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;

    .line 431
    :goto_0
    return-object v1

    .line 425
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    new-instance v2, Lorg/apache/poi/ss/formula/ptg/NumberPtg;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lorg/apache/poi/ss/formula/ptg/NumberPtg;-><init>(D)V

    aput-object v2, v1, v3

    goto :goto_0

    .line 427
    :cond_1
    if-eqz p1, :cond_2

    .line 428
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Both formula and value cannot be present"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 430
    :cond_2
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v0

    .line 431
    .local v0, "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    invoke-virtual {v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v1

    invoke-static {p0, v0, v3, v1}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;II)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    goto :goto_0
.end method

.method private static convertNumber(Ljava/lang/String;)Ljava/lang/Double;
    .locals 4
    .param p0, "numberStr"    # Ljava/lang/String;

    .prologue
    .line 223
    if-nez p0, :cond_0

    .line 224
    const/4 v1, 0x0

    .line 227
    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "The supplied text \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 230
    const-string/jumbo v3, "\' could not be parsed as a number"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 229
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static convertTime(Ljava/lang/String;)Ljava/lang/Double;
    .locals 2
    .param p0, "timeStr"    # Ljava/lang/String;

    .prologue
    .line 238
    if-nez p0, :cond_0

    .line 239
    const/4 v0, 0x0

    .line 241
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lorg/apache/poi/hssf/usermodel/HSSFDateUtil;->convertTime(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method public static createCustomFormulaConstraint(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;
    .locals 8
    .param p0, "formula"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 266
    if-nez p0, :cond_0

    .line 267
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "formula must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_0
    new-instance v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;

    const/4 v1, 0x7

    const/4 v2, 0x0

    move-object v3, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static createDateConstraint(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;
    .locals 9
    .param p0, "comparisonOperator"    # I
    .param p1, "expr1"    # Ljava/lang/String;
    .param p2, "expr2"    # Ljava/lang/String;
    .param p3, "dateFormat"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 179
    if-nez p1, :cond_0

    .line 180
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "expr1 must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_0
    invoke-static {p0, p2}, Lorg/apache/poi/ss/usermodel/DataValidationConstraint$OperatorType;->validateSecondArg(ILjava/lang/String;)V

    .line 183
    if-nez p3, :cond_1

    move-object v8, v7

    .line 186
    .local v8, "df":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "formula1":Ljava/lang/String;
    if-nez v3, :cond_2

    invoke-static {p1, v8}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertDate(Ljava/lang/String;Ljava/text/SimpleDateFormat;)Ljava/lang/Double;

    move-result-object v5

    .line 189
    .local v5, "value1":Ljava/lang/Double;
    :goto_1
    invoke-static {p2}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 190
    .local v4, "formula2":Ljava/lang/String;
    if-nez v4, :cond_3

    invoke-static {p2, v8}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertDate(Ljava/lang/String;Ljava/text/SimpleDateFormat;)Ljava/lang/Double;

    move-result-object v6

    .line 191
    .local v6, "value2":Ljava/lang/Double;
    :goto_2
    new-instance v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;

    const/4 v1, 0x4

    move v2, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;[Ljava/lang/String;)V

    return-object v0

    .line 183
    .end local v3    # "formula1":Ljava/lang/String;
    .end local v4    # "formula2":Ljava/lang/String;
    .end local v5    # "value1":Ljava/lang/Double;
    .end local v6    # "value2":Ljava/lang/Double;
    .end local v8    # "df":Ljava/text/SimpleDateFormat;
    :cond_1
    new-instance v8, Ljava/text/SimpleDateFormat;

    invoke-direct {v8, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .restart local v3    # "formula1":Ljava/lang/String;
    .restart local v8    # "df":Ljava/text/SimpleDateFormat;
    :cond_2
    move-object v5, v7

    .line 187
    goto :goto_1

    .restart local v4    # "formula2":Ljava/lang/String;
    .restart local v5    # "value1":Ljava/lang/Double;
    :cond_3
    move-object v6, v7

    .line 190
    goto :goto_2
.end method

.method public static createExplicitListConstraint([Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;
    .locals 2
    .param p0, "explicitListValues"    # [Ljava/lang/String;

    .prologue
    .line 134
    new-instance v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static createFormulaListConstraint(Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;
    .locals 2
    .param p0, "listFormula"    # Ljava/lang/String;

    .prologue
    .line 131
    new-instance v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method private createListFormula(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 7
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    const/4 v6, 0x0

    .line 397
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    if-nez v3, :cond_0

    .line 398
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v2

    .line 400
    .local v2, "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    const/4 v4, 0x5

    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v5

    invoke-static {v3, v2, v4, v5}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;II)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v3

    .line 413
    .end local v2    # "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    :goto_0
    return-object v3

    .line 405
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x10

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 406
    .local v1, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_1

    .line 413
    const/4 v3, 0x1

    new-array v3, v3, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    new-instance v4, Lorg/apache/poi/ss/formula/ptg/StringPtg;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/ss/formula/ptg/StringPtg;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v6

    goto :goto_0

    .line 407
    :cond_1
    if-lez v0, :cond_2

    .line 408
    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 410
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;
    .locals 8
    .param p0, "validationType"    # I
    .param p1, "comparisonOperator"    # I
    .param p2, "expr1"    # Ljava/lang/String;
    .param p3, "expr2"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 103
    packed-switch p0, :pswitch_data_0

    .line 118
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Validation Type ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") not supported with this method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :pswitch_1
    if-nez p2, :cond_0

    if-eqz p3, :cond_2

    .line 106
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "expr1 and expr2 must be null for validation type \'any\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :pswitch_2
    if-nez p2, :cond_1

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "expr1 must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_1
    invoke-static {p1, p3}, Lorg/apache/poi/ss/usermodel/DataValidationConstraint$OperatorType;->validateSecondArg(ILjava/lang/String;)V

    .line 122
    :cond_2
    invoke-static {p2}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123
    .local v3, "formula1":Ljava/lang/String;
    if-nez v3, :cond_3

    invoke-static {p2}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertNumber(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    .line 125
    .local v5, "value1":Ljava/lang/Double;
    :goto_0
    invoke-static {p3}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 126
    .local v4, "formula2":Ljava/lang/String;
    if-nez v4, :cond_4

    invoke-static {p3}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertNumber(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v6

    .line 127
    .local v6, "value2":Ljava/lang/Double;
    :goto_1
    new-instance v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;

    move v1, p0

    move v2, p1

    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;[Ljava/lang/String;)V

    return-object v0

    .end local v4    # "formula2":Ljava/lang/String;
    .end local v5    # "value1":Ljava/lang/Double;
    .end local v6    # "value2":Ljava/lang/Double;
    :cond_3
    move-object v5, v7

    .line 123
    goto :goto_0

    .restart local v4    # "formula2":Ljava/lang/String;
    .restart local v5    # "value1":Ljava/lang/Double;
    :cond_4
    move-object v6, v7

    .line 126
    goto :goto_1

    .line 103
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static createTimeConstraint(ILjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/usermodel/DVConstraint;
    .locals 8
    .param p0, "comparisonOperator"    # I
    .param p1, "expr1"    # Ljava/lang/String;
    .param p2, "expr2"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 150
    if-nez p1, :cond_0

    .line 151
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "expr1 must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/poi/ss/usermodel/DataValidationConstraint$OperatorType;->validateSecondArg(ILjava/lang/String;)V

    .line 156
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 157
    .local v3, "formula1":Ljava/lang/String;
    if-nez v3, :cond_1

    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertTime(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    .line 159
    .local v5, "value1":Ljava/lang/Double;
    :goto_0
    invoke-static {p2}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, "formula2":Ljava/lang/String;
    if-nez v4, :cond_2

    invoke-static {p2}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertTime(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v6

    .line 161
    .local v6, "value2":Ljava/lang/Double;
    :goto_1
    new-instance v0, Lorg/apache/poi/hssf/usermodel/DVConstraint;

    const/4 v1, 0x5

    move v2, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/poi/hssf/usermodel/DVConstraint;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;[Ljava/lang/String;)V

    return-object v0

    .end local v4    # "formula2":Ljava/lang/String;
    .end local v5    # "value1":Ljava/lang/Double;
    .end local v6    # "value2":Ljava/lang/Double;
    :cond_1
    move-object v5, v7

    .line 157
    goto :goto_0

    .restart local v4    # "formula2":Ljava/lang/String;
    .restart local v5    # "value1":Ljava/lang/Double;
    :cond_2
    move-object v6, v7

    .line 160
    goto :goto_1
.end method

.method private static getFormulaFromTextExpression(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "textExpr"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 206
    if-nez p0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-object v0

    .line 209
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v3, :cond_2

    .line 210
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Empty string is not a valid formula/value expression"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3d

    if-ne v1, v2, :cond_0

    .line 213
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method createFormulas(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)Lorg/apache/poi/hssf/usermodel/DVConstraint$FormulaPair;
    .locals 4
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 385
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->isListValidationType()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 386
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->createListFormula(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    .line 387
    .local v0, "formula1":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    sget-object v1, Lorg/apache/poi/ss/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;

    .line 392
    .local v1, "formula2":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :goto_0
    new-instance v2, Lorg/apache/poi/hssf/usermodel/DVConstraint$FormulaPair;

    invoke-direct {v2, v0, v1}, Lorg/apache/poi/hssf/usermodel/DVConstraint$FormulaPair;-><init>([Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    return-object v2

    .line 389
    .end local v0    # "formula1":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .end local v1    # "formula2":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value1:Ljava/lang/Double;

    invoke-static {v2, v3, p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertDoubleFormula(Ljava/lang/String;Ljava/lang/Double;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    .line 390
    .restart local v0    # "formula1":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula2:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value2:Ljava/lang/Double;

    invoke-static {v2, v3, p1}, Lorg/apache/poi/hssf/usermodel/DVConstraint;->convertDoubleFormula(Ljava/lang/String;Ljava/lang/Double;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    .restart local v1    # "formula2":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    goto :goto_0
.end method

.method public getExplicitListValues()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    return-object v0
.end method

.method public getFormula1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    return-object v0
.end method

.method public getFormula2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula2:Ljava/lang/String;

    return-object v0
.end method

.method public getOperator()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_operator:I

    return v0
.end method

.method public getValidationType()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_validationType:I

    return v0
.end method

.method public getValue1()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value1:Ljava/lang/Double;

    return-object v0
.end method

.method public getValue2()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value2:Ljava/lang/Double;

    return-object v0
.end method

.method public isExplicitList()Z
    .locals 2

    .prologue
    .line 290
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_validationType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isListValidationType()Z
    .locals 2

    .prologue
    .line 283
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_validationType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setExplicitListValues([Ljava/lang/String;)V
    .locals 2
    .param p1, "explicitListValues"    # [Ljava/lang/String;

    .prologue
    .line 315
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_validationType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 316
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Cannot setExplicitListValues on non-list constraint"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    .line 319
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    .line 320
    return-void
.end method

.method public setFormula1(Ljava/lang/String;)V
    .locals 1
    .param p1, "formula1"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 332
    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value1:Ljava/lang/Double;

    .line 333
    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_explicitListValues:[Ljava/lang/String;

    .line 334
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    .line 335
    return-void
.end method

.method public setFormula2(Ljava/lang/String;)V
    .locals 1
    .param p1, "formula2"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value2:Ljava/lang/Double;

    .line 348
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula2:Ljava/lang/String;

    .line 349
    return-void
.end method

.method public setOperator(I)V
    .locals 0
    .param p1, "operator"    # I

    .prologue
    .line 302
    iput p1, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_operator:I

    .line 303
    return-void
.end method

.method public setValue1(D)V
    .locals 1
    .param p1, "value1"    # D

    .prologue
    .line 361
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula1:Ljava/lang/String;

    .line 362
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value1:Ljava/lang/Double;

    .line 363
    return-void
.end method

.method public setValue2(D)V
    .locals 1
    .param p1, "value2"    # D

    .prologue
    .line 375
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_formula2:Ljava/lang/String;

    .line 376
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/DVConstraint;->_value2:Ljava/lang/Double;

    .line 377
    return-void
.end method
