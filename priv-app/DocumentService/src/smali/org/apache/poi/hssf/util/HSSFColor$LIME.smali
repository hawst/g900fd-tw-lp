.class public final Lorg/apache/poi/hssf/util/HSSFColor$LIME;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LIME"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "9999:CCCC:0"

.field public static final index:S = 0x32s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 783
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 784
    const/16 v2, 0x99

    aput-short v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0xcc

    aput-short v2, v0, v1

    .line 782
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LIME;->triplet:[S

    .line 786
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 778
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 800
    const-string/jumbo v0, "9999:CCCC:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 790
    const/16 v0, 0x32

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 795
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LIME;->triplet:[S

    return-object v0
.end method
