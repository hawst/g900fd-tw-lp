.class public Lorg/apache/poi/hssf/usermodel/HSSFShapeFactory;
.super Ljava/lang/Object;
.source "HSSFShapeFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createShapeTree(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/EscherAggregate;Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 20
    .param p0, "container"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "agg"    # Lorg/apache/poi/hssf/record/EscherAggregate;
    .param p2, "out"    # Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;
    .param p3, "root"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .prologue
    .line 48
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v18

    const/16 v19, -0xffd

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 49
    const/4 v8, 0x0

    .line 50
    .local v8, "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v18

    check-cast v18, Lorg/apache/poi/ddf/EscherContainerRecord;

    const/16 v19, -0xfef

    invoke-virtual/range {v18 .. v19}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/ddf/EscherClientDataRecord;

    .line 51
    .local v4, "clientData":Lorg/apache/poi/ddf/EscherClientDataRecord;
    if-eqz v4, :cond_0

    .line 52
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/record/EscherAggregate;->getShapeToObjMapping()Ljava/util/Map;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    check-cast v8, Lorg/apache/poi/hssf/record/ObjRecord;

    .line 54
    .restart local v8    # "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    :cond_0
    new-instance v6, Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v8}, Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V

    .line 55
    .local v6, "group":Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v3

    .line 57
    .local v3, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v7, v0, :cond_2

    .line 63
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;->addShape(Lorg/apache/poi/hssf/usermodel/HSSFShape;)V

    .line 125
    .end local v3    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    .end local v4    # "clientData":Lorg/apache/poi/ddf/EscherClientDataRecord;
    .end local v6    # "group":Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;
    .end local v7    # "i":I
    .end local v8    # "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    :cond_1
    :goto_1
    return-void

    .line 58
    .restart local v3    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    .restart local v4    # "clientData":Lorg/apache/poi/ddf/EscherClientDataRecord;
    .restart local v6    # "group":Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;
    .restart local v7    # "i":I
    .restart local v8    # "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    :cond_2
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 59
    .local v16, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v7, :cond_3

    .line 60
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v6, v2}, Lorg/apache/poi/hssf/usermodel/HSSFShapeFactory;->createShapeTree(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/EscherAggregate;Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 57
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 64
    .end local v3    # "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherContainerRecord;>;"
    .end local v4    # "clientData":Lorg/apache/poi/ddf/EscherClientDataRecord;
    .end local v6    # "group":Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;
    .end local v7    # "i":I
    .end local v8    # "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    .end local v16    # "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v18

    const/16 v19, -0xffc

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 65
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/record/EscherAggregate;->getShapeToObjMapping()Ljava/util/Map;

    move-result-object v15

    .line 66
    .local v15, "shapeToObj":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/poi/ddf/EscherRecord;Lorg/apache/poi/hssf/record/Record;>;"
    const/4 v9, 0x0

    .line 67
    .local v9, "objRecord":Lorg/apache/poi/hssf/record/ObjRecord;
    const/16 v17, 0x0

    .line 69
    .local v17, "txtRecord":Lorg/apache/poi/hssf/record/TextObjectRecord;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_5

    .line 80
    if-eqz v9, :cond_6

    invoke-static {v9}, Lorg/apache/poi/hssf/usermodel/HSSFShapeFactory;->isEmbeddedObject(Lorg/apache/poi/hssf/record/ObjRecord;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 81
    new-instance v10, Lorg/apache/poi/hssf/usermodel/HSSFObjectData;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v10, v0, v9, v1}, Lorg/apache/poi/hssf/usermodel/HSSFObjectData;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    .line 82
    .local v10, "objectData":Lorg/apache/poi/hssf/usermodel/HSSFObjectData;
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;->addShape(Lorg/apache/poi/hssf/usermodel/HSSFShape;)V

    goto :goto_1

    .line 69
    .end local v10    # "objectData":Lorg/apache/poi/hssf/usermodel/HSSFObjectData;
    :cond_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/ddf/EscherRecord;

    .line 70
    .local v13, "record":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v13}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v19

    sparse-switch v19, :sswitch_data_0

    goto :goto_2

    .line 75
    :sswitch_0
    invoke-interface {v15, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "txtRecord":Lorg/apache/poi/hssf/record/TextObjectRecord;
    check-cast v17, Lorg/apache/poi/hssf/record/TextObjectRecord;

    .restart local v17    # "txtRecord":Lorg/apache/poi/hssf/record/TextObjectRecord;
    goto :goto_2

    .line 72
    :sswitch_1
    invoke-interface {v15, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "objRecord":Lorg/apache/poi/hssf/record/ObjRecord;
    check-cast v9, Lorg/apache/poi/hssf/record/ObjRecord;

    .line 73
    .restart local v9    # "objRecord":Lorg/apache/poi/hssf/record/ObjRecord;
    goto :goto_2

    .line 86
    .end local v13    # "record":Lorg/apache/poi/ddf/EscherRecord;
    :cond_6
    if-eqz v9, :cond_1

    .line 90
    invoke-virtual {v9}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v18

    const/16 v19, 0x0

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    .line 92
    .local v5, "cmo":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->getObjectType()S

    move-result v18

    sparse-switch v18, :sswitch_data_1

    .line 121
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v9, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V

    .line 123
    .local v14, "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :goto_3
    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;->addShape(Lorg/apache/poi/hssf/usermodel/HSSFShape;)V

    goto/16 :goto_1

    .line 94
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_2
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFPicture;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V

    .line 95
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 97
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_3
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v9, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V

    .line 98
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 100
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_4
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V

    .line 101
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 103
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_5
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFCombobox;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFCombobox;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V

    .line 104
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 106
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_6
    const/16 v18, -0xff5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 107
    .local v11, "optRecord":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v18, 0x145

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v12

    .line 108
    .local v12, "property":Lorg/apache/poi/ddf/EscherProperty;
    if-eqz v12, :cond_7

    .line 109
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFPolygon;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v9, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPolygon;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V

    .line 110
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 111
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :cond_7
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v9, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V

    .line 113
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 115
    .end local v11    # "optRecord":Lorg/apache/poi/ddf/EscherOptRecord;
    .end local v12    # "property":Lorg/apache/poi/ddf/EscherProperty;
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_7
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v14, v0, v9, v1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V

    .line 116
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 118
    .end local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :sswitch_8
    new-instance v14, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/record/EscherAggregate;->getNoteRecordByObj(Lorg/apache/poi/hssf/record/ObjRecord;)Lorg/apache/poi/hssf/record/NoteRecord;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v14, v0, v9, v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFComment;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;Lorg/apache/poi/hssf/record/NoteRecord;)V

    .line 119
    .restart local v14    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    goto :goto_3

    .line 70
    :sswitch_data_0
    .sparse-switch
        -0xff3 -> :sswitch_0
        -0xfef -> :sswitch_1
    .end sparse-switch

    .line 92
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_3
        0x6 -> :sswitch_7
        0x8 -> :sswitch_2
        0x14 -> :sswitch_5
        0x19 -> :sswitch_8
        0x1e -> :sswitch_6
    .end sparse-switch
.end method

.method private static isEmbeddedObject(Lorg/apache/poi/hssf/record/ObjRecord;)Z
    .locals 3
    .param p0, "obj"    # Lorg/apache/poi/hssf/record/ObjRecord;

    .prologue
    .line 128
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 129
    .local v1, "subRecordIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/SubRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 135
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 130
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/SubRecord;

    .line 131
    .local v0, "sub":Lorg/apache/poi/hssf/record/SubRecord;
    instance-of v2, v0, Lorg/apache/poi/hssf/record/EmbeddedObjectRefSubRecord;

    if-eqz v2, :cond_0

    .line 132
    const/4 v2, 0x1

    goto :goto_0
.end method
