.class public final Lorg/apache/poi/hssf/record/chart/SeriesListRecord;
.super Lorg/apache/poi/hssf/record/StandardRecord;
.source "SeriesListRecord.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final sid:S = 0x1016s


# instance fields
.field private field_1_seriesNumbers:[S


# direct methods
.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 43
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUShort()I

    move-result v1

    .line 44
    .local v1, "nItems":I
    new-array v2, v1, [S

    .line 45
    .local v2, "ss":[S
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 49
    iput-object v2, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    .line 50
    return-void

    .line 46
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v3

    aput-short v3, v2, v0

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([S)V
    .locals 0
    .param p1, "seriesNumbers"    # [S

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    .line 40
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 81
    new-instance v1, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;

    iget-object v0, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    invoke-virtual {v0}, [S->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [S

    invoke-direct {v1, v0}, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;-><init>([S)V

    return-object v1
.end method

.method protected getDataSize()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getSeriesNumbers()[S
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    return-object v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 77
    const/16 v0, 0x1016

    return v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 3
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 65
    iget-object v2, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    array-length v1, v2

    .line 66
    .local v1, "nItems":I
    invoke-interface {p1, v1}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 67
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 70
    return-void

    .line 68
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->field_1_seriesNumbers:[S

    aget-short v2, v2, v0

    invoke-interface {p1, v2}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 55
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[SERIESLIST]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    const-string/jumbo v1, "    .seriesNumbers= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/chart/SeriesListRecord;->getSeriesNumbers()[S

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 57
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 59
    const-string/jumbo v1, "[/SERIESLIST]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
