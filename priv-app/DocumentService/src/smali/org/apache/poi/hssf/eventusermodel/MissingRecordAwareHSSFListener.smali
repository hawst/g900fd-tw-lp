.class public final Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;
.super Ljava/lang/Object;
.source "MissingRecordAwareHSSFListener.java"

# interfaces
.implements Lorg/apache/poi/hssf/eventusermodel/HSSFListener;


# instance fields
.field private childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

.field private lastCellColumn:I

.field private lastCellRow:I

.field private lastRowRow:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;)V
    .locals 0
    .param p1, "listener"    # Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-direct {p0}, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->resetCounts()V

    .line 54
    iput-object p1, p0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    .line 55
    return-void
.end method

.method private resetCounts()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 194
    iput v0, p0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastRowRow:I

    .line 195
    iput v0, p0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    .line 196
    iput v0, p0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    .line 197
    return-void
.end method


# virtual methods
.method public processRecord(Lorg/apache/poi/hssf/record/Record;)V
    .locals 18
    .param p1, "record"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 60
    const/4 v4, 0x0

    .line 62
    .local v4, "expandedRecords":[Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    move-object/from16 v0, p1

    instance-of v14, v0, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    if-eqz v14, :cond_8

    move-object/from16 v13, p1

    .line 63
    check-cast v13, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .line 64
    .local v13, "valueRec":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    invoke-interface {v13}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v12

    .line 65
    .local v12, "thisRow":I
    invoke-interface {v13}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v11

    .line 130
    .end local v13    # "valueRec":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    .local v11, "thisColumn":I
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    array-length v14, v4

    if-lez v14, :cond_1

    .line 131
    const/4 v14, 0x0

    aget-object v14, v4, v14

    invoke-interface {v14}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v12

    .line 132
    const/4 v14, 0x0

    aget-object v14, v4, v14

    invoke-interface {v14}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v11

    .line 138
    :cond_1
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    if-eq v12, v14, :cond_2

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    const/4 v15, -0x1

    if-le v14, v15, :cond_2

    .line 139
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    .local v5, "i":I
    :goto_1
    if-lt v5, v12, :cond_d

    .line 150
    .end local v5    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    const/4 v15, -0x1

    if-eq v14, v15, :cond_3

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    const/4 v15, -0x1

    if-eq v14, v15, :cond_3

    const/4 v14, -0x1

    if-ne v12, v14, :cond_3

    .line 151
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    new-instance v15, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/LastCellOfRowDummyRecord;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    move/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/LastCellOfRowDummyRecord;-><init>(II)V

    invoke-interface {v14, v15}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 153
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    .line 154
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    .line 159
    :cond_3
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    if-eq v12, v14, :cond_4

    .line 160
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    .line 165
    :cond_4
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    add-int/lit8 v15, v11, -0x1

    if-eq v14, v15, :cond_5

    .line 166
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    add-int/lit8 v5, v14, 0x1

    .restart local v5    # "i":I
    :goto_2
    if-lt v5, v11, :cond_f

    .line 172
    .end local v5    # "i":I
    :cond_5
    if-eqz v4, :cond_6

    array-length v14, v4

    if-lez v14, :cond_6

    .line 173
    array-length v14, v4

    add-int/lit8 v14, v14, -0x1

    aget-object v14, v4, v14

    invoke-interface {v14}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v11

    .line 178
    :cond_6
    const/4 v14, -0x1

    if-eq v11, v14, :cond_7

    .line 179
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    .line 180
    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    .line 184
    :cond_7
    if-eqz v4, :cond_11

    array-length v14, v4

    if-lez v14, :cond_11

    .line 185
    array-length v15, v4

    const/4 v14, 0x0

    :goto_3
    if-lt v14, v15, :cond_10

    .line 191
    .end local v11    # "thisColumn":I
    .end local v12    # "thisRow":I
    :goto_4
    return-void

    .line 67
    :cond_8
    move-object/from16 v0, p1

    instance-of v14, v0, Lorg/apache/poi/hssf/record/StringRecord;

    if-eqz v14, :cond_9

    .line 69
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_4

    .line 72
    :cond_9
    const/4 v12, -0x1

    .line 73
    .restart local v12    # "thisRow":I
    const/4 v11, -0x1

    .line 75
    .restart local v11    # "thisColumn":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v14

    sparse-switch v14, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    move-object/from16 v8, p1

    .line 122
    check-cast v8, Lorg/apache/poi/hssf/record/NoteRecord;

    .line 123
    .local v8, "nrec":Lorg/apache/poi/hssf/record/NoteRecord;
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/NoteRecord;->getRow()I

    move-result v12

    .line 124
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/NoteRecord;->getColumn()I

    move-result v11

    goto/16 :goto_0

    .end local v8    # "nrec":Lorg/apache/poi/hssf/record/NoteRecord;
    :sswitch_1
    move-object/from16 v1, p1

    .line 79
    check-cast v1, Lorg/apache/poi/hssf/record/BOFRecord;

    .line 80
    .local v1, "bof":Lorg/apache/poi/hssf/record/BOFRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/BOFRecord;->getType()I

    move-result v14

    const/4 v15, 0x5

    if-eq v14, v15, :cond_a

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/BOFRecord;->getType()I

    move-result v14

    const/16 v15, 0x10

    if-ne v14, v15, :cond_0

    .line 82
    :cond_a
    invoke-direct/range {p0 .. p0}, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->resetCounts()V

    goto/16 :goto_0

    .end local v1    # "bof":Lorg/apache/poi/hssf/record/BOFRecord;
    :sswitch_2
    move-object/from16 v10, p1

    .line 86
    check-cast v10, Lorg/apache/poi/hssf/record/RowRecord;

    .line 91
    .local v10, "rowrec":Lorg/apache/poi/hssf/record/RowRecord;
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastRowRow:I

    add-int/lit8 v14, v14, 0x1

    invoke-virtual {v10}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v15

    if-ge v14, v15, :cond_b

    .line 92
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastRowRow:I

    add-int/lit8 v5, v14, 0x1

    .restart local v5    # "i":I
    :goto_5
    invoke-virtual {v10}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v14

    if-lt v5, v14, :cond_c

    .line 99
    .end local v5    # "i":I
    :cond_b
    invoke-virtual {v10}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastRowRow:I

    goto/16 :goto_0

    .line 93
    .restart local v5    # "i":I
    :cond_c
    new-instance v3, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/MissingRowDummyRecord;

    invoke-direct {v3, v5}, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/MissingRowDummyRecord;-><init>(I)V

    .line 94
    .local v3, "dr":Lorg/apache/poi/hssf/eventusermodel/dummyrecord/MissingRowDummyRecord;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    invoke-interface {v14, v3}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 92
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 106
    .end local v3    # "dr":Lorg/apache/poi/hssf/eventusermodel/dummyrecord/MissingRowDummyRecord;
    .end local v5    # "i":I
    .end local v10    # "rowrec":Lorg/apache/poi/hssf/record/RowRecord;
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_4

    :sswitch_4
    move-object/from16 v6, p1

    .line 112
    check-cast v6, Lorg/apache/poi/hssf/record/MulBlankRecord;

    .line 113
    .local v6, "mbr":Lorg/apache/poi/hssf/record/MulBlankRecord;
    invoke-static {v6}, Lorg/apache/poi/hssf/record/RecordFactory;->convertBlankRecords(Lorg/apache/poi/hssf/record/MulBlankRecord;)[Lorg/apache/poi/hssf/record/BlankRecord;

    move-result-object v4

    .line 114
    goto/16 :goto_0

    .end local v6    # "mbr":Lorg/apache/poi/hssf/record/MulBlankRecord;
    :sswitch_5
    move-object/from16 v7, p1

    .line 118
    check-cast v7, Lorg/apache/poi/hssf/record/MulRKRecord;

    .line 119
    .local v7, "mrk":Lorg/apache/poi/hssf/record/MulRKRecord;
    invoke-static {v7}, Lorg/apache/poi/hssf/record/RecordFactory;->convertRKRecords(Lorg/apache/poi/hssf/record/MulRKRecord;)[Lorg/apache/poi/hssf/record/NumberRecord;

    move-result-object v4

    .line 120
    goto/16 :goto_0

    .line 140
    .end local v7    # "mrk":Lorg/apache/poi/hssf/record/MulRKRecord;
    .restart local v5    # "i":I
    :cond_d
    const/4 v2, -0x1

    .line 141
    .local v2, "cols":I
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellRow:I

    if-ne v5, v14, :cond_e

    .line 142
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->lastCellColumn:I

    .line 144
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    new-instance v15, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/LastCellOfRowDummyRecord;

    invoke-direct {v15, v5, v2}, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/LastCellOfRowDummyRecord;-><init>(II)V

    invoke-interface {v14, v15}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 139
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 167
    .end local v2    # "cols":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    new-instance v15, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/MissingCellDummyRecord;

    invoke-direct {v15, v12, v5}, Lorg/apache/poi/hssf/eventusermodel/dummyrecord/MissingCellDummyRecord;-><init>(II)V

    invoke-interface {v14, v15}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 166
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 185
    .end local v5    # "i":I
    :cond_10
    aget-object v9, v4, v14

    .line 186
    .local v9, "r":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    move-object/from16 v16, v0

    check-cast v9, Lorg/apache/poi/hssf/record/Record;

    .end local v9    # "r":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 185
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    .line 189
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/eventusermodel/MissingRecordAwareHSSFListener;->childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto/16 :goto_4

    .line 75
    nop

    :sswitch_data_0
    .sparse-switch
        0x1c -> :sswitch_0
        0xbd -> :sswitch_5
        0xbe -> :sswitch_4
        0x208 -> :sswitch_2
        0x4bc -> :sswitch_3
        0x809 -> :sswitch_1
    .end sparse-switch
.end method
