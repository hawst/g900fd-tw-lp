.class public final Lorg/apache/poi/hssf/util/HSSFColor$PLUM;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PLUM"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "9999:3333:6666"

.field public static final index:S = 0x3ds

.field public static final index2:S = 0x19s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1160
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$PLUM;->triplet:[S

    .line 1164
    return-void

    .line 1161
    nop

    :array_0
    .array-data 2
        0x99s
        0x33s
        0x66s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1155
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1178
    const-string/jumbo v0, "9999:3333:6666"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1168
    const/16 v0, 0x3d

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1173
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$PLUM;->triplet:[S

    return-object v0
.end method
