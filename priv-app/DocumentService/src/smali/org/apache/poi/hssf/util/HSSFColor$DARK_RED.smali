.class public final Lorg/apache/poi/hssf/util/HSSFColor$DARK_RED;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DARK_RED"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "8080:0:0"

.field public static final index:S = 0x10s

.field public static final index2:S = 0x25s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 471
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 472
    const/16 v2, 0x80

    aput-short v2, v0, v1

    .line 470
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$DARK_RED;->triplet:[S

    .line 474
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 488
    const-string/jumbo v0, "8080:0:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 478
    const/16 v0, 0x10

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$DARK_RED;->triplet:[S

    return-object v0
.end method
