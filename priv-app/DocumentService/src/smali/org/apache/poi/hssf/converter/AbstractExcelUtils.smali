.class public Lorg/apache/poi/hssf/converter/AbstractExcelUtils;
.super Ljava/lang/Object;
.source "AbstractExcelUtils.java"


# static fields
.field static final EMPTY:Ljava/lang/String; = ""

.field private static final EXCEL_COLUMN_WIDTH_FACTOR:S = 0x100s

.field private static final UNIT_OFFSET_LENGTH:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAlign(S)Ljava/lang/String;
    .locals 1
    .param p0, "alignment"    # S

    .prologue
    .line 47
    packed-switch p0, :pswitch_data_0

    .line 65
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    const-string/jumbo v0, "center"

    goto :goto_0

    .line 52
    :pswitch_1
    const-string/jumbo v0, "center"

    goto :goto_0

    .line 55
    :pswitch_2
    const-string/jumbo v0, ""

    goto :goto_0

    .line 57
    :pswitch_3
    const-string/jumbo v0, ""

    goto :goto_0

    .line 59
    :pswitch_4
    const-string/jumbo v0, "justify"

    goto :goto_0

    .line 61
    :pswitch_5
    const-string/jumbo v0, "left"

    goto :goto_0

    .line 63
    :pswitch_6
    const-string/jumbo v0, "right"

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public static getBorderStyle(S)Ljava/lang/String;
    .locals 1
    .param p0, "xlsBorder"    # S

    .prologue
    .line 72
    packed-switch p0, :pswitch_data_0

    .line 94
    :pswitch_0
    const-string/jumbo v0, "solid"

    .line 97
    .local v0, "borderStyle":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 75
    .end local v0    # "borderStyle":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v0, "none"

    .line 76
    .restart local v0    # "borderStyle":Ljava/lang/String;
    goto :goto_0

    .line 84
    .end local v0    # "borderStyle":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v0, "dotted"

    .line 85
    .restart local v0    # "borderStyle":Ljava/lang/String;
    goto :goto_0

    .line 88
    .end local v0    # "borderStyle":Ljava/lang/String;
    :pswitch_3
    const-string/jumbo v0, "dashed"

    .line 89
    .restart local v0    # "borderStyle":Ljava/lang/String;
    goto :goto_0

    .line 91
    .end local v0    # "borderStyle":Ljava/lang/String;
    :pswitch_4
    const-string/jumbo v0, "double"

    .line 92
    .restart local v0    # "borderStyle":Ljava/lang/String;
    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static getBorderWidth(S)Ljava/lang/String;
    .locals 1
    .param p0, "xlsBorder"    # S

    .prologue
    .line 103
    packed-switch p0, :pswitch_data_0

    .line 114
    :pswitch_0
    const-string/jumbo v0, "thin"

    .line 117
    .local v0, "borderWidth":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 108
    .end local v0    # "borderWidth":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v0, "2pt"

    .line 109
    .restart local v0    # "borderWidth":Ljava/lang/String;
    goto :goto_0

    .line 111
    .end local v0    # "borderWidth":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v0, "thick"

    .line 112
    .restart local v0    # "borderWidth":Ljava/lang/String;
    goto :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;
    .locals 7
    .param p0, "color"    # Lorg/apache/poi/hssf/util/HSSFColor;

    .prologue
    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 123
    .local v2, "stringBuilder":Ljava/lang/StringBuilder;
    const/16 v3, 0x23

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {p0}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_1

    .line 131
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "result":Ljava/lang/String;
    const-string/jumbo v3, "#ffffff"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 134
    const-string/jumbo v0, "white"

    .line 145
    .end local v0    # "result":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v0

    .line 124
    :cond_1
    aget-short v1, v4, v3

    .line 126
    .local v1, "s":S
    const/16 v6, 0xa

    if-ge v1, v6, :cond_2

    .line 127
    const/16 v6, 0x30

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 136
    .end local v1    # "s":S
    .restart local v0    # "result":Ljava/lang/String;
    :cond_3
    const-string/jumbo v3, "#c0c0c0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 137
    const-string/jumbo v0, "silver"

    goto :goto_1

    .line 139
    :cond_4
    const-string/jumbo v3, "#808080"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 140
    const-string/jumbo v0, "gray"

    goto :goto_1

    .line 142
    :cond_5
    const-string/jumbo v3, "#000000"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    const-string/jumbo v0, "black"

    goto :goto_1
.end method

.method public static getColumnWidthInPx(I)I
    .locals 4
    .param p0, "widthUnits"    # I

    .prologue
    .line 155
    div-int/lit16 v2, p0, 0x100

    mul-int/lit8 v1, v2, 0x7

    .line 158
    .local v1, "pixels":I
    rem-int/lit16 v0, p0, 0x100

    .line 159
    .local v0, "offsetWidthUnits":I
    int-to-float v2, v0

    .line 160
    const v3, 0x42124925

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v1, v2

    .line 162
    return v1
.end method

.method public static getMergedRange([[Lorg/apache/poi/ss/util/CellRangeAddress;II)Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 3
    .param p0, "mergedRanges"    # [[Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p1, "rowNumber"    # I
    .param p2, "columnNumber"    # I

    .prologue
    const/4 v0, 0x0

    .line 175
    array-length v2, p0

    if-ge p1, v2, :cond_1

    aget-object v1, p0, p1

    .line 177
    .local v1, "mergedRangeRowInfo":[Lorg/apache/poi/ss/util/CellRangeAddress;
    :goto_0
    if-eqz v1, :cond_0

    .line 178
    array-length v2, v1

    if-ge p2, v2, :cond_0

    aget-object v0, v1, p2

    .line 181
    .local v0, "cellRangeAddress":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_0
    return-object v0

    .end local v0    # "cellRangeAddress":Lorg/apache/poi/ss/util/CellRangeAddress;
    .end local v1    # "mergedRangeRowInfo":[Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_1
    move-object v1, v0

    .line 176
    goto :goto_0
.end method

.method static isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 186
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static isNotEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-static {p0}, Lorg/apache/poi/hssf/converter/AbstractExcelUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static loadXls(Ljava/io/File;)Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .locals 2
    .param p0, "xlsFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 199
    .local v0, "inputStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 199
    return-object v1

    .line 202
    :catchall_0
    move-exception v1

    .line 203
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 204
    throw v1
.end method
