.class public final Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
.super Ljava/lang/Object;
.source "Biff8EncryptionKey.java"


# static fields
.field private static final KEY_DIGEST_LENGTH:I = 0x5

.field private static final PASSWORD_HASH_NUMBER_OF_BYTES_USED:I = 0x5

.field private static final _userPasswordTLS:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _keyDigest:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->_userPasswordTLS:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>([B)V
    .locals 3
    .param p1, "keyDigest"    # [B

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    array-length v0, p1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Expected 5 byte key digest, but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->_keyDigest:[B

    .line 51
    return-void
.end method

.method private static check16Bytes([BLjava/lang/String;)V
    .locals 3
    .param p0, "data"    # [B
    .param p1, "argName"    # Ljava/lang/String;

    .prologue
    .line 125
    array-length v0, p0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Expected 16 byte "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    return-void
.end method

.method public static create(Ljava/lang/String;[B)Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
    .locals 2
    .param p0, "password"    # Ljava/lang/String;
    .param p1, "docIdData"    # [B

    .prologue
    .line 43
    new-instance v0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    invoke-static {p0, p1}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->createKeyDigest(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;-><init>([B)V

    return-object v0
.end method

.method public static create([B)Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
    .locals 2
    .param p0, "docId"    # [B

    .prologue
    .line 40
    new-instance v0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;

    const-string/jumbo v1, "VelvetSweatshop"

    invoke-static {v1, p0}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->createKeyDigest(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;-><init>([B)V

    return-object v0
.end method

.method static createKeyDigest(Ljava/lang/String;[B)[B
    .locals 14
    .param p0, "password"    # Ljava/lang/String;
    .param p1, "docIdData"    # [B

    .prologue
    const/16 v13, 0x10

    const/4 v12, 0x5

    const/4 v11, 0x0

    .line 54
    const-string/jumbo v9, "docId"

    invoke-static {p1, v9}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->check16Bytes([BLjava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    invoke-static {v9, v13}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 56
    .local v5, "nChars":I
    mul-int/lit8 v9, v5, 0x2

    new-array v6, v9, [B

    .line 57
    .local v6, "passwordData":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 66
    :try_start_0
    const-string/jumbo v9, "MD5"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 71
    .local v4, "md5":Ljava/security/MessageDigest;
    invoke-virtual {v4, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 72
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    .line 73
    .local v7, "passwordHash":[B
    invoke-virtual {v4}, Ljava/security/MessageDigest;->reset()V

    .line 75
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v13, :cond_1

    .line 79
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    .line 80
    .local v3, "kd":[B
    new-array v8, v12, [B

    .line 81
    .local v8, "result":[B
    invoke-static {v3, v11, v8, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    return-object v8

    .line 58
    .end local v3    # "kd":[B
    .end local v4    # "md5":Ljava/security/MessageDigest;
    .end local v7    # "passwordHash":[B
    .end local v8    # "result":[B
    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 59
    .local v0, "ch":C
    mul-int/lit8 v9, v2, 0x2

    add-int/lit8 v9, v9, 0x0

    shl-int/lit8 v10, v0, 0x0

    and-int/lit16 v10, v10, 0xff

    int-to-byte v10, v10

    aput-byte v10, v6, v9

    .line 60
    mul-int/lit8 v9, v2, 0x2

    add-int/lit8 v9, v9, 0x1

    shl-int/lit8 v10, v0, 0x8

    and-int/lit16 v10, v10, 0xff

    int-to-byte v10, v10

    aput-byte v10, v6, v9

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "ch":C
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 76
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v4    # "md5":Ljava/security/MessageDigest;
    .restart local v7    # "passwordHash":[B
    :cond_1
    invoke-virtual {v4, v7, v11, v12}, Ljava/security/MessageDigest;->update([BII)V

    .line 77
    array-length v9, p1

    invoke-virtual {v4, p1, v11, v9}, Ljava/security/MessageDigest;->update([BII)V

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static getCurrentUserPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->_userPasswordTLS:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static setCurrentUserPassword(Ljava/lang/String;)V
    .locals 1
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    .line 165
    sget-object v0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->_userPasswordTLS:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 166
    return-void
.end method

.method private static xor([B[B)[B
    .locals 4
    .param p0, "a"    # [B
    .param p1, "b"    # [B

    .prologue
    .line 118
    array-length v2, p0

    new-array v0, v2, [B

    .line 119
    .local v0, "c":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_0

    .line 122
    return-object v0

    .line 120
    :cond_0
    aget-byte v2, p0, v1

    aget-byte v3, p1, v1

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method createRC4(I)Lorg/apache/poi/hssf/record/crypto/RC4;
    .locals 5
    .param p1, "keyBlockNo"    # I

    .prologue
    .line 137
    :try_start_0
    const-string/jumbo v4, "MD5"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 142
    .local v3, "md5":Ljava/security/MessageDigest;
    iget-object v4, p0, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->_keyDigest:[B

    invoke-virtual {v3, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 143
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/4 v4, 0x4

    invoke-direct {v0, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 144
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Lorg/apache/poi/util/LittleEndianOutputStream;

    invoke-direct {v4, v0}, Lorg/apache/poi/util/LittleEndianOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v4, p1}, Lorg/apache/poi/util/LittleEndianOutputStream;->writeInt(I)V

    .line 145
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 147
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 148
    .local v1, "digest":[B
    new-instance v4, Lorg/apache/poi/hssf/record/crypto/RC4;

    invoke-direct {v4, v1}, Lorg/apache/poi/hssf/record/crypto/RC4;-><init>([B)V

    return-object v4

    .line 138
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "digest":[B
    .end local v3    # "md5":Ljava/security/MessageDigest;
    :catch_0
    move-exception v2

    .line 139
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public validate([B[B)Z
    .locals 7
    .param p1, "saltData"    # [B
    .param p2, "saltHash"    # [B

    .prologue
    .line 89
    const-string/jumbo v6, "saltData"

    invoke-static {p1, v6}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->check16Bytes([BLjava/lang/String;)V

    .line 90
    const-string/jumbo v6, "saltHash"

    invoke-static {p2, v6}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->check16Bytes([BLjava/lang/String;)V

    .line 93
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;->createRC4(I)Lorg/apache/poi/hssf/record/crypto/RC4;

    move-result-object v3

    .line 94
    .local v3, "rc4":Lorg/apache/poi/hssf/record/crypto/RC4;
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    .line 95
    .local v4, "saltDataPrime":[B
    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/record/crypto/RC4;->encrypt([B)V

    .line 97
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    .line 98
    .local v5, "saltHashPrime":[B
    invoke-virtual {v3, v5}, Lorg/apache/poi/hssf/record/crypto/RC4;->encrypt([B)V

    .line 102
    :try_start_0
    const-string/jumbo v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 106
    .local v2, "md5":Ljava/security/MessageDigest;
    invoke-virtual {v2, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 107
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 114
    .local v1, "finalSaltResult":[B
    invoke-static {v5, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    return v6

    .line 103
    .end local v1    # "finalSaltResult":[B
    .end local v2    # "md5":Ljava/security/MessageDigest;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method
