.class public final Lorg/apache/poi/hssf/util/HSSFColor$YELLOW;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "YELLOW"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:FFFF:0"

.field public static final index:S = 0xds

.field public static final index2:S = 0x22s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 1034
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 1035
    aput-short v2, v0, v1

    const/4 v1, 0x1

    aput-short v2, v0, v1

    .line 1033
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$YELLOW;->triplet:[S

    .line 1037
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1028
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1051
    const-string/jumbo v0, "FFFF:FFFF:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1041
    const/16 v0, 0xd

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1046
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$YELLOW;->triplet:[S

    return-object v0
.end method
