.class public final Lorg/apache/poi/hssf/util/HSSFColor$SKY_BLUE;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SKY_BLUE"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:CCCC:FFFF"

.field public static final index:S = 0x28s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1129
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x1

    .line 1130
    const/16 v2, 0xcc

    aput-short v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0xff

    aput-short v2, v0, v1

    .line 1128
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$SKY_BLUE;->triplet:[S

    .line 1132
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1124
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1146
    const-string/jumbo v0, "0:CCCC:FFFF"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1136
    const/16 v0, 0x28

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1141
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$SKY_BLUE;->triplet:[S

    return-object v0
.end method
