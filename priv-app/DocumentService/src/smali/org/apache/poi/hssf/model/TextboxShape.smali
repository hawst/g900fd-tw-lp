.class public Lorg/apache/poi/hssf/model/TextboxShape;
.super Lorg/apache/poi/hssf/model/AbstractShape;
.source "TextboxShape.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private escherTextbox:Lorg/apache/poi/ddf/EscherTextboxRecord;

.field private objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

.field private spContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

.field private textObjectRecord:Lorg/apache/poi/hssf/record/TextObjectRecord;


# direct methods
.method constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)V
    .locals 1
    .param p1, "hssfShape"    # Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
    .param p2, "shapeId"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/AbstractShape;-><init>()V

    .line 48
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/model/TextboxShape;->createSpContainer(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->spContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 49
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/model/TextboxShape;->createObjRecord(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

    .line 50
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/model/TextboxShape;->createTextObjectRecord(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)Lorg/apache/poi/hssf/record/TextObjectRecord;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->textObjectRecord:Lorg/apache/poi/hssf/record/TextObjectRecord;

    .line 51
    return-void
.end method

.method private createObjRecord(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)Lorg/apache/poi/hssf/record/ObjRecord;
    .locals 6
    .param p1, "hssfShape"    # Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
    .param p2, "shapeId"    # I

    .prologue
    const/4 v5, 0x1

    .line 58
    move-object v3, p1

    .line 60
    .local v3, "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    new-instance v2, Lorg/apache/poi/hssf/record/ObjRecord;

    invoke-direct {v2}, Lorg/apache/poi/hssf/record/ObjRecord;-><init>()V

    .line 61
    .local v2, "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    new-instance v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;-><init>()V

    .line 62
    .local v0, "c":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    check-cast v3, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;

    .end local v3    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;->getShapeType()I

    move-result v4

    int-to-short v4, v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 63
    invoke-virtual {p0, p2}, Lorg/apache/poi/hssf/model/TextboxShape;->getCmoObjectId(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectId(I)V

    .line 64
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setLocked(Z)V

    .line 65
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setPrintable(Z)V

    .line 66
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setAutofill(Z)V

    .line 67
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setAutoline(Z)V

    .line 68
    new-instance v1, Lorg/apache/poi/hssf/record/EndSubRecord;

    invoke-direct {v1}, Lorg/apache/poi/hssf/record/EndSubRecord;-><init>()V

    .line 70
    .local v1, "e":Lorg/apache/poi/hssf/record/EndSubRecord;
    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/record/ObjRecord;->addSubRecord(Lorg/apache/poi/hssf/record/SubRecord;)Z

    .line 71
    invoke-virtual {v2, v1}, Lorg/apache/poi/hssf/record/ObjRecord;->addSubRecord(Lorg/apache/poi/hssf/record/SubRecord;)Z

    .line 73
    return-object v2
.end method

.method private createSpContainer(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 11
    .param p1, "hssfShape"    # Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
    .param p2, "shapeId"    # I

    .prologue
    const/4 v10, 0x0

    .line 84
    move-object v3, p1

    .line 86
    .local v3, "shape":Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
    new-instance v5, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v5}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 87
    .local v5, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v4, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-direct {v4}, Lorg/apache/poi/ddf/EscherSpRecord;-><init>()V

    .line 88
    .local v4, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    new-instance v2, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-direct {v2}, Lorg/apache/poi/ddf/EscherOptRecord;-><init>()V

    .line 89
    .local v2, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    new-instance v0, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;-><init>()V

    .line 90
    .local v0, "anchor":Lorg/apache/poi/ddf/EscherRecord;
    new-instance v1, Lorg/apache/poi/ddf/EscherClientDataRecord;

    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherClientDataRecord;-><init>()V

    .line 91
    .local v1, "clientData":Lorg/apache/poi/ddf/EscherClientDataRecord;
    new-instance v7, Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-direct {v7}, Lorg/apache/poi/ddf/EscherTextboxRecord;-><init>()V

    iput-object v7, p0, Lorg/apache/poi/hssf/model/TextboxShape;->escherTextbox:Lorg/apache/poi/ddf/EscherTextboxRecord;

    .line 93
    const/16 v7, -0xffc

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 94
    const/16 v7, 0xf

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 95
    const/16 v7, -0xff6

    invoke-virtual {v4, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setRecordId(S)V

    .line 96
    const/16 v7, 0xca2

    invoke-virtual {v4, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 98
    invoke-virtual {v4, p2}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    .line 99
    const/16 v7, 0xa00

    invoke-virtual {v4, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 100
    const/16 v7, -0xff5

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->setRecordId(S)V

    .line 102
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x80

    invoke-direct {v7, v8, v10}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 103
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x81

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getMarginLeft()I

    move-result v9

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 104
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x83

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getMarginRight()I

    move-result v9

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 105
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x84

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getMarginBottom()I

    move-result v9

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 106
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x82

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getMarginTop()I

    move-result v9

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 108
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x85

    invoke-direct {v7, v8, v10}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 109
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x87

    invoke-direct {v7, v8, v10}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 110
    new-instance v7, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x3bf

    const/high16 v9, 0x80000

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 112
    invoke-virtual {p0, v3, v2}, Lorg/apache/poi/hssf/model/TextboxShape;->addStandardOptions(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/ddf/EscherOptRecord;)I

    .line 113
    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getAnchor()Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    move-result-object v6

    .line 118
    .local v6, "userAnchor":Lorg/apache/poi/hssf/usermodel/HSSFAnchor;
    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/model/TextboxShape;->createAnchor(Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    .line 119
    const/16 v7, -0xfef

    invoke-virtual {v1, v7}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setRecordId(S)V

    .line 120
    invoke-virtual {v1, v10}, Lorg/apache/poi/ddf/EscherClientDataRecord;->setOptions(S)V

    .line 121
    iget-object v7, p0, Lorg/apache/poi/hssf/model/TextboxShape;->escherTextbox:Lorg/apache/poi/ddf/EscherTextboxRecord;

    const/16 v8, -0xff3

    invoke-virtual {v7, v8}, Lorg/apache/poi/ddf/EscherTextboxRecord;->setRecordId(S)V

    .line 122
    iget-object v7, p0, Lorg/apache/poi/hssf/model/TextboxShape;->escherTextbox:Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v7, v10}, Lorg/apache/poi/ddf/EscherTextboxRecord;->setOptions(S)V

    .line 124
    invoke-virtual {v5, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 125
    invoke-virtual {v5, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 126
    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 127
    invoke-virtual {v5, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 128
    iget-object v7, p0, Lorg/apache/poi/hssf/model/TextboxShape;->escherTextbox:Lorg/apache/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 130
    return-object v5
.end method

.method private createTextObjectRecord(Lorg/apache/poi/hssf/usermodel/HSSFTextbox;I)Lorg/apache/poi/hssf/record/TextObjectRecord;
    .locals 3
    .param p1, "hssfShape"    # Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
    .param p2, "shapeId"    # I

    .prologue
    .line 139
    move-object v1, p1

    .line 141
    .local v1, "shape":Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
    new-instance v0, Lorg/apache/poi/hssf/record/TextObjectRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/TextObjectRecord;-><init>()V

    .line 142
    .local v0, "obj":Lorg/apache/poi/hssf/record/TextObjectRecord;
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getHorizontalAlignment()S

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/TextObjectRecord;->setHorizontalTextAlignment(I)V

    .line 143
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getVerticalAlignment()S

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/TextObjectRecord;->setVerticalTextAlignment(I)V

    .line 144
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/TextObjectRecord;->setTextLocked(Z)V

    .line 145
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/TextObjectRecord;->setTextOrientation(I)V

    .line 146
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->getString()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/TextObjectRecord;->setStr(Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;)V

    .line 148
    return-object v0
.end method


# virtual methods
.method public getEscherTextbox()Lorg/apache/poi/ddf/EscherRecord;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->escherTextbox:Lorg/apache/poi/ddf/EscherTextboxRecord;

    return-object v0
.end method

.method public getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->objRecord:Lorg/apache/poi/hssf/record/ObjRecord;

    return-object v0
.end method

.method public getSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->spContainer:Lorg/apache/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getTextObjectRecord()Lorg/apache/poi/hssf/record/TextObjectRecord;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/poi/hssf/model/TextboxShape;->textObjectRecord:Lorg/apache/poi/hssf/record/TextObjectRecord;

    return-object v0
.end method
