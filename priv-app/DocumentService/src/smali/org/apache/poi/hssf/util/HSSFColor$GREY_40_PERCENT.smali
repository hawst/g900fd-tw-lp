.class public final Lorg/apache/poi/hssf/util/HSSFColor$GREY_40_PERCENT;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GREY_40_PERCENT"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "9696:9696:9696"

.field public static final index:S = 0x37s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 939
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 938
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$GREY_40_PERCENT;->triplet:[S

    .line 942
    return-void

    .line 939
    nop

    :array_0
    .array-data 2
        0x96s
        0x96s
        0x96s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 934
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 956
    const-string/jumbo v0, "9696:9696:9696"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 946
    const/16 v0, 0x37

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 951
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$GREY_40_PERCENT;->triplet:[S

    return-object v0
.end method
