.class public final Lorg/apache/poi/hssf/util/HSSFColor$LIGHT_GREEN;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LIGHT_GREEN"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "CCCC:FFFF:CCCC"

.field public static final index:S = 0x2as

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1316
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1315
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LIGHT_GREEN;->triplet:[S

    .line 1319
    return-void

    .line 1316
    nop

    :array_0
    .array-data 2
        0xccs
        0xffs
        0xccs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1311
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1333
    const-string/jumbo v0, "CCCC:FFFF:CCCC"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1323
    const/16 v0, 0x2a

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1328
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LIGHT_GREEN;->triplet:[S

    return-object v0
.end method
