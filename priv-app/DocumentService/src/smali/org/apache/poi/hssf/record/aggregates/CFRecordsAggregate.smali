.class public final Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;
.super Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;
.source "CFRecordsAggregate.java"


# static fields
.field private static final MAX_CONDTIONAL_FORMAT_RULES:I = 0x3


# instance fields
.field private final header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

.field private final rules:Ljava/util/List;


# direct methods
.method private constructor <init>(Lorg/apache/poi/hssf/record/CFHeaderRecord;[Lorg/apache/poi/hssf/record/CFRuleRecord;)V
    .locals 4
    .param p1, "pHeader"    # Lorg/apache/poi/hssf/record/CFHeaderRecord;
    .param p2, "pRules"    # [Lorg/apache/poi/hssf/record/CFRuleRecord;

    .prologue
    const/4 v3, 0x3

    .line 50
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 51
    if-nez p1, :cond_0

    .line 52
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "header must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 54
    :cond_0
    if-nez p2, :cond_1

    .line 55
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "rules must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_1
    array-length v1, p2

    if-le v1, v3, :cond_2

    .line 58
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "No more than 3 rules may be specified"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 61
    :cond_2
    array-length v1, p2

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->getNumberOfConditionalFormats()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 62
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Mismatch number of rules"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 64
    :cond_3
    iput-object p1, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_4

    .line 69
    return-void

    .line 67
    :cond_4
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    aget-object v2, p2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/poi/ss/util/CellRangeAddress;[Lorg/apache/poi/hssf/record/CFRuleRecord;)V
    .locals 2
    .param p1, "regions"    # [Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "rules"    # [Lorg/apache/poi/hssf/record/CFRuleRecord;

    .prologue
    .line 72
    new-instance v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;

    array-length v1, p2

    invoke-direct {v0, p1, v1}, Lorg/apache/poi/hssf/record/CFHeaderRecord;-><init>([Lorg/apache/poi/ss/util/CellRangeAddress;I)V

    invoke-direct {p0, v0, p2}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;-><init>(Lorg/apache/poi/hssf/record/CFHeaderRecord;[Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    .line 73
    return-void
.end method

.method private checkRuleIndex(I)V
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 120
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Bad rule record index ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 122
    const-string/jumbo v2, ") nRules="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_1
    return-void
.end method

.method public static createCFAggregate(Lorg/apache/poi/hssf/model/RecordStream;)Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;
    .locals 9
    .param p0, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;

    .prologue
    const/16 v8, 0x1b0

    .line 81
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    .line 82
    .local v3, "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v5

    if-eq v5, v8, :cond_0

    .line 83
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "next record sid was "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 84
    const-string/jumbo v7, " instead of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " as expected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 83
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    move-object v0, v3

    .line 87
    check-cast v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;

    .line 88
    .local v0, "header":Lorg/apache/poi/hssf/record/CFHeaderRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->getNumberOfConditionalFormats()I

    move-result v2

    .line 90
    .local v2, "nRules":I
    new-array v4, v2, [Lorg/apache/poi/hssf/record/CFRuleRecord;

    .line 91
    .local v4, "rules":[Lorg/apache/poi/hssf/record/CFRuleRecord;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v4

    if-lt v1, v5, :cond_1

    .line 95
    new-instance v5, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;

    invoke-direct {v5, v0, v4}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;-><init>(Lorg/apache/poi/hssf/record/CFHeaderRecord;[Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    return-object v5

    .line 92
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hssf/record/CFRuleRecord;

    aput-object v5, v4, v1

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static shiftRange(Lorg/apache/poi/ss/formula/FormulaShifter;Lorg/apache/poi/ss/util/CellRangeAddress;I)Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 12
    .param p0, "shifter"    # Lorg/apache/poi/ss/formula/FormulaShifter;
    .param p1, "cra"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "currentExternSheetIx"    # I

    .prologue
    const/4 v5, 0x0

    .line 227
    new-instance v0, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v4

    move v6, v5

    move v7, v5

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;-><init>(IIIIZZZZ)V

    .line 228
    .local v0, "aptg":Lorg/apache/poi/ss/formula/ptg/AreaPtg;
    const/4 v1, 0x1

    new-array v11, v1, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    aput-object v0, v11, v5

    .line 230
    .local v11, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {p0, v11, p2}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 239
    .end local p1    # "cra":Lorg/apache/poi/ss/util/CellRangeAddress;
    :goto_0
    return-object p1

    .line 233
    .restart local p1    # "cra":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_0
    aget-object v10, v11, v5

    .line 234
    .local v10, "ptg0":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v1, v10, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    if-eqz v1, :cond_1

    move-object v9, v10

    .line 235
    check-cast v9, Lorg/apache/poi/ss/formula/ptg/AreaPtg;

    .line 236
    .local v9, "bptg":Lorg/apache/poi/ss/formula/ptg/AreaPtg;
    new-instance p1, Lorg/apache/poi/ss/util/CellRangeAddress;

    .end local p1    # "cra":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getFirstRow()I

    move-result v1

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getLastRow()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getFirstColumn()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/ss/formula/ptg/AreaPtg;->getLastColumn()I

    move-result v4

    invoke-direct {p1, v1, v2, v3, v4}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    goto :goto_0

    .line 238
    .end local v9    # "bptg":Lorg/apache/poi/ss/formula/ptg/AreaPtg;
    .restart local p1    # "cra":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_1
    instance-of v1, v10, Lorg/apache/poi/ss/formula/ptg/AreaErrPtg;

    if-eqz v1, :cond_2

    .line 239
    const/4 p1, 0x0

    goto :goto_0

    .line 241
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unexpected shifted ptg class ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public addRule(Lorg/apache/poi/hssf/record/CFRuleRecord;)V
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/hssf/record/CFRuleRecord;

    .prologue
    .line 137
    if-nez p1, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 141
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot have more than 3 conditional format rules"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->setNumberOfConditionalFormats(I)V

    .line 146
    return-void
.end method

.method public cloneCFAggregate()Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;
    .locals 4

    .prologue
    .line 104
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Lorg/apache/poi/hssf/record/CFRuleRecord;

    .line 105
    .local v1, "newRecs":[Lorg/apache/poi/hssf/record/CFRuleRecord;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 108
    new-instance v3, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;

    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-direct {v3, v2, v1}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;-><init>(Lorg/apache/poi/hssf/record/CFHeaderRecord;[Lorg/apache/poi/hssf/record/CFRuleRecord;)V

    return-object v3

    .line 106
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->getRule(I)Lorg/apache/poi/hssf/record/CFRuleRecord;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/CFRuleRecord;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/CFRuleRecord;

    aput-object v2, v1, v0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getHeader()Lorg/apache/poi/hssf/record/CFHeaderRecord;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    return-object v0
.end method

.method public getNumberOfRules()I
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRule(I)Lorg/apache/poi/hssf/record/CFRuleRecord;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->checkRuleIndex(I)V

    .line 127
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/CFRuleRecord;

    return-object v0
.end method

.method public setRule(ILorg/apache/poi/hssf/record/CFRuleRecord;)V
    .locals 2
    .param p1, "idx"    # I
    .param p2, "r"    # Lorg/apache/poi/hssf/record/CFRuleRecord;

    .prologue
    .line 130
    if-nez p2, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->checkRuleIndex(I)V

    .line 134
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 135
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 158
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v3, "[CF]\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 159
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    if-eqz v3, :cond_0

    .line 161
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 168
    const-string/jumbo v3, "[/CF]\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 165
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/CFRuleRecord;

    .line 166
    .local v1, "cfRule":Lorg/apache/poi/hssf/record/CFRuleRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public updateFormulasAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;I)Z
    .locals 11
    .param p1, "shifter"    # Lorg/apache/poi/ss/formula/FormulaShifter;
    .param p2, "currentExternSheetIx"    # I

    .prologue
    .line 184
    iget-object v10, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-virtual {v10}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->getCellRanges()[Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 185
    .local v0, "cellRanges":[Lorg/apache/poi/ss/util/CellRangeAddress;
    const/4 v1, 0x0

    .line 186
    .local v1, "changed":Z
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v9, "temp":Ljava/util/List;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v10, v0

    if-lt v4, v10, :cond_0

    .line 200
    if-eqz v1, :cond_4

    .line 201
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    .line 202
    .local v5, "nRanges":I
    if-nez v5, :cond_3

    .line 203
    const/4 v10, 0x0

    .line 222
    .end local v5    # "nRanges":I
    :goto_1
    return v10

    .line 188
    :cond_0
    aget-object v3, v0, v4

    .line 189
    .local v3, "craOld":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-static {p1, v3, p2}, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->shiftRange(Lorg/apache/poi/ss/formula/FormulaShifter;Lorg/apache/poi/ss/util/CellRangeAddress;I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v2

    .line 190
    .local v2, "craNew":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-nez v2, :cond_2

    .line 191
    const/4 v1, 0x1

    .line 187
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 194
    :cond_2
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    if-eq v2, v3, :cond_1

    .line 196
    const/4 v1, 0x1

    goto :goto_2

    .line 205
    .end local v2    # "craNew":Lorg/apache/poi/ss/util/CellRangeAddress;
    .end local v3    # "craOld":Lorg/apache/poi/ss/util/CellRangeAddress;
    .restart local v5    # "nRanges":I
    :cond_3
    new-array v6, v5, [Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 206
    .local v6, "newRanges":[Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-interface {v9, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 207
    iget-object v10, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-virtual {v10, v6}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->setCellRanges([Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 210
    .end local v5    # "nRanges":I
    .end local v6    # "newRanges":[Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_4
    const/4 v4, 0x0

    :goto_3
    iget-object v10, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lt v4, v10, :cond_5

    .line 222
    const/4 v10, 0x1

    goto :goto_1

    .line 211
    :cond_5
    iget-object v10, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/hssf/record/CFRuleRecord;

    .line 213
    .local v8, "rule":Lorg/apache/poi/hssf/record/CFRuleRecord;
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getParsedExpression1()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v7

    .line 214
    .local v7, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    if-eqz v7, :cond_6

    invoke-virtual {p1, v7, p2}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;I)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 215
    invoke-virtual {v8, v7}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setParsedExpression1([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 217
    :cond_6
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getParsedExpression2()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v7

    .line 218
    if-eqz v7, :cond_7

    invoke-virtual {p1, v7, p2}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;I)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 219
    invoke-virtual {v8, v7}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setParsedExpression2([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 210
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method public visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 3
    .param p1, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    .line 173
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->header:Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-interface {p1, v2}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 174
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 178
    return-void

    .line 175
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/CFRecordsAggregate;->rules:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/CFRuleRecord;

    .line 176
    .local v1, "rule":Lorg/apache/poi/hssf/record/CFRuleRecord;
    invoke-interface {p1, v1}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
