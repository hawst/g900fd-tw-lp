.class public final Lorg/apache/poi/hssf/record/PasswordRecord;
.super Lorg/apache/poi/hssf/record/StandardRecord;
.source "PasswordRecord.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final sid:S = 0x13s


# instance fields
.field private field_1_password:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "password"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 35
    iput p1, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    .line 36
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 39
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    .line 40
    return-void
.end method

.method public static hashPassword(Ljava/lang/String;)S
    .locals 6
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 46
    .local v3, "passwordCharacters":[B
    const/4 v2, 0x0

    .line 47
    .local v2, "hash":I
    array-length v4, v3

    if-lez v4, :cond_0

    .line 48
    array-length v0, v3

    .local v0, "charIndex":I
    move v1, v0

    .line 49
    .end local v0    # "charIndex":I
    .local v1, "charIndex":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "charIndex":I
    .restart local v0    # "charIndex":I
    if-gtz v1, :cond_1

    .line 54
    shr-int/lit8 v4, v2, 0xe

    and-int/lit8 v4, v4, 0x1

    shl-int/lit8 v5, v2, 0x1

    and-int/lit16 v5, v5, 0x7fff

    or-int v2, v4, v5

    .line 55
    array-length v4, v3

    xor-int/2addr v2, v4

    .line 56
    const v4, 0xce4b

    xor-int/2addr v2, v4

    .line 58
    .end local v0    # "charIndex":I
    :cond_0
    int-to-short v4, v2

    return v4

    .line 50
    .restart local v0    # "charIndex":I
    :cond_1
    shr-int/lit8 v4, v2, 0xe

    and-int/lit8 v4, v4, 0x1

    shl-int/lit8 v5, v2, 0x1

    and-int/lit16 v5, v5, 0x7fff

    or-int v2, v4, v5

    .line 51
    aget-byte v4, v3, v0

    xor-int/2addr v2, v4

    move v1, v0

    .end local v0    # "charIndex":I
    .restart local v1    # "charIndex":I
    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lorg/apache/poi/hssf/record/PasswordRecord;

    iget v1, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/PasswordRecord;-><init>(I)V

    return-object v0
.end method

.method protected getDataSize()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x2

    return v0
.end method

.method public getPassword()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0x13

    return v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 90
    iget v0, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 91
    return-void
.end method

.method public setPassword(I)V
    .locals 0
    .param p1, "password"    # I

    .prologue
    .line 68
    iput p1, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    .line 69
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 83
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[PASSWORD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    const-string/jumbo v1, "    .password = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hssf/record/PasswordRecord;->field_1_password:I

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    const-string/jumbo v1, "[/PASSWORD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
