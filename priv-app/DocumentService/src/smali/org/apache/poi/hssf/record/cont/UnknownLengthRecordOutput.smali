.class final Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;
.super Ljava/lang/Object;
.source "UnknownLengthRecordOutput.java"

# interfaces
.implements Lorg/apache/poi/util/LittleEndianOutput;


# static fields
.field private static final MAX_DATA_SIZE:I = 0x2020


# instance fields
.field private final _byteBuffer:[B

.field private final _dataSizeOutput:Lorg/apache/poi/util/LittleEndianOutput;

.field private final _originalOut:Lorg/apache/poi/util/LittleEndianOutput;

.field private _out:Lorg/apache/poi/util/LittleEndianOutput;

.field private _size:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/util/LittleEndianOutput;I)V
    .locals 4
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;
    .param p2, "sid"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_originalOut:Lorg/apache/poi/util/LittleEndianOutput;

    .line 43
    invoke-interface {p1, p2}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 44
    instance-of v1, p1, Lorg/apache/poi/util/DelayableLittleEndianOutput;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 46
    check-cast v0, Lorg/apache/poi/util/DelayableLittleEndianOutput;

    .line 47
    .local v0, "dleo":Lorg/apache/poi/util/DelayableLittleEndianOutput;
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lorg/apache/poi/util/DelayableLittleEndianOutput;->createDelayedOutput(I)Lorg/apache/poi/util/LittleEndianOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_dataSizeOutput:Lorg/apache/poi/util/LittleEndianOutput;

    .line 48
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_byteBuffer:[B

    .line 49
    iput-object p1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    .line 56
    .end local v0    # "dleo":Lorg/apache/poi/util/DelayableLittleEndianOutput;
    :goto_0
    return-void

    .line 52
    :cond_0
    iput-object p1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_dataSizeOutput:Lorg/apache/poi/util/LittleEndianOutput;

    .line 53
    const/16 v1, 0x2020

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_byteBuffer:[B

    .line 54
    new-instance v1, Lorg/apache/poi/util/LittleEndianByteArrayOutputStream;

    iget-object v2, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_byteBuffer:[B

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/util/LittleEndianByteArrayOutputStream;-><init>([BI)V

    iput-object v1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    goto :goto_0
.end method


# virtual methods
.method public getAvailableSpace()I
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Record already terminated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    rsub-int v0, v0, 0x2020

    return v0
.end method

.method public getTotalSize()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public terminate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Record already terminated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_dataSizeOutput:Lorg/apache/poi/util/LittleEndianOutput;

    iget v1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    invoke-interface {v0, v1}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 78
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_byteBuffer:[B

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_originalOut:Lorg/apache/poi/util/LittleEndianOutput;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_byteBuffer:[B

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    invoke-interface {v0, v1, v2, v3}, Lorg/apache/poi/util/LittleEndianOutput;->write([BII)V

    .line 80
    iput-object v4, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    .line 84
    :goto_0
    return-void

    .line 83
    :cond_1
    iput-object v4, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    goto :goto_0
.end method

.method public write([B)V
    .locals 2
    .param p1, "b"    # [B

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1}, Lorg/apache/poi/util/LittleEndianOutput;->write([B)V

    .line 88
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 89
    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/poi/util/LittleEndianOutput;->write([BII)V

    .line 92
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 93
    return-void
.end method

.method public writeByte(I)V
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1}, Lorg/apache/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 96
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 97
    return-void
.end method

.method public writeDouble(D)V
    .locals 1
    .param p1, "v"    # D

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1, p2}, Lorg/apache/poi/util/LittleEndianOutput;->writeDouble(D)V

    .line 100
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 101
    return-void
.end method

.method public writeInt(I)V
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 104
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 105
    return-void
.end method

.method public writeLong(J)V
    .locals 1
    .param p1, "v"    # J

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1, p2}, Lorg/apache/poi/util/LittleEndianOutput;->writeLong(J)V

    .line 108
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 109
    return-void
.end method

.method public writeShort(I)V
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    invoke-interface {v0, p1}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 112
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->_size:I

    .line 113
    return-void
.end method
