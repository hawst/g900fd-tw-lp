.class public Lorg/apache/poi/hssf/usermodel/HSSFPicture;
.super Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;
.source "HSSFPicture.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/Picture;


# static fields
.field public static final PICTURE_TYPE_DIB:I = 0x7

.field public static final PICTURE_TYPE_EMF:I = 0x2

.field public static final PICTURE_TYPE_JPEG:I = 0x5

.field public static final PICTURE_TYPE_PICT:I = 0x4

.field public static final PICTURE_TYPE_PNG:I = 0x6

.field public static final PICTURE_TYPE_WMF:I = 0x3

.field private static final PX_DEFAULT:F = 32.0f

.field private static final PX_MODIFIED:F = 36.56f

.field private static final PX_ROW:I = 0xf

.field private static logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/poi/hssf/usermodel/HSSFPicture;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->logger:Lorg/apache/poi/util/POILogger;

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V
    .locals 0
    .param p1, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "objRecord"    # Lorg/apache/poi/hssf/record/ObjRecord;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .param p2, "anchor"    # Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V

    .line 75
    const/16 v1, 0x4b

    invoke-super {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;->setShapeType(I)V

    .line 76
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    .line 77
    .local v0, "cod":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 78
    return-void
.end method

.method private getColumnWidthInPixels(I)F
    .locals 3
    .param p1, "column"    # I

    .prologue
    .line 214
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getColumnWidth(I)I

    move-result v0

    .line 215
    .local v0, "cw":I
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPixelWidth(I)F

    move-result v1

    .line 217
    .local v1, "px":F
    int-to-float v2, v0

    div-float/2addr v2, v1

    return v2
.end method

.method private getPixelWidth(I)F
    .locals 3
    .param p1, "column"    # I

    .prologue
    .line 232
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDefaultColumnWidth()I

    move-result v2

    mul-int/lit16 v1, v2, 0x100

    .line 233
    .local v1, "def":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getColumnWidth(I)I

    move-result v0

    .line 235
    .local v0, "cw":I
    if-ne v0, v1, :cond_0

    const/high16 v2, 0x42000000    # 32.0f

    :goto_0
    return v2

    :cond_0
    const v2, 0x42123d71    # 36.56f

    goto :goto_0
.end method

.method private getRowHeightInPixels(I)F
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 222
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v1

    .line 224
    .local v1, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v2

    int-to-float v0, v2

    .line 227
    .local v0, "height":F
    :goto_0
    const/high16 v2, 0x41700000    # 15.0f

    div-float v2, v0, v2

    return v2

    .line 225
    .end local v0    # "height":F
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDefaultRowHeight()S

    move-result v2

    int-to-float v0, v2

    .restart local v0    # "height":F
    goto :goto_0
.end method


# virtual methods
.method afterInsert(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V
    .locals 4
    .param p1, "patriarch"    # Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .prologue
    .line 263
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->_getBoundAggregate()Lorg/apache/poi/hssf/record/EscherAggregate;

    move-result-object v0

    .line 264
    .local v0, "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    const/16 v3, -0xfef

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/apache/poi/hssf/record/EscherAggregate;->associateShapeToObjRecord(Lorg/apache/poi/ddf/EscherRecord;Lorg/apache/poi/hssf/record/Record;)V

    .line 266
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPictureIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v1

    .line 267
    .local v1, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherBSERecord;->getRef()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 268
    return-void
.end method

.method protected cloneShape()Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .locals 5

    .prologue
    .line 302
    new-instance v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v2}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 303
    .local v2, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->serialize()[B

    move-result-object v0

    .line 304
    .local v0, "inSp":[B
    const/4 v3, 0x0

    new-instance v4, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v4}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    invoke-virtual {v2, v0, v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 305
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/ObjRecord;->cloneViaReserialise()Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/ObjRecord;

    .line 306
    .local v1, "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    new-instance v3, Lorg/apache/poi/hssf/usermodel/HSSFPicture;

    invoke-direct {v3, v2, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;)V

    return-object v3
.end method

.method protected createSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 3

    .prologue
    .line 96
    invoke-super {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;->createSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 97
    .local v1, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v2, -0xff5

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 98
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x1ce

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 99
    const/16 v2, 0x1ff

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 100
    const/16 v2, -0xff3

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->removeChildRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    .line 101
    return-object v1
.end method

.method public getFileName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 274
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v2

    .line 275
    const/16 v3, 0x105

    .line 274
    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 277
    .local v1, "propFile":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-nez v1, :cond_0

    .line 278
    const-string/jumbo v2, ""

    .line 282
    :goto_0
    return-object v2

    .line 280
    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v3

    const-string/jumbo v4, "UTF-16LE"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string/jumbo v2, ""

    goto :goto_0
.end method

.method public getImageDimension()Lorg/apache/poi/java/awt/Dimension;
    .locals 5

    .prologue
    .line 244
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPictureIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v0

    .line 245
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherBlipRecord;->getPicturedata()[B

    move-result-object v1

    .line 246
    .local v1, "data":[B
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipTypeWin32()B

    move-result v2

    .line 247
    .local v2, "type":I
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v3, v2}, Lorg/apache/poi/ss/util/ImageUtils;->getImageDimension(Ljava/io/InputStream;I)Lorg/apache/poi/java/awt/Dimension;

    move-result-object v3

    return-object v3
.end method

.method public getPictureData()Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    .locals 3

    .prologue
    .line 256
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v1

    .line 257
    .local v1, "iwb":Lorg/apache/poi/hssf/model/InternalWorkbook;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPictureIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherBSERecord;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v0

    .line 258
    .local v0, "blipRecord":Lorg/apache/poi/ddf/EscherBlipRecord;
    new-instance v2, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;

    invoke-direct {v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;-><init>(Lorg/apache/poi/ddf/EscherBlipRecord;)V

    return-object v2
.end method

.method public bridge synthetic getPictureData()Lorg/apache/poi/ss/usermodel/PictureData;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPictureData()Lorg/apache/poi/hssf/usermodel/HSSFPictureData;

    move-result-object v0

    return-object v0
.end method

.method public getPictureIndex()I
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v1

    const/16 v2, 0x104

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 83
    .local v0, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v0, :cond_0

    .line 84
    const/4 v1, -0x1

    .line 86
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    goto :goto_0
.end method

.method public getPreferredSize()Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
    .locals 2

    .prologue
    .line 154
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPreferredSize(D)Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    move-result-object v0

    return-object v0
.end method

.method public getPreferredSize(D)Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
    .locals 27
    .param p1, "scale"    # D

    .prologue
    .line 165
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getAnchor()Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    .line 167
    .local v2, "anchor":Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getImageDimension()Lorg/apache/poi/java/awt/Dimension;

    move-result-object v20

    .line 168
    .local v20, "size":Lorg/apache/poi/java/awt/Dimension;
    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Dimension;->getWidth()D

    move-result-wide v22

    mul-double v18, v22, p1

    .line 169
    .local v18, "scaledWidth":D
    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Dimension;->getHeight()D

    move-result-wide v22

    mul-double v16, v22, p1

    .line 171
    .local v16, "scaledHeight":D
    const/16 v21, 0x0

    .line 174
    .local v21, "w":F
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getColumnWidthInPixels(I)F

    move-result v22

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx1()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    const/high16 v25, 0x44800000    # 1024.0f

    div-float v24, v24, v25

    sub-float v23, v23, v24

    mul-float v22, v22, v23

    add-float v21, v21, v22

    .line 175
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v22

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    int-to-short v3, v0

    .line 176
    .local v3, "col2":S
    const/4 v7, 0x0

    .local v7, "dx2":I
    move v6, v3

    .line 178
    .end local v3    # "col2":S
    .local v6, "col2":S
    :goto_0
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    cmpg-double v22, v22, v18

    if-ltz v22, :cond_0

    .line 182
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    cmpl-double v22, v22, v18

    if-lez v22, :cond_3

    .line 184
    add-int/lit8 v22, v6, -0x1

    move/from16 v0, v22

    int-to-short v3, v0

    .line 185
    .end local v6    # "col2":S
    .restart local v3    # "col2":S
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getColumnWidthInPixels(I)F

    move-result v22

    move/from16 v0, v22

    float-to-double v8, v0

    .line 186
    .local v8, "cw":D
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    sub-double v10, v22, v18

    .line 187
    .local v10, "delta":D
    sub-double v22, v8, v10

    div-double v22, v22, v8

    const-wide/high16 v24, 0x4090000000000000L    # 1024.0

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v7, v0

    .line 189
    .end local v8    # "cw":D
    .end local v10    # "delta":D
    :goto_1
    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    .line 190
    invoke-virtual {v2, v7}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDx2(I)V

    .line 192
    const/4 v13, 0x0

    .line 193
    .local v13, "h":F
    const/high16 v22, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy1()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    const/high16 v24, 0x43800000    # 256.0f

    div-float v23, v23, v24

    sub-float v22, v22, v23

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v23

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getRowHeightInPixels(I)F

    move-result v23

    mul-float v22, v22, v23

    add-float v13, v13, v22

    .line 194
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v22

    add-int/lit8 v14, v22, 0x1

    .line 195
    .local v14, "row2":I
    const/4 v12, 0x0

    .local v12, "dy2":I
    move v15, v14

    .line 197
    .end local v14    # "row2":I
    .local v15, "row2":I
    :goto_2
    float-to-double v0, v13

    move-wide/from16 v22, v0

    cmpg-double v22, v22, v16

    if-ltz v22, :cond_1

    .line 200
    float-to-double v0, v13

    move-wide/from16 v22, v0

    cmpl-double v22, v22, v16

    if-lez v22, :cond_2

    .line 201
    add-int/lit8 v14, v15, -0x1

    .line 202
    .end local v15    # "row2":I
    .restart local v14    # "row2":I
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getRowHeightInPixels(I)F

    move-result v22

    move/from16 v0, v22

    float-to-double v4, v0

    .line 203
    .local v4, "ch":D
    float-to-double v0, v13

    move-wide/from16 v22, v0

    sub-double v10, v22, v16

    .line 204
    .restart local v10    # "delta":D
    sub-double v22, v4, v10

    div-double v22, v22, v4

    const-wide/high16 v24, 0x4070000000000000L    # 256.0

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v12, v0

    .line 206
    .end local v4    # "ch":D
    .end local v10    # "delta":D
    :goto_3
    invoke-virtual {v2, v14}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setRow2(I)V

    .line 207
    invoke-virtual {v2, v12}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDy2(I)V

    .line 209
    return-object v2

    .line 179
    .end local v3    # "col2":S
    .end local v12    # "dy2":I
    .end local v13    # "h":F
    .end local v14    # "row2":I
    .restart local v6    # "col2":S
    :cond_0
    add-int/lit8 v22, v6, 0x1

    move/from16 v0, v22

    int-to-short v3, v0

    .end local v6    # "col2":S
    .restart local v3    # "col2":S
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getColumnWidthInPixels(I)F

    move-result v22

    add-float v21, v21, v22

    move v6, v3

    .end local v3    # "col2":S
    .restart local v6    # "col2":S
    goto/16 :goto_0

    .line 198
    .end local v6    # "col2":S
    .restart local v3    # "col2":S
    .restart local v12    # "dy2":I
    .restart local v13    # "h":F
    .restart local v15    # "row2":I
    :cond_1
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "row2":I
    .restart local v14    # "row2":I
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getRowHeightInPixels(I)F

    move-result v22

    add-float v13, v13, v22

    move v15, v14

    .end local v14    # "row2":I
    .restart local v15    # "row2":I
    goto :goto_2

    :cond_2
    move v14, v15

    .end local v15    # "row2":I
    .restart local v14    # "row2":I
    goto :goto_3

    .end local v3    # "col2":S
    .end local v12    # "dy2":I
    .end local v13    # "h":F
    .end local v14    # "row2":I
    .restart local v6    # "col2":S
    :cond_3
    move v3, v6

    .end local v6    # "col2":S
    .restart local v3    # "col2":S
    goto :goto_1
.end method

.method public bridge synthetic getPreferredSize()Lorg/apache/poi/ss/usermodel/ClientAnchor;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPreferredSize()Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    move-result-object v0

    return-object v0
.end method

.method public resize()V
    .locals 2

    .prologue
    .line 144
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->resize(D)V

    .line 145
    return-void
.end method

.method public resize(D)V
    .locals 9
    .param p1, "scale"    # D

    .prologue
    const/4 v7, 0x0

    .line 117
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getAnchor()Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    .line 118
    .local v0, "anchor":Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setAnchorType(I)V

    .line 120
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->getPreferredSize(D)Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    move-result-object v2

    .line 122
    .local v2, "pref":Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v4

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v5

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v3, v4, v5

    .line 123
    .local v3, "row2":I
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v4

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    move-result v5

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v6

    sub-int/2addr v5, v6

    add-int v1, v4, v5

    .line 125
    .local v1, "col2":I
    int-to-short v4, v1

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    .line 126
    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDx1(I)V

    .line 127
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx2()I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDx2(I)V

    .line 129
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setRow2(I)V

    .line 130
    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDy1(I)V

    .line 131
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy2()I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDy2(I)V

    .line 132
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 5
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 288
    :try_start_0
    new-instance v1, Lorg/apache/poi/ddf/EscherComplexProperty;

    const/16 v2, 0x105

    const/4 v3, 0x1

    const-string/jumbo v4, "UTF-16LE"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/poi/ddf/EscherComplexProperty;-><init>(SZ[B)V

    .line 289
    .local v1, "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    .end local v1    # "prop":Lorg/apache/poi/ddf/EscherComplexProperty;
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v2, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x7

    const-string/jumbo v4, "Unsupported encoding: UTF-16LE"

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public setPictureIndex(I)V
    .locals 4
    .param p1, "pictureIndex"    # I

    .prologue
    .line 91
    new-instance v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v1, 0x104

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, p1}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPicture;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 92
    return-void
.end method

.method public setShapeType(I)V
    .locals 3
    .param p1, "shapeType"    # I

    .prologue
    .line 297
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Shape type can not be changed in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
