.class public final Lorg/apache/poi/hssf/util/HSSFRegionUtil;
.super Ljava/lang/Object;
.source "HSSFRegionUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public static setBorderBottom(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "border"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 141
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setBorderBottom(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 142
    return-void
.end method

.method public static setBorderBottom(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "border"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 129
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setBorderBottom(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 130
    return-void
.end method

.method public static setBorderLeft(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "border"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 61
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setBorderLeft(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 62
    return-void
.end method

.method public static setBorderLeft(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "border"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 48
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setBorderLeft(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 49
    return-void
.end method

.method public static setBorderRight(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "border"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 101
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setBorderRight(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 102
    return-void
.end method

.method public static setBorderRight(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "border"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 89
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setBorderRight(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 90
    return-void
.end method

.method public static setBorderTop(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "border"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 181
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setBorderTop(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 182
    return-void
.end method

.method public static setBorderTop(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "border"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 169
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setBorderTop(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 170
    return-void
.end method

.method public static setBottomBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "color"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 161
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setBottomBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 162
    return-void
.end method

.method public static setBottomBorderColor(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "color"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 149
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setBottomBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 150
    return-void
.end method

.method public static setLeftBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "color"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 81
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setLeftBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 82
    return-void
.end method

.method public static setLeftBorderColor(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "color"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 69
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setLeftBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 70
    return-void
.end method

.method public static setRightBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "color"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 121
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setRightBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 122
    return-void
.end method

.method public static setRightBorderColor(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "color"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 109
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setRightBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 110
    return-void
.end method

.method public static setTopBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .param p0, "color"    # I
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 201
    invoke-static {p0, p1, p2, p3}, Lorg/apache/poi/ss/util/RegionUtil;->setTopBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/usermodel/Sheet;Lorg/apache/poi/ss/usermodel/Workbook;)V

    .line 202
    return-void
.end method

.method public static setTopBorderColor(SLorg/apache/poi/hssf/util/Region;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p0, "color"    # S
    .param p1, "region"    # Lorg/apache/poi/hssf/util/Region;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 189
    invoke-static {p1}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lorg/apache/poi/hssf/util/HSSFRegionUtil;->setTopBorderColor(ILorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 190
    return-void
.end method

.method private static toCRA(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 1
    .param p0, "region"    # Lorg/apache/poi/ss/util/Region;

    .prologue
    .line 40
    invoke-static {p0}, Lorg/apache/poi/ss/util/Region;->convertToCellRangeAddress(Lorg/apache/poi/ss/util/Region;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    return-object v0
.end method
